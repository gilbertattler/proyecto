﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web;
namespace SolicitudIntegralMVC.Infraestructure.Concrete
{
    public class CustomAuthSessionRoleAttribute : AuthorizeAttribute
    {
        private string[] allowedRols;

        public CustomAuthSessionRoleAttribute(params string[] rols)
        {
            allowedRols = rols;
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool resultado = false;
            SolicitudIntegralMVC.Models.UserLogon usuario_logueado = SolicitudIntegralMVC.Infraestructure.Utilerias.ObtenerUsuarioAutentificado(httpContext);
            if(usuario_logueado != null)
            {
                if(allowedRols.Contains(usuario_logueado.PermisoUsuarioActual))
                {
                    resultado = true;
                }
            }            
            return resultado;
        }
    }
}