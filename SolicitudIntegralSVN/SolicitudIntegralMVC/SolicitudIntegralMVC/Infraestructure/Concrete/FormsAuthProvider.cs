﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SolicitudIntegralMVC.Infraestructure.Abstract;
using System.Web.Security;

namespace SolicitudIntegralMVC.Infraestructure.Concrete
{
    public class FormsAuthProvider : IAuthProvider
    {
        public bool Authenticate(string username, string password)
        {
            bool result = Membership.ValidateUser(username, password);
            if (result)
            {
                FormsAuthentication.SetAuthCookie(username, false);
            }
            return result;
        }
    }
}