﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Security.Cryptography;
using System.Text;


namespace SolicitudIntegralMVC.Infraestructure.Concrete
{

    public class SiteMember 
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }

    public class CustomMembershipProvider : MembershipProvider
    {
        //private PedidosCatalogos.Domain.Abstract.IUsuariosRepository usuariosRepository;

        public CustomMembershipProvider()
        {
            //usuariosRepository = PedidosCatalogos.Domain.Mocks.UsuariosRepositoryMock.Instance;
        }

        public override bool ValidateUser(string username, string password) 
        {
            //sso.Autentificacion.ValidateUser()
            //PedidosCatalogos.Domain.Entities.Usuario usuario = usuariosRepository.Usuarios.FirstOrDefault(x => x.NombreUsuario == username && x.Password == sha1Pswd);
            //return usuario != null;
            return true;
        }


        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            throw new NotImplementedException();
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            MembershipUser resultado = null;

            ValidatePasswordEventArgs args = new ValidatePasswordEventArgs(username, password, true);
            OnValidatingPassword(args);

            if (args.Cancel)
            {
                status = MembershipCreateStatus.InvalidPassword;
            }

            if (RequiresUniqueEmail && GetUserNameByEmail(email) != string.Empty)
            {
                status = MembershipCreateStatus.DuplicateEmail;
            }

            MembershipUser user = GetUser(username, true);

            //if (user == null)
            //{

            //    PedidosCatalogos.Domain.Entities.Usuario usuario = new PedidosCatalogos.Domain.Entities.Usuario();
                
            //    usuario.NombreUsuario = username;
            //    usuario.Password = password;
            //    usuario.CorreoElectronico = email;
            //    usuario.BorradoLogico = false;
            //    usuario.NombreCompleto = "";
            //    usuario.Rol = "";

            //    usuariosRepository.Save(usuario);

            //    status = MembershipCreateStatus.Success;

            //    resultado = GetUser(username, true);
            //}
            //else
            //{
            //    status = MembershipCreateStatus.DuplicateUserName;
            //}

            status = new MembershipCreateStatus();

            return resultado;
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }

        public override bool EnablePasswordReset
        {
            get { throw new NotImplementedException(); }
        }

        public override bool EnablePasswordRetrieval
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            MembershipUser resultado = null;

            //PedidosCatalogos.Domain.Entities.Usuario usuario = usuariosRepository.Usuarios.SingleOrDefault(u => u.NombreUsuario == username);

            //if (usuario != null)
            //{
            //    resultado = new MembershipUser("CustomMembershipProvider",
            //                                    username, usuario.IdUsuario, usuario.CorreoElectronico,
            //                                   string.Empty, string.Empty,
            //                                   true, false, DateTime.MinValue,
            //                                   DateTime.MinValue,
            //                                   DateTime.MinValue,
            //                                   DateTime.Now, DateTime.Now);
            //}
            return resultado;
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override string GetUserNameByEmail(string email)
        {
            string resultado = null;
            //PedidosCatalogos.Domain.Entities.Usuario usuario = usuariosRepository.Usuarios.SingleOrDefault(u => u.CorreoElectronico == email);

            //if (usuario != null)
            //{
            //    resultado = usuario.NombreUsuario;
            //}
            
            return resultado;
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredPasswordLength
        {
            get { return 6; }
        }

        public override int PasswordAttemptWindow
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { throw new NotImplementedException(); }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresUniqueEmail
        {
            get { throw new NotImplementedException(); }
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }
    }
}