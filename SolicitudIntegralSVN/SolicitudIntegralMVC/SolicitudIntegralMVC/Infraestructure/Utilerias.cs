﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Web.UI;
using System.IO;

namespace SolicitudIntegralMVC.Infraestructure
{
    public class Utilerias
    {

        public static SolicitudIntegralMVC.Models.UserLogon ObtenerUsuarioAutentificado(HttpContextBase context)
        {
            SolicitudIntegralMVC.Models.UserLogon resultado = null;

            if (context.Session["UsuarioLogueado"] != null)
            {
                resultado = context.Session["UsuarioLogueado"] as SolicitudIntegralMVC.Models.UserLogon;
            }

            return resultado;
        }

        //.Page page
        public static SolicitudIntegralMVC.Models.UserLogon ObtenerUsuarioAutentificado(System.Web.SessionState.HttpSessionState session)
        {
            SolicitudIntegralMVC.Models.UserLogon resultado = null;

            if (session["UsuarioLogueado"] != null)
            {
                resultado = session["UsuarioLogueado"] as SolicitudIntegralMVC.Models.UserLogon;
            }

            return resultado;
        }

        public static SolicitudIntegralMVC.Models.UserLogon ObtenerUsuarioAutentificado(System.Web.HttpSessionStateBase session)
        {
            SolicitudIntegralMVC.Models.UserLogon resultado = null;

            if (session["UsuarioLogueado"] != null)
            {
                resultado = session["UsuarioLogueado"] as SolicitudIntegralMVC.Models.UserLogon;
            }

            return resultado;
        }

        public static void GuardarUsuarioAutentificado(HttpContextBase context, SolicitudIntegralMVC.Models.UserLogon usuario)
        {
            context.Session["UsuarioLogueado"] = usuario;
        }

        public static string RenderPartialToString(Controller controller, string partialViewName, object model, ViewDataDictionary viewData, TempDataDictionary tempData)
        {
            ViewEngineResult result = ViewEngines.Engines.FindPartialView(controller.ControllerContext, partialViewName);

            if (result.View != null)
            {
                controller.ViewData.Model = model;
                StringBuilder sb = new StringBuilder();
                using (StringWriter sw = new StringWriter(sb))
                {
                    using (HtmlTextWriter output = new HtmlTextWriter(sw))
                    {
                        ViewContext viewContext = new ViewContext(controller.ControllerContext, result.View, viewData, tempData, output);
                        result.View.Render(viewContext, output);
                    }
                }

                return sb.ToString();
            }

            return String.Empty;
        }

        public static string ObtenerTipoTrabajador(string fichaRFC)
        {

            string regexRFC = "^(?<RFCC>[a-zA-Z]{4})\\W*(?<RFCN>\\d{4,6})(?<RFC>\\w{0,4})$";
            string regexFicha = "^(?<N>[0]*)(?<FICHA>\\d{5,8})$";
            string tipoTrabajador = "";

            System.Text.RegularExpressions.RegexOptions options = ((System.Text.RegularExpressions.RegexOptions.IgnorePatternWhitespace | System.Text.RegularExpressions.RegexOptions.Multiline) | System.Text.RegularExpressions.RegexOptions.IgnoreCase);


            if (System.Text.RegularExpressions.Regex.IsMatch(fichaRFC, regexFicha, options))
            {
                tipoTrabajador = "PEMEX";
            }
            else
            {
                if (System.Text.RegularExpressions.Regex.IsMatch(fichaRFC, regexRFC, options))
                {
                    tipoTrabajador = "COMPAÑIA";
                }
                else
                {
                    //No coincido con la validacion del RFC sin embargo se va a considerar como de compañia
                    tipoTrabajador = "COMPAÑIA";
                }
            }
            return tipoTrabajador;
        }

        public static string FormatearFicha(string ficha)
        {
            string resultado = ficha;
            string tipo_trabajador = ObtenerTipoTrabajador(ficha);

            if (tipo_trabajador == "PEMEX")
                resultado = String.Format("{0:00000000}", Convert.ToInt32(ficha));
            else
            {
                if (tipo_trabajador == "COMPAÑIA")
                {
                    resultado = ficha;
                }
            }
            return resultado;
        }

        public static string FormatearRFCCompania(string RFC)
        {
            return RFC.PadRight(14, '0');
        }

        public static void ValidarUsuario(System.Web.HttpContext context, System.Web.UI.Page pagina, Models.UserLogon usuario_logueado)
        {
            if (usuario_logueado == null)
            {
                pagina.Response.Redirect(System.Configuration.ConfigurationManager.AppSettings["urlTimeOutExpired"]);
            }
            else
            {

            }
        }

        public static void ValidarUsuario(System.Web.UI.Page pagina, Models.UserLogon usuario_logueado)
        {
            if (usuario_logueado == null)
            {
                pagina.Response.Redirect(System.Configuration.ConfigurationManager.AppSettings["urlTimeOutExpired"]);
            }
            else
            {

            }
        }

    }
}