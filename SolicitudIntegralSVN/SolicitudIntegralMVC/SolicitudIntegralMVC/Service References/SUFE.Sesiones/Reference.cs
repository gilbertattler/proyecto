﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.17929
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SolicitudIntegralMVC.SUFE.Sesiones {
    using System.Data;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://pemex.dctipn.org/", ConfigurationName="SUFE.Sesiones.SesionesCrtInfoSoap")]
    public interface SesionesCrtInfoSoap {
        
        // CODEGEN: Se está generando un contrato de mensaje, ya que el mensaje ObtenerIDRequest tiene encabezados.
        [System.ServiceModel.OperationContractAttribute(Action="http://pemex.dctipn.org/ObtenerID", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        SolicitudIntegralMVC.SUFE.Sesiones.ObtenerIDResponse ObtenerID(SolicitudIntegralMVC.SUFE.Sesiones.ObtenerIDRequest request);
        
        // CODEGEN: Se está generando un contrato de mensaje, ya que el mensaje ObtenerDatosRequest tiene encabezados.
        [System.ServiceModel.OperationContractAttribute(Action="http://pemex.dctipn.org/ObtenerDatos", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        SolicitudIntegralMVC.SUFE.Sesiones.ObtenerDatosResponse ObtenerDatos(SolicitudIntegralMVC.SUFE.Sesiones.ObtenerDatosRequest request);
        
        // CODEGEN: Se está generando un contrato de mensaje, ya que el mensaje ActualizarRequest tiene encabezados.
        [System.ServiceModel.OperationContractAttribute(Action="http://pemex.dctipn.org/Actualizar", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        SolicitudIntegralMVC.SUFE.Sesiones.ActualizarResponse Actualizar(SolicitudIntegralMVC.SUFE.Sesiones.ActualizarRequest request);
        
        // CODEGEN: Se está generando un contrato de mensaje, ya que el mensaje ObtenerCertificadosRequest tiene encabezados.
        [System.ServiceModel.OperationContractAttribute(Action="http://pemex.dctipn.org/ObtenerCertificados", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        SolicitudIntegralMVC.SUFE.Sesiones.ObtenerCertificadosResponse ObtenerCertificados(SolicitudIntegralMVC.SUFE.Sesiones.ObtenerCertificadosRequest request);
    }
    
    /// <comentarios/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://pemex.dctipn.org/")]
    public partial class CredencialesHeader : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string appKeyField;
        
        private System.Xml.XmlAttribute[] anyAttrField;
        
        /// <comentarios/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string AppKey {
            get {
                return this.appKeyField;
            }
            set {
                this.appKeyField = value;
                this.RaisePropertyChanged("AppKey");
            }
        }
        
        /// <comentarios/>
        [System.Xml.Serialization.XmlAnyAttributeAttribute()]
        public System.Xml.XmlAttribute[] AnyAttr {
            get {
                return this.anyAttrField;
            }
            set {
                this.anyAttrField = value;
                this.RaisePropertyChanged("AnyAttr");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <comentarios/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://pemex.dctipn.org/")]
    public partial class SesionesCrtInfo : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string sesionIDField;
        
        private string sistemaOrigenField;
        
        private string tipoPkcsField;
        
        private string pCreacionField;
        
        private string iDACField;
        
        private string atributoACField;
        
        private string fEstatusField;
        
        private string estatusField;
        
        /// <comentarios/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string SesionID {
            get {
                return this.sesionIDField;
            }
            set {
                this.sesionIDField = value;
                this.RaisePropertyChanged("SesionID");
            }
        }
        
        /// <comentarios/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string SistemaOrigen {
            get {
                return this.sistemaOrigenField;
            }
            set {
                this.sistemaOrigenField = value;
                this.RaisePropertyChanged("SistemaOrigen");
            }
        }
        
        /// <comentarios/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public string TipoPkcs {
            get {
                return this.tipoPkcsField;
            }
            set {
                this.tipoPkcsField = value;
                this.RaisePropertyChanged("TipoPkcs");
            }
        }
        
        /// <comentarios/>
        [System.Xml.Serialization.XmlElementAttribute(Order=3)]
        public string PCreacion {
            get {
                return this.pCreacionField;
            }
            set {
                this.pCreacionField = value;
                this.RaisePropertyChanged("PCreacion");
            }
        }
        
        /// <comentarios/>
        [System.Xml.Serialization.XmlElementAttribute(Order=4)]
        public string IDAC {
            get {
                return this.iDACField;
            }
            set {
                this.iDACField = value;
                this.RaisePropertyChanged("IDAC");
            }
        }
        
        /// <comentarios/>
        [System.Xml.Serialization.XmlElementAttribute(Order=5)]
        public string AtributoAC {
            get {
                return this.atributoACField;
            }
            set {
                this.atributoACField = value;
                this.RaisePropertyChanged("AtributoAC");
            }
        }
        
        /// <comentarios/>
        [System.Xml.Serialization.XmlElementAttribute(Order=6)]
        public string FEstatus {
            get {
                return this.fEstatusField;
            }
            set {
                this.fEstatusField = value;
                this.RaisePropertyChanged("FEstatus");
            }
        }
        
        /// <comentarios/>
        [System.Xml.Serialization.XmlElementAttribute(Order=7)]
        public string Estatus {
            get {
                return this.estatusField;
            }
            set {
                this.estatusField = value;
                this.RaisePropertyChanged("Estatus");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <comentarios/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://pemex.dctipn.org/")]
    public partial class Return : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string estatusField;
        
        private string mensajeField;
        
        /// <comentarios/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string Estatus {
            get {
                return this.estatusField;
            }
            set {
                this.estatusField = value;
                this.RaisePropertyChanged("Estatus");
            }
        }
        
        /// <comentarios/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string Mensaje {
            get {
                return this.mensajeField;
            }
            set {
                this.mensajeField = value;
                this.RaisePropertyChanged("Mensaje");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="ObtenerID", WrapperNamespace="http://pemex.dctipn.org/", IsWrapped=true)]
    public partial class ObtenerIDRequest {
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://pemex.dctipn.org/")]
        public SolicitudIntegralMVC.SUFE.Sesiones.CredencialesHeader CredencialesHeader;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://pemex.dctipn.org/", Order=0)]
        public string SistemaOrigen;
        
        public ObtenerIDRequest() {
        }
        
        public ObtenerIDRequest(SolicitudIntegralMVC.SUFE.Sesiones.CredencialesHeader CredencialesHeader, string SistemaOrigen) {
            this.CredencialesHeader = CredencialesHeader;
            this.SistemaOrigen = SistemaOrigen;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="ObtenerIDResponse", WrapperNamespace="http://pemex.dctipn.org/", IsWrapped=true)]
    public partial class ObtenerIDResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://pemex.dctipn.org/", Order=0)]
        public SolicitudIntegralMVC.SUFE.Sesiones.Return ObtenerIDResult;
        
        public ObtenerIDResponse() {
        }
        
        public ObtenerIDResponse(SolicitudIntegralMVC.SUFE.Sesiones.Return ObtenerIDResult) {
            this.ObtenerIDResult = ObtenerIDResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="ObtenerDatos", WrapperNamespace="http://pemex.dctipn.org/", IsWrapped=true)]
    public partial class ObtenerDatosRequest {
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://pemex.dctipn.org/")]
        public SolicitudIntegralMVC.SUFE.Sesiones.CredencialesHeader CredencialesHeader;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://pemex.dctipn.org/", Order=0)]
        public string ID;
        
        public ObtenerDatosRequest() {
        }
        
        public ObtenerDatosRequest(SolicitudIntegralMVC.SUFE.Sesiones.CredencialesHeader CredencialesHeader, string ID) {
            this.CredencialesHeader = CredencialesHeader;
            this.ID = ID;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="ObtenerDatosResponse", WrapperNamespace="http://pemex.dctipn.org/", IsWrapped=true)]
    public partial class ObtenerDatosResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://pemex.dctipn.org/", Order=0)]
        public SolicitudIntegralMVC.SUFE.Sesiones.SesionesCrtInfo ObtenerDatosResult;
        
        public ObtenerDatosResponse() {
        }
        
        public ObtenerDatosResponse(SolicitudIntegralMVC.SUFE.Sesiones.SesionesCrtInfo ObtenerDatosResult) {
            this.ObtenerDatosResult = ObtenerDatosResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="Actualizar", WrapperNamespace="http://pemex.dctipn.org/", IsWrapped=true)]
    public partial class ActualizarRequest {
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://pemex.dctipn.org/")]
        public SolicitudIntegralMVC.SUFE.Sesiones.CredencialesHeader CredencialesHeader;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://pemex.dctipn.org/", Order=0)]
        public SolicitudIntegralMVC.SUFE.Sesiones.SesionesCrtInfo SesionesCrtInfo;
        
        public ActualizarRequest() {
        }
        
        public ActualizarRequest(SolicitudIntegralMVC.SUFE.Sesiones.CredencialesHeader CredencialesHeader, SolicitudIntegralMVC.SUFE.Sesiones.SesionesCrtInfo SesionesCrtInfo) {
            this.CredencialesHeader = CredencialesHeader;
            this.SesionesCrtInfo = SesionesCrtInfo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="ActualizarResponse", WrapperNamespace="http://pemex.dctipn.org/", IsWrapped=true)]
    public partial class ActualizarResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://pemex.dctipn.org/", Order=0)]
        public SolicitudIntegralMVC.SUFE.Sesiones.Return ActualizarResult;
        
        public ActualizarResponse() {
        }
        
        public ActualizarResponse(SolicitudIntegralMVC.SUFE.Sesiones.Return ActualizarResult) {
            this.ActualizarResult = ActualizarResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="ObtenerCertificados", WrapperNamespace="http://pemex.dctipn.org/", IsWrapped=true)]
    public partial class ObtenerCertificadosRequest {
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://pemex.dctipn.org/")]
        public SolicitudIntegralMVC.SUFE.Sesiones.CredencialesHeader CredencialesHeader;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://pemex.dctipn.org/", Order=0)]
        public string ID;
        
        public ObtenerCertificadosRequest() {
        }
        
        public ObtenerCertificadosRequest(SolicitudIntegralMVC.SUFE.Sesiones.CredencialesHeader CredencialesHeader, string ID) {
            this.CredencialesHeader = CredencialesHeader;
            this.ID = ID;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="ObtenerCertificadosResponse", WrapperNamespace="http://pemex.dctipn.org/", IsWrapped=true)]
    public partial class ObtenerCertificadosResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://pemex.dctipn.org/", Order=0)]
        public System.Data.DataSet ObtenerCertificadosResult;
        
        public ObtenerCertificadosResponse() {
        }
        
        public ObtenerCertificadosResponse(System.Data.DataSet ObtenerCertificadosResult) {
            this.ObtenerCertificadosResult = ObtenerCertificadosResult;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface SesionesCrtInfoSoapChannel : SolicitudIntegralMVC.SUFE.Sesiones.SesionesCrtInfoSoap, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class SesionesCrtInfoSoapClient : System.ServiceModel.ClientBase<SolicitudIntegralMVC.SUFE.Sesiones.SesionesCrtInfoSoap>, SolicitudIntegralMVC.SUFE.Sesiones.SesionesCrtInfoSoap {
        
        public SesionesCrtInfoSoapClient() {
        }
        
        public SesionesCrtInfoSoapClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public SesionesCrtInfoSoapClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public SesionesCrtInfoSoapClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public SesionesCrtInfoSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        SolicitudIntegralMVC.SUFE.Sesiones.ObtenerIDResponse SolicitudIntegralMVC.SUFE.Sesiones.SesionesCrtInfoSoap.ObtenerID(SolicitudIntegralMVC.SUFE.Sesiones.ObtenerIDRequest request) {
            return base.Channel.ObtenerID(request);
        }
        
        public SolicitudIntegralMVC.SUFE.Sesiones.Return ObtenerID(SolicitudIntegralMVC.SUFE.Sesiones.CredencialesHeader CredencialesHeader, string SistemaOrigen) {
            SolicitudIntegralMVC.SUFE.Sesiones.ObtenerIDRequest inValue = new SolicitudIntegralMVC.SUFE.Sesiones.ObtenerIDRequest();
            inValue.CredencialesHeader = CredencialesHeader;
            inValue.SistemaOrigen = SistemaOrigen;
            SolicitudIntegralMVC.SUFE.Sesiones.ObtenerIDResponse retVal = ((SolicitudIntegralMVC.SUFE.Sesiones.SesionesCrtInfoSoap)(this)).ObtenerID(inValue);
            return retVal.ObtenerIDResult;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        SolicitudIntegralMVC.SUFE.Sesiones.ObtenerDatosResponse SolicitudIntegralMVC.SUFE.Sesiones.SesionesCrtInfoSoap.ObtenerDatos(SolicitudIntegralMVC.SUFE.Sesiones.ObtenerDatosRequest request) {
            return base.Channel.ObtenerDatos(request);
        }
        
        public SolicitudIntegralMVC.SUFE.Sesiones.SesionesCrtInfo ObtenerDatos(SolicitudIntegralMVC.SUFE.Sesiones.CredencialesHeader CredencialesHeader, string ID) {
            SolicitudIntegralMVC.SUFE.Sesiones.ObtenerDatosRequest inValue = new SolicitudIntegralMVC.SUFE.Sesiones.ObtenerDatosRequest();
            inValue.CredencialesHeader = CredencialesHeader;
            inValue.ID = ID;
            SolicitudIntegralMVC.SUFE.Sesiones.ObtenerDatosResponse retVal = ((SolicitudIntegralMVC.SUFE.Sesiones.SesionesCrtInfoSoap)(this)).ObtenerDatos(inValue);
            return retVal.ObtenerDatosResult;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        SolicitudIntegralMVC.SUFE.Sesiones.ActualizarResponse SolicitudIntegralMVC.SUFE.Sesiones.SesionesCrtInfoSoap.Actualizar(SolicitudIntegralMVC.SUFE.Sesiones.ActualizarRequest request) {
            return base.Channel.Actualizar(request);
        }
        
        public SolicitudIntegralMVC.SUFE.Sesiones.Return Actualizar(SolicitudIntegralMVC.SUFE.Sesiones.CredencialesHeader CredencialesHeader, SolicitudIntegralMVC.SUFE.Sesiones.SesionesCrtInfo SesionesCrtInfo) {
            SolicitudIntegralMVC.SUFE.Sesiones.ActualizarRequest inValue = new SolicitudIntegralMVC.SUFE.Sesiones.ActualizarRequest();
            inValue.CredencialesHeader = CredencialesHeader;
            inValue.SesionesCrtInfo = SesionesCrtInfo;
            SolicitudIntegralMVC.SUFE.Sesiones.ActualizarResponse retVal = ((SolicitudIntegralMVC.SUFE.Sesiones.SesionesCrtInfoSoap)(this)).Actualizar(inValue);
            return retVal.ActualizarResult;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        SolicitudIntegralMVC.SUFE.Sesiones.ObtenerCertificadosResponse SolicitudIntegralMVC.SUFE.Sesiones.SesionesCrtInfoSoap.ObtenerCertificados(SolicitudIntegralMVC.SUFE.Sesiones.ObtenerCertificadosRequest request) {
            return base.Channel.ObtenerCertificados(request);
        }
        
        public System.Data.DataSet ObtenerCertificados(SolicitudIntegralMVC.SUFE.Sesiones.CredencialesHeader CredencialesHeader, string ID) {
            SolicitudIntegralMVC.SUFE.Sesiones.ObtenerCertificadosRequest inValue = new SolicitudIntegralMVC.SUFE.Sesiones.ObtenerCertificadosRequest();
            inValue.CredencialesHeader = CredencialesHeader;
            inValue.ID = ID;
            SolicitudIntegralMVC.SUFE.Sesiones.ObtenerCertificadosResponse retVal = ((SolicitudIntegralMVC.SUFE.Sesiones.SesionesCrtInfoSoap)(this)).ObtenerCertificados(inValue);
            return retVal.ObtenerCertificadosResult;
        }
    }
}
