﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="CargaPasajeros.aspx.cs" Inherits="SolicitudIntegralMVC.WebTransporteTerrestre.Solicitudterrestre.CargaPasajeros" %>

<%@ OutputCache Location="None" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register Src="~/WebTransporteTerrestre/Solicitudterrestre/Controles/ctlCargaPasajeros.ascx" TagPrefix="uc1" TagName="ctlCargaPasajeros" %>
<%@ Register Src="~/WebTransporteTerrestre/Recursos/controles/message-box.ascx" TagPrefix="uc1" TagName="messagebox" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <base target="_self" />
    <title>STPVT: Carga de Pasajeros</title>
    <link rel="stylesheet" href="~/WebTransporteTerrestre/Recursos/css/screen.css" type="text/css"
        media="screen, projection" />
    <link rel="stylesheet" href="~/WebTransporteTerrestre/Recursos/css/print.css" type="text/css"
        media="print" />
    <link href="~/WebTransporteTerrestre/Recursos/css/style.css" rel="stylesheet" type="text/css" />
    <!--[if IE]>
    <link rel="stylesheet" href="~/WebTransporteTerrestre/Recursos/css/ie.css" type="text/css" media="screen, projection" />
    <![endif]-->
</head>
<body>
    <form id="form1" runat="server">
        
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <contenttemplate>
        
                <div class="span-20 last">
                    <uc1:messagebox runat="server" id="ctlMensaje" />
                </div>

            <div class="span-20 last">
                <uc1:ctlCargaPasajeros runat="server" id="ctlPasajeros" />
            </div>

        </contenttemplate>
        </asp:UpdatePanel>

    </form>

</body>

</html>
