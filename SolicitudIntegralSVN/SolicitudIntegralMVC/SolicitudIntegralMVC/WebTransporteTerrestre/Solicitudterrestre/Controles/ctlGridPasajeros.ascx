﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlGridPasajeros.ascx.cs" Inherits="SolicitudIntegralMVC.WebTransporteTerrestre.Solicitudterrestre.Controles.ctlGridPasajeros" %>

<asp:Panel ID="pnlGrid"  runat="server" ScrollBars="Vertical" Height="137">
    <asp:GridView ID="gvPasajeros" runat="server" class="grid mGrid" 
        AutoGenerateColumns="False" onrowcommand="gvPasajeros_RowCommand" 
        onselectedindexchanged="gvPasajeros_SelectedIndexChanged" 
        CssClass="mGrid" >
        <Columns>
            <asp:TemplateField HeaderText="Ficha">
                <ItemTemplate>
                    <asp:Label ID="lblFicha" runat="server" Text='<%# Eval("Ficha") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Nombre">
                <ItemTemplate>
                    <asp:Label ID="lblNombre" runat="server" Text='<%# Eval("Nombre") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Tipo">
                <ItemTemplate>
                    <asp:Label ID="lblTipo" runat="server" Text='<%# Eval("Tipo") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <%--<asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="IBtnAnexo" runat="server" 
                        CommandArgument='<%# Eval("Ficha") %>' CommandName="Select" 
                        ImageUrl="~/WebTransporteTerrestre/Recursos/img/IconosHomologados16px/AnexosEnabled.png" 
                        ToolTip="Anexo" Visible="False" />
                </ItemTemplate>
            </asp:TemplateField>--%>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="ibtnEliminar" runat="server" CommandName="Eliminar" 
                        ImageUrl="~/WebTransporteTerrestre/Recursos/img/IconosHomologados16px/EliminarEnabled.png" 
                        CommandArgument='<%# Eval("Ficha") %>' ToolTip="Eliminar Solo para este dia" />

                    <asp:ImageButton ID="ImgEliminarSubsecuentes" runat="server" CommandName="EliminarSubsecuentes" 
                    ImageUrl="~/WebTransporteTerrestre/Recursos/img/IconosHomologados16px/EliminarSubsecuentesEnabled.png" 
                    CommandArgument='<%# Eval("Ficha") %>' ToolTip="Eliminar para este dia y los subsecuentes" />

                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <SelectedRowStyle CssClass="sel" />
        <AlternatingRowStyle CssClass="alt" />
    </asp:GridView>
</asp:Panel>
