﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolicitudIntegralMVC.WebTransporteTerrestre.Solicitudterrestre.Controles
{
    public partial class ctlCargaPasajeros : System.Web.UI.UserControl
    {
        public delegate void MensajeEventHandler(string Mensaje, string Tipo);
        public event MensajeEventHandler Mensaje;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //TransporteTerrestre.Negocios.Usuarios Usuario = Session["usuario_logueado"] as TransporteTerrestre.Negocios.Usuarios;
                Models.UserLogon usuario_logueado = Infraestructure.Utilerias.ObtenerUsuarioAutentificado(Page.Session);

                ctlGridPasajerosViaje.EliminaElemento += new  SolicitudIntegralMVC.WebTransporteTerrestre.Solicitudterrestre.Controles.ctlGridPasajeros.DeleteEventHandler(ctlGridPasajerosViaje_EliminaElemento);
                ctlGridPasajerosRetorno.EliminaElemento += new SolicitudIntegralMVC.WebTransporteTerrestre.Solicitudterrestre.Controles.ctlGridPasajeros.DeleteEventHandler(ctlGridPasajerosRetorno_EliminaElemento);
                ctlGridPasajerosViaje.Mensaje += new SolicitudIntegralMVC.WebTransporteTerrestre.Solicitudterrestre.Controles.ctlGridPasajeros.MensajeEventHandler(ctlGridPasajerosViaje_Mensaje);
                ctlGridPasajerosRetorno.Mensaje += new SolicitudIntegralMVC.WebTransporteTerrestre.Solicitudterrestre.Controles.ctlGridPasajeros.MensajeEventHandler(ctlGridPasajerosRetorno_Mensaje);
                ctlGridPasajerosViaje.EditaElemento += new SolicitudIntegralMVC.WebTransporteTerrestre.Solicitudterrestre.Controles.ctlGridPasajeros.EditEventHandler(ctlGridPasajerosViaje_EditaElemento);
                ctlGridPasajerosRetorno.EditaElemento += new SolicitudIntegralMVC.WebTransporteTerrestre.Solicitudterrestre.Controles.ctlGridPasajeros.EditEventHandler(ctlGridPasajerosRetorno_EditaElemento);

                if (!Page.IsPostBack)
                {
                    TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection Pasajeros = Page.Session["PasajerosSesion"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection;

                    TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection PasajerosRetorno = Page.Session["PasajerosRetornoSesion"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection;

                    TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.DatosVigenciaUtilerias DatosSolicitud = Page.Session["DatosSolicitud"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.DatosVigenciaUtilerias;

                    if (Pasajeros.Count() <= 0)
                    {
                        for (int i = 0; i < Convert.ToInt32(sldextDias.Maximum); i++)
                        {
                            TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo PasajeroMin = null;

                            System.DateTime FechaCompletaInicio = DatosSolicitud.VigenciaFechaInicio.AddDays(i);
                            System.DateTime Hora = Convert.ToDateTime(DatosSolicitud.HoraServicio);
                            FechaCompletaInicio = FechaCompletaInicio.AddHours(Hora.Hour).AddMinutes(Hora.Minute);

                            System.DateTime FechaCompletaFin = DatosSolicitud.VigenciaFechaInicio.AddDays(i);
                            System.DateTime HoraFin = Convert.ToDateTime(DatosSolicitud.HoraServicio).AddHours(12);
                            FechaCompletaFin = FechaCompletaFin.AddHours(HoraFin.Hour).AddMinutes(HoraFin.Minute);
                            if (Convert.ToDateTime(DatosSolicitud.HoraServicio).Hour > 12)
                            {
                                FechaCompletaFin = FechaCompletaFin.AddDays(1);
                            }

                            //revisar 
                            PasajerosRetorno.Agregar(i + 1, DatosSolicitud.VigenciaFechaInicio.AddDays(i), PasajeroMin, FechaCompletaInicio, FechaCompletaFin, "");
                            Page.Session["PasajerosRetornoSesion"] = PasajerosRetorno;
                            Pasajeros.Agregar(i + 1, DatosSolicitud.VigenciaFechaInicio.AddDays(i), PasajeroMin, FechaCompletaInicio, FechaCompletaFin, "");
                            Page.Session["PasajerosSesion"] = Pasajeros;
                        }
                    }
                    //nueva
                    else
                    {
                        for (int i = Pasajeros.Count(); i < Convert.ToInt32(sldextDias.Maximum); i++)
                        {
                            TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo PasajeroMin = null;

                            System.DateTime FechaCompletaInicio = DatosSolicitud.VigenciaFechaInicio.AddDays(i);
                            System.DateTime Hora = Convert.ToDateTime(DatosSolicitud.HoraServicio);
                            FechaCompletaInicio = FechaCompletaInicio.AddHours(Hora.Hour).AddMinutes(Hora.Minute);

                            System.DateTime FechaCompletaFin = DatosSolicitud.VigenciaFechaInicio.AddDays(i);
                            System.DateTime HoraFin = Convert.ToDateTime(DatosSolicitud.HoraServicio).AddHours(12);
                            FechaCompletaFin = FechaCompletaFin.AddHours(HoraFin.Hour).AddMinutes(HoraFin.Minute);
                            if (Convert.ToDateTime(DatosSolicitud.HoraServicio).Hour > 12)
                            {
                                FechaCompletaFin = FechaCompletaFin.AddDays(1);
                            }

                            //revisar
                            PasajerosRetorno.Agregar(i + 1, DatosSolicitud.VigenciaFechaInicio.AddDays(i), PasajeroMin, FechaCompletaInicio, FechaCompletaFin, "");
                            Page.Session["PasajerosRetornoSesion"] = PasajerosRetorno;
                            Pasajeros.Agregar(i + 1, DatosSolicitud.VigenciaFechaInicio.AddDays(i), PasajeroMin, FechaCompletaInicio, FechaCompletaFin, "");
                            Page.Session["PasajerosSesion"] = Pasajeros;
                        }
                    }
                    //hasta aqui
                    int? Indice = Pasajeros.ObtenerIndicexDia(Convert.ToInt32(sldextDias.Minimum));
                    int? IndiceRetorno = PasajerosRetorno.ObtenerIndicexDia(Convert.ToInt32(sldextDias.Minimum));
                    if (Indice != null)
                    {
                        ctlGridPasajerosViaje.LlenarGrid(Pasajeros.ColeccionPasajerosDia[Convert.ToInt32(Indice)].Pasajeros);
                        LlenaDatos(Indice, Pasajeros);
                    }
                    else
                    {
                        ctlGridPasajerosViaje.LlenarGrid(new System.Collections.Generic.List<TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo>());
                    }
                    if (IndiceRetorno != null)
                    {
                        ctlGridPasajerosRetorno.LlenarGrid(PasajerosRetorno.ColeccionPasajerosDia[Convert.ToInt32(IndiceRetorno)].Pasajeros);
                        LlenaDatos(Indice, Pasajeros);
                    }
                    else
                    {
                        ctlGridPasajerosRetorno.LlenarGrid(new System.Collections.Generic.List<TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo>());
                    }

                    if (DatosSolicitud.Origen == "CAS" || DatosSolicitud.Origen == "CASES" || DatosSolicitud.Destino == "CAS" || DatosSolicitud.Destino == "CASES")
                    {
                        pnlCases.Visible = true;
                    }
                    else
                    {
                        pnlCases.Visible = false;
                    }

                    if (usuario_logueado.NodoAtencionActual == "ZN")
                    {
                        fupExcel.Visible = false;
                        lbtnCargaExcel.Visible = false;
                    }
                    if (usuario_logueado.NodoAtencionActual == "CME")
                    {
                        fupExcel.Visible = true;
                        lbtnCargaExcel.Visible = true;
                    }
                }
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        void LlenaDatos(int? Indice, TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection Pasajeros)
        {
            try
            {
                //if (Indice != null) x
                //{x
                txtObservaciones.Text = Pasajeros.ColeccionPasajerosDia[Convert.ToInt32(Indice)].Observaciones;
                //txtInicioServicio.Text = Pasajeros.ColeccionPasajerosDia[Convert.ToInt32(Indice)].InicioServicio.ToShortDateString();
                //txtFinServicio.Text = Pasajeros.ColeccionPasajerosDia[Convert.ToInt32(Indice)].FinServicio.ToShortDateString();
                txtInicioHora.Text = Pasajeros.ColeccionPasajerosDia[Convert.ToInt32(Indice)].InicioServicio.ToString("HH:mm");
                txtFinHora.Text = Pasajeros.ColeccionPasajerosDia[Convert.ToInt32(Indice)].FinServicio.ToString("HH:mm");
                //pcldInicioServicio.SelectedDate = Pasajeros.ColeccionPasajerosDia[Convert.ToInt32(Indice)].InicioServicio.ToShortDateString();
                //pcldFinServicio.SelectedDate = Pasajeros.ColeccionPasajerosDia[Convert.ToInt32(Indice)].FinServicio.ToShortDateString();
                pcldInicioServicio.SetDateValue(Convert.ToDateTime(Pasajeros.ColeccionPasajerosDia[Convert.ToInt32(Indice)].InicioServicio));
                pcldFinServicio.SetDateValue(Convert.ToDateTime(Pasajeros.ColeccionPasajerosDia[Convert.ToInt32(Indice)].FinServicio));

                //Pasajeros.ColeccionPasajerosDia[Convert.ToInt32(Indice)].InicioServicio.ToString("HH:mm");
                //}x
                //elsex
                //{x
                //    txtObservaciones.Text = "";x
                //    txtInicioServicio.Text = "";x
                //    txtFinServicio.Text = "";x
                //    txtInicioHora.Text = "";x
                //    txtFinHora.Text = "";x
                //}x

            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        void ctlGridPasajerosViaje_EliminaElemento(string Ficha, bool borrar_dias_subsecuentes)
        {
            try
            {
                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection Pasajeros = Page.Session["PasajerosSesion"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection;

                if (borrar_dias_subsecuentes)
                {
                    for (int i = Convert.ToInt32(txtDia.Text) - 1; i < sldextDias.Maximum; i++)
                    {
                        Pasajeros.EliminarPasajero(Convert.ToInt32(i) + 1, Ficha);
                    }
                }
                else
                {
                    Pasajeros.EliminarPasajero(Convert.ToInt32(txtDia.Text), Ficha);
                }
                Page.Session["PasajerosSesion"] = Pasajeros;
                int? Indice = Pasajeros.ObtenerIndicexDia(Convert.ToInt32(txtDia.Text));
                if (Indice != null)
                    ctlGridPasajerosViaje.LlenarGrid(Pasajeros.ColeccionPasajerosDia[Convert.ToInt32(Indice)].Pasajeros);
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        void ctlGridPasajerosRetorno_EliminaElemento(string Ficha, bool borrar_dias_subsecuentes)
        {
            try
            {
                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection PasajerosRetorno = Page.Session["PasajerosRetornoSesion"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection;

                if (borrar_dias_subsecuentes)
                {
                    for (int i = Convert.ToInt32(txtDia.Text) - 1; i < sldextDias.Maximum; i++)
                    {
                        PasajerosRetorno.EliminarPasajero(Convert.ToInt32(i) + 1, Ficha);
                    }
                }
                else
                {
                    PasajerosRetorno.EliminarPasajero(Convert.ToInt32(txtDia.Text), Ficha);
                }

                Page.Session["PasajerosRetornoSesion"] = PasajerosRetorno;
                int? Indice = PasajerosRetorno.ObtenerIndicexDia(Convert.ToInt32(txtDia.Text));
                if (Indice != null)
                    ctlGridPasajerosRetorno.LlenarGrid(PasajerosRetorno.ColeccionPasajerosDia[Convert.ToInt32(Indice)].Pasajeros);
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        void ctlGridPasajerosViaje_Mensaje(string Mensaje, string Tipo)
        {
            try
            {
                this.Mensaje(Mensaje, Tipo);
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        void ctlGridPasajerosRetorno_Mensaje(string Mensaje, string Tipo)
        {
            try
            {
                this.Mensaje(Mensaje, Tipo);
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        void ctlGridPasajerosViaje_EditaElemento(string Ficha, string Nombre)
        {
            PnlAnexos.Visible = false;
            try
            {
                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.DatosVigenciaUtilerias DatosSolicitud = Page.Session["DatosSolicitud"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.DatosVigenciaUtilerias;
                //bool Test = TransporteTerrestre.Negocios.Terrestre.Utilerias.ValidaAnexo("07:00", "19:00", "15:00", Convert.ToDateTime("06/09/2010"));
                if (!TransporteTerrestre.Negocios.Terrestre.Utilerias.ValidaAnexo(DatosSolicitud.HoraInicioCandado, DatosSolicitud.HoraFinCandado, DatosSolicitud.HoraServicio, Convert.ToDateTime(txtInicioServicio.Text)))
                {
                    throw new Exception("El horario para crear anexos ha terminado");
                }
                PnlAnexos.Visible = true;
                IBtnAnexos.CommandArgument = Ficha;
                IBtnAnexos.CommandName = "VIAJE";
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        void ctlGridPasajerosRetorno_EditaElemento(string Ficha, string Nombre)
        {
            PnlAnexos.Visible = false;
            try
            {
                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.DatosVigenciaUtilerias DatosSolicitud = Page.Session["DatosSolicitud"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.DatosVigenciaUtilerias;
                //bool Test = TransporteTerrestre.Negocios.Terrestre.Utilerias.ValidaAnexo("07:00", "19:00", "15:00", Convert.ToDateTime("06/09/2010"));
                if (!TransporteTerrestre.Negocios.Terrestre.Utilerias.ValidaAnexo(DatosSolicitud.HoraInicioCandado, DatosSolicitud.HoraFinCandado, DatosSolicitud.HoraServicio, Convert.ToDateTime(txtInicioServicio.Text)))
                {
                    throw new Exception("El horario para crear anexos ha terminado");
                }
                PnlAnexos.Visible = true;
                IBtnAnexos.CommandArgument = Ficha;
                IBtnAnexos.CommandName = "RETORNO";
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        public void SetSlider(int Max, int Min)
        {
            try
            {
                sldextDias.Maximum = Max;
                sldextDias.Minimum = Min;
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }

        }

        protected void txtSlider_TextChanged(object sender, EventArgs e)
        {
            RefrescaGrids();
        }

        /*protected void lbtnAgregarRetorno_Click(object sender, EventArgs e)
        {
            try
            {
                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection PasajerosRetorno = Page.Session["PasajerosRetornoSesion"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection;
                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo PasajeroMin = new TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo();
                PasajeroMin.Ficha = txtFicha.Text;
                PasajeroMin.Nombre = lblNombre.Text;
                PasajeroMin.Tipo = lblTipo.Text;
                //revisar
                PasajerosRetorno.Agregar(Convert.ToInt32(txtDia.Text), System.DateTime.Today, PasajeroMin, Convert.ToDateTime(txtInicioServicio.Text), Convert.ToDateTime(txtFinServicio.Text), txtObservaciones.Text);
                Page.Session["PasajerosRetornoSesion"] = PasajerosRetorno;
                int? Indice = PasajerosRetorno.ObtenerIndicexDia(Convert.ToInt32(txtDia.Text));
                if (Indice != null)
                    ctlGridPasajerosRetorno.LlenarGrid(PasajerosRetorno.ColeccionPasajerosDia[Convert.ToInt32(Indice)].Pasajeros);        
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        
        }*/

        protected void RefrescaGrids()
        {
            try
            {
                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection Pasajeros = Page.Session["PasajerosSesion"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection;
                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection PasajerosRetorno = Page.Session["PasajerosRetornoSesion"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection;

                int? Indice = Pasajeros.ObtenerIndicexDia(Convert.ToInt32(txtDia.Text));
                int? IndiceRetorno = PasajerosRetorno.ObtenerIndicexDia(Convert.ToInt32(txtDia.Text));
                if (Indice != null)
                {
                    ctlGridPasajerosViaje.LlenarGrid(Pasajeros.ColeccionPasajerosDia[Convert.ToInt32(Indice)].Pasajeros);
                    LlenaDatos(Convert.ToInt32(Indice), Pasajeros);
                }
                else
                {
                    ctlGridPasajerosViaje.LlenarGrid(new System.Collections.Generic.List<TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo>());
                    LlenaDatos(Indice, Pasajeros);
                }
                if (IndiceRetorno != null)
                {
                    ctlGridPasajerosRetorno.LlenarGrid(PasajerosRetorno.ColeccionPasajerosDia[Convert.ToInt32(IndiceRetorno)].Pasajeros);
                    LlenaDatos(Convert.ToInt32(Indice), Pasajeros);
                }
                else
                {
                    ctlGridPasajerosRetorno.LlenarGrid(new System.Collections.Generic.List<TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo>());
                    LlenaDatos(Indice, Pasajeros);
                }
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        protected void lbtnTodosDias_Click(object sender, EventArgs e)
        {
            try
            {
                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection Pasajeros = Page.Session["PasajerosSesion"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection;
                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection PasajerosRetorno = Page.Session["PasajerosRetornoSesion"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection;
                PasajerosRetorno.Clonar(Pasajeros);
                Page.Session["PasajerosRetornoSesion"] = PasajerosRetorno;
                RefrescaGrids();
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        protected void txtDia_TextChanged(object sender, EventArgs e)
        {
            RefrescaGrids();
        }

        protected void lbtnFicha_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                TransporteTerrestre.Datos.TerrestrePasajeros Pasajero = TransporteTerrestre.Negocios.Terrestre.Pasajero.ValidaPasajero(txtFicha.Text);
                txtNombre.Text = Pasajero.Nombre;
                txtTipo.Text = Pasajero.Tipo;
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        protected void lbtnHeredarViaje_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection Pasajeros = Page.Session["PasajerosSesion"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection;
                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection PasajerosRetorno = Page.Session["PasajerosRetornoSesion"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection;

                System.DateTime FechaCompletaInicio = Convert.ToDateTime(txtInicioServicio.Text);
                System.DateTime Hora = Convert.ToDateTime(txtInicioHora.Text);
                FechaCompletaInicio = FechaCompletaInicio.AddHours(Hora.Hour).AddMinutes(Hora.Minute);

                System.DateTime FechaCompletaFin = Convert.ToDateTime(txtFinServicio.Text);
                System.DateTime HoraFin = Convert.ToDateTime(txtFinHora.Text);
                FechaCompletaFin = FechaCompletaFin.AddHours(HoraFin.Hour).AddMinutes(HoraFin.Minute);

                PasajerosRetorno.ClonarxIndice(Pasajeros, Convert.ToInt32(txtDia.Text), FechaCompletaInicio, FechaCompletaFin, txtObservaciones.Text);
                Page.Session["PasajerosRetornoSesion"] = PasajerosRetorno;
                RefrescaGrids();
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        protected void lbtnAgregar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection Pasajeros = Page.Session["PasajerosSesion"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection;
                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo PasajeroMin = new TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo();
                TransporteTerrestre.Datos.TerrestrePasajeros Pasajero = TransporteTerrestre.Negocios.Terrestre.Pasajero.ValidaPasajero(txtFicha.Text);
                PasajeroMin.Ficha = Pasajero.Ficha;
                PasajeroMin.Nombre = Pasajero.Nombre;
                PasajeroMin.Tipo = Pasajero.Tipo;
                bool ExistePasajero = false;
                if (txtInicioServicio.Text == "" || txtFinServicio.Text == "")
                {
                    throw new Exception("Favor de capturar las fechas de inicio y fin del servicio");
                }
                if (ctlGridPasajerosViaje.VerificaPasajero(PasajeroMin.Ficha))
                {
                    throw new Exception("El pasajero ya fue capturado para este dia");
                }
                System.DateTime FechaCompletaInicio = Convert.ToDateTime(txtInicioServicio.Text);
                System.DateTime Hora = Convert.ToDateTime(txtInicioHora.Text);
                FechaCompletaInicio = FechaCompletaInicio.AddHours(Hora.Hour).AddMinutes(Hora.Minute);

                System.DateTime FechaCompletaFin = Convert.ToDateTime(txtFinServicio.Text);
                System.DateTime HoraFin = Convert.ToDateTime(txtFinHora.Text);
                FechaCompletaFin = FechaCompletaFin.AddHours(HoraFin.Hour).AddMinutes(HoraFin.Minute);

                //revisar

                if (chkDiasSubsecuentes.Checked)
                {
                    int contador_dias = 0;
                    for (int i = Convert.ToInt32(txtDia.Text) - 1; i < sldextDias.Maximum; i++)
                    {
                        List<TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo> pasajero_minimo_busqueda = (from x in Pasajeros.ColeccionPasajerosDia[i].Pasajeros where x.Ficha == PasajeroMin.Ficha select x).ToList<TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo>();

                        if (pasajero_minimo_busqueda != null && pasajero_minimo_busqueda.Count > 0)
                        {
                            ExistePasajero = true;
                        }
                        else
                        {
                            ExistePasajero = false;
                        }

                        if (!ExistePasajero)
                        {
                            Pasajeros.Agregar(i + 1, Convert.ToDateTime(txtInicioServicio.Text).AddDays(contador_dias), PasajeroMin, FechaCompletaInicio.AddDays(contador_dias), FechaCompletaFin.AddDays(contador_dias), txtObservaciones.Text);
                        }
                        contador_dias++;
                    }
                }
                else
                {
                    Pasajeros.Agregar(Convert.ToInt32(txtDia.Text), Convert.ToDateTime(txtInicioServicio.Text), PasajeroMin, FechaCompletaInicio, FechaCompletaFin, txtObservaciones.Text);
                }
                
                Page.Session["PasajerosSesion"] = Pasajeros;
                int? Indice = Pasajeros.ObtenerIndicexDia(Convert.ToInt32(txtDia.Text));
                if (Indice != null)
                    ctlGridPasajerosViaje.LlenarGrid(Pasajeros.ColeccionPasajerosDia[Convert.ToInt32(Indice)].Pasajeros);
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        protected void lbtnAgregarRetorno_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection PasajerosRetorno = Page.Session["PasajerosRetornoSesion"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection;
                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo PasajeroMin = new TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo();
                TransporteTerrestre.Datos.TerrestrePasajeros Pasajero = TransporteTerrestre.Negocios.Terrestre.Pasajero.ValidaPasajero(txtFicha.Text);
                PasajeroMin.Ficha = Pasajero.Ficha;
                PasajeroMin.Nombre = Pasajero.Nombre;
                PasajeroMin.Tipo = Pasajero.Tipo;
                bool ExistePasajero = false;

                if (txtInicioServicio.Text == "" || txtFinServicio.Text == "")
                {
                    throw new Exception("Favor de capturar las fechas de inicio y fin del servicio");
                }
                if (ctlGridPasajerosRetorno.VerificaPasajero(PasajeroMin.Ficha))
                {
                    throw new Exception("El pasajero ya fue capturado para este dia");
                }

                System.DateTime FechaCompletaInicio = Convert.ToDateTime(txtInicioServicio.Text);
                System.DateTime Hora = Convert.ToDateTime(txtInicioHora.Text);
                FechaCompletaInicio = FechaCompletaInicio.AddHours(Hora.Hour).AddMinutes(Hora.Minute);

                System.DateTime FechaCompletaFin = Convert.ToDateTime(txtFinServicio.Text);
                System.DateTime HoraFin = Convert.ToDateTime(txtFinHora.Text);
                FechaCompletaFin = FechaCompletaFin.AddHours(HoraFin.Hour).AddMinutes(HoraFin.Minute);

                //revisar

                if (chkDiasSubsecuentes.Checked)
                {
                    int contador_dias = 0;

                    for (int i = Convert.ToInt32(txtDia.Text) - 1; i < sldextDias.Maximum; i++)
                    {
                        List<TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo> pasajero_minimo_busqueda = (from x in PasajerosRetorno.ColeccionPasajerosDia[i].Pasajeros where x.Ficha == PasajeroMin.Ficha select x).ToList<TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo>();

                        if (pasajero_minimo_busqueda != null && pasajero_minimo_busqueda.Count > 0)
                        {
                            ExistePasajero = true;
                        }
                        else
                        {
                            ExistePasajero = false;
                        }

                        if (!ExistePasajero)
                        {
                            PasajerosRetorno.Agregar(i + 1, Convert.ToDateTime(txtInicioServicio.Text).AddDays(contador_dias), PasajeroMin, FechaCompletaInicio.AddDays(contador_dias), FechaCompletaFin.AddDays(contador_dias), txtObservaciones.Text);
                        }
                    }
                }
                else
                {
                    PasajerosRetorno.Agregar(Convert.ToInt32(txtDia.Text), Convert.ToDateTime(txtInicioServicio.Text), PasajeroMin, FechaCompletaInicio, FechaCompletaFin, txtObservaciones.Text);
                }

                //PasajerosRetorno.Agregar(Convert.ToInt32(txtDia.Text), Convert.ToDateTime(txtInicioServicio.Text), PasajeroMin, FechaCompletaInicio, FechaCompletaFin, txtObservaciones.Text);
                Page.Session["PasajerosRetornoSesion"] = PasajerosRetorno;
                int? Indice = PasajerosRetorno.ObtenerIndicexDia(Convert.ToInt32(txtDia.Text));
                if (Indice != null)
                    ctlGridPasajerosRetorno.LlenarGrid(PasajerosRetorno.ColeccionPasajerosDia[Convert.ToInt32(Indice)].Pasajeros);
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        protected void lbtnTodosDias_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection Pasajeros = Page.Session["PasajerosSesion"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection;
                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection PasajerosRetorno = Page.Session["PasajerosRetornoSesion"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection;
                PasajerosRetorno.Clonar(Pasajeros);
                Page.Session["PasajerosRetornoSesion"] = PasajerosRetorno;
                RefrescaGrids();
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        protected void ibtnCargaPasajeros_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection PasajerosRetorno = Page.Session["PasajerosRetornoSesion"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection;

                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection Pasajeros = Page.Session["PasajerosSesion"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection;

                //TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo PasajeroMin = null;
                //PasajeroMin.Ficha = txtFicha.Text;
                //PasajeroMin.Nombre = txtNombre.Text;
                //PasajeroMin.Tipo = txtTipo.Text;


                System.DateTime FechaCompletaInicio = Convert.ToDateTime(txtInicioServicio.Text);
                System.DateTime Hora = Convert.ToDateTime(txtInicioHora.Text);
                FechaCompletaInicio = FechaCompletaInicio.AddHours(Hora.Hour).AddMinutes(Hora.Minute);

                System.DateTime FechaCompletaFin = Convert.ToDateTime(txtFinServicio.Text);
                System.DateTime HoraFin = Convert.ToDateTime(txtFinHora.Text);
                FechaCompletaFin = FechaCompletaFin.AddHours(HoraFin.Hour).AddMinutes(HoraFin.Minute);

                PasajerosRetorno.ColeccionPasajerosDia[Convert.ToInt32(txtDia.Text) - 1].FechaDia = Convert.ToDateTime(txtInicioServicio.Text);
                PasajerosRetorno.ColeccionPasajerosDia[Convert.ToInt32(txtDia.Text) - 1].InicioServicio = FechaCompletaInicio;
                PasajerosRetorno.ColeccionPasajerosDia[Convert.ToInt32(txtDia.Text) - 1].FinServicio = FechaCompletaFin;
                PasajerosRetorno.ColeccionPasajerosDia[Convert.ToInt32(txtDia.Text) - 1].Observaciones = txtObservaciones.Text;

                Pasajeros.ColeccionPasajerosDia[Convert.ToInt32(txtDia.Text) - 1].FechaDia = Convert.ToDateTime(txtInicioServicio.Text);
                Pasajeros.ColeccionPasajerosDia[Convert.ToInt32(txtDia.Text) - 1].InicioServicio = FechaCompletaInicio;
                Pasajeros.ColeccionPasajerosDia[Convert.ToInt32(txtDia.Text) - 1].FinServicio = FechaCompletaFin;
                Pasajeros.ColeccionPasajerosDia[Convert.ToInt32(txtDia.Text) - 1].Observaciones = txtObservaciones.Text;

                //revisar
                //PasajerosRetorno.Agregar(Convert.ToInt32(txtDia.Text), Convert.ToDateTime(txtInicioServicio.Text), PasajeroMin, FechaCompletaInicio, FechaCompletaFin, txtObservaciones.Text);
                Page.Session["PasajerosRetornoSesion"] = PasajerosRetorno;
                //Pasajeros.Agregar(Convert.ToInt32(txtDia.Text), Convert.ToDateTime(txtInicioServicio.Text), PasajeroMin, FechaCompletaInicio, FechaCompletaFin, txtObservaciones.Text);
                Page.Session["PasajerosSesion"] = Pasajeros;
                //int? Indice = PasajerosRetorno.ObtenerIndicexDia(Convert.ToInt32(txtDia.Text));
                //if (Indice != null)
                //    ctlGridPasajerosRetorno.LlenarGrid(PasajerosRetorno.ColeccionPasajerosDia[Convert.ToInt32(Indice)].Pasajeros);
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        /*protected void lbtnSolicitudPrevia_Click(object sender, ImageClickEventArgs e)
        {
        
        }*/

        protected void lbtnSolicitudPrevia_Click1(object sender, ImageClickEventArgs e)
        {

        }

        protected void lbtnSolicitudPrevia_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                LimpiaGrids();

                //TransporteTerrestre.Negocios.Usuarios Usuario = Session["usuario_logueado"] as TransporteTerrestre.Negocios.Usuarios;
                Models.UserLogon usuario_logueado = Infraestructure.Utilerias.ObtenerUsuarioAutentificado(Page.Session);

                TransporteTerrestre.Datos.VWTerrestreSolicitudDiaPasajerosCollection TodosPasajeros = new SubSonic.Select()
                    .From<TransporteTerrestre.Datos.VWTerrestreSolicitudDiaPasajeros>()
                    .Where(TransporteTerrestre.Datos.VWTerrestreSolicitudDiaPasajeros.Columns.Folio)
                    .IsEqualTo(txtSolicitudPrevia.Text)
                    .And(TransporteTerrestre.Datos.VWTerrestreSolicitudDiaPasajeros.Columns.Status)
                    .IsNotEqualTo("ELIMINADO")
                    .And(TransporteTerrestre.Datos.VWTerrestreSolicitudDiaPasajeros.Columns.Nodo)
                    .IsEqualTo(usuario_logueado.NodoAtencionActual)
                    .ExecuteAsCollection<TransporteTerrestre.Datos.VWTerrestreSolicitudDiaPasajerosCollection>();

                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection Pasajeros = Page.Session["PasajerosSesion"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection;

                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection PasajerosRetorno = Page.Session["PasajerosRetornoSesion"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection;

                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo PasajeroMin = null;

                for (int d = 0; d < sldextDias.Maximum; d++)
                {

                    for (int i = 0; i < TodosPasajeros.Count; i++)
                    {
                        if (TodosPasajeros[i].Movimiento == "VIAJE" && TodosPasajeros[i].CveDia == d + 1)
                        {
                            PasajeroMin = new TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo();
                            PasajeroMin.Ficha = TodosPasajeros[i].Ficha;
                            PasajeroMin.Nombre = TodosPasajeros[i].Nombre;
                            PasajeroMin.Tipo = TodosPasajeros[i].Tipo;
                            Pasajeros.Agregar(TodosPasajeros[i].CveDia, TodosPasajeros[i].Fecha, PasajeroMin, TodosPasajeros[i].InicioServicio, TodosPasajeros[i].TerminoServicio, TodosPasajeros[i].Observaciones);
                        }
                        else if (TodosPasajeros[i].Movimiento == "RETORNO" && TodosPasajeros[i].CveDia == d + 1)
                        {
                            PasajeroMin = new TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo();
                            PasajeroMin.Ficha = TodosPasajeros[i].Ficha;
                            PasajeroMin.Nombre = TodosPasajeros[i].Nombre;
                            PasajeroMin.Tipo = TodosPasajeros[i].Tipo;
                            PasajerosRetorno.Agregar(TodosPasajeros[i].CveDia, TodosPasajeros[i].Fecha, PasajeroMin, TodosPasajeros[i].InicioServicio, TodosPasajeros[i].TerminoServicio, TodosPasajeros[i].Observaciones);
                        }
                    }
                }
                int? Indice = Pasajeros.ObtenerIndicexDia(Convert.ToInt32(sldextDias.Minimum));
                int? IndiceRetorno = PasajerosRetorno.ObtenerIndicexDia(Convert.ToInt32(sldextDias.Minimum));
                if (Indice != null)
                {
                    ctlGridPasajerosViaje.LlenarGrid(Pasajeros.ColeccionPasajerosDia[Convert.ToInt32(Indice)].Pasajeros);
                    //LlenaDatos(Indice, Pasajeros);
                }
                else
                {
                    ctlGridPasajerosViaje.LlenarGrid(new System.Collections.Generic.List<TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo>());
                }
                if (IndiceRetorno != null)
                {
                    ctlGridPasajerosRetorno.LlenarGrid(PasajerosRetorno.ColeccionPasajerosDia[Convert.ToInt32(IndiceRetorno)].Pasajeros);
                    //LlenaDatos(Indice, Pasajeros);
                }
                else
                {
                    ctlGridPasajerosRetorno.LlenarGrid(new System.Collections.Generic.List<TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo>());
                }
                Page.Session["PasajerosSesion"] = Pasajeros;
                Page.Session["PasajerosRetornoSesion"] = PasajerosRetorno;

            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        protected void lbtnCargaExcel_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //System.Web.UI.WebControls.FileUpload fupExcel = ctlPasajeros.getFileUpload();
                bool yes = fupExcel.HasFile;
                //Conertir el archivo enviado a un dataset.
                try
                {
                    TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection Pasajeros = Page.Session["PasajerosSesion"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection;
                    TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection PasajerosRetorno = Page.Session["PasajerosRetornoSesion"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection;
                    System.Collections.Generic.Dictionary<string, string> FichasNoEncontradas = new System.Collections.Generic.Dictionary<string, string>();
                    //LimpiaGrids();
                    if (fupExcel.PostedFile.ContentType == "application/vnd.ms-excel" ||
                    fupExcel.PostedFile.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                    {
                        bool excel = (fupExcel.PostedFile.ContentType == "application/vnd.ms-excel" ? true : (fupExcel.PostedFile.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" ? true : false));
                        string mime_type = fupExcel.PostedFile.ContentType;

                        System.Data.DataSet fupExcel_dataset = TransporteTerrestre.Negocios.Terrestre.Utilerias.CargarPasajeros(fupExcel.FileContent, excel, mime_type, "PMX");

                        //Tomar el contenido de la table 0 del dataset y crear List<PasajeroMinimo>
                        DataTable tblViaje = null;
                        DataTable tblRetorno = null;
                        DataTable TblAmbos = null;

                        if (fupExcel_dataset.Tables.Count > 0)
                        {
                            tblViaje = fupExcel_dataset.Tables[0];
                        }
                        else
                        {
                            tblViaje = new DataTable();
                        }

                        if (fupExcel_dataset.Tables.Count > 1)
                        {
                            tblRetorno = fupExcel_dataset.Tables[1];
                        }
                        else
                        {
                            tblRetorno = new DataTable();
                        }

                        if (fupExcel_dataset.Tables.Count > 2)
                        {
                            TblAmbos = fupExcel_dataset.Tables[2];
                        }
                        else
                        {
                            TblAmbos = new DataTable();
                        }


                        System.Collections.Generic.List<TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo> lista = new System.Collections.Generic.List<TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo>();
                        TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo persona = null;

                        for (int i = 0; i < Pasajeros.Count(); i++)
                        {
                            foreach (DataRow fila in tblViaje.Rows)
                            {
                                //if (Pasajeros.ColeccionPasajerosDia[i].CveDia == Convert.ToInt32(fila["DIA"]))
                                //{
                                //    persona = new TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo();
                                //    persona.Ficha = fila[0].ToString();
                                //    persona.Nombre = fila[1].ToString();
                                //    persona.Tipo = fila[2].ToString();
                                //    Pasajeros.AgregarPasajeroPorDia(Pasajeros.ColeccionPasajerosDia[i].CveDia, persona);
                                //}
                                //for (int j = 0; j < Pasajeros.ColeccionPasajerosDia.Count; j++)
                                //{
                                TransporteTerrestre.Datos.TerrestrePasajeros Pasajero = TransporteTerrestre.Negocios.Terrestre.Pasajero.ValidaPasajeroExcel(fila[0].ToString());
                                persona = new TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo();
                                if (Pasajero != null)
                                {
                                    //persona.Ficha = fila[0].ToString();
                                    //persona.Nombre = fila[1].ToString();
                                    //persona.Tipo = fila[2].ToString();
                                    //Pasajeros.AgregarPasajeroPorDia(Pasajeros.ColeccionPasajerosDia[i].CveDia, persona);
                                    persona.Ficha = Pasajero.Ficha;
                                    persona.Nombre = Pasajero.Nombre;
                                    persona.Tipo = Pasajero.Tipo;
                                    //persona.SitCont = fila[4].ToString().ToUpper();
                                    //persona.Nivel = fila[5].ToString().ToUpper();
                                    //persona.Categoria = fila[6].ToString().ToUpper();
                                    //persona.Gerencia = fila[7].ToString().ToUpper();
                                    //persona.Instalacion = fila[8].ToString().ToUpper();
                                    //persona.Rol = fila[9].ToString().ToUpper();
                                    //persona.Plaza = fila[10].ToString().ToUpper();
                                    //persona.Subgerencia = fila[11].ToString().ToUpper();
                                    //persona.Clave = fila[12].ToString().ToUpper();
                                    //persona.Curso = fila[13].ToString().ToUpper();
                                    Pasajeros.AgregarPasajeroPorDia(Pasajeros.ColeccionPasajerosDia[i].CveDia, persona);
                                    //}
                                }
                                else
                                {
                                    if (!FichasNoEncontradas.ContainsValue(fila[0].ToString()))
                                    {
                                        FichasNoEncontradas.Add(FichasNoEncontradas.Keys.Count.ToString(), fila[0].ToString());
                                    }
                                }
                            }
                            foreach (DataRow fila in TblAmbos.Rows)
                            {
                                //if (PasajerosRetorno.ColeccionPasajerosDia[i].CveDia == Convert.ToInt32(fila["DIA"]))
                                //{
                                //    persona = new TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo();
                                //    persona.Ficha = fila[0].ToString();
                                //    persona.Nombre = fila[1].ToString();
                                //    persona.Tipo = fila[2].ToString();
                                //    PasajerosRetorno.AgregarPasajeroPorDia(Pasajeros.ColeccionPasajerosDia[i].CveDia, persona);
                                //}

                                TransporteTerrestre.Datos.TerrestrePasajeros Pasajero = TransporteTerrestre.Negocios.Terrestre.Pasajero.ValidaPasajeroExcel(fila[0].ToString());
                                persona = new TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo();
                                if (Pasajero != null)
                                {
                                    //persona.Ficha = fila[0].ToString();
                                    //persona.Nombre = fila[1].ToString();
                                    //persona.Tipo = fila[2].ToString();
                                    persona.Ficha = Pasajero.Ficha;
                                    persona.Nombre = Pasajero.Nombre;
                                    persona.Tipo = Pasajero.Tipo;
                                    //persona.SitCont = fila[4].ToString().ToUpper();
                                    //persona.Nivel = fila[5].ToString().ToUpper();
                                    //persona.Categoria = fila[6].ToString().ToUpper();
                                    //persona.Gerencia = fila[7].ToString().ToUpper();
                                    //persona.Instalacion = fila[8].ToString().ToUpper();
                                    //persona.Rol = fila[9].ToString().ToUpper();
                                    //persona.Plaza = fila[10].ToString().ToUpper();
                                    //persona.Subgerencia = fila[11].ToString().ToUpper();
                                    //persona.Clave = fila[12].ToString().ToUpper();
                                    //persona.Curso = fila[13].ToString().ToUpper();
                                    //PasajerosRetorno.AgregarPasajeroPorDia(Pasajeros.ColeccionPasajerosDia[i].CveDia, persona);
                                    Pasajeros.AgregarPasajeroPorDia(Pasajeros.ColeccionPasajerosDia[i].CveDia, persona);
                                }
                                else
                                {
                                    if (!FichasNoEncontradas.ContainsValue(fila[0].ToString()))
                                    {
                                        FichasNoEncontradas.Add(FichasNoEncontradas.Keys.Count.ToString(), fila[0].ToString());
                                    }
                                }
                            }
                        }

                        for (int i = 0; i < PasajerosRetorno.Count(); i++)
                        {
                            foreach (DataRow fila in tblRetorno.Rows)
                            {
                                //if (PasajerosRetorno.ColeccionPasajerosDia[i].CveDia == Convert.ToInt32(fila["DIA"]))
                                //{
                                //    persona = new TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo();
                                //    persona.Ficha = fila[0].ToString();
                                //    persona.Nombre = fila[1].ToString();
                                //    persona.Tipo = fila[2].ToString();
                                //    PasajerosRetorno.AgregarPasajeroPorDia(Pasajeros.ColeccionPasajerosDia[i].CveDia, persona);
                                //}

                                TransporteTerrestre.Datos.TerrestrePasajeros Pasajero = TransporteTerrestre.Negocios.Terrestre.Pasajero.ValidaPasajeroExcel(fila[0].ToString());
                                persona = new TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo();
                                if (Pasajero != null)
                                {
                                    //persona.Ficha = fila[0].ToString();
                                    //persona.Nombre = fila[1].ToString();
                                    //persona.Tipo = fila[2].ToString();
                                    persona.Ficha = Pasajero.Ficha;
                                    persona.Nombre = Pasajero.Nombre;
                                    persona.Tipo = Pasajero.Tipo;
                                    //persona.SitCont = fila[4].ToString().ToUpper();
                                    //persona.Nivel = fila[5].ToString().ToUpper();
                                    //persona.Categoria = fila[6].ToString().ToUpper();
                                    //persona.Gerencia = fila[7].ToString().ToUpper();
                                    //persona.Instalacion = fila[8].ToString().ToUpper();
                                    //persona.Rol = fila[9].ToString().ToUpper();
                                    //persona.Plaza = fila[10].ToString().ToUpper();
                                    //persona.Subgerencia = fila[11].ToString().ToUpper();
                                    //persona.Clave = fila[12].ToString().ToUpper();
                                    //persona.Curso = fila[13].ToString().ToUpper();
                                    PasajerosRetorno.AgregarPasajeroPorDia(Pasajeros.ColeccionPasajerosDia[i].CveDia, persona);
                                }
                                else
                                {
                                    if (!FichasNoEncontradas.ContainsValue(fila[0].ToString()))
                                    {
                                        FichasNoEncontradas.Add(FichasNoEncontradas.Keys.Count.ToString(), fila[0].ToString());
                                    }
                                }
                            }
                            foreach (DataRow fila in TblAmbos.Rows)
                            {
                                //if (PasajerosRetorno.ColeccionPasajerosDia[i].CveDia == Convert.ToInt32(fila["DIA"]))
                                //{
                                //    persona = new TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo();
                                //    persona.Ficha = fila[0].ToString();
                                //    persona.Nombre = fila[1].ToString();
                                //    persona.Tipo = fila[2].ToString();
                                //    PasajerosRetorno.AgregarPasajeroPorDia(Pasajeros.ColeccionPasajerosDia[i].CveDia, persona);
                                //}

                                TransporteTerrestre.Datos.TerrestrePasajeros Pasajero = TransporteTerrestre.Negocios.Terrestre.Pasajero.ValidaPasajeroExcel(fila[0].ToString());
                                persona = new TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo();
                                if (Pasajero != null)
                                {
                                    //persona.Ficha = fila[0].ToString();
                                    //persona.Nombre = fila[1].ToString();
                                    //persona.Tipo = fila[2].ToString();
                                    persona.Ficha = Pasajero.Ficha;
                                    persona.Nombre = Pasajero.Nombre;
                                    persona.Tipo = Pasajero.Tipo;
                                    //persona.SitCont = fila[4].ToString().ToUpper();
                                    //persona.Nivel = fila[5].ToString().ToUpper();
                                    //persona.Categoria = fila[6].ToString().ToUpper();
                                    //persona.Gerencia = fila[7].ToString().ToUpper();
                                    //persona.Instalacion = fila[8].ToString().ToUpper();
                                    //persona.Rol = fila[9].ToString().ToUpper();
                                    //persona.Plaza = fila[10].ToString().ToUpper();
                                    //persona.Subgerencia = fila[11].ToString().ToUpper();
                                    //persona.Clave = fila[12].ToString().ToUpper();
                                    //persona.Curso = fila[13].ToString().ToUpper();
                                    PasajerosRetorno.AgregarPasajeroPorDia(Pasajeros.ColeccionPasajerosDia[i].CveDia, persona);
                                    //Pasajeros.AgregarPasajeroPorDia(Pasajeros.ColeccionPasajerosDia[i].CveDia, persona);
                                }
                                else
                                {
                                    if (!FichasNoEncontradas.ContainsValue(fila[0].ToString()))
                                    {
                                        FichasNoEncontradas.Add(FichasNoEncontradas.Keys.Count.ToString(), fila[0].ToString());
                                    }
                                }
                            }
                        }
                        //foreach (DataRow fila in tblExcel.Rows)
                        //{
                        //    persona = new TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo();
                        //    persona.Ficha = fila[0].ToString();
                        //    persona.Nombre = fila[1].ToString();
                        //    persona.Tipo = fila[2].ToString();

                        //    lista.Add(persona);
                        //}
                        //ctlGridPasajerosViaje.LlenarGrid(lista);
                    }
                    Page.Session["PasajerosRetornoSesion"] = PasajerosRetorno;
                    Page.Session["Pasajeros"] = Pasajeros;
                    RefrescaGrids();

                    if (FichasNoEncontradas.Count > 0)
                    {
                        string Mensaje = "Los trabajadores con las siguientes fichas no se encuentran dados de alta en el sistema:";
                        for (int i = 0; i < FichasNoEncontradas.Values.Count; i++)
                        {
                            Mensaje = Mensaje + " " + FichasNoEncontradas[i.ToString()];
                        }
                        this.Mensaje(Mensaje, "ERROR");
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void IBtnFichaAnexo_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                TransporteTerrestre.Datos.TerrestrePasajeros Pasajero = TransporteTerrestre.Negocios.Terrestre.Pasajero.ValidaPasajero(TxtNuevaFicha.Text);
                TxtNuevoNombre.Text = Pasajero.Nombre;
                TxtNuevoTipo.Text = Pasajero.Tipo;
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        protected void IBtnAnexos_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

                //bool Test = TransporteTerrestre.Negocios.Terrestre.Utilerias.ValidaAnexo("07:00", "19:00", "15:00", Convert.ToDateTime("06/09/2010"));
                //if (!TransporteTerrestre.Negocios.Terrestre.Utilerias.ValidaAnexo(DatosSolicitud.HoraInicioCandado, DatosSolicitud.HoraFinCandado,DatosSolicitud.HoraServicio, Convert.ToDateTime(txtInicioServicio.Text)))
                //{
                //    throw new Exception("El horario para crear anexos ha terminado");
                //}

                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo PasajeroAnterior = null;

                if (IBtnAnexos.CommandName == "VIAJE")
                {
                    TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection Pasajeros = Page.Session["PasajerosSesion"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection;
                    TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo NuevoPasajero = null;
                    for (int i = 0; i < Pasajeros.ColeccionPasajerosDia[Convert.ToInt32(txtDia.Text) - 1].Pasajeros.Count; i++)
                    {
                        if (Pasajeros.ColeccionPasajerosDia[Convert.ToInt32(txtDia.Text) - 1].Pasajeros[i].Ficha == IBtnAnexos.CommandArgument.ToString())
                        {
                            PasajeroAnterior = ctlGridPasajerosViaje.RegresaPasajeroSeleccionado();
                            Pasajeros.EliminarPasajero(Convert.ToInt32(txtDia.Text), IBtnAnexos.CommandArgument.ToString());
                            NuevoPasajero = new TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo();
                            NuevoPasajero.Ficha = TransporteTerrestre.Negocios.Terrestre.Utilerias.FormateaFicha(TxtNuevaFicha.Text);
                            NuevoPasajero.Nombre = TxtNuevoNombre.Text;
                            NuevoPasajero.Tipo = TxtNuevoTipo.Text;
                            Pasajeros.AgregarPasajeroPorDia(Convert.ToInt32(txtDia.Text), NuevoPasajero);
                            Page.Session["PasajerosSesion"] = Pasajeros;
                            int? Indice = Pasajeros.ObtenerIndicexDia(Convert.ToInt32(txtDia.Text));
                            if (Indice != null)
                                ctlGridPasajerosViaje.LlenarGrid(Pasajeros.ColeccionPasajerosDia[Convert.ToInt32(Indice)].Pasajeros);

                            ctlGridPasajerosViaje.ResetIndexGrid();
                            break;
                        }
                    }
                }
                else if (IBtnAnexos.CommandName == "RETORNO")
                {
                    TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection PasajerosRetorno = Page.Session["PasajerosRetornoSesion"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection;
                    TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo NuevoPasajero = null;
                    for (int i = 0; i < PasajerosRetorno.ColeccionPasajerosDia[Convert.ToInt32(txtDia.Text) - 1].Pasajeros.Count; i++)
                    {
                        if (PasajerosRetorno.ColeccionPasajerosDia[Convert.ToInt32(txtDia.Text) - 1].Pasajeros[i].Ficha == IBtnAnexos.CommandArgument.ToString())
                        {
                            PasajeroAnterior = ctlGridPasajerosRetorno.RegresaPasajeroSeleccionado();
                            PasajerosRetorno.EliminarPasajero(Convert.ToInt32(txtDia.Text), IBtnAnexos.CommandArgument.ToString());
                            NuevoPasajero = new TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo();
                            NuevoPasajero.Ficha = TransporteTerrestre.Negocios.Terrestre.Utilerias.FormateaFicha(TxtNuevaFicha.Text);
                            NuevoPasajero.Nombre = TxtNuevoNombre.Text;
                            NuevoPasajero.Tipo = TxtNuevoTipo.Text;
                            PasajerosRetorno.AgregarPasajeroPorDia(Convert.ToInt32(txtDia.Text), NuevoPasajero);
                            Page.Session["PasajerosRetornoSesion"] = PasajerosRetorno;
                            int? Indice = PasajerosRetorno.ObtenerIndicexDia(Convert.ToInt32(txtDia.Text));
                            if (Indice != null)
                                ctlGridPasajerosRetorno.LlenarGrid(PasajerosRetorno.ColeccionPasajerosDia[Convert.ToInt32(Indice)].Pasajeros);

                            ctlGridPasajerosRetorno.ResetIndexGrid();
                            break;
                        }
                    }
                }
                TransporteTerrestre.Datos.TerrestreAnexosCollection Anexos = Page.Session["Anexos"] as TransporteTerrestre.Datos.TerrestreAnexosCollection;
                TransporteTerrestre.Datos.TerrestreAnexos Anexo = new TransporteTerrestre.Datos.TerrestreAnexos();
                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.DatosVigenciaUtilerias DatosSolicitud = Page.Session["DatosSolicitud"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.DatosVigenciaUtilerias;
                if (Anexos.Count <= 0)
                {
                    TransporteTerrestre.Datos.TerrestreAnexosCollection AnexosBase = new SubSonic.Select().From<TransporteTerrestre.Datos.TerrestreAnexos>().Where(TransporteTerrestre.Datos.TerrestreAnexos.FolioSolicitudColumn).IsEqualTo(DatosSolicitud.Folio).ExecuteAsCollection<TransporteTerrestre.Datos.TerrestreAnexosCollection>();
                    if (AnexosBase.Count <= 0)
                    {
                        Anexo.Anexo = 1;
                    }
                    else
                    {
                        AnexosBase.OrderByAsc("Anexo");
                        Anexo.Anexo = AnexosBase[AnexosBase.Count - 1].Anexo + 1;
                    }
                }
                else
                {
                    Anexos.OrderByAsc("Anexo");
                    Anexo.Anexo = Anexos[Anexos.Count - 1].Anexo + 1;
                }
                Anexo.FichaAnterior = PasajeroAnterior.Ficha;
                Anexo.NombreAnterior = PasajeroAnterior.Nombre;
                Anexo.NombreNuevo = TxtNuevoNombre.Text;
                Anexo.FichaNueva = TxtNuevaFicha.Text;
                Anexo.Observaciones = txtObservaciones.Text;
                Anexo.FechaCambio = Convert.ToDateTime(txtInicioServicio.Text);
                Anexo.FolioSolicitud = DatosSolicitud.Folio;
                Anexo.PkIdAnexo = DatosSolicitud.IdFolioSolicitud;
                Anexos.Add(Anexo);
                Page.Session["Anexos"] = Anexos;
                PnlAnexos.Visible = false;
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        public void Confirmada()
        {
            try
            {
                txtFicha.Enabled = false;
                txtFinHora.Enabled = false;
                txtInicioHora.Enabled = false;
                txtFinServicio.Enabled = false;
                txtInicioServicio.Enabled = false;
                txtNombre.Enabled = false;
                txtTipo.Enabled = false;
                lbtnFicha.Enabled = false;
                lbtnAgregarRetorno.Enabled = false;
                lbtnCargaExcel.Enabled = false;
                lbtnHeredarViaje.Enabled = false;
                lbtnSolicitudPrevia.Enabled = false;
                lbtnTodosDias.Enabled = false;
                lbtnAgregar.Enabled = false;
                ibtnCargaPasajeros.Enabled = false;
                txtObservaciones.Enabled = false;
                txtSolicitudPrevia.Enabled = false;
                fupExcel.Enabled = false;
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        protected void GvSolicitudesPrevias_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ModalPopupExtender1.Hide();
                txtSolicitudPrevia.Text = ((Label)GvSolicitudesPrevias.Rows[GvSolicitudesPrevias.SelectedIndex].FindControl("LblFolio")).Text;
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        protected void IbtnVentanaSolicitudPrevia_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ModalPopupExtender1.Show();
                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.DatosVigenciaUtilerias DatosSolicitud = Page.Session["DatosSolicitud"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.DatosVigenciaUtilerias;

                TransporteTerrestre.Datos.TerrestreSolicitudCollection SolicitudesPrevias = new SubSonic.Select()
                    .From<TransporteTerrestre.Datos.TerrestreSolicitud>()
                    .Where(TransporteTerrestre.Datos.TerrestreSolicitud.Columns.FkIdAutorizador)
                    .IsEqualTo(DatosSolicitud.IdAutorizador)
                    .ExecuteAsCollection<TransporteTerrestre.Datos.TerrestreSolicitudCollection>();
                GvSolicitudesPrevias.DataSource = SolicitudesPrevias;
                GvSolicitudesPrevias.DataBind();
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        protected void LimpiaGrids()
        {
            try
            {
                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection Pasajeros = Page.Session["PasajerosSesion"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection;
                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection PasajerosRetorno = Page.Session["PasajerosRetornoSesion"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection;

                for (int j = 0; j < Pasajeros.ColeccionPasajerosDia.Count; j++)
                {
                    Pasajeros.ColeccionPasajerosDia[j].Pasajeros.Clear();
                }
                for (int j = 0; j < PasajerosRetorno.ColeccionPasajerosDia.Count; j++)
                {
                    PasajerosRetorno.ColeccionPasajerosDia[j].Pasajeros.Clear();
                }
                int? Indice = PasajerosRetorno.ObtenerIndicexDia(Convert.ToInt32(txtDia.Text));
                Page.Session["PasajerosSesion"] = Pasajeros;
                Page.Session["PasajerosRetornoSesion"] = PasajerosRetorno;
                ctlGridPasajerosViaje.LlenarGrid(Pasajeros.ColeccionPasajerosDia[Convert.ToInt32(Indice)].Pasajeros);
                ctlGridPasajerosRetorno.LlenarGrid(PasajerosRetorno.ColeccionPasajerosDia[Convert.ToInt32(Indice)].Pasajeros);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close", "window.close();", true);
        }

        //Seccion de busqueda de pasajeros de CASES
        //protected void imgbBuscarCursosCases_Click(object sender, ImageClickEventArgs e)
        //{
        //    SubSonic.DataProvider pdvRhmCases = TransporteTerrestre.Negocios.Utilerias.ObtenerProveedorDatosRhmCases();
        //    List<TransporteTerrestre.RhmCases.Datos.CasesMaest> listado_curso_cases = new SubSonic.Select(pdvRhmCases).From<TransporteTerrestre.RhmCases.Datos.CasesMaest>().Where(TransporteTerrestre.RhmCases.Datos.CasesMaest.Columns.CveCurso).IsEqualTo(txtCveCurso.Text).And(TransporteTerrestre.RhmCases.Datos.CasesMaest.Columns.CveSap).IsEqualTo(txtNoSapCurso.Text).ExecuteTypedList<TransporteTerrestre.RhmCases.Datos.CasesMaest>();
        //    List<string> listado_areas_servicio = (from x in listado_curso_cases select x.CveActge).Distinct().ToList<string>();

        //    cmbActivo.DataSource = listado_areas_servicio;
        //    cmbActivo.DataBind();

        //    if(listado_areas_servicio != null && listado_areas_servicio.Count > 0)
        //    {
        //        imgbAgregarPasajerosCursos.Enabled = true;
        //    }
        //    else
        //    {
        //        imgbAgregarPasajerosCursos.Enabled = false;
        //    }
        //}

        protected void imgbAgregarPasajerosCursos_Click(object sender, ImageClickEventArgs e)
        {
            SubSonic.DataProvider pdvRhmCases = TransporteTerrestre.Negocios.Utilerias.ObtenerProveedorDatosRhmCases();
            List<TransporteTerrestre.RhmCases.Datos.CasesMaest> listado_curso_cases = new SubSonic.Select(pdvRhmCases).From<TransporteTerrestre.RhmCases.Datos.CasesMaest>().Where(TransporteTerrestre.RhmCases.Datos.CasesMaest.Columns.CveSap).IsEqualTo(txtNoSapCurso.Text).ExecuteTypedList<TransporteTerrestre.RhmCases.Datos.CasesMaest>();

            CargarPasajerosFromCurso(listado_curso_cases, "Ambos");
        }

        private void CargarPasajerosFromCurso(List<TransporteTerrestre.RhmCases.Datos.CasesMaest> listado_pasajeros_cases, string movimiento)
        {
            try
            {

                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection Pasajeros = Page.Session["PasajerosSesion"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection;
                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection PasajerosRetorno = Page.Session["PasajerosRetornoSesion"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection;
                System.Collections.Generic.Dictionary<string, string> FichasNoEncontradas = new System.Collections.Generic.Dictionary<string, string>();
                //LimpiaGrids();

                System.Collections.Generic.List<TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo> lista = new System.Collections.Generic.List<TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo>();
                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo persona = null;

                for (int i = 0; i < Pasajeros.Count(); i++)
                {
                    foreach (TransporteTerrestre.RhmCases.Datos.CasesMaest fila in listado_pasajeros_cases)
                    {
                        TransporteTerrestre.Datos.TerrestrePasajeros Pasajero = TransporteTerrestre.Negocios.Terrestre.Pasajero.ValidaPasajeroExcel(fila.Ficha);
                        persona = new TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo();

                        if (Pasajero != null)
                        {
                            persona.Ficha = Pasajero.Ficha;
                            persona.Nombre = Pasajero.Nombre;
                            persona.Tipo = Pasajero.Tipo;
                            Pasajeros.AgregarPasajeroPorDia(Pasajeros.ColeccionPasajerosDia[i].CveDia, persona);
                        }
                        else
                        {
                            if (!FichasNoEncontradas.ContainsValue(fila.Ficha))
                            {
                                FichasNoEncontradas.Add(FichasNoEncontradas.Keys.Count.ToString(), fila.Ficha);
                            }
                        }
                    }


                    foreach (TransporteTerrestre.RhmCases.Datos.CasesMaest fila in listado_pasajeros_cases)
                    {
                        TransporteTerrestre.Datos.TerrestrePasajeros Pasajero = TransporteTerrestre.Negocios.Terrestre.Pasajero.ValidaPasajeroExcel(fila.Ficha);
                        persona = new TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo();

                        if (Pasajero != null)
                        {
                            persona.Ficha = Pasajero.Ficha;
                            persona.Nombre = Pasajero.Nombre;
                            persona.Tipo = Pasajero.Tipo;
                            PasajerosRetorno.AgregarPasajeroPorDia(Pasajeros.ColeccionPasajerosDia[i].CveDia, persona);
                        }
                        else
                        {
                            if (!FichasNoEncontradas.ContainsValue(fila.Ficha))
                            {
                                FichasNoEncontradas.Add(FichasNoEncontradas.Keys.Count.ToString(), fila.Ficha);
                            }
                        }
                    }
                }

                Page.Session["PasajerosRetornoSesion"] = PasajerosRetorno;
                Page.Session["Pasajeros"] = Pasajeros;
                RefrescaGrids();

                if (FichasNoEncontradas.Count > 0)
                {
                    string Mensaje = "Los trabajadores con las siguientes fichas no se encuentran dados de alta en el sistema:";
                    for (int i = 0; i < FichasNoEncontradas.Values.Count; i++)
                    {
                        Mensaje = Mensaje + " " + FichasNoEncontradas[i.ToString()];
                    }
                    this.Mensaje(Mensaje, "ERROR");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}