﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlListaSolicitudes.ascx.cs" Inherits="SolicitudIntegralMVC.WebTransporteTerrestre.Solicitudterrestre.Controles.ctlListaSolicitudes" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register Src="~/WebTransporteTerrestre/Solicitudterrestre/Controles/ctlDatosGenerales.ascx" TagPrefix="uc1" TagName="ctlDatosGenerales" %>
<style type="text/css">
    
    .CajaDialogo
    {
        background-color: White;
        border-width: 4px;
        border-style: Double;
        border-color: Black;
        padding: 0px;
        width: 500px;
        font-weight: bold;
        font-style: italic;
    }
    .CajaDialogo div
    {
        margin: 5px;
        text-align: center;
    }
    
    .FondoAplicacion
    {
        background-color:  Gray;
        filter: alpha(opacity=70);
        opacity: 0.7;
    }
</style>
<div class="span-24 last small">

    <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="176">

    <asp:Button ID="BtnMensaje" runat="server" 
            onclick="BtnMensaje_Click" BackColor="White" BorderStyle="None" 
            Enabled="False" />

     <asp:GridView ID="GvSolicitudes" class="grid mGrid" runat="server" AutoGenerateColumns="False"
        OnRowCommand="GvSolicitudes_RowCommand" OnPageIndexChanged="GvSolicitudes_PageIndexChanged"
        OnSelectedIndexChanged="GvSolicitudes_SelectedIndexChanged" 
        CssClass="mGrid">
        <Columns>
            <asp:TemplateField HeaderText="Folio">
                <ItemTemplate>
                    <asp:Label ID="lblFolio" runat="server" Text='<%# Eval("Folio") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Servicio">
                <ItemTemplate>
                    <asp:Label ID="lblServicio" runat="server" Text='<%# Eval("CveServicio") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Ficha" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="lblFichaAutorizador" runat="server" Text='<%# Eval("FichaAutorizador") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Autorizador">
                <ItemTemplate>
                    <asp:Label ID="lblAutorizador" runat="server" Text='<%# Eval("Autorizador") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Ficha" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="lblFichaSolicitante" runat="server" Text='<%# Eval("FichaSolicitante") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Solicitante">
                <ItemTemplate>
                    <asp:Label ID="lblSolicitante" runat="server" Text='<%# Eval("Solicitante") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Inicio">
                <ItemTemplate>
                    <asp:Label ID="lblInicioVigencia" runat="server" Text='<%# Eval("InicioVigencia", "{0:d}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Fin">
                <ItemTemplate>
                    <asp:Label ID="lblFinVigencia" runat="server" Text='<%# Eval("FinVigencia", "{0:d}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Observaciones">
                <ItemTemplate>
                    <asp:Label ID="lblObservaciones" runat="server" Text='<%# Eval("Observaciones") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Status">
                <ItemTemplate>
                    <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("Status") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="ibtnEliminar" runat="server" CommandArgument='<%# Eval("IdSolicitudTerrestre") %>'
                        CommandName="Cancelar" 
                        ImageUrl="~/WebTransporteTerrestre/Recursos/img/IconosHomologados16px/EliminarEnabled.png" 
                        Enabled='<%# Convert.ToString(Eval("Status")) == "SOLICITADO" ? true : false %>' 
                        ToolTip="Eliminar" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="ibtnModificar" runat="server" CommandArgument='<%# Eval("IdSolicitudTerrestre") %>'
                        ImageUrl="~/WebTransporteTerrestre/Recursos/img/IconosHomologados16px/EditarEnabled.png" 
                        CommandName="Select" ToolTip="Modificar" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="IBtnPrint" runat="server" 
                        CommandArgument='<%# Eval("IdSolicitudTerrestre") %>' CommandName="Print" 
                        ImageUrl="~/WebTransporteTerrestre/Recursos/img/IconosHomologados16px/ImprimirEnabled.png" 
                        ToolTip="Imprimir" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <SelectedRowStyle CssClass="sel" />
        <AlternatingRowStyle CssClass="alt" />
    </asp:GridView>
    </asp:Panel>       
    
    <asp:Panel ID="PnlAcceso" runat="server" Height="150px" Width="330px" 
           CssClass="CajaDialogo" Style="display: none;"  >
           <table class="style6">
               <tr>
                   <td align="center" bgcolor="#0033CC" colspan="3">
                       <asp:Label ID="Label7" runat="server" ForeColor="White" 
                Text="Mensaje Terrestre"></asp:Label>
                   </td>
               </tr>
               <tr>
                   <td class="style1">
                       </td>
                   <td class="style1">
                       </td>
                   <td class="style1">
                       </td>
               </tr>
               <tr>
                   <td class="style7">
                       <asp:Image ID="Image1" runat="server" ImageUrl="~/WebTransporteTerrestre/Recursos/img/Avisos.png" />
                   </td>
                   <td align="center" colspan="2">
                       <asp:Label ID="LbMEnsajes" runat="server" Width="274px" Font-Size="Medium" ></asp:Label>
                   </td>
               </tr>
               <tr>
                   <td class="style1" >
                       </td>
                   <td class="style1">
                       </td>
                   <td class="style1">
                       </td>
               </tr>
               <tr>
                   <td class="style1" >
                       </td>
                   <td class="style1" align="center">
                       <asp:Button ID="BtnAceptar" runat="server" Text="Aceptar" 
                           onclick="BtnAceptar_Click" />
                       </td>
                   <td class="style1">
                       <asp:Button ID="BtnCancelar" runat="server" Text="Cancelar" />
                       </td>
               </tr>
               <tr>
                   <td align="center" class="style7" colspan="3">
                       &nbsp;</td>
               </tr>
           </table>
       </asp:Panel> 
    <asp:ModalPopupExtender ID="MPEAcceso" runat="server"
         BackgroundCssClass="FondoAplicacion" CancelControlID="BtnCancelar" 
            DropShadow="True" PopupControlID="PnlAcceso" TargetControlID="BtnMensaje">
            </asp:ModalPopupExtender>
         
    
</div>
<div class="span-24 last">
    <div class="span-2">
        <asp:HiddenField ID="HFSol" runat="server" Visible="False" />
    </div>
    <div class="span-20">
        <uc1:ctlDatosGenerales ID="ctlDatosGenerales" runat="server" />
    </div>
    <div class="span-2 last">
    </div>
</div>
