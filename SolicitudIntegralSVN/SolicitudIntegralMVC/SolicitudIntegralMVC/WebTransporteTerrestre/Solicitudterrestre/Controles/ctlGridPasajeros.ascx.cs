﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TransporteTerrestre;

namespace SolicitudIntegralMVC.WebTransporteTerrestre.Solicitudterrestre.Controles
{
    public partial class ctlGridPasajeros : System.Web.UI.UserControl
    {
        public delegate void DeleteEventHandler(string Ficha, bool dias_subsecuentes);
        public event DeleteEventHandler EliminaElemento;
        public delegate void MensajeEventHandler(string Mensaje, string Tipo);
        public event MensajeEventHandler Mensaje;
        public delegate void EditEventHandler(string Ficha, string Nombre);
        public event EditEventHandler EditaElemento;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void LlenarGrid(System.Collections.Generic.List<TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo> Pasajeros)
        {
            try
            {
                gvPasajeros.DataSource = Pasajeros;
                gvPasajeros.DataBind();
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }
                
        public TransporteTerrestre.Datos.TerrestrePasajerosCollection EliminaPasajeroLista(TransporteTerrestre.Datos.TerrestrePasajerosCollection Pasajeros)
        {
            TransporteTerrestre.Datos.TerrestrePasajerosCollection NuevosPasajerosCollection = new TransporteTerrestre.Datos.TerrestrePasajerosCollection();
            try
            {
                TransporteTerrestre.Datos.TerrestrePasajeros NuevoPasajero = null;

                string ficha_busqueda = ((Label)gvPasajeros.Rows[gvPasajeros.SelectedIndex].FindControl("lblFicha")).Text;

                var NuevosPasajeros = Pasajeros.Where(x => x.Ficha != ficha_busqueda);

                foreach (TransporteTerrestre.Datos.TerrestrePasajeros N in NuevosPasajeros)
                {
                    NuevoPasajero = new TransporteTerrestre.Datos.TerrestrePasajeros();
                    NuevoPasajero.IdPasajero = N.IdPasajero;
                    NuevoPasajero.Ficha = N.Ficha;
                    NuevoPasajero.Nombre = N.Nombre;
                    NuevoPasajero.FkSolicitudDia = N.FkSolicitudDia;
                    NuevoPasajero.Tipo = N.Tipo;
                    NuevoPasajero.BorradoLogico = N.BorradoLogico;
                    NuevosPasajerosCollection.Add(NuevoPasajero.Clone());
                }
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
            return NuevosPasajerosCollection;
        }

        protected void gvPasajeros_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.EditaElemento != null)
                {
                    TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.DatosVigenciaUtilerias DatosSolicitud = Page.Session["DatosSolicitud"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.DatosVigenciaUtilerias;
                    if (DatosSolicitud.EstadoSolicitud != "ANEXO")
                    {
                        throw new Exception("Para generar un anexo debe solicitarlo al administrador");
                    }
                    this.EditaElemento(((Label)gvPasajeros.Rows[gvPasajeros.SelectedIndex].FindControl("lblFicha")).Text, ((Label)gvPasajeros.Rows[gvPasajeros.SelectedIndex].FindControl("lblNombre")).Text);
                }
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        protected void gvPasajeros_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                switch (e.CommandName)
                {
                    case "Eliminar":
                        {
                            if (this.EliminaElemento != null)
                            {
                                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.DatosVigenciaUtilerias DatosSolicitud = Page.Session["DatosSolicitud"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.DatosVigenciaUtilerias;
                                if (DatosSolicitud.EstadoSolicitud == "CONFIRMADO" || DatosSolicitud.EstadoSolicitud == "ANEXO")
                                {
                                    throw new Exception("Solo puede eliminar pasajeros de una solicitud antes de ser confirmada");
                                }
                                this.EliminaElemento(e.CommandArgument.ToString(), false);
                            }
                            break;
                        }
                    case "EliminarSubsecuentes":
                        {
                            if (this.EliminaElemento != null)
                            {
                                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.DatosVigenciaUtilerias DatosSolicitud = Page.Session["DatosSolicitud"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.DatosVigenciaUtilerias;
                                if (DatosSolicitud.EstadoSolicitud == "CONFIRMADO" || DatosSolicitud.EstadoSolicitud == "ANEXO")
                                {
                                    throw new Exception("Solo puede eliminar pasajeros de una solicitud antes de ser confirmada");
                                }
                                this.EliminaElemento(e.CommandArgument.ToString(), true);
                            }
                            break;
                        }
                }
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        public TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo RegresaPasajeroSeleccionado()
        {
            try
            {
                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo Pasajero = new TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo();
                Pasajero.Ficha = ((Label)gvPasajeros.Rows[gvPasajeros.SelectedIndex].FindControl("LblFicha")).Text;
                Pasajero.Nombre = ((Label)gvPasajeros.Rows[gvPasajeros.SelectedIndex].FindControl("LblNombre")).Text;
                Pasajero.Tipo = ((Label)gvPasajeros.Rows[gvPasajeros.SelectedIndex].FindControl("LblTipo")).Text;

                return Pasajero;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public void ResetIndexGrid()
        {
            gvPasajeros.SelectedIndex = -1;
        }

        public bool VerificaPasajero(string Ficha)
        {
            bool Existe = false;
            try
            {
                for (int i = 0; i < gvPasajeros.Rows.Count; i++)
                {
                    if (((Label)gvPasajeros.Rows[i].FindControl("LblFicha")).Text == Ficha)
                    {
                        Existe = true;
                    }
                }
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
            return Existe;
        }
    }
}