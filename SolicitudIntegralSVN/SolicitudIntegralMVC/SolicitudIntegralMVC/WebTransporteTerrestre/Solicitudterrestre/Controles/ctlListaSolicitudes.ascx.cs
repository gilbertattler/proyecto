﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolicitudIntegralMVC.WebTransporteTerrestre.Solicitudterrestre.Controles
{
    public partial class ctlListaSolicitudes : System.Web.UI.UserControl
    {
        public delegate void MensajeEventHandler(string Mensaje, string Tipo);
        public event MensajeEventHandler Mensaje;

        protected void Page_Load(object sender, EventArgs e)
        {
            ctlDatosGenerales.SetIndice += new SolicitudIntegralMVC.WebTransporteTerrestre.Solicitudterrestre.Controles.ctlDatosGenerales.UpdateEventHandler(ctlDatosGenerales_SetIndice);
            ctlDatosGenerales.Mensaje += new SolicitudIntegralMVC.WebTransporteTerrestre.Solicitudterrestre.Controles.ctlDatosGenerales.MensajeEventHandler(ctlDatosGenerales_Mensaje);
            ctlDatosGenerales.ActualizaGrid += new SolicitudIntegralMVC.WebTransporteTerrestre.Solicitudterrestre.Controles.ctlDatosGenerales.UpdateGridEventHandler(ctlDatosGenerales_ActualizaGrid);
            
            if (!Page.IsPostBack)
            {
                ctlDatosGenerales.SoloLectura();
                ctlDatosGenerales_ActualizaGrid();
            }
        }

        protected void ctlDatosGenerales_Mensaje(string Mensaje, string Tipo)
        {
            if (this.Mensaje != null)
            {
                this.Mensaje(Mensaje, Tipo);
            }
        }

        protected void ctlDatosGenerales_ActualizaGrid()
        {
            try
            {
                string Subdivision = Page.Session["SubdivisionUsuario"] as string;
                string Nodo_Usuario = Page.Session["Nodo_Usuario"] as string;
                GvSolicitudes.DataSource = TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.SolicitudCollection.FiltraSolicitudes("SUBDIVISION", Subdivision, Nodo_Usuario).ColeccionSolicitudes;
                GvSolicitudes.DataBind();
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        protected void GvSolicitudes_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string Subdivision = Page.Session["SubdivisionUsuario"] as string;
                string Nodo_Usuario = Page.Session["Nodo_Usuario"] as string;
                if (e.CommandName == "Confirmar")
                {
                    TransporteTerrestre.Negocios.Terrestre.Solicitud.ModificarStatus(Convert.ToInt32(e.CommandArgument), "CONFIRMADA", true);
                    //SolicitudIntegral

                }
                else if (e.CommandName == "Cancelar")
                {
                    HFSol.Value = (Convert.ToString(e.CommandArgument));
                    
                    LbMEnsajes.Text = "¿Deseas cancelar la solicitud?";
                    BtnMensaje_Click(sender, e);
                    
                    //TransporteTerrestre.Negocios.Terrestre.Solicitud.ModificarStatus(Convert.ToInt32(e.CommandArgument), "CANCELADA", true);
                }
                else if (e.CommandName == "Print")
                {
                    ctlDatosGenerales.Editar(Convert.ToInt32(((ImageButton)GvSolicitudes.Rows[((System.Web.UI.WebControls.GridViewRow)((((Control)e.CommandSource)).Parent.Parent)).RowIndex].FindControl("IBtnPrint")).CommandArgument));
                    ctlDatosGenerales.SoloLectura();
                    ctlDatosGenerales.Print(Convert.ToInt32(((ImageButton)GvSolicitudes.Rows[((System.Web.UI.WebControls.GridViewRow)((((Control)e.CommandSource)).Parent.Parent)).RowIndex].FindControl("IBtnPrint")).CommandArgument));
                }

                GvSolicitudes.DataSource = TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.SolicitudCollection.FiltraSolicitudes("SUBDIVISION", Subdivision, Nodo_Usuario).ColeccionSolicitudes;
                GvSolicitudes.DataBind();
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        protected void GvSolicitudes_PageIndexChanged(object sender, EventArgs e)
        {

        }

        protected void GvSolicitudes_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (((Label)GvSolicitudes.Rows[GvSolicitudes.SelectedIndex].FindControl("lblStatus")).Text == "SOLICITADO")
                {
                    ctlDatosGenerales.Escritura();
                }
                else
                {
                    ctlDatosGenerales.SoloLectura();
                }
                ctlDatosGenerales.Editar(Convert.ToInt32(((ImageButton)GvSolicitudes.Rows[GvSolicitudes.SelectedIndex].FindControl("ibtnModificar")).CommandArgument));
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        public void ctlDatosGenerales_SetIndice(int Indice)
        {
            try
            {
                GvSolicitudes.SelectedIndex = Indice;
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        public void FiltraSolicitudes()
        {
            try
            {
                string Subdivision = Page.Session["SubdivisionUsuario"] as string;
                string Nodo_Usuario = Page.Session["Nodo_Usuario"] as string;
                GvSolicitudes.DataSource = TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.SolicitudCollection.FiltraSolicitudes("SUBDIVISION", Subdivision, Nodo_Usuario).ColeccionSolicitudes;
                GvSolicitudes.DataBind();
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }
        
        public void ObtenerDatosSession(Models.UserLogon usuario_logueado)
        {
            try
            {
                Page.Session["SubdivisionUsuario"] = usuario_logueado.SubdivisionActual;
                Page.Session["FichaUsuario"] = usuario_logueado.NombreUsuario;
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        public void ObtenerDatosSession(TransporteTerrestre.Negocios.Usuarios Usuario)
        {
            try
            {
                Page.Session["SubdivisionUsuario"] = Usuario.ClaveSubdivisionActual;
                Page.Session["FichaUsuario"] = Usuario.FichaRFCUsuario;
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        protected void BtnMensaje_Click(object sender, EventArgs e)
        {
            MPEAcceso.Show();
        }

        protected void BtnAceptar_Click(object sender, EventArgs e)
        {
            string Subdivision = Page.Session["SubdivisionUsuario"] as string;
            string Nodo_Usuario = Page.Session["Nodo_Usuario"] as string;
            TransporteTerrestre.Negocios.Terrestre.Solicitud.ModificarStatus(Convert.ToInt32(HFSol.Value), "CANCELADA", true);
            GvSolicitudes.DataSource = TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.SolicitudCollection.FiltraSolicitudes("SUBDIVISION", Subdivision, Nodo_Usuario).ColeccionSolicitudes;
            GvSolicitudes.DataBind();
        }

    }
}