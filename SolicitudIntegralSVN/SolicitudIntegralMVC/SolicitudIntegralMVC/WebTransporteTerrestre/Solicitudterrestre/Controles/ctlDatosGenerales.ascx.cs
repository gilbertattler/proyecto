﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolicitudIntegralMVC.WebTransporteTerrestre.Solicitudterrestre.Controles
{
    public partial class ctlDatosGenerales : System.Web.UI.UserControl
    {
        public delegate void UpdateEventHandler(int Indice);
        public event UpdateEventHandler SetIndice;
        public delegate void MensajeEventHandler(string Mensaje, string Tipo);
        public event MensajeEventHandler Mensaje;
        public delegate void UpdateGridEventHandler();
        public event UpdateGridEventHandler ActualizaGrid;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    FiltrarTipo();
                    CargarContratos();
                    LimpiarSessiones();

                    //TransporteTerrestre.GestionGL.Datos.VwDUsuariosGeneralCollection Usuario = TransporteTerrestre.Negocios.Terrestre.Solicitante.BuscaSolicitante(56247);
                    //txtSolicitante.Text = Usuario[0].Ficha;
                    //txtNombreSolicitante.Text = Usuario[0].Nombre;
                    //lbtnSolicitante.CommandArgument = Usuario[0].IdDUsuario.ToString();
                    //TransporteTerrestre.GestionGL.Datos.VwDUsuariosGeneral UsuarioSimple = Usuario[0].Clone();
                    //Page.Session["Usuario"] = UsuarioSimple;            
                }
                int id_servicio = Convert.ToInt32(ddlServicios.SelectedItem.Value);
                ActualizarFechaFin(id_servicio);
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        protected void FiltrarTipo()
        {
            try
            {
                string Nodo_Usuario = Page.Session["Nodo_Usuario"].ToString();
                SubSonic.DataProvider pvdSTPTerrestre = SubSonic.DataService.GetInstance("pvdSTPTerrestre");
                TransporteTerrestre.Datos.CatServiciosCollection Servicios = null;
                if (rbtnDisposicion.Checked)
                {
                    Servicios = new SubSonic.Select(pvdSTPTerrestre)
                    .From<TransporteTerrestre.Datos.CatServicios>()
                    .Where(TransporteTerrestre.Datos.CatServicios.BorradoLogicoColumn)
                    .IsEqualTo(false)
                    .And(TransporteTerrestre.Datos.CatServicios.TipoColumn)
                    .IsEqualTo("Disposicion")
                    .And(TransporteTerrestre.Datos.CatServicios.NodoColumn)
                    .IsEqualTo(Nodo_Usuario)
                    .ExecuteAsCollection<TransporteTerrestre.Datos.CatServiciosCollection>();
                    //txtInicioServicio.Enabled = false;
                    //txtFinServicio.Enabled = false;
                    //txtInicioServicio.Text = "";
                    //txtFinServicio.Text = "";
                }
                else if (rbtnFijo.Checked)
                {
                    Servicios = new SubSonic.Select(pvdSTPTerrestre)
                    .From<TransporteTerrestre.Datos.CatServicios>()
                    .Where(TransporteTerrestre.Datos.CatServicios.BorradoLogicoColumn)
                    .IsEqualTo(false)
                    .And(TransporteTerrestre.Datos.CatServicios.TipoColumn)
                    .IsEqualTo("Fijo")
                    .And(TransporteTerrestre.Datos.CatServicios.NodoColumn)
                    .IsEqualTo(Nodo_Usuario)
                    .ExecuteAsCollection<TransporteTerrestre.Datos.CatServiciosCollection>();
                    //txtInicioServicio.Enabled = true;
                    //txtFinServicio.Enabled = true;
                }

                ddlServicios.DataValueField = "IdServicio";
                ddlServicios.DataTextField = "CveServicio";
                ddlServicios.DataSource = Servicios;
                ddlServicios.DataBind();
                LlenaOrigenDestino();

            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        protected void CargarContratos()
        {
            System.DateTime FechaMod = DateTime.Now.Date;

            
            //TransporteTerrestre.Negocios.Usuarios PermisosUsuario = Utilerias.ObtenerDatosUsariosDesdeSession(this.Page);
            //TransporteTerrestre.Negocios.Usuarios PermisosUsuario = new TransporteTerrestre.Negocios.Usuarios(0, "", "", TransporteTerrestre.Negocios.TipoPersonal.PEMEX, null, "CME");
            //Models.UserLogon usuario_logueado = Infraestructure.Utilerias.ObtenerUsuarioAutentificado(Page.Session);
            Models.UserLogon usuario_logueado = Infraestructure.Utilerias.ObtenerUsuarioAutentificado(Page.Session);


            try
            {
                SubSonic.DataProvider pvdSTPTerrestre = SubSonic.DataService.GetInstance("pvdSTPTerrestre");
                TransporteTerrestre.Datos.VwContratosCollection Contratos = new SubSonic.Select(pvdSTPTerrestre)
                .From<TransporteTerrestre.Datos.VwContratos>()
                .Where(TransporteTerrestre.Datos.VwContratos.Columns.BorradoLogico)
                .IsEqualTo(false)
                .And(TransporteTerrestre.Datos.VwContratos.Columns.Nodo)
                .IsEqualTo(usuario_logueado.NodoAtencionActual)
                .And(TransporteTerrestre.Datos.VwContratos.Columns.FechaInicio)
                .IsLessThanOrEqualTo(FechaMod)
                .And(TransporteTerrestre.Datos.VwContratos.Columns.FechaFinal)
                .IsGreaterThanOrEqualTo(FechaMod)
                .ExecuteAsCollection<TransporteTerrestre.Datos.VwContratosCollection>();

                if (Contratos.Count > 0)
                {

                    ddlContratos.DataSource = Contratos;
                    ddlContratos.DataTextField = "Contratos";
                    ddlContratos.DataValueField = "IdContrato";
                    ddlContratos.DataBind();
                }
                else
                {
                    lbtnAutorizador.Enabled = false;
                    lbtnSolicitante.Enabled = false;
                    throw new System.Exception("No se pueden realizar operaciones por no tener un contrato activo.");
                }
                ddlContratos.Visible = true;
                lblContratoReadOnly.Visible = false;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        protected void rbtnDisposicion_CheckedChanged(object sender, EventArgs e)
        {
            FiltrarTipo();
        }

        protected void rbtnFijo_CheckedChanged(object sender, EventArgs e)
        {
            FiltrarTipo();
        }

        protected void lbtnPasajeros_Click(object sender, EventArgs e)
        {


        }

        public void Editar(int IdSolicitudTerrestre)
        {
            try
            {
                TransporteTerrestre.Datos.TerrestreSolicitud Solicitud = TransporteTerrestre.Datos.TerrestreSolicitud.FetchByID(IdSolicitudTerrestre);
                TransporteTerrestre.Datos.CatServicios Servicio = TransporteTerrestre.Datos.CatServicios.FetchByID(Solicitud.FkIdServicio);
                TransporteTerrestre.GestionGL.Datos.DUsuarios DUsuario = TransporteTerrestre.GestionGL.Datos.DUsuarios.FetchByID(Solicitud.FkIdSolicitante);
                TransporteTerrestre.GestionGL.Datos.MUsuarios MUsuario = TransporteTerrestre.GestionGL.Datos.MUsuarios.FetchByID(DUsuario.IdMUsuario);
                SubSonic.DataProvider pvdGestionGL = SubSonic.DataService.GetInstance("pvdGestionGL");

                TransporteTerrestre.GestionGL.Datos.CEmpleadosPemexCollection EmpleadoPEP = new SubSonic.Select(pvdGestionGL)
                    .From<TransporteTerrestre.GestionGL.Datos.CEmpleadosPemex>()
                    .Where(TransporteTerrestre.GestionGL.Datos.CEmpleadosPemex.FichaColumn)
                    .IsEqualTo(Solicitud.FichaAutorizador)
                    .ExecuteAsCollection<TransporteTerrestre.GestionGL.Datos.CEmpleadosPemexCollection>();

                TransporteTerrestre.Datos.VWTerrestreSolicitudDiaPasajerosCollection TodosPasajeros = new SubSonic.Select()
                    .From<TransporteTerrestre.Datos.VWTerrestreSolicitudDiaPasajeros>()
                    .Where(TransporteTerrestre.Datos.VWTerrestreSolicitudDiaPasajeros.Columns.FkSolicitudTerrestre)
                    .IsEqualTo(IdSolicitudTerrestre)
                    .And(TransporteTerrestre.Datos.VWTerrestreSolicitudDiaPasajeros.Columns.BorradoLogicoPasajeros)
                    .IsEqualTo(false)
                    .ExecuteAsCollection<TransporteTerrestre.Datos.VWTerrestreSolicitudDiaPasajerosCollection>();

                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.DatosVigenciaUtilerias DatosSolicitud = Page.Session["DatosSolicitud"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.DatosVigenciaUtilerias;

                txtNombreAutorizador.Text = EmpleadoPEP[0].Nombres + EmpleadoPEP[0].Apellidos;
                txtAutorizador.Text = Solicitud.FichaAutorizador;
                txtNombreSolicitante.Text = MUsuario.Nombre;
                txtSolicitante.Text = Solicitud.FichaSolicitante;
                txtObservaciones.Text = Solicitud.Observaciones;
                txtInicioVigencia.Text = Solicitud.InicioVigencia.ToShortDateString();
                txtFinVigencia.Text = Solicitud.FinVigencia.ToShortDateString();
                TxtPrograma.Text = Solicitud.CveProgramaPresupuestal;
                TxtFondo.Text = Solicitud.CveDescripcionFondo;
                TxtCentroGestor.Text = Solicitud.CveCentroGestor;
                TxtEpep.Text = Solicitud.CveElementoPEP;
                TxtPosicion.Text = Solicitud.CvePosicionPresupuestaria;
                DatosSolicitud.Folio = Solicitud.Folio;
                DatosSolicitud.IdFolioSolicitud = Solicitud.IdSolicitudTerrestre;
                DatosSolicitud.EstadoSolicitud = Solicitud.Status;
                lbtnPasajeros.CommandArgument = Solicitud.Status;
                pcldInicioVigencia.From.Date = Solicitud.InicioVigencia;
                pcldFinVigencia.To.Date = Solicitud.FinVigencia;
                DatosSolicitud.IdAutorizador = Solicitud.FkIdAutorizador;

                ddlContratos.Visible = false;
                lblContratoReadOnly.Visible = true;
                lblContratoReadOnly.Text = Solicitud.Contrato;

                switch (Solicitud.Status)
                {
                    case "CANCELADO":
                    case "CONFIRMADO":
                    case "ATENDIDA":
                    case "CANCELADA":
                        {
                            lbtnPasajeros.Enabled = false;
                            lbtnGuardar.Enabled = false;
                            break;
                        }
                    case "ANEXO":
                    case "SOLICITADO":
                        {
                            lbtnPasajeros.Enabled = true;
                            break;
                        }
                }

                //Page.Session["SubdivisionUsuario"] = Solicitud.Subdivision;
                //Page.Session["FichaUsuario"] = FichaActualiza;
                if (Servicio.Tipo == "FIJO")
                {
                    rbtnFijo.Checked = true;
                    rbtnDisposicion.Checked = false;
                }
                else
                {
                    rbtnDisposicion.Checked = true;
                    rbtnFijo.Checked = false;
                }
                FiltrarTipo();
                ddlServicios.SelectedValue = Servicio.IdServicio.ToString();
                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection Pasajeros = new TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection();
                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection PasajerosRetorno = new TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection();
                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection EditarPasajeros = new TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection();
                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection EditarPasajerosRetorno = new TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection();
                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo PasajeroMin = null;
                for (int i = 0; i < TodosPasajeros.Count; i++)
                {
                    if (TodosPasajeros[i].Movimiento == "VIAJE")
                    {
                        PasajeroMin = new TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo();
                        PasajeroMin.Ficha = TodosPasajeros[i].Ficha;
                        PasajeroMin.Nombre = TodosPasajeros[i].Nombre;
                        PasajeroMin.Tipo = TodosPasajeros[i].Tipo;
                        Pasajeros.Agregar(TodosPasajeros[i].CveDia, TodosPasajeros[i].Fecha, PasajeroMin, TodosPasajeros[i].InicioServicio, TodosPasajeros[i].TerminoServicio, TodosPasajeros[i].Observaciones);
                    }
                    else if (TodosPasajeros[i].Movimiento == "RETORNO")
                    {
                        PasajeroMin = new TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajeroMinimo();
                        PasajeroMin.Ficha = TodosPasajeros[i].Ficha;
                        PasajeroMin.Nombre = TodosPasajeros[i].Nombre;
                        PasajeroMin.Tipo = TodosPasajeros[i].Tipo;
                        PasajerosRetorno.Agregar(TodosPasajeros[i].CveDia, TodosPasajeros[i].Fecha, PasajeroMin, TodosPasajeros[i].InicioServicio, TodosPasajeros[i].TerminoServicio, TodosPasajeros[i].Observaciones);
                    }
                }
                Page.Session["PasajerosSesion"] = Pasajeros;
                Page.Session["PasajerosRetornoSesion"] = PasajerosRetorno;
                EditarPasajeros.Clonar(Pasajeros);
                Page.Session["EditarPasajeros"] = EditarPasajeros;
                EditarPasajerosRetorno.Clonar(PasajerosRetorno);
                Page.Session["EditarPasajerosRetorno"] = EditarPasajerosRetorno;
                Page.Session["EditarSolicitud"] = Solicitud;
                Page.Session["DatosSolicitud"] = DatosSolicitud;
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        protected void lbtnGuardar_Click(object sender, ImageClickEventArgs e)
        {
            if (txtInicioVigencia.Text == "")
            {
                return;
            }
            try
            {
                //TransporteTerrestre.Negocios.Usuarios PermisosUsuario = Utilerias.ObtenerDatosUsariosDesdeSession(this.Page);
                //TransporteTerrestre.Negocios.Usuarios PermisosUsuario = new TransporteTerrestre.Negocios.Usuarios(0, "", "", TransporteTerrestre.Negocios.TipoPersonal.PEMEX, null, "CME");
                //Models.UserLogon usuario_logueado = Infraestructure.Utilerias.ObtenerUsuarioAutentificado(Page.Session);
                Models.UserLogon usuario_logueado = Infraestructure.Utilerias.ObtenerUsuarioAutentificado(Page.Session);
                
                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection EditarPasajeros = Page.Session["EditarPasajeros"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection;

                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection EditarPasajerosRetorno = Page.Session["EditarPasajerosRetorno"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection;

                TransporteTerrestre.Datos.TerrestreSolicitud EditarSolicitud = Page.Session["EditarSolicitud"] as TransporteTerrestre.Datos.TerrestreSolicitud;

                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection Pasajeros = Page.Session["PasajerosSesion"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection;

                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection PasajerosRetorno = Page.Session["PasajerosRetornoSesion"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection;

                //TransporteTerrestre.Datos.CatServicios Servicio = TransporteTerrestre.Datos.CatServicios.FetchByID(Convert.ToInt32(ddlServicios.SelectedValue));
                string folio = "";
                if (EditarSolicitud == null)
                {
                    TransporteTerrestre.Datos.TerrestreSolicitud Solicitud = new TransporteTerrestre.Datos.TerrestreSolicitud();

                    TransporteTerrestre.Datos.CatServicios Servicio = TransporteTerrestre.Datos.CatServicios.FetchByID(Convert.ToInt32(ddlServicios.SelectedValue));

                    string Ficha = Page.Session["FichaUsuario"] as string;
                    Solicitud.Folio = TransporteTerrestre.Negocios.Terrestre.Utilerias.GenerarFolio(usuario_logueado.NodoAtencionActual);
                    Solicitud.InicioVigencia = Convert.ToDateTime(txtInicioVigencia.Text);
                    Solicitud.FinVigencia = Convert.ToDateTime(txtFinVigencia.Text);
                    Solicitud.Observaciones = txtObservaciones.Text;
                    Solicitud.FkIdAutorizador = Convert.ToInt32(lbtnAutorizador.CommandArgument);
                    Solicitud.FkIdServicio = Convert.ToInt32(ddlServicios.SelectedValue);
                    Solicitud.FkIdSolicitante = Convert.ToInt32(lbtnSolicitante.CommandArgument);
                    Solicitud.FichaAutorizador = TransporteTerrestre.Negocios.Terrestre.Utilerias.FormateaFicha(txtAutorizador.Text);
                    Solicitud.FichaSolicitante = TransporteTerrestre.Negocios.Terrestre.Utilerias.FormateaFicha(txtSolicitante.Text);
                    Solicitud.CveProgramaPresupuestal = TxtPrograma.Text;
                    Solicitud.CveDescripcionFondo = TxtFondo.Text;
                    Solicitud.CveCentroGestor = TxtCentroGestor.Text;
                    Solicitud.CveElementoPEP = TxtEpep.Text;
                    Solicitud.CvePosicionPresupuestaria = TxtPosicion.Text;
                    Solicitud.OrigenServicio = TxtOrigen.Text;
                    Solicitud.DestinoServicio = TxtDestino.Text;
                    Solicitud.CveServicio = Servicio.CveServicio;
                    Solicitud.FichaActualiza = Ficha;
                    Solicitud.FechaActualiza = System.DateTime.Now;
                    Solicitud.IPActualiza = Page.Request.UserHostAddress;
                    Solicitud.Contrato = ddlContratos.Visible == true ? ddlContratos.SelectedItem.Text : lblContratoReadOnly.Text;
                    Solicitud.Subdivision = Page.Session["SubdivisionUsuario"] as string;
                    Solicitud.Nodo = usuario_logueado.NodoAtencionActual;

                    TransporteTerrestre.Negocios.Terrestre.Solicitud.InsertarNuevaSolicitudPasajeros(Solicitud, Pasajeros, PasajerosRetorno, usuario_logueado.NodoAtencionActual, true);
                    folio = Solicitud.Folio;
                    //if(Servicio.Curso)
                    //{
                    //    //TransporteTerrestre.Datos.VWTerrestreSolicitudDiaPasajerosCollection PasajerosSolicitud = new SubSonic.Select().From<TransporteTerrestre.Datos.VWTerrestreSolicitudDiaPasajeros>().Where(TransporteTerrestre.Datos.VWTerrestreSolicitudDiaPasajeros.Columns.BorradoLogicoSolicitudDia).IsEqualTo(false).And(TransporteTerrestre.Datos.VWTerrestreSolicitudDiaPasajeros.Columns.BorradoLogicoPasajeros).IsEqualTo(false).And(TransporteTerrestre.Datos.VWTerrestreSolicitudDiaPasajeros.Columns.FkSolicitudTerrestre).IsEqualTo(Solicitud.IdSolicitudTerrestre).ExecuteAsCollection<TransporteTerrestre.Datos.VWTerrestreSolicitudDiaPasajerosCollection>();
                    //    TransporteTerrestre.Datos.TerrestreSolicitudDiaCollection SolicitudesDia = new SubSonic.Select().From<TransporteTerrestre.Datos.TerrestreSolicitudDia>().Where(TransporteTerrestre.Datos.TerrestreSolicitudDia.BorradoLogicoColumn).IsEqualTo(false).And(TransporteTerrestre.Datos.TerrestreSolicitudDia.FkSolicitudTerrestreColumn).IsEqualTo(Solicitud.IdSolicitudTerrestre).And(TransporteTerrestre.Datos.TerrestreSolicitudDia.MovimientoColumn).IsEqualTo("VIAJE").ExecuteAsCollection<TransporteTerrestre.Datos.TerrestreSolicitudDiaCollection>();
                    //    for (int i = 0; i < SolicitudesDia.Count; i++)
                    //    { 
                    //        TransporteTerrestre.Datos.VWTerrestreSolicitudDiaPasajerosCollection PasajerosSolicitud = new SubSonic.Select().From<TransporteTerrestre.Datos.VWTerrestreSolicitudDiaPasajeros>().Where(TransporteTerrestre.Datos.VWTerrestreSolicitudDiaPasajeros.Columns.BorradoLogicoSolicitudDia).IsEqualTo(false).And(TransporteTerrestre.Datos.VWTerrestreSolicitudDiaPasajeros.Columns.BorradoLogicoPasajeros).IsEqualTo(false).And(TransporteTerrestre.Datos.VWTerrestreSolicitudDiaPasajeros.Columns.FkSolicitudTerrestre).IsEqualTo(Solicitud.IdSolicitudTerrestre).ExecuteAsCollection<TransporteTerrestre.Datos.VWTerrestreSolicitudDiaPasajerosCollection>();
                    //        TransporteTerrestre.Datos.VwViajesServiciosCollection ViajesProgramados = new SubSonic.Select().From<TransporteTerrestre.Datos.VwViajesServicios>().Where(TransporteTerrestre.Datos.VwViajesServicios.Columns.FechaViaje).IsEqualTo(SolicitudesDia[i].Fecha).And(TransporteTerrestre.Datos.VwViajesServicios.Columns.IdServicio).IsEqualTo(Servicio.IdServicio).ExecuteAsCollection<TransporteTerrestre.Datos.VwViajesServiciosCollection>();
                    //        for (int j = 0; j < ViajesProgramados.Count; j++)
                    //        {

                    //        }
                    //    }
                    //}

                    //EnviarCorreo("rhcantoj@pep.pemex.com", "rhcantoj@pep.pemex.com", "Creación de solicitud", "Se ha creado una nueva solicitud de Transporte, Folio: " + Solicitud.Folio);
                }
                else
                {
                    //SoloLecturaEditar();
                    
                    EditarSolicitud.FechaActualiza = System.DateTime.Now;
                    EditarSolicitud.IPActualiza = Page.Request.UserHostAddress; ;
                    EditarSolicitud.FichaActualiza = Page.Session["FichaUsuario"] as string;
                    EditarSolicitud.Observaciones = txtObservaciones.Text;
                    folio = EditarSolicitud.Folio;
                    TransporteTerrestre.Negocios.Terrestre.Solicitud.ModificarSolicitud(EditarSolicitud, Pasajeros, PasajerosRetorno, EditarPasajeros, EditarPasajerosRetorno, false, usuario_logueado.NodoAtencionActual);
                    TransporteTerrestre.Datos.TerrestreAnexosCollection Anexos = Page.Session["Anexos"] as TransporteTerrestre.Datos.TerrestreAnexosCollection;

                    InsertarPasajerosIda(Pasajeros, EditarSolicitud, usuario_logueado);
                    InsertarPasajerosRetorno(PasajerosRetorno, EditarSolicitud, usuario_logueado);  
         
                    for (int i = 0; i < Anexos.Count; i++)
                    {
                        Anexos[i].FkIdSolicitud = EditarSolicitud.IdSolicitudTerrestre;
                        Anexos[i].Nodo = usuario_logueado.NodoAtencionActual;
                    }
                    Anexos.SaveAll();
                    if (this.SetIndice != null)
                    {
                        this.SetIndice(-1);
                    }
                }
                LimpiarSessiones();
                LimpiarCampos();
                if (this.Mensaje != null)
                {
                    this.Mensaje(String.Format("Se guardaron los cambios con exito, Folio Solicitud: {0}", folio), "OK");
                    SoloLectura();
                }
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        protected void LimpiarSessiones()
        {
            try
            {
                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection Pasajeros = new TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection();
                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection PasajerosRetorno = new TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection();
                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection EditarPasajeros = new TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection();
                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection EditarPasajerosRetorno = new TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection();
                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.DatosVigenciaUtilerias DatosSolicitud = new TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.DatosVigenciaUtilerias();
                TransporteTerrestre.Datos.TerrestreAnexosCollection Anexos = new TransporteTerrestre.Datos.TerrestreAnexosCollection();
                TransporteTerrestre.Datos.TerrestreSolicitud Solicitud = null;

                Page.Session["PasajerosSesion"] = Pasajeros;
                Page.Session["PasajerosRetornoSesion"] = PasajerosRetorno;
                Page.Session["EditarPasajeros"] = EditarPasajeros;
                Page.Session["EditarPasajerosRetorno"] = EditarPasajerosRetorno;
                Page.Session["EditarSolicitud"] = Solicitud;
                Page.Session["DatosSolicitud"] = DatosSolicitud;
                Page.Session["Anexos"] = Anexos;
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        protected void LimpiarCampos()
        {
            try
            {
                txtNombreAutorizador.Text = "";
                txtAutorizador.Text = "";
                txtNombreSolicitante.Text = "";
                txtSolicitante.Text = "";
                txtObservaciones.Text = "";
                txtInicioVigencia.Text = "";
                txtFinVigencia.Text = "";
                TxtPrograma.Text = "";
                TxtPosicion.Text = "";
                TxtCentroGestor.Text = "";
                TxtEpep.Text = "";
                TxtFondo.Text = "";
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }
        
        public void ObtenerDatosSession(TransporteTerrestre.Negocios.Usuarios Usuario)
        {
            try
            {
                txtSolicitante.Text = Usuario.FichaRFCUsuario;
                txtNombreSolicitante.Text = Usuario.NombreUsuario;
                //if (Usuario.DiccionarioAreasServicioIdDUsuarios["STPVT"].Count > 0)
                //{
                lbtnSolicitante.CommandArgument = Usuario.DiccionarioAreasServicioIdDUsuarios["STPVT"][0].ToString();
                //}
                Page.Session["SubdivisionUsuario"] = Usuario.ClaveSubdivisionActual;
                Page.Session["FichaUsuario"] = Usuario.FichaRFCUsuario;
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        //Adaptacion para el sistema de solicitudes integrales
        public void ObtenerDatosSession(Models.UserLogon usuario_logueado)
        {
            try
            {
                txtSolicitante.Text = usuario_logueado.NombreUsuario;
                txtNombreSolicitante.Text = usuario_logueado.NombreCompletoUsuario;
                //if (Usuario.DiccionarioAreasServicioIdDUsuarios["STPVT"].Count > 0)
                //{
                lbtnSolicitante.CommandArgument = usuario_logueado.IdDUsuarioActual.ToString();
                //}
                Page.Session["SubdivisionUsuario"] = usuario_logueado.SubdivisionActual;
                Page.Session["FichaUsuario"] = usuario_logueado.NombreUsuario;
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }
        
        protected void lbtnAutorizador_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //Revisar el valor de la variable de session
                string AreaServicioAutorizador = "";

                
                //TransporteTerrestre.Negocios.Usuarios usuario = Utilerias.ObtenerDatosUsariosDesdeSession(this.Page);
                //TransporteTerrestre.Negocios.Usuarios usuario = new TransporteTerrestre.Negocios.Usuarios(0, "", "", TransporteTerrestre.Negocios.TipoPersonal.PEMEX, null, "CME");
                //Models.UserLogon usuario_logueado = Infraestructure.Utilerias.ObtenerUsuarioAutentificado(Page.Session);
                Models.UserLogon usuario_logueado = Infraestructure.Utilerias.ObtenerUsuarioAutentificado(Page.Session);

                if (usuario_logueado.NodoAtencionActual == "CME")
                {
                    AreaServicioAutorizador = "STPVT";
                }
                else if (usuario_logueado.NodoAtencionActual == "ZN")
                {
                    AreaServicioAutorizador = "STPVT_ZN";
                }

                string SubdivisionUsuario = usuario_logueado.SubdivisionActual.Substring(0, 3);

                string Ficha = TransporteTerrestre.Negocios.Terrestre.Utilerias.FormateaFicha(txtAutorizador.Text);

                SubSonic.DataProvider pvdGestionGL = TransporteTerrestre.Negocios.Utilerias.ObtenerProveedorDatosGestionGl();

                TransporteTerrestre.GestionGL.Datos.VwCDAutorizadores Autoriador = new TransporteTerrestre.GestionGL.Datos.VwCDAutorizadores();

                TransporteTerrestre.GestionGL.Datos.VwPermisosDireccionamientosGeneralCollection Permisos = new SubSonic.Select(pvdGestionGL)
                .From<TransporteTerrestre.GestionGL.Datos.VwPermisosDireccionamientosGeneral>()
                .Where(TransporteTerrestre.GestionGL.Datos.VwPermisosDireccionamientosGeneral.Columns.SubdivisionDireccionamiento)
                .Like(SubdivisionUsuario + "%")
                .And(TransporteTerrestre.GestionGL.Datos.VwPermisosDireccionamientosGeneral.Columns.BorradoLogicoPermisos)
                .IsEqualTo(false)
                .And(TransporteTerrestre.GestionGL.Datos.VwPermisosDireccionamientosGeneral.Columns.Inicio)
                .IsLessThanOrEqualTo(Convert.ToDateTime(txtInicioVigencia.Text))
                .And(TransporteTerrestre.GestionGL.Datos.VwPermisosDireccionamientosGeneral.Columns.Fin)
                .IsGreaterThanOrEqualTo(Convert.ToDateTime(txtFinVigencia.Text))
                .And(TransporteTerrestre.GestionGL.Datos.VwPermisosDireccionamientosGeneral.Columns.SiglasInstalacion)
                .IsEqualTo(usuario_logueado.NodoAtencionActual)
                .And(TransporteTerrestre.GestionGL.Datos.VwPermisosDireccionamientosGeneral.Columns.AreaServicioAutorizador)
                .IsEqualTo(AreaServicioAutorizador)
                .And(TransporteTerrestre.GestionGL.Datos.VwPermisosDireccionamientosGeneral.Columns.FichaAutorizador)
                .IsEqualTo(Ficha)
                .ExecuteAsCollection<TransporteTerrestre.GestionGL.Datos.VwPermisosDireccionamientosGeneralCollection>();

                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.DatosVigenciaUtilerias DatosSolicitud = Page.Session["DatosSolicitud"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.DatosVigenciaUtilerias;

                if (Permisos.Count < 1)
                {
                    throw new Exception("El autorizador no tiene direccionamientos para transporte terrestre en el rango de fechas seleccionado");
                }
                else if (Permisos.Count == 1)
                {
                    TxtPrograma.Text = Permisos[0].CveProgramaPresupuestal;
                    TxtCentroGestor.Text = Permisos[0].CveCtoGestor;
                    TxtPosicion.Text = Permisos[0].CvePosicionPresupuestaria;
                    TxtFondo.Text = Permisos[0].DescripcionFondo;
                    if (Permisos[0].DescripcionFondo == "PEF - INVERSION")
                    {
                        TxtEpep.Text = Permisos[0].CveElementoPep;
                    }
                    else if (Permisos[0].DescripcionFondo == "PEF - OPERACION")
                    {
                        //CAMBIO SOLICITUD TERRESTRE
                        //TxtEpep.Text = Permisos[0].CveCtoCostos;
                        TxtEpep.Text = Permisos[0].CveElementoPep;
                    }
                    lbtnPasajeros.Enabled = true;
                    lbtnGuardar.Enabled = true;
                }
                else if (Permisos.Count > 1)
                {
                    GvDireccionamientos.DataSource = Permisos;
                    GvDireccionamientos.DataBind();
                }

                if (usuario_logueado.SubdivisionActual != "")
                {
                    Autoriador = TransporteTerrestre.Negocios.Terrestre.Autorizador.ValidaAutorizador(txtAutorizador.Text, usuario_logueado.SubdivisionActual, "STPVT", Convert.ToDateTime(txtInicioVigencia.Text), Convert.ToDateTime(txtFinVigencia.Text));
                }
                else
                {
                    throw new Exception("Captura primero un solicitante");
                }
                txtNombreAutorizador.Text = Autoriador.Nombres + " " + Autoriador.Apellidos;
                lbtnAutorizador.CommandArgument = Autoriador.IdCDAutorizador.ToString();
                DatosSolicitud.IdAutorizador = Autoriador.IdCDAutorizador;
                Page.Session["DatosSolicitud"] = DatosSolicitud;
                lbtnPasajeros.Enabled = true;
                lbtnGuardar.Enabled = true;
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                    lbtnPasajeros.Enabled = false;
                    lbtnGuardar.Enabled = false;
                }
            }
        }

        protected void lbtnSolicitante_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                string SubdivisionUsuario = Page.Session["SubdivisionUsuario"] as string;
                TransporteTerrestre.GestionGL.Datos.VwDUsuariosGeneral Usuario = TransporteTerrestre.Negocios.Terrestre.Solicitante.ValidaSolicitantexServicio(txtSolicitante.Text, SubdivisionUsuario, Convert.ToInt32(ddlServicios.SelectedValue));
                txtNombreSolicitante.Text = Usuario.Nombre;
                lbtnSolicitante.CommandArgument = Usuario.IdDUsuario.ToString();
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        protected void GvDireccionamientos_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                TxtPrograma.Text = ((Label)GvDireccionamientos.Rows[GvDireccionamientos.SelectedIndex].FindControl("LblPrograma")).Text;
                TxtCentroGestor.Text = ((Label)GvDireccionamientos.Rows[GvDireccionamientos.SelectedIndex].FindControl("LblCentroGestor")).Text;
                TxtPosicion.Text = ((Label)GvDireccionamientos.Rows[GvDireccionamientos.SelectedIndex].FindControl("LblPosicionPresupuestaria")).Text;
                TxtFondo.Text = ((Label)GvDireccionamientos.Rows[GvDireccionamientos.SelectedIndex].FindControl("LblFondo")).Text;
                TxtEpep.Text = ((Label)GvDireccionamientos.Rows[GvDireccionamientos.SelectedIndex].FindControl("LblCostosEPEP")).Text;
                //TxtEpep.Text = ((Label)GvDireccionamientos.Rows[GvDireccionamientos.SelectedIndex].FindControl("LblCostosEPEP")).Text;            
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        public void SoloLectura()
        {
            try
            {
                txtSolicitante.Enabled = false;
                TxtPrograma.Enabled = false;
                TxtPosicion.Enabled = false;
                txtObservaciones.Enabled = false;
                txtNombreSolicitante.Enabled = false;
                txtNombreAutorizador.Enabled = false;
                txtInicioVigencia.Enabled = false;
                TxtFondo.Enabled = false;
                txtFinVigencia.Enabled = false;
                TxtEpep.Enabled = false;
                TxtCentroGestor.Enabled = false;
                txtAutorizador.Enabled = false;
                lbtnAutorizador.Enabled = false;
                //lbtnGuardar.Enabled = false;
                lbtnSolicitante.Enabled = false;
                pcldFinVigencia.Enabled = false;
                pcldInicioVigencia.Enabled = false;
                ddlServicios.Enabled = false;
                rbtnDisposicion.Enabled = false;
                rbtnFijo.Enabled = false;
                TxtOrigen.Enabled = false;
                TxtDestino.Enabled = false;
                ddlContratos.Enabled = false;
                lbtnGuardar.Enabled = true;
                lbtnPasajeros.Enabled = true;
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        public void Escritura()
        {
            try
            {
                //txtSolicitante.Enabled = true;
                //TxtPrograma.Enabled = true;
                //TxtPosicion.Enabled = true;
                txtObservaciones.Enabled = true;
                //txtNombreSolicitante.Enabled = true;
                //txtNombreAutorizador.Enabled = true;
                //txtInicioVigencia.Enabled = true;
                //TxtFondo.Enabled = true;
                //txtFinVigencia.Enabled = true;
                //TxtEpep.Enabled = true;
                //TxtCentroGestor.Enabled = true;
                //txtAutorizador.Enabled = true;
                //lbtnAutorizador.Enabled = true;
                //lbtnGuardar.Enabled = true;
                //lbtnSolicitante.Enabled = true;
                //pcldFinVigencia.Enabled = true;
                //pcldInicioVigencia.Enabled = true;
                //ddlServicios.Enabled = false;
                //rbtnDisposicion.Enabled = true;
                //rbtnFijo.Enabled = true;
                //TxtDestino.Enabled = true;
                //TxtOrigen.Enabled = true;
                //ddlContratos.Enabled = true;


                txtSolicitante.Enabled = false;
                TxtPrograma.Enabled = false;
                TxtPosicion.Enabled = false;
                //txtObservaciones.Enabled = false;
                txtNombreSolicitante.Enabled = false;
                txtNombreAutorizador.Enabled = false;
                txtInicioVigencia.Enabled = false;
                TxtFondo.Enabled = false;
                txtFinVigencia.Enabled = false;
                TxtEpep.Enabled = false;
                TxtCentroGestor.Enabled = false;
                txtAutorizador.Enabled = false;
                lbtnAutorizador.Enabled = false;
                //lbtnGuardar.Enabled = false;
                lbtnSolicitante.Enabled = false;
                pcldFinVigencia.Enabled = false;
                pcldInicioVigencia.Enabled = false;
                ddlServicios.Enabled = false;
                rbtnDisposicion.Enabled = false;
                rbtnFijo.Enabled = false;
                TxtOrigen.Enabled = false;
                TxtDestino.Enabled = false;
                ddlContratos.Enabled = false;
                lbtnGuardar.Enabled = true;
                lbtnPasajeros.Enabled = true;







            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        protected void EnviarCorreo(string From, string To, string Subject, string Body)
        {
            try
            {
                System.Net.Mail.MailMessage correo = new System.Net.Mail.MailMessage();
                correo.From = new System.Net.Mail.MailAddress(From);
                correo.To.Add(To);
                correo.Subject = Subject;
                correo.Body = Body;
                correo.IsBodyHtml = true;
                correo.Priority = System.Net.Mail.MailPriority.Normal;
                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
                smtp.Host = "pepmarexc05.mar.dpep.pep.pemex.com";

                smtp.Send(correo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void pcldFinVigencia_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                //TransporteTerrestre.Datos.CatContratos Contrato = TransporteTerrestre.Datos.CatContratos.FetchByID(Convert.ToInt32(ddlContratos.SelectedValue));

                SubSonic.DataProvider pvdSTPTerrestre = SubSonic.DataService.GetInstance("pvdSTPTerrestre");

                TransporteTerrestre.Datos.VwContratosCollection ContratosActivosFin = new SubSonic.Select(pvdSTPTerrestre)
               .From<TransporteTerrestre.Datos.VwContratos>()
               .Where(TransporteTerrestre.Datos.VwContratos.Columns.IdContrato)
               .IsEqualTo(ddlContratos.SelectedValue)
               .ExecuteAsCollection<TransporteTerrestre.Datos.VwContratosCollection>();

                TimeSpan Vigencia = Convert.ToDateTime(ContratosActivosFin[0].FechaFinal) - Convert.ToDateTime(pcldFinVigencia.SelectedDate);
                int Dias = Vigencia.Days;
                if (Dias < 0)
                {
                    throw new Exception("La fecha seleccionada como termino de la solicitud excede el fin del contrato");
                }
                int id_servicio = Convert.ToInt32(ddlServicios.SelectedItem.Value);
                ActualizarFechaFin(id_servicio);
            }
            catch (System.Exception ex)
            {
                pcldFinVigencia.SelectedDate = "";
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        protected void lbtnPasajeros_Click(object sender, ImageClickEventArgs e)
        {
            if (txtInicioVigencia.Text == "")
            {
                return;
            }
            try
            {
                TimeSpan Dias = Convert.ToDateTime(txtFinVigencia.Text).AddDays(1) - Convert.ToDateTime(txtInicioVigencia.Text);
                int NoDias = Dias.Days;



                TransporteTerrestre.Datos.CatServicios Servicio = TransporteTerrestre.Datos.CatServicios.FetchByID(Convert.ToInt32(ddlServicios.SelectedValue));

                TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.DatosVigenciaUtilerias DatosSolicitud = Page.Session["DatosSolicitud"] as TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.DatosVigenciaUtilerias;
                DatosSolicitud.VigenciaFechaFin = Convert.ToDateTime(txtFinVigencia.Text);
                DatosSolicitud.VigenciaFechaInicio = Convert.ToDateTime(txtInicioVigencia.Text);
                DatosSolicitud.TipoServicio = Servicio.Tipo;
                DatosSolicitud.HoraServicio = Servicio.Horario;
                DatosSolicitud.HoraInicioCandado = Servicio.InicioHorarioAtencion;
                DatosSolicitud.HoraFinCandado = Servicio.FinHorarioAtencion;
                DatosSolicitud.Origen = TxtOrigen.Text;
                DatosSolicitud.Destino = TxtDestino.Text;

                Page.Session["DatosSolicitud"] = DatosSolicitud;

                //if (NoDias > 30 & TxtDestino.Text == "SCASE")
                //{
                //    throw new Exception("La vigencia de un servicio no puede ser mayor a 30 dias");
                //}

                //if (NoDias > 30 & TxtDestino.Text == "ATASTA")
                //{
                //    throw new Exception("La vigencia de un servicio no puede ser mayor a 30 dias");
                //}

                //if (NoDias > 30 & rbtnDisposicion.Checked  == true )
                //{
                //    throw new Exception("La vigencia de un servicio no puede ser mayor a 30 dias");
                //}

                TransporteTerrestre.Datos.CatServicios servicios = TransporteTerrestre.Datos.CatServicios.FetchByID(ddlServicios.SelectedValue);

                if (NoDias > servicios.DiasCandado)
                {
                    throw new Exception("La vigencia de un servicio no puede ser mayor a " + servicios.DiasCandado + " dias");
                }

                string Url = "<script language='JavaScript'> window.showModalDialog('CargaPasajeros.aspx?Dias=" + NoDias + "&Operacion=" + lbtnPasajeros.CommandArgument.ToString() + "','CargaPasajeros','dialogTop=250px; dialogLeft=250px; dialogWidth=800px; " +
                "dialogHeight=700px; center=no; help=no; status=no; menubar=no; resizable=no; border=thin'); </script>";
                // ScriptManager.RegisterStartupScript(this, this.GetType(), "ColorFondo", "$get('DivPrincipal').className = 'Opaco';", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "CargaPasajeros", Url, false);
            }

            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        protected void pcldInicioVigencia_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                //TransporteTerrestre.Datos.CatContratos Contrato = TransporteTerrestre.Datos.CatContratos.FetchByID(Convert.ToInt32(ddlContratos.SelectedValue));

                SubSonic.DataProvider pvdSTPTerrestre = SubSonic.DataService.GetInstance("pvdSTPTerrestre");

                TransporteTerrestre.Datos.VwContratosCollection ContratosActivosInicio = new SubSonic.Select(pvdSTPTerrestre)
               .From<TransporteTerrestre.Datos.VwContratos>()
               .Where(TransporteTerrestre.Datos.VwContratos.Columns.IdContrato)
               .IsEqualTo(ddlContratos.SelectedValue)
               .ExecuteAsCollection<TransporteTerrestre.Datos.VwContratosCollection>();

                TimeSpan Vigencia = Convert.ToDateTime(pcldInicioVigencia.SelectedDate) - Convert.ToDateTime(ContratosActivosInicio[0].FechaInicio);
                int Dias = Vigencia.Days;
                if (Dias < 0)
                {
                    throw new Exception("La fecha seleccionada como inicio de la solicitud excede el inicio del contrato");
                }

                int id_servicio = Convert.ToInt32(ddlServicios.SelectedItem.Value);
                ActualizarFechaFin(id_servicio);
            }
            catch (System.Exception ex)
            {
                pcldInicioVigencia.SelectedDate = "";
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        protected void LlenaOrigenDestino()
        {
            try
            {
                if (ddlServicios.SelectedValue != "")
                {
                    TransporteTerrestre.Datos.CatServicios Servicio = TransporteTerrestre.Datos.CatServicios.FetchByID(Convert.ToInt32(ddlServicios.SelectedValue));

                    TransporteTerrestre.Datos.VwCMInstalacionesGestionGLCollection Origen = new SubSonic.Select()
                    .From<TransporteTerrestre.Datos.VwCMInstalacionesGestionGL>()
                    .Where(TransporteTerrestre.Datos.VwCMInstalacionesGestionGL.Columns.IdCMInstalacion)
                    .IsEqualTo(Servicio.CveOrigen)
                    .ExecuteAsCollection<TransporteTerrestre.Datos.VwCMInstalacionesGestionGLCollection>();

                    TxtOrigen.Text = Origen[0].NombreInstalacion;

                    TransporteTerrestre.Datos.VwCMInstalacionesGestionGLCollection Destino = new SubSonic.Select()
                    .From<TransporteTerrestre.Datos.VwCMInstalacionesGestionGL>()
                    .Where(TransporteTerrestre.Datos.VwCMInstalacionesGestionGL.Columns.IdCMInstalacion)
                    .IsEqualTo(Servicio.CveDestino)
                    .ExecuteAsCollection<TransporteTerrestre.Datos.VwCMInstalacionesGestionGLCollection>();

                    TxtDestino.Text = Destino[0].NombreInstalacion;
                }
                else
                {
                    TxtDestino.Text = "";
                    TxtOrigen.Text = "";
                    throw new System.Exception("No se pueden realizar operaciones por no tener servicios creados.");


                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        protected void ddlServicios_SelectedIndexChanged(object sender, EventArgs e)
        {
            LlenaOrigenDestino();

            int id_servicio = Convert.ToInt32(ddlServicios.SelectedItem.Value);
            ActualizarFechaFin(id_servicio);
        }

        protected void VisibleImpresion(string estado_solicitud)
        {
            try
            {
                TxtDia.Visible = true;
                LblImprimir.Visible = true;
                IBtImprimir.Visible = true;
                IBtImprimirBoleto.Visible = true;

                if (estado_solicitud == "SOLICITADO")
                {
                    ConfirmButtonExtender1.ConfirmText = "Al imprimir esta solicitud se confirmara automaticamente";
                    ConfirmButtonExtender1.Enabled = true;
                }
                else
                {
                    ConfirmButtonExtender1.ConfirmText = "";
                    ConfirmButtonExtender1.Enabled = false;
                }
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        protected void IBtImprimir_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                string Nodo_Usuario = Page.Session["Nodo_Usuario"].ToString();
                TransporteTerrestre.Datos.TerrestreSolicitud Solicitud = TransporteTerrestre.Datos.TerrestreSolicitud.FetchByID(Convert.ToInt32(IBtImprimir.CommandArgument));

                if (Solicitud.Status == "SOLICITADO")
                {
                    TransporteTerrestre.Negocios.Terrestre.Solicitud.ModificarStatus(Convert.ToInt32(IBtImprimir.CommandArgument), "CONFIRMADO", true);
                }

                TransporteTerrestre.Datos.TerrestreSolicitudDiaCollection SolicitudDia = new SubSonic.Select()
                    .From<TransporteTerrestre.Datos.TerrestreSolicitudDia>()
                    .Where(TransporteTerrestre.Datos.TerrestreSolicitudDia.FkSolicitudTerrestreColumn)
                    .IsEqualTo(Convert.ToInt32(IBtImprimir.CommandArgument))
                    .And(TransporteTerrestre.Datos.TerrestreSolicitudDia.FechaColumn)
                    .IsEqualTo(Convert.ToDateTime(TxtDia.Text))
                    .And(TransporteTerrestre.Datos.TerrestreSolicitudDia.NodoColumn)
                    .IsEqualTo(Nodo_Usuario)
                    .ExecuteAsCollection<TransporteTerrestre.Datos.TerrestreSolicitudDiaCollection>();

                if (SolicitudDia.Count <= 0)
                {
                    throw new Exception("No existe una solicitud para el dia solicitado");
                }

                TransporteTerrestre.Datos.TerrestrePasajerosCollection Pasajeros = new SubSonic.Select()
                    .From<TransporteTerrestre.Datos.TerrestrePasajeros>()
                    .Where(TransporteTerrestre.Datos.TerrestrePasajeros.FkIdSolicitudColumn)
                    .IsEqualTo(Convert.ToInt32(IBtImprimir.CommandArgument))
                    //.And(TransporteTerrestre.Datos.TerrestrePasajeros.SitContColumn)
                    //.IsNotNull()
                    .And(TransporteTerrestre.Datos.TerrestrePasajeros.FkIdSolicitudColumn)
                    .IsEqualTo(SolicitudDia[0].FkSolicitudTerrestre)
                    .And(TransporteTerrestre.Datos.TerrestrePasajeros.NodoColumn)
                    .IsEqualTo(Nodo_Usuario)
                    .ExecuteAsCollection<TransporteTerrestre.Datos.TerrestrePasajerosCollection>();

                string UrlReports = System.Web.Configuration.WebConfigurationManager.AppSettings["urlReports_terrestre"];
                string Url = String.Format("{0}RptSolicitud_Terrestre&rs:Command=Render&rs:Parameters=false&rc:toolbar=true&FolioSolicitud={1}&CveDia={2}&Nodo_Usuario={3}", UrlReports, Solicitud.Folio, SolicitudDia[0].CveDia, Nodo_Usuario);
                //if (Pasajeros.Count > 0)
                //{
                //    Url = String.Format("{0}RptSolicitudCases&rs:Command=Render&rs:Parameters=false&rc:toolbar=true&FolioSolicitud={1}&CveDia={2}&Nodo_Usuario={3}", UrlReports, Solicitud.Folio, SolicitudDia[0].CveDia, Nodo_Usuario);
                //}
                //string UrlReports = System.Web.Configuration.WebConfigurationManager.AppSettings["urlReports"];
                //string Url = String.Format("{0}RptSolicitud_Terrestre&rs:Command=Render&rs:Parameters=false&rc:toolbar=true&FolioSolicitud={1}&CveDia={2}", UrlReports, Solicitud.Folio,SolicitudDia[0].CveDia);
                string AbreVentana = "<script> javascript: var ventana = window.open('" + Url + "','Programas','height=0,width=0,status=yes,titlebar=yes,scrollbars=no,resizable=yes,menubar=no,toolbar=no');</script>";
                //ClientScript.RegisterStartupScript(Page.GetType(), "Imprime", FocusScript);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ReporteVersion", AbreVentana, false);
                if (this.SetIndice != null)
                {
                    this.SetIndice(-1);
                }
                if (this.ActualizaGrid != null)
                {
                    this.ActualizaGrid();
                }
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        public void Print(int IdSolicitudTerrestre)
        {
            try
            {
                TransporteTerrestre.Datos.TerrestreSolicitud solicitud_terrestre = TransporteTerrestre.Datos.TerrestreSolicitud.FetchByID(IdSolicitudTerrestre);

                VisibleImpresion(solicitud_terrestre.Status);

                pclImprimir.From.Date = solicitud_terrestre.InicioVigencia;
                pclImprimir.To.Date = solicitud_terrestre.FinVigencia;

                TransporteTerrestre.Datos.TerrestreAnexosCollection Anexos = new SubSonic.Select().From<TransporteTerrestre.Datos.TerrestreAnexos>().Where(TransporteTerrestre.Datos.TerrestreAnexos.FkIdSolicitudColumn).IsEqualTo(IdSolicitudTerrestre).ExecuteAsCollection<TransporteTerrestre.Datos.TerrestreAnexosCollection>();
                if (Anexos.Count > 0)
                {
                    DdlAnexos.Items.Clear();
                    for (int i = 0; i < Anexos.Count; i++)
                    {
                        DdlAnexos.Items.Add(new ListItem(Anexos[i].Anexo + " - " + Anexos[i].FechaCambio.ToShortDateString(), Anexos[i].PkIdAnexo.ToString()));
                    }
                    VisibleAnexo();
                }
                lbtnPasajeros.Enabled = false;
                lbtnGuardar.Enabled = false;
                IBtImprimir.CommandArgument = IdSolicitudTerrestre.ToString();
                IBtImprimirBoleto.CommandArgument = IdSolicitudTerrestre.ToString();
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        protected void VisibleAnexo()
        {
            try
            {
                LblAnexos.Visible = true;
                DdlAnexos.Visible = true;
                IBtnImprimirAnexo.Visible = true;
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        protected void IBtnImprimirAnexo_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //TransporteTerrestre.Negocios.Terrestre.Solicitud.ModificarStatusPrint(Convert.ToInt32(IBtImprimir.CommandArgument), "CONFIRMADO", true);
                //TransporteTerrestre.Datos.TerrestreSolicitud Solicitud = TransporteTerrestre.Datos.TerrestreSolicitud.FetchByID(Convert.ToInt32(IBtImprimir.CommandArgument));
                //TransporteTerrestre.Datos.TerrestreSolicitudDiaCollection SolicitudDia = new SubSonic.Select().From<TransporteTerrestre.Datos.TerrestreSolicitudDia>().Where(TransporteTerrestre.Datos.TerrestreSolicitudDia.FkSolicitudTerrestreColumn).IsEqualTo(Convert.ToInt32(IBtImprimir.CommandArgument)).And(TransporteTerrestre.Datos.TerrestreSolicitudDia.FechaColumn).IsEqualTo(Convert.ToDateTime(TxtDia.Text)).ExecuteAsCollection<TransporteTerrestre.Datos.TerrestreSolicitudDiaCollection>();
                //if (SolicitudDia.Count <= 0)
                //{
                //    throw new Exception("No existe una solicitud para el dia solicitado");
                //}


                string UrlReports = System.Web.Configuration.WebConfigurationManager.AppSettings["urlReports_terrestre"];
                string Url = String.Format("{0}RptAnexo&rs:Command=Render&rs:Parameters=false&rc:toolbar=true&IdAnexo={1}", UrlReports, Convert.ToInt32(DdlAnexos.SelectedValue));
                string AbreVentana = "<script> javascript: var ventana = window.open('" + Url + "','Programas','height=0,width=0,status=yes,titlebar=yes,scrollbars=no,resizable=yes,menubar=no,toolbar=no');</script>";
                //ClientScript.RegisterStartupScript(Page.GetType(), "Imprime", FocusScript);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ReporteVersion", AbreVentana, false);
                //if (this.SetIndice != null)
                //{
                //    this.SetIndice(-1);
                //}
                //if (this.ActualizaGrid != null)
                //{
                //    this.ActualizaGrid();
                //}
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        protected void lbtnGuardar_Unload(object sender, EventArgs e)
        {
            //LimpiarSessiones();
        }

        protected void IBtImprimirBoleto_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                string Nodo_Usuario = Page.Session["Nodo_Usuario"].ToString();
                TransporteTerrestre.Datos.TerrestreSolicitud Solicitud = TransporteTerrestre.Datos.TerrestreSolicitud.FetchByID(Convert.ToInt32(IBtImprimirBoleto.CommandArgument));

                if (Solicitud.Status == "SOLICITADO")
                {
                    TransporteTerrestre.Negocios.Terrestre.Solicitud.ModificarStatus(Convert.ToInt32(IBtImprimirBoleto.CommandArgument), "CONFIRMADO", true);
                }

                TransporteTerrestre.Datos.TerrestreSolicitudDiaCollection SolicitudDia = new SubSonic.Select()
                    .From<TransporteTerrestre.Datos.TerrestreSolicitudDia>()
                    .Where(TransporteTerrestre.Datos.TerrestreSolicitudDia.FkSolicitudTerrestreColumn)
                    .IsEqualTo(Convert.ToInt32(IBtImprimir.CommandArgument))
                    .And(TransporteTerrestre.Datos.TerrestreSolicitudDia.FechaColumn)
                    .IsEqualTo(Convert.ToDateTime(TxtDia.Text))
                    .And(TransporteTerrestre.Datos.TerrestreSolicitudDia.NodoColumn)
                    .IsEqualTo(Nodo_Usuario)
                    .ExecuteAsCollection<TransporteTerrestre.Datos.TerrestreSolicitudDiaCollection>();

                if (SolicitudDia.Count <= 0)
                {
                    throw new Exception("No existe una solicitud para el dia solicitado");
                }

                TransporteTerrestre.Datos.TerrestrePasajerosCollection Pasajeros = new SubSonic.Select()
                    .From<TransporteTerrestre.Datos.TerrestrePasajeros>()
                    .Where(TransporteTerrestre.Datos.TerrestrePasajeros.FkIdSolicitudColumn)
                    .IsEqualTo(Convert.ToInt32(IBtImprimir.CommandArgument))
                    //.And(TransporteTerrestre.Datos.TerrestrePasajeros.SitContColumn)
                    //.IsNotNull()
                    .And(TransporteTerrestre.Datos.TerrestrePasajeros.NodoColumn)
                    .IsEqualTo(Nodo_Usuario)
                    .ExecuteAsCollection<TransporteTerrestre.Datos.TerrestrePasajerosCollection>();

                string UrlReports = System.Web.Configuration.WebConfigurationManager.AppSettings["urlReports_terrestre"];
                string Url = String.Format("{0}Boletos&rs:Command=Render&rs:Parameters=false&rc:toolbar=true&Folio={1}&FechaSalida={2:yyyyMMdd}&Nodo_Usuario={3}", UrlReports, Solicitud.Folio, pclImprimir.DateValue, Nodo_Usuario);

                string AbreVentana = "<script> javascript: var ventana = window.open('" + Url + "','Programas','height=0,width=0,status=yes,titlebar=yes,scrollbars=no,resizable=yes,menubar=no,toolbar=no');</script>";
                //ClientScript.RegisterStartupScript(Page.GetType(), "Imprime", FocusScript);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ReporteVersion", AbreVentana, false);

                if (this.SetIndice != null)
                {
                    this.SetIndice(-1);
                }
                if (this.ActualizaGrid != null)
                {
                    this.ActualizaGrid();
                }
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        private void ActualizarFechaFin(int id_servicio)
        {
            TransporteTerrestre.Datos.CatServicios servicios = TransporteTerrestre.Datos.CatServicios.FetchByID(id_servicio);
            pcldFinVigencia.To.Date = pcldInicioVigencia.DateValue.AddDays(servicios.DiasCandado - 1);
        }

        public void SoloLecturaEditar()
        {
            try
            {
                txtSolicitante.Enabled = false;
                TxtPrograma.Enabled = false;
                TxtPosicion.Enabled = false;
                //txtObservaciones.Enabled = false;
                txtNombreSolicitante.Enabled = false;
                txtNombreAutorizador.Enabled = false;
                txtInicioVigencia.Enabled = false;
                TxtFondo.Enabled = false;
                txtFinVigencia.Enabled = false;
                TxtEpep.Enabled = false;
                TxtCentroGestor.Enabled = false;
                txtAutorizador.Enabled = false;
                lbtnAutorizador.Enabled = false;
                //lbtnGuardar.Enabled = false;
                lbtnSolicitante.Enabled = false;
                pcldFinVigencia.Enabled = false;
                pcldInicioVigencia.Enabled = false;
                ddlServicios.Enabled = false;
                rbtnDisposicion.Enabled = false;
                rbtnFijo.Enabled = false;
                TxtOrigen.Enabled = false;
                TxtDestino.Enabled = false;
                ddlContratos.Enabled = false;
                lbtnGuardar.Enabled = true;
                lbtnPasajeros.Enabled = true;
            }
            catch (System.Exception ex)
            {
                if (this.Mensaje != null)
                {
                    this.Mensaje(ex.Message, "ERROR");
                }
            }
        }

        public void InsertarPasajerosIda(TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection Pasajeros, TransporteTerrestre.Datos.TerrestreSolicitud EditarSolicitud, Models.UserLogon usuario_logueado)
        {
            //int ii = 0;

            for (int jj = 0; jj < Pasajeros.ColeccionPasajerosDia.Count; jj++)
            {
                //ii = ii + 1;
                TransporteTerrestre.Datos.TerrestreSolicitudDiaCollection SolicitudDia = new SubSonic.Select()
                .From<TransporteTerrestre.Datos.TerrestreSolicitudDia>()
                .Where(TransporteTerrestre.Datos.TerrestreSolicitudDia.FkSolicitudTerrestreColumn)
                 .IsEqualTo(EditarSolicitud.IdSolicitudTerrestre)
                 .And(TransporteTerrestre.Datos.TerrestreSolicitudDia.CveDiaColumn)
                 .IsEqualTo(Pasajeros.ColeccionPasajerosDia[jj].CveDia)
                 .And(TransporteTerrestre.Datos.TerrestreSolicitudDia.MovimientoColumn)
                 .IsEqualTo("VIAJE")
                 .And(TransporteTerrestre.Datos.TerrestreSolicitudDia.BorradoLogicoColumn)
                 .IsEqualTo(false)
                 .And(TransporteTerrestre.Datos.TerrestreSolicitudDia.NodoColumn)
                 .IsEqualTo(usuario_logueado.NodoAtencionActual)
                 .ExecuteAsCollection<TransporteTerrestre.Datos.TerrestreSolicitudDiaCollection>();

                for (int ff = 0; ff < Pasajeros.ColeccionPasajerosDia[jj].Pasajeros.Count; ff++)
                {
                    TransporteTerrestre.Datos.TerrestrePasajerosCollection IngresaPasajero = new SubSonic.Select()
                   .From<TransporteTerrestre.Datos.TerrestrePasajeros>()
                   .Where(TransporteTerrestre.Datos.TerrestrePasajeros.FkIdSolicitudColumn)
                   .IsEqualTo(EditarSolicitud.IdSolicitudTerrestre)
                   .And(TransporteTerrestre.Datos.TerrestrePasajeros.FkSolicitudDiaColumn)
                   .IsEqualTo(SolicitudDia[0].IdSolicitudDia)
                   .And(TransporteTerrestre.Datos.TerrestrePasajeros.FichaColumn)
                   .IsEqualTo(Pasajeros.ColeccionPasajerosDia[jj].Pasajeros[ff].Ficha)
                   .And(TransporteTerrestre.Datos.TerrestrePasajeros.NodoColumn)
                   .IsEqualTo(usuario_logueado.NodoAtencionActual)
                   .ExecuteAsCollection<TransporteTerrestre.Datos.TerrestrePasajerosCollection>();

                    if (IngresaPasajero.Count == 0)
                    {
                        TransporteTerrestre.Datos.TerrestrePasajeros NuevoPasajero = null;
                        NuevoPasajero = new TransporteTerrestre.Datos.TerrestrePasajeros();

                        NuevoPasajero.BorradoLogico = false;
                        NuevoPasajero.Ficha = Pasajeros.ColeccionPasajerosDia[jj].Pasajeros[ff].Ficha;
                        NuevoPasajero.FkSolicitudDia = SolicitudDia[0].IdSolicitudDia;
                        NuevoPasajero.Nombre = Pasajeros.ColeccionPasajerosDia[jj].Pasajeros[ff].Nombre;
                        NuevoPasajero.Status = "SOLICITADO";
                        NuevoPasajero.Tipo = Pasajeros.ColeccionPasajerosDia[jj].Pasajeros[ff].Tipo;
                        NuevoPasajero.FkIdSolicitud = (EditarSolicitud.IdSolicitudTerrestre);
                        NuevoPasajero.Nodo = usuario_logueado.NodoAtencionActual;
                        NuevoPasajero.Cobro = false;
                        NuevoPasajero.Save();
                    }
                }
            } 
        }

        public void InsertarPasajerosRetorno(TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection PasajerosRetorno, TransporteTerrestre.Datos.TerrestreSolicitud EditarSolicitud, Models.UserLogon usuario_logueado)
        {
            //int ii = 0;

            for (int jj = 0; jj < PasajerosRetorno.ColeccionPasajerosDia.Count; jj++)
            {
                //ii = ii + 1;
                TransporteTerrestre.Datos.TerrestreSolicitudDiaCollection SolicitudDia = new SubSonic.Select()
                .From<TransporteTerrestre.Datos.TerrestreSolicitudDia>()
                .Where(TransporteTerrestre.Datos.TerrestreSolicitudDia.FkSolicitudTerrestreColumn)
                 .IsEqualTo(EditarSolicitud.IdSolicitudTerrestre)
                 .And(TransporteTerrestre.Datos.TerrestreSolicitudDia.CveDiaColumn)
                 .IsEqualTo(PasajerosRetorno.ColeccionPasajerosDia[jj].CveDia)
                 .And(TransporteTerrestre.Datos.TerrestreSolicitudDia.MovimientoColumn)
                 .IsEqualTo("RETORNO")
                 .And(TransporteTerrestre.Datos.TerrestreSolicitudDia.BorradoLogicoColumn)
                 .IsEqualTo(false)
                 .And(TransporteTerrestre.Datos.TerrestreSolicitudDia.NodoColumn)
                 .IsEqualTo(usuario_logueado.NodoAtencionActual)
                 .ExecuteAsCollection<TransporteTerrestre.Datos.TerrestreSolicitudDiaCollection>();

                for (int ff = 0; ff < PasajerosRetorno.ColeccionPasajerosDia[jj].Pasajeros.Count; ff++)
                {
                    TransporteTerrestre.Datos.TerrestrePasajerosCollection IngresaPasajero = new SubSonic.Select()
                   .From<TransporteTerrestre.Datos.TerrestrePasajeros>()
                   .Where(TransporteTerrestre.Datos.TerrestrePasajeros.FkIdSolicitudColumn)
                   .IsEqualTo(EditarSolicitud.IdSolicitudTerrestre)
                   .And(TransporteTerrestre.Datos.TerrestrePasajeros.FkSolicitudDiaColumn)
                   .IsEqualTo(SolicitudDia[0].IdSolicitudDia)
                   .And(TransporteTerrestre.Datos.TerrestrePasajeros.FichaColumn)
                   .IsEqualTo(PasajerosRetorno.ColeccionPasajerosDia[jj].Pasajeros[ff].Ficha)
                   .And(TransporteTerrestre.Datos.TerrestrePasajeros.NodoColumn)
                   .IsEqualTo(usuario_logueado.NodoAtencionActual)
                   .ExecuteAsCollection<TransporteTerrestre.Datos.TerrestrePasajerosCollection>();

                    if (IngresaPasajero.Count == 0)
                    {
                        TransporteTerrestre.Datos.TerrestrePasajeros NuevoPasajero = null;
                        NuevoPasajero = new TransporteTerrestre.Datos.TerrestrePasajeros();

                        NuevoPasajero.BorradoLogico = false;
                        NuevoPasajero.Ficha = PasajerosRetorno.ColeccionPasajerosDia[jj].Pasajeros[ff].Ficha;
                        NuevoPasajero.FkSolicitudDia = SolicitudDia[0].IdSolicitudDia;
                        NuevoPasajero.Nombre = PasajerosRetorno.ColeccionPasajerosDia[jj].Pasajeros[ff].Nombre;
                        NuevoPasajero.Status = "SOLICITADO";
                        NuevoPasajero.Tipo = PasajerosRetorno.ColeccionPasajerosDia[jj].Pasajeros[ff].Tipo;
                        NuevoPasajero.FkIdSolicitud = (EditarSolicitud.IdSolicitudTerrestre);
                        NuevoPasajero.Nodo = usuario_logueado.NodoAtencionActual;
                        NuevoPasajero.Cobro = false;
                        NuevoPasajero.Save();
                    }
                }
            }
        }
    }
}