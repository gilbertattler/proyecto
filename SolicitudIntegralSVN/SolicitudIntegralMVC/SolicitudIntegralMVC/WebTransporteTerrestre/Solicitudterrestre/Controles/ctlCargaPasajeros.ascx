﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlCargaPasajeros.ascx.cs" Inherits="SolicitudIntegralMVC.WebTransporteTerrestre.Solicitudterrestre.Controles.ctlCargaPasajeros" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register Assembly="RJS.Web.WebControl.PopCalendar.Net.2008" Namespace="RJS.Web.WebControl" TagPrefix="rjs" %>
<%@ Register Src="~/WebTransporteTerrestre/Solicitudterrestre/Controles/ctlGridPasajeros.ascx" TagPrefix="uc1" TagName="ctlGridPasajeros" %>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>

        <div class="span-20 last FontPopUp">

            <div class="span-20 last">
                
                <div class="span-9 last titulo-formulario">
                    <div class="xsnazzy blue-box">
                        <b class="xtop"><b class="xb1"></b><b class="xb2"></b><b class="xb3"></b><b class="xb4">
                        </b></b>
                        <h4>
                            <label id="lblDatosGenerales">
                                Datos Solicitud Por Dia</label>
                        </h4>
                        <b class="xbottom"><b class="xb4"></b><b class="xb3"></b><b class="xb2"></b><b class="xb1">
                        </b></b>
                    </div>
                </div>

                <div class="span-11 last">
                    <div class="prefix-2 span-1 ">
                        <asp:Label ID="Label2" class="etiqueta-formulario" runat="server" Text="Dia"></asp:Label>
                    </div>
                    <div class="span-1">
                        <asp:TextBox ID="txtDia" class="texbox-max-width align-center" runat="server" AutoPostBack="True" CausesValidation="True" OnTextChanged="txtDia_TextChanged">1</asp:TextBox>
                    </div>
                    <div class="span-7 last">
                        <asp:TextBox ID="txtSlider" CssClass="texbox-max-width" runat="server" AutoPostBack="True" CausesValidation="True" OnTextChanged="txtSlider_TextChanged">1</asp:TextBox>
                        <asp:SliderExtender ID="sldextDias" runat="server" BoundControlID="txtDia" EnableHandleAnimation="True" Minimum="1" Steps="0" TargetControlID="txtSlider"></asp:SliderExtender>
                    </div>
                </div>

            </div>

            <div class="span-20 last">
                
                <div class="span-10 last">
                
                    <div class="span-10">
                        <div class="span-3 align-right">
                            <asp:Label ID="lblInicioServicio" class="etiqueta-formulario" runat="server" Text="InicioServicio"></asp:Label>
                        </div>
                        <div class="span-2">
                            <asp:TextBox ID="txtInicioServicio" class="texbox-max-width" runat="server" ReadOnly="True"></asp:TextBox>
                        </div>
                        <div class="span-2">
                            <asp:TextBox ID="txtInicioHora" class="texbox-max-width" runat="server" ReadOnly="True"></asp:TextBox>
                            <asp:MaskedEditExtender ID="txtInicioHora_MaskedEditExtender" runat="server" CultureAMPMPlaceholder=""
                                CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder=""
                                CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder=""
                                Enabled="True" Mask="99:99" MaskType="Time" TargetControlID="txtInicioHora" UserTimeFormat="TwentyFourHour">
                            </asp:MaskedEditExtender>
                        </div>
                        <div class="span-3 last align-left">
                            <rjs:PopCalendar ID="pcldInicioServicio" runat="server" AutoPostBack="False" 
                                Control="txtInicioServicio" Separator="/" Visible="False" />
                        </div>
                    </div>

                    <div class="span-10">
                        <div class="span-3 align-right">
                            <asp:Label ID="lblFinServicio" class="etiqueta-formulario" runat="server" Text="FinServicio"></asp:Label>
                        </div>
                        <div class="span-2">
                            <asp:TextBox ID="txtFinServicio" class="texbox-max-width" runat="server" ReadOnly="True"></asp:TextBox>
                        </div>
                        <div class="span-2">
                            <asp:TextBox ID="txtFinHora" class="texbox-max-width" runat="server" ReadOnly="True"></asp:TextBox>
                            <asp:MaskedEditExtender ID="txtFinHora_MaskedEditExtender" runat="server" CultureAMPMPlaceholder=""
                                CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder=""
                                CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder=""
                                Enabled="True" Mask="99:99" MaskType="Time" TargetControlID="txtFinHora" UserTimeFormat="TwentyFourHour">
                            </asp:MaskedEditExtender>
                        </div>
                        <div class="span-3 last align-left">
                            <rjs:PopCalendar ID="pcldFinServicio" runat="server" AutoPostBack="True" 
                                Control="txtFinServicio" Separator="/" Visible="False" />
                            <asp:ImageButton ID="ibtnCargaPasajeros" runat="server" 
                                OnClick="ibtnCargaPasajeros_Click" 
                                ImageUrl="~/WebTransporteTerrestre/Recursos/img/IconosHomologados24px/ConfirmarOperacion.png" 
                                ToolTip="Confirmar Cambios" Width="24px" Visible="False" />
                        </div>
                    </div>
                
                </div>

                <div class="span-10 last">
                        
                    <div class="span-10 last">
	                    <div class="span-2 align-right">
		                    <asp:Label ID="lblObservaciones" class="etiqueta-formulario" runat="server" Text="Obser."></asp:Label>
	                    </div>
	                    <div class="span-8 last">
		                    <asp:TextBox ID="txtObservaciones" class="texbox-max-width" runat="server" TextMode="MultiLine" AutoPostBack="True"></asp:TextBox>
	                    </div>
                    </div>

                </div>

            </div>

            <div class="span-20 last">
                
                <div class="span-9 last">
                    
                    <div class="span-9 last titulo-formulario">
                        <div class="xsnazzy blue-box">
                            <b class="xtop"><b class="xb1"></b><b class="xb2"></b><b class="xb3"></b><b class="xb4">
                            </b></b>
                            <h4>
                                <label id="Label6">
                                    Datos Pasajeros</label>
                            </h4>
                            <b class="xbottom"><b class="xb4"></b><b class="xb3"></b><b class="xb2"></b><b class="xb1">
                            </b></b>
                        </div>
                    </div>
                    
                    <!--  -->

                    <div class="span-9 last">

                        <div class="span-1">
                            <asp:Label ID="lblFicha" class="etiqueta-formulario" runat="server" Text="Ficha"></asp:Label>
                        </div>
                        <div class="span-3">
                            <asp:TextBox ID="txtFicha" runat="server" CssClass="texbox-max-width"></asp:TextBox>
                        </div>
                        <div class="span-3">
                            <asp:ImageButton ID="lbtnFicha" runat="server" OnClick="lbtnFicha_Click" ImageUrl="~/WebTransporteTerrestre/Recursos/img/IconosHomologados24px/BusquedaEnabled.png" ToolTip="Buscar Ficha"></asp:ImageButton>
                            <asp:ImageButton ID="lbtnAgregar" runat="server" OnClick="lbtnAgregar_Click" ImageUrl="~/WebTransporteTerrestre/Recursos/img/IconosHomologados24px/AgregarPasajeroEnabled.png" ToolTip="Agregar Pasajero"></asp:ImageButton>
                            <asp:ImageButton ID="lbtnAgregarRetorno" runat="server" OnClick="lbtnAgregarRetorno_Click" imageUrl="~/WebTransporteTerrestre/Recursos/img/IconosHomologados24px/Pasajero Retorno.png" ToolTip="Agregar Relevo"></asp:ImageButton>
                        </div>

                        <div class="span-2 last align-left">
                            <asp:CheckBox ID="chkDiasSubsecuentes" runat="server" Text="Subsec." ToolTip="Dar de alta en los dias subsecuentes" />
                        </div>
                    
                    </div>


                    <!-- -->

                    <div class="span-1 align-right">
                        <asp:Label ID="lblNombre" class="etiqueta-formulario" runat="server" Text="Nom."></asp:Label>
                    </div>
                    <div class="span-7 suffix-1 last">
                        <asp:TextBox ID="txtNombre" class="texbox-max-width" runat="server" ReadOnly="True"></asp:TextBox>
                    </div>

                    <!-- -->

                    <div class="span-1 align-right">
                        <asp:Label ID="lblTipo" class="etiqueta-formulario" runat="server" Text="Tip."></asp:Label>
                    </div>
                    <div class="span-5 suffix-3 last ">
                        <asp:TextBox ID="txtTipo" class="texbox-max-width" runat="server" ReadOnly="True"></asp:TextBox>
                    </div>

                </div>
                
                <!--  -->
            
                <div class="span-11 last">
                    
                    <div class="span-11 last titulo-formulario">
                        <div class="xsnazzy blue-box">
                            <b class="xtop"><b class="xb1"></b><b class="xb2"></b><b class="xb3"></b><b class="xb4">
                            </b></b>
                                <h4><label id="Label7">Accesos Directos</label></h4>
                            <b class="xbottom"><b class="xb4"></b><b class="xb3"></b><b class="xb2"></b><b class="xb1">
                            </b></b>
                        </div>
                    </div>
                    
                    <!-- -->
                    
                    <div class="span-2 align-right">
                        <asp:Label ID="lblSolicitudPrevia" class="etiqueta-formulario" runat="server" Text="Sol. Prev."></asp:Label>
                    </div>
                    
                    <div class="span-3">
                        <asp:TextBox ID="txtSolicitudPrevia" class="texbox-max-width" runat="server"></asp:TextBox>
                    </div>

                    <div class="span-2">
                        <asp:ImageButton ID="IbtnVentanaSolicitudPrevia" runat="server" ImageUrl="~/WebTransporteTerrestre/Recursos/img/IconosHomologados24px/VerSolAnterioresEnabled.png" OnClick="IbtnVentanaSolicitudPrevia_Click" ToolTip="Ver Solicitudes Anteriores" />
                        <asp:ImageButton ID="lbtnSolicitudPrevia" runat="server" ImageUrl="~/WebTransporteTerrestre/Recursos/img/IconosHomologados24px/CargarSolPreEnabled.png" ToolTip="Carga Solicitud Previa" OnClick="lbtnSolicitudPrevia_Click"></asp:ImageButton>
                    </div>

                    <div class="span-2">
                        <asp:Label ID="Label1" class="etiqueta-formulario" runat="server" Text="Heredar" Visible="False"></asp:Label>
                    </div>

                    <div class="span-2 last">
                        <asp:ImageButton ID="lbtnHeredarViaje" runat="server" OnClick="lbtnHeredarViaje_Click" ImageUrl="~/WebTransporteTerrestre/Recursos/img/IconosHomologados24px/HeredarDiaEnabled.png" ToolTip="Heredar Dia" Visible="False"></asp:ImageButton>
                        <asp:ImageButton ID="lbtnTodosDias" runat="server" OnClick="lbtnTodosDias_Click" ImageUrl="~/WebTransporteTerrestre/Recursos/img/IconosHomologados24px/HeredarTodosEnabled.png" ToolTip="Heredar Dias" Visible="False"></asp:ImageButton>
                    </div>

                    <!-- -->
                    
                    <div class="span-9">
                        <asp:FileUpload ID="fupExcel" runat="server" class="texbox-max-width" Visible ="true" />
                    </div>
                    <div class="span-2 last align-left">
                        <asp:ImageButton ID="lbtnCargaExcel" runat="server" ImageUrl="~/WebTransporteTerrestre/Recursos/img/IconosHomologados24px/Excel.png" ToolTip="Cargar Excel" OnClick="lbtnCargaExcel_Click" Visible="true"></asp:ImageButton>
                    </div>

                    <!-- -->

                    <asp:Panel ID="pnlCases" runat="server">

                        <div class="span-11 last">
                            <div class="span-1">
                                <asp:Label ID="lblCveCurso" class="etiqueta-formulario" runat="server" 
                                    Text="Curso" Visible="False"></asp:Label>
                            </div>
                            <div class="span-2">
                                
                            </div>
                        
                            <div class="span-1">
                                <asp:Label ID="lblNoSap" class="etiqueta-formulario" runat="server" 
                                    Text="N.Sap" Visible="False"></asp:Label>
                            </div>
                            <div class="span-2">
                                <asp:TextBox ID="txtNoSapCurso" class="texbox-max-width" runat="server" 
                                    Visible="False"></asp:TextBox> 
                            </div>
                            <div class="span-1">
                                
                            </div>
                            <div class="span-3">
                                
                            </div>
                            <div class="span-1 last">
                                <asp:ImageButton ID="imgbAgregarPasajerosCursos" runat="server" 
                                    ImageUrl="~/WebTransporteTerrestre/Recursos/img/IconosHomologados24px/AgregarPasajeroEnabled.png" 
                                    ToolTip="Agregar pasajeros curso" Visible="False" Enabled="True" 
                                    onclick="imgbAgregarPasajerosCursos_Click"></asp:ImageButton>
                            </div>

                        </div>

                    </asp:Panel>
              
                </div>
            
            </div>
            
            <div class="span-20 last">
                <asp:Panel ID="PnlSolicitudesAnteriores" class="span-20 last small" runat="server">
                    <asp:GridView ID="GvSolicitudesPrevias" class="grid mGrid" runat="server" 
                        AutoGenerateColumns="False" 
                        onselectedindexchanged="GvSolicitudesPrevias_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="IBtnSeleccionar" runat="server" CommandName="Select" 
                                        ImageUrl="~/WebTransporteTerrestre/Recursos/img/IconosHomologados16px/SeleccionarEnabled.png" 
                                        ToolTip="Seleccionar" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Folio">
                                <ItemTemplate>
                                    <asp:Label ID="LblFolio" runat="server" Text='<%# Eval("Folio") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Servicio">
                                <ItemTemplate>
                                    <asp:Label ID="LblServicio" runat="server" Text='<%# Eval("CveServicio") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Inicio">
                                <ItemTemplate>
                                    <asp:Label ID="LblInicio" runat="server" Text='<%# Eval("InicioVigencia","{0:d}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Fin">
                                <ItemTemplate>
                                    <asp:Label ID="Label12" runat="server" Text='<%# Eval("FinVigencia","{0:d}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <SelectedRowStyle CssClass="sel" />
                        <AlternatingRowStyle CssClass="alt" />
                    </asp:GridView>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server" 
                    PopupControlID="PnlSolicitudesAnteriores" TargetControlID="Button1">
                </asp:ModalPopupExtender>
            </div>
            
            <div class="span-20  last titulo-formulario">
                <div class="xsnazzy blue-box">
                    <b class="xtop"><b class="xb1"></b><b class="xb2"></b><b class="xb3"></b><b class="xb4">
                    </b></b>
                        <h4><label id="Label5">Pasajeros</label></h4>
                    <b class="xbottom"><b class="xb4"></b><b class="xb3"></b><b class="xb2"></b><b class="xb1">
                    </b></b>
                </div>
            </div>

            <div class="span-20 last align-center small">
                <uc1:ctlGridPasajeros runat="server" id="ctlGridPasajerosViaje" />
            </div>
            <div class="span-20 last">
                <asp:Label ID="Label3" runat="server" class="align-center etiqueta-formulario" 
                    Text="Retorno / Relevos"></asp:Label>
            </div>
            <div class="span-20 last align-center small">
                <uc1:ctlGridPasajeros ID="ctlGridPasajerosRetorno" runat="server" />
                <asp:Button ID="Button1" runat="server" CssClass="oculto" BackColor="White" 
                    BorderStyle="None" Enabled="False" ForeColor="White" />
            </div>
            <asp:Panel ID="PnlAnexos" CssClass="span-20 last" runat="server" Visible="False">
                <div class="span-20 last">
                    <div class="span-20 last titulo-formulario">
                        <div class="xsnazzy blue-box">
                            <b class="xtop"><b class="xb1"></b><b class="xb2"></b><b class="xb3"></b><b class="xb4">
                            </b></b>
                            <h4>
                                <label id="Label10">
                                    Anexos</label>
                            </h4>
                            <b class="xbottom"><b class="xb4"></b><b class="xb3"></b><b class="xb2"></b><b class="xb1">
                            </b></b>
                        </div>
                    </div>
                </div>
                <div class="span-20 last align-center">
                    <div class="span-10">
                        <div class="span-10 last">
                            <div class="span-3">
                                <asp:Label ID="Label4" runat="server" Text="Ficha Cambio" class="etiqueta-formulario"></asp:Label>
                            </div>
                            <div class="span-5 last">
                                <asp:TextBox ID="TxtNuevaFicha" runat="server" class="texbox-max-width"></asp:TextBox>
                            </div>
                            <div class="span-1 last">
                                <asp:ImageButton ID="IBtnFichaAnexo" runat="server" ImageUrl="~/WebTransporteTerrestre/Recursos/img/IconosHomologados24px/BuscarEnabled.png"
                                    OnClick="IBtnFichaAnexo_Click" ToolTip="Buscar" />
                            </div>
                        </div>
                        <div class="span-10 last">
                            <div class="span-10 last">
                                <div class="span-3">
                                    <asp:Label ID="Label8" runat="server" Text="Nombre Cambio" class="etiqueta-formulario"></asp:Label>
                                </div>
                                <div class="span-7 last">
                                    <asp:TextBox ID="TxtNuevoNombre" runat="server" class="texbox-max-width"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="span-10 last">
                            <div class="span-10 last">
                                <div class="span-3">
                                    <asp:Label ID="Label11" runat="server" Text="Tipo Cambio" class="etiqueta-formulario"></asp:Label>
                                </div>
                                <div class="span-7 last">
                                    <asp:TextBox ID="TxtNuevoTipo" runat="server" class="texbox-max-width"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span-10 last">
                        <div class="span-3">
                            <asp:Label ID="Label9" runat="server" Text="Observaciones" class="etiqueta-formulario"></asp:Label>
                        </div>
                        <div class="span-7 last">
                            <asp:TextBox ID="TxtObservacionesAnexo" runat="server" class="texbox-max-width" TextMode="MultiLine"></asp:TextBox>
                        </div>
                        <div class="span-10 last">
                            <asp:ImageButton ID="IBtnAnexos" runat="server" ImageUrl="~/WebTransporteTerrestre/Recursos/img/IconosHomologados24px/ConfirmarOperacion.png"
                                OnClick="IBtnAnexos_Click" ToolTip="Confirmar Anexo" Visible="False" />
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="lbtnCargaExcel"></asp:PostBackTrigger>
    </Triggers>
</asp:UpdatePanel>