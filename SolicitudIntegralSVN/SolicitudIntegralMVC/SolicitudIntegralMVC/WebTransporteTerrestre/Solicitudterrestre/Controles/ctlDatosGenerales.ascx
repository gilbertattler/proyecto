﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlDatosGenerales.ascx.cs" Inherits="SolicitudIntegralMVC.WebTransporteTerrestre.Solicitudterrestre.Controles.ctlDatosGenerales" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="RJS.Web.WebControl.PopCalendar.Net.2008" Namespace="RJS.Web.WebControl" TagPrefix="rjs" %>

<div class="span-9 last titulo-formulario">
    <div class="xsnazzy blue-box">
        <b class="xtop"><b class="xb1"></b><b class="xb2"></b><b class="xb3"></b><b class="xb4"></b></b>
        <h4>
            <label id="lblDatosGenerales">
                Datos generales de la solicitud terrestre</label>
        </h4>
        <b class="xbottom"><b class="xb4"></b><b class="xb3"></b><b class="xb2"></b><b class="xb1"></b></b>
    </div>
</div>
<div class="span-20 last">
    <div class="span-10">
    </div>
    <div class="span-10 last">
        <div class="span-3 align-right">
            <asp:Label ID="LblAnexos" runat="server" Text="Anexos" Visible="False"></asp:Label>
        </div>
        <div class="span-6">
            <asp:DropDownList ID="DdlAnexos" runat="server" Visible="False">
            </asp:DropDownList>
        </div>
        <div class="span-1 last">
            <asp:ImageButton ID="IBtnImprimirAnexo" runat="server" OnClick="IBtnImprimirAnexo_Click" Visible="False" ImageUrl="~/WebTransporteTerrestre/Recursos/img/IconosHomologados24px/ImprimirEnabled.png" ToolTip="Imprimir" />
        </div>
    </div>
</div>

<div class="span-16 last">
    <div class="span-10">
        <div class="span-4 align-right">
            <asp:RadioButton ID="rbtnDisposicion" class="etiqueta-formulario" runat="server" Text="Disposicion" GroupName="TipoServicio" AutoPostBack="True" CausesValidation="True" OnCheckedChanged="rbtnDisposicion_CheckedChanged" />
        </div>
        <div class="span-4 align-left">
            <asp:RadioButton ID="rbtnFijo" class="etiqueta-formulario" runat="server" Text="Fijo" GroupName="TipoServicio" AutoPostBack="True" CausesValidation="True" OnCheckedChanged="rbtnFijo_CheckedChanged" Checked="True" />
        </div>
        <div class="span-2 last">
        </div>
    </div>
    <div class="span-6 last">
    </div>
</div>


<div class="span-20 last">
    <div class="span-10">
        <div class="span-4 align-right">
            <asp:Label ID="lblServicio" class="etiqueta-formulario" runat="server" Text="Servicio"></asp:Label>
        </div>
        <div class="span-6 last">
            <asp:DropDownList ID="ddlServicios" class="texbox-max-width" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlServicios_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
    </div>
    <div class="span-10 last align-right">
        <div class="span-5">
            <div class="span-3">
                <asp:Label ID="Label7" class="etiqueta-formulario" runat="server" Text="Origen"></asp:Label>
            </div>
            <div class="span-2 last">
                <asp:TextBox ID="TxtOrigen" class="texbox-max-width" runat="server" 
                    ReadOnly="True" Width="135px"></asp:TextBox>
            </div>
        </div>
        <div class="span-5 last">
            <div class="span-3">
                <asp:Label ID="Label8" class="etiqueta-formulario" runat="server" Text="Destino"></asp:Label>
            </div>
            <div class="span-2 last">
                <asp:TextBox ID="TxtDestino" class="texbox-max-width" runat="server" 
                    ReadOnly="True" Width="135px"></asp:TextBox>
            </div>
        </div>
    </div>
</div>
<div class="span-20 last">
    <div class="span-10">
        <div class="span-4 align-right">
            <asp:Label ID="lblInicioVigencia" runat="server" class="etiqueta-formulario" Text="Inicio"></asp:Label>
        </div>
        <div class="span-5">
            <asp:TextBox ID="txtInicioVigencia" class="texbox-max-width" runat="server" 
                ReadOnly="True"></asp:TextBox>
        </div>
        <div class="span-1 last">
            <rjs:PopCalendar ID="pcldInicioVigencia" runat="server" 
                Control="txtInicioVigencia" AutoPostBack="True" 
                OnSelectionChanged="pcldInicioVigencia_SelectionChanged" Separator="/" />
        </div>
    </div>
    <div class="span-11 last">
        <div class="span-3 align-right">
            <asp:Label ID="LblImprimir" runat="server" class="etiqueta-formulario" Text="Imprimir Dia" Visible="False"></asp:Label>
        </div>
        <div class="span-5">
            <asp:TextBox ID="TxtDia" class="texbox-max-width" runat="server" 
                Visible="False" ReadOnly="True"></asp:TextBox>
        </div>
        <div class="span-1">
            <rjs:PopCalendar ID="pclImprimir" runat="server" Control="TxtDia" 
                Separator="/" />
            <%--<cc1:SliderExtender ID="sldextDias" runat="server" BoundControlID="txtDia" EnableHandleAnimation="True"
                    HandleImageUrl="~/WebTransporteTerrestre/Recursos/img/icons/email.png" Minimum="1" Steps="100" TargetControlID="txtSlider">
                </cc1:SliderExtender>
            --%>
        </div>
        <div class="span-1 last">
            <asp:ImageButton ID="IBtImprimir" runat="server" OnClick="IBtImprimir_Click" 
                Visible="False" 
                ImageUrl="~/WebTransporteTerrestre/Recursos/img/IconosHomologados24px/ImprimirEnabled.png" 
                ToolTip="Imprimir Solicitud del día" />
            <asp:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" ConfirmText="Al imprimir esta solicitud se confirmara automaticamente" TargetControlID="IBtImprimir">
            </asp:ConfirmButtonExtender>
        </div>
        <div class="span-1 last">
            <asp:ImageButton ID="IBtImprimirBoleto" runat="server" Visible="False" 
                Width="24px" Height="24px"
                ImageUrl="~/WebTransporteTerrestre/Recursos/img/boleto.jpg" ToolTip="Imprimir boletos del día" 
                onclick="IBtImprimirBoleto_Click" />
        </div>
        
    </div>
</div>
<div class="span-20 last">
    <div class="span-10">
        <div class="span-4 align-right">
            <asp:Label ID="lblFinVigencia" runat="server" class="etiqueta-formulario" Text="Fin"></asp:Label>
        </div>
        <div class="span-5">
            <asp:TextBox ID="txtFinVigencia" class="texbox-max-width" runat="server" 
                ReadOnly="True"></asp:TextBox>
        </div>
        <div class="span-1 last">
            <rjs:PopCalendar ID="pcldFinVigencia" runat="server" Control="txtFinVigencia" 
                AutoPostBack="True" OnSelectionChanged="pcldFinVigencia_SelectionChanged" 
                From-Control="txtInicioVigencia" From-Date="" Separator="/" />
        </div>
    </div>
    <div class="span-10 last">
        <div class="span-3 align-right">
            <asp:Label ID="Label6" runat="server" class="etiqueta-formulario" Text="Contrato"></asp:Label>
        </div>
        <div class="span-3">
            <asp:DropDownList ID="ddlContratos" class="etiqueta-formulario texbox-max-width" runat="server">
            </asp:DropDownList>
            <asp:Label ID="lblContratoReadOnly" Text="" Visible="False" runat="server"></asp:Label>
        </div>
        <div class="span-3 last">
        </div>
    </div>
</div>

<div class="span-20 last">
    <div class="span-20 last">
        <div class="span-4 align-right">
            <asp:Label ID="lblSolicitante" class="etiqueta-formulario" runat="server" Text="Solicita"></asp:Label>
        </div>
        <div class="span-5">
            <asp:TextBox ID="txtSolicitante" class="texbox-max-width" runat="server"></asp:TextBox>
        </div>
        <div class="span-1">
            <asp:ImageButton ID="lbtnSolicitante" runat="server" OnClick="lbtnSolicitante_Click" ImageUrl="~/WebTransporteTerrestre/Recursos/img/IconosHomologados24px/BuscarEnabled.png" ToolTip="Buscar"></asp:ImageButton>
        </div>
        <div class="span-10 last">
            <asp:TextBox ID="txtNombreSolicitante" class="texbox-max-width" runat="server" ReadOnly="True"></asp:TextBox>
        </div>
    </div>
</div>
<div class="span-20 last">
    <div class="span-20 last">
        <div class="span-4 align-right">
            <asp:Label ID="lblAutorizador" class="etiqueta-formulario" runat="server" Text="Autoriza"></asp:Label>
        </div>
        <div class="span-5">
            <asp:TextBox ID="txtAutorizador" class="texbox-max-width" runat="server"></asp:TextBox>
        </div>
        <div class="span-1">
            <asp:ImageButton ID="lbtnAutorizador" runat="server" OnClick="lbtnAutorizador_Click" ImageUrl="~/WebTransporteTerrestre/Recursos/img/IconosHomologados24px/BuscarEnabled.png" ToolTip="Buscar"></asp:ImageButton>
        </div>
        <div class="span-10 last">
            <asp:TextBox ID="txtNombreAutorizador" class="texbox-max-width" runat="server" ReadOnly="True"></asp:TextBox>
        </div>
    </div>
    <div class="span-20 last">
        <div class="span-4">
            <div class="span-4 last">
                <asp:Label ID="Label1" class="etiqueta-formulario" runat="server" Text="Programa"></asp:Label>
            </div>
            <div class="span-4 last">
                <asp:TextBox ID="TxtPrograma" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="span-4">
            <div class="span-4 last">
                <asp:Label ID="Label2" class="etiqueta-formulario" runat="server" Text="Fondo"></asp:Label>
            </div>
            <div class="span-4 last">
                <asp:TextBox ID="TxtFondo" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="span-4">
            <div class="span-4 last">
                <asp:Label ID="Label3" class="etiqueta-formulario" runat="server" Text="Centro Gestor"></asp:Label>
            </div>
            <div class="span-4 last">
                <asp:TextBox ID="TxtCentroGestor" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="span-4">
            <div class="span-4 last">
                <asp:Label ID="Label4" class="etiqueta-formulario" runat="server" Text="C. Costos/E. PEP"></asp:Label>
            </div>
            <div class="span-4 last">
                <asp:TextBox ID="TxtEpep" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="span-4 last">
            <div class="span-4 last">
                <asp:Label ID="Label5" class="etiqueta-formulario" runat="server" Text="P. Presupuestaria"></asp:Label>
            </div>
            <div class="span-4 last">
                <asp:TextBox ID="TxtPosicion" runat="server"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="span-20 last small">
        <asp:GridView ID="GvDireccionamientos" class="grid mGrid" runat="server" AutoGenerateColumns="False" OnSelectedIndexChanged="GvDireccionamientos_SelectedIndexChanged" CssClass="mGrid">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:ImageButton ID="IbtnSeleccionar" runat="server" CommandName="Select" ImageUrl="~/WebTransporteTerrestre/Recursos/img/IconosHomologados16px/SeleccionarEnabled.png" ToolTip="Seleccionar" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Programa">
                    <ItemTemplate>
                        <asp:Label ID="LblPrograma" runat="server" Text='<%# Eval("CveProgramaPresupuestal") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Fondo">
                    <ItemTemplate>
                        <asp:Label ID="LblFondo" runat="server" Text='<%# Eval("DescripcionFondo") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Centro Gestor">
                    <ItemTemplate>
                        <asp:Label ID="LblCentroGestor" runat="server" Text='<%# Eval("CveCtoGestor") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="C.Costos/Elemento PEP">
                    <ItemTemplate>
                        <asp:Label ID="LblCostosEPEP" runat="server" Text='<%# Eval("CveFondo").ToString()=="PEF - INVERSION" ? Eval("CveElementoPep") : Eval("CveCtoCostos") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Posición Presupuestaria">
                    <ItemTemplate>
                        <asp:Label ID="LblPosicionPresupuestaria" runat="server" Text='<%# Eval("CvePosicionPresupuestaria") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
</div>
<div class="span-20 last">
    <div class="span-20 last">
        <div class="span-4 align-right">
            <asp:Label ID="lblObservaciones" class="etiqueta-formulario" runat="server" Text="Observaciones"></asp:Label>
        </div>
        <div class="span-16 last">
            <asp:TextBox ID="txtObservaciones" class="texbox-max-width" runat="server" TextMode="MultiLine"></asp:TextBox>
        </div>
    </div>
</div>
<div class="span-20 last">
    <div class="span-20 last">
        <div class="prefix-12 span-8 last align-right">
            <asp:ImageButton ID="lbtnPasajeros" runat="server" OnClick="lbtnPasajeros_Click" ImageUrl="~/WebTransporteTerrestre/Recursos/img/IconosHomologados24px/PasajerosEnabled.png" ToolTip="Cargar Pasajeros"></asp:ImageButton>
            <asp:ImageButton ID="lbtnGuardar" runat="server" OnClick="lbtnGuardar_Click" ImageUrl="~/WebTransporteTerrestre/Recursos/img/IconosHomologados24px/GuardarEnabled.png" ToolTip="Guardar" OnUnload="lbtnGuardar_Unload"></asp:ImageButton>
        </div>
    </div>
</div>
