﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebTransporteTerrestre/Recursos/MasterPage/PaginaMaestra.master" AutoEventWireup="true" CodeBehind="SolicitudTerrestre.aspx.cs" Inherits="SolicitudIntegralMVC.WebTransporteTerrestre.Solicitudterrestre.SolicitudTerrestre" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="Controles/ctlDatosGenerales.ascx" TagName="ctlDatosGenerales" TagPrefix="uc1" %>
<%@ Register Src="../Recursos/Controles/message-box.ascx" TagName="message" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTitulo" runat="Server">
    Creación de Solicitudes 
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphContenido" runat="Server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <asp:Label ID="LblExcel" runat="server" Font-Bold="True" 
        Text="Formato de Excel para cargar pasajeros en archivo plano :"></asp:Label>
    <div class="span-24 last">
        <uc2:message ID="ctlMensaje" runat="server" />
    </div>
    <div class="span-24 last">
        <uc1:ctlDatosGenerales ID="ctlDatosGenerales" runat="server" />
    </div>
    <asp:HyperLink ID="HpLDescarArchivo" runat="server" 
        ImageUrl="~/WebTransporteTerrestre/Recursos/img/IconosHomologados24px/xls_file.png" 
        NavigateUrl="~/WebTransporteTerrestre/Recursos/Archivo_Descarga/Formato para Insertar Pasajeros.xls"></asp:HyperLink>
</asp:Content>
