﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolicitudIntegralMVC.WebTransporteTerrestre.Solicitudterrestre
{
    public partial class Solicitudes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    Models.UserLogon usuario_logueado = Infraestructure.Utilerias.ObtenerUsuarioAutentificado(Page.Session);

                    ctlListaSolicitudes.ObtenerDatosSession(usuario_logueado);
                    ctlListaSolicitudes.FiltraSolicitudes();
                }

                ctlListaSolicitudes.Mensaje += new SolicitudIntegralMVC.WebTransporteTerrestre.Solicitudterrestre.Controles.ctlListaSolicitudes.MensajeEventHandler(ctlListaSolicitudes_Mensaje);
            }
            catch (System.Exception ex)
            {
                ctlMensaje.MostrarMensaje(SolicitudIntegralMVC.WebTransporteTerrestre.Recursos.controles.message_box.MessageBoxClasses.ERROR, "", ex.Message, true);
            }
        }

        protected void ctlListaSolicitudes_Mensaje(string Mensaje, string Tipo)
        {
            try
            {
                if (Tipo == "OK")
                {
                    ctlMensaje.MostrarMensaje(SolicitudIntegralMVC.WebTransporteTerrestre.Recursos.controles.message_box.MessageBoxClasses.SUCCESS, "", Mensaje, true);
                }
                else if (Tipo == "ERROR")
                {
                    ctlMensaje.MostrarMensaje(SolicitudIntegralMVC.WebTransporteTerrestre.Recursos.controles.message_box.MessageBoxClasses.ERROR, "", Mensaje, true);
                }
            }
            catch (System.Exception ex)
            {
                ctlMensaje.MostrarMensaje(SolicitudIntegralMVC.WebTransporteTerrestre.Recursos.controles.message_box.MessageBoxClasses.ERROR, "", ex.Message, true);
            }
        }
    }
}