﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolicitudIntegralMVC.WebTransporteTerrestre.Solicitudterrestre
{
    public partial class CargaPasajeros : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ctlPasajeros.Mensaje += new SolicitudIntegralMVC.WebTransporteTerrestre.Solicitudterrestre.Controles.ctlCargaPasajeros.MensajeEventHandler(ctlPasajeros_Mensaje);
                if (!Page.IsPostBack)
                {
                    if (this.Request.QueryString.Count > 0)
                    {
                        ctlPasajeros.SetSlider(Convert.ToInt32(this.Request.QueryString.Get(0)), 1);
                        if (this.Request.QueryString.Get(1).ToString() == "CONFIRMADO" || this.Request.QueryString.Get(1).ToString() == "ANEXO")
                        {
                            ctlPasajeros.Confirmada();
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                ctlMensaje.MostrarMensaje(SolicitudIntegralMVC.WebTransporteTerrestre.Recursos.controles.message_box.MessageBoxClasses.ERROR, "", ex.Message, true);
            }
        }

        protected void ctlPasajeros_Mensaje(string Mensaje, string Tipo)
        {
            try
            {
                if (Tipo == "OK")
                {
                    ctlMensaje.MostrarMensaje(SolicitudIntegralMVC.WebTransporteTerrestre.Recursos.controles.message_box.MessageBoxClasses.SUCCESS, "", Mensaje, true);
                }
                else if (Tipo == "ERROR")
                {
                    ctlMensaje.MostrarMensaje(SolicitudIntegralMVC.WebTransporteTerrestre.Recursos.controles.message_box.MessageBoxClasses.ERROR, "", Mensaje, true);
                }
            }
            catch (System.Exception ex)
            {
                ctlMensaje.MostrarMensaje(SolicitudIntegralMVC.WebTransporteTerrestre.Recursos.controles.message_box.MessageBoxClasses.ERROR, "", ex.Message, true);
            }
        }
    }
}