﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolicitudIntegralMVC.WebTransporteTerrestre.Solicitudterrestre
{
    public partial class SolicitudTerrestre : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ctlDatosGenerales.Mensaje += new SolicitudIntegralMVC.WebTransporteTerrestre.Solicitudterrestre.Controles.ctlDatosGenerales.MensajeEventHandler(ctlDatosGenerales_Mensaje);
            if (!Page.IsPostBack)
            {
                //TransporteTerrestre.Negocios.Usuarios Usuario = Session["usuario_logueado"] as TransporteTerrestre.Negocios.Usuarios;
                Models.UserLogon usuario_logueado = Infraestructure.Utilerias.ObtenerUsuarioAutentificado(Page.Session);

                Infraestructure.Utilerias.ValidarUsuario(this.Page, usuario_logueado);

                ctlDatosGenerales.ObtenerDatosSession(usuario_logueado);

                string Nodo_Usuario = usuario_logueado.NodoAtencionActual;

                if (Nodo_Usuario == "CME")
                {
                    LblExcel.Visible = true;
                    HpLDescarArchivo.Visible = true;
                }
                else if (Nodo_Usuario == "ZN")
                {
                    LblExcel.Visible = false;
                    HpLDescarArchivo.Visible = false;
                }
            }
        }

        protected void ctlDatosGenerales_Mensaje(string Mensaje, string Tipo)
        {
            try
            {
                if (Tipo == "OK")
                {
                    ctlMensaje.MostrarMensaje(SolicitudIntegralMVC.WebTransporteTerrestre.Recursos.controles.message_box.MessageBoxClasses.SUCCESS, "", Mensaje, true);
                }
                else if (Tipo == "ERROR")
                {
                    ctlMensaje.MostrarMensaje(SolicitudIntegralMVC.WebTransporteTerrestre.Recursos.controles.message_box.MessageBoxClasses.ERROR, "", Mensaje, true);
                }
            }
            catch (System.Exception ex)
            {
                ctlMensaje.MostrarMensaje(SolicitudIntegralMVC.WebTransporteTerrestre.Recursos.controles.message_box.MessageBoxClasses.ERROR, "", ex.Message, true);
            }
        }
    }
}