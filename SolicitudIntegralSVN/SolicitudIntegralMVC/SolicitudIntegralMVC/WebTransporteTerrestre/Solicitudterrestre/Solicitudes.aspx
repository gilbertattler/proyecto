﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebTransporteTerrestre/Recursos/MasterPage/PaginaMaestra.master" AutoEventWireup="true" CodeBehind="Solicitudes.aspx.cs" Inherits="SolicitudIntegralMVC.WebTransporteTerrestre.Solicitudterrestre.Solicitudes" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register Src="~/WebTransporteTerrestre/Recursos/controles/message-box.ascx" TagPrefix="uc1" TagName="messagebox" %>
<%@ Register Src="~/WebTransporteTerrestre/Solicitudterrestre/Controles/ctlListaSolicitudes.ascx" TagPrefix="uc2" TagName="ctlListaSolicitudes" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cphTitulo" runat="server">
    Administración de Solicitudes  
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphContenido" runat="server">
    
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
    
    <div class="span-24 last">    
        <uc1:messagebox runat="server" id="ctlMensaje" />
    </div>
    <div class="span-24 last">
        <uc2:ctlListaSolicitudes runat="server" id="ctlListaSolicitudes" />
    </div>
</asp:Content>
