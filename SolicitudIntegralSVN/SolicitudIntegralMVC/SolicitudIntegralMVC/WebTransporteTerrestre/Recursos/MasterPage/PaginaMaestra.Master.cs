﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolicitudIntegralMVC.WebTransporteTerrestre.Recursos.MasterPage
{
    public partial class PaginaMaestra : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Models.UserLogon usuario_logueado = Infraestructure.Utilerias.ObtenerUsuarioAutentificado(Page.Session);

            //TODO: Hay que reimplementar el validar usuario
            Infraestructure.Utilerias.ValidarUsuario(this.Page, usuario_logueado);

            if (!Page.IsPostBack)
            {
                //TODO: ARREGLAR EL TEXTO DE USUARIOS DE LA MASTER DE TERRESTRE
                LblDatosUsuario.Text = "[" + usuario_logueado.NombreUsuario + "] " + usuario_logueado.NombreCompletoUsuario + "  | Nodo: "  + usuario_logueado.NodoAtencionActual + " | [" + usuario_logueado.SubdivisionActual + "] " + usuario_logueado.DescripcionSubdivisionActual  + " >> " + usuario_logueado.SiglasActivoActual  + " >> " + usuario_logueado.SiglasRegionActual;
                string Nodo_Usuario = usuario_logueado.NodoAtencionActual;
                Session["Nodo_Usuario"] = Nodo_Usuario;
            }
        }
        
        protected void btnCerrarSesion_Click(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            Page.Session.Clear();
            Page.Response.Redirect("~/LogOn/Index");
        }
    }
}