﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VentanaMensajes.ascx.cs" Inherits="SolicitudIntegralMVC.WebTransporteTerrestre.Recursos.controles_internos.VentanaMensajes" %>

<asp:Panel ID="pnlMensaje" CssClass="span-24 last cuadro-mensaje" runat="server"
        HorizontalAlign="Center" Visible="False">

        <div class="span-22">
                    <asp:Timer ID="Timer1" runat="server" Interval="5000" OnTick="Timer1_Tick" 
        Enabled="False">
    </asp:Timer>
            <asp:Label ID="lblMensaje" runat="server" Text=""
                CssClass="etiqueta-mensaje"></asp:Label>
        </div>
        <div class="span-2 last">
            <asp:ImageButton ID="ibtnCerrar" runat="server" OnClick="ibtnCerrar_Click" 
                ImageUrl="~/WebTransporteTerrestre/Recursos/img/IconosHomologados24px/CerrarEnabled.png" 
                ToolTip="Cerrar" />
        </div>
    </asp:Panel> 