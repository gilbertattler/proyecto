﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolicitudIntegralMVC.WebTransporteTerrestre.Recursos.controles_internos
{
    public partial class VentanaMensajes : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void ibtnCerrar_Click(object sender, ImageClickEventArgs e)
        {
            pnlMensaje.Visible = false;
            Timer1.Enabled = false;
        }
        protected void Timer1_Tick(object sender, EventArgs e)
        {
            pnlMensaje.Visible = false;
            Timer1.Enabled = false;
        }
        public void Mensaje(string Mensaje, string Accion)
        {
            switch (Accion)
            {
                case "Ok":
                    pnlMensaje.Visible = true;
                    pnlMensaje.CssClass = "span-24 last cuadro-mensaje ok";
                    lblMensaje.Text = Mensaje;
                    Timer1.Enabled = true;
                    break;
                case "Error":
                    pnlMensaje.Visible = true;
                    pnlMensaje.CssClass = "span-24 last cuadro-mensaje merror";
                    lblMensaje.Text = Mensaje;
                    Timer1.Enabled = true;
                    break;
                default: break;
            }
        
        }
    }
}