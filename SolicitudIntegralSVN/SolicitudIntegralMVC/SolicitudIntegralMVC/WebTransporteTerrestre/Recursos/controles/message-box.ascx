﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="message-box.ascx.cs" Inherits="SolicitudIntegralMVC.WebTransporteTerrestre.Recursos.controles.message_box" %>

<asp:Panel ID="pnlXsnazzyMensaje" runat="server" CssClass="xsnazzy">
   
    <b class="xtop">
        <b class="xb1"></b>
        <b class="xb2"></b>
        <b class="xb3"></b>
        <b class="xb4"></b>
    </b>
    
    <div class="xboxcontent message-box">

        <div class="message-box-titulo">
            <div class="message-box-titulo-mensaje">
                <h3>
                    <asp:Label ID="lblTitulo" runat="server" Text="Título"></asp:Label>
                </h3>
            </div>
            
            <div class="message-box-close-button">
                <asp:LinkButton ID="lbtnCerrar" runat="server" onclick="lbtnCerrar_Click">Cerrar</asp:LinkButton>
	        </div>
        </div>
        
	    <div class="message-box-contenido">
            <asp:Literal ID="ltrContenido" runat="server"></asp:Literal>
	    </div>
    	
    </div>

    <b class="xbottom">
        <b class="xb4"></b>
        <b class="xb3"></b>
        <b class="xb2"></b>
        <b class="xb1"></b>
     </b>

</asp:Panel>