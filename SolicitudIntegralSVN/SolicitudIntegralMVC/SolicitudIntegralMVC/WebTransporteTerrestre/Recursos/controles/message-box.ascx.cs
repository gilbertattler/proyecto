﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolicitudIntegralMVC.WebTransporteTerrestre.Recursos.controles
{
    public partial class message_box : System.Web.UI.UserControl
    {
        public enum MessageBoxClasses
        {
            NINGUNO = 0,
            INFO = 1,
            SUCCESS = 2,
            WARNING = 3,
            ERROR = 4
        }

        #region Propiedades de Acceso

        public string Titulo
        {
            get { return lblTitulo.Text; }

            set
            {
                lblTitulo.Text = value;
                if (value == "")
                {
                    lblTitulo.Visible = false;
                }
                else
                {
                    lblTitulo.Visible = true;
                }
            }
        }

        public MessageBoxClasses TipoMensaje
        {
            get
            {
                MessageBoxClasses resultado = MessageBoxClasses.SUCCESS;

                switch (pnlXsnazzyMensaje.CssClass)
                {
                    case "xsnazzy message-box-info":
                        {
                            resultado = MessageBoxClasses.INFO;
                            break;
                        }

                    case "xsnazzy message-box-error":
                        {
                            resultado = MessageBoxClasses.ERROR;
                            break;
                        }

                    case "xsnazzy message-box-success":
                        {
                            resultado = MessageBoxClasses.SUCCESS;
                            break;
                        }

                    case "xsnazzy message-box-warning":
                        {
                            resultado = MessageBoxClasses.WARNING;
                            break;
                        }
                    default:
                        {
                            resultado = MessageBoxClasses.NINGUNO;
                            break;
                        }
                }

                return resultado;
            }
            set
            {
                switch (value)
                {
                    case MessageBoxClasses.INFO:
                        {
                            pnlXsnazzyMensaje.CssClass = "xsnazzy message-box-info";
                            //pnlMensaje.CssClass = "message-box message-box-info";
                            break;
                        }

                    case MessageBoxClasses.ERROR:
                        {
                            pnlXsnazzyMensaje.CssClass = "xsnazzy message-box-error";
                            //pnlMensaje.CssClass = "message-box message-box-error";
                            break;
                        }

                    case MessageBoxClasses.SUCCESS:
                        {
                            pnlXsnazzyMensaje.CssClass = "xsnazzy message-box-success";
                            //pnlMensaje.CssClass = "message-box message-box-success";
                            break;
                        }

                    case MessageBoxClasses.WARNING:
                        {
                            pnlXsnazzyMensaje.CssClass = "xsnazzy message-box-warning";
                            //pnlMensaje.CssClass = "message-box message-box-warning";
                            break;
                        }
                    default:
                        {
                            pnlXsnazzyMensaje.CssClass = "xsnazzy";
                            //pnlMensaje.CssClass = "message-box";
                            break;
                        }
                }
            }
        }

        public string Contenido
        {
            get { return ltrContenido.Text; }

            set
            {
                ltrContenido.Text = value;
                if (value == "")
                {
                    ltrContenido.Visible = false;
                }
                else
                {
                    ltrContenido.Visible = true;
                }
            }
        }

        public bool EsVisibleBotonCerrar
        {
            get { return lbtnCerrar.Visible; }

            set
            {
                lbtnCerrar.Visible = value;
            }
        }

        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Visible = false;
            }
        }

        protected void lbtnCerrar_Click(object sender, EventArgs e)
        {
            Visible = false;
        }

        public void MostrarMensaje(MessageBoxClasses tipo, string titulo, string contenido, bool es_visible_boton_cerrar)
        {
            Titulo = titulo;
            Contenido = contenido;
            TipoMensaje = tipo;
            EsVisibleBotonCerrar = es_visible_boton_cerrar;
            Visible = true;
        }
    }
}