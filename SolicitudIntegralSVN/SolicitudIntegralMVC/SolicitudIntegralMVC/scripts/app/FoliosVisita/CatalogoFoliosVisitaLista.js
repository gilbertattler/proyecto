﻿define(['jquery',
        'knockout',
        'knockout.mapping',
        'jquery.ui',
        'jquery.ajax.setup',
        'libs/exception.handling',
        'linq',
        'parseDate',
        'domReady!'],
function ($, ko, mapping) {
    return function CatalogoFoliosVisitaLista()
    {
        self = this;
        self.listado_folios = ko.observableArray();

        self.Init = function ()
        {
            $(document).ready(function () 
            {
                $("#txtBuscar").val("");
                self.BusquedaFoliosVisita();
                $("#txtBuscar").focus();
            });
            ko.applyBindings(self);
        }
        $('#txtBuscar').bind('keypress', function (e) {
            if (e.keyCode == 13) {
                self.BusquedaFoliosVisita(e);
            }
        });
        self.BusquedaFoliosVisita = function ()
        {
            $.ajax(rootDir + "FoliosVisita/BusquedaFoliosVisita",
            {
                data: {
                    fraseBusqueda: $("#txtBuscar").val()
                },
                type: "get",
                contentType: "application/json",
                success: function (response, status, xhr)
                {
                    var ex = HandleException(response, status, xhr);
                    if (ex == null) {
                        var mappedTasks = ko.utils.arrayMap(response.result, function (item) { return item });
                        self.listado_folios(mappedTasks);
                    }
                    else {
                        //Error
                        ShowException(ex);
                    }
                }
            });
        };

        self.EditarFolio = function (data) {
            //window.open('/FoliosVisita?data="' + data.IdFolioVisita + '|' + data.Foliodocumento + '|' + data.FolioVisita +'|'+ parseDate(data.FechaInicio) + '|' + parseDate(data.FechaFin) + '|' + data.RfcCompania + '|' + data.NoAcreedorSap + '|' + data.NombreCompania + '|' + data.FichaSolicitante + '|' + data.FichaAutorizador + '|' + data.Motivo+'"');
            self.redirect_by_post('/FoliosVisita/', {
                FolioVisitaData: data.IdFolioVisita + '|' + (data.Foliodocumento == null ? '' : data.Foliodocumento) + '|' + data.FolioVisita + '|' + parseDate(data.FechaInicio) + '|' + parseDate(data.FechaFin) + '|' + (data.RfcCompania == null ? '' : data.RfcCompania) + '|' + (data.NoAcreedorSap == null ? '' : data.NoAcreedorSap) + '|' + (data.NombreCompania == null ? '' : data.NombreCompania) + '|' + data.FichaSolicitante + '|' + data.FichaAutorizador + '|' + (data.Motivo == null ? '' : data.Motivo)
            }, true);
        };


        self.redirect_by_post = function (purl, pparameters, in_new_tab) {
            pparameters = (typeof pparameters == 'undefined') ? {} : pparameters;
            in_new_tab = (typeof in_new_tab == 'undefined') ? true : in_new_tab;

            var form = document.createElement("form");
            $(form).attr("id", "reg-form").attr("name", "reg-form").attr("action", purl).attr("method", "post").attr("enctype", "multipart/form-data");
            if (in_new_tab) {
                $(form).attr("target", "_blank");
            }
            $.each(pparameters, function (key) {
                $(form).append('<input type="text" name="' + key + '" value="' + this + '" />');
            });
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);

            return false;
        };
    }
});