﻿define(['jquery',
        'knockout',
        'knockout.mapping',
        'app/catalogo/catalogo-busqueda-companias',
        'jquery.ui',
        'jquery.ajax.setup',
        'libs/exception.handling',
        'linq',
        'domReady!'],
function ($, ko, mapping, catalogoBusquedaCompanias)
{
    return function CatalogoFoliosVisitaViewModel()
    {
        var self = this;
        //var objFecha = new Date();
        //var DiaActual = objFecha.getDate();
        //var MesActual = objFecha.getMonth()+1;
        //var AnioActual = objFecha.getFullYear();
        //var FechaActual = (DiaActual < 10 ? '0' : '') + DiaActual + '/' + (MesActual < 10 ? '0' : '') + MesActual + '/' + AnioActual;

        self.Operacion = "";
        self.IdFolioVisita = 0;
        self.BorradoLogico = false;
        self.NombreAutorizador = ko.observable();
        self.NombreSolicitante = ko.observable();
        //self.IdFolioVisita = ko.observable();
        self.FolioDocumento = ko.observable();
        self.FolioVisita = ko.observable();
        self.FechaInicio = ko.observable();
        self.FechaFin = ko.observable();
        self.RfcCompania = ko.observable();
        self.NoAcreedorSap = ko.observable();
        self.NombreCompania = ko.observable();
        self.FichaSolicitante = ko.observable();
        self.FichaAutorizador = ko.observable();
        //self.BorradoLogico = ko.observable();
        self.Motivo = ko.observable();

        //self.objFolioVisita = ko.observable();

        self.Init = function ()
        {
            $(document).ready(function ()
            {
                $('#txtFechaInicio').datepicker({
                    defaultDate: "+1w", numberOfMonths: 3, stepMonths: 3, dateFormat: 'dd/mm/yy'
                    //,onSelect: function (selectedDate) {
                    //}
                });

                //$("#txtFechaInicio").datepicker("option", "minDate", 0);
                //$("#txtFechaInicio").val(FechaActual);

                $('#txtFechaFin').datepicker({
                    defaultDate: "+1w", numberOfMonths: 3, stepMonths: 3, dateFormat: 'dd/mm/yy'
                });

                //$("#txtFechaFin").datepicker("option", "minDate", 0);
                
                //$("#txtFechaFin").val(FechaActual);
                //$("#operacion").val('');

                //$("#chkFolioActivo").prop('checked', true);
                $("#frmFolioVisita input,#frmFolioVisita textarea").attr("disabled", true);

                if (parseInt($('input[id=txtIdFolioVisita]').val()) != 0 && $('input[id=txtIdFolioVisita]').val() != null)
                {
                    self.ModoVista();
                }
            });

            ko.applyBindings(self);
        }

        self.ModoVista = function () {
            $("#frmFolioVisita input,#frmFolioVisita textarea").attr("disabled", true);
            $("#btnValidarSolicitante").attr("disabled", true);
            $("#btnValidarAutorizador").attr("disabled", true);
            $("#btnBusquedaCompania").attr("disabled", true);

            $("#btnCancelar").attr("hidden", true);
            $("#btnGuardar").attr("hidden", true);

            $("#btnCancelar").attr("disabled", true);
            $("#btnGuardar").attr("disabled", true);

            $("#btnAniadir").attr("disabled", true);
            $("#btnAniadir").attr("hidden", true);
            $("#btnEditar").attr("disabled", false);
            $("#btnEditar").attr("hidden", false);
            $("#btnEliminar").attr("disabled", false);
            $("#btnEliminar").attr("hidden", false);
            
            self.IdFolioVisita = $('input[id=txtIdFolioVisita]').val();
            self.FolioDocumento($('input[id=txtFolioDocumento]').val());
            self.FolioVisita($('input[id=txtFolioVisita]').val());
            self.FechaInicio($('input[id=txtFechaInicio]').val());
            self.FechaFin($('input[id=txtFechaFin]').val());
            self.RfcCompania($('input[id=txtRFCCompania]').val());
            self.NoAcreedorSap($('input[id=txtNoAcreedorSAP]').val());
            self.NombreCompania($('input[id=txtCompania]').val());
            self.FichaSolicitante($('input[id=txtFichaSolicitante]').val());
            self.FichaAutorizador($('input[id=txtFichaAutorizador]').val());
            self.Motivo($('textarea[id=txtMotivo]').val());
            self.Operacion = "";
        };

        self.AniadirFolio = function () {
            $("#frmFolioVisita input,#frmFolioVisita textarea").attr("disabled", false);
            $("#frmFolioVisita input,#frmFolioVisita textarea").val("");

            self.NombreAutorizador("");
            self.NombreSolicitante("");
            $("#txtFolioDocumento").focus();

            $("#btnAniadir").attr("disabled", true);
            $("#btnCancelar").attr("disabled", false);
            $("#btnGuardar").attr("disabled", false);
            $("#btnEliminar").attr("disabled", true);
            $("#btnEditar").attr("disabled", true);

            $("#btnCancelar").attr("hidden", false);
            $("#btnGuardar").attr("hidden", false);
            $("#btnEditar").attr("hidden", true);
            $("#btnEliminar").attr("hidden", true);
            $("#btnAniadir").attr("hidden", true);

            $("#btnValidarSolicitante").attr("disabled", false);
            $("#btnValidarAutorizador").attr("disabled", false);
            $("#btnBusquedaCompania").attr("disabled", false);
            //$('#operacion').val("Aniadir");
            self.Operacion = "Aniadir";
        };

        self.EditarFolio = function () {
            $("#frmFolioVisita input,#frmFolioVisita textarea").attr("disabled", false);
            $("#txtFolioVisita,#txtFichaSolicitante,#txtFichaAutorizador").attr("disabled", true);

            $("#txtFolioDocumento").focus();

            self.FechaFin.disabled=false;

            $("#btnAniadir").attr("disabled", true);
            $("#btnCancelar").attr("disabled", false);
            $("#btnGuardar").attr("disabled", false);
            $("#btnEliminar").attr("disabled", true);
            $("#btnEditar").attr("disabled", true);

            $("#btnCancelar").attr("hidden", false);
            $("#btnGuardar").attr("hidden", false);
            $("#btnEditar").attr("hidden", true);
            $("#btnEliminar").attr("hidden", true);
            $("#btnAniadir").attr("hidden", true);

            $("#btnBusquedaCompania").attr("disabled", false);
            $("#btnValidarSolicitante").attr("disabled", false);
            $("#btnValidarAutorizador").attr("disabled", false);
            
            //$("#operacion").val('Editar');
            self.Operacion = "Editar";
        };

        self.CancelarFolio = function () {
            $("#frmFolioVisita input,#frmFolioVisita textarea").attr("disabled", true);

            $("#btnCancelar").attr("disabled", true);
            $("#btnGuardar").attr("disabled", true);
            
            $("#btnCancelar").attr("hidden", true);
            $("#btnGuardar").attr("hidden", true);

            $("#btnValidarSolicitante").attr("disabled", true);
            $("#btnValidarAutorizador").attr("disabled", true);
            $("#btnBusquedaCompania").attr("disabled", true);
            if (self.Operacion == "Aniadir") {
                $("#frmFolioVisita input,#frmFolioVisita textarea").val("");
                self.NombreAutorizador("");
                self.NombreSolicitante("");
                $("#btnAniadir").attr("disabled", false);
                $("#btnAniadir").attr("hidden", false);
            }
            else if (self.Operacion == "Editar") {
                $("#btnEliminar").attr("disabled", false);
                $("#btnEliminar").attr("hidden", false);
                $("#btnEditar").attr("disabled", false);
                $("#btnEditar").attr("hidden", false);
            }
            //$('#operacion').val("");
            self.Operacion = "";
            //$("#txtFechaInicio").val(FechaActual);
            //$("#txtFechaFin").val(FechaActual);
            //$("#txtFolioVisita").val("0");
        };

        self.Guardado = function () {
            $("#frmFolioVisita input,#frmFolioVisita textarea").attr("disabled", true);
            $("#btnValidarSolicitante").attr("disabled", true);
            $("#btnValidarAutorizador").attr("disabled", true);
            $("#btnBusquedaCompania").attr("disabled", true);

            $("#btnCancelar").attr("hidden", true);
            $("#btnGuardar").attr("hidden", true);

            $("#btnCancelar").attr("disabled", true);
            $("#btnGuardar").attr("disabled", true);

            if (self.Operacion == "Eliminar") {
                $("#frmFolioVisita input,#frmFolioVisita textarea").val("");
                self.NombreAutorizador("");
                self.NombreSolicitante("");
                
                $("#btnAniadir").attr("disabled", false);
                $("#btnEliminar").attr("disabled", true);
                $("#btnEditar").attr("disabled", true);

                $("#btnAniadir").attr("hidden", false);
                $("#btnEliminar").attr("hidden", true);
                $("#btnEditar").attr("hidden", true);
            }
            else {
                $("#btnAniadir").attr("disabled", true);
                $("#btnAniadir").attr("hidden", true);
                $("#btnEditar").attr("disabled", false);
                $("#btnEditar").attr("hidden", false);
                $("#btnEliminar").attr("disabled", false);
                $("#btnEliminar").attr("hidden", false);
            }
            //$("#operacion").val("");
            self.Operacion = "";
            //$("#txtFechaInicio").val(FechaActual);
            //$("#txtFechaFin").val(FechaActual);
            //$("#txtFolioVisita").val("0");
        };

        self.MostrarBusquedaCompanias = function () {
            var busqueda_companias = new catalogoBusquedaCompanias();

            busqueda_companias.MostrarCatalogo('folios-visita', self.CompaniaSeleccionada);
        }

        self.CompaniaSeleccionada = function (compania) {
            self.NombreCompania(compania.DESCRIPCION_ACREEDOR);
            self.NoAcreedorSap(compania.CVE_ACREEDOR);
            self.RfcCompania(compania.RFC_ACREEDOR);
        }

        self.EliminarFolio = function () {  
            if (confirm("¿Seguro que desea eliminar el folio?")) {
                //$("#chkFolioActivo").prop('checked', true);
                //$("#operacion").val("Eliminar");
                self.BorradoLogico = true;
                self.Operacion = "Eliminar";
                self.GuardarFolio();
            }
        };

        $('#txtFichaAutorizador').bind('keypress', function (e) {
            if (e.keyCode == 13) {
                self.FichaAutorizador($('input[id=txtFichaAutorizador]').val());
                self.ConsultaAutorizador(e);
            }
        });

        self.ValidarAutorizador = function () {
            self.FichaAutorizador($('input[id=txtFichaAutorizador]').val());
            self.ConsultaAutorizador();
        };

        self.ConsultaAutorizador = function () {
            if ($.trim(self.FichaAutorizador()) == "") {
                self.NombreAutorizador("Debe especificar una ficha para el usuario Autorizador");
            }
            else {
                $.ajax({
                    url: rootDir + "FoliosVisita/ConsultaAutorizador",
                    dataType: "json",
                    data:
                    {
                        ficha_autorizador: self.FichaAutorizador()
                    },
                    success: function (response, status, xhr) {
                        var ex = HandleException(response, status, xhr);

                        if (ex == null) {
                            var i = 0;

                            self.NombreAutorizador(response.result.NOMBRES + ' ' + response.result.APELLIDOS);
                            //Funciono
                        }

                        else {
                            //Error
                            self.NombreAutorizador(ex.Message);
                            //ShowException(ex);
                        }
                    },
                    beforeSend: function () {
                        $("#image-loading-autorizador").show();
                    },
                    complete: function () {
                        $("#image-loading-autorizador").hide();
                    }
                });
            }
        };

        $('#txtFichaSolicitante').bind('keypress', function (e) {
            if (e.keyCode == 13) {
                self.FichaSolicitante($('input[id=txtFichaSolicitante]').val());
                self.ConsultaSolicitante(e);
            }
        });

        self.ValidarSolicitante = function () {
            self.FichaSolicitante($('input[id=txtFichaSolicitante]').val());
            self.ConsultaSolicitante();
        };

        self.ConsultaSolicitante = function () {
            if ($.trim(self.FichaSolicitante()) == "") {
                self.NombreSolicitante("Debe especificar una ficha para el usuario Solicitante");
            }
            else {
                $.ajax({
                    url: rootDir + "FoliosVisita/ConsultaSolicitante",
                    dataType: "json",
                    data:
                    {
                        ficha_solicitante: self.FichaSolicitante()
                    },
                    success: function (response, status, xhr) {
                        var ex = HandleException(response, status, xhr);

                        if (ex == null) {
                            var i = 0;

                            self.NombreSolicitante(response.result.NOMBRES + ' ' + response.result.APELLIDOS);
                            //Funciono
                        }

                        else {
                            //Error
                            self.NombreSolicitante(ex.Message);
                        }
                    },
                    beforeSend: function () {
                        $("#image-loading-solicitante").show();
                    },
                    complete: function () {
                        $("#image-loading-solicitante").hide();
                    }
                });
            }
        };

        self.GuardarFolio = function () {
            //var id_empleado_pemex = self.EmpleadoPemex().ID_C_EMPLEADO_PEMEX();

            $.ajax(rootDir + "FoliosVisita/GuardarFolioVisita",
            {
                data: ko.toJSON(
                {
                    FolioVisita: mapping.toJS(self)
                }),
                type: "post",
                contentType: "application/json",
                success: function (response, status, xhr) {
                    var ex = HandleException(response, status, xhr);
                    if (ex == null) {
                        self.IdFolioVisita=response.result.IdFolioVisita;
                        //alert("Folio guardado");
                        self.Guardado();
                    }
                    else {
                        //Error
                        ShowException(ex);
                    }
                }
            });
        };
    }
});