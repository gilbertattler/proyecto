﻿define(['jquery', 'jquery.ui', 'libs/exception.handling'], function ($)
{
    return function RelacionCuentaUsuarioDominio()
    {
        var self = this;

        //Variable que mantiene el cuadro de dialogo
        self.dialog_catalogo_relacion = null;

        self.Mostrar = function (elementId)
        {
            elementId = "#" + elementId;
            $(elementId).before("<div id='dialogo'></div>");
            $('#dialogo').load(rootDir + "LogOn/RelacionarCuentaUsuarioDominio", self.Load);

            self.dialog_catalogo_relacion = $('#dialogo').dialog({ position: { my: "center" }, title: "Relacion de Cuenta del Dominio", minWidth: 420, modal: true, closeText: 'x', resizable: false, close: function (event, ui) { $('#dialogo').remove(); } });
            self.dialog_catalogo_relacion.dialog('open');
            // prevent the default action, e.g., following a link
        };

        self.RelacionarCuentaUsuarioDominio = function ()
        {
            var i = 0;

            $.ajax({
                url: rootDir + "LogOn/RelacionarCuentaUsuarioDominio/",
                dataType: "json",
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify(
                {
                    dominio: $('#txtCuentaDominio').val(),
                    usuario: $('#txtUsuarioSistema').val(),
                    password: $('#txContraseña').val()
                }),
                success: function (response, status, xhr)
                {
                    var ex = HandleException(response, status, xhr);
                    if (ex == null)
                    {
                        alert('Se ha relacionado correctamente la cuenta del dominio con la cuenta del usuario');
                        self.dialog_catalogo_relacion.dialog('close');
                        location.reload(true);
                    }
                    else
                    {
                        //Error
                        ShowException(ex);
                    }
                },
                beforeSend: function ()
                {
                    //$("#image-loading-instalacion-destino").show();
                },
                complete: function ()
                {
                    //$("#image-loading-instalacion-destino").hide();
                }
            });
        };

        self.Load = function (responseText, textStatus, XMLHttpRequest)
        {
            $('#btnCerrar').click(function ()
            {
                self.dialog_catalogo_relacion.dialog('close');
                return false;
            });

            $('#btnAceptar').click(self.RelacionarCuentaUsuarioDominio);

            $('#txtUsuarioSistema').focus();

            self.dialog_catalogo_relacion.dialog({ position: 'center' });
        };

    };
});