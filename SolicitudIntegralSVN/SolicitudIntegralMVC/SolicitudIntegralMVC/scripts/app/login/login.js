﻿define(['jquery', 'app/login/vistaRelacionCuentaUsuarioDominio', 'jquery.ui', 'libs/exception.handling'], function ($, vistaRelacionCuentaUsuarioDominio)
{
    return function LogIn()
    {
        var self = this;

        self.MostrarVistaRelacionCuentaUsuarioDominio = function()
        {
            var vistaRelacion = new vistaRelacionCuentaUsuarioDominio();
            vistaRelacion.Mostrar('login-page');
        };

        self.Init = function ()
        {
            var relacion_cuenta_dominio = $("#relacion-cuenta-dominio");

            if (relacion_cuenta_dominio != null)
            {
                relacion_cuenta_dominio.click(self.MostrarVistaRelacionCuentaUsuarioDominio);
            }

            if ($.browser.msie == true && parseInt($.browser.version, 10) < 9)
            {
                $("#mensaje-version-navegador").show();
            }
            
        };
    };
});