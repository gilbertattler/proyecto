define(["jquery", 'knockout', 'knockout.mapping', 'app/models/stp-integral/SolicitudIntegral', 'app/models/stp-integral/detalleSolicitudIntegral', 'app/solicitud-integral/EditarObservaciones', 'libs/koGrid/KoGrid', 'jquery.ui', 'jquery.ajax.setup', 'exception.handling', 'linq', 'libs/jquery.maskedinput.min', 'domReady!'], function ($, ko, mapping, SolicitudIntegral, DetalleSolicitudIntegral, EditarObservaciones)
{
    //Indispensable para evitar que el &%$% Internet Explorer use el cache
    $.ajaxSetup({ cache: false });

    return function ListadoSolicitudesIntegralesController()
    {
        //variables globales a la clase
        var self = this;

        self.solicitudesIntegrales = ko.observableArray([]);

        self.ObtenerFoliosSeleccionados = function ()
        {
            var solicitudes_seleccionadas = Enumerable.From(self.solicitudesIntegrales()).Where(function (x) { return x().Seleccionado() == true }).Select(function (x) { return x().FolioSolicitudIntegral; }).ToArray();
            var folios = "("
            for (var i = 0; i < solicitudes_seleccionadas.length; ++i)
            {
                folios += "'" + solicitudes_seleccionadas[i] + "'";

                if (i + 1 < solicitudes_seleccionadas.length)
                {
                    folios += ",";
                }
            }

            folios += ")";

            return folios;
        }

        //self.MostrarCalendarioFechab = function () {
        //    //$('#txtFecha').datepicker("show");
        //};
        self.FoliosSeleccionados = ko.computed(function ()
        {
            return self.ObtenerFoliosSeleccionados();
        });

        //Inicio: GridSolicitudesIntegrales
        self.gridSolicitudesIntegrales =
        {
            data: self.solicitudesIntegrales,
            columnDefs:
            [
                { field: 'Seleccionado', displayName: 'Sel', width: 40, cellTemplate: '<div><input type="checkbox" data-bind="checked: $parent.entity.Seleccionado" /></div>' },
                { field: 'FolioSolicitudIntegral', displayName: 'Folio', width: 120 },
                { field: 'FechaCreacion', displayName: 'F.Creacion', width: 80, visible: false },
                { field: 'TipoTransporte', displayName: 'Transporte', width: 160, visible: false },
                { field: 'DesTipoServicioItinerarioIda', displayName: 'Tip.Itin. Ida', width: 160, visible: false },
                { field: 'DesItinerarioIda', displayName: 'Itinerario Ida', width: 160 },
                { field: 'DesTipoServicioItinerarioRetorno', displayName: 'Tip.Itin. Ret', width: 160, visible: false },
                { field: 'DesItinerarioRetorno', displayName: 'Itinerario Retorno', width: 160, visible: false },
                { field: 'FichaAutorizador', displayName: 'FicAutorizador', width: 160, visible: false },
                { field: 'FichaSolicitante', displayName: 'FicSolicitante', width: 160, visible: false },
                { field: 'NombreSolicitante', displayName: 'Solicitante', width: 160, visible: false },
                { field: 'NombreAutorizador', displayName: 'Autorizador', width: 160, visible: false },
                { field: 'TipoSolicitudIntegral', displayName: 'Tipo', width: 230, cellTemplate: '<div><label data-bind="text: $parent.entity().TipoSolicitudIntegral != \'SOLO HOSPEDAJE\' ? $parent.entity().TipoSolicitudIntegral + \' - \' + $parent.entity().TipoTransporte : $parent.entity().TipoSolicitudIntegral"></label></div>' },
                { field: 'TipoServicio', displayName: 'Servicio', width: 140 },
                //{ field: 'TipoPersonal', displayName: 'Pers', width: 40, cellTemplate: '<div><label data-bind="text: $parent.entity.TipoPersonal == \'PEMEX\' ? \'PMX\' : $parent.entity.TipoPersonal == \'COMPANIA\' ? \'CIA\' : \'COM\'"></label></div>' },
                { field: 'TipoPersonalFormateado', displayName: 'Pers', width: 40 },
                { field: 'EstadoSolicitudIntegral', displayName: 'Estado', width: 100 },
                { field: 'FechaSubidaFormateada', displayName: 'Subida-Bajada', width: 150, cellTemplate: '<div><label data-bind="text: $parent.entity().FechaSubidaFormateada"></label> <label data-bind="visible: $parent.entity().TipoSolicitudIntegral != \'SOLO TRANSPORTE\' , text: $parent.entity().FechaBajadaFormateada"></label></div>' },
                { field: 'EditLink', displayName: 'Opc.', width: 90, cellTemplate: rootDir + 'SolicitudIntegral/GridOpcionesListadoSolicitudes' }
            ],
            canSelectRows: false,
            rowHeight: 20,
            jqueryUITheme: false,
            enablePaging: true,
            disableTextSelection: false,
            filterOptions:
            {
                filterText: ko.observable(""),
                useExternalFilter: true
            },
            pagingOptions:
            {
                pageSizes: ko.observableArray([10, 20, 50, 100, 250, 500, 1000]),
                pageSize: ko.observable(50),
                totalServerItems: ko.observable(0),
                currentPage: ko.observable(1)
            }
        };

        self.gridSolicitudesIntegrales.filterOptions.filterText.subscribe(function (data)
        {
            self.ObtenerSolicitudesIntegrales();
        });

        self.gridSolicitudesIntegrales.pagingOptions.pageSizes.subscribe(function (data)
        {
            self.ObtenerSolicitudesIntegrales();
        });

        self.gridSolicitudesIntegrales.pagingOptions.pageSize.subscribe(function (data)
        {
            self.gridSolicitudesIntegrales.pagingOptions.currentPage(1);
            self.ObtenerSolicitudesIntegrales();
        });

        self.gridSolicitudesIntegrales.pagingOptions.totalServerItems.subscribe(function (data)
        {
            self.ObtenerSolicitudesIntegrales();
        });

        self.gridSolicitudesIntegrales.pagingOptions.currentPage.subscribe(function (data)
        {
            if (data < 1)
            {
                self.gridSolicitudesIntegrales.pagingOptions.currentPage(1);
            }

            self.ObtenerSolicitudesIntegrales();
        });
        //Fin: gridSolicitudesIntegrales


        //Constructor de la clase
        self.Init = function () //Inicio -- INIT()
        {
            $(document).ready(function ()
            {
                self.ObtenerSolicitudesIntegrales();

                $('#txtFecha').datepicker({
                    defaultDate: "+1w", numberOfMonths: 3, stepMonths: 3, dateFormat: 'dd/mm/yy',
                    //Hack para que el datepicker se muestre sobre todos los demas elementos.
                    beforeShow: function ()
                    {
                        setTimeout(function ()
                        {
                            $('.ui-datepicker').css('z-index', 99999999999999);
                        }, 0);
                    }


                });

            });
            $("#txtFecha").mask("99/99/9999");
            //Se aplica el ligado
            ko.applyBindings(self);
        };//fin -- INIT()

        self.RenderizarKoGrid = function ()
        {
            //var evt = document.createEvent('UIEvents');
            //evt.initUIEvent('resize', true, false, window, 0);
            //window.dispatchEvent(evt);

            $(window).trigger("resize");
        };

        self.ObtenerSolicitudesIntegrales = function ()
        {
            var datos_empleado = null;

            $.ajax({
                url: rootDir + "SolicitudIntegral/ConsultaSolicitudesIntegrales/",
                dataType: "json",
                data:
                {
                    page: self.gridSolicitudesIntegrales.pagingOptions.currentPage(),
                    pageSize: self.gridSolicitudesIntegrales.pagingOptions.pageSize(),
                    searchString: self.gridSolicitudesIntegrales.filterOptions.filterText(),
                    ficha: $("#txtFicha").val(),
                    fecha: $("#txtFecha").val()
                },
                success: function (response, status, xhr)
                {
                    var ex = HandleException(response, status, xhr);

                    if (ex == null)
                    {
                        if (response.result != null || response.result != "undefined")
                        {
                            var solicitudes_integrales = ko.utils.arrayMap(response.result, function (item)
                            {
                                var sol = new SolicitudIntegral(item);
                                sol.Seleccionado = ko.observable(false);
                                return ko.observable(sol);
                            });
                            self.solicitudesIntegrales(solicitudes_integrales);
                            self.gridSolicitudesIntegrales.pagingOptions.totalServerItems(response.total_registros);
                            self.RenderizarKoGrid();
                        }
                    }
                    else
                    {
                        //Error
                        ShowException(ex);
                    }
                },
                beforeSend: function ()
                {
                    $("#image-loading-listado-solicitudes").show();
                },
                complete: function ()
                {
                    $("#image-loading-listado-solicitudes").hide();
                }
            });
        }

        self.ConfirmacionMultipleSolicitudesIntegrales = function ()
        {
            var solicitudes_estatus_no_solicitado = Enumerable.From(self.solicitudesIntegrales()).Where(function (x) { return x.Seleccionado == true && x().EstadoSolicitudIntegral != "SOLICITADO" }).Select(function (x) { return x; }).ToArray();

            if (solicitudes_estatus_no_solicitado != null && solicitudes_estatus_no_solicitado.length > 0)
            {
                alert('Solo se pueden confirmar solicitudes en estado \'SOLICITADO\'');
            }
            else
            {
                var ids_solicitudes_estatus_solicitado = Enumerable.From(self.solicitudesIntegrales()).Where(function (x) { return x.Seleccionado == true && x().EstadoSolicitudIntegral == "SOLICITADO" }).Select(function (x) { return x().IdSolicitudIntegral; }).ToArray();

                if (ids_solicitudes_estatus_solicitado == null || (ids_solicitudes_estatus_solicitado != null && ids_solicitudes_estatus_solicitado.length <= 0))
                {
                    alert('Debe seleccionar alguna solicitud para la poder ser confirmada.');
                }
                else
                {
                    $.ajax({
                        url: rootDir + "SolicitudIntegral/ConfirmacionMultipleSolicitudesIntegrales/",
                        data: JSON.stringify({ idSolicitudesIntegrales: ids_solicitudes_estatus_solicitado }),
                        traditional: true,
                        type: "POST",
                        dataType: 'json',
                        contentType: "application/json",
                        success: function (response, status, xhr)
                        {
                            var ex = HandleException(response, status, xhr);
                            if (ex == null)
                            {
                                self.ObtenerSolicitudesIntegrales();
                                alert(response.result);
                            }
                            else
                            {
                                ShowException(ex);
                            }
                        },
                        beforeSend: function ()
                        {
                            $("span.image-loading-confirmar-solicitudes").show();
                        },
                        complete: function ()
                        {
                            $("span.image-loading-confirmar-solicitudes").hide();
                        }
                    });
                }
            }
        };//fin: ConfirmacionMultipleSolicitudesIntegrales()

        self.ConfirmarSolicitudes = function ()
        {
            //var solicitudes_seleccionadas = Enumerable.From(self.solicitudesIntegrales()).Where(function (x) { return x.Seleccionado == true }).Select(function (x) { return x; }).ToArray();
            var solicitudes_estatus_no_solicitado = Enumerable.From(self.solicitudesIntegrales()).Where(function (x) { return x.Seleccionado == true && x().EstadoSolicitudIntegral != "SOLICITADO" }).Select(function (x) { return x; }).ToArray();

            if (solicitudes_estatus_no_solicitado != null && solicitudes_estatus_no_solicitado.length > 0)
            {
                alert('Solo se pueden confirmar solicitudes en estado \'SOLICITADO\'');
            }
        }

        self.ImprimirBoletos = function ()
        {
            var folios = self.ObtenerFoliosSeleccionados();
            window.location.href = rootDir + 'SolicitudIntegral/ImprimirBoletos/' + folios
            //alert(folios);
        }

        self.ObtenerFoliosSeleccionados = function ()
        {
            var solicitudes_seleccionadas = Enumerable.From(self.solicitudesIntegrales()).Where(function (x) { return x.Seleccionado == true }).Select(function (x) { return x().FolioSolicitudIntegral; }).ToArray();
            var folios = "";//"("
            for(var i = 0; i < solicitudes_seleccionadas.length;++i)
            {
                folios += "" + solicitudes_seleccionadas[i] + "";

                if(i + 1 < solicitudes_seleccionadas.length)
                {
                    folios += ",";
                }
            }

            //folios += ")";

            return folios;
        }
        //Edicion de observaciones
        self.entity().FolioSolicitudIntegral = function (folioSI) {
        var editar_observaciones = new EditarObservaciones();
        editar_observaciones.MostrarEditorObservacion('solicitud-integral', folioSI);
        }
    };//fin SolicitudIntegralController()
});//fin define

