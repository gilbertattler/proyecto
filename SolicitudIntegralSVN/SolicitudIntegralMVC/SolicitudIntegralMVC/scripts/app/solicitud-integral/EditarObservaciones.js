﻿define(['jquery', 'knockout', 'knockout.mapping', 'libs/exception.handling', 'jquery.ui', 'dateJs'], function ($, ko, mapping) {
    return function EditarObservaciones() {
        var self = this;

        self.MostrarEditorObservacion = function (elementId,folioSI) {

            elementId = "#" + elementId;
            $(elementId).before("<div id='dialogo-Editar-observaciones'></div>");
            $('#dialogo-Editar-observaciones').load(rootDir + "SolicitudIntegral/EdicionObservaciones", self.LoadListaDestinos);

            self.dialog_lista_destinos = $('#dialogo-Editar-observaciones').dialog({ position: { my: "center" }, title: "Edicion de Observaciones", minWidth: 600, modal: true, closeText: 'x', resizable: false, close: function (event, ui) { $('#dialogo-Editar-observaciones').remove(); } });
            self.dialog_lista_destinos.dialog('open');
        };

        self.LoadListaDestinos = function (responseText, textStatus, XMLHttpRequest) {
            $('#btnCerrar').click(function () {
                self.dialog_lista_destinos.dialog('close');
                return false;
            });

            ko.applyBindings(self, $("#frmObservacionVentanillaLogistica")[0]);

            self.dialog_lista_destinos.dialog({ position: 'center' });
        };

        self.GuardarObservaciones = function () {

        };

    };
});
