﻿define(['jquery', 'knockout', 'knockout.mapping', 'libs/exception.handling', 'jquery.ui', 'linq'], function ($, ko, mapping)
{
    return function ConsultaInstalacionesAutorizador()
    {
        var self = this;

        //Variable que mantiene el cuadro de dialogo
        self.dialog_consulta_instalaciones_destino = null;

        self.listado_instalaciones_autor = ko.observableArray();
        self.ficha_autorizador = '';
        self.fecha_inicio = '';
        self.fecha_fin = '';

        self.cadenabusqueda = ko.observable('');

        self.listado_instalaciones_computadas = ko.computed(function ()
        {
            var resultado = null;
            if (self.cadenabusqueda() == '' ) {
                resultado = (Enumerable.From(self.listado_instalaciones_autor()).Where(function (x) { return x; }).Select(function (x) { return x; }).ToArray())
            }
            else {
                resultado = (Enumerable.From(self.listado_instalaciones_autor()).Where(function (x) { return x.NOMBRE_INSTALACION.toUpperCase().indexOf(self.cadenabusqueda().toUpperCase())>=0; }).Select(function (x) { return x; }).ToArray())
            }
            return resultado;
        }, self);
        
        self.Mostrar = function (elementId,ficha_autorizador,fecha_inicio,fecha_fin)
        {
            self.ficha_autorizador = ficha_autorizador;
            self.fecha_fin = fecha_fin;
            self.fecha_inicio = fecha_inicio;

            elementId = "#" + elementId;
            $(elementId).before("<div id='dialogo-consulta-instalaciones-destino'></div>");
            $('#dialogo-consulta-instalaciones-destino').load(rootDir + "SolicitudIntegral/VistaConsultaInstalacionesDestinoAutorizador", self.LoadConsultaInstalacionesDestino);

            self.dialog_consulta_instalaciones_destino = $('#dialogo-consulta-instalaciones-destino').dialog({ position: { my: "center" }, title: "Consulta Instalaciones Destino", minWidth: 820, modal: true, closeText: 'x', resizable: false, close: function (event, ui) { $('#dialogo-consulta-instalaciones-destino').remove(); } });
            self.dialog_consulta_instalaciones_destino.dialog('open');
            // prevent the default action, e.g., following a link
        };

        self.LoadConsultaInstalacionesDestino = function (responseText, textStatus, XMLHttpRequest)
        {
            $('#btnCerrar').click(function ()
            {
                self.dialog_consulta_instalaciones_destino.dialog('close');
                return false;
            });

            self.ConsultaInstalacionesAutor();

            ko.applyBindings(self, $("#consulta-instalaciones-destino-autorizador")[0]);

            self.dialog_consulta_instalaciones_destino.dialog({ position: 'center' });
        };

        self.ConsultaInstalacionesAutor = function ()
        {
            $.ajax(rootDir + "SolicitudIntegral/ConsultaInstalacionesDestinoAutorizador",
            {
                data: {
                    ficha_autorizador: self.ficha_autorizador,
                    fecha_subida:self.fecha_inicio,
                    fecha_bajada:self.fecha_fin
                    
                },
                type: "get",
                contentType: "application/json",
                success: function (response, status, xhr)
                {
                    var ex = HandleException(response, status, xhr);
                    if (ex == null)
                    {
                        var mappedTasks = ko.utils.arrayMap(response.result, function (item) { return item });
                        self.listado_instalaciones_autor(mappedTasks);
                    }
                    else
                    {
                        //Error
                        ShowException(ex);
                    }
                },
                beforeSend: function ()
                    {
                    $("#image-loading-instalaciones-autorizador").show();
                    },
                complete: function ()
                {
                    $("#image-loading-instalaciones-autorizador").hide();
                }
            });
        };
    };
});


