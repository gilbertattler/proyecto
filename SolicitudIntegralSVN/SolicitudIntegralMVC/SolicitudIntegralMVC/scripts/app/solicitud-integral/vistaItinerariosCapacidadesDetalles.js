﻿define(['jquery', 'jquery.ui', 'dateJs'], function ($)
{
    //Indispensable para evitar que el &%$% Internet Explorer use el cache
    $.ajaxSetup({ cache: false });

    return function vistaItinerariosCapacidadesDetalles()
	{
		var self = this;

		self.dialog_itinerarios_capacidades_detalles = null;

		self.Mostrar = function (tipo_transporte, id_itinerario, fecha_servicio, subdivision_autorizador, contrato, folio_especial, elementId)
		{
		    var fecha_servicio_js = Date.parse(fecha_servicio);
		    
		    elementId = "#" + elementId;
			$(elementId).before("<div id='vistaItinerariosCapacidadesDetalles'></div>");

			var load_url = rootDir + "SolicitudIntegral/VistaItinerarioCapacidadDetallePorDia/" + tipo_transporte + "/" + id_itinerario.toString() + "/" + fecha_servicio_js.toString('yyyy-MM-dd') + "/" + (subdivision_autorizador == "" ? "-" : subdivision_autorizador) + "/" + (contrato == "" ? "-" : contrato) + "/" + (folio_especial == "" ? "-" : encodeURIComponent(folio_especial));
			$('#vistaItinerariosCapacidadesDetalles').load(load_url, self.LoadCapacidadesItinerariosDetalles);

			self.dialog_itinerarios_capacidades_detalles = $('#vistaItinerariosCapacidadesDetalles').dialog({ position: { my: "center" }, title: "Detalle de Capacidades por Itinerario", minWidth: 600, modal: true, closeText: 'x', resizable: false, close: function (event, ui) { $('#vistaItinerariosCapacidadesDetalles').remove(); } });
			self.dialog_itinerarios_capacidades_detalles.dialog('open');
			// prevent the default action, e.g., following a link
		};

		self.LoadCapacidadesItinerariosDetalles = function (responseText, textStatus, XMLHttpRequest)
		{
		    $('#btnCerrar').click(function ()
			{
		        self.dialog_itinerarios_capacidades_detalles.dialog('close');
				return false;
			});


			self.dialog_itinerarios_capacidades_detalles.dialog({ position: 'center' });
		};
	};
});