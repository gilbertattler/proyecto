﻿define(['jquery', 'jquery.ui', 'dateJs'], function ($)
{
    //Indispensable para evitar que el &%$% Internet Explorer use el cache
    $.ajaxSetup({ cache: false });

    return function vistaPermisosAutorizadores()
	{
		var self = this;

		self.dialog_permisos_autorizadores = null;

		self.Mostrar = function (tipo_servicio, tipo_transporte, ficha_autorizador, fecha_subida, fecha_bajada, elementId)
		{
		    var fecha_subida_js = Date.parse(fecha_subida);
		    var fecha_bajada_js = Date.parse(fecha_bajada);
		    
		    elementId = "#" + elementId;
			$(elementId).before("<div id='vistaPermisosAutorizadores'></div>");
			$('#vistaPermisosAutorizadores').load(rootDir + "SolicitudIntegral/VistaPermisosAutorizador/" + encodeURIComponent(tipo_servicio) + "/" + tipo_transporte + "/" + ficha_autorizador + "/" + fecha_subida_js.toString('yyyy-MM-dd') + "/" + fecha_bajada_js.toString('yyyy-MM-dd'), self.LoadistadoPermisosAutorizador);

			self.dialog_permisos_autorizadores = $('#vistaPermisosAutorizadores').dialog({ position: { my: "center" }, title: "Permisos de Autorizador", minWidth: 500, modal: true, closeText: 'x', resizable: false, close: function (event, ui) { $('#vistaPermisosAutorizadores').remove(); } });
			self.dialog_permisos_autorizadores.dialog('open');
			// prevent the default action, e.g., following a link
		};

		self.LoadistadoPermisosAutorizador = function (responseText, textStatus, XMLHttpRequest)
		{
		    $('#btnCerrar').click(function ()
			{
		        self.dialog_permisos_autorizadores.dialog('close');
				return false;
			});


			self.dialog_permisos_autorizadores.dialog({ position: 'center' });
		};
	};
});