define(['jquery',
        'knockout',
        'knockout.mapping',
        'app/models/stp-integral/SolicitudIntegral',
        'app/models/stp-integral/detalleSolicitudIntegral',
        'app/models/stp-integral/itinerarioTransporte',
        'app/models/gestion-gl/vwCContratos',
        'app/models/gestion-gl/cMInstalaciones',
        'app/models/gestion-gl/vwPermisosDireccionamientosGeneral',
        'app/models/utilerias/EmpleadoPemexCia',
        'app/catalogo/catalogo-empleado-cia',
        'app/catalogo/catalogo-empleado-pemex',
        'app/models/gestion-gl/CEmpleadosCia',
        'app/models/gestion-gl/CEmpleadosPemex',
        'app/solicitud-integral/vistaItinerariosDisponibles',
        'app/solicitud-integral/vistaPermisosAutorizador',
        'app/solicitud-integral/vistaItinerariosCapacidadesDetalles',
        'app/solicitud-integral/vistaCapacidadHospedajePorRangoFechas',
        'app/solicitud-integral/vistaDetallesEmpleado',
        'app/solicitud-integral/consulta-instalaciones-autorizador',
        'app/solicitud-integral/ListaDestinosItinerario',
        'app/solicitud-integral/BusquedaxInstalacion',
        'jquery.ui',
        'jquery.ajax.setup',
        'libs/exception.handling',
        'linq',
        'domReady!',
        'jquery.tagit',
        'dateJs',
        'parseDate',
        'libs/jquery.maskedinput.min'],
function ($, ko, mapping, SolicitudIntegral, DetalleSolicitudIntegral, ItinerarioTransporte, VwCContratos, CMInstalaciones, VwPermisosDireccionamientosGeneral, EmpleadoPemexCia, VistaCatalogoEmpleadoCia, VistaCatalogoEmpleadoPemex, CEmpleadosCia, CEmpleadosPemex, VistaItinerariosDisponibles, VistaPermisosAutorizador, VistaItinerariosCapacidadesDetalles, VistaCapacidadHospedajePorRangoFechas, VistaDetallesEmpleado, VistaConsultaInstalacionesDestino, ListaDestinoItinerario, BusquedaxInstalacion)
{
    //Indispensable para evitar que el &%$# Internet Explorer use el cache
    $.ajaxSetup({ cache: false });

    /////////// JQuery UI ComboBox Widget
        
    (function ($)
        {
            $.widget("ui.combobox",
                {
                    _create: function ()
                    {
                        this.wrapper = $("<span>")
                          .addClass("custom-combobox")
                          .insertAfter(this.element);

                        this.element.hide();
                        this._createAutocomplete();
                        //this._createShowAllButton();

                        $(this.input).click(function ()
                        {
                            if(!$(this).hasClass("selected"))
                                $(this).select();
                            $(this).addClass("selected");
                        });

                        $(this.input).blur(function ()
                        {
                            if ($(this).hasClass("selected"))
                                $(this).removeClass("selected");
                        });

                        this.element.change(this.changeSelectedItem);
                    },

                    changeSelectedItem: function(e)
                    {
                        var selected = $(e.target).children("option:selected");
                        $(e.target).parent().children("span").children("input").val(selected.text())
                    },

                    _createAutocomplete: function ()
                    {
                        var selected = this.element.children(":selected"),
                          value = selected.val() ? selected.text() : "";

                        this.input = $("<input>")
                          .appendTo(this.wrapper)
                          .val(value)
                          .attr("title", "")
                          .addClass("custom-combobox-input ui-widget ui-widget-content ui-corner-left")
                          .autocomplete({
                              delay: 0,
                              minLength: 0,
                              source: $.proxy(this, "_source")
                          })
                          .tooltip({
                              tooltipClass: "ui-state-highlight"
                          });

                        this._on(this.input, {
                            autocompleteselect: function (event, ui)
                            {
                                ui.item.option.selected = true;
                                this._trigger("select", event, {
                                    item: ui.item.option
                                });

                                $(this.element).trigger("change")
                            },

                            autocompletechange: "_removeIfInvalid"
                        });
                    },

                    _createShowAllButton: function ()
                    {
                        var input = this.input,
                          wasOpen = false;

                        $("<a>")
                          .attr("tabIndex", -1)
                          .attr("title", "Show All Items")
                          .tooltip()
                          .appendTo(this.wrapper)
                          .button({
                              icons: {
                                  primary: "ui-icon-triangle-1-s"
                              },
                              text: false
                          })
                          .removeClass("ui-corner-all")
                          .addClass("custom-combobox-toggle ui-corner-right")
                          .mousedown(function ()
                          {
                              wasOpen = input.autocomplete("widget").is(":visible");
                          })
                          .click(function ()
                          {
                              input.focus();

                              // Close if already visible
                              if (wasOpen)
                              {
                                  return;
                              }

                              // Pass empty string as value to search for, displaying all results
                              input.autocomplete("search", "");
                          });
                    },

                    _source: function (request, response)
                    {
                        var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                        response(this.element.children("option").map(function ()
                        {
                            var text = $(this).text();
                            if (this.value && (!request.term || matcher.test(text)))
                                return {
                                    label: text,
                                    value: text,
                                    option: this
                                };
                        }));
                    },

                    _removeIfInvalid: function (event, ui)
                    {
                        // Selected an item, nothing to do
                        if (ui.item)
                        {
                            return;
                        }

                        // Search for a match (case-insensitive)
                        var value = this.input.val(),
                          valueLowerCase = value.toLowerCase(),
                          valid = false;
                        this.element.children("option").each(function ()
                        {
                            if ($(this).text().toLowerCase() === valueLowerCase)
                            {
                                this.selected = valid = true;
                                return false;
                            }
                        });

                        // Found a match, nothing to do
                        if (valid)
                        {
                            return;
                        }

                        //// Remove invalid value
                        //this.input
                        //  .val("")
                        //  .attr("title", value + " didn't match any item")
                        //  .tooltip("open");
                        //this.element.val("");
                        //this._delay(function ()
                        //{
                        //    this.input.tooltip("close").attr("title", "");
                        //}, 2500);
                        this.input.data("ui-autocomplete").term = "";
                    },

                    _destroy: function ()
                    {
                        this.wrapper.remove();
                        this.element.show();
                    },

                    toggleCombo: function ()
                    {
                        this.wrapper.toggle();
                        this.element.toggle();
                    },

                    enabled: function (enable)
                    {
                        if (enable)
                        {
                            $(this.element).removeAttr('disabled');
                            $(this.wrapper).removeAttr('disabled');
                            $(this.input).removeAttr('disabled');
                            $(this.input).removeAttr('readonly');
                        }
                        else
                        {
                            $(this.element).attr('disabled', true);
                            $(this.wrapper).attr('disabled', true);
                            $(this.input).attr('disabled', true);
                            $(this.input).attr('readonly', true);
                        }
                    }
                }
            );
        })(jQuery);

    return function SolicitudIntegralController()
    {
        //variables globales a la clase
        var self = this;

        self.FolioSolicitudIntegralBusqueda = ko.observable("");
        self.TipoSolicitudSeleccionadaCheckBox = ko.observable("");

        self.solicitudIntegral = mapping.fromJS(new SolicitudIntegral());
        self.detallesSolicitudIntegral = ko.observableArray([]);

        self.contratoSap = mapping.fromJS(new VwCContratos());
        self.itinerariosTransporte = ko.observableArray([]);

        self.itinerariosTransporteRetorno = ko.observableArray([]);

        self.itinerarioTransporteIdaSeleccionado = ko.observable();
        self.itinerarioTransporteRetornoSeleccionado = ko.observable();

        self.SolicitudIntegralRequiereRetorno = ko.observable("");

        self.instalacionesOrigenDisponibles = ko.observableArray([]);

        self.instalacionesDestinoDisponibles = ko.observableArray([]);

        self.direccionamientosInstalacionesDisponibles = ko.observableArray([]);

        self.direccionamientosInstalacionDestinoAH = ko.observableArray([]);
        self.direccionamientosInstalacionDestinoSTP = ko.observableArray([]);

        self.direccionamientoInstalacionDestinoAHSeleccionado = null;
        self.direccionamientoInstalacionDestinoSTPSeleccionado = null;

        //// Direccionamientos de Instalaciones Origen, solo aplica en caso de TRASNPORTE y cuando el destino es CME o bien cuando es una solicitud interplataforma
        self.direccionamientosInstalacionesOrigenDisponibles = ko.observableArray([]);

        //self.direccionamientosInstalacionOrigenSTP = ko.observableArray([]);
        //self.direccionamientoInstalacionOrigenSTPSeleccionado = null;
        ////

        var ko_subscripcion_solicitudIntegral_IdSolicitudIntegral = null;
        var ko_subscripcion_solicitudIntegral_TipoServicio = null;
        var ko_subscripcion_solicitudIntegral_FKidOrigenSolicitud = null;
        var ko_subscripcion_solicitudIntegral_FKidDestinoSolicitud = null;
        var ko_subscripcion_solicitudIntegral_RequiereRetorno = null;

        self.PermitirCapturaCamposSolicitudIntegral = ko.computed(function ()
        {
            var resultado = false;

            if (self.TipoSolicitudSeleccionadaCheckBox() != "")
            {
                switch (self.TipoSolicitudSeleccionadaCheckBox())
                {
                    case "SOLO HOSPEDAJE":
                        {
                            self.solicitudIntegral.TipoSolicitudIntegral("SOLO HOSPEDAJE");
                            resultado = self.solicitudIntegral.TipoPersonal() != "";
                            break;
                        }
                    case "SOLO TRANSPORTE":
                        {
                            self.solicitudIntegral.TipoSolicitudIntegral("SOLO TRANSPORTE");
                            resultado = self.solicitudIntegral.TipoPersonal() != "" && self.solicitudIntegral.TipoTransporte() != "" && self.solicitudIntegral.TipoServicio() != "";
                            break;
                        }
                    case "HOSPEDAJE Y TRANSPORTE":
                        {
                            self.solicitudIntegral.TipoSolicitudIntegral("HOSPEDAJE Y TRANSPORTE");
                            switch (self.solicitudIntegral.TipoTransporte())
                            {
                                case "AEREO":
                                case "MARITIMO":
                                    {
                                        resultado = self.solicitudIntegral.TipoPersonal() != "" && self.solicitudIntegral.TipoServicio() != "";
                                        break;
                                    }
                                case "MEDIOS PROPIOS":
                                    {
                                        self.solicitudIntegral.TipoSolicitudIntegral("SOLO HOSPEDAJE");
                                        resultado = self.solicitudIntegral.TipoPersonal() != "";
                                        break;
                                    }
                            }
                        break;
                    }
                }
            }
            return resultado;
        },self);

        self.EsVisibleMediosPropios = ko.computed(function ()
        {
            //return (mapping.toJS(self.TipoPersonal) == 'PEMEX' && mapping.toJS(self.TipoTransporte) == 'AEREO') || mapping.toJS(self.TipoTransporte) != 'AEREO';
            var resultado = mapping.toJS(self.solicitudIntegral.TipoSolicitudIntegral() != "SOLO TRANSPORTE");
            return resultado;
        });

        self.EsVisibleTipoServicioCambioGuardiaDirecto = ko.computed(function ()
        {
            var resultado = false;

            if(self.solicitudIntegral.TipoSolicitudIntegral() != "SOLO TRANSPORTE")
            {
                resultado = (self.solicitudIntegral.TipoPersonal() == 'PEMEX' && self.solicitudIntegral.TipoTransporte() == 'AEREO') || self.solicitudIntegral.TipoTransporte() != 'AEREO';
            }
                
            return resultado;
        });

        self.EsVisibleTipoServicioCambioGuardiaDirecto.subscribe(function (value)
        {
            if (value == false && self.solicitudIntegral.TipoServicio() == "CAMBIO DE GUARDIA")
            {
                $("#chkTipoServcioRegular").click();
                self.solicitudIntegral.TipoServicio("REGULARES");
            }
        });

        self.EsVisibleOpcionRetorno = ko.computed(function ()
        {
            var resultado = false;
            
            if (self.solicitudIntegral.IdSolicitudIntegral() > 0 && self.solicitudIntegral.RequiereRetorno() == true)
            {
                resultado = true;
            }

            if (self.solicitudIntegral != null && self.solicitudIntegral != "undefined")
            {
                if (self.solicitudIntegral.TipoSolicitudIntegral() == 'SOLO TRANSPORTE' || (self.solicitudIntegral.TipoSolicitudIntegral() == 'HOSPEDAJE Y TRANSPORTE' && self.solicitudIntegral.TipoServicio() != 'MEDIOS PROPIOS' && self.solicitudIntegral.FechaSubida() == self.solicitudIntegral.FechaBajada()))
                {
                    if (self.itinerarioTransporteIdaSeleccionado() != null && self.itinerarioTransporteIdaSeleccionado != "undefined")
                    {
                        switch (self.solicitudIntegral.TipoServicio())
                        {
                            case "REGULARES":
                            {
                                resultado = (self.itinerarioTransporteIdaSeleccionado().cve_nodo_origen != 'IXT' && self.itinerarioTransporteIdaSeleccionado().cve_nodo_destino == 'IXT');
                                break;
                            }
                            case "LATERALES":
                            {
                                resultado = true;
                                break;
                            }
                        }
                    }
                }
            }

            if (resultado == false && self.solicitudIntegral.IdSolicitudIntegral() <= 0)
            {
                self.solicitudIntegral.RequiereRetorno(false);
            }
            return resultado;
        }, self);

        self.DetallesSolicitudIntegral = ko.computed(function () 
        {
            return self.detallesSolicitudIntegral;
        });

        self.ItinerariosTransporteIda = ko.computed(function ()
        {
            var resultado = null;

            if(mapping.toJS(self.solicitudIntegral.TipoServicio()) == 'CAMBIO DE GUARDIA')
            {
                resultado = (Enumerable.From(self.itinerariosTransporte()).Where(function (x) { return (x.cve_nodo_origen != "IXT" && x.cve_nodo_origen != x.cve_nodo_destino) || (x.cve_nodo_origen == 'IXT' && x.cve_nodo_origen == x.cve_nodo_destino) }).Select(function (x) { return x; }).ToArray())
            }
            else
            {
                resultado = self.itinerariosTransporte();
            }
            
            return resultado;
        });

        self.ItinerariosTransporteRetorno = ko.computed(function ()
        {
            //return (mapping.toJS(self.TipoPersonal) == 'PEMEX' && mapping.toJS(self.TipoTransporte) == 'AEREO') || mapping.toJS(self.TipoTransporte) != 'AEREO';
            var resultado = null;

            if (self.solicitudIntegral.RequiereRetorno() || self.solicitudIntegral.TipoServicio() == 'CAMBIO DE GUARDIA')
            {
                switch (self.solicitudIntegral.TipoServicio())
                {
                    case "CAMBIO DE GUARDIA":
                    case "REGULARES":
                    {
                        resultado = (Enumerable.From(self.itinerariosTransporte()).Where(function (x) { return (x.id_itinerario != self.solicitudIntegral.FkIdItinerarioTransporteIda() && x.cve_nodo_origen == "IXT" && x.cve_nodo_destino != x.cve_nodo_origen) }).Select(function (x) { return x; }).ToArray())
                        break;
                    }
                    case "LATERALES":
                    {
                        resultado = (Enumerable.From(self.itinerariosTransporte()).Select(function (x) { return x; }).ToArray())
                        break;
                    }
                }
            }
            return resultado;
        });

        //Subscripciones
        self.InicializarSubscripciones = function ()
        {
            ko_subscripcion_solicitudIntegral_IdSolicitudIntegral = self.solicitudIntegral.IdSolicitudIntegral.subscribe(function (newValue)
            {
                self.HabilitarDesabilitarControlesSegunEstadoSolicitud();
            });

            ko_subscripcion_solicitudIntegral_TipoServicio = self.solicitudIntegral.TipoServicio.subscribe(function (newvalue) { self.ConsultarItinerariosFunction(newvalue); });
            ko_subscripcion_solicitudIntegral_TipoPersonal = self.solicitudIntegral.TipoPersonal.subscribe(function (newvalue) 
            { 
                if (newvalue == "PEMEX")
                {
                    self.solicitudIntegral.NumeroContrato("PEMEX");
                    self.solicitudIntegral.NombreAcreedor("");
                    self.solicitudIntegral.RFCAcreedor("");
                }
                else
                {
                    if (self.solicitudIntegral.NumeroContrato() == "PEMEX")
                    {
                        self.solicitudIntegral.NumeroContrato("");
                        self.solicitudIntegral.NombreAcreedor("");
                        self.solicitudIntegral.RFCAcreedor("");
                    }
                }
                self.ConsultarItinerariosFunction(newvalue);
            });

            ko_subscripcion_solicitudIntegral_TipoTransporte = self.solicitudIntegral.TipoTransporte.subscribe(function (newvalue) { self.ConsultarItinerariosFunction(newvalue); });
            ko_subscripcion_solicitudIntegral_TipoSolicitudIntegral = self.solicitudIntegral.TipoSolicitudIntegral.subscribe(function (newvalue) { self.ConsultarItinerariosFunction(newvalue); });
            ko_subscripcion_solicitudIntegral_RequiereRetorno = self.solicitudIntegral.RequiereRetorno.subscribe(function (newvalue) { self.CambioRequiereRetornoFunction(newvalue); });

            ko_subscripcion_solicitudIntegral_FKidOrigenSolicitud = self.solicitudIntegral.FKidOrigenSolicitud.subscribe(function (newValue)
            {
                var itinerario_elegido = null;
                
                if(self.solicitudIntegral.FkIdItinerarioTransporteIda() != null && self.solicitudIntegral.FkIdItinerarioTransporteIda() != "nothing" && self.solicitudIntegral.FkIdItinerarioTransporteIda() != "")
                {
                    itinerario_elegido = Enumerable.From(self.itinerariosTransporte()).Where(function (x) { return x.id_itinerario == self.solicitudIntegral.FkIdItinerarioTransporteIda() }).Select(function (x) { return x; }).FirstOrDefault();
                }
                
                if (self.solicitudIntegral.TipoServicio() == "LATERALES" || (itinerario_elegido != null && itinerario_elegido != "nothing" && self.solicitudIntegral.TipoServicio() == "REGULARES" && itinerario_elegido.cve_nodo_origen == "IXT"))
                {
                    self.ObtenerDireccionamientosDestinosSTP_AH(self.solicitudIntegral.FKidDestinoSolicitud());
                }
            });

            ko_subscripcion_solicitudIntegral_FKidDestinoSolicitud = self.solicitudIntegral.FKidDestinoSolicitud.subscribe(function (newValue)
            {
                self.ObtenerDireccionamientosDestinosSTP_AH(newValue);
            });
        };

        self.CambioRequiereRetornoFunction = function (newValue)
        {
            var id_itinerario_seleccionado = $('#cmbItinerarios').val();
            var itinerario_seleccionado = Enumerable.From(self.itinerariosTransporte()).Where(function (x) { return x.id_itinerario == id_itinerario_seleccionado }).Select(function (x) { return x; }).SingleOrDefault();

            //Si la solcitud requiere retorno se revisa si el itineraio elegido es VESPERTINO para que de la oportunidad de regresar hasta el dia siguiente.
            if (newValue && (itinerario_seleccionado != null && itinerario_seleccionado != "undefined" && itinerario_seleccionado != ""))
            {
                var fecha_fin_retorno = Date.parseExact(self.solicitudIntegral.FechaSubida(), "dd/MM/yyyy")

                $("#txtFechaBajadaRetorno").datepicker("option", "minDate", fecha_fin_retorno.toString("dd/MM/yyyy"));

                if (itinerario_seleccionado.des_itinerario.toUpperCase().indexOf("VESPERTINO") >= 0)
                {
                    $("#txtFechaBajadaRetorno").datepicker("option", "maxDate", fecha_fin_retorno.addDays(1).toString("dd/MM/yyyy"));
                }
                else
                {
                    $("#txtFechaBajadaRetorno").datepicker("option", "maxDate", fecha_fin_retorno.toString("dd/MM/yyyy"));
                }
            }
        }

        self.ConsultarItinerariosFunction = function (newValue)
        {
            if (self.solicitudIntegral.TipoSolicitudIntegral() != "" && self.solicitudIntegral.TipoPersonal() != "" && self.solicitudIntegral.TipoTransporte() != "" && self.solicitudIntegral.TipoServicio() != "" /*&& self.solicitudIntegral.FechaSubida() != "10/26/2012" && self.solicitudIntegral.FechaBajada() != "10/27/2012"*/)
            {
                self.ConsultaItinerarios(self.itinerariosTransporte, self.solicitudIntegral.FechaSubida());
            }
        };

        self.VerDetallesEmpleado = function (empleado)
        {
            if (empleado != null && empleado != "undefined")
            {
                var vista_detalle_empleado = new VistaDetallesEmpleado();
                vista_detalle_empleado.Mostrar(mapping.toJS(empleado.IdDetalleSolicitudIntegral), self.solicitudIntegral.TipoSolicitudIntegral(), self.solicitudIntegral.TipoTransporte(), empleado.TipoPersonal(), empleado.TipoMovimientoPersonal(), self.CallBackReemplazoEmpleados, "solicitud-integral");
            }
            else
            {
                alert("Debe eleg�r un detalle de la solicitud integral para poder hacer esta consulta");
            }

            return false;
        }

        self.CallBackReemplazoEmpleados = function (detallesEmpleadosReemplazados)
        {
            $.each(detallesEmpleadosReemplazados, function (index, value)
            {
                var detalle_solicitud = Enumerable.From(self.detallesSolicitudIntegral()).Where(function (x) { return (x.IdDetalleSolicitudIntegral() == value.IdDetalleSolicitudIntegral) }).Select(function (x) { return x; }).SingleOrDefault();

                if (detalle_solicitud != null && detalle_solicitud != "nothing")
                {
                    detalle_solicitud.FichaRFCEmpleado(value.FichaRFCEmpleado);
                    detalle_solicitud.NombreEmpleado(value.NombreEmpleado);
                }
            });
        }

        self.VerListadoItinerariosDisponibles = function ()
        {
            if (self.solicitudIntegral.TipoTransporte() != "" && self.solicitudIntegral.TipoServicio() != "" && self.solicitudIntegral.FechaSubida() != "")
            {
                var vista_itinearios_disponibles = new VistaItinerariosDisponibles();

                var subdivision = self.solicitudIntegral.SubdivisionAutorizador();
                var contrato = self.solicitudIntegral.NumeroContrato();
                var folio_especial_ida = $('#txtFolioEspecialTransporteIda').val();
                var folio_especial_regreso = $('#txtFolioEspecialTransporteretorno').val();

                if (subdivision == "" || subdivision == "nothing")
                {
                    subdivision = "-"
                }

                if (contrato == "" || contrato == "nothing")
                {
                    contrato = "-"
                }

                if (folio_especial_ida == "" || folio_especial_ida == "nothing")
                {
                    folio_especial_ida = "-"
                }

                if (folio_especial_regreso == "" || folio_especial_regreso == "nothing")
                {
                    folio_especial_regreso = "-"
                }
                               
                vista_itinearios_disponibles.Mostrar(self.solicitudIntegral.TipoSolicitudIntegral(), self.solicitudIntegral.TipoTransporte(), self.solicitudIntegral.TipoServicio(), self.solicitudIntegral.FechaSubida(),subdivision,contrato ,folio_especial_ida,folio_especial_regreso, "solicitud-integral");
            }
            else
            {
                alert("Debe eleg�r un tipo de Transporte, un tipo de Servicio y una Fecha de Subida para poder hacer esta consulta");
            }

            return false;
        }

        self.VerCapacidadItinerarioDetalleIda = function ()
        {
            if (self.solicitudIntegral.TipoTransporte() != "" && self.solicitudIntegral.FechaSubida() != "" && self.solicitudIntegral.FkIdItinerarioTransporteIda() > 0)
            {
                var folio_especial_transporte = $("#txtFolioEspecialTransporteIda").val();

                var vista_itinerarios_capacidades_detalles = new VistaItinerariosCapacidadesDetalles();
                vista_itinerarios_capacidades_detalles.Mostrar(self.solicitudIntegral.TipoTransporte(), self.solicitudIntegral.FkIdItinerarioTransporteIda(), self.solicitudIntegral.FechaSubida(), self.solicitudIntegral.SubdivisionAutorizador(), (self.solicitudIntegral.TipoPersonal() == "PEMEX" ? "" : self.solicitudIntegral.NumeroContrato()), folio_especial_transporte, "solicitud-integral");
            }
            else
            {
                alert("Debe eleg�r un tipo de Transporte, Fecha de Subida/Serv., e itinerario de Ida para poder hacer esta consulta");
            }

            return false;
        }

        self.VerCapacidadHospedajePorRangoFechas = function ()
        {
            if (self.solicitudIntegral.TipoSolicitudIntegral() != "SOLO HOSPEDAJE" && self.solicitudIntegral.FechaSubida() != "" && self.solicitudIntegral.FechaBajada() != "")
            {
                var folio_especial_transporte = $("#txtFolioEspecialHospedaje").val();
                var id_instalacion_destino = $('#cmbInstalacionesDestino').val();

                var vista_capacidades_hospedaje = new VistaCapacidadHospedajePorRangoFechas();
                vista_capacidades_hospedaje.Mostrar(id_instalacion_destino, self.solicitudIntegral.FechaSubida(), self.solicitudIntegral.FechaBajada(), self.solicitudIntegral.SubdivisionAutorizador(), (self.solicitudIntegral.TipoPersonal() == "PEMEX" ? "" : self.solicitudIntegral.NumeroContrato()), folio_especial_transporte, "solicitud-integral");
            }
            else
            {
                alert("La solicitud debe incluir hospedaje y una fecha de Subida/Serv. y bajada para poder hacer esta consulta");
            }

            return false;
        }

        self.VerCapacidadItinerarioDetalleRetorno = function ()
        {
            if (self.solicitudIntegral.TipoTransporte() != "" && self.solicitudIntegral.FechaSubida() != "" && self.solicitudIntegral.FkIdItinerarioTransporteRetorno() > 0)
            {
                var folio_especial_transporte = $("#txtFolioEspecialTransporteretorno").val();

                var vista_itinerarios_capacidades_detalles = new VistaItinerariosCapacidadesDetalles();
                vista_itinerarios_capacidades_detalles.Mostrar(self.solicitudIntegral.TipoTransporte(), self.solicitudIntegral.FkIdItinerarioTransporteRetorno(), self.solicitudIntegral.FechaSubida(), self.solicitudIntegral.SubdivisionAutorizador(), (self.solicitudIntegral.TipoPersonal() == "PEMEX" ? "" : self.solicitudIntegral.NumeroContrato()), folio_especial_transporte, "solicitud-integral");
            }
            else
            {
                alert("Debe eleg�r un tipo de Transporte, Fecha de Subida/Serv., e itinerario de Retorno para poder hacer esta consulta");
            }

            return false;
        }

        self.VerPermisosAutorizador = function ()
        {
            var vista_permisos_autorizadores = new VistaPermisosAutorizador();

            if (self.solicitudIntegral.TipoSolicitudIntegral() == "SOLO TRANSPORTE")
            {
                if (mapping.toJS(self.solicitudIntegral.FichaAutorizador) != "" && self.solicitudIntegral.FechaSubida() != "" && self.solicitudIntegral.FechaSubida() != "")
                {
                    vista_permisos_autorizadores.Mostrar(self.solicitudIntegral.TipoServicio(), self.solicitudIntegral.TipoTransporte(), mapping.toJS(self.solicitudIntegral.FichaAutorizador), self.solicitudIntegral.FechaSubida(), self.solicitudIntegral.FechaSubida(), "solicitud-integral");
                }
                else
                {
                    alert("Debe proporcionar un tipo de solicitud, un tipo de transporte, una ficha de autorizador y una fecha de subida para poder hacer esta consulta");
                }
            }
            else
            {
                if (mapping.toJS(self.solicitudIntegral.FichaAutorizador) != "" && self.solicitudIntegral.FechaSubida() != "")
                {
                    vista_permisos_autorizadores.Mostrar(self.solicitudIntegral.TipoServicio(), self.solicitudIntegral.TipoTransporte(), mapping.toJS(self.solicitudIntegral.FichaAutorizador), self.solicitudIntegral.FechaBajada(), self.solicitudIntegral.FechaSubida(), "solicitud-integral");
                }
                else
                {
                    alert("Debe proporcionar un tipo de solicitud, un tipo de transporte, una ficha de autorizador, una fecha de subida y una de bajada para poder hacer esta consulta");
                }
                
            }

            return false;
        }
        
        self.DesuscribirSubscripciones = function ()
        {
            if (ko_subscripcion_solicitudIntegral_IdSolicitudIntegral != null && ko_subscripcion_solicitudIntegral_IdSolicitudIntegral != "undefined")
            {
                ko_subscripcion_solicitudIntegral_IdSolicitudIntegral.dispose();
            }
            
            if (ko_subscripcion_solicitudIntegral_TipoServicio != null && ko_subscripcion_solicitudIntegral_TipoServicio != "undefined")
            {
                ko_subscripcion_solicitudIntegral_TipoServicio.dispose();
            }

            if (ko_subscripcion_solicitudIntegral_FKidOrigenSolicitud != null && ko_subscripcion_solicitudIntegral_FKidOrigenSolicitud != "undefined")
            {
                ko_subscripcion_solicitudIntegral_FKidOrigenSolicitud.dispose();
            }
            
            if (ko_subscripcion_solicitudIntegral_FKidDestinoSolicitud != null && ko_subscripcion_solicitudIntegral_FKidDestinoSolicitud != "undefined")
            {
                ko_subscripcion_solicitudIntegral_FKidDestinoSolicitud.dispose();
            }

            if (ko_subscripcion_solicitudIntegral_TipoPersonal != null && ko_subscripcion_solicitudIntegral_TipoPersonal != "undefined")
            {
                ko_subscripcion_solicitudIntegral_TipoPersonal.dispose();
            }

            if (ko_subscripcion_solicitudIntegral_TipoTransporte != null && ko_subscripcion_solicitudIntegral_TipoTransporte != "undefined")
            {
                ko_subscripcion_solicitudIntegral_TipoTransporte.dispose();
            }

            if (ko_subscripcion_solicitudIntegral_TipoSolicitudIntegral != null && ko_subscripcion_solicitudIntegral_TipoSolicitudIntegral != "undefined")
            {
                ko_subscripcion_solicitudIntegral_TipoSolicitudIntegral.dispose();
            }

            if (ko_subscripcion_solicitudIntegral_RequiereRetorno != null && ko_subscripcion_solicitudIntegral_RequiereRetorno != "undefined")
            {
                ko_subscripcion_solicitudIntegral_RequiereRetorno.dispose();
            }
        }
        //fin: subscripciones

        self.ObtenerDireccionamientosDestinosSTP_AH = function (id_instalacion_destino)
        {
            if ((self.direccionamientosInstalacionesDisponibles != null && self.direccionamientosInstalacionesDisponibles() != "undefined") && (id_instalacion_destino != null && id_instalacion_destino != "undefined" && id_instalacion_destino > 0))
            {
                var area_servicio_transporte = self.solicitudIntegral.TipoTransporte() == "AEREO" ? "STA" : "STPVM";

                self.direccionamientosInstalacionDestinoAH(Enumerable.From(self.direccionamientosInstalacionesDisponibles()).Where(function (x) { return x.ID_C_M_INSTALACION == id_instalacion_destino && x.CVE_AREA_SERVICIO == "AH" }).Select(function (x) { return x; }).ToArray());

                if (id_instalacion_destino != 403 && self.solicitudIntegral.TipoServicio() != "LATERALES")
                {
                    //Si la instalacion destino es diferente de CME y ademas la solicitud es diferente de un movimiento interplataforma entonces
                    //se toman los direccionamientos de la instalacion destino
                    self.direccionamientosInstalacionDestinoSTP(Enumerable.From(self.direccionamientosInstalacionesDisponibles()).Where(function (x) { return x.ID_C_M_INSTALACION == id_instalacion_destino && x.CVE_AREA_SERVICIO == area_servicio_transporte }).Select(function (x) { return x; }).ToArray());
                }
                else
                {
                    //Si la instalacion destino es CMEo si la solicitud es un movimiento interplataforma entonces
                    //se toman los direccionamientos de la instalacion origen
                    var id_instalacion_origen = self.solicitudIntegral.FKidOrigenSolicitud();
                    self.direccionamientosInstalacionDestinoSTP(Enumerable.From(self.direccionamientosInstalacionesOrigenDisponibles()).Where(function (x) { return x.ID_C_M_INSTALACION == id_instalacion_origen && x.CVE_AREA_SERVICIO == area_servicio_transporte }).Select(function (x) { return x; }).ToArray());
                }

                //Aqui hace falta validar si alguno esta seleccionado.
                if (self.direccionamientosInstalacionDestinoAH().length == 1)
                {
                    self.direccionamientosInstalacionDestinoAH()[0].SELECCIONADO(true);
                    self.SeleccionarDireccionamientoHospedaje(self.direccionamientosInstalacionDestinoAH()[0]);
                }
                else
                {
                    var listado_seleccionados_ah = Enumerable.From(self.direccionamientosInstalacionDestinoAH()).Where(function (x) { return x.SELECCIONADO() == true }).Select(function (x) { return x; }).ToArray();

                    if (listado_seleccionados_ah != null && listado_seleccionados_ah != "undefined" && listado_seleccionados_ah.length > 0)
                    {
                        self.SeleccionarDireccionamientoHospedaje(listado_seleccionados_ah[0]);
                    }
                    else
                    {
                        self.direccionamientoInstalacionDestinoAHSeleccionado = null;
                    }
                }

                if (self.direccionamientosInstalacionDestinoSTP().length == 1)
                {
                    self.direccionamientosInstalacionDestinoSTP()[0].SELECCIONADO(true);
                    self.SeleccionarDireccionamientoTransporte(self.direccionamientosInstalacionDestinoSTP()[0]);
                }
                else
                {
                    var listado_seleccionados_stp = Enumerable.From(self.direccionamientosInstalacionDestinoSTP()).Where(function (x) { return x.SELECCIONADO() == true }).Select(function (x) { return x; }).ToArray();

                    if (listado_seleccionados_stp != null && listado_seleccionados_stp != "undefined" && listado_seleccionados_stp.length > 0)
                    {
                        self.SeleccionarDireccionamientoTransporte(listado_seleccionados_stp[0]);
                    }
                    else
                    {
                        self.direccionamientoInstalacionDestinoSTPSeleccionado = null;
                    }
                }
            }
            else
            {
                self.direccionamientosInstalacionDestinoAH([]);
                self.direccionamientosInstalacionDestinoSTP([]);
            }
        }//fin: ObtenerDireccionamientosDestinosSTP_AH

        //Constructor de la clase
        self.Init = function () //Inicio -- INIT()
        {
            $(document).ready(function ()
            {
                //Haciendo que los checkboxes parescan botones
                $('#radio-tipo-solicitud').buttonset();
                $('#radio-tipo-personal').buttonset();
                $('#radio-tipo-transporte').buttonset();
                $('#radio-tipo-servicio').buttonset();
                $('#radio-tipo-hospedaje').buttonset();

                $("#cmbInstalacionesOrigen").combobox();
                $("#cmbInstalacionesDestino").combobox();

                //Configurando los datetimePikers
                $('#txtFechaSubida').datepicker(
                {
                    defaultDate: "+1w", numberOfMonths: 3, stepMonths: 3, dateFormat: 'dd/mm/yy',
                    onSelect: function (selectedDate)
                    {
                        var fecha_fin_retorno = Date.parseExact(selectedDate, "dd/MM/yyyy")
                        fecha_fin_retorno = fecha_fin_retorno.addDays(1);

                        $("#txtFechaBajada").datepicker("option", "minDate", selectedDate);
                        $("#txtFechaBajadaRetorno").datepicker("option", "minDate", selectedDate);
                        $("#txtFechaBajadaRetorno").datepicker("option", "maxDate", fecha_fin_retorno.toString("dd/MM/yyyy"));
                        self.solicitudIntegral.FechaSubida(selectedDate);
                        if (Date.parse(self.solicitudIntegral.FechaBajada()) < Date.parse(selectedDate))
                        {
                            self.solicitudIntegral.FechaBajada(selectedDate);
                        }
                        self.ConsultaItinerarios(self.itinerariosTransporte, self.solicitudIntegral.FechaSubida());
                    }
                });

                $("#txtFechaSubida").datepicker("option", "minDate", 0);

                $('#txtFechaBajada').datepicker({
                    defaultDate: "+1w", numberOfMonths: 3, stepMonths: 3, dateFormat: 'dd/mm/yy',
                    onSelect: function (selectedDate)
                    {
                        self.solicitudIntegral.FechaBajada(selectedDate);
                        self.ConsultarCapacidadHospedaje();
                    }
                });

                $('#txtFechaBajadaRetorno').datepicker({
                    defaultDate: "+1w", numberOfMonths: 3, stepMonths: 3, dateFormat: 'dd/mm/yy',
                    onSelect: function (selectedDate)
                    {
                        self.solicitudIntegral.FechaBajada(selectedDate);
                        self.ConsultarCapacidadHospedaje();
                    }
                }); 

                $("#txtFechaBajada").datepicker("option", "minDate", 0);

                //Configurando pulsacion de la tecla enter para los texbox que necesitan consultas al server
                $('#txtSolicitante').bind('keypress', function (e)
                {
                    if (e.keyCode == 13)
                    {
                        self.solicitudIntegral.FichaSolicitante($('#txtSolicitante').val());
                        self.ValidarFichaSolitante(e);
                    }
                });

                $('#txtAutorizador').bind('keypress', function (e)
                {
                    if (e.keyCode == 13)
                    {
                        self.solicitudIntegral.FichaAutorizador($('#txtAutorizador').val());
                        self.ValidarAutorizador(e);
                    }
                });

                $('#txtContrato').bind('keypress', function (e)
                {
                    if (e.keyCode == 13)
                    {
                        self.solicitudIntegral.NumeroContrato($('#txtContrato').val());
                        self.ConsultarContrato(e);
                    }
                });
            });


            $("#listAgregarPersonalSubida").tagit({ beforeAddtag: self.AgregarFichaRFCPersonalSubida, tagClick: self.ModificarEmpleadoSubida });
            $("#listAgregarPersonalBajada").tagit({ beforeAddtag: self.AgregarFichaRFCPersonalBajada, tagClick: self.ModificarEmpleadoSubida });

            self.HabilitarDesabilitarControlesSegunEstadoSolicitud();

            //Se aplica el ligado
            //console.log(Date().toString() + 'Init - Init');
            ko.applyBindings(self);

            self.InicializarSubscripciones();

            //Revisando si hay alguna solicitud integral en modo Edicion.
            var datosSolicitudIntegral = $("#txtDatosSolicitudIntegral").val();
            if (datosSolicitudIntegral != null && datosSolicitudIntegral != "undefined" && datosSolicitudIntegral != "")
            {
                self.ProcesarDatosJsonSolicitudIntegral($.parseJSON(datosSolicitudIntegral).result);
            }
            $("#txtDatosSolicitudIntegral").remove();

            //MASKED INPUTS
            $("#txtFechaSubida").mask("99/99/9999");
            $("#txtFechaBajada").mask("99/99/9999");
            $("#txtHoraSalida").mask("99:99");
            $("#txtAutorizador").mask("00999999");
            $("#txtSolicitante").mask("00999999");


            $("#btnCambiarInstalacionDestino").click(function ()
            {
                $("#cmbInstalacionesDestino").combobox("toggleCombo");
            });

            $("#btnCambiarInstalacionOrigen").click(function ()
            {
                $("#cmbInstalacionesOrigen").combobox("toggleCombo");
            });

        };//fin -- INIT()

        self.ModificarEmpleadoSubida = function (ficha_rfc)
        {
            var empleado_pemex_cia = self.ValidarFichaRFC(ficha_rfc, self.solicitudIntegral.TipoPersonal, 'SUBIDA');

            if (empleado_pemex_cia != null && empleado_pemex_cia != undefined && empleado_pemex_cia.IdEmpleadoPemexCia > 0)
            {
                if (empleado_pemex_cia.TipoEmpleado != "PEMEX")
                {
                    var catalogo_empleado_cia = new CEmpleadosCia();

                    catalogo_empleado_cia.ID_C_EMPLEADO_CIA = empleado_pemex_cia.IdEmpleadoPemexCia;
                    catalogo_empleado_cia.ID_C_CONTRATO = null;
                    catalogo_empleado_cia.RFC_FM3 = empleado_pemex_cia.FichaRfc;
                    catalogo_empleado_cia.NOMBRES = empleado_pemex_cia.Nombres;
                    catalogo_empleado_cia.APELLIDOS = empleado_pemex_cia.Apellidos;
                    catalogo_empleado_cia.CATEGORIA = empleado_pemex_cia.Categoria;
                    catalogo_empleado_cia.GENERO = empleado_pemex_cia.Genero;
                    catalogo_empleado_cia.NACIONALIDAD = empleado_pemex_cia.Nacionalidad;
                    catalogo_empleado_cia.CURP = empleado_pemex_cia.CURP;
                    catalogo_empleado_cia.FOLIO_LIBRETA_MAR = empleado_pemex_cia.FolioLibretaMar;
                    catalogo_empleado_cia.VIGENCIA_LIBRETA_MAR = parseDate(empleado_pemex_cia.VigenciaLibretaMar);
                    catalogo_empleado_cia.BORRADO_LOGICO = false;

                    var vista_catalogo_empleados_cia = new VistaCatalogoEmpleadoCia();

                    vista_catalogo_empleados_cia.MostrarCatalogo(catalogo_empleado_cia, "solicitud-integral", "Modificar", null);
                }
                else
                {
                    var catalogo_empleado_pemex = new CEmpleadosPemex();

                    catalogo_empleado_pemex.ID_C_EMPLEADO_PEMEX = empleado_pemex_cia.IdEmpleadoPemexCia;
                    catalogo_empleado_pemex.ID_C_CONTRATO = null;
                    catalogo_empleado_pemex.FICHA = empleado_pemex_cia.FichaRfc;
                    catalogo_empleado_pemex.RFC = "";
                    catalogo_empleado_pemex.CURP = "";
                    catalogo_empleado_pemex.NOMBRES = empleado_pemex_cia.Nombres;
                    catalogo_empleado_pemex.APELLIDOS = empleado_pemex_cia.Apellidos;
                    catalogo_empleado_pemex.CATEGORIA = empleado_pemex_cia.Categoria;
                    catalogo_empleado_pemex.CARGO = "";
                    catalogo_empleado_pemex.GENERO = empleado_pemex_cia.Genero
                    catalogo_empleado_pemex.EXTENSION_TELEFONICA = "";
                    catalogo_empleado_pemex.EMAIL = "";
                    catalogo_empleado_pemex.BORRADO_LOGICO = false;

                    var vista_catalogo_empleados_pemex = new VistaCatalogoEmpleadoPemex();

                    vista_catalogo_empleados_pemex.MostrarCatalogo(catalogo_empleado_pemex, "solicitud-integral", "Modificar", null);
                }
            }
        };

        self.AltaEmpleados = function (tipo_movimiento)
        {
            //VistaCatalogoEmpleadoCia, VistaCatalogoEmpleadoPemex,  CEmpleadosCia, CEmpleadosPemex
            //Hace falta validar que el catalogo sea de PEMEX o CIA dependiendo el tipo de Solicitud integral
            if (self.solicitudIntegral.TipoPersonal() == "PEMEX")
            {
                var catalogo_empleado_pemex = new CEmpleadosPemex();
                var vista_catalogo_empleados_pemex = new VistaCatalogoEmpleadoPemex();

                if (tipo_movimiento == "SUBIDA")
                {
                    vista_catalogo_empleados_pemex.MostrarCatalogo(catalogo_empleado_pemex, "solicitud-integral", "Nuevo", self.CallBackVistaEmpleadosPemexSubida);
                }
                else
                {
                    vista_catalogo_empleados_pemex.MostrarCatalogo(catalogo_empleado_pemex, "solicitud-integral", "Nuevo", self.CallBackVistaEmpleadosPemexBajada);
                }
            }
            else
            {
                var catalogo_empleado_cia = new CEmpleadosCia();
                var vista_catalogo_empleados_cia = new VistaCatalogoEmpleadoCia();

                if (tipo_movimiento == "SUBIDA")
                {
                    vista_catalogo_empleados_cia.MostrarCatalogo(catalogo_empleado_cia, "solicitud-integral", "Nuevo", self.CallBackVistaEmpleadosCiaSubida);
                }
                else
                {
                    vista_catalogo_empleados_cia.MostrarCatalogo(catalogo_empleado_cia, "solicitud-integral", "Nuevo", self.CallBackVistaEmpleadosCiaBajada);
                }
            }
            return false;
        };

        self.CallBackVistaEmpleadosCiaSubida = function (empleadoCia, operacion)
        {
            switch (operacion)
            {
                case "Nuevo":
                {
                    $("#listAgregarPersonalSubida").tagit("add", { label: empleadoCia.RFC_FM3, value: empleadoCia.RFC_FM3 })
                    break;
                }
            }
        };

        self.CallBackVistaEmpleadosPemexSubida = function (empleadoPemex, operacion)
        {
            switch (operacion)
            {
                case "Nuevo":
                {
                    $("#listAgregarPersonalSubida").tagit("add", { label: empleadoPemex.FICHA, value: empleadoPemex.FICHA })
                    break;
                }
            }
        };

        self.CallBackVistaEmpleadosCiaBajada = function (empleadoCia, operacion)
        {
            switch (operacion)
            {
                case "Nuevo":
                {
                    $("#listAgregarPersonalBajada").tagit("add", { label: empleadoCia.RFC_FM3, value: empleadoCia.RFC_FM3 })
                    break;
                }
            }
        };

        self.CallBackVistaEmpleadosPemexBajada = function (empleadoPemex, operacion)
        {
            switch (operacion)
            {
                case "Nuevo":
                {
                    $("#listAgregarPersonalBajada").tagit("add", { label: empleadoPemex.FICHA, value: empleadoPemex.FICHA })
                    break;
                }
            }
        };

        self.NuevaSolicitudIntegral = function ()
        {
            self.FolioSolicitudIntegralBusqueda("");

            self.solicitudIntegral = mapping.fromJS(new SolicitudIntegral());
            self.detallesSolicitudIntegral = ko.observableArray([]);

            self.contratoSap = mapping.fromJS(new VwCContratos());
            self.itinerariosTransporte([]);

            self.instalacionesOrigenDisponibles([]);
            self.instalacionesDestinoDisponibles([]);

            self.direccionamientosInstalacionesDisponibles([]);

            self.direccionamientosInstalacionDestinoAH([]);
            self.direccionamientosInstalacionDestinoSTP([]);

            self.direccionamientoInstalacionDestinoAHSeleccionado = null;
            self.direccionamientoInstalacionDestinoSTPSeleccionado = null;

            self.HabilitarDesabilitarControlesSegunEstadoSolicitud();

            $("#lblCapacidadTransporteRetorno").text("0");
            $("#lblCapacidadTransporte").text("0");
            $("#lblCapacidadHospedaje").text("0");

            $("#listAgregarPersonalSubida").tagit("reset");
            $("#listAgregarPersonalBajada").tagit("reset");

            //Se aplica el ligado
            ////console.log(Date().toString() + 'NuevaSolicitudIntegral() - ko.applyBindings');
            //ko.applyBindings(self);

            self.InicializarSubscripciones();
        };

        self.MostrarCalendarioFechaSubida = function ()
        {
            $('#txtFechaSubida').datepicker("show");
        };

        self.MostrarCalendarioFechaBajada = function ()
        {
            $('#txtFechaBajada').datepicker("show");
        };

        self.MostrarCalendarioFechaBajadaRetorno = function ()
        {
            $('#txtFechaBajadaRetorno').datepicker("show");
        };

        self.AgregarFichaRFCPersonalBajada = function (e, ev)
        {
            self.AgregarFichaRFC(e,"Bajada")
        }

        self.AgregarFichaRFCPersonalSubida = function (e, ev)
        {
            self.AgregarFichaRFC(e, "Subida")

            return e.allowAddTag;
        }

        self.AgregarFichaRFC = function(e, movimiento_personal)
        {
            if (e.labelSplit != true)
            {
                if (e.label != "undefined" && e.label != "")
                {
                    var empleado_pemex_cia = self.ValidarFichaRFC(e.label, self.solicitudIntegral.TipoPersonal, movimiento_personal);
                    if (empleado_pemex_cia != null && empleado_pemex_cia != undefined && empleado_pemex_cia.IdEmpleadoPemexCia > 0)
                    {
                        e.label = "[" + empleado_pemex_cia.FichaRfc + "] " + empleado_pemex_cia.NombreCompleto;
                        e.value = empleado_pemex_cia.FichaRfc;


                        if (empleado_pemex_cia.TipoEmpleado != "PEMEX" && (Date.parse(parseDate(empleado_pemex_cia.VigenciaLibretaMar)) < Date.today() || empleado_pemex_cia.FolioLibretaMar == ""))
                        {
                            var catalogo_empleado_cia = new CEmpleadosCia();

                            catalogo_empleado_cia.ID_C_EMPLEADO_CIA = empleado_pemex_cia.IdEmpleadoPemexCia;
                            catalogo_empleado_cia.ID_C_CONTRATO = null;
                            catalogo_empleado_cia.RFC_FM3 = empleado_pemex_cia.FichaRfc;
                            catalogo_empleado_cia.NOMBRES = empleado_pemex_cia.Nombres;
                            catalogo_empleado_cia.APELLIDOS = empleado_pemex_cia.Apellidos;
                            catalogo_empleado_cia.CATEGORIA = empleado_pemex_cia.Categoria;
                            catalogo_empleado_cia.GENERO = empleado_pemex_cia.Genero;
                            catalogo_empleado_cia.NACIONALIDAD = empleado_pemex_cia.Nacionalidad;
                            catalogo_empleado_cia.CURP = empleado_pemex_cia.CURP;
                            catalogo_empleado_cia.FOLIO_LIBRETA_MAR = empleado_pemex_cia.FolioLibretaMar;
                            catalogo_empleado_cia.VIGENCIA_LIBRETA_MAR = parseDate(empleado_pemex_cia.VigenciaLibretaMar);
                            catalogo_empleado_cia.BORRADO_LOGICO = false;

                            var vista_catalogo_empleados_cia = new VistaCatalogoEmpleadoCia();

                            //Hay que actualizar la libreta de mar y vigencia.

                            if (movimiento_personal == "Subida")
                            {
                                vista_catalogo_empleados_cia.MostrarCatalogo(catalogo_empleado_cia, "solicitud-integral", "Nuevo", self.CallBackVistaEmpleadosCiaSubida);
                            }
                            else
                            {
                                vista_catalogo_empleados_cia.MostrarCatalogo(catalogo_empleado_cia, "solicitud-integral", "Nuevo", self.CallBackVistaEmpleadosCiaBajada);
                            }

                            e.label = "";
                            e.value = "";

                            e.allowAddTag = false;
                        }
                        else
                        {
                            e.allowAddTag = true;
                        }
                    }
                    else
                    {
                        var answer = confirm('No se encontro ningun empleado con la ficha: ' + e.label + ' \xBFDesea darlo de alta?')

                        if (answer)
                        {

                            if (self.solicitudIntegral.TipoPersonal() == "PEMEX")
                            {
                                var catalogo_empleado_pemex = new CEmpleadosPemex();

                                catalogo_empleado_pemex.FICHA = e.label;

                                var vista_catalogo_empleados_pemex = new VistaCatalogoEmpleadoPemex();

                                if (movimiento_personal == "Subida")
                                {
                                    vista_catalogo_empleados_pemex.MostrarCatalogo(catalogo_empleado_pemex, "solicitud-integral", "Nuevo", self.CallBackVistaEmpleadosPemexSubida);
                                }
                                else
                                {
                                    vista_catalogo_empleados_pemex.MostrarCatalogo(catalogo_empleado_pemex, "solicitud-integral", "Nuevo", self.CallBackVistaEmpleadosPemexBajada);
                                }
                            }
                            else
                            {
                                var catalogo_empleado_cia = new CEmpleadosCia();

                                catalogo_empleado_cia.RFC_FM3 = e.label;

                                var vista_catalogo_empleados_cia = new VistaCatalogoEmpleadoCia();

                                if (movimiento_personal == "Subida")
                                {
                                    vista_catalogo_empleados_cia.MostrarCatalogo(catalogo_empleado_cia, "solicitud-integral", "Nuevo", self.CallBackVistaEmpleadosCiaSubida);
                                }
                                else
                                {
                                    vista_catalogo_empleados_cia.MostrarCatalogo(catalogo_empleado_cia, "solicitud-integral", "Nuevo", self.CallBackVistaEmpleadosCiaBajada);
                                }
                            }
                        }
                        e.allowAddTag = false;
                    }
                }
                else
                {
                    e.allowAddTag = false;
                }
            }
            else
            {
                e.allowAddTag = true;
            }

            return true;
        }; //fin: AgregarFichaRFC

        self.ValidarFichaRFC = function (fichaRfc, tipoEmpleado, movimiento_personal)
        {
            var datos_empleado = null;

            if ($.trim(fichaRfc) == '')
            {
                ShowException("Debe especificar una ficha/rfc para la busqueda del empleado");
            }
            else
            {

                $.ajax({
                    url: rootDir + "SolicitudIntegral/ValidarFichaRFC/",
                    dataType: "json",
                    data:
                    {
                        ficha_rfc: fichaRfc,
                        tipo_empleado: tipoEmpleado,
                    },
                    async: false,
                    success: function (response, status, xhr)
                    {
                        var ex = HandleException(response, status, xhr);

                        if (ex == null)
                        {
                            if (response.result != null || response.result != "undefined")
                            {
                                datos_empleado = new EmpleadoPemexCia(response.result);
                            }
                        }
                        else
                        {
                            //Error
                            ShowException(ex);
                        }
                    },
                    beforeSend: function ()
                    {
                        if (movimiento_personal == "Subida")
                        {
                            $("#image-loading-personal-subida").show();
                        }
                        else
                        {
                            $("#image-loading-personal-bajada").show();
                        }
                    },
                    complete: function ()
                    {
                        if (movimiento_personal == "Subida")
                        {
                            $("#image-loading-personal-subida").hide();
                        }
                        else
                        {
                            $("#image-loading-personal-bajada").hide();
                        }
                    }
                });
            }

            return datos_empleado;
        } // fin: ValidarFichaRFC

        self.ObtenerDatosSolicitudIntegralEditar = function ()//inicio ValidarFichaSolicitante()
        {
            if ($.trim(self.FolioSolicitudIntegralBusqueda()) == "")
            {
                ShowException("Debe especificar un folio de Solicitud Integral");
            }
            else
            {
                $.ajax({
                    url: rootDir + "SolicitudIntegral/EditarSolicitudIntegral/",
                    dataType: "json",
                    data:
                    {
                        folioSolicitudIntegral: self.FolioSolicitudIntegralBusqueda()
                    },
                    success: function (response, status, xhr)
                    {
                        var ex = HandleException(response, status, xhr);
                        if (ex == null)
                        {
                            if (response != null)
                            {
                                self.ProcesarDatosJsonSolicitudIntegral(response.result);
                            }
                        }
                        else
                        {
                            //Error
                            ShowException(ex);
                        }
                    },
                    beforeSend: function ()
                    {
                        $("#image-loading-cargar-datos-solicitud").show();
                    },
                    complete: function ()
                    {
                        $("#image-loading-cargar-datos-solicitud").hide();
                    }
                });
            }
        };//fin ValidarFichaSolicitante()

        self.ProcesarDatosJsonSolicitudIntegral = function (solicitudIntegralJson)
        {
            if (solicitudIntegralJson.SolicitudNueva != "undefined" && solicitudIntegralJson.SolicitudNueva != null && solicitudIntegralJson.SolicitudNueva == true)
            {
                //Se trata de una solicitud nueva
                self.solicitudIntegral.FichaSolicitante(solicitudIntegralJson.FichaSolicitante);
                self.solicitudIntegral.NombreSolicitante(solicitudIntegralJson.NombreSolicitante);
                self.solicitudIntegral.SubdivisionSolicitante(solicitudIntegralJson.SubdivisionSolicitante);
                self.solicitudIntegral.DescripcionSubdivisionSolicitante(solicitudIntegralJson.DescripcionSubdivisionSolicitante);
            }
            else
            {

                //var solicitud_integral = new SolicitudIntegral();
                var detalles_solicitud_integral = ko.utils.arrayMap(solicitudIntegralJson.DetallesSolicitudIntegral, function (item) { return mapping.fromJS(new DetalleSolicitudIntegral(item)) });
                var contrato_sap = new VwCContratos(solicitudIntegralJson.ContratoSap);
                var itinearios_transporte = ko.utils.arrayMap(solicitudIntegralJson.ItinerariosTransporte, function (item) { return new ItinerarioTransporte(item) });

                var instalaciones_origen = ko.utils.arrayMap(solicitudIntegralJson.InstalacionesOrigen, function (item) { return new CMInstalaciones(item) });
                var instalaciones_destino = ko.utils.arrayMap(solicitudIntegralJson.InstalacionDestino, function (item) { return new CMInstalaciones(item) });

                var direccionamientos_instalaciones_destino = ko.utils.arrayMap(solicitudIntegralJson.ListadoDireccionamientosDestinos, function (item) { return new VwPermisosDireccionamientosGeneral(item) });
                var direccionamientos_instalaciones_origen = ko.utils.arrayMap(solicitudIntegralJson.ListadoDireccionamientosOrigenes, function (item) { return new VwPermisosDireccionamientosGeneral(item) });

                self.DesuscribirSubscripciones();

                self.ActualizarSolicitudIntegral(solicitudIntegralJson.SolicitudIntegral);
                self.detallesSolicitudIntegral(detalles_solicitud_integral);

                //self.solicitudIntegral.FechaSubida(self.solicitudIntegral.FechaSubidaFormateada());
                //self.solicitudIntegral.FechaBajada(self.solicitudIntegral.FechaBajadaFormateada());

                if (contrato_sap != null && contrato_sap != "undefined")
                {
                    self.contratoSap = mapping.fromJS(contrato_sap);
                }

                if (itinearios_transporte != null && itinearios_transporte != "undefined")
                {
                    self.itinerariosTransporte(itinearios_transporte);
                    $("#cmbItinerarios").val(solicitudIntegralJson.SolicitudIntegral.FkIdItinerarioTransporteIda);

                    if (solicitudIntegralJson.SolicitudIntegral.FkIdItinerarioTransporteRetorno != null && solicitudIntegralJson.SolicitudIntegral.FkIdItinerarioTransporteRetorno != "undefined" && solicitudIntegralJson.SolicitudIntegral.FkIdItinerarioTransporteRetorno > 0)
                    {
                        $("#cmbItinerariosRetorno").val(solicitudIntegralJson.SolicitudIntegral.FkIdItinerarioTransporteRetorno);
                    }

                    self.solicitudIntegral.FkIdItinerarioTransporteIda(solicitudIntegralJson.SolicitudIntegral.FkIdItinerarioTransporteIda);
                    self.solicitudIntegral.FkIdItinerarioTransporteRetorno(solicitudIntegralJson.SolicitudIntegral.FkIdItinerarioTransporteRetorno);
                }

                if (direccionamientos_instalaciones_destino != null && direccionamientos_instalaciones_destino != "undefined")
                {
                    self.direccionamientosInstalacionesDisponibles(direccionamientos_instalaciones_destino);
                }

                if (direccionamientos_instalaciones_origen != null && direccionamientos_instalaciones_origen != "undefined")
                {
                    self.direccionamientosInstalacionesOrigenDisponibles(direccionamientos_instalaciones_origen);
                }

                if (instalaciones_origen != null && instalaciones_origen != "undefined")
                {
                    self.instalacionesOrigenDisponibles(instalaciones_origen);

                    self.solicitudIntegral.FKidOrigenSolicitud(solicitudIntegralJson.SolicitudIntegral.FKidOrigenSolicitud);
                    $("#cmbInstalacionesOrigen").val(self.solicitudIntegral.FKidOrigenSolicitud());
                    $("#cmbInstalacionesOrigen").trigger("change");
                    //$("#cmbInstalacionesOrigen").val(solicitudIntegralJson.SolicitudIntegral.FKidOrigenSolicitud);
                }


                if (instalaciones_destino != null && instalaciones_destino != "undefined")
                {
                    self.instalacionesDestinoDisponibles(instalaciones_destino);

                    self.solicitudIntegral.FKidDestinoSolicitud(solicitudIntegralJson.SolicitudIntegral.FKidDestinoSolicitud);
                    $("#cmbInstalacionesDestino").val(self.solicitudIntegral.FKidDestinoSolicitud());
                    $("#cmbInstalacionesDestino").trigger("change"); //Esto es necesario para que el autoacomplete detecte el cambio

                    //$("#cmbInstalacionesDestino").val(solicitudIntegralJson.SolicitudIntegral.FKidDestinoSolicitud);

                    if (self.solicitudIntegral.FKidDestinoSolicitud() != null && self.solicitudIntegral.FKidDestinoSolicitud() != "undefined" && self.solicitudIntegral.FKidDestinoSolicitud() > 0)
                    {
                        self.ObtenerDireccionamientosDestinosSTP_AH(self.solicitudIntegral.FKidDestinoSolicitud());
                    }
                }

                $('#radio-tipo-solicitud').buttonset("refresh");
                $('#radio-tipo-personal').buttonset("refresh");
                $('#radio-tipo-transporte').buttonset("refresh");
                $('#radio-tipo-servicio').buttonset("refresh");

                self.InicializarSubscripciones();

                self.HabilitarDesabilitarControlesSegunEstadoSolicitud();

                self.ConsultarCapacidadTransporte();
                self.ConsultarCapacidadTransporteRetorno();

                self.ConsultarCapacidadHospedaje();

                //TODO: Verificar 
                //ko.applyBindings(self, $('#linkImprimirSolicitud')[0]);
                //ko.applyBindings(self, $('#linkImprimirBoleto')[0]);
            }
        };//fin: ProcesarDatosJsonSolicitudIntegral

        self.ValidarFichaSolitante = function ()//inicio ValidarFichaSolicitante()
        {
            if ($.trim(self.solicitudIntegral.FichaSolicitante()) == "")
            {
                ShowException("Debe especificar una ficha para el usuario Solicitante");
            }
            else
            {
                $.ajax({
                    url: rootDir + "SolicitudIntegral/ValidarSolicitante/",
                    dataType: "json",
                    data:
                    {
                        ficha_solictante: self.solicitudIntegral.FichaSolicitante(),
                        tipo_solicitud: self.solicitudIntegral.TipoSolicitudIntegral,
                        tipo_transporte: self.solicitudIntegral.TipoTransporte()
                    },
                    success: function (response, status, xhr)
                    {
                        var ex = HandleException(response, status, xhr);
                        if (ex == null)
                        {
                            self.solicitudIntegral.FichaSolicitante(response.result.FICHA);
                            self.solicitudIntegral.NombreSolicitante(response.result.NOMBRE);
                            self.solicitudIntegral.SubdivisionSolicitante(response.result.SUBDIVISION);
                            self.solicitudIntegral.DescripcionSubdivisionSolicitante(response.result.DESCRIPCION_SUBDIVISION);
                        }
                        else
                        {
                            //Error
                            ShowException(ex);
                        }
                    },
                    beforeSend: function ()
                    {
                        $("#image-loading-solicitante").show();
                    },
                    complete: function ()
                    {
                        $("#image-loading-solicitante").hide();
                    }
                });
            }
        };//fin ValidarFichaSolicitante()

        self.ValidarAutorizador = function ()//inicio ValidarAutorizador()
        {
            if ($.trim(self.solicitudIntegral.FichaAutorizador()) == "")
            {
                ShowException("Debe especificar una ficha para el usuario Autorizador");
            }
            else
            {
                $.ajax({
                    url: rootDir + "SolicitudIntegral/ValidarAutorizador/",
                    dataType: "json",
                    data:
                    {
                        ficha_autorizador: self.solicitudIntegral.FichaAutorizador(),
                        fecha_subida: self.solicitudIntegral.FechaSubida(),
                        fecha_bajada: self.solicitudIntegral.FechaBajada(),
                        tipo_solicitud: self.solicitudIntegral.TipoSolicitudIntegral(),
                        tipo_transporte: self.solicitudIntegral.TipoTransporte()
                    },
                    success: function (response, status, xhr)
                    {
                        var ex = HandleException(response, status, xhr);
                        if (ex == null)
                        {
                            if (response.result.FICHA == null || response.result.FICHA == '')
                            {
                                self.solicitudIntegral.FichaAutorizador(null);
                                self.solicitudIntegral.NombreAutorizador("");
                                self.solicitudIntegral.SubdivisionAutorizador("");
                                self.solicitudIntegral.DescripcionSubdivisionAutorizador("");
                            }
                            else
                            {
                                self.solicitudIntegral.FichaAutorizador(response.result.FICHA);
                                self.solicitudIntegral.NombreAutorizador(response.result.NOMBRE);
                                self.solicitudIntegral.SubdivisionAutorizador(response.result.SUBDIVISION);
                                self.solicitudIntegral.DescripcionSubdivisionAutorizador(response.result.DESCRIPCION_SUBDIVISION);
                            }

                            self.ConsultarInstalacionesDestinos();
                        }
                        else
                        {
                            //Error
                            ShowException(ex);
                        }
                    },
                    beforeSend: function ()
                    {
                        $("#image-loading-autorizador").show();
                    },
                    complete: function ()
                    {
                        $("#image-loading-autorizador").hide();
                    }
                });
            }
        };//fin ValidarAutorizador()

        //self.ConsultaItinerarios = function ()//inicio ConsultarItinerarios()
        //{
        //    $.ajax({
        //        url: rootDir + "SolicitudIntegral/ConsultarItinerarios/",
        //        dataType: "json",
        //        data:
        //        {
        //            tipo_transporte: self.solicitudIntegral.TipoTransporte(),
        //            tipo_servicio: self.solicitudIntegral.TipoServicio,
        //            fecha_servicio: self.solicitudIntegral.FechaSubida()
        //        },
        //        success: function (response, status, xhr)
        //        {
        //            var ex = HandleException(response, status, xhr);
        //            if (ex == null)
        //            {
        //                var mappedTasks = ko.utils.arrayMap(response.result, function (item) { return new ItinerarioTransporte(item) });
        //                self.itinerariosTransporte(mappedTasks);

        //                if (self.itinerariosTransporte != null && self.itinerariosTransporte != "undefined" && self.itinerariosTransporte().length <= 0)
        //                {
        //                    //Si no hay ningun itinerario seleccionado entonces se resetean las instalaciones y los direccionamientos.
        //                    self.BorrarInstalacionesOrigenDestinosDireccionamientos();
        //                }

        //                $("#lblEspaciosDisponiblesFolioEspecialTransporteIda").val("");
        //                $("#lblEspaciosDisponiblesFolioEspecialTransporteRetorno").val("");
        //            }
        //            else
        //            {
        //                //Error
        //                ShowException(ex);
        //            }
        //        },
        //        beforeSend: function ()
        //        {
        //            $("#image-loading-itinerarios").show();
        //            $("#image-loading-itinerarios-retorno").show();
        //        },
        //        complete: function ()
        //        {
        //            $("#image-loading-itinerarios").hide();
        //            $("#image-loading-itinerarios-retorno").hide();
        //        }
        //    });
        //};//fin ConsultarItinerarios()

        self.ConsultaItinerarios = function (listado_itinerarios, fecha_subida)//inicio ConsultarItinerarios()
        {
            $.ajax({
                url: rootDir + "SolicitudIntegral/ConsultarItinerarios/",
                dataType: "json",
                data:
                {
                    tipo_solicitud_integral: self.solicitudIntegral.TipoSolicitudIntegral(),
                    tipo_transporte: self.solicitudIntegral.TipoTransporte(),
                    tipo_servicio: self.solicitudIntegral.TipoServicio,
                    fecha_servicio: fecha_subida
                },
                success: function (response, status, xhr)
                {
                    var ex = HandleException(response, status, xhr);
                    if (ex == null)
                    {
                        var mappedTasks = ko.utils.arrayMap(response.result, function (item) { return new ItinerarioTransporte(item) });
                        listado_itinerarios(mappedTasks);

                        if (listado_itinerarios != null && listado_itinerarios != "undefined" && listado_itinerarios().length <= 0)
                        {
                            //Si no hay ningun itinerario seleccionado entonces se resetean las instalaciones y los direccionamientos.
                            self.BorrarInstalacionesOrigenDestinosDireccionamientos();
                        }

                        $("#lblEspaciosDisponiblesFolioEspecialTransporteIda").val("");
                        $("#lblEspaciosDisponiblesFolioEspecialTransporteRetorno").val("");
                    }
                    else
                    {
                        //Error
                        ShowException(ex);
                    }
                },
                beforeSend: function ()
                {
                    $("#image-loading-itinerarios").show();
                    $("#image-loading-itinerarios-retorno").show();
                },
                complete: function ()
                {
                    $("#image-loading-itinerarios").hide();
                    $("#image-loading-itinerarios-retorno").hide();
                }
            });
        };//fin ConsultarItinerarios()

        self.ConsultarContrato = function ()//inicio ConsultarContrato()
        {
            if ($.trim(self.solicitudIntegral.NumeroContrato()) == "")
            {
                ShowException("Debe especificar un contrato para poder hacer la consulta");
            }
            else
            {
                $.ajax({
                    url: rootDir + "SolicitudIntegral/ConsultarContrato/",
                    dataType: "json",
                    data:
                    {
                        numero_contrato: self.solicitudIntegral.NumeroContrato(),
                        fecha_incio: self.solicitudIntegral.FechaSubida(), 
                        fecha_fin: self.solicitudIntegral.FechaBajada()
                    },
                    success: function (response, status, xhr)
                    {
                        var ex = HandleException(response, status, xhr);
                        if (ex == null)
                        {
                            self.contratoSap = mapping.fromJS(new VwCContratos(response.result));

                            self.solicitudIntegral.NombreAcreedor(self.contratoSap.DESCRIPCION_ACREEDOR());
                            self.solicitudIntegral.RFCAcreedor(self.contratoSap.RFC_ACREEDOR());
                        }
                        else
                        {
                            //Error
                            ShowException(ex);
                        }
                    },
                    beforeSend: function ()
                    {
                        $("#image-loading-contrato").show();
                    },
                    complete: function ()
                    {
                        $("#image-loading-contrato").hide();
                    }
                });
            }
        };//fin ConsultarContrato()

        self.ConsultarInstalacionesOrigen = function ()//inicio ConsultarInstalacionesOrigen()
        {
            $.ajax({
                url: rootDir + "SolicitudIntegral/ConsultarInstalacionesOrigen/",
                dataType: "json",
                data:
                {
                    tipo_solicitud: self.solicitudIntegral.TipoSolicitudIntegral(),
                    tipo_personal: self.solicitudIntegral.TipoPersonal(),
                    tipo_transporte: self.solicitudIntegral.TipoTransporte(),
                    tipo_servicio: self.solicitudIntegral.TipoServicio(),
                    id_itinerario: self.solicitudIntegral.FkIdItinerarioTransporteIda(),
                    ficha_autorizador: self.solicitudIntegral.FichaAutorizador(),
                    fecha_subida: self.solicitudIntegral.FechaSubida(),
                    fecha_bajada: self.solicitudIntegral.FechaBajada(),
                },
                success: function (response, status, xhr)
                {
                    var ex = HandleException(response, status, xhr);
                    if (ex == null)
                    {
                        var instalaciones_origen = ko.utils.arrayMap(response.result.InstalacionesOrigen, function (item) { return new CMInstalaciones(item) });
                        var direccionamientos_instalaciones = ko.utils.arrayMap(response.result.DireccionamientosInstalaciones, function (item) { return new VwPermisosDireccionamientosGeneral(item) });

                        self.instalacionesOrigenDisponibles(instalaciones_origen);
                        self.direccionamientosInstalacionesOrigenDisponibles(direccionamientos_instalaciones);

                        if (instalaciones_origen.length == 1)
                        {
                            $("#cmbInstalacionesOrigen").val(instalaciones_origen[0].ID_C_M_INSTALACION);
                            $("#cmbInstalacionesOrigen").trigger("change"); //Esto es una agregado para el autoacomplete
                            self.solicitudIntegral.FKidOrigenSolicitud(instalaciones_origen[0].ID_C_M_INSTALACION)
                        }
                    }
                    else
                    {
                        //Error
                        ShowException(ex);
                    }
                },
                beforeSend: function ()
                {
                    $("#image-loading-instalacion-origen").show();
                },
                complete: function ()
                {
                    $("#image-loading-instalacion-origen").hide();
                }
            });
        };//fin ConsultarInstalacionesOrigen()

        self.ConsultarCapacidadTransporte = function ()//inicio ConsultarCapacidadTransporte()
        {
            if (self.solicitudIntegral.TipoSolicitudIntegral() != "SOLO HOSPEDAJE")
            {
                if (self.solicitudIntegral.FkIdItinerarioTransporteIda() == null || self.solicitudIntegral.FkIdItinerarioTransporteIda() == "undefined")
                {
                    self.solicitudIntegral.FkIdItinerarioTransporteIda($("#cmbItinerarios").val());
                }

                if (self.solicitudIntegral.FkIdItinerarioTransporteIda() != null && self.solicitudIntegral.FkIdItinerarioTransporteIda() != "undefined")
                {
                    var folioEspecialTransporte = $("#txtFolioEspecialTransporteIda").val();

                    if (folioEspecialTransporte != "")
                    {
                        self.ConsultaCapacidadFolioEspecialTransporte(folioEspecialTransporte, self.solicitudIntegral.FkIdItinerarioTransporteIda(), self.solicitudIntegral.FechaSubida(), "lblEspaciosDisponiblesFolioEspecialTransporteIda", "image-loading-consulta-folio-especial-transporte-ida");
                    }

                    $.ajax({
                        url: rootDir + "SolicitudIntegral/ObtenerCapacidadTransportePorItem/",
                        dataType: "json",
                        data:
                        {
                            id_itinerario: self.solicitudIntegral.FkIdItinerarioTransporteIda(),
                            fecha_subida: self.solicitudIntegral.FechaSubida(),
                            folio_especial_transporte: folioEspecialTransporte,
                            no_contrato: self.solicitudIntegral.NumeroContrato(),
                            subdivision_autorizador: self.solicitudIntegral.SubdivisionAutorizador()
                        },
                        success: function (response, status, xhr)
                        {
                            var ex = HandleException(response, status, xhr);
                            if (ex == null)
                            {
                                $("#lblCapacidadTransporte").text(response.result);
                            }
                            else
                            {
                                //Error
                                ShowException(ex);
                            }
                        },
                        beforeSend: function ()
                        {
                            $("#image-loading-capacidad-transporte").show();
                        },
                        complete: function ()
                        {
                            $("#image-loading-capacidad-transporte").hide();
                        }
                    });
                }
            }
        };//fin ConsultarCapacidadTransporte()

        self.ConsultarCapacidadTransporteRetorno = function ()//inicio ConsultarCapacidadTransporteRetorno()
        {
            if (self.solicitudIntegral.TipoSolicitudIntegral() != "SOLO HOSPEDAJE")
            {
                if (self.solicitudIntegral.FkIdItinerarioTransporteRetorno() == null || self.solicitudIntegral.FkIdItinerarioTransporteRetorno() == "undefined")
                {
                    self.solicitudIntegral.FkIdItinerarioTransporteRetorno($("#cmbItinerariosRetorno").val());
                }

                if (self.solicitudIntegral.FkIdItinerarioTransporteRetorno() != null && self.solicitudIntegral.FkIdItinerarioTransporteRetorno() != "undefined")
                {

                    var folioEspecialTransporte = $("#txtFolioEspecialTransporteretorno").val();

                    if (folioEspecialTransporte != "")
                    {
                        self.ConsultaCapacidadFolioEspecialTransporte(folioEspecialTransporte, self.solicitudIntegral.FkIdItinerarioTransporteRetorno(), self.solicitudIntegral.FechaSubida(), "lblEspaciosDisponiblesFolioEspecialTransporteRetorno", "image-loading-consulta-folio-especial-transporte-retorno");
                    }

                    $.ajax({
                        url: rootDir + "SolicitudIntegral/ObtenerCapacidadTransportePorItem/",
                        dataType: "json",
                        data:
                        {
                            id_itinerario: self.solicitudIntegral.FkIdItinerarioTransporteRetorno(),
                            fecha_subida: self.solicitudIntegral.FechaSubida(),
                            folio_especial_transporte: folioEspecialTransporte,
                            no_contrato: self.solicitudIntegral.NumeroContrato(),
                            subdivision_autorizador: self.solicitudIntegral.SubdivisionAutorizador()
                        },
                        success: function (response, status, xhr)
                        {
                            var ex = HandleException(response, status, xhr);
                            if (ex == null)
                            {
                                $("#lblCapacidadTransporteRetorno").text(response.result);
                            }
                            else
                            {
                                //Error
                                ShowException(ex);
                            }
                        },
                        beforeSend: function ()
                        {
                            $("#image-loading-capacidad-transporte-retorno").show();
                        },
                        complete: function ()
                        {
                            $("#image-loading-capacidad-transporte-retorno").hide();
                        }
                    });
                }
            }
        };//fin ConsultarCapacidadTransporteRetorno()
        
        self.ConsultarCapacidadHospedaje = function ()//inicio ConsultarCapacidadHospedaje()
        {
            if (self.solicitudIntegral.FKidDestinoSolicitud() != null && self.solicitudIntegral.FKidDestinoSolicitud() != "undefined" && self.solicitudIntegral.FKidDestinoSolicitud() > 0)
            {
                var folio_especial = $("#txtFolioEspecialHospedaje").val();

                var id_instalacion_destino = self.solicitudIntegral.FKidDestinoSolicitud();

                if (folio_especial != "" && (id_instalacion_destino != "" && id_instalacion_destino != null && id_instalacion_destino != "undefined"))
                {
                    self.ConsultaCapacidadFolioEspecialHospedaje(folio_especial, id_instalacion_destino, self.solicitudIntegral.FechaSubida(), self.solicitudIntegral.FechaBajada(), "lblEspaciosDisponiblesFolioEspecialHospedaje", "image-loading-consulta-folio-especial-hospedaje");
                }


                $.ajax({
                    url: rootDir + "SolicitudIntegral/ObtenerCapacidadHospedaje/",
                    dataType: "json",
                    data:
                    {
                        id_instalacion_destino: self.solicitudIntegral.FKidDestinoSolicitud(),
                        fecha_subida: self.solicitudIntegral.FechaSubida(),
                        fecha_bajada: self.solicitudIntegral.FechaBajada(),
                        no_contrato: self.solicitudIntegral.NumeroContrato(),
                        folio_especial: folio_especial,
                        subdivision_autorizador: self.solicitudIntegral.SubdivisionAutorizador()
                    },
                    success: function (response, status, xhr)
                    {
                        var ex = HandleException(response, status, xhr);
                        if (ex == null)
                        {
                            $("#lblCapacidadHospedaje").text(response.result);
                        }
                        else
                        {
                            //Error
                            ShowException(ex);
                        }
                    },
                    beforeSend: function ()
                    {
                        $("#image-loading-capacidad-hospedaje").show();
                    },
                    complete: function ()
                    {
                        $("#image-loading-capacidad-hospedaje").hide();
                    }
                });
            }
            //else
            //{
            //    $("#lblCapacidadHospedaje").text("0");
            //}
        };//fin ConsultarCapacidadHospedaje()

        self.ConsultarInstalacionesDestinos = function ()//inicio ConsultarInstalacionesDestinos()
        {
            if ((self.solicitudIntegral.TipoSolicitudIntegral() != "SOLO HOSPEDAJE" && (self.solicitudIntegral.FkIdItinerarioTransporteIda() != null && self.solicitudIntegral.FkIdItinerarioTransporteIda() != "undefined" && self.solicitudIntegral.FkIdItinerarioTransporteIda() > 0)) || self.solicitudIntegral.TipoSolicitudIntegral() == "SOLO HOSPEDAJE")
            {
                if (self.solicitudIntegral.FichaAutorizador() != "")
                {
                    $.ajax({
                        url: rootDir + "SolicitudIntegral/ConsultarInstalacionesDestinos/",
                        dataType: "json",
                        data:
                        {
                            tipo_solicitud: self.solicitudIntegral.TipoSolicitudIntegral(),
                            tipo_personal: self.solicitudIntegral.TipoPersonal(),
                            tipo_transporte: self.solicitudIntegral.TipoTransporte(),
                            tipo_servicio: self.solicitudIntegral.TipoServicio(),
                            id_itinerario: self.solicitudIntegral.FkIdItinerarioTransporteIda(),
                            ficha_autorizador: self.solicitudIntegral.FichaAutorizador(),
                            fecha_subida: self.solicitudIntegral.FechaSubida(),
                            fecha_bajada: self.solicitudIntegral.FechaBajada(),
                            id_instalacion_origen: self.solicitudIntegral.FKidOrigenSolicitud()
                        },
                        success: function (response, status, xhr)
                        {
                            var ex = HandleException(response, status, xhr);
                            if (ex == null)
                            {
                                var instalaciones_destino = ko.utils.arrayMap(response.result.InstalacionesDestino, function (item) { return new CMInstalaciones(item) });
                                var direccionamientos_instalaciones = ko.utils.arrayMap(response.result.DireccionamientosInstalaciones, function (item) { return new VwPermisosDireccionamientosGeneral(item) });

                                self.instalacionesDestinoDisponibles(instalaciones_destino);
                                self.direccionamientosInstalacionesDisponibles(direccionamientos_instalaciones);
                            }
                            else
                            {
                                //Error
                                ShowException(ex);
                            }
                        },
                        beforeSend: function ()
                        {
                            $("#image-loading-instalacion-destino").show();
                        },
                        complete: function ()
                        {
                            $("#image-loading-instalacion-destino").hide();
                        }
                    });
                }
            }
        };//fin ConsultarInstalacionesDestinos()

        self.AgregarDetalleMultipleSolicitud = function ()
        {
            var folio_especial_transporte_ida = $("#txtFolioEspecialTransporteIda").val();
            var folio_especial_transporte_retorno = $("#txtFolioEspecialTransporteRetorno").val();
            var folio_especial_hospedaje = $("#txtFolioEspecialHospedaje").val();

            var personal_subida = $("#listAgregarPersonalSubida").tagit("tags");
            var personal_bajada = $("#listAgregarPersonalBajada").tagit("tags");

            //Validando que hayan proporcionado un numero de contrato 
            if (self.solicitudIntegral.TipoPersonal() == "COMPANIA")
            {
                if (self.solicitudIntegral.NumeroContrato() == "")
                {
                    alert("Debe especificar un numero de contrato");
                    return false;
                }

            }

            //hora del itinerario solo en caso de que la solicitud incluya transporte y sea de disposicion
            if (self.solicitudIntegral.TipoSolicitudIntegral() != "SOLO HOSPEDAJE" && self.solicitudIntegral.TipoServicio() == 'DISPOSICION')
            {
                if ($("#txtHoraSalida").val() == "")
                {
                    alert("Cuando el tipo de servicio es disposicion debe especificar una hora de salida");
                    return false;
                }

                var datevar = new Date(self.solicitudIntegral.HoraSalidaItinerarioTransporte())
                var strHoraSalidaDisp = datevar.toString("dd/MM/yyyy") + "  " + $("#txtHoraSalida").val();
                self.solicitudIntegral.HoraSalidaItinerarioTransporte(strHoraSalidaDisp);
            }


            if (personal_subida.length <= 0 && personal_bajada.length <= 0)
            {
                alert('No se ha ingresado a ningun empleado en la solicitud');
            }
            else
            {
                var listado_detalles_solicitud_personal_subida_bajada = [];

                //Agregando al personal de Subida
                $.each(personal_subida, function (key, value)
                {
                    listado_detalles_solicitud_personal_subida_bajada.push(self.ObtenerDetalleSolicitud(value.value, "SUBIDA"));
                });

                //Agregando al personal de Bajada
                $.each(personal_bajada, function (key, value)
                {
                    listado_detalles_solicitud_personal_subida_bajada.push(self.ObtenerDetalleSolicitud(value.value, "BAJADA"));
                });

                self.solicitudIntegral.FechaSubida(parseDate(self.solicitudIntegral.FechaSubida()));
                self.solicitudIntegral.FechaBajada(parseDate(self.solicitudIntegral.FechaBajada()));
                self.solicitudIntegral.FechaCreacion(parseDate(self.solicitudIntegral.FechaCreacion()));


                var datos = JSON.stringify(
                {
                    solicitud_integral: mapping.toJS(self.solicitudIntegral),
                    detalles_solicitud_integral: mapping.toJS(listado_detalles_solicitud_personal_subida_bajada),
                    folio_especial_transporte_ida: folio_especial_transporte_ida,
                    folio_especial_transporte_retorno: folio_especial_transporte_retorno,
                    folio_especial_hospedaje: folio_especial_hospedaje
                });

                $.ajax({
                    url: rootDir + "SolicitudIntegral/AgregarEmpleadosSolicitudIntegral/",
                    data: datos,
                    traditional: true,
                    type: "POST",
                    dataType: 'json',
                    contentType: "application/json",
                    success: function (response, status, xhr)
                    {
                        var ex = HandleException(response, status, xhr);
                        if (ex == null)
                        {
                            var i = 0;

                            if (response.result != null && response.result != "undefined")
                            {
                                $("#listAgregarPersonalSubida").tagit("reset");
                                $("#listAgregarPersonalBajada").tagit("reset");

                                if (self.solicitudIntegral.IdSolicitudIntegral() == 0 && self.solicitudIntegral.FolioSolicitudIntegral() == "")
                                {
                                    self.solicitudIntegral.IdSolicitudIntegral(response.result.SolicitudIntegral.IdSolicitudIntegral);
                                    self.solicitudIntegral.FolioSolicitudIntegral(response.result.SolicitudIntegral.FolioSolicitudIntegral);
                                }

                                var detalles_solicitud = ko.utils.arrayMap(response.result.DetallesSolicitudIntegral, function (item) { return mapping.fromJS(new DetalleSolicitudIntegral(item)) });
                                self.detallesSolicitudIntegral(detalles_solicitud);

                                self.ConsultarCapacidadHospedaje();
                                self.ConsultarCapacidadTransporte();
                                self.ConsultarCapacidadTransporteRetorno();
                            }
                        }
                        else
                        {
                            //Error
                            self.ConsultarCapacidadHospedaje();
                            self.ConsultarCapacidadTransporte();
                            self.ConsultarCapacidadTransporteRetorno();

                            ShowException(ex);
                        }
                    },
                    beforeSend: function ()
                    {
                        $("#image-loading-agregar-personal").show();
                    },
                    complete: function ()
                    {
                        $("#image-loading-agregar-personal").hide();
                    }
                });
            }
        }; //fin: AgregarDetalleMultipleSolicitud()

        self.EliminarDetalleSolicitudIntegral = function (detalleSolicitudIntegral, event)
        {
            $.ajax({
                url: rootDir + "SolicitudIntegral/EliminarDetalleSolicitudIntegral/",
                data: JSON.stringify({ IdDetalleSolicitudIntegral: mapping.toJS(detalleSolicitudIntegral.IdDetalleSolicitudIntegral) }),
                traditional: true,
                type: "POST",
                dataType: 'json',
                contentType: "application/json",
                success: function (response, status, xhr)
                {
                    var ex = HandleException(response, status, xhr);
                    if (ex == null)
                    {
                        switch (response.operacion)
                        {
                            case "ELIMINADO":
                                {
                                    var index_eliminar = 0;

                                    $.each(response.result, function (index, value)
                                    {
                                        var detalle_solicitud = Enumerable.From(self.detallesSolicitudIntegral()).Where(function (x) { return (x.IdDetalleSolicitudIntegral() == value.IdDetalleSolicitudIntegral) }).Select(function (x) { return x; }).SingleOrDefault();

                                        self.detallesSolicitudIntegral.remove(detalle_solicitud);
                                    });

                                    self.ConsultarCapacidadHospedaje();
                                    self.ConsultarCapacidadTransporte();
                                    self.ConsultarCapacidadTransporteRetorno();

                                    break;
                                }
                            case "CANCELADO":
                            case "SOLICITUD CANCELADA":
                                {
                                    $.each(response.result, function (index, value)
                                    {
                                        var detalle_solicitud = Enumerable.From(self.detallesSolicitudIntegral()).Where(function (x) { return (x.IdDetalleSolicitudIntegral() == value.IdDetalleSolicitudIntegral) }).Select(function (x) { return x; }).SingleOrDefault();

                                        detalle_solicitud.EstadoHuespedAH("CANCELADO");
                                        detalle_solicitud.EstadoPasajeroSTP("CANCELADO");
                                        detalle_solicitud.EstadoOcupanteSolicitudIntegral("CANCELADO");
                                    });

                                    self.ConsultarCapacidadHospedaje();
                                    self.ConsultarCapacidadTransporte();
                                    self.ConsultarCapacidadTransporteRetorno();
                                    
                                    if (response.operacion == "SOLICITUD CANCELADA")
                                    {
                                        self.solicitudIntegral.EstadoSolicitudIntegral("CANCELADO");
                                    }
                                    break;
                                }
                        }
                    }
                    else
                    {
                        ShowException(ex);
                    }
                },
                beforeSend: function ()
                {
                    $(event.srcElement).parent("td").children("span.loading-image.eliminar").show();
                },
                complete: function ()
                {
                    $(event.srcElement).parent("td").children("span.loading-image.eliminar").hide();
                }
            });
        }; //fin: EliminarDetalleSolicitudIntegral()

        self.CancelarSolicitudIntegral = function ()
        {
            $.ajax({
                url: rootDir + "SolicitudIntegral/CancelarSolicitudIntegral/",
                data: JSON.stringify({ IdSolicitudIntegral: mapping.toJS(self.solicitudIntegral.IdSolicitudIntegral) }),
                traditional: true,
                type: "POST",
                dataType: 'json',
                contentType: "application/json",
                success: function (response, status, xhr)
                {
                    var ex = HandleException(response, status, xhr);
                    if (ex == null)
                    {
                        self.solicitudIntegral.EstadoSolicitudIntegral(response.result.EstadoSolicitudIntegral);

                        $.each(self.detallesSolicitudIntegral(), function (index, value)
                        {
                            value.EstadoOcupanteSolicitudIntegral("CANCELADO");
                            value.EstadoHuespedAH("CANCELADO");
                            value.EstadoPasajeroSTP("CANCELADO");
                        });

                        //TODO:Verificar
                        //ko.applyBindings(self, $("#tabla-detalles-solicitud-integral")[0]);
                    }
                    else
                    {
                        ShowException(ex);
                    }
                },
                beforeSend: function ()
                {
                    //$(event.srcElement).parent("td").children("span.loading-image.eliminar").show();
                },
                complete: function ()
                {
                    //$(event.srcElement).parent("td").children("span.loading-image.eliminar").hide();
                }
            });
        }; //fin: CancelarSolicitudIntegral()

        self.ConfirmarSolicitudIntegral = function ()
        {
            $.ajax({
                url: rootDir + "SolicitudIntegral/ConfirmarSolicitudIntegral/",
                data: JSON.stringify({ IdSolicitudIntegral: mapping.toJS(self.solicitudIntegral.IdSolicitudIntegral) }),
                traditional: true,
                type: "POST",
                dataType: 'json',
                contentType: "application/json",
                success: function (response, status, xhr)
                {
                    var ex = HandleException(response, status, xhr);
                    if (ex == null)
                    {
                        self.solicitudIntegral.EstadoSolicitudIntegral(response.result.EstadoSolicitudIntegral);

                        $.each(self.detallesSolicitudIntegral(), function (index, value)
                        {
                            value.EstadoOcupanteSolicitudIntegral("CONFIRMADO");
                            value.EstadoHuespedAH("CONFIRMADO");
                            value.EstadoPasajeroSTP("CONFIRMADO");
                        });VerDetallesEmpleado

                        //TODO: VERIFICAR
                        //ko.applyBindings(self, $("#tabla-detalles-solicitud-integral")[0]);
                    }
                    else
                    {
                        ShowException(ex);
                    }
                },
                beforeSend: function ()
                {
                    //$(event.srcElement).parent("td").children("span.loading-image.eliminar").show();
                },
                complete: function ()
                {
                    //$(event.srcElement).parent("td").children("span.loading-image.eliminar").hide();
                }
            });
        };//fin: ConfirmarSolicitudIntegral()

        self.ItinerarioSelectionChanged = function (nuevoItinerario)
        {
            var id_itinerario_seleccionado = $('#cmbItinerarios').val();
            var itinerario_seleccionado = Enumerable.From(self.itinerariosTransporte()).Where(function (x) { return x.id_itinerario == id_itinerario_seleccionado }).Select(function (x) { return x; }).SingleOrDefault();

            self.itinerarioTransporteIdaSeleccionado(itinerario_seleccionado);

            if (itinerario_seleccionado != null && itinerario_seleccionado != "undefined")
            {
                self.solicitudIntegral.FkIdItinerarioTransporteIda(id_itinerario_seleccionado);
                self.solicitudIntegral.HoraSalidaItinerarioTransporte(itinerario_seleccionado.horario_itinerario_formateado());

                if (self.solicitudIntegral.FichaAutorizador() != "")
                {
                    self.ConsultarInstalacionesOrigen();
                    self.ConsultarInstalacionesDestinos();

                    self.ConsultarCapacidadTransporte();

                    self.BorrarDireccionamientosDisponiblesYSeleccionados();
                }
                else
                {
                    self.BorrarInstalacionesOrigenDestinosDireccionamientos();
                    $("#lblCapacidadTransporte").text("0");
                    $("#lblEspaciosDisponiblesFolioEspecialTransporteIda").text("");
                }
            }
            else
            {
                self.BorrarInstalacionesOrigenDestinosDireccionamientos();
                $("#lblEspaciosDisponiblesFolioEspecialTransporteIda").text("");
                $("#lblCapacidadTransporte").text("0");
            }

            //Si la solcitud requiere retorno se revisa si el itineraio elegido es VESPERTINO para que de la oportunidad de regresar hasta el dia siguiente.
            if (self.solicitudIntegral.RequiereRetorno() && (itinerario_seleccionado != null && itinerario_seleccionado != "undefined" && itinerario_seleccionado != ""))
            {
                var fecha_fin_retorno = Date.parseExact(self.solicitudIntegral.FechaSubida(), "dd/MM/yyyy")

                $("#txtFechaBajadaRetorno").datepicker("option", "minDate", fecha_fin_retorno.toString("dd/MM/yyyy"));

                if (itinerario_seleccionado.des_itinerario.toUpperCase().indexOf("VESPERTINO") >= 0)
                {
                    $("#txtFechaBajadaRetorno").datepicker("option", "maxDate", fecha_fin_retorno.addDays(1).toString("dd/MM/yyyy"));
                }
                else
                {
                    $("#txtFechaBajadaRetorno").datepicker("option", "maxDate", fecha_fin_retorno.toString("dd/MM/yyyy"));
                }
            }
        };//fin: ItinerarioSelectionChanged()

        self.ItinerarioRetornoSelectionChanged = function (nuevoItinerario)
        {
            var id_itinerario_seleccionado = $('#cmbItinerariosRetorno').val();
            var itinerario_seleccionado = Enumerable.From(self.itinerariosTransporte()).Where(function (x) { return x.id_itinerario == id_itinerario_seleccionado }).Select(function (x) { return x; }).SingleOrDefault();

            self.itinerarioTransporteRetornoSeleccionado(itinerario_seleccionado);

            if (itinerario_seleccionado != null && itinerario_seleccionado != "undefined")
            {
                self.solicitudIntegral.FkIdItinerarioTransporteRetorno(id_itinerario_seleccionado);

                if (self.solicitudIntegral.FichaAutorizador() != "")
                {
                    self.ConsultarCapacidadTransporteRetorno();
                }
                else
                {
                    $("#lblCapacidadTransporteRetorno").text("0");
                    $("#lblEspaciosDisponiblesFolioEspecialTransporteRetorno").text("");
                }
            }
            else
            {
                $("#lblCapacidadTransporteRetorno").text("0");
                $("#lblEspaciosDisponiblesFolioEspecialTransporteRetorno").text("");
            }
        };//fin: ItinerarioRetornoSelectionChanged()

        self.BorrarInstalacionesOrigenDestinosDireccionamientos = function ()
        {
            self.instalacionesOrigenDisponibles([]);
            self.instalacionesDestinoDisponibles([]);

            self.BorrarDireccionamientosDisponiblesYSeleccionados();
        }

        self.BorrarDireccionamientosDisponiblesYSeleccionados = function ()
        {
            self.direccionamientosInstalacionesDisponibles([]);

            self.direccionamientosInstalacionDestinoAH([]);
            self.direccionamientosInstalacionDestinoSTP([]);

            self.solicitudIntegral.FKidOrigenSolicitud(null);
            self.solicitudIntegral.FKidDestinoSolicitud(null);

            self.direccionamientoInstalacionDestinoAHSeleccionado = null;
            self.direccionamientoInstalacionDestinoSTPSeleccionado = null;
        }

        self.InstalacionOrigenSelectionChanged = function (nuevaInstalacionOrigen)
        {
            self.solicitudIntegral.FKidOrigenSolicitud($('#cmbInstalacionesOrigen').val());
        };

        self.InstalacionDestinoSelectionChanged = function (nuevaInstalacionOrigen)
        {
            self.solicitudIntegral.FKidDestinoSolicitud($('#cmbInstalacionesDestino').val());

            if (self.solicitudIntegral.FKidDestinoSolicitud() > 0)
            {
                self.ConsultarCapacidadHospedaje();
            }
            else
            {
                $("#lblEspaciosDisponiblesFolioEspecialHospedaje").text("");
                $("#lblCapacidadHospedaje").text("");
            }
        };

        self.SeleccionarDireccionamientoHospedaje = function (direccionamiento_hospedaje)
        {
            self.direccionamientoInstalacionDestinoAHSeleccionado = direccionamiento_hospedaje;

            var elementos_seleccionados = (Enumerable.From(self.direccionamientosInstalacionDestinoAH()).Where(function (x) { return x.SELECCIONADO() == true }).Select(function (x) { return x; }).ToArray());

            $.each(elementos_seleccionados, function (key, value)
            {
                value.SELECCIONADO(false);
            });

            direccionamiento_hospedaje.SELECCIONADO(true);
        };

        self.SeleccionarDireccionamientoTransporte = function (direccionamiento_transporte)
        {
            self.direccionamientoInstalacionDestinoSTPSeleccionado = direccionamiento_transporte;

            var elementos_seleccionados = (Enumerable.From(self.direccionamientosInstalacionDestinoSTP()).Where(function (x) { return x.SELECCIONADO() == true }).Select(function (x) { return x; }).ToArray());

            $.each(elementos_seleccionados, function (key, value)
            {
                value.SELECCIONADO(false);
            });

            direccionamiento_transporte.SELECCIONADO(true);
        };

        self.ValidarEmpleadoDetalleSolicitudKeyPress = function(detalleSolcitudIntegral,e,sender)
        {
            var retorno = true;
            if (e.keyCode == 13)
            {
                detalleSolcitudIntegral.FichaRFCEmpleado($(e.srcElement).val());
                self.ValidarEmpleadoDetalleSolicitud(detalleSolcitudIntegral, e);
                retorno = false;
            }
            return retorno;
        }

        self.ObtenerDetalleSolicitud = function (ficha_rfc, movimiento_personal)
        {
            var nuevo_detalle_solicitud = new DetalleSolicitudIntegral();

            var instalacion_origen_seleccionada = Enumerable.From(self.instalacionesOrigenDisponibles()).Where(function (x) { return x.ID_C_M_INSTALACION == self.solicitudIntegral.FKidOrigenSolicitud() }).Select(function (x) { return x; }).SingleOrDefault();
            var instalacion_destino_seleccionada = Enumerable.From(self.instalacionesDestinoDisponibles()).Where(function (x) { return x.ID_C_M_INSTALACION == self.solicitudIntegral.FKidDestinoSolicitud() }).Select(function (x) { return x; }).SingleOrDefault();

            nuevo_detalle_solicitud.FichaRFCEmpleado = ficha_rfc;

            nuevo_detalle_solicitud.FechaSubida = self.solicitudIntegral.FechaSubida();
            nuevo_detalle_solicitud.FechaBajada = self.solicitudIntegral.FechaBajada();

            if ((instalacion_origen_seleccionada == null || instalacion_origen_seleccionada == "undefined") && self.solicitudIntegral.TipoSolicitudIntegral() != "SOLO HOSPEDAJE")
            {
                throw "Debe seleccionar una instalaci�n origen";
            }

            if (instalacion_destino_seleccionada == null || instalacion_destino_seleccionada == "undefined")
            {
                throw "Debe seleccionar una instalaci�n destino";
            }

            if (movimiento_personal == "SUBIDA")
            {
                if (instalacion_origen_seleccionada != null && instalacion_origen_seleccionada != "undefined")
                {
                    nuevo_detalle_solicitud.IdInstalacionOrigen = instalacion_origen_seleccionada.ID_C_M_INSTALACION;
                    nuevo_detalle_solicitud.ClaveInstalacionOrigen = instalacion_origen_seleccionada.SIGLAS_INSTALACION;
                    nuevo_detalle_solicitud.NombreInstalacionOrigen = instalacion_origen_seleccionada.NOMBRE_INSTALACION;
                }
                else
                {
                    nuevo_detalle_solicitud.IdInstalacionOrigen = null;
                    nuevo_detalle_solicitud.ClaveInstalacionOrigen = null;
                    nuevo_detalle_solicitud.NombreInstalacionOrigen = null;
                }

                nuevo_detalle_solicitud.IdInstalacionDestino = instalacion_destino_seleccionada.ID_C_M_INSTALACION;
                nuevo_detalle_solicitud.ClaveInstalacionDestino = instalacion_destino_seleccionada.SIGLAS_INSTALACION;
                nuevo_detalle_solicitud.NombreInstalacionDestino = instalacion_destino_seleccionada.NOMBRE_INSTALACION;
            }
            else
            {
                nuevo_detalle_solicitud.IdInstalacionDestino = instalacion_origen_seleccionada.ID_C_M_INSTALACION;
                nuevo_detalle_solicitud.IdInstalacionOrigen = instalacion_destino_seleccionada.ID_C_M_INSTALACION;

                nuevo_detalle_solicitud.ClaveInstalacionDestino = instalacion_origen_seleccionada.SIGLAS_INSTALACION;
                nuevo_detalle_solicitud.ClaveInstalacionOrigen = instalacion_destino_seleccionada.SIGLAS_INSTALACION;

                nuevo_detalle_solicitud.NombreInstalacionDestino = instalacion_origen_seleccionada.NOMBRE_INSTALACION;
                nuevo_detalle_solicitud.NombreInstalacionOrigen = instalacion_destino_seleccionada.NOMBRE_INSTALACION;
            }

            nuevo_detalle_solicitud.fk_itinerario = self.solicitudIntegral.FkIdItinerarioTransporteIda;

            nuevo_detalle_solicitud.TipoMovimientoPersonal = movimiento_personal;

            if (self.direccionamientoInstalacionDestinoAHSeleccionado != null && self.direccionamientoInstalacionDestinoAHSeleccionado != "undefined")
            {
                nuevo_detalle_solicitud.FondoDireccionamientoAH = self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_FONDO;
                nuevo_detalle_solicitud.CentroGestorDireccionamientoAH = self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_CTO_GESTOR;
                nuevo_detalle_solicitud.ElementoPEPDireccionamiento = self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_ELEMENTO_PEP;
                nuevo_detalle_solicitud.CuentaMayorDireccionamientoAH = self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_CTA_MAYOR;
                nuevo_detalle_solicitud.ProgramaPresupuestalDireccionamientoAH = self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_PROGRAMA_PRESUPUESTAL;
                nuevo_detalle_solicitud.PosicionPresupuestalDireccionamientoAH = self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_POSICION_PRESUPUESTARIA;
            }

            if (self.direccionamientoInstalacionDestinoSTPSeleccionado != null && self.direccionamientoInstalacionDestinoSTPSeleccionado != "undefined")
            {
                nuevo_detalle_solicitud.FondoDireccionamientoSTP = self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_FONDO;
                nuevo_detalle_solicitud.CentroGestorDireccionamientoSTP = self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_CTO_GESTOR;
                nuevo_detalle_solicitud.ElementoPEPDireccionamientoSTP = self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_ELEMENTO_PEP;
                nuevo_detalle_solicitud.CuentaMayorDireccionamientoSTP = self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_CTA_MAYOR;
                nuevo_detalle_solicitud.ProgramaPresupuestalDireccionamientoSTP = self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_PROGRAMA_PRESUPUESTAL;
                nuevo_detalle_solicitud.PosicionPresupuestalDireccionamientoSTP = self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_POSICION_PRESUPUESTARIA;
            }

            nuevo_detalle_solicitud.TipoPersonal = self.solicitudIntegral.TipoPersonal();

            return nuevo_detalle_solicitud;
        }; //fin: ObtenerDetalleSolicitud()

        self.HabilitarDesabilitarControlesSegunEstadoSolicitud = function ()
        {
            //si no es una solicitud Nueva entonces hay que desabilitar los controles maestros
            if ((parseInt(mapping.toJS(self.solicitudIntegral.IdSolicitudIntegral)) <= 0))
            {
                $('#radio-tipo-solicitud').buttonset("enable");
                $('#radio-tipo-personal').buttonset("enable");
                $('#radio-tipo-transporte').buttonset("enable");
                $('#radio-tipo-servicio').buttonset("enable");

                $('#txtFechaSubida').removeAttr('disabled');
                $('#btnMostrarCalendarioFechaSubida').removeAttr('disabled');
                $('#txtFechaBajada').removeAttr('disabled');
                $('#btnMostrarCalendarioFechaBajada').removeAttr('disabled');
                $('#txtSolicitante').removeAttr('disabled');
                $('#btnValidarFichaSolitante').removeAttr('disabled');
                $('#txtAutorizador').removeAttr('disabled');
                $('#btnValidarFichaAutorizador').removeAttr('disabled');
                $('#txtContrato').removeAttr('disabled');
                $('#btnConsultarContrato').removeAttr('disabled');
                $('#cmbItinerarios').removeAttr('disabled');
                $('#btnActuaizarItinerarios').removeAttr('disabled');
                $('#cmbItinerariosRetorno').removeAttr('disabled');

                $('#cmbInstalacionesOrigen').combobox("enabled", true);
                $('#cmbInstalacionesDestino').combobox("enabled", true);
                //$('#cmbInstalacionesOrigen').removeAttr('disabled');
                //$('#cmbInstalacionesDestino').removeAttr('disabled');

                //Movimientos de Retorno
                $('#chkIncluyeRetorno').removeAttr('disabled');
                $('#txtFechaBajadaRetorno').removeAttr('disabled');
                $('#btnMostrarCalendarioFechaBajadaRetorno').removeAttr('disabled');


            }
            else
            {
                $('#radio-tipo-solicitud').buttonset("disable");
                $('#radio-tipo-personal').buttonset("disable");
                $('#radio-tipo-transporte').buttonset("disable");
                $('#radio-tipo-servicio').buttonset("disable");

                $('#txtFechaSubida').attr('disabled', true);
                $('#btnMostrarCalendarioFechaSubida').attr('disabled', true);
                $('#txtFechaBajada').attr('disabled', true);
                $('#btnMostrarCalendarioFechaBajada').attr('disabled', true);
                $('#txtSolicitante').attr('disabled', true);
                $('#btnValidarFichaSolitante').attr('disabled', true);
                $('#txtAutorizador').attr('disabled', true);
                $('#btnValidarFichaAutorizador').attr('disabled', true);
                $('#txtContrato').attr('disabled', true);
                $('#btnConsultarContrato').attr('disabled', true);
                $('#cmbItinerarios').attr('disabled', true);
                $('#btnActuaizarItinerarios').attr('disabled', true);
                $('#cmbItinerariosRetorno').attr('disabled', true);

                $('#cmbInstalacionesOrigen').combobox("enabled",false);
                //$('#cmbInstalacionesOrigen').attr('disabled', true);

                if (mapping.toJS(self.solicitudIntegral.EstadoSolicitudIntegral) != "SOLICITADO")
                {
                    //$('#cmbInstalacionesDestino').attr('disabled', true);
                    $('#cmbInstalacionesDestino').combobox("enabled",false);
                }

                //Movimientos de Retorno
                $('#chkIncluyeRetorno').attr('disabled', true);
                $('#txtFechaBajadaRetorno').attr('disabled', true);
                $('#btnMostrarCalendarioFechaBajadaRetorno').attr('disabled', true);
            }
        }; //fin: HabilitarDesabilitarControlesSegunEstadoSolicitud()

        self.ActualizarSolicitudIntegral = function (data)
        {
            self.solicitudIntegral.IdSolicitudIntegral(data.IdSolicitudIntegral);
            self.solicitudIntegral.FolioSolicitudIntegral(data.FolioSolicitudIntegral);
            self.solicitudIntegral.FkIdItinerarioTransporteIda(data.FkIdItinerarioTransporteIda);
            self.solicitudIntegral.FkIdItinerarioTransporteRetorno(data.FkIdItinerarioTransporteRetorno);
            self.solicitudIntegral.TipoSolicitudIntegral(data.TipoSolicitudIntegral);
            self.solicitudIntegral.TipoPersonal(data.TipoPersonal);
            self.solicitudIntegral.TipoTransporte(data.TipoTransporte);
            self.solicitudIntegral.TipoServicio(data.TipoServicio);
            self.solicitudIntegral.FechaCreacion(parseDate(data.FechaCreacion));
            self.solicitudIntegral.FechaSubida(parseDate(data.FechaSubida));
            self.solicitudIntegral.FechaBajada(parseDate(data.FechaBajada));
            self.solicitudIntegral.FichaSolicitante(data.FichaSolicitante);
            self.solicitudIntegral.NombreSolicitante(data.NombreSolicitante);
            self.solicitudIntegral.FichaAutorizador(data.FichaAutorizador);
            self.solicitudIntegral.NombreAutorizador(data.NombreAutorizador);
            self.solicitudIntegral.NumeroContrato(data.NumeroContrato);
            self.solicitudIntegral.NombreAcreedor(data.NombreAcreedor);
            self.solicitudIntegral.RFCAcreedor(data.RFCAcreedor);
            self.solicitudIntegral.HoraSalidaItinerarioTransporte(parseDateTime(data.HoraSalidaItinerarioTransporte));
            self.solicitudIntegral.FKidOrigenSolicitud(data.FKidOrigenSolicitud);
            self.solicitudIntegral.FKidDestinoSolicitud(data.FKidDestinoSolicitud);
            self.solicitudIntegral.SubdivisionAutorizador(data.SubdivisionAutorizador);
            self.solicitudIntegral.SubdivisionSolicitante(data.SubdivisionSolicitante);
            self.solicitudIntegral.FolioEspecialHospedaje(data.FolioEspecialHospedaje);
            self.solicitudIntegral.FolioEspecialTransporte(data.FolioEspecialTransporte);
            self.solicitudIntegral.EstadoSolicitudIntegral(data.EstadoSolicitudIntegral);
            self.solicitudIntegral.RequiereRetorno(data.RequiereRetorno);

            //
            //Actualizar self.TipoSolicitudSeleccionadaCheckBox
            //
            switch (self.solicitudIntegral.TipoSolicitudIntegral())
            {
                case "SOLO HOSPEDAJE":
                case "HOSPEDAJE Y TRANSPORTE":
                {
                    self.TipoSolicitudSeleccionadaCheckBox("HOSPEDAJE Y TRANSPORTE");
                    break;
                }
                case "SOLO TRANSPORTE":
                {
                        self.TipoSolicitudSeleccionadaCheckBox("SOLO TRANSPORTE");
                    break;
                }
            }
        }; //fin: ActualizarSolicitudIntegral()

        self.AgregarDetalleSolicitud = function()
        {
            var nuevo_detalle_solicitud = mapping.fromJS(new DetalleSolicitudIntegral());

            var instalacion_origen_seleccionada = Enumerable.From(self.instalacionesOrigenDisponibles()).Where(function (x) { return x.ID_C_M_INSTALACION == self.solicitudIntegral.FKidOrigenSolicitud() }).Select(function (x) { return x; }).SingleOrDefault();
            var instalacion_destino_seleccionada = Enumerable.From(self.instalacionesDestinoDisponibles()).Where(function (x) { return x.ID_C_M_INSTALACION == self.solicitudIntegral.FKidDestinoSolicitud() }).Select(function (x) { return x; }).SingleOrDefault();

            nuevo_detalle_solicitud.FechaSubida(self.solicitudIntegral.FechaSubida());
            nuevo_detalle_solicitud.FechaBajada(self.solicitudIntegral.FechaBajada());

            nuevo_detalle_solicitud.IdInstalacionOrigen(instalacion_origen_seleccionada.ID_C_M_INSTALACION);
            nuevo_detalle_solicitud.IdInstalacionDestino(instalacion_destino_seleccionada.ID_C_M_INSTALACION);
            
            nuevo_detalle_solicitud.ClaveInstalacionOrigen(instalacion_origen_seleccionada.SIGLAS_INSTALACION);
            nuevo_detalle_solicitud.ClaveInstalacionDestino(instalacion_destino_seleccionada.SIGLAS_INSTALACION);

            nuevo_detalle_solicitud.NombreInstalacionOrigen(instalacion_origen_seleccionada.NOMBRE_INSTALACION);
            nuevo_detalle_solicitud.NombreInstalacionDestino(instalacion_destino_seleccionada.NOMBRE_INSTALACION);

            nuevo_detalle_solicitud.fk_itinerario = self.solicitudIntegral.FkIdItinerarioTransporteIda;

            nuevo_detalle_solicitud.TipoMovimientoPersonal("SUBIDA");

            if (self.direccionamientoInstalacionDestinoAHSeleccionado != null && self.direccionamientoInstalacionDestinoAHSeleccionado != "undefined")
            {
                nuevo_detalle_solicitud.FondoDireccionamientoAH(self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_FONDO);
                nuevo_detalle_solicitud.CentroGestorDireccionamientoAH(self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_CTO_GESTOR);
                nuevo_detalle_solicitud.ElementoPEPDireccionamiento(self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_ELEMENTO_PEP);
                nuevo_detalle_solicitud.CuentaMayorDireccionamientoAH(self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_CTA_MAYOR);
                nuevo_detalle_solicitud.ProgramaPresupuestalDireccionamientoAH(self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_PROGRAMA_PRESUPUESTAL);
                nuevo_detalle_solicitud.PosicionPresupuestalDireccionamientoAH(self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_POSICION_PRESUPUESTARIA);
            }

            if (self.direccionamientoInstalacionDestinoSTPSeleccionado != null && self.direccionamientoInstalacionDestinoSTPSeleccionado != "undefined")
            {
                nuevo_detalle_solicitud.FondoDireccionamientoSTP(self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_FONDO);
                nuevo_detalle_solicitud.CentroGestorDireccionamientoSTP(self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_CTO_GESTOR);
                nuevo_detalle_solicitud.ElementoPEPDireccionamientoSTP(self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_ELEMENTO_PEP);
                nuevo_detalle_solicitud.CuentaMayorDireccionamientoSTP(self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_CTA_MAYOR);
                nuevo_detalle_solicitud.ProgramaPresupuestalDireccionamientoSTP(self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_PROGRAMA_PRESUPUESTAL);
                nuevo_detalle_solicitud.PosicionPresupuestalDireccionamientoSTP(self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_POSICION_PRESUPUESTARIA);
            }

            nuevo_detalle_solicitud.TipoPersonal(self.solicitudIntegral.TipoPersonal());

            self.detallesSolicitudIntegral.push(nuevo_detalle_solicitud);

            //Validando si la solicitud es de cambio de guardia, en ese caso se tiene que agregar el pasajero de subida y el de bajada.
            if (self.solicitudIntegral.TipoServicio() == "CAMBIO DE GUARDIA")
            {
                var detalle_solicitud_bajada = mapping.fromJS(new DetalleSolicitudIntegral());

                detalle_solicitud_bajada.FechaSubida(self.solicitudIntegral.FechaSubida());
                detalle_solicitud_bajada.FechaBajada(self.solicitudIntegral.FechaBajada());

                detalle_solicitud_bajada.IdInstalacionOrigen(instalacion_destino_seleccionada.ID_C_M_INSTALACION);
                detalle_solicitud_bajada.IdInstalacionDestino(instalacion_origen_seleccionada.ID_C_M_INSTALACION);

                detalle_solicitud_bajada.ClaveInstalacionOrigen(instalacion_destino_seleccionada.SIGLAS_INSTALACION);
                detalle_solicitud_bajada.ClaveInstalacionDestino(instalacion_origen_seleccionada.SIGLAS_INSTALACION);

                detalle_solicitud_bajada.NombreInstalacionOrigen(instalacion_destino_seleccionada.NOMBRE_INSTALACION);
                detalle_solicitud_bajada.NombreInstalacionDestino(instalacion_origen_seleccionada.NOMBRE_INSTALACION);

                nuevo_detalle_solicitud.fk_itinerario = self.solicitudIntegral.FkIdItinerarioTransporteRetorno;

                detalle_solicitud_bajada.TipoMovimientoPersonal("BAJADA");

                if (self.direccionamientoInstalacionDestinoAHSeleccionado != null && self.direccionamientoInstalacionDestinoAHSeleccionado != "undefined")
                {
                    detalle_solicitud_bajada.FondoDireccionamientoAH(self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_FONDO);
                    detalle_solicitud_bajada.CentroGestorDireccionamientoAH(self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_CTO_GESTOR);
                    detalle_solicitud_bajada.ElementoPEPDireccionamiento(self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_ELEMENTO_PEP);
                    detalle_solicitud_bajada.CuentaMayorDireccionamientoAH(self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_CTA_MAYOR);
                    detalle_solicitud_bajada.ProgramaPresupuestalDireccionamientoAH(self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_PROGRAMA_PRESUPUESTAL);
                    detalle_solicitud_bajada.PosicionPresupuestalDireccionamientoAH(self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_POSICION_PRESUPUESTARIA);
                }

                if (self.direccionamientoInstalacionDestinoSTPSeleccionado != null && self.direccionamientoInstalacionDestinoSTPSeleccionado != "undefined")
                {
                    detalle_solicitud_bajada.FondoDireccionamientoSTP(self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_FONDO);
                    detalle_solicitud_bajada.CentroGestorDireccionamientoSTP(self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_CTO_GESTOR);
                    detalle_solicitud_bajada.ElementoPEPDireccionamientoSTP(self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_ELEMENTO_PEP);
                    detalle_solicitud_bajada.CuentaMayorDireccionamientoSTP(self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_CTA_MAYOR);
                    detalle_solicitud_bajada.ProgramaPresupuestalDireccionamientoSTP(self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_PROGRAMA_PRESUPUESTAL);
                    detalle_solicitud_bajada.PosicionPresupuestalDireccionamientoSTP(self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_POSICION_PRESUPUESTARIA);
                }

                detalle_solicitud_bajada.TipoPersonal(self.solicitudIntegral.TipoPersonal());

                self.detallesSolicitudIntegral.push(detalle_solicitud_bajada);
            }
        };//fin: AgregarDetalleSolicitud()

        self.ImprimirReporteSolicitud = function ()
        {
            alert('Reporte');
        }

        //////////
        ///////CONSULTA FOLIOS ESPECIALES
        //////////

        self.ConsultarFolioEspecialTransporteIda = function ()
        {
            var folio_especial = $("#txtFolioEspecialTransporteIda").val();
            var itinerario = self.solicitudIntegral.FkIdItinerarioTransporteIda();

            if (folio_especial != "" && (itinerario != null && itinerario != "undefined"))
            {
                self.ConsultaCapacidadFolioEspecialTransporte(folio_especial, itinerario, self.solicitudIntegral.FechaSubida(), "lblEspaciosDisponiblesFolioEspecialTransporteIda", "image-loading-consulta-folio-especial-transporte-ida");
            }
            else
            {
                if (folio_especial == "")
                {
                    $("#lblEspaciosDisponiblesFolioEspecialTransporteIda").text("Debe especificar un folio especial para hacer la consulta");
                }
                else
                {
                    if (itinerario == null || itinerario == "undefined")
                    {
                        $("#lblEspaciosDisponiblesFolioEspecialTransporteIda").text("Debe seleccionar un itinerario para hacer esta consulta");
                    }
                }
            }
        }

        self.ConsultarFolioEspecialTransporteRetorno = function ()
        {
            var folio_especial = $("#txtFolioEspecialTransporteretorno").val();
            var itinerario = self.solicitudIntegral.FkIdItinerarioTransporteRetorno();


            var fecha_consulta = self.solicitudIntegral.FechaSubida();
            if (self.solicitudIntegral.RequiereRetorno())
            {
                fecha_consulta = self.solicitudIntegral.FechaBajada();
            }

            if (folio_especial != "" && (itinerario != null && itinerario != "undefined"))
            {
                self.ConsultaCapacidadFolioEspecialTransporte(folio_especial, itinerario, fecha_consulta, "lblEspaciosDisponiblesFolioEspecialTransporteRetorno", "image-loading-consulta-folio-especial-transporte-retorno");
            }
            else
            {
                if (folio_especial == "")
                {
                    $("#lblEspaciosDisponiblesFolioEspecialTransporteRetorno").text("Debe especificar un folio especial para hacer la consulta");
                }
                else
                {
                    if (itinerario == null || itinerario == "undefined")
                    {
                        $("#lblEspaciosDisponiblesFolioEspecialTransporteRetorno").text("Debe seleccionar un itinerario para hacer esta consulta");
                    }
                }
            }
        }

        self.ConsultarFolioEspecialHospedaje = function ()
        {
            var folio_especial = $("#txtFolioEspecialHospedaje").val();
            var id_instalacion_destino = $('#cmbInstalacionesDestino').val();

            if (folio_especial != "" && (id_instalacion_destino != "" && id_instalacion_destino != null && id_instalacion_destino != "undefined"))
            {
                self.ConsultaCapacidadFolioEspecialHospedaje(folio_especial, id_instalacion_destino, self.solicitudIntegral.FechaSubida(), self.solicitudIntegral.FechaBajada(), "lblEspaciosDisponiblesFolioEspecialHospedaje", "image-loading-consulta-folio-especial-hospedaje");
            }
            else
            {
                if (folio_especial == "")
                {
                    $("#lblEspaciosDisponiblesFolioEspecialHospedaje").text("Debe especificar un folio especial para hacer la consulta");
                }
                else
                {
                    if (id_instalacion_destino == "" || id_instalacion_destino == null || id_instalacion_destino == "undefined")
                    {
                        $("#lblEspaciosDisponiblesFolioEspecialHospedaje").text("Debe seleccionar una instalacion destino para hacer esta consulta");
                    }
                }
            }
        }

        self.ConsultaCapacidadFolioEspecialTransporte = function (folio_especial, itinerario, fecha_subida, nombre_label_resultado, nombre_span_image)
        {
            if (folio_especial != "" && (itinerario != null && itinerario != "undefined"))
            {
                $.ajax({
                    url: rootDir + "SolicitudIntegral/ObtenerCapacidadFolioEspecialTransporte/",
                    dataType: "json",
                    data:
                    {
                        idItinerarioTransporte: itinerario,
                        fechaServicio: fecha_subida,
                        folioEspecial: folio_especial
                    },
                    async: true,
                    success: function (response, status, xhr)
                    {
                        var ex = HandleException(response, status, xhr);

                        if (ex == null)
                        {
                            if (response.result != null || response.result != "undefined")
                            {
                                var capacidad_folio_especial = response.result;
                                $("#" + nombre_label_resultado).text("Capacidad Disponible: " + capacidad_folio_especial.toString());
                            }
                        }
                        else
                        {
                            //Error
                            ShowException(ex);
                        }
                    },
                    beforeSend: function ()
                    {
                        $("#" + nombre_span_image).show();
                    },
                    complete: function ()
                    {

                        $("#" + nombre_span_image).hide();
                    }
                });
            }
            else
            {
               $("#" + nombre_label_resultado).text("");
            }
        }

        self.ConsultaCapacidadFolioEspecialHospedaje = function (folio_especial, idInstalacionDestino, fecha_subida, fecha_bajada, nombre_label_resultado, nombre_span_image)
        {
            if (folio_especial != "" && (idInstalacionDestino != "" && idInstalacionDestino != null && idInstalacionDestino != "undefined"))
            {
                $.ajax({
                    url: rootDir + "SolicitudIntegral/ObtenerCapacidadFolioEspecialHospedaje/",
                    dataType: "json",
                    data:
                    {
                        idInstalacionDestino: idInstalacionDestino,
                        fechaSubida: fecha_subida,
                        fechaBajada: fecha_bajada,
                        folioEspecial: folio_especial,
                    },
                    async: true,
                    success: function (response, status, xhr)
                    {
                        var ex = HandleException(response, status, xhr);

                        if (ex == null)
                        {
                            if (response.result != null || response.result != "undefined")
                            {
                                var capacidad_folio_especial = response.result;
                                $("#" + nombre_label_resultado).text("Capacidad Disponible: " + capacidad_folio_especial.toString());
                            }
                        }
                        else
                        {
                            //Error
                            ShowException(ex);
                        }
                    },
                    beforeSend: function ()
                    {
                        $("#" + nombre_span_image).show();
                    },
                    complete: function ()
                    {

                        $("#" + nombre_span_image).hide();
                    }
                });
            }
            else
            {
                $("#" + nombre_label_resultado).text("");
            }
        }

        self.ConsultaInstalacionesDestinoAutorizador = function ()
        {
            var mi_dialogo = new VistaConsultaInstalacionesDestino();
            mi_dialogo.Mostrar('solicitud-integral',self.solicitudIntegral.FichaAutorizador(),self.solicitudIntegral.FechaSubida(),self.solicitudIntegral.FechaBajada());
        }

        self.CambiarInstalacionOrigenDestino = function (data)
        {

            var detalle_solictud = mapping.toJS(data);

            detalle_solictud.IdInstalacionOrigen = self.solicitudIntegral.FKidOrigenSolicitud();
            detalle_solictud.IdInstalacionDestino = self.solicitudIntegral.FKidDestinoSolicitud();

            var instalacion_destino = Enumerable.From(self.instalacionesDestinoDisponibles()).Where(function (x) { return (x.ID_C_M_INSTALACION == detalle_solictud.IdInstalacionDestino); }).Select(function (x) { return x; }).Single();
            var instalacion_origen = Enumerable.From(self.instalacionesOrigenDisponibles()).Where(function (x) { return (x.ID_C_M_INSTALACION == detalle_solictud.IdInstalacionOrigen); }).Select(function (x) { return x; }).Single();

            detalle_solictud.ClaveInstalacionOrigen = instalacion_origen.SIGLAS_INSTALACION;
            detalle_solictud.ClaveInstalacionDestino = instalacion_destino.SIGLAS_INSTALACION;
            
            detalle_solictud.NombreInstalacionOrigen = instalacion_origen.NOMBRE_INSTALACION;
            detalle_solictud.NombreInstalacionDestino = instalacion_destino.NOMBRE_INSTALACION;

            if (self.direccionamientoInstalacionDestinoAHSeleccionado != null) {
                detalle_solictud.FondoDireccionamientoAH = self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_FONDO;
                detalle_solictud.CentroGestorDireccionamientoAH = self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_CTO_GESTOR;
                detalle_solictud.ElementoPEPDireccionamiento = self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_ELEMENTO_PEP;
                detalle_solictud.CuentaMayorDireccionamientoAH = self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_CTA_MAYOR;
                detalle_solictud.ProgramaPresupuestalDireccionamientoAH = self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_PROGRAMA_PRESUPUESTAL;
                detalle_solictud.PosicionPresupuestalDireccionamientoAH = self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_POSICION_PRESUPUESTARIA;
            }
            else {
                detalle_solictud.FondoDireccionamientoAH = "";
                detalle_solictud.CentroGestorDireccionamientoAH = "";
                detalle_solictud.ElementoPEPDireccionamiento = "";
                detalle_solictud.CuentaMayorDireccionamientoAH = "";
                detalle_solictud.ProgramaPresupuestalDireccionamientoAH = "";
                detalle_solictud.PosicionPresupuestalDireccionamientoAH = "";
            }

            if (self.direccionamientoInstalacionDestinoSTPSeleccionado != null) {
                detalle_solictud.FondoDireccionamientoSTP = self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_FONDO;
                detalle_solictud.CentroGestorDireccionamientoSTP = self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_CTO_GESTOR;
                detalle_solictud.ElementoPEPDireccionamientoSTP = self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_ELEMENTO_PEP;
                detalle_solictud.CuentaMayorDireccionamientoSTP = self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_CTA_MAYOR;
                detalle_solictud.ProgramaPresupuestalDireccionamientoSTP = self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_PROGRAMA_PRESUPUESTAL;
                detalle_solictud.PosicionPresupuestalDireccionamientoSTP = self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_POSICION_PRESUPUESTARIA;
            }
            else {
                detalle_solictud.FondoDireccionamientoSTP = "";
                detalle_solictud.CentroGestorDireccionamientoSTP = "";
                detalle_solictud.ElementoPEPDireccionamientoSTP = "";
                detalle_solictud.CuentaMayorDireccionamientoSTP = "";
                detalle_solictud.ProgramaPresupuestalDireccionamientoSTP = "";
                detalle_solictud.PosicionPresupuestalDireccionamientoSTP = "";

            }

            $.ajax({
                url: rootDir + "SolicitudIntegral/CambiarOrigenDestinoPasajero/",
                dataType: "json",
                type: "POST",
                async: true,
                traditional: true,
                contentType: "application/json",
                data: JSON.stringify(
                {
                    detalleSolicitud: detalle_solictud
                }),
                success: function (response, status, xhr) {
                    var ex = HandleException(response, status, xhr);

                    if (ex == null) {

                        if (response.result != null)
                        {
                            $.each(response.result, function (index, value)
                            {
                                var detalle_solicitud = Enumerable.From(self.detallesSolicitudIntegral()).Where(function (x) { return (x.IdDetalleSolicitudIntegral() == value.IdDetalleSolicitudIntegral) }).Select(function (x) { return x; }).SingleOrDefault();

                                if (detalle_solicitud != null)
                                {
                                    detalle_solicitud.ClaveInstalacionOrigen(value.ClaveInstalacionOrigen);
                                    detalle_solicitud.ClaveInstalacionDestino(value.ClaveInstalacionDestino);

                                    detalle_solicitud.IdInstalacionOrigen(value.IdInstalacionOrigen);
                                    detalle_solicitud.IdInstalacionDestino(value.IdInstalacionDestino);
                                    
                                    detalle_solicitud.NombreInstalacionOrigen(value.NombreInstalacionOrigen);
                                    detalle_solicitud.NombreInstalacionDestino(value.NombreInstalacionDestino);
                                    
                                    detalle_solicitud.FondoDireccionamientoAH(value.FondoDireccionamientoAH);
                                    detalle_solicitud.CentroGestorDireccionamientoAH(value.CentroGestorDireccionamientoAH);
                                    detalle_solicitud.ElementoPEPDireccionamiento(value.ElementoPEPDireccionamiento);
                                    detalle_solicitud.CuentaMayorDireccionamientoAH(value.CuentaMayorDireccionamientoAH);
                                    detalle_solicitud.ProgramaPresupuestalDireccionamientoAH(value.ProgramaPresupuestalDireccionamientoAH);
                                    detalle_solicitud.PosicionPresupuestalDireccionamientoAH(value.PosicionPresupuestalDireccionamientoAH);
                                    detalle_solicitud.FondoDireccionamientoSTP(value.FondoDireccionamientoSTP);
                                    detalle_solicitud.CentroGestorDireccionamientoSTP(value.CentroGestorDireccionamientoSTP);
                                    detalle_solicitud.ElementoPEPDireccionamientoSTP(value.ElementoPEPDireccionamientoSTP);
                                    detalle_solicitud.CuentaMayorDireccionamientoSTP(value.CuentaMayorDireccionamientoSTP);
                                    detalle_solicitud.ProgramaPresupuestalDireccionamientoSTP(value.ProgramaPresupuestalDireccionamientoSTP);
                                    detalle_solicitud.PosicionPresupuestalDireccionamientoSTP(value.PosicionPresupuestalDireccionamientoSTP);
                                }

                            });
                            
                        }
                        

                        self.ConsultarCapacidadHospedaje();
                        self.ConsultarCapacidadTransporte();
                        self.ConsultarCapacidadTransporteRetorno();
                    }
                    else {
                        //Error
                        ShowException(ex);
                    }
                },
                beforeSend: function () {
                    //$("#" + nombre_span_image).show();
                },
                complete: function () {

                    //$("#" + nombre_span_image).hide();
                }
            });



            var i = 0;
           

        };

        //Ver Destinos Itinerarios
        self.VerDestinosItinerarioIda = function () {
            var destinos_itinerarios = new ListaDestinoItinerario();
            destinos_itinerarios.MostrarDestinos('solicitud-integral', self.solicitudIntegral.FkIdItinerarioTransporteIda());
        }
        //Busqueda de Itinerarios x Instalacion
        self.MostrarBusquedaxInstalacion = function () {
            var busqueda_itinerarios = new BusquedaxInstalacion();
            busqueda_itinerarios.MostrarBuscadorxInstalacion('solicitud-integral');
        }
        //Edicion de observaciones
        //self.VerEditorObservaciones = function () {
            //var editar_observaciones = new EditarObservacion();
            //editar_observaciones.MostrarEditorObservacion('solicitud-integral', self.solicitudIntegral.FkIdItinerarioTransporteIda());
        //}


    };//fin SolicitudIntegralController()
});//fin define