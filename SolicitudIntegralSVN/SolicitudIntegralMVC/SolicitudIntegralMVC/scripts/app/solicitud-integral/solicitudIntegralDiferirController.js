define(['jquery',
        'knockout',
        'knockout.mapping',
        'app/models/stp-integral/SolicitudIntegral',
        'app/models/stp-integral/detalleSolicitudIntegral',
        'app/models/stp-integral/itinerarioTransporte',
        'app/models/stp-integral/capacidadesReprogramacion',
        'app/models/gestion-gl/vwCContratos',
        'app/models/gestion-gl/cMInstalaciones',
        'app/models/gestion-gl/vwPermisosDireccionamientosGeneral',
        'app/models/utilerias/EmpleadoPemexCia',
        'jquery.ui',
        'jquery.ajax.setup',
        'exception.handling',
        'linq',
        'jquery.tagit',
        'parseDate',
        'domReady!'],
function ($, ko, mapping, SolicitudIntegral, DetalleSolicitudIntegral, ItinerarioTransporte, CapacidadesReprogramacion, VwCContratos, CMInstalaciones, VwPermisosDireccionamientosGeneral, EmpleadoPemexCia)
{
    //Indispensable para evitar que el &%$% Internet Explorer use el cache
    $.ajaxSetup({ cache: false });

    return function SolicitudIntegralController()
    {
        //variables globales a la clase
        var self = this;

        self.solicitudIntegral = mapping.fromJS(new SolicitudIntegral());
        self.detallesSolicitudIntegral = ko.observableArray([]);

        self.solicitudIntegralDiferida = mapping.fromJS(new SolicitudIntegral());
        self.detallesSolicitudIntegralDiferida = ko.observableArray([]);

        self.capacidadesDiferir = ko.observableArray([]);

        self.contratoSap = mapping.fromJS(new VwCContratos());
        self.itinerariosTransporte = ko.observableArray([]);
        self.itinerariosTransporteDiferir = ko.observableArray([]);

        self.instalacionesOrigenDisponibles = ko.observableArray([]);
        self.instalacionesDestinoDisponibles = ko.observableArray([]);

        self.direccionamientosInstalacionesDisponibles = ko.observableArray([]);

        self.direccionamientosInstalacionDestinoAH = ko.observableArray([]);
        self.direccionamientosInstalacionDestinoSTP = ko.observableArray([]);

        self.direccionamientoInstalacionDestinoAHSeleccionado = null;
        self.direccionamientoInstalacionDestinoSTPSeleccionado = null;

        self.AutorizadorDiferir = null;

        var ko_subscripcion_solicitudIntegral_IdSolicitudIntegral = null;
        var ko_subscripcion_solicitudIntegral_TipoServicio = null;
        var ko_subscripcion_solicitudIntegral_FKidDestinoSolicitud = null;

        self.EsVisibleTipoServicioCambioGuardiaDirecto = ko.computed(function ()
        {
            //return (mapping.toJS(self.TipoPersonal) == 'PEMEX' && mapping.toJS(self.TipoTransporte) == 'AEREO') || mapping.toJS(self.TipoTransporte) != 'AEREO';
            var resultado = (mapping.toJS(self.solicitudIntegral.TipoPersonal()) == 'PEMEX' && mapping.toJS(self.solicitudIntegral.TipoTransporte()) == 'AEREO') || mapping.toJS(self.solicitudIntegral.TipoTransporte()) != 'AEREO';
            return resultado;
        });

        self.EsVisibleTipoServicioCambioGuardiaDirecto.subscribe(function (value)
        {
            if (value == false && self.solicitudIntegral.TipoServicio() == "CAMBIO DE GUARDIA")
            {
                $("#chkTipoServcioRegular").click();
                self.solicitudIntegral.TipoServicio("REGULARES");
            }
        });

        self.DetallesSolicitudIntegral = ko.computed(function () 
        {
            return self.detallesSolicitudIntegral;
        });

        self.ItinerariosTransporteIda = ko.computed(function ()
        {
            var resultado = null;

            if(mapping.toJS(self.solicitudIntegral.TipoServicio()) == 'CAMBIO DE GUARDIA')
            {
                resultado = (Enumerable.From(self.itinerariosTransporte()).Where(function (x) { return (x.cve_nodo_origen != "IXT") && (x.cve_nodo_origen != x.cve_nodo_destino) }).Select(function (x) { return x; }).ToArray())
            }
            else
            {
                resultado = self.itinerariosTransporte();
            }
            
            return resultado;
        });

        self.ItinerariosTransporteRetorno = ko.computed(function ()
        {
            //return (mapping.toJS(self.TipoPersonal) == 'PEMEX' && mapping.toJS(self.TipoTransporte) == 'AEREO') || mapping.toJS(self.TipoTransporte) != 'AEREO';
            var resultado = null;

            if (mapping.toJS(self.solicitudIntegral.TipoServicio()) == 'CAMBIO DE GUARDIA')
            {
                resultado = (Enumerable.From(self.itinerariosTransporte()).Where(function (x) { return (x.cve_nodo_origen == "IXT") && (x.cve_nodo_origen != x.cve_nodo_destino) }).Select(function (x) { return x; }).ToArray())
            }

            return resultado;
        });

        self.ItinerariosTransporteIdaDiferir = ko.computed(function ()
        {
            var resultado = null;

            if (mapping.toJS(self.solicitudIntegral.TipoServicio()) == 'CAMBIO DE GUARDIA')
            {
                resultado = (Enumerable.From(self.itinerariosTransporteDiferir()).Where(function (x) { return (x.cve_nodo_origen != "IXT") && (x.cve_nodo_origen != x.cve_nodo_destino) }).Select(function (x) { return x; }).ToArray())
            }
            else
            {
                resultado = self.itinerariosTransporteDiferir();
            }

            return resultado;
        });

        self.ItinerariosTransporteRetornoDiferir = ko.computed(function ()
        {
            //return (mapping.toJS(self.TipoPersonal) == 'PEMEX' && mapping.toJS(self.TipoTransporte) == 'AEREO') || mapping.toJS(self.TipoTransporte) != 'AEREO';
            var resultado = null;

            if (mapping.toJS(self.solicitudIntegral.TipoServicio()) == 'CAMBIO DE GUARDIA')
            {
                resultado = (Enumerable.From(self.itinerariosTransporteDiferir()).Where(function (x) { return (x.cve_nodo_origen == "IXT") && (x.cve_nodo_origen != x.cve_nodo_destino) }).Select(function (x) { return x; }).ToArray())
            }

            return resultado;
        });

        //Subscripciones

        self.InicializarSubscripciones = function ()
        {
            ko_subscripcion_solicitudIntegral_IdSolicitudIntegral = self.solicitudIntegral.IdSolicitudIntegral.subscribe(function (newValue)
            {
                self.HabilitarDesabilitarControlesSegunEstadoSolicitud();
            });

            ko_subscripcion_solicitudIntegral_TipoServicio = self.solicitudIntegral.TipoServicio.subscribe(function (newvalue) { self.ConsultarItinerariosFunction(newvalue); });
            ko_subscripcion_solicitudIntegral_TipoPersonal = self.solicitudIntegral.TipoPersonal.subscribe(function (newvalue) { self.ConsultarItinerariosFunction(newvalue); });
            ko_subscripcion_solicitudIntegral_TipoTransporte = self.solicitudIntegral.TipoTransporte.subscribe(function (newvalue) { self.ConsultarItinerariosFunction(newvalue); });
            ko_subscripcion_solicitudIntegral_TipoSolicitudIntegral = self.solicitudIntegral.TipoSolicitudIntegral.subscribe(function (newvalue) { self.ConsultarItinerariosFunction(newvalue); });

            ko_subscripcion_solicitudIntegral_FKidDestinoSolicitud = self.solicitudIntegral.FKidDestinoSolicitud.subscribe(function (newValue)
            {
                self.ObtenerDireccionamientosDestinosSTP_AH(newValue);
            });
        };

        self.ConsultaItinerariosDiferir = function ()//inicio ConsultarItinerarios()
        {
            $.ajax({
                url: rootDir + "SolicitudIntegral/ConsultarItinerarios/",
                dataType: "json",
                data:
                {
                    tipo_transporte: self.solicitudIntegral.TipoTransporte() == 'AEREO' ? 'MARITIMO' : 'AEREO',
                    tipo_servicio: self.solicitudIntegral.TipoServicio,
                    fecha_servicio: self.solicitudIntegral.FechaSubida()
                },
                success: function (response, status, xhr)
                {
                    var ex = HandleException(response, status, xhr);
                    if (ex == null)
                    {
                        var mappedTasks = ko.utils.arrayMap(response.result, function (item) { return new ItinerarioTransporte(item) });
                        self.itinerariosTransporteDiferir(mappedTasks);

                        if (self.itinerariosTransporte != null && self.itinerariosTransporte != "undefined" && self.itinerariosTransporte().length <= 0)
                        {
                            //Si no hay ningun itinerario seleccionado entonces se resetean las instalaciones y los direccionamientos.
                            self.BorrarInstalacionesOrigenDestinosDireccionamientos();
                        }
                    }
                    else
                    {
                        //Error
                        ShowException(ex);
                    }
                },
                beforeSend: function ()
                {
                    $("#image-loading-itinerarios-diferir").show();
                    $("#image-loading-itinerarios-diferir-retorno").show();
                },
                complete: function ()
                {
                    $("#image-loading-itinerarios-diferir").hide();
                    $("#image-loading-itinerarios-diferir-retorno").hide();
                }
            });
        };//fin ConsultarItinerarios()

        self.ConsultarCapacidadTransporteDiferir = function ()//inicio ConsultarCapacidadTransporte()
        {
            if (self.solicitudIntegral.TipoSolicitudIntegral() != "SOLO HOSPEDAJE")
            {
                if (self.solicitudIntegral.FkIdItinerarioTransporteIdaDiferir() == null || self.solicitudIntegral.FkIdItinerarioTransporteIdaDiferir() == "undefined")
                {
                    self.solicitudIntegral.FkIdItinerarioTransporteIdaDiferir($("#cmbItinerariosDiferir").val());
                }

                if (self.solicitudIntegral.FkIdItinerarioTransporteIdaDiferir() != null && self.solicitudIntegral.FkIdItinerarioTransporteIdaDiferir() != "undefined")
                {
                    $.ajax({
                        url: rootDir + "SolicitudIntegral/ObtenerCapacidadTransportePorItem/",
                        dataType: "json",
                        data:
                        {
                            id_itinerario: self.solicitudIntegral.FkIdItinerarioTransporteIdaDiferir(),
                            fecha_subida: self.solicitudIntegral.FechaSubida(),
                            folio_especial_transporte: "",
                            no_contrato: self.solicitudIntegral.NumeroContrato(),
                        },
                        success: function (response, status, xhr)
                        {
                            var ex = HandleException(response, status, xhr);
                            if (ex == null)
                            {
                                $("#lblCapacidadTransporteDiferir").text(response.result);
                            }
                            else
                            {
                                //Error
                                ShowException(ex);
                            }
                        },
                        beforeSend: function ()
                        {
                            $("#image-loading-capacidad-transporte-diferir").show();
                        },
                        complete: function ()
                        {
                            $("#image-loading-capacidad-transporte-diferir").hide();
                        }
                    });
                }
            }
        };//fin ConsultarCapacidadTransporte()

        self.ConsultarCapacidadTransporteRetornoDiferir = function ()//inicio ConsultarCapacidadTransporteRetorno()
        {
            if (self.solicitudIntegral.TipoSolicitudIntegral() != "SOLO HOSPEDAJE")
            {
                if (self.solicitudIntegral.FkIdItinerarioTransporteRetornoDiferir() == null || self.solicitudIntegral.FkIdItinerarioTransporteRetornoDiferir() == "undefined")
                {
                    self.solicitudIntegral.FkIdItinerarioTransporteRetornoDiferir($("#cmbItinerariosRetornoDiferir").val());
                }

                if (self.solicitudIntegral.FkIdItinerarioTransporteRetornoDiferir() != null && self.solicitudIntegral.FkIdItinerarioTransporteRetornoDiferir() != "undefined")
                {
                    $.ajax({
                        url: rootDir + "SolicitudIntegral/ObtenerCapacidadTransportePorItem/",
                        dataType: "json",
                        data:
                        {
                            id_itinerario: self.solicitudIntegral.FkIdItinerarioTransporteRetornoDiferir(),
                            fecha_subida: self.solicitudIntegral.FechaSubida(),
                            folio_especial_transporte: "",
                            no_contrato: self.solicitudIntegral.NumeroContrato(),
                        },
                        success: function (response, status, xhr)
                        {
                            var ex = HandleException(response, status, xhr);
                            if (ex == null)
                            {
                                $("#lblCapacidadTransporteDiferirRetorno").text(response.result);
                            }
                            else
                            {
                                //Error
                                ShowException(ex);
                            }
                        },
                        beforeSend: function ()
                        {
                            $("#image-loading-capacidad-transporte-diferir-retorno").show();
                        },
                        complete: function ()
                        {
                            $("#image-loading-capacidad-transporte-diferir-retorno").hide();
                        }
                    });
                }
            }
        };//fin ConsultarCapacidadTransporteRetorno()

        self.DesuscribirSubscripciones = function ()
        {
            if (ko_subscripcion_solicitudIntegral_IdSolicitudIntegral != null && ko_subscripcion_solicitudIntegral_IdSolicitudIntegral != "undefined")
            {
                ko_subscripcion_solicitudIntegral_IdSolicitudIntegral.dispose();
            }
            
            if (ko_subscripcion_solicitudIntegral_TipoServicio != null && ko_subscripcion_solicitudIntegral_TipoServicio != "undefined")
            {
                ko_subscripcion_solicitudIntegral_TipoServicio.dispose();
            }
            
            if (ko_subscripcion_solicitudIntegral_FKidDestinoSolicitud != null && ko_subscripcion_solicitudIntegral_FKidDestinoSolicitud != "undefined")
            {
                ko_subscripcion_solicitudIntegral_FKidDestinoSolicitud.dispose();
            }

            if (ko_subscripcion_solicitudIntegral_TipoPersonal != null && ko_subscripcion_solicitudIntegral_TipoPersonal != "undefined")
            {
                ko_subscripcion_solicitudIntegral_TipoPersonal.dispose();
            }

            if (ko_subscripcion_solicitudIntegral_TipoTransporte != null && ko_subscripcion_solicitudIntegral_TipoTransporte != "undefined")
            {
                ko_subscripcion_solicitudIntegral_TipoTransporte.dispose();
            }

            if (ko_subscripcion_solicitudIntegral_TipoSolicitudIntegral != null && ko_subscripcion_solicitudIntegral_TipoSolicitudIntegral != "undefined")
            {
                ko_subscripcion_solicitudIntegral_TipoSolicitudIntegral.dispose();
            }
        }

        //fin: subscripciones
        self.ObtenerDireccionamientosDestinosSTP_AH = function (id_instalacion_destino)
        {
            if ((self.direccionamientosInstalacionesDisponibles != null && self.direccionamientosInstalacionesDisponibles() != "undefined") && (id_instalacion_destino != null && id_instalacion_destino != "undefined" && id_instalacion_destino > 0))
            {
                //En este caso se invierten las claves de las areas de servicio pues se esta difiriendo la solicitud
                var area_servicio_transporte = self.solicitudIntegral.TipoTransporte() == "AEREO" ? "STPVM" : "STA";

                self.direccionamientosInstalacionDestinoAH(Enumerable.From(self.direccionamientosInstalacionesDisponibles()).Where(function (x) { return x.ID_C_M_INSTALACION == id_instalacion_destino && x.CVE_AREA_SERVICIO == "AH" }).Select(function (x) { return x; }).ToArray());
                self.direccionamientosInstalacionDestinoSTP(Enumerable.From(self.direccionamientosInstalacionesDisponibles()).Where(function (x) { return x.ID_C_M_INSTALACION == id_instalacion_destino && x.CVE_AREA_SERVICIO == area_servicio_transporte }).Select(function (x) { return x; }).ToArray());

                //Aqui hace falta validar si alguno esta seleccionado.

                if (self.direccionamientosInstalacionDestinoAH().length == 1)
                {
                    self.direccionamientosInstalacionDestinoAH()[0].SELECCIONADO(true);
                    self.SeleccionarDireccionamientoHospedaje(self.direccionamientosInstalacionDestinoAH()[0]);
                }
                else
                {
                    var listado_seleccionados_ah = Enumerable.From(self.direccionamientosInstalacionDestinoAH()).Where(function (x) { return x.SELECCIONADO() == true }).Select(function (x) { return x; }).ToArray();

                    if (listado_seleccionados_ah != null && listado_seleccionados_ah != "undefined" && listado_seleccionados_ah.length > 0)
                    {
                        self.SeleccionarDireccionamientoHospedaje(listado_seleccionados_ah[0]);
                    }
                    else
                    {
                        self.direccionamientoInstalacionDestinoAHSeleccionado = null;
                    }
                }

                if (self.direccionamientosInstalacionDestinoSTP().length == 1)
                {
                    self.direccionamientosInstalacionDestinoSTP()[0].SELECCIONADO(true);
                    self.SeleccionarDireccionamientoTransporte(self.direccionamientosInstalacionDestinoSTP()[0]);
                }
                else
                {
                    var listado_seleccionados_stp = Enumerable.From(self.direccionamientosInstalacionDestinoSTP()).Where(function (x) { return x.SELECCIONADO() == true }).Select(function (x) { return x; }).ToArray();

                    if (listado_seleccionados_stp != null && listado_seleccionados_stp != "undefined" && listado_seleccionados_stp.length > 0)
                    {
                        self.SeleccionarDireccionamientoTransporte(listado_seleccionados_stp[0]);
                    }
                    else
                    {
                        self.direccionamientoInstalacionDestinoSTPSeleccionado = null;
                    }
                }
            }
            else
            {
                self.direccionamientosInstalacionDestinoAH([]);
                self.direccionamientosInstalacionDestinoSTP([]);
            }
        }//fin: ObtenerDireccionamientosDestinosSTP_AH

        //Constructor de la clase
        self.Init = function () //Inicio -- INIT()
        {
            $(document).ready(function ()
            {
                //Configurando los datetimePikers
                $('#txtNuevaFechaSubida').datepicker(
                {
                    defaultDate: "+1w", numberOfMonths: 3, stepMonths: 3, dateFormat: 'dd/mm/yy',
                    onSelect: function (selectedDate)
                    {
                        $("#txtNuevaFechaBajada").datepicker("option", "minDate", selectedDate);
                        self.ObtenerCapacidadesDiferir(self.solicitudIntegral.IdSolicitudIntegral(), self.solicitudIntegral.FkIdItinerarioTransporteIdaDiferir(), self.solicitudIntegral.FkIdItinerarioTransporteRetornoDiferir(), self.solicitudIntegral.SubdivisionAutorizadorDiferir(), $('#txtNuevaFechaSubida').datepicker('getDate'), $("#txtNuevaFechaBajada").datepicker('getDate'));
                    }
                });

                $("#txtNuevaFechaSubida").datepicker("option", "minDate", 0);

                $('#txtNuevaFechaBajada').datepicker({
                    defaultDate: "+1w", numberOfMonths: 3, stepMonths: 3, dateFormat: 'dd/mm/yy',
                    onSelect: function (selectedDate)
                    {
                        self.ObtenerCapacidadesDiferir(self.solicitudIntegral.IdSolicitudIntegral(), self.solicitudIntegral.FkIdItinerarioTransporteIdaDiferir(), self.solicitudIntegral.FkIdItinerarioTransporteRetornoDiferir(), self.solicitudIntegral.SubdivisionAutorizadorDiferir(), $('#txtNuevaFechaSubida').datepicker('getDate'), $("#txtNuevaFechaBajada").datepicker('getDate'));
                    }
                });

                $("#txtNuevaFechaBajada").datepicker("option", "minDate", 0);

                $('#txtAutorizadorDiferir').bind('keypress', function (e)
                {
                    if (e.keyCode == 13)
                    {
                        self.solicitudIntegral.FichaAutorizadorDiferir($('#txtAutorizadorDiferir').val());
                        self.ValidarAutorizadorDiferir(e);
                    }
                });

            });

            self.HabilitarDesabilitarControlesSegunEstadoSolicitud();

            //Se aplica el ligado
            ko.applyBindings(self);

            self.InicializarSubscripciones();

            //Revisando si hay alguna solicitud integral en modo Edicion.
            var datosSolicitudIntegral = $("#txtDatosSolicitudIntegral").val();

            if (datosSolicitudIntegral != null && datosSolicitudIntegral != "undefined" && datosSolicitudIntegral != "")
            {
                self.ProcesarDatosJsonSolicitudIntegral($.parseJSON(datosSolicitudIntegral).result);


                //self.itinerariosTransporteDiferir
                
            }
            $("#txtDatosSolicitudIntegral").remove();

        };//fin -- INIT()

        self.ConsultarInstalacionesDestinos = function ()//inicio ConsultarInstalacionesDestinos()
        {
            if ((self.solicitudIntegral.TipoSolicitudIntegral() != "SOLO HOSPEDAJE" && (self.solicitudIntegral.FkIdItinerarioTransporteIda() != null && self.solicitudIntegral.FkIdItinerarioTransporteIda() != "undefined" && self.solicitudIntegral.FkIdItinerarioTransporteIda() > 0)) || self.solicitudIntegral.TipoSolicitudIntegral() == "SOLO HOSPEDAJE")
            {
                if (self.solicitudIntegral.FichaAutorizadorDiferir() != "")
                {
                    $.ajax({
                        url: rootDir + "SolicitudIntegral/ConsultarInstalacionesDestinos/",
                        dataType: "json",
                        data:
                        {
                            tipo_solicitud: self.solicitudIntegral.TipoSolicitudIntegral(),
                            tipo_personal: self.solicitudIntegral.TipoPersonal(),
                            tipo_transporte: self.solicitudIntegral.TipoTransporte() == "AEREO" ? "MARITIMO" : "AEREO", //Se invierte pues se trata de diferir la solicitud
                            tipo_servicio: self.solicitudIntegral.TipoServicio(),
                            id_itinerario: self.solicitudIntegral.FkIdItinerarioTransporteIdaDiferir(),
                            ficha_autorizador: self.solicitudIntegral.FichaAutorizadorDiferir(),
                            fecha_subida: $('#txtNuevaFechaSubida').val(),
                            fecha_bajada: $("#txtNuevaFechaBajada").val(),
                            id_instalacion_origen: self.solicitudIntegral.FKidOrigenSolicitud()
                        },
                        success: function (response, status, xhr)
                        {
                            var ex = HandleException(response, status, xhr);
                            if (ex == null)
                            {
                                var instalaciones_destino = ko.utils.arrayMap(response.result.InstalacionesDestino, function (item) { return new CMInstalaciones(item) });
                                var direccionamientos_instalaciones = ko.utils.arrayMap(response.result.DireccionamientosInstalaciones, function (item) { return new VwPermisosDireccionamientosGeneral(item) });

                                self.instalacionesDestinoDisponibles(instalaciones_destino);
                                self.direccionamientosInstalacionesDisponibles(direccionamientos_instalaciones);
                            }
                            else
                            {
                                //Error
                                ShowException(ex);
                            }
                        },
                        beforeSend: function ()
                        {
                            $("#image-loading-instalacion-destino").show();
                        },
                        complete: function ()
                        {
                            $("#image-loading-instalacion-destino").hide();
                        }
                    });
                }
            }
        };//fin ConsultarInstalacionesDestinos()

        self.ObtenerDatosSolicitudIntegralEditar = function ()//inicio ValidarFichaSolicitante()
        {
            $.ajax({
                url: rootDir + "SolicitudIntegral/EditarSolicitudIntegral/",
                dataType: "json",
                data:
                {
                    folioSolicitudIntegral: $("#txtFolioSolicitudIntegral").val()
                },
                success: function (response, status, xhr)
                {
                    var ex = HandleException(response, status, xhr);
                    if (ex == null)
                    {
                        self.ProcesarDatosJsonSolicitudIntegral(response.result);
                    }
                    else
                    {
                        //Error
                        ShowException(ex);
                    }
                },
                beforeSend: function ()
                {
                    $("#image-loading-cargar-datos-solicitud").show();
                },
                complete: function ()
                {
                    $("#image-loading-cargar-datos-solicitud").hide();
                }
            });
        };//fin ValidarFichaSolicitante()

        self.ProcesarDatosJsonSolicitudIntegral = function (solicitudIntegralJson)
        {
            //var solicitud_integral = new SolicitudIntegral();
            var detalles_solicitud_integral = ko.utils.arrayMap(solicitudIntegralJson.DetallesSolicitudIntegral, function (item) { return mapping.fromJS(new DetalleSolicitudIntegral(item)) });
            var contrato_sap = new VwCContratos(solicitudIntegralJson.ContratoSap);
            var itinearios_transporte = ko.utils.arrayMap(solicitudIntegralJson.ItinerariosTransporte, function (item) { return new ItinerarioTransporte(item) });
            var instalaciones_origen = ko.utils.arrayMap(solicitudIntegralJson.InstalacionesOrigen, function (item) { return new CMInstalaciones(item) });
            var instalaciones_destino = ko.utils.arrayMap(solicitudIntegralJson.InstalacionDestino, function (item) { return new CMInstalaciones(item) });
            var direccionamientos_instalaciones_destino = ko.utils.arrayMap(solicitudIntegralJson.ListadoDireccionamientos, function (item) { return new VwPermisosDireccionamientosGeneral(item) });

            self.DesuscribirSubscripciones();

            self.ActualizarSolicitudIntegral(solicitudIntegralJson.SolicitudIntegral, self.solicitudIntegral);

            //Cargando los datos inicales de la solicitud integral diferida
            self.ActualizarSolicitudIntegral(solicitudIntegralJson.SolicitudIntegral, self.solicitudIntegralDiferida);

            self.solicitudIntegralDiferida.IdSolicitudIntegral(0);
            self.solicitudIntegralDiferida.FolioSolicitudIntegral("");

            self.solicitudIntegralDiferida.FichaAutorizador(0);
            self.solicitudIntegralDiferida.FolioSolicitudIntegral("");


            self.solicitudIntegralDiferida.TipoTransporte(self.solicitudIntegralDiferida.TipoTransporte() == "AEREO" ? "MARITIMO" : "AEREO");


            self.detallesSolicitudIntegral(detalles_solicitud_integral);

            if (solicitudIntegralJson.SolicitudIntegral.FechaSubida >= $.now())
            {
                $("#txtNuevaFechaSubida").val(parseDate(solicitudIntegralJson.SolicitudIntegral.FechaSubida));
                $("#txtNuevaFechaBajada").val(parseDate(solicitudIntegralJson.SolicitudIntegral.FechaBajada));
            }
            else
            {
                var dateNow = new Date($.now());

                $("#txtNuevaFechaSubida").val(parseDate(dateNow));
                $("#txtNuevaFechaBajada").val(parseDate(dateNow));
            }

            if (contrato_sap != null && contrato_sap != "undefined")
            {
                self.contratoSap = mapping.fromJS(contrato_sap);
            }

            if (itinearios_transporte != null && itinearios_transporte != "undefined")
            {
                self.itinerariosTransporte(itinearios_transporte);
                $("#cmbItinerarios").val(solicitudIntegralJson.SolicitudIntegral.FkIdItinerarioTransporteIda);

                if (solicitudIntegralJson.SolicitudIntegral.FkIdItinerarioTransporteRetorno != null && solicitudIntegralJson.SolicitudIntegral.FkIdItinerarioTransporteRetorno != "undefined" && solicitudIntegralJson.SolicitudIntegral.FkIdItinerarioTransporteRetorno > 0)
                {
                    $("#cmbItinerariosRetorno").val(solicitudIntegralJson.SolicitudIntegral.FkIdItinerarioTransporteRetorno);
                }

                self.solicitudIntegral.FkIdItinerarioTransporteIda(solicitudIntegralJson.SolicitudIntegral.FkIdItinerarioTransporteIda);
                self.solicitudIntegral.FkIdItinerarioTransporteRetorno(solicitudIntegralJson.SolicitudIntegral.FkIdItinerarioTransporteRetorno);
            }

            if (direccionamientos_instalaciones_destino != null && direccionamientos_instalaciones_destino != "undefined")
            {
                self.direccionamientosInstalacionesDisponibles(direccionamientos_instalaciones_destino);
            }

            if (instalaciones_origen != null && instalaciones_origen != "undefined")
            {
                self.instalacionesOrigenDisponibles(instalaciones_origen);
                $("#cmbInstalacionesOrigen").val(self.solicitudIntegral.FKidOrigenSolicitud());
            }

            if (instalaciones_destino != null && instalaciones_destino != "undefined")
            {
                self.instalacionesDestinoDisponibles(instalaciones_destino);
                $("#cmbInstalacionesDestino").val(self.solicitudIntegral.FKidDestinoSolicitud());
            }

            self.ObtenerDireccionamientosDestinosSTP_AH(0);

            self.InicializarSubscripciones();

            self.HabilitarDesabilitarControlesSegunEstadoSolicitud();

            self.ValidarAutorizadorDiferir();
            self.ConsultaItinerariosDiferir();
            self.ConsultarCapacidadTransporteDiferir();
            self.ConsultarCapacidadTransporteRetornoDiferir();

            self.ConsultarInstalacionesDestinos();

            self.ObtenerCapacidadesDiferir(self.solicitudIntegral.IdSolicitudIntegral(), self.solicitudIntegral.FkIdItinerarioTransporteIdaDiferir(), self.solicitudIntegral.FkIdItinerarioTransporteRetornoDiferir(), self.solicitudIntegral.SubdivisionAutorizadorDiferir(), $('#txtNuevaFechaSubida').datepicker('getDate'), $("#txtNuevaFechaBajada").datepicker('getDate'));
        };//fin: ProcesarDatosJsonSolicitudIntegral

        self.ObtenerCapacidadesDiferir = function (id_solicitud_integral, id_itinerario_ida_diferir, id_itinerario_retorno_diferir, subdivision_autorizador, nueva_fecha_subida, nueva_fecha_bajada)
        {
            //int id_solicitud_integral, DateTime nueva_fecha_subida, DateTime nueva_fecha_bajada
            if ((id_solicitud_integral != null && id_itinerario_ida_diferir != null && id_itinerario_retorno_diferir != null && subdivision_autorizador != null && nueva_fecha_subida != null && nueva_fecha_bajada != null) && (id_solicitud_integral != "undefined" && id_itinerario_ida_diferir != "undefined" && id_itinerario_retorno_diferir != "undefined" && subdivision_autorizador != "undefined" && nueva_fecha_subida != "undefined" && nueva_fecha_bajada != "undefined"))
            {
                self.capacidadesDiferir([]);

                $.ajax({
                    url: rootDir + "SolicitudIntegral/ObtenerCapacidadesDiferirSolicitudIntegral/",
                    data: {
                        idSolicitudIntegral: id_solicitud_integral,
                        idItinerarioIdaDiferir: id_itinerario_ida_diferir,
                        idItinerarioReotrnoDiferir: id_itinerario_retorno_diferir,
                        subdivisionAutorizador: subdivision_autorizador,
                        nuevaFechaSubida: parseDate(nueva_fecha_subida),
                        nuevaFechaBajada: parseDate(nueva_fecha_bajada)
                    },
                    traditional: true,
                    type: "GET",
                    dataType: 'json',
                    contentType: "application/json",
                    success: function (response, status, xhr)
                    {
                        var ex = HandleException(response, status, xhr);
                        if (ex == null)
                        {
                            var capacidades_diferir = ko.utils.arrayMap(response.result, function (item) { return new CapacidadesReprogramacion(item) });

                            self.capacidadesDiferir(capacidades_diferir);
                            self.ActualizarDetallesSolicitudAReprogramar();
                        }
                        else
                        {
                            ShowException(ex);
                        }
                    },
                    beforeSend: function ()
                    {
                        $("#image-loading-cargar-detalles-reprogramacion").show();
                    },
                    complete: function ()
                    {
                        $("#image-loading-cargar-detalles-reprogramacion").hide();
                    }
                });
            }
        };

        self.DiferirSolicitudIntegral = function ()
        {
            var NuevaFechaSubida = $("#txtNuevaFechaSubida").val();
            var NuevaFechaBajada = $("#txtNuevaFechaBajada").val();

            //Actualizando las fechas de subida y de bajada en la solicitud integral diferida
            self.solicitudIntegralDiferida.FechaSubida(NuevaFechaSubida);
            self.solicitudIntegralDiferida.FechaBajada(NuevaFechaBajada);

            self.solicitudIntegralDiferida.FkIdItinerarioTransporteIda($("#cmbItinerariosDiferir").val());
            self.solicitudIntegralDiferida.FkIdItinerarioTransporteRetorno($("#cmbItinerariosRetornoDiferir").val());

            self.solicitudIntegralDiferida.FichaAutorizador(self.AutorizadorDiferir.FICHA),
            self.solicitudIntegralDiferida.NombreAutorizador(self.AutorizadorDiferir.NOMBRE);
            self.solicitudIntegralDiferida.SubdivisionAutorizador(self.AutorizadorDiferir.SUBDIVISION);
            self.solicitudIntegralDiferida.DescripcionSubdivisionAutorizador(self.AutorizadorDiferir.DESCRIPCION_SUBDIVISION);

            var pasajeros_seleccionados_reprogramacion = Enumerable.From(self.detallesSolicitudIntegral()).Where(function (x) { return x.SeleccionadoReprogramacion() == true }).Select(function (x) { return x; }).ToArray();

            //Agregando los nuevos direccionamientos a los detalles de la solicitud integral:
            $.each(pasajeros_seleccionados_reprogramacion, function (key, value)
            {
                self.fk_itinerario = value.TipoMovimientoPersonal() == "SUBIDA" ? self.solicitudIntegralDiferida.FkIdItinerarioTransporteIda() : self.solicitudIntegralDiferida.FkIdItinerarioTransporteRetorno();

                if (self.direccionamientoInstalacionDestinoAHSeleccionado != null)
                {
                    value.FondoDireccionamientoAH = self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_FONDO;
                    value.CentroGestorDireccionamientoAH = self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_CTO_GESTOR;
                    value.ElementoPEPDireccionamiento = self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_ELEMENTO_PEP;
                    value.CuentaMayorDireccionamientoAH = self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_CTA_MAYOR;
                    value.ProgramaPresupuestalDireccionamientoAH = self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_PROGRAMA_PRESUPUESTAL;
                    value.PosicionPresupuestalDireccionamientoAH = self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_POSICION_PRESUPUESTARIA;
                }

                value.FondoDireccionamientoSTP = self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_FONDO;
                value.CentroGestorDireccionamientoSTP = self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_CTO_GESTOR;
                value.ElementoPEPDireccionamientoSTP = self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_ELEMENTO_PEP;
                value.CuentaMayorDireccionamientoSTP = self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_CTA_MAYOR;
                value.ProgramaPresupuestalDireccionamientoSTP = self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_PROGRAMA_PRESUPUESTAL;
                value.PosicionPresupuestalDireccionamientoSTP = self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_POSICION_PRESUPUESTARIA;
            });
            

            var datos = JSON.stringify({ solicitud_integral: mapping.toJS(self.solicitudIntegralDiferida), detalles_diferir: mapping.toJS(pasajeros_seleccionados_reprogramacion), folio_solicitud_integral_a_diferir: self.solicitudIntegral.FolioSolicitudIntegral() });

            $.ajax({
                url: rootDir + "SolicitudIntegral/Diferir/",
                data: datos,
                traditional: true,
                type: "POST",
                dataType: 'json',
                contentType: "application/json",
                success: function (response, status, xhr)
                {
                    var ex = HandleException(response, status, xhr);
                    if (ex == null)
                    {
                        var i = 0;

                        if (response.SolicitudADiferir != null && response.SolicitudADiferir != "undefined")
                        {
                            self.ActualizarSolicitudIntegral(response.SolicitudADiferir.SolicitudIntegral, self.solicitudIntegral);
                            var detalles_solicitud = ko.utils.arrayMap(response.SolicitudADiferir.DetallesSolicitudIntegral, function (item) { return mapping.fromJS(new DetalleSolicitudIntegral(item)) });
                            self.detallesSolicitudIntegral(detalles_solicitud);

                            var solicitud_integral_diferida = mapping.fromJS(new SolicitudIntegral(response.SolicitudDiferida.SolicitudIntegral));
                            var detalles_solicitud_diferida = ko.utils.arrayMap(response.SolicitudDiferida.DetallesSolicitudIntegral, function (item) { return mapping.fromJS(new DetalleSolicitudIntegral(item)) });

                            self.ActualizarSolicitudIntegral(response.SolicitudDiferida.SolicitudIntegral, self.solicitudIntegralDiferida);
                            self.detallesSolicitudIntegralDiferida(detalles_solicitud_diferida);

                            self.ObtenerCapacidadesDiferir(self.solicitudIntegral.IdSolicitudIntegral(), self.solicitudIntegral.FkIdItinerarioTransporteIdaDiferir(), self.solicitudIntegral.FkIdItinerarioTransporteRetornoDiferir(), self.solicitudIntegral.SubdivisionAutorizadorDiferir(), $('#txtNuevaFechaSubida').datepicker('getDate'), $("#txtNuevaFechaBajada").datepicker('getDate'));
                        }
                    }
                    else
                    {
                        //Error
                        ShowException(ex);
                    }
                },
                beforeSend: function ()
                {
                    $("#image-loading-reprogramar").show();
                },
                complete: function ()
                {
                    $("#image-loading-reprogramar").hide();
                }
            });
        }; //fin: ReprogramarSolicitudIntegral()

        self.ValidarAutorizadorDiferir = function ()//inicio ValidarAutorizador()
        {
            if ($.trim(self.solicitudIntegral.FichaAutorizadorDiferir()) == "")
            {
                ShowException("Debe especificar una ficha para el nuevo usuario Autorizador");
            }
            else
            {
                $.ajax({
                    url: rootDir + "SolicitudIntegral/ValidarAutorizador/",
                    dataType: "json",
                    data:
                    {
                        ficha_autorizador: self.solicitudIntegral.FichaAutorizadorDiferir(),
                        fecha_subida: self.solicitudIntegral.FechaSubida(),
                        fecha_bajada: self.solicitudIntegral.FechaBajada(),
                        tipo_solicitud: self.solicitudIntegral.TipoSolicitudIntegral(),
                        tipo_transporte: self.solicitudIntegral.TipoTransporte() == 'AEREO' ? 'MARITIMO' : 'AEREO'
                    },
                    success: function (response, status, xhr)
                    {
                        var ex = HandleException(response, status, xhr);
                        if (ex == null)
                        {
                            self.solicitudIntegral.FichaAutorizadorDiferir(response.result.FICHA),
                            self.solicitudIntegral.NombreAutorizadorDiferir(response.result.NOMBRE);
                            self.solicitudIntegral.SubdivisionAutorizadorDiferir(response.result.SUBDIVISION);
                            self.solicitudIntegral.DescripcionSubdivisionAutorizadorDiferir(response.result.DESCRIPCION_SUBDIVISION);

                            self.AutorizadorDiferir = response.result;

                            self.ConsultarInstalacionesDestinos();
                        }
                        else
                        {
                            //Error
                            ShowException(ex);
                        }
                    },
                    beforeSend: function ()
                    {
                        $("#image-loading-autorizador-diferir").show();
                    },
                    complete: function ()
                    {
                        $("#image-loading-autorizador-diferir").hide();
                    }
                });
            }
        };//fin ValidarAutorizador()

        self.ActualizarDetallesSolicitudAReprogramar = function ()
        {
            $.each(self.capacidadesDiferir(), function (key, value)
            {

                var pasajeros_mismo_origen_destino_seleccionados_programacion = Enumerable.From(self.detallesSolicitudIntegral()).Where(function (x) { return x.IdInstalacionOrigen() == value.FkIdInstalacionOrigen && x.IdInstalacionDestino() == value.FkIdInstalacionDestino && x.SeleccionadoReprogramacion() == true }).Select(function (x) { return x; }).ToArray();
                var cantidad_total_a_reprogramar = value.CantidadTotalPasajerosProgramar();

                value.CapacidadRestantePasajerosPorProgramar = cantidad_total_a_reprogramar - pasajeros_mismo_origen_destino_seleccionados_programacion.length;

                var pasajeros_mismo_origen_destino = Enumerable.From(self.detallesSolicitudIntegral()).Where(function (x) { return x.IdInstalacionOrigen() == value.FkIdInstalacionOrigen && x.IdInstalacionDestino() == value.FkIdInstalacionDestino }).Select(function (x) { return x; }).ToArray();
                var total_pasajeros_a_programar = value.CapacidadRestantePasajerosPorProgramar;

                //Habilitamos o desabilitamos los checkboxes dependiendo de si ya se alcanzo el total de pasajeros a reprogramar
                $.each(pasajeros_mismo_origen_destino, function (key, value)
                {
                    //Solo se desabilitan los que estan desmarcados
                    value.DisponibleReprogramacion(total_pasajeros_a_programar <= 0 && value.SeleccionadoReprogramacion() == false && value.EstadoOcupanteSolicitudIntegral() != "SOLICITADO" ? false : true);

                    if (value.EstadoOcupanteSolicitudIntegral() != "SOLICITADO")
                    {
                        value.SeleccionadoReprogramacion(false);
                    }
                });

                //Verificamos que no hayan pasajeros reprogramados de mas.
                if (pasajeros_mismo_origen_destino_seleccionados_programacion.length >= cantidad_total_a_reprogramar)
                {
                    var total_de_personas_desmarcar = pasajeros_mismo_origen_destino_seleccionados_programacion.length - cantidad_total_a_reprogramar;
                    //Hay que desmarcar la siguiente cantidad de pasajeros: total_de_personas_desmarcar
                    $.each(pasajeros_mismo_origen_destino_seleccionados_programacion, function (key, value)
                    {
                        if (total_de_personas_desmarcar <= 0)
                        {
                            //equivalente al break en el for de C#
                            return false;
                        }

                        value.SeleccionadoReprogramacion(false);

                        total_de_personas_desmarcar--;
                    });
                }
            });
        }

        self.ItinerarioSelectionChanged = function (nuevoItinerario)
        {
            var id_itinerario_seleccionado = $('#cmbItinerarios').val();
            var itinerario_seleccionado = Enumerable.From(self.itinerariosTransporte()).Where(function (x) { return x.id_itinerario == id_itinerario_seleccionado }).Select(function (x) { return x; }).SingleOrDefault();

            if (itinerario_seleccionado != null && itinerario_seleccionado != "undefined")
            {
                self.solicitudIntegral.FkIdItinerarioTransporteIda(id_itinerario_seleccionado);
                self.solicitudIntegral.Horario(itinerario_seleccionado.horario_itinerario_formateado());

                if (self.solicitudIntegral.FichaAutorizador() != "")
                {
                    //self.ConsultarInstalacionesOrigen();
                    self.ConsultarInstalacionesDestinos();

                    self.ConsultarCapacidadTransporte();

                    self.BorrarDireccionamientosDisponiblesYSeleccionados();
                }
                else
                {
                    self.BorrarInstalacionesOrigenDestinosDireccionamientos();
                    $("#lblCapacidadTransporte").text("0");
                }
            }
            else
            {
                self.BorrarInstalacionesOrigenDestinosDireccionamientos();
                $("#lblCapacidadTransporte").text("0");
            }
        };//fin: ItinerarioSelectionChanged()

        self.ItinerarioRetornoSelectionChanged = function (nuevoItinerario)
        {
            var id_itinerario_seleccionado = $('#cmbItinerariosRetorno').val();
            var itinerario_seleccionado = Enumerable.From(self.itinerariosTransporte()).Where(function (x) { return x.id_itinerario == id_itinerario_seleccionado }).Select(function (x) { return x; }).SingleOrDefault();

            if (itinerario_seleccionado != null && itinerario_seleccionado != "undefined")
            {
                self.solicitudIntegral.FkIdItinerarioTransporteRetorno(id_itinerario_seleccionado);

                if (self.solicitudIntegral.FichaAutorizador() != "")
                {
                    self.ConsultarCapacidadTransporteRetorno();
                }
                else
                {
                    $("#lblCapacidadTransporteRetorno").text("0");
                }
            }
            else
            {
                $("#lblCapacidadTransporteRetorno").text("0");
            }
        };//fin: ItinerarioRetornoSelectionChanged()

        self.ItinerarioDiferirSelectionChanged = function (nuevoItinerario)
        {
            var id_itinerario_diferir_seleccionado = $('#cmbItinerariosDiferir').val();
            var itinerario_seleccionado = Enumerable.From(self.itinerariosTransporteDiferir()).Where(function (x) { return x.id_itinerario == id_itinerario_diferir_seleccionado }).Select(function (x) { return x; }).SingleOrDefault();

            if (itinerario_seleccionado != null && itinerario_seleccionado != "undefined")
            {
                self.solicitudIntegral.FkIdItinerarioTransporteIdaDiferir(id_itinerario_diferir_seleccionado);
                //self.solicitudIntegral.Horario(itinerario_seleccionado.horario_itinerario_formateado());

                if (self.solicitudIntegral.FichaAutorizador() != "")
                {
                    //self.ConsultarInstalacionesOrigen();
                    self.ConsultarInstalacionesDestinos();

                    //self.ConsultarCapacidadTransporte();
                    self.ConsultarCapacidadTransporteDiferir();

                    self.BorrarDireccionamientosDisponiblesYSeleccionados();

                    self.ObtenerCapacidadesDiferir(self.solicitudIntegral.IdSolicitudIntegral(), self.solicitudIntegral.FkIdItinerarioTransporteIdaDiferir(), self.solicitudIntegral.FkIdItinerarioTransporteRetornoDiferir(), self.solicitudIntegral.SubdivisionAutorizadorDiferir(), $('#txtNuevaFechaSubida').datepicker('getDate'), $("#txtNuevaFechaBajada").datepicker('getDate'));
                }
                else
                {
                    self.BorrarInstalacionesOrigenDestinosDireccionamientos();
                    $("#lblCapacidadTransporteDiferir").text("0");

                    self.instalacionesDestinoDisponibles([]);
                    self.direccionamientosInstalacionesDisponibles([]);

                    self.capacidadesDiferir([]);
                }
            }
            else
            {
                self.BorrarInstalacionesOrigenDestinosDireccionamientos();
                $("#lblCapacidadTransporteDiferir").text("0");

                self.instalacionesDestinoDisponibles([]);
                self.direccionamientosInstalacionesDisponibles([]);

                self.capacidadesDiferir([]);
            }
        };//fin: ItinerarioSelectionChanged()

        self.ItinerarioRetornoDiferirSelectionChanged = function (nuevoItinerario)
        {
            var id_itinerario_diferir_seleccionado = $('#cmbItinerariosRetornoDiferir').val();
            var itinerario_seleccionado = Enumerable.From(self.itinerariosTransporteDiferir()).Where(function (x) { return x.id_itinerario == id_itinerario_diferir_seleccionado }).Select(function (x) { return x; }).SingleOrDefault();

            if (itinerario_seleccionado != null && itinerario_seleccionado != "undefined")
            {
                self.solicitudIntegral.FkIdItinerarioTransporteRetornoDiferir(id_itinerario_diferir_seleccionado);

                if (self.solicitudIntegral.FichaAutorizador() != "")
                {
                    //self.ConsultarCapacidadTransporteRetorno();
                    self.ConsultarCapacidadTransporteRetornoDiferir();
                    self.ObtenerCapacidadesDiferir(self.solicitudIntegral.IdSolicitudIntegral(), self.solicitudIntegral.FkIdItinerarioTransporteIdaDiferir(), self.solicitudIntegral.FkIdItinerarioTransporteRetornoDiferir(), self.solicitudIntegral.SubdivisionAutorizadorDiferir(), $('#txtNuevaFechaSubida').datepicker('getDate'), $("#txtNuevaFechaBajada").datepicker('getDate'));
                }
                else
                {
                    $("#lblCapacidadTransporteDiferirRetorno").text("0");

                    self.instalacionesDestinoDisponibles([]);
                    self.direccionamientosInstalacionesDisponibles([]);

                    self.capacidadesDiferir([]);
                }
            }
            else
            {
                $("#lblCapacidadTransporteDiferirRetorno").text("0");

                self.instalacionesDestinoDisponibles([]);
                self.direccionamientosInstalacionesDisponibles([]);

                self.capacidadesDiferir([]);
            }
        };//fin: ItinerarioRetornoSelectionChanged()

        self.MostrarCalendarioNuevaFechaSubida = function ()
        {
            $('#txtNuevaFechaSubida').datepicker("show");
        };

        self.MostrarCalendarioNuevaFechaBajada = function ()
        {
            $('#txtNuevaFechaBajada').datepicker("show");
        };

        self.BorrarInstalacionesOrigenDestinosDireccionamientos = function ()
        {
            self.instalacionesOrigenDisponibles([]);
            self.instalacionesDestinoDisponibles([]);

            self.BorrarDireccionamientosDisponiblesYSeleccionados();
        }

        self.BorrarDireccionamientosDisponiblesYSeleccionados = function ()
        {
            self.direccionamientosInstalacionesDisponibles([]);

            self.direccionamientosInstalacionDestinoAH([]);
            self.direccionamientosInstalacionDestinoSTP([]);

            self.direccionamientoInstalacionDestinoAHSeleccionado = null;
            self.direccionamientoInstalacionDestinoSTPSeleccionado = null;
        }

        self.SeleccionarDireccionamientoHospedaje = function (direccionamiento_hospedaje)
        {
            self.direccionamientoInstalacionDestinoAHSeleccionado = direccionamiento_hospedaje;

            var elementos_seleccionados = (Enumerable.From(self.direccionamientosInstalacionDestinoAH()).Where(function (x) { return x.SELECCIONADO() == true }).Select(function (x) { return x; }).ToArray());

            $.each(elementos_seleccionados, function (key, value)
            {
                value.SELECCIONADO(false);
            });

            direccionamiento_hospedaje.SELECCIONADO(true);
        };

        self.SeleccionarDireccionamientoTransporte = function (direccionamiento_transporte)
        {
            self.direccionamientoInstalacionDestinoSTPSeleccionado = direccionamiento_transporte;

            var elementos_seleccionados = (Enumerable.From(self.direccionamientosInstalacionDestinoSTP()).Where(function (x) { return x.SELECCIONADO() == true }).Select(function (x) { return x; }).ToArray());

            $.each(elementos_seleccionados, function (key, value)
            {
                value.SELECCIONADO(false);
            });

            direccionamiento_transporte.SELECCIONADO(true);
        };

        self.ObtenerDetalleSolicitud = function (ficha_rfc, movimiento_personal)
        {
            var nuevo_detalle_solicitud = new DetalleSolicitudIntegral();

            var instalacion_origen_seleccionada = Enumerable.From(self.instalacionesOrigenDisponibles()).Where(function (x) { return x.ID_C_M_INSTALACION == self.solicitudIntegral.FKidOrigenSolicitud() }).Select(function (x) { return x; }).SingleOrDefault();
            var instalacion_destino_seleccionada = Enumerable.From(self.instalacionesDestinoDisponibles()).Where(function (x) { return x.ID_C_M_INSTALACION == self.solicitudIntegral.FKidDestinoSolicitud() }).Select(function (x) { return x; }).SingleOrDefault();

            nuevo_detalle_solicitud.FichaRFCEmpleado = ficha_rfc;

            nuevo_detalle_solicitud.FechaSubida = self.solicitudIntegral.FechaSubida();
            nuevo_detalle_solicitud.FechaBajada = self.solicitudIntegral.FechaBajada();

            if (movimiento_personal == "SUBIDA")
            {
                nuevo_detalle_solicitud.IdInstalacionOrigen = instalacion_origen_seleccionada.ID_C_M_INSTALACION;
                nuevo_detalle_solicitud.IdInstalacionDestino = instalacion_destino_seleccionada.ID_C_M_INSTALACION;

                nuevo_detalle_solicitud.ClaveInstalacionOrigen = instalacion_origen_seleccionada.SIGLAS_INSTALACION;
                nuevo_detalle_solicitud.ClaveInstalacionDestino = instalacion_destino_seleccionada.SIGLAS_INSTALACION;

                nuevo_detalle_solicitud.NombreInstalacionOrigen = instalacion_origen_seleccionada.NOMBRE_INSTALACION;
                nuevo_detalle_solicitud.NombreInstalacionDestino = instalacion_destino_seleccionada.NOMBRE_INSTALACION;
            }
            else
            {
                nuevo_detalle_solicitud.IdInstalacionDestino = instalacion_origen_seleccionada.ID_C_M_INSTALACION;
                nuevo_detalle_solicitud.IdInstalacionOrigen = instalacion_destino_seleccionada.ID_C_M_INSTALACION;

                nuevo_detalle_solicitud.ClaveInstalacionDestino = instalacion_origen_seleccionada.SIGLAS_INSTALACION;
                nuevo_detalle_solicitud.ClaveInstalacionOrigen = instalacion_destino_seleccionada.SIGLAS_INSTALACION;

                nuevo_detalle_solicitud.NombreInstalacionDestino = instalacion_origen_seleccionada.NOMBRE_INSTALACION;
                nuevo_detalle_solicitud.NombreInstalacionOrigen = instalacion_destino_seleccionada.NOMBRE_INSTALACION;
            }

            nuevo_detalle_solicitud.fk_itinerario = self.solicitudIntegral.FkIdItinerarioTransporteIda;

            nuevo_detalle_solicitud.TipoMovimientoPersonal = movimiento_personal;

            if (self.direccionamientoInstalacionDestinoAHSeleccionado != null && self.direccionamientoInstalacionDestinoAHSeleccionado != "undefined")
            {
                nuevo_detalle_solicitud.FondoDireccionamientoAH = self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_FONDO;
                nuevo_detalle_solicitud.CentroGestorDireccionamientoAH = self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_CTO_GESTOR;
                nuevo_detalle_solicitud.ElementoPEPDireccionamiento = self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_ELEMENTO_PEP;
                nuevo_detalle_solicitud.CuentaMayorDireccionamientoAH = self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_CTA_MAYOR;
                nuevo_detalle_solicitud.ProgramaPresupuestalDireccionamientoAH = self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_PROGRAMA_PRESUPUESTAL;
                nuevo_detalle_solicitud.PosicionPresupuestalDireccionamientoAH = self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_POSICION_PRESUPUESTARIA;
            }

            if (self.direccionamientoInstalacionDestinoSTPSeleccionado != null && self.direccionamientoInstalacionDestinoSTPSeleccionado != "undefined")
            {
                nuevo_detalle_solicitud.FondoDireccionamientoSTP = self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_FONDO;
                nuevo_detalle_solicitud.CentroGestorDireccionamientoSTP = self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_CTO_GESTOR;
                nuevo_detalle_solicitud.ElementoPEPDireccionamientoSTP = self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_ELEMENTO_PEP;
                nuevo_detalle_solicitud.CuentaMayorDireccionamientoSTP = self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_CTA_MAYOR;
                nuevo_detalle_solicitud.ProgramaPresupuestalDireccionamientoSTP = self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_PROGRAMA_PRESUPUESTAL;
                nuevo_detalle_solicitud.PosicionPresupuestalDireccionamientoSTP = self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_POSICION_PRESUPUESTARIA;
            }

            nuevo_detalle_solicitud.TipoPersonal = self.solicitudIntegral.TipoPersonal();

            return nuevo_detalle_solicitud;
        }; //fin: ObtenerDetalleSolicitud()

        self.HabilitarDesabilitarControlesSegunEstadoSolicitud = function ()
        {
            //si no es una solicitud Nueva entonces hay que desabilitar los controles maestros
            if ((parseInt(mapping.toJS(self.solicitudIntegral.IdSolicitudIntegral)) <= 0))
            {
                $('#lblFechaSubida').removeAttr('disabled');
                $('#lblFechaBajada').removeAttr('disabled');
                $('#lblSolicitante').removeAttr('disabled');
                $('#lblAutorizador').removeAttr('disabled');
                $('#lblContrato').removeAttr('disabled');
                $('#cmbItinerarios').removeAttr('disabled');
                $('#cmbItinerariosRetorno').removeAttr('disabled');
            }
            else
            {
                $('#lblFechaSubida').attr('disabled', true);
                $('#lblFechaBajada').attr('disabled', true);
                $('#lblSolicitante').attr('disabled', true);
                $('#lblAutorizador').attr('disabled', true);
                $('#lblContrato').attr('disabled', true);
                $('#cmbItinerarios').attr('disabled', true);
                $('#cmbItinerariosRetorno').attr('disabled', true);
            }
        }; //fin: HabilitarDesabilitarControlesSegunEstadoSolicitud()

        self.ActualizarSolicitudIntegral = function (data, solicitudIntegral)
        {
            solicitudIntegral.IdSolicitudIntegral(data.IdSolicitudIntegral);
            solicitudIntegral.FolioSolicitudIntegral(data.FolioSolicitudIntegral);
            solicitudIntegral.FkIdItinerarioTransporteIda(data.FkIdItinerarioTransporteIda);
            solicitudIntegral.FkIdItinerarioTransporteRetorno(data.FkIdItinerarioTransporteRetorno);
            solicitudIntegral.TipoSolicitudIntegral(data.TipoSolicitudIntegral);
            solicitudIntegral.TipoPersonal(data.TipoPersonal);
            solicitudIntegral.TipoTransporte(data.TipoTransporte);
            solicitudIntegral.TipoServicio(data.TipoServicio);
            solicitudIntegral.FechaCreacion(parseDate(data.FechaCreacion));
            solicitudIntegral.FechaSubida(parseDate(data.FechaSubida));
            solicitudIntegral.FechaBajada(parseDate(data.FechaBajada));
            solicitudIntegral.FichaSolicitante(data.FichaSolicitante);
            solicitudIntegral.NombreSolicitante(data.NombreSolicitante);
            solicitudIntegral.FichaAutorizador(data.FichaAutorizador);
            solicitudIntegral.NombreAutorizador(data.NombreAutorizador);

            solicitudIntegral.FichaAutorizadorDiferir(data.FichaAutorizador);
            solicitudIntegral.NombreAutorizadorDiferir(data.NombreAutorizador);
            solicitudIntegral.SubdivisionAutorizadorDiferir(data.SubdivisionAutorizador);

            solicitudIntegral.NumeroContrato(data.NumeroContrato);
            solicitudIntegral.NombreAcreedor(data.NombreAcreedor);
            solicitudIntegral.RFCAcreedor(data.RFCAcreedor);
            //solicitudIntegral.Horario(data.HoraSalidaItinerarioTransporte);
            solicitudIntegral.FKidOrigenSolicitud(data.FKidOrigenSolicitud);
            solicitudIntegral.FKidDestinoSolicitud(data.FKidDestinoSolicitud);
            solicitudIntegral.SubdivisionAutorizador(data.SubdivisionAutorizador);
            solicitudIntegral.SubdivisionSolicitante(data.SubdivisionSolicitante);
            solicitudIntegral.FolioEspecialHospedaje(data.FolioEspecialHospedaje);
            solicitudIntegral.FolioEspecialTransporte(data.FolioEspecialTransporte);
            solicitudIntegral.EstadoSolicitudIntegral(data.EstadoSolicitudIntegral);
        }; //fin: ActualizarSolicitudIntegral()

        self.SeleccionarDetalleReprogramacion = function (e)
        {
            var i = 0;
            var listado_detalles_encontrados = Enumerable.From(self.detallesSolicitudIntegral()).Where(function (x) { return x.IdInstalacionOrigen() == e.FkIdInstalacionOrigen && x.IdInstalacionDestino() == e.FkIdInstalacionDestino }).Select(function (x) { return x; }).ToArray();
            var listado_otros_detalles_encontrados = Enumerable.From(self.detallesSolicitudIntegral()).Where(function (x) { return !(x.IdInstalacionOrigen() == e.FkIdInstalacionOrigen && x.IdInstalacionDestino() == e.FkIdInstalacionDestino) }).Select(function (x) { return x; }).ToArray();
            
            //Deseleccionado todos los elementos que no cumplan el criterio de busqueda (id_origen e id_destino)
            $.each(listado_otros_detalles_encontrados, function (index, value)
            {
                value.ElementoSeleccionado(false);
                value.SeleccionadoReprogramacion(false);
            });

            //Alternando la seleccion para los items que si cumplieron con el criterio de busqueda.
            $.each(listado_detalles_encontrados, function (index, value)
            {
                value.ElementoSeleccionado(!value.ElementoSeleccionado());
            });

            self.ObtenerDireccionamientosDestinosSTP_AH(e.FkIdInstalacionDestino);
        }

        self.SeleccionarDetalleSolicitudIntegral = function (e, obj)
        {
            var i = 0;

            if (obj.target.nodeName != "INPUT" && obj.target.type != "checkbox")
            {
                if (e.DisponibleReprogramacion() == true && e.ElementoSeleccionado() == true)
                {
                    if (e.EstadoOcupanteSolicitudIntegral() == "SOLICITADO")
                    {
                        e.SeleccionadoReprogramacion(!e.SeleccionadoReprogramacion());
                    }
                    else
                    {
                        e.ElementoSeleccionado(false);
                    }
                    
                }
            }

            self.ActualizarDetallesSolicitudAReprogramar();

            return true;
        }

        self.ActualizarCapacidadReprogramacionOrigenDestinoRelacionada = function (obj)
        {
            var listado_detalles_encontrados = Enumerable.From(self.detallesSolicitudIntegral()).Where(function (x) { return x.IdInstalacionOrigen() == obj.FkIdInstalacionOrigen && x.IdInstalacionDestino() == obj.FkIdInstalacionDestino }).Select(function (x) { return x; }).ToArray();

        }; //Fin self.ActualizarCapacidadReprogramacionOrigenDestinoRelacionada

    };//fin SolicitudIntegralController()

});//fin define