define(['jquery',
        'knockout',
        'knockout.mapping',
        'app/models/stp-integral/SolicitudIntegral',
        'app/models/stp-integral/detalleSolicitudIntegral',
        'app/models/stp-integral/itinerarioTransporte',
        'app/models/stp-integral/capacidadesReprogramacion',
        'app/models/gestion-gl/vwCContratos',
        'app/models/gestion-gl/cMInstalaciones',
        'app/models/gestion-gl/vwPermisosDireccionamientosGeneral',
        'app/models/utilerias/EmpleadoPemexCia',
        'jquery.ui',
        'jquery.ajax.setup',
        'exception.handling',
        'linq',
        'jquery.tagit',
        'parseDate',
        'libs/jquery.maskedinput.min',
        'domReady!'],
function ($, ko, mapping, SolicitudIntegral, DetalleSolicitudIntegral, ItinerarioTransporte, CapacidadesReprogramacion, VwCContratos, CMInstalaciones, VwPermisosDireccionamientosGeneral, EmpleadoPemexCia)
{
    //Indispensable para evitar que el &%$% Internet Explorer use el cache
    $.ajaxSetup({ cache: false });

    return function SolicitudIntegralController()
    {
        //variables globales a la clase
        var self = this;

        self.solicitudIntegral = mapping.fromJS(new SolicitudIntegral());
        self.detallesSolicitudIntegral = ko.observableArray([]);

        self.capacidadesReprogramacion = ko.observableArray([]);

        self.contratoSap = mapping.fromJS(new VwCContratos());
        self.itinerariosTransporte = ko.observableArray([]);

        self.instalacionesOrigenDisponibles = ko.observableArray([]);
        self.instalacionesDestinoDisponibles = ko.observableArray([]);

        self.direccionamientosInstalacionesDisponibles = ko.observableArray([]);

        self.direccionamientosInstalacionDestinoAH = ko.observableArray([]);
        self.direccionamientosInstalacionDestinoSTP = ko.observableArray([]);

        self.direccionamientoInstalacionDestinoAHSeleccionado = null;
        self.direccionamientoInstalacionDestinoSTPSeleccionado = null;

        var ko_subscripcion_solicitudIntegral_IdSolicitudIntegral = null;
        var ko_subscripcion_solicitudIntegral_TipoServicio = null;
        var ko_subscripcion_solicitudIntegral_FKidDestinoSolicitud = null;

        self.EsVisibleTipoServicioCambioGuardiaDirecto = ko.computed(function ()
        {
            //return (mapping.toJS(self.TipoPersonal) == 'PEMEX' && mapping.toJS(self.TipoTransporte) == 'AEREO') || mapping.toJS(self.TipoTransporte) != 'AEREO';
            var resultado = (mapping.toJS(self.solicitudIntegral.TipoPersonal()) == 'PEMEX' && mapping.toJS(self.solicitudIntegral.TipoTransporte()) == 'AEREO') || mapping.toJS(self.solicitudIntegral.TipoTransporte()) != 'AEREO';
            return resultado;
        });

        self.EsVisibleTipoServicioCambioGuardiaDirecto.subscribe(function (value)
        {
            if (value == false && self.solicitudIntegral.TipoServicio() == "CAMBIO DE GUARDIA")
            {
                $("#chkTipoServcioRegular").click();
                self.solicitudIntegral.TipoServicio("REGULARES");
            }
        });

        self.DetallesSolicitudIntegral = ko.computed(function () 
        {
            return self.detallesSolicitudIntegral;
        });

        self.ItinerariosTransporteIda = ko.computed(function ()
        {
            var resultado = null;

            if(mapping.toJS(self.solicitudIntegral.TipoServicio()) == 'CAMBIO DE GUARDIA')
            {
                resultado = (Enumerable.From(self.itinerariosTransporte()).Where(function (x) { return (x.cve_nodo_origen != "IXT") && (x.cve_nodo_origen != x.cve_nodo_destino) }).Select(function (x) { return x; }).ToArray())
            }
            else
            {
                resultado = self.itinerariosTransporte();
            }
            
            return resultado;
        });


        self.ItinerariosTransporteRetorno = ko.computed(function ()
        {
            //return (mapping.toJS(self.TipoPersonal) == 'PEMEX' && mapping.toJS(self.TipoTransporte) == 'AEREO') || mapping.toJS(self.TipoTransporte) != 'AEREO';
            var resultado = null;

            if (mapping.toJS(self.solicitudIntegral.TipoServicio()) == 'CAMBIO DE GUARDIA')
            {
                resultado = (Enumerable.From(self.itinerariosTransporte()).Where(function (x) { return (x.cve_nodo_origen == "IXT") && (x.cve_nodo_origen != x.cve_nodo_destino) }).Select(function (x) { return x; }).ToArray())
            }

            return resultado;
        });

        //Subscripciones

        self.InicializarSubscripciones = function ()
        {
            ko_subscripcion_solicitudIntegral_IdSolicitudIntegral = self.solicitudIntegral.IdSolicitudIntegral.subscribe(function (newValue)
            {
                self.HabilitarDesabilitarControlesSegunEstadoSolicitud();
            });

            ko_subscripcion_solicitudIntegral_TipoServicio = self.solicitudIntegral.TipoServicio.subscribe(function (newvalue) { self.ConsultarItinerariosFunction(newvalue); });
            ko_subscripcion_solicitudIntegral_TipoPersonal = self.solicitudIntegral.TipoPersonal.subscribe(function (newvalue) { self.ConsultarItinerariosFunction(newvalue); });
            ko_subscripcion_solicitudIntegral_TipoTransporte = self.solicitudIntegral.TipoTransporte.subscribe(function (newvalue) { self.ConsultarItinerariosFunction(newvalue); });
            ko_subscripcion_solicitudIntegral_TipoSolicitudIntegral = self.solicitudIntegral.TipoSolicitudIntegral.subscribe(function (newvalue) { self.ConsultarItinerariosFunction(newvalue); });

            ko_subscripcion_solicitudIntegral_FKidDestinoSolicitud = self.solicitudIntegral.FKidDestinoSolicitud.subscribe(function (newValue)
            {
                self.ObtenerDireccionamientosDestinosSTP_AH(newValue);
            });
        };

        self.ConsultarItinerariosFunction = function (newValue)
        {
            if (self.solicitudIntegral.TipoSolicitudIntegral() != "" && self.solicitudIntegral.TipoPersonal() != "" && self.solicitudIntegral.TipoTransporte() != "" && self.solicitudIntegral.TipoServicio() != "" /*&& self.solicitudIntegral.FechaSubida() != "10/26/2012" && self.solicitudIntegral.FechaBajada() != "10/27/2012"*/)
            {
                self.ConsultaItinerarios();
            }
        };

        self.DesuscribirSubscripciones = function ()
        {
            if (ko_subscripcion_solicitudIntegral_IdSolicitudIntegral != null && ko_subscripcion_solicitudIntegral_IdSolicitudIntegral != "undefined")
            {
                ko_subscripcion_solicitudIntegral_IdSolicitudIntegral.dispose();
            }
            
            if (ko_subscripcion_solicitudIntegral_TipoServicio != null && ko_subscripcion_solicitudIntegral_TipoServicio != "undefined")
            {
                ko_subscripcion_solicitudIntegral_TipoServicio.dispose();
            }
            
            if (ko_subscripcion_solicitudIntegral_FKidDestinoSolicitud != null && ko_subscripcion_solicitudIntegral_FKidDestinoSolicitud != "undefined")
            {
                ko_subscripcion_solicitudIntegral_FKidDestinoSolicitud.dispose();
            }

            if (ko_subscripcion_solicitudIntegral_TipoPersonal != null && ko_subscripcion_solicitudIntegral_TipoPersonal != "undefined")
            {
                ko_subscripcion_solicitudIntegral_TipoPersonal.dispose();
            }

            if (ko_subscripcion_solicitudIntegral_TipoTransporte != null && ko_subscripcion_solicitudIntegral_TipoTransporte != "undefined")
            {
                ko_subscripcion_solicitudIntegral_TipoTransporte.dispose();
            }

            if (ko_subscripcion_solicitudIntegral_TipoSolicitudIntegral != null && ko_subscripcion_solicitudIntegral_TipoSolicitudIntegral != "undefined")
            {
                ko_subscripcion_solicitudIntegral_TipoSolicitudIntegral.dispose();
            }
        }

        //fin: subscripciones

        self.ObtenerDireccionamientosDestinosSTP_AH = function (id_instalacion_destino)
        {
            if ((self.direccionamientosInstalacionesDisponibles != null && self.direccionamientosInstalacionesDisponibles() != "undefined") && (id_instalacion_destino != null && id_instalacion_destino != "undefined" && id_instalacion_destino > 0))
            {
                var area_servicio_transporte = self.solicitudIntegral.TipoTransporte() == "AEREO" ? "STA" : "STPVM";

                self.direccionamientosInstalacionDestinoAH(Enumerable.From(self.direccionamientosInstalacionesDisponibles()).Where(function (x) { return x.ID_C_M_INSTALACION == id_instalacion_destino && x.CVE_AREA_SERVICIO == "AH" }).Select(function (x) { return x; }).ToArray());
                self.direccionamientosInstalacionDestinoSTP(Enumerable.From(self.direccionamientosInstalacionesDisponibles()).Where(function (x) { return x.ID_C_M_INSTALACION == id_instalacion_destino && x.CVE_AREA_SERVICIO == area_servicio_transporte }).Select(function (x) { return x; }).ToArray());

                //Aqui hace falta validar si alguno esta seleccionado.

                if (self.direccionamientosInstalacionDestinoAH().length == 1)
                {
                    self.direccionamientosInstalacionDestinoAH()[0].SELECCIONADO(true);
                    self.SeleccionarDireccionamientoHospedaje(self.direccionamientosInstalacionDestinoAH()[0]);
                }
                else
                {
                    var listado_seleccionados_ah = Enumerable.From(self.direccionamientosInstalacionDestinoAH()).Where(function (x) { return x.SELECCIONADO() == true }).Select(function (x) { return x; }).ToArray();

                    if (listado_seleccionados_ah != null && listado_seleccionados_ah != "undefined" && listado_seleccionados_ah.length > 0)
                    {
                        self.SeleccionarDireccionamientoHospedaje(listado_seleccionados_ah[0]);
                    }
                    else
                    {
                        self.direccionamientoInstalacionDestinoAHSeleccionado = null;
                    }
                }

                if (self.direccionamientosInstalacionDestinoSTP().length == 1)
                {
                    self.direccionamientosInstalacionDestinoSTP()[0].SELECCIONADO(true);
                    self.SeleccionarDireccionamientoTransporte(self.direccionamientosInstalacionDestinoSTP()[0]);
                }
                else
                {
                    var listado_seleccionados_stp = Enumerable.From(self.direccionamientosInstalacionDestinoSTP()).Where(function (x) { return x.SELECCIONADO() == true }).Select(function (x) { return x; }).ToArray();

                    if (listado_seleccionados_stp != null && listado_seleccionados_stp != "undefined" && listado_seleccionados_stp.length > 0)
                    {
                        self.SeleccionarDireccionamientoTransporte(listado_seleccionados_stp[0]);
                    }
                    else
                    {
                        self.direccionamientoInstalacionDestinoSTPSeleccionado = null;
                    }
                }
            }
            else
            {
                self.direccionamientosInstalacionDestinoAH([]);
                self.direccionamientosInstalacionDestinoSTP([]);
            }
        }//fin: ObtenerDireccionamientosDestinosSTP_AH

        //Constructor de la clase
        self.Init = function () //Inicio -- INIT()
        {
            $(document).ready(function ()
            {
                //Configurando los datetimePikers
                $('#txtNuevaFechaSubida').datepicker(
                {
                    defaultDate: "+1w", numberOfMonths: 3, stepMonths: 3, dateFormat: 'dd/mm/yy',
                    onSelect: function (selectedDate)
                    {
                        $("#txtNuevaFechaBajada").datepicker("option", "minDate", selectedDate);
                        self.ObtenerCapacidadesReprogramacion(self.solicitudIntegral.IdSolicitudIntegral(), $('#txtNuevaFechaSubida').datepicker('getDate'), $("#txtNuevaFechaBajada").datepicker('getDate'));
                    }
                });

                $("#txtNuevaFechaSubida").datepicker("option", "minDate", 0);

                $('#txtNuevaFechaBajada').datepicker({
                    defaultDate: "+1w", numberOfMonths: 3, stepMonths: 3, dateFormat: 'dd/mm/yy',
                    onSelect: function (selectedDate)
                    {
                        self.ObtenerCapacidadesReprogramacion(self.solicitudIntegral.IdSolicitudIntegral(), $('#txtNuevaFechaSubida').datepicker('getDate'), $("#txtNuevaFechaBajada").datepicker('getDate'));
                    }
                });

                $("#txtNuevaFechaBajada").datepicker("option", "minDate", 0);

            });

            self.HabilitarDesabilitarControlesSegunEstadoSolicitud();

            $("#txtNuevaFechaSubida").mask("99/99/9999");
            $("#txtNuevaFechaBajada").mask("99/99/9999");

            //Se aplica el ligado
            ko.applyBindings(self);

            self.InicializarSubscripciones();

            //Revisando si hay alguna solicitud integral en modo Edicion.
            var datosSolicitudIntegral = $("#txtDatosSolicitudIntegral").val();

            if (datosSolicitudIntegral != null && datosSolicitudIntegral != "undefined" && datosSolicitudIntegral != "")
            {
                self.ProcesarDatosJsonSolicitudIntegral($.parseJSON(datosSolicitudIntegral).result);
            }
            $("#txtDatosSolicitudIntegral").remove();

        };//fin -- INIT()

        self.ObtenerDatosSolicitudIntegralEditar = function ()//inicio ValidarFichaSolicitante()
        {
            $.ajax({
                url: rootDir + "SolicitudIntegral/EditarSolicitudIntegral/",
                dataType: "json",
                data:
                {
                    folioSolicitudIntegral: $("#txtFolioSolicitudIntegral").val()
                },
                success: function (response, status, xhr)
                {
                    var ex = HandleException(response, status, xhr);
                    if (ex == null)
                    {
                        self.ProcesarDatosJsonSolicitudIntegral(response.result);
                    }
                    else
                    {
                        //Error
                        ShowException(ex);
                    }
                },
                beforeSend: function ()
                {
                    $("#image-loading-cargar-datos-solicitud").show();
                },
                complete: function ()
                {
                    $("#image-loading-cargar-datos-solicitud").hide();
                }
            });
        };//fin ValidarFichaSolicitante()

        self.ProcesarDatosJsonSolicitudIntegral = function (solicitudIntegralJson)
        {
            //var solicitud_integral = new SolicitudIntegral();
            var detalles_solicitud_integral = ko.utils.arrayMap(solicitudIntegralJson.DetallesSolicitudIntegral, function (item) { return mapping.fromJS(new DetalleSolicitudIntegral(item)) });
            var contrato_sap = new VwCContratos(solicitudIntegralJson.ContratoSap);
            var itinearios_transporte = ko.utils.arrayMap(solicitudIntegralJson.ItinerariosTransporte, function (item) { return new ItinerarioTransporte(item) });
            var instalaciones_origen = ko.utils.arrayMap(solicitudIntegralJson.InstalacionesOrigen, function (item) { return new CMInstalaciones(item) });
            var instalaciones_destino = ko.utils.arrayMap(solicitudIntegralJson.InstalacionDestino, function (item) { return new CMInstalaciones(item) });
            var direccionamientos_instalaciones_destino = ko.utils.arrayMap(solicitudIntegralJson.ListadoDireccionamientos, function (item) { return new VwPermisosDireccionamientosGeneral(item) });

            self.DesuscribirSubscripciones();

            self.ActualizarSolicitudIntegral(solicitudIntegralJson.SolicitudIntegral);
            self.detallesSolicitudIntegral(detalles_solicitud_integral);

            if (solicitudIntegralJson.SolicitudIntegral.FechaSubida >= $.now())
            {
                $("#txtNuevaFechaSubida").val(parseDate(solicitudIntegralJson.SolicitudIntegral.FechaSubida));
                $("#txtNuevaFechaBajada").val(parseDate(solicitudIntegralJson.SolicitudIntegral.FechaBajada));
            }
            else
            {
                var dateNow = new Date($.now());

                $("#txtNuevaFechaSubida").val(parseDate(dateNow));
                $("#txtNuevaFechaBajada").val(parseDate(dateNow));
            }
            

            //self.solicitudIntegral.FechaSubida(self.solicitudIntegral.FechaSubidaFormateada());
            //self.solicitudIntegral.FechaBajada(self.solicitudIntegral.FechaBajadaFormateada());

            if (contrato_sap != null && contrato_sap != "undefined")
            {
                self.contratoSap = mapping.fromJS(contrato_sap);
            }

            if (itinearios_transporte != null && itinearios_transporte != "undefined")
            {
                self.itinerariosTransporte(itinearios_transporte);
                $("#cmbItinerarios").val(solicitudIntegralJson.SolicitudIntegral.FkIdItinerarioTransporteIda);

                if (solicitudIntegralJson.SolicitudIntegral.FkIdItinerarioTransporteRetorno != null && solicitudIntegralJson.SolicitudIntegral.FkIdItinerarioTransporteRetorno != "undefined" && solicitudIntegralJson.SolicitudIntegral.FkIdItinerarioTransporteRetorno > 0)
                {
                    $("#cmbItinerariosRetorno").val(solicitudIntegralJson.SolicitudIntegral.FkIdItinerarioTransporteRetorno);
                }

                self.solicitudIntegral.FkIdItinerarioTransporteIda(solicitudIntegralJson.SolicitudIntegral.FkIdItinerarioTransporteIda);
                self.solicitudIntegral.FkIdItinerarioTransporteRetorno(solicitudIntegralJson.SolicitudIntegral.FkIdItinerarioTransporteRetorno);
            }

            if (direccionamientos_instalaciones_destino != null && direccionamientos_instalaciones_destino != "undefined")
            {
                self.direccionamientosInstalacionesDisponibles(direccionamientos_instalaciones_destino);
            }

            if (instalaciones_origen != null && instalaciones_origen != "undefined")
            {
                self.instalacionesOrigenDisponibles(instalaciones_origen);
                $("#cmbInstalacionesOrigen").val(self.solicitudIntegral.FKidOrigenSolicitud());
            }


            if (instalaciones_destino != null && instalaciones_destino != "undefined")
            {
                self.instalacionesDestinoDisponibles(instalaciones_destino);
                $("#cmbInstalacionesDestino").val(self.solicitudIntegral.FKidDestinoSolicitud());

                if (self.solicitudIntegral.FKidDestinoSolicitud() != null && self.solicitudIntegral.FKidDestinoSolicitud() != "undefined" && self.solicitudIntegral.FKidDestinoSolicitud() > 0)
                {
                    self.ObtenerDireccionamientosDestinosSTP_AH(self.solicitudIntegral.FKidDestinoSolicitud());
                }

            }

            self.InicializarSubscripciones();

            self.HabilitarDesabilitarControlesSegunEstadoSolicitud();

            //self.ConsultarCapacidadTransporte();
            //self.ConsultarCapacidadTransporteRetorno();

            //self.ConsultarCapacidadHospedaje();

            self.ObtenerCapacidadesReprogramacion(self.solicitudIntegral.IdSolicitudIntegral(), $('#txtNuevaFechaSubida').datepicker('getDate'), $("#txtNuevaFechaBajada").datepicker('getDate'));

        };//fin: ProcesarDatosJsonSolicitudIntegral

        //
        self.ObtenerCapacidadesReprogramacion = function (id_solicitud_integral, nueva_fecha_subida, nueva_fecha_bajada)
        {
            //int id_solicitud_integral, DateTime nueva_fecha_subida, DateTime nueva_fecha_bajada
            self.capacidadesReprogramacion([]);

            $.ajax({
                url: rootDir + "SolicitudIntegral/ObtenerCapacidadesReprogramacionSolicitudIntegral/",
                data: { idSolicitudIntegral: id_solicitud_integral, nuevaFechaSubida: parseDate(nueva_fecha_subida), nuevaFechaBajada: parseDate(nueva_fecha_bajada) },
                traditional: true,
                type: "GET",
                dataType: 'json',
                contentType: "application/json",
                success: function (response, status, xhr)
                {
                    var ex = HandleException(response, status, xhr);
                    if (ex == null)
                    {
                        var capacidades_reprogramacion = ko.utils.arrayMap(response.result, function (item) { return new CapacidadesReprogramacion(item) });

                        self.capacidadesReprogramacion(capacidades_reprogramacion);
                        self.ActualizarDetallesSolicitudAReprogramar();
                    }
                    else
                    {
                        ShowException(ex);
                    }
                },
                beforeSend: function ()
                {
                    $("#image-loading-cargar-detalles-reprogramacion").show();
                },
                complete: function ()
                {
                    $("#image-loading-cargar-detalles-reprogramacion").hide();
                }
            });
        };

        self.ReprogramarSolicitudIntegral = function ()
        {
            var NuevaFechaSubida = $("#txtNuevaFechaSubida").val();
            var NuevaFechaBajada = $("#txtNuevaFechaBajada").val();

            var pasajeros_seleccionados_reprogramacion = Enumerable.From(self.detallesSolicitudIntegral()).Where(function (x) { return x.SeleccionadoReprogramacion() == true }).Select(function (x) { return x; }).ToArray();

            var datos = JSON.stringify({ solicitud_integral: mapping.toJS(self.solicitudIntegral), detalles_solicitud_integral: mapping.toJS(pasajeros_seleccionados_reprogramacion), nueva_fecha_subida: NuevaFechaSubida, nueva_fecha_bajada: NuevaFechaBajada });

            $.ajax({
                url: rootDir + "SolicitudIntegral/ReprogramarSolicitudIntegral/",
                data: datos,
                traditional: true,
                type: "POST",
                dataType: 'json',
                contentType: "application/json",
                success: function (response, status, xhr)
                {
                    var ex = HandleException(response, status, xhr);
                    if (ex == null)
                    {
                        var i = 0;

                        if (response.result != null && response.result != "undefined")
                        {
                            self.ActualizarSolicitudIntegral(response.result.SolicitudIntegral);

                            var detalles_solicitud = ko.utils.arrayMap(response.result.DetallesSolicitudIntegral, function (item) { return mapping.fromJS(new DetalleSolicitudIntegral(item)) });
                            self.detallesSolicitudIntegral(detalles_solicitud);

                            self.ObtenerCapacidadesReprogramacion(self.solicitudIntegral.IdSolicitudIntegral(), $('#txtNuevaFechaSubida').datepicker('getDate'), $("#txtNuevaFechaBajada").datepicker('getDate'));
                        }
                    }
                    else
                    {
                        //Error
                        ShowException(ex);
                    }
                },
                beforeSend: function ()
                {
                    $("#image-loading-reprogramar").show();
                },
                complete: function ()
                {
                    $("#image-loading-reprogramar").hide();
                }
            });
        }; //fin: ReprogramarSolicitudIntegral()
        

        self.ActualizarDetallesSolicitudAReprogramar = function ()
        {
            $.each(self.capacidadesReprogramacion(), function (key, value)
            {

                var pasajeros_mismo_origen_destino_seleccionados_programacion = Enumerable.From(self.detallesSolicitudIntegral()).Where(function (x) { return x.IdInstalacionOrigen() == value.FkIdInstalacionOrigen && x.IdInstalacionDestino() == value.FkIdInstalacionDestino && x.SeleccionadoReprogramacion() == true }).Select(function (x) { return x; }).ToArray();
                var cantidad_total_a_reprogramar = value.CantidadTotalPasajerosProgramar();

                value.CapacidadRestantePasajerosPorProgramar = cantidad_total_a_reprogramar - pasajeros_mismo_origen_destino_seleccionados_programacion.length;

                var pasajeros_mismo_origen_destino = Enumerable.From(self.detallesSolicitudIntegral()).Where(function (x) { return x.IdInstalacionOrigen() == value.FkIdInstalacionOrigen && x.IdInstalacionDestino() == value.FkIdInstalacionDestino }).Select(function (x) { return x; }).ToArray();
                var total_pasajeros_a_programar = value.CapacidadRestantePasajerosPorProgramar;

                //Habilitamos o desabilitamos los checkboxes dependiendo de si ya se alcanzo el total de pasajeros a reprogramar
                $.each(pasajeros_mismo_origen_destino, function (key, value)
                {
                    //Solo se desabilitan los que estan desmarcados
                    value.DisponibleReprogramacion(total_pasajeros_a_programar <= 0 && value.SeleccionadoReprogramacion() == false && value.EstadoOcupanteSolicitudIntegral() != "SOLICITADO" ? false : true);

                    if (value.EstadoOcupanteSolicitudIntegral() != "SOLICITADO")
                    {
                        value.SeleccionadoReprogramacion(false);
                    }
                });

                //Verificamos que no hayan pasajeros reprogramados de mas.
                if (pasajeros_mismo_origen_destino_seleccionados_programacion.length >= cantidad_total_a_reprogramar)
                {
                    var total_de_personas_desmarcar = pasajeros_mismo_origen_destino_seleccionados_programacion.length - cantidad_total_a_reprogramar;
                    //Hay que desmarcar la siguiente cantidad de pasajeros: total_de_personas_desmarcar
                    $.each(pasajeros_mismo_origen_destino_seleccionados_programacion, function (key, value)
                    {
                        if (total_de_personas_desmarcar <= 0)
                        {
                            //equivalente al break en el for de C#
                            return false;
                        }

                        value.SeleccionadoReprogramacion(false);

                        total_de_personas_desmarcar--;
                    });
                }
            });
        }


        self.ItinerarioSelectionChanged = function (nuevoItinerario)
        {
            var id_itinerario_seleccionado = $('#cmbItinerarios').val();
            var itinerario_seleccionado = Enumerable.From(self.itinerariosTransporte()).Where(function (x) { return x.id_itinerario == id_itinerario_seleccionado }).Select(function (x) { return x; }).SingleOrDefault();

            if (itinerario_seleccionado != null && itinerario_seleccionado != "undefined")
            {
                self.solicitudIntegral.FkIdItinerarioTransporteIda(id_itinerario_seleccionado);
                self.solicitudIntegral.Horario(itinerario_seleccionado.horario_itinerario_formateado());

                if (self.solicitudIntegral.FichaAutorizador() != "")
                {
                    self.ConsultarInstalacionesOrigen();
                    self.ConsultarInstalacionesDestinos();

                    //self.ConsultarCapacidadTransporte();

                    self.BorrarDireccionamientosDisponiblesYSeleccionados();
                }
                else
                {
                    self.BorrarInstalacionesOrigenDestinosDireccionamientos();
                    $("#lblCapacidadTransporte").text("0");
                }
            }
            else
            {
                self.BorrarInstalacionesOrigenDestinosDireccionamientos();
                $("#lblCapacidadTransporte").text("0");
            }
        };//fin: ItinerarioSelectionChanged()

        self.ItinerarioRetornoSelectionChanged = function (nuevoItinerario)
        {
            var id_itinerario_seleccionado = $('#cmbItinerariosRetorno').val();
            var itinerario_seleccionado = Enumerable.From(self.itinerariosTransporte()).Where(function (x) { return x.id_itinerario == id_itinerario_seleccionado }).Select(function (x) { return x; }).SingleOrDefault();

            if (itinerario_seleccionado != null && itinerario_seleccionado != "undefined")
            {
                self.solicitudIntegral.FkIdItinerarioTransporteRetorno(id_itinerario_seleccionado);

                if (self.solicitudIntegral.FichaAutorizador() != "")
                {
                    //self.ConsultarCapacidadTransporteRetorno();
                }
                else
                {
                    $("#lblCapacidadTransporteRetorno").text("0");
                }
            }
            else
            {
                $("#lblCapacidadTransporteRetorno").text("0");
            }
        };//fin: ItinerarioRetornoSelectionChanged()


        self.MostrarCalendarioNuevaFechaSubida = function ()
        {
            $('#txtNuevaFechaSubida').datepicker("show");
        };

        self.MostrarCalendarioNuevaFechaBajada = function ()
        {
            $('#txtNuevaFechaBajada').datepicker("show");
        };

        self.BorrarInstalacionesOrigenDestinosDireccionamientos = function ()
        {
            self.instalacionesOrigenDisponibles([]);
            self.instalacionesDestinoDisponibles([]);

            self.BorrarDireccionamientosDisponiblesYSeleccionados();
        }

        self.BorrarDireccionamientosDisponiblesYSeleccionados = function ()
        {
            self.direccionamientosInstalacionesDisponibles([]);

            self.direccionamientosInstalacionDestinoAH([]);
            self.direccionamientosInstalacionDestinoSTP([]);

            self.direccionamientoInstalacionDestinoAHSeleccionado = null;
            self.direccionamientoInstalacionDestinoSTPSeleccionado = null;
        }

        self.SeleccionarDireccionamientoHospedaje = function (direccionamiento_hospedaje)
        {
            self.direccionamientoInstalacionDestinoAHSeleccionado = direccionamiento_hospedaje;

            var elementos_seleccionados = (Enumerable.From(self.direccionamientosInstalacionDestinoAH()).Where(function (x) { return x.SELECCIONADO() == true }).Select(function (x) { return x; }).ToArray());

            $.each(elementos_seleccionados, function (key, value)
            {
                value.SELECCIONADO(false);
            });

            direccionamiento_hospedaje.SELECCIONADO(true);
        };

        self.SeleccionarDireccionamientoTransporte = function (direccionamiento_transporte)
        {
            self.direccionamientoInstalacionDestinoSTPSeleccionado = direccionamiento_transporte;

            var elementos_seleccionados = (Enumerable.From(self.direccionamientosInstalacionDestinoSTP()).Where(function (x) { return x.SELECCIONADO() == true }).Select(function (x) { return x; }).ToArray());

            $.each(elementos_seleccionados, function (key, value)
            {
                value.SELECCIONADO(false);
            });

            direccionamiento_transporte.SELECCIONADO(true);
        };

        self.ObtenerDetalleSolicitud = function (ficha_rfc, movimiento_personal)
        {
            var nuevo_detalle_solicitud = new DetalleSolicitudIntegral();

            var instalacion_origen_seleccionada = Enumerable.From(self.instalacionesOrigenDisponibles()).Where(function (x) { return x.ID_C_M_INSTALACION == self.solicitudIntegral.FKidOrigenSolicitud() }).Select(function (x) { return x; }).SingleOrDefault();
            var instalacion_destino_seleccionada = Enumerable.From(self.instalacionesDestinoDisponibles()).Where(function (x) { return x.ID_C_M_INSTALACION == self.solicitudIntegral.FKidDestinoSolicitud() }).Select(function (x) { return x; }).SingleOrDefault();

            nuevo_detalle_solicitud.FichaRFCEmpleado = ficha_rfc;

            nuevo_detalle_solicitud.FechaSubida = self.solicitudIntegral.FechaSubida();
            nuevo_detalle_solicitud.FechaBajada = self.solicitudIntegral.FechaBajada();

            if (movimiento_personal == "SUBIDA")
            {
                nuevo_detalle_solicitud.IdInstalacionOrigen = instalacion_origen_seleccionada.ID_C_M_INSTALACION;
                nuevo_detalle_solicitud.IdInstalacionDestino = instalacion_destino_seleccionada.ID_C_M_INSTALACION;

                nuevo_detalle_solicitud.ClaveInstalacionOrigen = instalacion_origen_seleccionada.SIGLAS_INSTALACION;
                nuevo_detalle_solicitud.ClaveInstalacionDestino = instalacion_destino_seleccionada.SIGLAS_INSTALACION;

                nuevo_detalle_solicitud.NombreInstalacionOrigen = instalacion_origen_seleccionada.NOMBRE_INSTALACION;
                nuevo_detalle_solicitud.NombreInstalacionDestino = instalacion_destino_seleccionada.NOMBRE_INSTALACION;
            }
            else
            {
                nuevo_detalle_solicitud.IdInstalacionDestino = instalacion_origen_seleccionada.ID_C_M_INSTALACION;
                nuevo_detalle_solicitud.IdInstalacionOrigen = instalacion_destino_seleccionada.ID_C_M_INSTALACION;

                nuevo_detalle_solicitud.ClaveInstalacionDestino = instalacion_origen_seleccionada.SIGLAS_INSTALACION;
                nuevo_detalle_solicitud.ClaveInstalacionOrigen = instalacion_destino_seleccionada.SIGLAS_INSTALACION;

                nuevo_detalle_solicitud.NombreInstalacionDestino = instalacion_origen_seleccionada.NOMBRE_INSTALACION;
                nuevo_detalle_solicitud.NombreInstalacionOrigen = instalacion_destino_seleccionada.NOMBRE_INSTALACION;
            }

            nuevo_detalle_solicitud.fk_itinerario = self.solicitudIntegral.FkIdItinerarioTransporteIda;

            nuevo_detalle_solicitud.TipoMovimientoPersonal = movimiento_personal;

            if (self.direccionamientoInstalacionDestinoAHSeleccionado != null && self.direccionamientoInstalacionDestinoAHSeleccionado != "undefined")
            {
                nuevo_detalle_solicitud.FondoDireccionamientoAH = self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_FONDO;
                nuevo_detalle_solicitud.CentroGestorDireccionamientoAH = self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_CTO_GESTOR;
                nuevo_detalle_solicitud.ElementoPEPDireccionamiento = self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_ELEMENTO_PEP;
                nuevo_detalle_solicitud.CuentaMayorDireccionamientoAH = self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_CTA_MAYOR;
                nuevo_detalle_solicitud.ProgramaPresupuestalDireccionamientoAH = self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_PROGRAMA_PRESUPUESTAL;
                nuevo_detalle_solicitud.PosicionPresupuestalDireccionamientoAH = self.direccionamientoInstalacionDestinoAHSeleccionado.CVE_POSICION_PRESUPUESTARIA;
            }

            if (self.direccionamientoInstalacionDestinoSTPSeleccionado != null && self.direccionamientoInstalacionDestinoSTPSeleccionado != "undefined")
            {
                nuevo_detalle_solicitud.FondoDireccionamientoSTP = self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_FONDO;
                nuevo_detalle_solicitud.CentroGestorDireccionamientoSTP = self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_CTO_GESTOR;
                nuevo_detalle_solicitud.ElementoPEPDireccionamientoSTP = self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_ELEMENTO_PEP;
                nuevo_detalle_solicitud.CuentaMayorDireccionamientoSTP = self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_CTA_MAYOR;
                nuevo_detalle_solicitud.ProgramaPresupuestalDireccionamientoSTP = self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_PROGRAMA_PRESUPUESTAL;
                nuevo_detalle_solicitud.PosicionPresupuestalDireccionamientoSTP = self.direccionamientoInstalacionDestinoSTPSeleccionado.CVE_POSICION_PRESUPUESTARIA;
            }

            nuevo_detalle_solicitud.TipoPersonal = self.solicitudIntegral.TipoPersonal();

            return nuevo_detalle_solicitud;
        }; //fin: ObtenerDetalleSolicitud()

        self.HabilitarDesabilitarControlesSegunEstadoSolicitud = function ()
        {
            //si no es una solicitud Nueva entonces hay que desabilitar los controles maestros
            if ((parseInt(mapping.toJS(self.solicitudIntegral.IdSolicitudIntegral)) <= 0))
            {
                $('#lblFechaSubida').removeAttr('disabled');
                $('#lblFechaBajada').removeAttr('disabled');
                $('#lblSolicitante').removeAttr('disabled');
                $('#lblAutorizador').removeAttr('disabled');
                $('#lblContrato').removeAttr('disabled');
                $('#cmbItinerarios').removeAttr('disabled');
                $('#cmbItinerariosRetorno').removeAttr('disabled');
            }
            else
            {
                $('#lblFechaSubida').attr('disabled', true);
                $('#lblFechaBajada').attr('disabled', true);
                $('#lblSolicitante').attr('disabled', true);
                $('#lblAutorizador').attr('disabled', true);
                $('#lblContrato').attr('disabled', true);
                $('#cmbItinerarios').attr('disabled', true);
                $('#cmbItinerariosRetorno').attr('disabled', true);
            }
        }; //fin: HabilitarDesabilitarControlesSegunEstadoSolicitud()

        self.ActualizarSolicitudIntegral = function (data)
        {
            self.solicitudIntegral.IdSolicitudIntegral(data.IdSolicitudIntegral);
            self.solicitudIntegral.FolioSolicitudIntegral(data.FolioSolicitudIntegral);
            self.solicitudIntegral.FkIdItinerarioTransporteIda(data.FkIdItinerarioTransporteIda);
            self.solicitudIntegral.FkIdItinerarioTransporteRetorno(data.FkIdItinerarioTransporteRetorno);
            self.solicitudIntegral.TipoSolicitudIntegral(data.TipoSolicitudIntegral);
            self.solicitudIntegral.TipoPersonal(data.TipoPersonal);
            self.solicitudIntegral.TipoTransporte(data.TipoTransporte);
            self.solicitudIntegral.TipoServicio(data.TipoServicio);
            self.solicitudIntegral.FechaCreacion(parseDate(data.FechaCreacion));
            self.solicitudIntegral.FechaSubida(parseDate(data.FechaSubida));
            self.solicitudIntegral.FechaBajada(parseDate(data.FechaBajada));
            self.solicitudIntegral.FichaSolicitante(data.FichaSolicitante);
            self.solicitudIntegral.NombreSolicitante(data.NombreSolicitante);
            self.solicitudIntegral.FichaAutorizador(data.FichaAutorizador);
            self.solicitudIntegral.NombreAutorizador(data.NombreAutorizador);
            self.solicitudIntegral.NumeroContrato(data.NumeroContrato);
            self.solicitudIntegral.NombreAcreedor(data.NombreAcreedor);
            self.solicitudIntegral.RFCAcreedor(data.RFCAcreedor);
            //self.solicitudIntegral.Horario(data.HoraSalidaItinerarioTransporte);
            self.solicitudIntegral.FKidOrigenSolicitud(data.FKidOrigenSolicitud);
            self.solicitudIntegral.FKidDestinoSolicitud(data.FKidDestinoSolicitud);
            self.solicitudIntegral.SubdivisionAutorizador(data.SubdivisionAutorizador);
            self.solicitudIntegral.SubdivisionSolicitante(data.SubdivisionSolicitante);
            self.solicitudIntegral.FolioEspecialHospedaje(data.FolioEspecialHospedaje);
            self.solicitudIntegral.FolioEspecialTransporte(data.FolioEspecialTransporte);
            self.solicitudIntegral.EstadoSolicitudIntegral(data.EstadoSolicitudIntegral);
        }; //fin: ActualizarSolicitudIntegral()

        self.SeleccionarDetalleReprogramacion = function (e)
        {
            var i = 0;
            var listado_detalles_encontrados = Enumerable.From(self.detallesSolicitudIntegral()).Where(function (x) { return x.IdInstalacionOrigen() == e.FkIdInstalacionOrigen && x.IdInstalacionDestino() == e.FkIdInstalacionDestino }).Select(function (x) { return x; }).ToArray();
            var listado_otros_detalles_encontrados = Enumerable.From(self.detallesSolicitudIntegral()).Where(function (x) { return !(x.IdInstalacionOrigen() == e.FkIdInstalacionOrigen && x.IdInstalacionDestino() == e.FkIdInstalacionDestino) }).Select(function (x) { return x; }).ToArray();
            
            //Deseleccionado todos los elementos que no cumplan el criterio de busqueda (id_origen e id_destino)
            $.each(listado_otros_detalles_encontrados, function (index, value)
            {
                value.ElementoSeleccionado(false);
            });

            //Alternando la seleccion para los items que si cumplieron con el criterio de busqueda.
            $.each(listado_detalles_encontrados, function (index, value)
            {
                value.ElementoSeleccionado(!value.ElementoSeleccionado());
            });
        }

        self.SeleccionarDetalleSolicitudIntegral = function (e, obj)
        {
            var i = 0;

            if (obj.target.nodeName != "INPUT" && obj.target.type != "checkbox")
            {
                if (e.DisponibleReprogramacion() == true)
                {
                    if (e.EstadoOcupanteSolicitudIntegral() == "SOLICITADO")
                    {
                        e.SeleccionadoReprogramacion(!e.SeleccionadoReprogramacion());
                    }
                    else
                    {
                        e.ElementoSeleccionado(false);
                    }
                    
                }
            }

            self.ActualizarDetallesSolicitudAReprogramar();

            return true;
        }

        self.ActualizarCapacidadReprogramacionOrigenDestinoRelacionada = function (obj)
        {
            var listado_detalles_encontrados = Enumerable.From(self.detallesSolicitudIntegral()).Where(function (x) { return x.IdInstalacionOrigen() == obj.FkIdInstalacionOrigen && x.IdInstalacionDestino() == obj.FkIdInstalacionDestino }).Select(function (x) { return x; }).ToArray();

        }; //Fin self.ActualizarCapacidadReprogramacionOrigenDestinoRelacionada



    };//fin SolicitudIntegralController()

});//fin define

