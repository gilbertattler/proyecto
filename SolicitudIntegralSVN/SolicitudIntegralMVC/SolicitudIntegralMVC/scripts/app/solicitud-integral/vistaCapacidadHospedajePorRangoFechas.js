﻿define(['jquery', 'jquery.ui', 'dateJs'], function ($)
{
    //Indispensable para evitar que el &%$% Internet Explorer use el cache
    $.ajaxSetup({ cache: false });

    return function vistaCapacidadHospedajePorRangoFechas()
	{
		var self = this;

		self.dialog_capacidad_hospedaje_rango_fechas = null;

		self.Mostrar = function (id_instalacion, fecha_subida, fecha_bajada,subdivision_autorizador, contrato, folio_especial, elementId)
		{
		    var fecha_subida_js = Date.parse(fecha_subida);
		    var fecha_bajada_js = Date.parse(fecha_bajada);
		    
		    elementId = "#" + elementId;
		    $(elementId).before("<div id='vistaCapacidadHospedajePorRangoFechas'></div>");

		    var load_url = rootDir + "SolicitudIntegral/VistaCapacidadHospedajePorRangoFechas/" + id_instalacion + "/" + fecha_subida_js.toString('yyyy-MM-dd') + "/" + fecha_bajada_js.toString('yyyy-MM-dd') + "/" + (subdivision_autorizador == "" ? "-" : subdivision_autorizador) + "/" + (contrato == "" ? "-" : contrato) + "/" + (folio_especial == "" ? "-" : encodeURIComponent(folio_especial));
		    $('#vistaCapacidadHospedajePorRangoFechas').load(load_url, self.LoadCapacidadesHospedaje);

		    self.dialog_capacidad_hospedaje_rango_fechas = $('#vistaCapacidadHospedajePorRangoFechas').dialog({ position: { my: "center" }, title: "Detalle de Capacidades de Hospedaje", minWidth: 600, modal: true, closeText: 'x', resizable: false, close: function (event, ui) { $('#vistaCapacidadHospedajePorRangoFechas').remove(); } });
		    self.dialog_capacidad_hospedaje_rango_fechas.dialog('open');
			// prevent the default action, e.g., following a link
		};

		self.LoadCapacidadesHospedaje = function (responseText, textStatus, XMLHttpRequest)
		{
		    $('#btnCerrar').click(function ()
			{
		        self.dialog_capacidad_hospedaje_rango_fechas.dialog('close');
				return false;
			});

		    self.dialog_capacidad_hospedaje_rango_fechas.dialog({ position: 'center' });
		};
	};
});