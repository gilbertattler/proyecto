﻿define(['jquery', 'knockout', 'knockout.mapping', 'libs/exception.handling', 'jquery.ui', 'dateJs'], function ($, ko, mapping) {
    return function BusquedaxInstalacion() {
        var self = this;
        self.fraseConsulta = ko.observable();
        self.dialog_lista_itinerarios = null;
        self.listado_itinerarios = ko.observableArray();

        self.MostrarBuscadorxInstalacion = function (elementId) {

            elementId = "#" + elementId;
            $(elementId).before("<div id='dialogo-busqueda-itinerario'></div>");
            $('#dialogo-busqueda-itinerario').load(rootDir + "SolicitudIntegral/BusquedaItinerariosxIntalacion", self.LoadListaDestinos);

            self.dialog_lista_destinos = $('#dialogo-busqueda-itinerario').dialog({ position: { my: "center" }, title: "Busqueda por Instalación", minWidth: 600, modal: true, closeText: 'x', resizable: false, close: function (event, ui) { $('#dialogo-busqueda-itinerario').remove(); } });
            self.dialog_lista_destinos.dialog('open');
        };

        self.LoadListaDestinos = function (responseText, textStatus, XMLHttpRequest) {
            $('#btnCerrar').click(function () {
                self.dialog_lista_destinos.dialog('close');
                return false;
            });

            ko.applyBindings(self, $("#frmBusquedaxInstalacion")[0]);

            self.dialog_lista_destinos.dialog({ position: 'center' });
        };

        self.ConsultarItinerarioxInstalacion = function () {
            self.BuscarItinerarios(self.fraseConsulta());
        };

        self.BuscarItinerarios = function (frase) {
            $.ajax(rootDir + "SolicitudIntegral/BuscarItinerarioxIntalacion",
            {
                data: {
                    fraseBusqueda: (frase == null ? '' : frase)
                },
                type: "get",
                contentType: "application/json",
                success: function (response, status, xhr) {
                    var ex = HandleException(response, status, xhr);
                    if (ex == null) {
                        var mappedTasks = ko.utils.arrayMap(response.result, function (item) { return item });
                        self.listado_itinerarios(mappedTasks);
                    }
                    else {
                        ShowException(ex);
                    }
                }
            });
        };

    };
});
