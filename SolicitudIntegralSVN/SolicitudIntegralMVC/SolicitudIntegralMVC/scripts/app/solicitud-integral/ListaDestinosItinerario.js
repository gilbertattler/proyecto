﻿define(['jquery', 'knockout', 'knockout.mapping', 'libs/exception.handling', 'jquery.ui', 'dateJs'], function ($, ko, mapping) {
    return function ListaDestinoItinerario() {
        var self = this;
        self.dialog_lista_destinos = null;
        self.listado_Destinos_Itinerario = ko.observableArray();
        self.cve_itinerario = ko.observable();
        self.des_itinerario = ko.observable();
        self.cve_nodo_origen = ko.observable();
        self.cve_nodo_destino = ko.observable();
        self.cve_tipo_servicio = ko.observable();
        self.des_tipo_servicio = ko.observable();
        self.CVE_AREA_SERVICIO = ko.observable();

        self.MostrarDestinos = function (elementId, idItinerario) {

            elementId = "#" + elementId;
            $(elementId).before("<div id='dialogo-Destino-Itinerario'></div>");
            $('#dialogo-Destino-Itinerario').load(rootDir + "SolicitudIntegral/ListaDestinosItinerario", self.LoadListaDestinos);

            self.dialog_lista_destinos = $('#dialogo-Destino-Itinerario').dialog({ position: { my: "center" }, title: "Listado de destinos", minWidth: 600, modal: true, closeText: 'x', resizable: false, close: function (event, ui) { $('#dialogo-Destino-Itinerario').remove(); } });
            self.dialog_lista_destinos.dialog('open');
            self.ListarDestinos(idItinerario);

        };

        self.LoadListaDestinos = function (responseText, textStatus, XMLHttpRequest) {
            $('#btnCerrar').click(function () {
                self.dialog_lista_destinos.dialog('close');
                return false;
            });

            ko.applyBindings(self, $("#frmListaDestinosItinerario")[0]);

            self.dialog_lista_destinos.dialog({ position: 'center' });
        };

        self.ListarDestinos = function (idItinerario) {
            $.ajax(rootDir + "SolicitudIntegral/ListarDestinosItinerario",
            {
                data: {
                    id_itinerario: idItinerario
                },
                type: "get",
                contentType: "application/json",
                success: function (response, status, xhr) {
                    var ex = HandleException(response, status, xhr);
                    if (ex == null) {
                        var mappedTasks = ko.utils.arrayMap(response.result, function (item) { return item });
                        self.listado_Destinos_Itinerario(mappedTasks);

                        if (typeof mappedTasks[0] != "undefined")
                        {
                            self.cve_itinerario(mappedTasks[0].cve_itinerario);
                            self.des_itinerario(mappedTasks[0].des_itinerario);
                            self.CVE_AREA_SERVICIO(mappedTasks[0].CVE_AREA_SERVICIO);
                            self.cve_nodo_origen(mappedTasks[0].cve_nodo_origen);
                            self.cve_nodo_destino(mappedTasks[0].cve_nodo_destino);
                            self.cve_tipo_servicio(mappedTasks[0].cve_tipo_servicio);
                            self.des_tipo_servicio(mappedTasks[0].des_tipo_servicio);
                        }
                    }
                    else {
                        //Error
                        ShowException(ex);
                    }
                }
            });
        };

    };
});
