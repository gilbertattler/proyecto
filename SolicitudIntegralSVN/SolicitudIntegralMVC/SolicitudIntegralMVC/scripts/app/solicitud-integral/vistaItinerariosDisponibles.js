﻿define(['jquery', 'jquery.ui', 'dateJs'], function ($)
{
    //Indispensable para evitar que el &%$% Internet Explorer use el cache
    $.ajaxSetup({ cache: false });

    return function VistaItinerariosDisponibles()
	{
		var self = this;

		self.dialog_listado_itinerarios = null;

		self.Mostrar = function (tipo_transporte, tipo_movimiento, fecha_consulta,subdivision,contrato,folio_especial_ida,folio_especial_regreso ,elementId)
		{
		    var fecha_consulta_js = Date.parse(fecha_consulta);

		    elementId = "#" + elementId;
			$(elementId).before("<div id='vistaItinerariosDisponibles'><span id='image-loading-vista-itinerarios' class='loading-image' ><img src='" + rootDir + "/Content/images/icons/loading.gif'/></span> </div>");
			$('#vistaItinerariosDisponibles').load(rootDir + "SolicitudIntegral/VistaItinerariosPorDia/" + tipo_transporte + "/" + encodeURIComponent(tipo_movimiento) + "/" + fecha_consulta_js.toString('yyyy-MM-dd') + "/" + subdivision + "/" + contrato + "/" + encodeURIComponent(folio_especial_ida) + "/" + encodeURIComponent(folio_especial_regreso), self.LoadistadoItinerariosDisponibles);
			self.dialog_listado_itinerarios = $('#vistaItinerariosDisponibles').dialog({ position: { my: "center" }, title: "Listado de Itinerarios por fecha", minWidth: 600, modal: true, closeText: 'x', resizable: false, close: function (event, ui) { $('#vistaItinerariosDisponibles').remove(); } });
			self.dialog_listado_itinerarios.dialog('open');
			// prevent the default action, e.g., following a link
		};

		self.LoadistadoItinerariosDisponibles = function (responseText, textStatus, XMLHttpRequest)
		{
		    $("#image-loading-vista-itinerarios").remove();

		    $('#btnCerrar').click(function ()
			{
		        self.dialog_listado_itinerarios.dialog('close');
				return false;
			});


			self.dialog_listado_itinerarios.dialog({ position: 'center' });
		};
	};
});