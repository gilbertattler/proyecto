﻿define([
    'jquery', 
    'knockout',
    'knockout.mapping',
    'app/models/stp-integral/detalleSolicitudIntegral',
    'app/models/utilerias/EmpleadoPemexCia',
    'app/catalogo/catalogo-empleado-cia',
    'app/catalogo/catalogo-empleado-pemex',
    'app/models/gestion-gl/CEmpleadosCia',
    'app/models/gestion-gl/CEmpleadosPemex',
    'libs/exception.handling',
    'jquery.ui'
], function ($, ko, mappings, DetalleSolicitudIntegral, EmpleadoPemexCia, VistaCatalogoEmpleadoCia, VistaCatalogoEmpleadoPemex, CEmpleadosCia, CEmpleadosPemex)
{
    //Indispensable para evitar que el &%$% Internet Explorer use el cache
    $.ajaxSetup({ cache: false });

    return function vistaDetallesEmpleado()
	{
        var self = this;

        self.TipoMovimiento = "";
        self.TipoPersonal = "";
        self.TipoSolicitudIntegral = "";
        self.TipoTransporte = "";
        self.IdDetalleSolicitudIntegral = 0;
        self.CallbackReemplazoEmpleado = null;

		self.dialog_permisos_autorizadores = null;

		self.Mostrar = function (id_detalle_empleado, tipo_solicitud_integral, tipo_transporte, tipoPersonal, tipoMovimiento, callbackReemplazoEmpleado, elementId)
		{
		    elementId = "#" + elementId;
		    $(elementId).before("<div id='VistaDetallesEmpleado'></div>");
		    $('#VistaDetallesEmpleado').load(rootDir + "SolicitudIntegral/VistaDetallesEmpleado/" + id_detalle_empleado.toString(), self.LoadVistaDetallesEmpleado);

		    self.TipoSolicitudIntegral = tipo_solicitud_integral;
		    self.TipoTransporte = tipo_transporte;
		    self.TipoPersonal = tipoPersonal;
		    self.TipoMovimiento = tipoMovimiento;
		    self.IdDetalleSolicitudIntegral = id_detalle_empleado;
		    self.CallbackReemplazoEmpleado = callbackReemplazoEmpleado;

			self.dialog_permisos_autorizadores = $('#VistaDetallesEmpleado').dialog({ position: { my: "center" }, title: "Detalles de Empleado", minWidth: 600, modal: true, closeText: 'x', resizable: false, close: function (event, ui) { $('#VistaDetallesEmpleado').remove(); } });
			self.dialog_permisos_autorizadores.dialog('open');
			// prevent the default action, e.g., following a link
		};

		self.CambiarUsuario = function ()
		{
		    $("#cambio-empleado").toggle();

		    if ($("#cambio-empleado").css('display') != 'none')
		    {
		        $("#hplCambiarEmpleado").text("Cancelar");
		    }
		    else
		    {
		        $("#txtNuevoEmpleado").val("");
		        $("#lblNuevoEmpleadoReemplazo").text("");
		        $("#hplCambiarEmpleado").text("Cambiar");
		        $("#hplGuardarReemplazo").hide();
		    }
		}

		self.LoadVistaDetallesEmpleado = function (responseText, textStatus, XMLHttpRequest)
		{
		    $('#btnCerrar').click(function ()
			{
		        self.dialog_permisos_autorizadores.dialog('close');
				return false;
			});

		    self.dialog_permisos_autorizadores.dialog({ position: 'center' });

		    ko.applyBindings(self, $("#vista-detalle-empleado")[0]);
		};

		self.GuardarReemplazo = function ()
		{
		    self.ReemplazarDetalleSolicitudIntegral();
		}

		self.InputKeyPress = function (model, e)
		{
		    if (e.keyCode == 13)
		    {
		        self.ValidarNuevoEmpleadoReemplazo();
		    }
		    return true;
		}

		self.ValidarNuevoEmpleadoReemplazo = function ()
		{
		    var ficha_rfc_reemplazo = $("#txtNuevoEmpleado").val();

		    if (ficha_rfc_reemplazo != "")
		    {
		        self.BuscarFichaRFC(ficha_rfc_reemplazo, self.TipoMovimiento);
		    }
		    else
		    {
		        alert("Debe especificar una ficha o RFC");
		    }
		}

		self.BuscarFichaRFC = function (ficha_rfc, movimiento_personal)
		{
		    if (ficha_rfc != "undefined" && ficha_rfc != "")
		    {
		        var empleado_pemex_cia = self.ValidarFichaRFC(ficha_rfc, self.TipoPersonal, movimiento_personal);

		        if (empleado_pemex_cia != null && empleado_pemex_cia != undefined && empleado_pemex_cia.IdEmpleadoPemexCia > 0)
		        {
		            $("#lblNuevoEmpleadoReemplazo").text(empleado_pemex_cia.NombreCompleto);
		            $("#txtNuevoEmpleado").val(empleado_pemex_cia.FichaRfc);
		            $("#hplGuardarReemplazo").show();

		            if (empleado_pemex_cia.TipoEmpleado != "PEMEX" && (Date.parse(parseDate(empleado_pemex_cia.VigenciaLibretaMar)) < Date.today() || empleado_pemex_cia.FolioLibretaMar == ""))
		            {
		                var catalogo_empleado_cia = new CEmpleadosCia();

		                catalogo_empleado_cia.ID_C_EMPLEADO_CIA = empleado_pemex_cia.IdEmpleadoPemexCia;
		                catalogo_empleado_cia.ID_C_CONTRATO = null;
		                catalogo_empleado_cia.RFC_FM3 = empleado_pemex_cia.FichaRfc;
		                catalogo_empleado_cia.NOMBRES = empleado_pemex_cia.Nombres;
		                catalogo_empleado_cia.APELLIDOS = empleado_pemex_cia.Apellidos;
		                catalogo_empleado_cia.CATEGORIA = empleado_pemex_cia.Categoria;
		                catalogo_empleado_cia.GENERO = empleado_pemex_cia.Genero;
		                catalogo_empleado_cia.NACIONALIDAD = empleado_pemex_cia.Nacionalidad;
		                catalogo_empleado_cia.CURP = empleado_pemex_cia.CURP;
		                catalogo_empleado_cia.FOLIO_LIBRETA_MAR = empleado_pemex_cia.FolioLibretaMar;
		                catalogo_empleado_cia.VIGENCIA_LIBRETA_MAR = parseDate(empleado_pemex_cia.VigenciaLibretaMar);
		                catalogo_empleado_cia.BORRADO_LOGICO = false;

		                var vista_catalogo_empleados_cia = new VistaCatalogoEmpleadoCia();

		                //Hay que actualizar la libreta de mar y vigencia.

		                vista_catalogo_empleados_cia.MostrarCatalogo(catalogo_empleado_cia, "vista-detalle-empleado", "Nuevo", self.CallBackVistaEmpleadosCia);
		            }
		        }
		        else
		        {
		            $("#lblNuevoEmpleadoReemplazo").text("");
		            $("#hplGuardarReemplazo").hide();

		            var answer = confirm('No se encontro ningun empleado con la ficha: ' + ficha_rfc + ' \xBFDesea darlo de alta?')

		            if (answer)
		            {
		                if (self.TipoPersonal == "PEMEX")
		                {
		                    var catalogo_empleado_pemex = new CEmpleadosPemex();

		                    catalogo_empleado_pemex.FICHA = ficha_rfc;

		                    var vista_catalogo_empleados_pemex = new VistaCatalogoEmpleadoPemex();

		                    vista_catalogo_empleados_pemex.MostrarCatalogo(catalogo_empleado_pemex, "vista-detalle-empleado", "Nuevo", self.CallBackVistaEmpleadosPemex);
		                }
		                else
		                {
		                    var catalogo_empleado_cia = new CEmpleadosCia();

		                    catalogo_empleado_cia.RFC_FM3 = ficha_rfc;

		                    var vista_catalogo_empleados_cia = new VistaCatalogoEmpleadoCia();
		                    
		                    vista_catalogo_empleados_cia.MostrarCatalogo(catalogo_empleado_cia, "vista-detalle-empleado", "Nuevo", self.CallBackVistaEmpleadosCia);
		                }
		            }
		        }
		    }
		    return true;
		}; //fin: AgregarFichaRFC

		self.ValidarFichaRFC = function (fichaRfc, tipoEmpleado, movimiento_personal)
		{
		    var datos_empleado = null;

		    if ($.trim(fichaRfc) == '')
		    {
		        ShowException("Debe especificar una ficha/rfc para la busqueda del empleado");
		    }
		    else
		    {
		        $.ajax({
		            url: rootDir + "SolicitudIntegral/ValidarFichaRFC/",
		            dataType: "json",
		            data:
                    {
                        ficha_rfc: fichaRfc,
                        tipo_empleado: tipoEmpleado,
                    },
		            async: false,
		            success: function (response, status, xhr)
		            {
		                var ex = HandleException(response, status, xhr);

		                if (ex == null)
		                {
		                    if (response.result != null || response.result != "undefined")
		                    {
		                        datos_empleado = new EmpleadoPemexCia(response.result);
		                    }
		                }
		                else
		                {
		                    //Error
		                    ShowException(ex);
		                }
		            },
		            beforeSend: function ()
		            {
		                $("#image-loading-busqueda-catalogo-empleados-reemplazar").show();
		            },
		            complete: function ()
		            {
		                $("#image-loading-busqueda-catalogo-empleados-reemplazar").hide();
		            }
		        });
		    }

		    return datos_empleado;
		} // fin: ValidarFichaRFC

		self.ModificarEmpleadoSubida = function (ficha_rfc)
		{
            var empleado_pemex_cia = self.ValidarFichaRFC(ficha_rfc, self.solicitudIntegral.TipoPersonal, 'SUBIDA');

            if (empleado_pemex_cia != null && empleado_pemex_cia != undefined && empleado_pemex_cia.IdEmpleadoPemexCia > 0)
            {
                if (empleado_pemex_cia.TipoEmpleado != "PEMEX")
                {
                    var catalogo_empleado_cia = new CEmpleadosCia();

                    catalogo_empleado_cia.ID_C_EMPLEADO_CIA = empleado_pemex_cia.IdEmpleadoPemexCia;
                    catalogo_empleado_cia.ID_C_CONTRATO = null;
                    catalogo_empleado_cia.RFC_FM3 = empleado_pemex_cia.FichaRfc;
                    catalogo_empleado_cia.NOMBRES = empleado_pemex_cia.Nombres;
                    catalogo_empleado_cia.APELLIDOS = empleado_pemex_cia.Apellidos;
                    catalogo_empleado_cia.CATEGORIA = empleado_pemex_cia.Categoria;
                    catalogo_empleado_cia.GENERO = empleado_pemex_cia.Genero;
                    catalogo_empleado_cia.NACIONALIDAD = empleado_pemex_cia.Nacionalidad;
                    catalogo_empleado_cia.CURP = empleado_pemex_cia.CURP;
                    catalogo_empleado_cia.FOLIO_LIBRETA_MAR = empleado_pemex_cia.FolioLibretaMar;
                    catalogo_empleado_cia.VIGENCIA_LIBRETA_MAR = parseDate(empleado_pemex_cia.VigenciaLibretaMar);
                    catalogo_empleado_cia.BORRADO_LOGICO = false;

                    var vista_catalogo_empleados_cia = new VistaCatalogoEmpleadoCia();

                    vista_catalogo_empleados_cia.MostrarCatalogo(catalogo_empleado_cia, "solicitud-integral", "Modificar", self.CallBackVistaEmpleadosCia);
                }
                else
                {
                    var catalogo_empleado_pemex = new CEmpleadosPemex();

                    catalogo_empleado_pemex.ID_C_EMPLEADO_PEMEX = empleado_pemex_cia.IdEmpleadoPemexCia;
                    catalogo_empleado_pemex.ID_C_CONTRATO = null;
                    catalogo_empleado_pemex.FICHA = empleado_pemex_cia.FichaRfc;
                    catalogo_empleado_pemex.RFC = "";
                    catalogo_empleado_pemex.CURP = "";
                    catalogo_empleado_pemex.NOMBRES = empleado_pemex_cia.Nombres;
                    catalogo_empleado_pemex.APELLIDOS = empleado_pemex_cia.Apellidos;
                    catalogo_empleado_pemex.CATEGORIA = empleado_pemex_cia.Categoria;
                    catalogo_empleado_pemex.CARGO = "";
                    catalogo_empleado_pemex.GENERO = empleado_pemex_cia.Genero
                    catalogo_empleado_pemex.EXTENSION_TELEFONICA = "";
                    catalogo_empleado_pemex.EMAIL = "";
                    catalogo_empleado_pemex.BORRADO_LOGICO = false;

                    var vista_catalogo_empleados_pemex = new VistaCatalogoEmpleadoPemex();

                    vista_catalogo_empleados_pemex.MostrarCatalogo(catalogo_empleado_pemex, "vista-detalle-empleado", "Modificar", self.CallBackVistaEmpleadosPemex);
                }
            }
        };

        self.AltaEmpleados = function (tipo_movimiento)
        {
            //VistaCatalogoEmpleadoCia, VistaCatalogoEmpleadoPemex,  CEmpleadosCia, CEmpleadosPemex
            //Hace falta validar que el catalogo sea de PEMEX o CIA dependiendo el tipo de Solicitud integral
            if (self.TipoPersonal == "PEMEX")
            {
                var catalogo_empleado_pemex = new CEmpleadosPemex();
                var vista_catalogo_empleados_pemex = new VistaCatalogoEmpleadoPemex();
                
                vista_catalogo_empleados_pemex.MostrarCatalogo(catalogo_empleado_pemex, "vista-detalle-empleado", "Nuevo", self.CallBackVistaEmpleadosPemex);
            }
            else
            {
                var catalogo_empleado_cia = new CEmpleadosCia();
                var vista_catalogo_empleados_cia = new VistaCatalogoEmpleadoCia();

                vista_catalogo_empleados_cia.MostrarCatalogo(catalogo_empleado_cia, "vista-detalle-empleado", "Nuevo", self.CallBackVistaEmpleadosCia);
            }
            return false;
        };

        self.CallBackVistaEmpleadosCia = function (empleadoCia, operacion)
        {
            switch (operacion)
            {
                case "Nuevo":
                {
                    //$("#listAgregarPersonalSubida").tagit("add", { label: empleadoCia.RFC_FM3, value: empleadoCia.RFC_FM3 })
                    $("#lblNuevoEmpleadoReemplazo").text(empleadoCia.NOMBRES + ' ' + empleadoCia.APELLIDOS);
                    $("#txtNuevoEmpleado").val(empleadoCia.RFC_FM3);
                    $("#hplGuardarReemplazo").show();
                    break;
                }
            }
        };

        self.CallBackVistaEmpleadosPemex = function (empleadoPemex, operacion)
        {
            switch (operacion)
            {
                case "Nuevo":
                {
                    $("#lblNuevoEmpleadoReemplazo").text(empleadoPemex.NOMBRES + ' ' + empleadoPemex.APELLIDOS);
                    $("#txtNuevoEmpleado").val(empleadoPemex.FICHA);
                    $("#hplGuardarReemplazo").show();
                    break;
                }
            }
        };

        self.ReemplazarDetalleSolicitudIntegral = function ()
        {
            var id_detalle_solicitud_integral = self.IdDetalleSolicitudIntegral;
            var nuevo_ficha_rfc = $("#txtNuevoEmpleado").val();

            $.ajax({
                url: rootDir + "SolicitudIntegral/ReemplazarDetalleSolicitudIntegral/",
                data: JSON.stringify({ IdDetalleSolicitudIntegral: id_detalle_solicitud_integral, NuevaFichaRFC: nuevo_ficha_rfc }),
                traditional: true,
                type: "POST",
                dataType: 'json',
                contentType: "application/json",
                async: false,
                success: function (response, status, xhr)
                {
                    var ex = HandleException(response, status, xhr);
                    if (ex == null)
                    {
                        var detalle_solicitud_integral = new DetalleSolicitudIntegral(response.result);
                        
                        $("#infoEmpleado").text("[" + detalle_solicitud_integral.FichaRFCEmpleado + "]" + " " + detalle_solicitud_integral.NombreEmpleado);

                        $("#cambio-empleado").hide();

                        $("#txtNuevoEmpleado").val("");
                        $("#lblNuevoEmpleadoReemplazo").text("");
                        $("#hplCambiarEmpleado").text("Cambiar");
                        $("#hplGuardarReemplazo").hide();

                        if (self.CallbackReemplazoEmpleado != null && self.CallbackReemplazoEmpleado != "nothing")
                        {
                            self.CallbackReemplazoEmpleado(detalle_solicitud_integral);
                        }
                    }
                    else
                    {
                        ShowException(ex);
                    }
                },
                beforeSend: function ()
                {
                    $(event.srcElement).parent("td").children("span.loading-image.eliminar").show();
                },
                complete: function ()
                {
                    $(event.srcElement).parent("td").children("span.loading-image.eliminar").hide();
                }
            });
        }; //fin: EliminarDetalleSolicitudIntegral()

        self.GuardarDetalleObservaciones = function () {
            var id_detalle_solicitud_integral = self.IdDetalleSolicitudIntegral;
            var ficha_ocupante = $("#hFichaRFCEmpleado").val();
            var detalle_observaciones = $("#txtObservacionesDetalle").val();
            var lleva_material = $("#chkLlevaMaterial").is(":checked");

            $.ajax({
                url: rootDir + "SolicitudIntegral/GuardarDetalleObservaciones/",
                data: JSON.stringify({ IdDetalleSolicitudIntegral: id_detalle_solicitud_integral, FichaOcupante: ficha_ocupante, Observaciones: detalle_observaciones, LlevaMaterial: lleva_material }),
                traditional: true,
                type: "POST",
                dataType: 'json',
                contentType: "application/json",
                async: false,
                success: function (response, status, xhr) {
                    var ex = HandleException(response, status, xhr);
                    if (ex == null) {
                        alert("Los datos se guardaron correctamente.");

                        //var detalle_solicitud_integral = new DetalleSolicitudIntegral(response.result);

                        //$("#infoEmpleado").text("[" + detalle_solicitud_integral.FichaRFCEmpleado + "]" + " " + detalle_solicitud_integral.NombreEmpleado);

                        //$("#cambio-empleado").hide();

                        //$("#txtNuevoEmpleado").val("");
                        //$("#lblNuevoEmpleadoReemplazo").text("");
                        //$("#hplCambiarEmpleado").text("Cambiar");
                        //$("#hplGuardarReemplazo").hide();

                        //if (self.CallbackReemplazoEmpleado != null && self.CallbackReemplazoEmpleado != "nothing") {
                        //    self.CallbackReemplazoEmpleado(detalle_solicitud_integral);
                        //}
                    }
                    else {
                        ShowException(ex);
                    }
                },
                beforeSend: function () {
                    $(event.srcElement).parent("td").children("span.loading-image.eliminar").show();
                },
                complete: function () {
                    $(event.srcElement).parent("td").children("span.loading-image.eliminar").hide();
                }
            });
        }; //fin: GuardarDetalleObservaciones()

	};
});