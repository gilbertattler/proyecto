﻿define(['jquery', 'knockout', 'knockout.mapping', 'app/models/gestion-gl/CEmpleadosPemex', 'libs/exception.handling', 'jquery.ui', 'dateJs'], function ($, ko, mapping, CatalogoEmpleadoPemex)
{
    return function CatalogoEmpleadosPemexController()
    {
        var self = this;

        self.EmpleadoPemex = ko.observable();

        //Variable que mantiene el cuadro de dialogo
        self.dialog_catalogo_empleados_pemex = null;

        self.Operacion = null;
        self.callback_function;

        self.MostrarCatalogo = function (empleadoPemex, elementId, operacion, callback_function)
        {
            self.callback_function = callback_function;

            self.Operacion = operacion;

            empleadoPemex = mapping.fromJS(empleadoPemex);
            self.EmpleadoPemex(empleadoPemex);

            elementId = "#" + elementId;
            $(elementId).before("<div id='dialogo'></div>");
            $('#dialogo').load(rootDir + "Catalogos/EmpleadosPemex", self.LoadCatalogoEmpleadoPemex);

            self.dialog_catalogo_empleados_pemex = $('#dialogo').dialog({ position: { my: "center" }, minWidth: 420, title: "Empleados de PEMEX", modal: true, resizable: false, closeText: 'x', close: function (event, ui) { $('#dialogo').remove(); } });
            self.dialog_catalogo_empleados_pemex.dialog('open');
            // prevent the default action, e.g., following a link
        };

        self.LoadCatalogoEmpleadoPemex = function (responseText, textStatus, XMLHttpRequest)
        {
            $('#btnCerrar').click(function ()
            {
                self.dialog_catalogo_empleados_pemex.dialog('close');
                return false;
            });

            if (self.Operacion != "Nuevo")
            {
                $("#btnAceptar").val("Modificar");
            }

            $('#frmCatalogoEmpleadoPemex').submit(function ()
            {
                self.SaveCatalogoEmpleado();
                return false;
            });

            $('#FICHA').focus();

            ko.applyBindings(self, $("#frmCatalogoEmpleadoPemex")[0]);

            self.dialog_catalogo_empleados_pemex.dialog({ position: 'center' });
        };

        self.SaveCatalogoEmpleado = function ()
        {
            var id_empleado_pemex = self.EmpleadoPemex().ID_C_EMPLEADO_PEMEX();

            $.ajax(rootDir + "Catalogos/GuardarEmpleadoPemex",
            {
                data: ko.toJSON(
                {
                    empleadoPemex: mapping.toJS(self.EmpleadoPemex)
                }),
                type: "post",
                contentType: "application/json",
                success: function (response, status, xhr)
                {
                    var ex = HandleException(response, status, xhr);
                    if (ex == null)
                    {
                        var empleado_pemex = new CatalogoEmpleadoPemex(response.result);
                         
                        if (self.dialog_catalogo_empleados_pemex != null && self.dialog_catalogo_empleados_pemex != "undefined")
                        {
                            self.dialog_catalogo_empleados_pemex.dialog('close');
                        }

                        if (self.callback_function != null && self.callback_function != "undefined")
                        {
                            self.callback_function(empleado_pemex, self.Operacion);
                        }

                    }
                    else
                    {
                        //Error
                        ShowException(ex);
                    }
                }
            });
        };

    };
});


