﻿define(['jquery', 'knockout', 'knockout.mapping', 'libs/exception.handling', 'jquery.ui', 'dateJs'], function ($, ko, mapping)
{
    return function CatalogoBusquedaCompanias()
    {
        var self = this;

        //self.EmpleadoCia = ko.observable();

        //Variable que mantiene el cuadro de dialogo
        self.dialog_catalogo_busqueda_cia = null;
        self.listado_companias_busqueda = ko.observableArray();

        self.callback_function;

        self.MostrarCatalogo = function (elementId, callback_function)
        {
            self.callback_function = callback_function;

            elementId = "#" + elementId;
            $(elementId).before("<div id='dialogo-busqueda-cia'></div>");
            $('#dialogo-busqueda-cia').load(rootDir + "Catalogos/BusquedaCompanias", self.LoadCatalogoBusquedaCompanias);

            self.dialog_catalogo_busqueda_cia = $('#dialogo-busqueda-cia').dialog({ position: { my: "center" }, title: "Busqueda de Compañias", minWidth: 600, modal: true, closeText: 'x', resizable: false, close: function (event, ui) { $('#dialogo-busqueda-cia').remove(); } });
            self.dialog_catalogo_busqueda_cia.dialog('open');

            // prevent the default action, e.g., following a link
        };

        self.LoadCatalogoBusquedaCompanias = function (responseText, textStatus, XMLHttpRequest)
        {
            $('#btnCerrar').click(function ()
            {
                self.dialog_catalogo_busqueda_cia.dialog('close');
                return false;
            });

            ko.applyBindings(self, $("#frmBusquedaCompanias")[0]);

            self.dialog_catalogo_busqueda_cia.dialog({ position: 'center' });
        };

        self.BusquedaCompanias = function ()
        {
            $.ajax(rootDir + "Catalogos/ConsultaCompanias",
            {
                data: {
                    buscar: $("#txtBuscar").val(),
                    buscarPor: $("#sltBuscarPor").val()
                },
                type: "get",
                contentType: "application/json",
                success: function (response, status, xhr)
                {
                    var ex = HandleException(response, status, xhr);
                    if (ex == null)
                    {
                        var mappedTasks = ko.utils.arrayMap(response.result, function (item) { return item });
                        self.listado_companias_busqueda(mappedTasks);
                    }
                    else
                    {
                        //Error
                        ShowException(ex);
                    }
                }
            });
        };

        self.SeleccionaCompania = function (data)
        {
            if (self.callback_function != null)
            {
                self.callback_function(data);
                self.dialog_catalogo_busqueda_cia.dialog('close');
            }
        }
    };
});


