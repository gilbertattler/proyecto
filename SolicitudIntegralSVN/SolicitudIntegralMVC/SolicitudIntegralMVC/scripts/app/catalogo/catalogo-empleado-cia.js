﻿define(['jquery', 'knockout', 'knockout.mapping', 'app/models/gestion-gl/CEmpleadosCia', 'libs/exception.handling', 'jquery.ui', 'dateJs'], function ($, ko, mapping, CatalogoEmpleadoCia)
{
    return function CatalogoEmpleadosCiaController()
	{
		var self = this;

		self.EmpleadoCia = ko.observable();

		//Variable que mantiene el cuadro de dialogo
		self.dialog_catalogo_empleados_cia = null;

		self.Operacion = null;
		self.callback_function;

		self.MostrarCatalogo = function (empleadoCia, elementId, operacion, callback_function)
		{
			self.callback_function = callback_function;

			self.Operacion = operacion;

			empleadoCia = mapping.fromJS(empleadoCia);
			self.EmpleadoCia(empleadoCia);

			elementId = "#" + elementId;
			$(elementId).before("<div id='dialogo'></div>");
			$('#dialogo').load(rootDir + "Catalogos/EmpleadosCompania", self.LoadCatalogoEmpleadoCia);

			self.dialog_catalogo_empleados_cia = $('#dialogo').dialog({ position: { my: "center" }, title: "Empleados de Compañia", minWidth: 420, modal: true, closeText: 'x', resizable: false, close: function (event, ui) { $('#dialogo').remove(); } });
			self.dialog_catalogo_empleados_cia.dialog('open');
			// prevent the default action, e.g., following a link
		};

		self.LoadCatalogoEmpleadoCia = function (responseText, textStatus, XMLHttpRequest)
		{
		    $('#btnCerrar').click(function ()
			{
		        self.dialog_catalogo_empleados_cia.dialog('close');
				return false;
			});

		    if (self.Operacion != "Nuevo")
            {
				$("#btnAceptar").val("Modificar");
			}

		    $('#frmCatalogoEmpleadoCia').submit(function ()
			{
				self.SaveCatalogoEmpleado();
				return false;
			});

		    $("#VIGENCIA_LIBRETA_MAR").datepicker();

			$('#RFC_FM3').focus();

			ko.applyBindings(self, $("#frmCatalogoEmpleadoCia")[0]);

			self.dialog_catalogo_empleados_cia.dialog({ position: 'center' });
		};

		self.SaveCatalogoEmpleado = function ()
		{
		    var id_empleado_cia = self.EmpleadoCia().ID_C_EMPLEADO_CIA();

		    $.ajax(rootDir + "Catalogos/GuardarEmpleadoCompania",
            {
                data: ko.toJSON({
                    empleadoCia: mapping.toJS(self.EmpleadoCia)
                }),
				type: "post",
				contentType: "application/json",
				success: function (response, status, xhr)
				{
					var ex = HandleException(response, status, xhr);
					if (ex == null)
					{
					    var empleado_cia = new CatalogoEmpleadoCia(response.result);

					    if (self.dialog_catalogo_empleados_cia != null && self.dialog_catalogo_empleados_cia != "undefined")
						{
					        self.dialog_catalogo_empleados_cia.dialog('close');
					    }

					    if (self.callback_function != null && self.callback_function != "undefined")
					    {
					        self.callback_function(empleado_cia, self.Operacion);
					    }
					}
					else
					{
						//Error
						ShowException(ex);
					}
				}
			});
		};
	};
});


