define([], function ()
{
    return function EmpleadoPemexCia(data)
    {
        var self = this;

        if (data == null || data == "undefined")
        {
            self.IdEmpleadoPemexCia = 0,
            self.TipoEmpleado = "",
            self.FichaRfc = "";
            self.NombreCompleto = "",
            self.Nombres = "",
            self.Apellidos = "",
            self.Categoria = "",
            self.Genero = "",
            self.Nacionalidad = "",
            self.RFC = "",
            self.CURP = "";
            self.FolioLibretaMar = "";
            self.VigenciaLibretaMar = "";
        }
        else
        {
            self.IdEmpleadoPemexCia = data.IdEmpleadoPemexCia,
            self.TipoEmpleado = data.TipoEmpleado,
            self.FichaRfc = data.FichaRfc;
            self.NombreCompleto = data.NombreCompleto,
            self.Nombres = data.Nombres,
            self.Apellidos = data.Apellidos,
            self.Categoria = data.Categoria,
            self.Genero = data.Genero,
            self.Nacionalidad = data.Nacionalidad,
            self.RFC = data.RFC,
            self.CURP = data.CURP;
            self.FolioLibretaMar = data.FolioLibretaMar;
            self.VigenciaLibretaMar = data.VigenciaLibretaMar;
        }
    };
});
