define(['knockout', 'parseDate'], function (ko)
{
    return function VwPermisosDireccionamientosGeneral(data)
    {
        var self = this;

        if (data == null || data == "undefined")
        {
            self.CVE_CTO_GESTOR = "";
            self.DESCRIPCION_CTO_GESTOR = "";
            self.CVE_FONDO = "";
            self.DESCRIPCION_FONDO = "";
            self.CVE_ELEMENTO_PEP = "";
            self.DESCRIPCION_ELEMENTO_PEP = "";
            self.CVE_PROGRAMA_PRESUPUESTAL = "";
            self.DESCRIPCION_PROGRAMA_PRESUPUESTAL = "";
            self.SUBDIVISION_DIRECCIONAMIENTO = "";
            self.CVE_POSICION_PRESUPUESTARIA = "";
            self.DESCRIPCION_PRESUPUESTARIA = "";
            self.INICIO = "";
            self.FIN = "";
            self.FICHA_AUTORIZADOR = "";
            self.AREA_SERVICIO_AUTORIZADOR = "";
            self.SUBDIVISION_AUTORIZADOR = "";
            self.CVE_AREA_SERVICIO = "";
            self.CVE_CTA_MAYOR = "";
            self.DESCRIPCION_CTA_MAYOR = "";
            self.CVE_CTO_COSTOS = "";
            self.NOMBRE_INSTALACION = "";
            self.ID_C_M_INSTALACION = "";
            self.SIGLAS_INSTALACION = "";
            self.PRESUPUESTERO = "";
        }
        else
        {
            self.CVE_CTO_GESTOR = data.CVE_CTO_GESTOR;
            self.DESCRIPCION_CTO_GESTOR = data.DESCRIPCION_CTO_GESTOR;
            self.CVE_FONDO = data.CVE_FONDO;
            self.DESCRIPCION_FONDO = data.DESCRIPCION_FONDO;
            self.CVE_ELEMENTO_PEP = data.CVE_ELEMENTO_PEP;
            self.DESCRIPCION_ELEMENTO_PEP = data.DESCRIPCION_ELEMENTO_PEP;
            self.CVE_PROGRAMA_PRESUPUESTAL = data.CVE_PROGRAMA_PRESUPUESTAL;
            self.DESCRIPCION_PROGRAMA_PRESUPUESTAL = data.DESCRIPCION_PROGRAMA_PRESUPUESTAL;
            self.SUBDIVISION_DIRECCIONAMIENTO = data.SUBDIVISION_DIRECCIONAMIENTO;
            self.CVE_POSICION_PRESUPUESTARIA = data.CVE_POSICION_PRESUPUESTARIA;
            self.DESCRIPCION_PRESUPUESTARIA = data.DESCRIPCION_PRESUPUESTARIA;
            self.INICIO = data.INICIO;
            self.FIN = data.FIN;
            self.FICHA_AUTORIZADOR = data.FICHA_AUTORIZADOR;
            self.AREA_SERVICIO_AUTORIZADOR = data.AREA_SERVICIO_AUTORIZADOR;
            self.SUBDIVISION_AUTORIZADOR = data.SUBDIVISION_AUTORIZADOR;
            self.CVE_AREA_SERVICIO = data.CVE_AREA_SERVICIO;
            self.CVE_CTA_MAYOR = data.CVE_CTA_MAYOR;
            self.DESCRIPCION_CTA_MAYOR = data.DESCRIPCION_CTA_MAYOR;
            self.CVE_CTO_COSTOS = data.CVE_CTO_COSTOS;
            self.NOMBRE_INSTALACION = data.NOMBRE_INSTALACION;
            self.ID_C_M_INSTALACION = data.ID_C_M_INSTALACION;
            self.SIGLAS_INSTALACION = data.SIGLAS_INSTALACION;
            self.PRESUPUESTERO = data.PRESUPUESTERO;
        }

        self.INICIO = data.INICIO;
        self.FIN = data.FIN;

        self.FECHA_INICIO_FORMATEADA = ko.computed(function () { return self.INICIO != "undefined" && self.INICIO != null ? parseDate(self.INICIO) : ""; });
        self.FECHA_FIN_FORMATEADA = ko.computed(function () { return self.FIN != "undefined" && self.FIN != null ? parseDate(self.FIN) : ""; });

        self.DIRECCIONAMIENTO_COMPLETO = ko.computed(function () { return "[" + self.CVE_CTO_GESTOR + "] " + "[" + self.CVE_FONDO + "] " + "[" + self.CVE_ELEMENTO_PEP + "] " + "[" + self.CVE_PROGRAMA_PRESUPUESTAL + "] " + "[" + self.CVE_POSICION_PRESUPUESTARIA + "]"; });
        self.VIGENCIAS = ko.computed(function () { return self.FECHA_INICIO_FORMATEADA() + " - " + self.FECHA_FIN_FORMATEADA(); });

        self.SELECCIONADO = ko.observable(false);
    };
});
