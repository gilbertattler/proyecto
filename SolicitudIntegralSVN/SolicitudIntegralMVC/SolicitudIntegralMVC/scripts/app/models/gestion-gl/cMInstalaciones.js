define(['knockout'], function (ko)
{
    return function cMInstalaciones(data)
    {
        var self = this;

        if (data == null || data == "undefined")
        {
            self.ID_C_M_INSTALACION = 0,
            self.SIGLAS_INSTALACION = "",
            self.NOMBRE_INSTALACION = ""
        }
        else
        {
            self.ID_C_M_INSTALACION = data.ID_C_M_INSTALACION,
            self.SIGLAS_INSTALACION = data.SIGLAS_INSTALACION,
            self.NOMBRE_INSTALACION = data.NOMBRE_INSTALACION
        }

        self.DESCRIPCION_INSTALACION = ko.computed(function () { return "[" + self.SIGLAS_INSTALACION + "] " + self.NOMBRE_INSTALACION; });
    };
});
