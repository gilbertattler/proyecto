define(['knockout'], function (ko)
{
    return function cEmpleadosPemex(data)
    {
        var self = this;

        if (data == null || data == "undefined")
        {
            self.ID_C_EMPLEADO_PEMEX = 0;
            self.ID_C_CONTRATO = 0;
            self.FICHA = "";
            self.RFC = "";
            self.CURP = "";
            self.NOMBRES = "";
            self.APELLIDOS = "";
            self.CATEGORIA = "";
            self.CARGO = "";
            self.GENERO = "";
            self.EXTENSION_TELEFONICA = "";
            self.EMAIL = "";
            self.BORRADO_LOGICO = false;
        }
        else
        {
            self.ID_C_EMPLEADO_PEMEX = data.ID_C_EMPLEADO_PEMEX;
            self.ID_C_CONTRATO = data.ID_C_CONTRATO;
            self.FICHA = data.FICHA;
            self.RFC = data.RFC;
            self.CURP = data.CURP;
            self.NOMBRES = data.NOMBRES;
            self.APELLIDOS = data.APELLIDOS;
            self.CATEGORIA = data.CATEGORIA;
            self.CARGO = data.CARGO;
            self.GENERO = data.GENERO;
            self.EXTENSION_TELEFONICA = data.EXTENSION_TELEFONICA;
            self.EMAIL = data.EMAIL;
            self.BORRADO_LOGICO = data.BORRADO_LOGICO;
        }
    };
});
