define(['knockout'], function (ko)
{
    return function cEmpleadosCia(data)
    {
        var self = this;

        if (data == null || data == "undefined")
        {
            self.ID_C_EMPLEADO_CIA = 0;
            self.ID_C_CONTRATO = 0;
            self.RFC_FM3 = "";
            self.NOMBRES = "";
            self.APELLIDOS = "";
            self.CATEGORIA = "";
            self.GENERO = "";
            self.NACIONALIDAD = "";
            self.CURP = "";
            self.FOLIO_LIBRETA_MAR = "";
            self.VIGENCIA_LIBRETA_MAR = "";
            self.BORRADO_LOGICO = "";
        }
        else
        {
            self.ID_C_EMPLEADO_CIA = data.ID_C_EMPLEADO_CIA;
            self.ID_C_CONTRATO = data.ID_C_CONTRATO;
            self.RFC_FM3 = data.RFC_FM3;
            self.NOMBRES = data.NOMBRES;
            self.APELLIDOS = data.APELLIDOS;
            self.CATEGORIA = data.CATEGORIA;
            self.GENERO = data.GENERO;
            self.NACIONALIDAD = data.NACIONALIDAD;
            self.CURP = data.CURP;
            self.FOLIO_LIBRETA_MAR = data.FOLIO_LIBRETA_MAR;
            self.VIGENCIA_LIBRETA_MAR = data.VIGENCIA_LIBRETA_MAR;
            self.BORRADO_LOGICO = data.BORRADO_LOGICO;
        }
    };
});
