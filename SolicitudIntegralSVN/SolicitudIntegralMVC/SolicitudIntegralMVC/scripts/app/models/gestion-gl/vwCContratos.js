define(['knockout', 'parseDate'], function (ko)
{
    return function VwCContratos(data)
    {
        var self = this;

        if (data == null || data == "undefined")
        {
            self.ID_C_CONTRATO = 0,
            self.CONTRATO = "",
            self.MONEDA = "";
            self.REGION = "",
            self.FECHA_INICIO = "",
            self.FECHA_FINAL = "";
            self.FEC_ACTUA = "";
            self.FIC_ACTUA = "";
            self.DESCRIPCION = "";
            self.BORRADO_LOGICO = "";
            self.CVE_ACREEDOR = "";
            self.DESCRIPCION_ACREEDOR = "";
            self.RFC_ACREEDOR = "";
            self.REPRESENTANTE_LEGAL = "";
            self.CORREO = "";
            self.EXT_TELEFONO = "";
        }
        else
        {
            self.ID_C_CONTRATO = data.ID_C_CONTRATO,
            self.CONTRATO = data.CONTRATO,
            self.MONEDA = data.MONEDA,
            self.REGION = data.REGION,
            self.FECHA_INICIO = data.FECHA_INICIO,
            self.FECHA_FINAL = data.FECHA_FINAL,
            self.FEC_ACTUA = data.FEC_ACTUA,
            self.FIC_ACTUA = data.FIC_ACTUA,
            self.DESCRIPCION = data.DESCRIPCION,
            self.BORRADO_LOGICO = data.BORRADO_LOGICO,
            self.CVE_ACREEDOR = data.CVE_ACREEDOR,
            self.DESCRIPCION_ACREEDOR = data.DESCRIPCION_ACREEDOR,
            self.RFC_ACREEDOR = data.RFC_ACREEDOR,
            self.REPRESENTANTE_LEGAL = data.REPRESENTANTE_LEGAL,
            self.CORREO = data.CORREO,
            self.EXT_TELEFONO = data.EXT_TELEFONO
        }

        self.FECHA_INICIO_FORMATEADA = ko.computed(function () { return self.FECHA_INICIO != "undefined" && self.FECHA_INICIO != null ? parseDate(self.FECHA_INICIO) : ""; });
        self.FECHA_FINAL_FORMATEADA = ko.computed(function () { return self.FECHA_FINAL != "undefined" && self.FECHA_FINAL != null ? parseDate(self.FECHA_FINAL) : ""; });
        self.FECHA_INICIO_FORMATEADA = ko.computed(function () { return self.FEC_ACTUA != "undefined" && self.FEC_ACTUA != null ? parseDate(self.FEC_ACTUA) : ""; });
    };
});
