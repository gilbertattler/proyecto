define(['knockout','parseDate','dateJs'], function (ko)
{
    return function ItinerarioTransporte(data)
    {
        var self = this;

        if (data == null || data == "undefined")
        {
            self.id_itinerario = 0,
            self.id_tipo_servicio = 0,
            self.fk_area_servicio = 0,
            self.fk_nodo_origen =0,
            self.fk_nodo_destino = 0,
            self.fk_nodo_atencion = 0,
            self.cve_tipo_servicio = "",
            self.des_tipo_servicio = "",
            self.cve_nodo_origen = "",
            self.des_nodo_origen = "",
            self.cve_nodo_destino = "",
            self.des_nodo_destino = "",
            self.cve_nodo_atencion = "",
            self.des_nodo_atencion = "",
            self.cve_itinerario = "",
            self.des_itinerario = "",
            self.horario_itinerario = "",
            self.horario_recepcion = "",
            self.dias_recepcion = "",
            self.horas_candado = "",
            self.fecha_capacidad = "",
            self.capacidad_total = "",
            self.capacidad_disponible = ""
        }
        else
        {
            self.id_itinerario = data.id_itinerario,
            self.id_tipo_servicio = data.id_tipo_servicio,
            self.fk_area_servicio = data.fk_area_servicio,
            self.fk_nodo_origen = data.fk_nodo_origen,
            self.fk_nodo_destino = data.fk_nodo_destino,
            self.fk_nodo_atencion = data.fk_nodo_atencion,
            self.cve_tipo_servicio = data.cve_tipo_servicio,
            self.des_tipo_servicio = data.des_tipo_servicio,
            self.cve_nodo_origen = data.cve_nodo_origen,
            self.des_nodo_origen = data.des_nodo_origen,
            self.cve_nodo_destino = data.cve_nodo_destino,
            self.des_nodo_destino = data.des_nodo_destino,
            self.cve_nodo_atencion = data.cve_nodo_atencion,
            self.des_nodo_atencion = data.des_nodo_atencion,
            self.cve_itinerario = data.cve_itinerario,
            self.des_itinerario = data.des_itinerario,
            self.horario_itinerario = data.horario_itinerario,
            self.horario_recepcion = data.horario_recepcion,
            self.dias_recepcion = data.dias_recepcion,
            self.horas_candado = data.horas_candado,
            self.fecha_capacidad = data.fecha_capacidad,
            self.capacidad_total = data.capacidad_total,
            self.capacidad_disponible = data.capacidad_disponible
        }

        self.horario_itinerario_formateado = ko.computed(function () { return self.horario_itinerario != "undefined" && self.horario_itinerario != null ? parseDate(self.horario_itinerario) : ""; });
        self.horario_recepcion_formateado = ko.computed(function () { return self.horario_recepcion != "undefined" && self.horario_recepcion != null ? parseDate(self.horario_recepcion) : ""; });
        self.fecha_capacidad_formateado = ko.computed(function () { return self.fecha_capacidad != "undefined" && self.fecha_capacidad != null ? parseDate(self.fecha_capacidad) : ""; });
        self.descripcion_larga = ko.computed(function () { return self.cve_itinerario + ' - ' + self.des_itinerario + ' - [' + self.cve_nodo_origen + ' - ' + self.cve_nodo_destino + '] ' + Date.parse(parseDateTime(self.horario_itinerario)).toString('HH:mm') + ' hrs'; });
    }
});
