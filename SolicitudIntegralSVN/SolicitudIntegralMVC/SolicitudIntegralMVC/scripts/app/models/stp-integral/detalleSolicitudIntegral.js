define(['knockout', 'knockout.mapping', 'parseDate'], function (ko, mapping)
{
    return function DetalleSolicitudIntegral(data)
    {
        var self = this;

        if (data == null || data == "undefined")
        {
            self.IdDetalleSolicitudIntegral = 0;
            self.IdSolicitudIntegral = 0;
            self.IdInstalacionOrigen = 0;
            self.IdInstalacionDestino = 0;
            self.IdPasajeroCiaPEMEX = 0;
            self.ClaveInstalacionOrigen = "";
            self.ClaveInstalacionDestino = "";
            self.TipoMovimientoPersonal = "";
            self.NombreInstalacionOrigen = "";
            self.NombreInstalacionDestino = "";
            self.FichaRFCEmpleado = "";
            self.NombreEmpleado = "";
            self.FolioLibretaMar = "";
            self.VigenciaLibretaMar = "";
            self.EstadoOcupanteSolicitudIntegral = "";
            self.EstadoHuespedAH = "";
            self.EstadoPasajeroSTP = "";
            self.HoraViajeItemParajeroSTA = "";
            self.FechaSubida = "";
            self.FechaBajada = "";
            self.TipoPersonal = "";
            self.fk_contrato = 0;
            self.fk_itinerario = 0;
            self.FondoDireccionamientoAH = "";
            self.CentroGestorDireccionamientoAH = "";
            self.ElementoPEPDireccionamiento = "";
            self.CuentaMayorDireccionamientoAH = "";
            self.ProgramaPresupuestalDireccionamientoAH = "";
            self.PosicionPresupuestalDireccionamientoAH = "";
            self.FondoDireccionamientoSTP = "";
            self.CentroGestorDireccionamientoSTP = "";
            self.ElementoPEPDireccionamientoSTP = "";
            self.CuentaMayorDireccionamientoSTP = "";
            self.ProgramaPresupuestalDireccionamientoSTP = "";
            self.PosicionPresupuestalDireccionamientoSTP = "";

            self.SeleccionadoReprogramacion = false;
            self.ElementoSeleccionado = false;
            self.DisponibleReprogramacion = false;

            self.NumeroVuelo = "";
            self.MatriculaAeronave = "";
        }
        else
        {
            self.IdDetalleSolicitudIntegral = data.IdDetalleSolicitudIntegral;
            self.IdSolicitudIntegral = data.IdSolicitudIntegral;
            self.IdInstalacionOrigen = data.IdInstalacionOrigen;
            self.IdInstalacionDestino = data.IdInstalacionDestino;
            self.IdPasajeroCiaPEMEX = data.IdPasajeroCiaPEMEX;
            self.ClaveInstalacionOrigen = data.ClaveInstalacionOrigen;
            self.ClaveInstalacionDestino = data.ClaveInstalacionDestino;
            self.TipoMovimientoPersonal = data.TipoMovimientoPersonal;
            self.NombreInstalacionOrigen = data.NombreInstalacionOrigen;
            self.NombreInstalacionDestino = data.NombreInstalacionDestino;
            self.FichaRFCEmpleado = data.FichaRFCEmpleado;
            self.NombreEmpleado = data.NombreEmpleado;
            self.FolioLibretaMar = data.FolioLibretaMar;
            self.VigenciaLibretaMar = data.VigenciaLibretaMar;
            self.EstadoOcupanteSolicitudIntegral = data.EstadoOcupanteSolicitudIntegral;
            self.EstadoHuespedAH = data.EstadoHuespedAH;
            self.EstadoPasajeroSTP = data.EstadoPasajeroSTP;
            self.HoraViajeItemParajeroSTA = parseDate(data.HoraViajeItemParajeroSTA);
            self.FechaSubida = parseDate(data.FechaSubida);
            self.FechaBajada = parseDate(data.FechaBajada);
            self.TipoPersonal = data.TipoPersonal;
            self.fk_contrato = data.fk_contrato;
            self.fk_itinerario = data.fk_itinerario;
            self.FondoDireccionamientoAH = data.FondoDireccionamientoAH;
            self.CentroGestorDireccionamientoAH = data.CentroGestorDireccionamientoAH;
            self.ElementoPEPDireccionamiento = data.ElementoPEPDireccionamiento;
            self.CuentaMayorDireccionamientoAH = data.CuentaMayorDireccionamientoAH;
            self.ProgramaPresupuestalDireccionamientoAH = data.ProgramaPresupuestalDireccionamientoAH;
            self.PosicionPresupuestalDireccionamientoAH = data.PosicionPresupuestalDireccionamientoAH;
            self.FondoDireccionamientoSTP = data.FondoDireccionamientoSTP;
            self.CentroGestorDireccionamientoSTP = data.CentroGestorDireccionamientoSTP;
            self.ElementoPEPDireccionamientoSTP = data.ElementoPEPDireccionamientoSTP;
            self.CuentaMayorDireccionamientoSTP = data.CuentaMayorDireccionamientoSTP;
            self.ProgramaPresupuestalDireccionamientoSTP = data.ProgramaPresupuestalDireccionamientoSTP;
            self.PosicionPresupuestalDireccionamientoSTP = data.PosicionPresupuestalDireccionamientoSTP;

            self.SeleccionadoReprogramacion = data.Seleccionado != null && data.Seleccionado != "undefined" ? data.Seleccionado : false;
            self.ElementoSeleccionado = data.ElementoSeleccionado != null && data.ElementoSeleccionado != "undefined" ? data.ElementoSeleccionado : false;
            self.DisponibleReprogramacion = data.DisponibleReprogramacion != null && data.DisponibleReprogramacion != "undefined" ? data.DisponibleReprogramacion : false;

            self.NumeroVuelo = data.NumeroVuelo;
            self.MatriculaAeronave = data.MatriculaAeronave;
        }

        self.FechaSubidaFormateada = ko.computed(function () { return self.FechaSubida != "undefined" && self.FechaSubida != null ? parseDate(mapping.toJS(self.FechaSubida)) : ""; });
        self.FechaBajadaFormateada = ko.computed(function () { return self.FechaBajada != "undefined" && self.FechaBajada != null ? parseDate(mapping.toJS(self.FechaBajada)) : ""; });
        self.CveOrigenDestino = ko.computed(function ()
        {
            var resultado = "";
            if (typeof self.ClaveInstalacionOrigen == "function")
            {
                resultado = self.ClaveInstalacionOrigen() + ' - ' + self.ClaveInstalacionDestino();
            }
            else
            {
                resultado = self.ClaveInstalacionOrigen + ' - ' + self.ClaveInstalacionDestino;
            }
            return resultado;

        });
    };
});
