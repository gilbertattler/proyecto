
define(['knockout', 'knockout.mapping', 'parseDate'], function (ko, mapping)
{
    return function CapacidadesReprogramacion(data)
    {
        var self = this;

        if (data == null || data == "undefined")
        {
            self.FkIdInstalacionOrigen = 0;
            self.FkIdInstalacionDestino = 0;
            self.NombreInstalacionOrigen = "";
            self.NombreInstalacionDestino = "";
            self.SiglasInstalacionOrigen = "";
            self.SiglasInstalacionDestino = "";
            self.FechaSubidaOriginal = parseDate($.now());
            self.FechaBajadaOriginal = parseDate($.now());
            self.FechaSubidaNueva = parseDate($.now());
            self.FechaBajadaNueva = parseDate($.now());
            self.CapacidadRequeridaTransporte = 0;
            self.CapacidadRequeridaHospedaje = 0;
            self.CapacidadDisponibleTransporte = 0;
            self.CapacidadDisponibleHospedaje = 0;
            self.FkIdItinerarioTransporte = 0;
            self.NombreItinerario = "";
        }
        else
        {
            self.FkIdInstalacionOrigen = data.FkIdInstalacionOrigen;
            self.FkIdInstalacionDestino = data.FkIdInstalacionDestino;
            self.NombreInstalacionOrigen = data.NombreInstalacionOrigen;
            self.NombreInstalacionDestino = data.NombreInstalacionDestino;
            self.SiglasInstalacionOrigen = data.SiglasInstalacionOrigen;
            self.SiglasInstalacionDestino = data.SiglasInstalacionDestino;
            self.FechaSubidaOriginal = data.FechaSubidaOriginal;
            self.FechaBajadaOriginal = data.FechaBajadaOriginal;
            self.FechaSubidaNueva = data.FechaSubidaNueva;
            self.FechaBajadaNueva = data.FechaBajadaNueva;
            self.CapacidadRequeridaTransporte = data.CapacidadRequeridaTransporte;
            self.CapacidadRequeridaHospedaje = data.CapacidadRequeridaHospedaje;
            self.CapacidadDisponibleTransporte = data.CapacidadDisponibleTransporte;
            self.CapacidadDisponibleHospedaje = data.CapacidadDisponibleHospedaje;
            self.FkIdItinerarioTransporte = data.FkIdItinerarioTransporte;
            self.NombreItinerario = data.NombreItinerario;
        }

        self.FechaSubidaOriginalFormateada = ko.computed(function () { return self.FechaSubidaOriginal != "undefined" && self.FechaSubidaOriginal != null ? parseDate(self.FechaSubidaOriginal) : ""; });
        self.FechaBajadaOriginalFormateada = ko.computed(function () { return self.FechaBajadaOriginal != "undefined" && self.FechaBajadaOriginal != null ? parseDate(self.FechaBajadaOriginal) : ""; });
        self.FechaSubidaNuevaFormateada = ko.computed(function () { return self.FechaSubidaNueva != "undefined" && self.FechaSubidaNueva != null ? parseDate(self.FechaSubidaNueva) : ""; });
        self.FechaBajadaNuevaFormateada = ko.computed(function () { return self.FechaBajadaNueva != "undefined" && self.FechaBajadaNueva != null ? parseDate(self.FechaBajadaNueva) : ""; });

        self.CantidadTotalPasajerosProgramar = ko.computed(function ()
        {
            var resultado = 0;
            if (self.CapacidadDisponibleTransporte >= self.CapacidadRequeridaTransporte && self.CapacidadDisponibleHospedaje >= self.CapacidadRequeridaHospedaje)
            {
                //success
                if (self.CapacidadRequeridaTransporte >= self.CapacidadRequeridaHospedaje)
                {
                    resultado = self.CapacidadRequeridaTransporte;
                }
                else
                {
                    resultado = self.CapacidadRequeridaHospedaje;
                }
            }
            else
            {

                if ((self.CapacidadRequeridaTransporte > 0 && self.CapacidadDisponibleTransporte <= 0) || (self.CapacidadRequeridaHospedaje >= 0 && self.CapacidadDisponibleHospedaje <= 0))
                {
                    //ERROR
                    resultado = 0;
                }
                else
                {
                    //WARNING
                    if (self.CapacidadDisponibleTransporte >= self.CapacidadDisponibleHospedaje)
                    {
                        resultado = self.CapacidadDisponibleHospedaje > 0 ? self.CapacidadDisponibleHospedaje : self.CapacidadDisponibleTransporte;
                    }
                    else
                    {
                        resultado = self.CapacidadDisponibleTransporte > 0 ? self.CapacidadDisponibleTransporte : self.CapacidadDisponibleHospedaje;
                    }
                }
            }

            return resultado;
        });

        self.CapacidadRestantePasajerosPorProgramar = self.CantidadTotalPasajerosProgramar();


    };
});
