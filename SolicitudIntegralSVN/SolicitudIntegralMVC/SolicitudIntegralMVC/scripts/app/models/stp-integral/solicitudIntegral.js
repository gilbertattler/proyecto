define(['knockout', 'knockout.mapping', 'parseDate'], function (ko, mapping)
{
    return function SolicitudIntegral(data)
    {
        var self = this;

        if (data == null || data == "undefined")
        {
            self.IdSolicitudIntegral = 0;
            self.FolioSolicitudIntegral = "";
            self.FkIdItinerarioTransporteIda = 0;
            self.FkIdItinerarioTransporteRetorno = 0;
            self.TipoSolicitudIntegral = "";
            self.TipoPersonal = "";
            self.TipoTransporte = "";
            self.TipoServicio = "";
            self.FechaCreacion = parseDate($.now());
            self.FechaSubida = parseDate($.now());
            self.FechaBajada = parseDate($.now());
            self.FichaSolicitante = "";
            self.NombreSolicitante = "";
            self.FichaAutorizador = "";
            self.NombreAutorizador = ""
            self.NumeroContrato = "4288118280";
            self.NombreAcreedor = "";
            self.RFCAcreedor = "";
            self.HoraSalidaItinerarioTransporte = "";
            self.FKidOrigenSolicitud = 0;
            self.FKidDestinoSolicitud = 0;
            self.SubdivisionAutorizador = "";
            self.SubdivisionSolicitante = "";
            self.RequiereRetorno = "";

            self.DescripcionSubdivisionAutorizador = "";
            self.DescripcionSubdivisionSolicitante = "";

            self.FolioEspecialHospedaje = "";
            self.FolioEspecialTransporte = "";
            self.EstadoSolicitudIntegral = "SOLICITADO";

            self.NodoAtencion = "";

            //Esto solo aplica para la opcion de diferir.
            self.FkIdItinerarioTransporteIdaDiferir = 0;
            self.FkIdItinerarioTransporteRetornoDiferir = 0;
            self.FichaAutorizadorDiferir = "";
            self.NombreAutorizadorDiferir = "";
            self.DescripcionSubdivisionAutorizadorDiferir = "";
            self.SubdivisionAutorizadorDiferir = "";

            //Esto aplica para el listado de solicitudes
            self.CveItinerarioIda = "";
            self.DesItinerarioIda = "";
            self.CveItinerarioRetorno = "";
            self.DesItinerarioRetorno = "";
            self.DesTipoServicioItinerarioIda = "";
            self.DesTipoServicioItinerarioRetorno = "";
        }
        else
        {
            self.IdSolicitudIntegral = data.IdSolicitudIntegral;
            self.FolioSolicitudIntegral = data.FolioSolicitudIntegral;
            self.FkIdItinerarioTransporteIda = data.FkIdItinerarioTransporteIda,
            self.FkIdItinerarioTransporteRetorno = data.FkIdItinerarioTransporteRetorno,
            self.TipoSolicitudIntegral = data.TipoSolicitudIntegral;
            self.TipoPersonal = data.TipoPersonal;
            self.TipoTransporte = data.TipoTransporte;
            self.TipoServicio = data.TipoServicio;
            self.FechaCreacion = parseDate(data.FechaCreacion);
            self.FechaSubida = parseDate(data.FechaSubida);
            self.FechaBajada = parseDate(data.FechaBajada);
            self.FichaSolicitante = data.FichaSolicitante;
            self.NombreSolicitante = data.NombreSolicitante;
            self.FichaAutorizador = data.FichaAutorizador;
            self.NombreAutorizador = data.NombreAutorizador;
            self.NumeroContrato = data.NumeroContrato;
            self.NombreAcreedor = data.NombreAcreedor;
            self.RFCAcreedor = data.RFCAcreedor;
            self.HoraSalidaItinerarioTransporte = data.HoraSalidaItinerarioTransporte;
            self.FKidOrigenSolicitud = data.FKidOrigenSolicitud;
            self.FKidDestinoSolicitud = data.FKidDestinoSolicitud;
            self.SubdivisionAutorizador = data.SubdivisionAutorizador;
            self.SubdivisionSolicitante = data.SubdivisionSolicitante;
            self.RequiereRetorno = data.RequiereRetorno;

            self.DescripcionSubdivisionAutorizador = data.DescripcionSubdivisionAutorizador;
            self.DescripcionSubdivisionSolicitante = data.DescripcionSubdivisionSolicitante;

            self.FolioEspecialHospedaje = data.FolioEspecialHospedaje;
            self.FolioEspecialTransporte = data.FolioEspecialTransporte;
            self.EstadoSolicitudIntegral = data.EstadoSolicitudIntegral;

            self.NodoAtencion = data.NodoAtencion;

            //Esto solo aplica para la opcion de diferir.
            self.FkIdItinerarioTransporteIdaDiferir = 0;
            self.FkIdItinerarioTransporteRetornoDiferir = 0;
            self.FichaAutorizadorDiferir = "";
            self.NombreAutorizadorDiferir = "";
            self.DescripcionSubdivisionAutorizadorDiferir = "";
            self.SubdivisionAutorizadorDiferir = "";

            //Esto aplica para el listado de solicitudes
            self.CveItinerarioIda = data.CveItinerarioIda;
            self.DesItinerarioIda = data.DesItinerarioIda;
            self.CveItinerarioRetorno = data.CveItinerarioRetorno;
            self.DesItinerarioRetorno = data.DesItinerarioRetorno;
            self.DesTipoServicioItinerarioIda = data.DesTipoServicioItinerarioIda;
            self.DesTipoServicioItinerarioRetorno = data.DesTipoServicioItinerarioRetorno;
        }

        self.FechaCreacionFormateada = ko.computed(function () { return self.FechaCreacion != "undefined" && self.FechaCreacion != null ? parseDate(mapping.toJS(self.FechaCreacion)) : ""; });
        self.FechaSubidaFormateada = ko.computed(function () { return self.FechaSubida != "undefined" && self.FechaSubida != null ? parseDate(mapping.toJS(self.FechaSubida)) : ""; });
        self.FechaBajadaFormateada = ko.computed(function () { return self.FechaBajada != "undefined" && self.FechaBajada != null ? parseDate(mapping.toJS(self.FechaBajada)) : ""; });

        self.TipoPersonalFormateado = ko.computed(function () { return mapping.toJS(self.TipoPersonal) == 'PEMEX' ? 'PMX' : (mapping.toJS(self.TipoPersonal) != 'COMODATARIO' ? 'CIA' : 'COM'); });

        self.EditLink = ko.computed(function () { return rootDir + "SolicitudIntegral/Editar/" + mapping.toJS(self.FolioSolicitudIntegral); });
        
        self.ImprimirSolicitudLink = ko.computed(function () { return rootDir + "SolicitudIntegral/Imprimir/" + mapping.toJS(self.FolioSolicitudIntegral); });
        self.ImprimirBoletosSolicitudLink = ko.computed(function () { return rootDir + "SolicitudIntegral/ImpimirReporteBoletos/" + mapping.toJS(self.FolioSolicitudIntegral); });

        self.ReprogramarLink = ko.computed(function () { return rootDir + "SolicitudIntegral/Reprogramar/" + mapping.toJS(self.FolioSolicitudIntegral); });
        self.DiferirLink = ko.computed(function () { return rootDir + "SolicitudIntegral/Diferir/" + mapping.toJS(self.FolioSolicitudIntegral); });
        self.FirmaElectronicaLink = ko.computed(function () { return rootDir + "FirmaElectronica/Firmar/" + mapping.toJS(self.FolioSolicitudIntegral); });

        self.Seleccionado = false;
    };
});