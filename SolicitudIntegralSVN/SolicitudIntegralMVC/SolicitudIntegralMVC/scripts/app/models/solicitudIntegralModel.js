define(['knockout', 'app/models/stp-integral/SolicitudIntegral', 'app/models/stp-integral/detalleSolicitudIntegral'], function (ko, SolicitudIntegral, DetalleSolicitudIntegral)
{
    return function SolicitudIntegralModel(data)
    {
        var self = this;

        if (data == null || data == "undefined")
        {
            self.SolicitudIntegral = null;
            self.DetallesSolicitudIntegral = null;
        }
        else
        {
            self.SolicitudIntegral = data.SolicitudIntegral;
            self.DetallesSolicitudIntegral = data.DetallesSolicitudIntegral;
        }
    };
});
