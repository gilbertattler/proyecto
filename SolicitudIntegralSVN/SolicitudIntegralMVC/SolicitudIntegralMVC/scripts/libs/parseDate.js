﻿//Esta funcion sirve para formatear fechas en cadenas json del tipo /DATE(123456749800)/ al formato de fecha del sistema.
function parseDate(StrDate)
{
    var resultado = StrDate;

    if (typeof StrDate == "string")
    {
        if (StrDate.substring(0, 6).toUpperCase() == "/DATE(")
        {
            resultado = new Date(parseInt(StrDate.substr(6)));
        }
        else
        {
            resultado = moment(StrDate, "DD/MM/YYYY");
        }
    }

    if (resultado != null && resultado != "undefined" && resultado != "")
    {
        resultado = moment(resultado);

        if (resultado != null && resultado != "undefined")
        {
            resultado = moment(resultado).format("DD/MM/YYYY");
        }
    }

    return resultado;
}

function parseDateTime(StrDate)
{
    var resultado = StrDate;

    if (typeof StrDate == "string")
    {
        if (StrDate.substring(0, 6).toUpperCase() == "/DATE(")
        {
            resultado = new Date(parseInt(StrDate.substr(6)));
        }
        else
        {
            resultado = moment(StrDate, "DD/MM/YYYY hh:mm");
        }
    }

    if (resultado != null && resultado != "undefined" && resultado != "")
    {
        resultado = moment(resultado);

        if (resultado != null && resultado != "undefined")
        {
            resultado = moment(resultado).format("DD/MM/YYYY hh:mm");
        }
    }

    return resultado;
}