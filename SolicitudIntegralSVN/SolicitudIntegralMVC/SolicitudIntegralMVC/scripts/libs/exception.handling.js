﻿//////////////////////////
//Exception handling
//////////////////////////

define(['libs/json2'], function ()
{
});

ExceptionBase = function (type, message)
{
    this.Type = type;
    this.Message = message;
};

ProgramException = function (message, netException)
{

    this.Reasons = [];
    this.NetException = netException;

    if (typeof message != "undefined" && message != null)
        this.Message = message;
    else
        this.Message = "";

    if (typeof netException.TMessage != "undefined" && netException.TMessage != null)
        this.TMessage = netException.TMessage;
    else
        this.TMessage = "error";
};
ProgramException.prototype = new ExceptionBase("ProgramException", "Operation Error");

FatalException = function (message, netException)
{
    this.NetException = netException;
    if (typeof message != "undefined" && message != null)
        this.Message = message;
}

FatalException.prototype = new ExceptionBase("FatalException", "Unexpected Error");

ShowException = function (exception)
{
    //non-fatal exceptions
    if (exception.Type == "ProgramException")
    {
        var buttons = {};

        var razones = '';

        if (exception.Reasons != "undefined" && exception.Reasons != null && exception.Reasons.length > 0)
        {
            razones = '<ul>';
            $.each(exception.Reasons, function (index, value)
            {
                razones.concat('<li>' + value + '</li>');
            });
            razones.concat('</ul>');
        }

        alert(exception.Message + ' ' + razones);
    }
        //fatal exceptions.
    else if (exception.Type == "FatalException")
    {
        alert(exception.Message + ' ' + razones);
    }
}

function HandleException(response, status, xhr)
{
    var responseStatus = xhr.getResponseHeader("RESPONSE_STATUS");
    var ex = null;
    if (responseStatus == "ApplicationException")
    {

        try
        {
            var netEx = JSON.parse(response);
            ex = new ProgramException(netEx.Message, netEx);
            ex.Reasons = netEx.Reasons;
        }
        catch(e)
        {
            ex = new ProgramException(response.Message, response);
            ex.Reasons = response.Reasons;
        }
        
    }
    else if (responseStatus == "UnexpectedException")
    {

        try
        {
            var netEx = JSON.parse(response);
            ex = new FatalException(netEx.Message, netEx);
        }
        catch (e)
        {
            ex = new FatalException(response.Message, response);
        }
        
    }

    return ex;
}