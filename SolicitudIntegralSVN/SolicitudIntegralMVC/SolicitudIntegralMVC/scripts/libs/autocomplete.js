﻿function Autocomplete(textboxid, hiddenid, url_consulta, value_field, text_field, callback_select_function)
{
    $("#" + textboxid).autocomplete({
        source: function (request, response)
        {
            $.ajax({
                url: url_consulta,
                dataType: "json",
                data:
                {
                    busqueda: request.term
                },
                success: function (data)
                {
                    response($.map(data.result, function (item)
                    {
                        return {
                            label: item[text_field],
                            value: item[text_field],
                            object: item
                        };
                    }));
                }
            });
        },
        minLength: 2,
        select: function (event, ui)
        {
            $("#" + hiddenid).val(ui.item.object[value_field]);

            if (!(typeof callback_select_function == 'undefined' || callback_select_function == null))
            {
                callback_select_function(ui.item.object);
            }
        },
        open: function ()
        {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function ()
        {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
    });
}