﻿var lastAjaxCall = { settings: null, jqXHR: null };

$(function ()
{
    $.ajaxSetup({
        error: function (jqXHR, settings, thrownError)
        {
            HandlingJqueryAjaxError(jqXHR, settings, thrownError);
        }
    });
});

function HandlingJqueryAjaxError(jqXHR, settings, thrownError)
{
    if (jqXHR.status === 0)
    {
        alert('Not connect.\n Verify Network.');
    }
    else if (jqXHR.status == 401)
    {
        loginUrl = rootDir + "Account/LogOn"
        if (loginUrl)
        {
            $('body').empty();
            $('body').append('<h1>El usuario no esta logueado</h1> <a href="' + loginUrl + '" >Clic para ir a la página de Login</a>');
            lastAjaxCall.jqXHR = jqXHR;
            lastAjaxCall.settings = settings;
        }
    }
    else if (jqXHR.status == 404)
    {
        alert('Requested page not found. [404]');
    } else if (jqXHR.status == 500)
    {
        alert('Internal Server Error [500].');
    } else if (exception === 'parsererror')
    {
        alert('Requested JSON parse failed.');
    } else if (exception === 'timeout')
    {
        alert('Time out error.');
    } else if (exception === 'abort')
    {
        alert('Ajax request aborted.');
    } else
    {
        alert('Uncaught Error.\n' + jqXHR.responseText);
    }
}