requirejs.config(
{
    paths: {
        'jquery.ui': 'libs/jquery-ui-1.9.0',
        'jquery.serialize.object': 'libs/jquery.serialize-object',
        'jquery.tagit': 'libs/jquery.tagit',
        'knockout': 'libs/knockout-2.2.0',
        'knockout.mapping': 'libs/knockout.mapping-latest',
        'linq': 'libs/linq.min',
        'dateJs': 'libs/date-es-MX',
        'moment': 'libs/moment',
        'jquery.to.dictionary': 'libs/jquery.to.dictionary',
        'autoacomplete': 'libs/autocomplete',
        'parseDate': 'libs/parseDate',
        'nicedit': 'libs/nicEdit',
        'jquery.ajax.setup': 'libs/jquery.ajax-setup',
        'exception.handling': 'libs/exception.handling',
        'metro.style.accordion': 'libs/metro-style/accordion',
        'metro.style.buttonset': 'libs/metro-style/buttonset',
        'metro.style.carousel': 'libs/metro-style/carousel',
        'metro.style.dropdown': 'libs/metro-style/dropdown',
        'metro.style.input-control': 'libs/metro-style/input-control',
        'metro.style.pagecontrol': 'libs/metro-style/pagecontrol',
        'metro.style.rating': 'libs/metro-style/rating',
        'metro.style.slider': 'libs/metro-style/slider',
        'metro.style.tile-drag': 'libs/metro-style/title-drag',
        'metro.style.tile-slider': 'libs/metro-style/tile-slider',
    },
    shim:
    {
        'jquery.ui':
        {
            deps: ['jquery']
        },
        'jquery.serialize.object':
        {
            deps: ['jquery']
        },
        'jquery.linq':
        {
            deps: ['jquery', 'linq']
        },
        'jquery.to.dictionary':
        {
            deps: ['jquery']
        },
        'jquery.tagit':
        {
            deps: ['jquery', 'jquery.ui']
        },
        'knockout.mapping':
        {
            deps: ['knockout']
        },
        'autoacomplete':
        {
            deps: ['jquery', 'jquery.ui']
        },
        'parseDate':
        {
            deps: ['moment']
        },
        'nicedit':
        {
            deps: ['jquery']
        }
    }
});

require(["metro.style.dropdown","domReady!"], function ($)
{
    
});
