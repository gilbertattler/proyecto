﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using SolicitudIntegralMVC.Infraestructure;
using SolicitudIntegralMVC.Infraestructure.Concrete;
using STPIntegral.Negocios;
using STPIntegral.Negocios.Models;

namespace SolicitudIntegralMVC.Controllers
{
    [Authorize]
    [AjaxAuthorizationAttribute]
    [CustomAuthSessionRoleAttribute("VENTANILLA", "USUARIO")]
    [System.Web.Mvc.OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class SolicitudIntegralController : Controller
    {
        //
        // GET: /SolicitudIntegral/

        [HttpGet]
        public ActionResult Listado()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GridOpcionesListadoSolicitudes()
        {
            return PartialView();
        }

        [HttpGet]
        public ActionResult VistaConsultaInstalacionesDestinoAutorizador()
        {
            return PartialView();
        }

        [HttpGet]
        public ActionResult ListaDestinosItinerario()
        {
            return PartialView();
        }

        [HttpGet]
        public ActionResult BusquedaItinerariosxIntalacion()
        {
            return PartialView();
        }

        [HttpGet]
        public ActionResult BuscarItinerarioxIntalacion(string fraseBusqueda)
        {
            ActionResult resultado = null;

            try
            {
                List<STPIntegral.Datos.VW_stp_c_itinerarios_destinos> resul_consul = null;

                using (STPIntegral.Datos.STPIntegralModel stpIntegralModel = new STPIntegral.Datos.STPIntegralModel())
                {
                    resul_consul = stpIntegralModel.VW_stp_c_itinerarios_destinos.Where(x => x.NOMBRE_INSTALACION.Contains(fraseBusqueda) || x.SIGLAS_INSTALACION.Contains(fraseBusqueda)).Distinct().ToList();
                }

                if (Request.IsAjaxRequest())
                {
                    resultado = Json(new { result = resul_consul }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }

            return resultado;
        }

        [HttpGet]
        public ActionResult ListarDestinosItinerario(int id_itinerario) 
        {
            ActionResult resultado = null;

            try
            {
                List<STPIntegral.Datos.VW_stp_c_itinerarios_destinos> resul_consul = null;

                using (STPIntegral.Datos.STPIntegralModel stpIntegralModel = new STPIntegral.Datos.STPIntegralModel())
                {
                    resul_consul = stpIntegralModel.VW_stp_c_itinerarios_destinos.Where(x => x.id_itinerario == id_itinerario).ToList();
                }

                if (Request.IsAjaxRequest())
                {
                    resultado = Json(new { result = resul_consul }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }

            return resultado;
        }

        [HttpGet]
        public ActionResult ConsultaInstalacionesDestinoAutorizador( string ficha_autorizador, DateTime fecha_subida, DateTime fecha_bajada)
        {
            ActionResult resultado = null;

            try
            {
                List<STPIntegral.Datos.SI_LISTADO_INSTALACIONES_AUTORIZADOR_Result> resul_consul = null;

                using (STPIntegral.Datos.STPIntegralModel stpIntegralModel = new STPIntegral.Datos.STPIntegralModel())
                {
                   
                    resul_consul = stpIntegralModel.SI_LISTADO_INSTALACIONES_AUTORIZADOR(ficha_autorizador,fecha_subida,fecha_bajada).ToList();
                }

                if (Request.IsAjaxRequest())
                {
                    resultado = Json(new { result = resul_consul }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }

            return resultado;

        }

        [HttpGet]
        public ActionResult VistaDetallesEmpleado(int id_detalle_empleado)
        {
            ActionResult resultado = null;

            STPIntegral.Datos.VW_TabDetallesSolicitudIntegral model = new STPIntegral.Datos.VW_TabDetallesSolicitudIntegral();

            try
            {
                using (STPIntegral.Datos.STPIntegralModel stpIntegralModel = new STPIntegral.Datos.STPIntegralModel())
                {
                    model = stpIntegralModel.VW_TabDetallesSolicitudIntegral.SingleOrDefault(x => x.IdDetalleSolicitudIntegral == id_detalle_empleado);
                }

                resultado = PartialView(model);
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            return resultado;
        }
    
        [HttpGet]
        public ActionResult VistaItinerariosPorDia(string tipo_solicitud_integral, string tipo_transporte, string tipo_servicio, DateTime fecha_servicio, string Subdivision, string Contrato, string folio_especial_ida, string folio_especial_regreso)
        {
            ActionResult resultado = null;

            Models.ItinerariosDiaViewModel model = new Models.ItinerariosDiaViewModel();

            model.FechaConsulta = fecha_servicio;
            model.TipoServicio = tipo_servicio;
            model.TipoTransporte = tipo_transporte;
            model.TipoSolicitudIntegral = tipo_solicitud_integral;
            
            try
            {
                SolicitudIntegralMVC.Models.UserLogon usuario_logueado = SolicitudIntegralMVC.Infraestructure.Utilerias.ObtenerUsuarioAutentificado(this.HttpContext);

                model.NodoAtencion = usuario_logueado.NodoAtencionActual;

                //TODO: Aplciar paso de parametros de contrato y subdivision
                IEnumerable<STPIntegral.Datos.Vw_SoltIntItinerariosCapacidadDia_NOLOCK> itinerarios_disponibles = ItinerariosTransporte.ConsultarItinerarios(tipo_solicitud_integral, tipo_transporte, tipo_servicio, fecha_servicio, usuario_logueado.NodoAtencionActual, folio_especial_ida, folio_especial_regreso, Contrato, Subdivision, false);

                model.ItinerariosCapacidadDia = itinerarios_disponibles;
                
                resultado = PartialView(model);
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            return resultado;
        }

        [HttpGet]
        public ActionResult VistaItinerarioCapacidadDetallePorDia(string tipo_transporte, int id_itinerario, DateTime fecha_servicio, string subdivision_autorizador = "", string contrato = "", string folio_especial = "")
        {
            ActionResult resultado = null;

            Models.CapacidadItinerariosDiaViewModel model = new Models.CapacidadItinerariosDiaViewModel();

            if (subdivision_autorizador == "-")
            {
                subdivision_autorizador = "";
            }

            if (contrato == "-")
            {
                contrato = "";
            }

            if (folio_especial == "-")
            {
                folio_especial = "";
            }
            
            model.FechaConsulta = fecha_servicio;
            model.TipoTransporte = tipo_transporte;
                        
            model.FolioEspecial = folio_especial;
            model.NoContrato = contrato;

            try
            {
                SolicitudIntegralMVC.Models.UserLogon usuario_logueado = SolicitudIntegralMVC.Infraestructure.Utilerias.ObtenerUsuarioAutentificado(this.HttpContext);

                model.NodoAtencion = usuario_logueado.NodoAtencionActual;

                int capacidad_fondo_comun = 0;

                STPIntegral.Datos.stp_c_itinerario_base itinerario = null;
                using (STPIntegral.Datos.STPIntegralModel StpIntegralModel = new STPIntegral.Datos.STPIntegralModel())
                {
                    itinerario = StpIntegralModel.stp_c_itinerario_base.SingleOrDefault(x => x.id_itinerario == id_itinerario);
                }

                GestionGL.Datos.C_SUBDIVISIONES subdivision = null;
                using (GestionGL.Datos.GestionGLModel GestionGlModel = new GestionGL.Datos.GestionGLModel())
                {
                    subdivision = GestionGlModel.C_SUBDIVISIONES.FirstOrDefault(x => x.SUBDIVISION == subdivision_autorizador);
                }

                model.NombreItinerario = itinerario.des_itinerario;
                model.CveItinerario = itinerario.cve_itinerario;

                model.SubdivisionAutorizador = subdivision_autorizador;
                model.DescripcionSubdivision = subdivision.DESCRIPCION_SUBDIVISION;

                IEnumerable<STPIntegral.Datos.Vw_SoltIntItinerariosCapacidadDia_NOLOCK> itinerarios_disponibles_detalles = ItinerariosTransporte.ConsultaCapacidadDetallesItinerarioPorDia(tipo_transporte, usuario_logueado.NodoAtencionActual, id_itinerario, fecha_servicio, subdivision_autorizador, contrato, folio_especial, out capacidad_fondo_comun);

                model.CantidadDisponibleFondoComun = capacidad_fondo_comun;

                model.ProcesarDetallesItinerariosEncontrados(itinerarios_disponibles_detalles);

                resultado = PartialView(model);
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            return resultado;
        }

        [HttpGet]
        public ActionResult VistaCapacidadHospedajePorRangoFechas(int fk_id_instalacion_destino, DateTime fecha_subida, DateTime fecha_bajada, string subdivision_autorizador = "", string contrato = "", string folio_especial = "")
        {
            ActionResult resultado = null;

            Models.CapacidadHospedajeViewModel model = new Models.CapacidadHospedajeViewModel();

            if (subdivision_autorizador == "-")
            {
                subdivision_autorizador = "";
            }

            if (contrato == "-")
            {
                contrato = "";
            }

            if (folio_especial == "-")
            {
                folio_especial = "";
            }

            model.FechaSubida = fecha_subida;
            model.FechaBajada = fecha_bajada;

            model.FolioEspecial = folio_especial;
            model.NoContrato = contrato;
            model.SubdivisionAutorizador = subdivision_autorizador;

            try
            {
                SolicitudIntegralMVC.Models.UserLogon usuario_logueado = SolicitudIntegralMVC.Infraestructure.Utilerias.ObtenerUsuarioAutentificado(this.HttpContext);

                GestionGL.Datos.C_SUBDIVISIONES subdivision = null;
                GestionGL.Datos.C_M_INSTALACIONES instalacion = null;
                using (GestionGL.Datos.GestionGLModel GestionGlModel = new GestionGL.Datos.GestionGLModel())
                {
                    subdivision = GestionGlModel.C_SUBDIVISIONES.FirstOrDefault(x => x.SUBDIVISION == subdivision_autorizador);
                    instalacion = GestionGlModel.C_M_INSTALACIONES.FirstOrDefault(x => x.ID_C_M_INSTALACION == fk_id_instalacion_destino);
                }

                model.SubdivisionAutorizador = subdivision_autorizador;
                model.DescripcionSubdivision = subdivision.DESCRIPCION_SUBDIVISION;

                model.NombreInstalacionDestino = instalacion.NOMBRE_INSTALACION;

                IEnumerable<STPIntegral.Datos.VW_TabReservacionesCapacidadHospedaje_NOLOCK> capacidades_disponibles_hospedaje = STPIntegral.Negocios.Vw_TabReservacionesCapacidadHospedaje.ObtenerDetallesCapacidad(fk_id_instalacion_destino,fecha_subida,fecha_bajada,subdivision_autorizador,contrato,folio_especial);

                model.ProcesarDetallesCapacidadesHospedaje(capacidades_disponibles_hospedaje);

                resultado = PartialView(model);
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            return resultado;
        }

        [HttpGet]
        public ActionResult VistaPermisosAutorizador(string tipo_servicio, string tipo_transporte, string ficha_autorizador, DateTime fecha_subida, DateTime fecha_bajada)
        {
            ActionResult resultado = null;

            Models.PermisosAutorizadorViewModel model = new Models.PermisosAutorizadorViewModel();

            try
            {
                using (GestionGL.Datos.GestionGLModel GestionGlmodel = new GestionGL.Datos.GestionGLModel())
                {
                    GestionGL.Datos.C_EMPLEADOS_PEMEX empleado_pemex = GestionGlmodel.C_EMPLEADOS_PEMEX.FirstOrDefault(x => x.FICHA == ficha_autorizador);

                    if (empleado_pemex != null)
                    {
                        model.FichaAutorizador = empleado_pemex.FICHA;
                        model.NombreAutorizador = empleado_pemex.NOMBRES + " " + empleado_pemex.APELLIDOS;

                        string[] cves_areas_servicio = null;

                        switch(tipo_servicio)
                        {
                            case "SOLO HOSPEDAJE":
                            {
                                cves_areas_servicio = new string[] { "AH" };
                                break;
                            }
                            case "SOLO TRANSPORTE":
                            {
                                if(tipo_transporte == "AEREO")
                                {
                                    cves_areas_servicio = new string[] { "STA" };
                                }
                                else
                                {
                                    cves_areas_servicio = new string[] { "STPVM" };
                                }
                                break;
                            }
                            default:
                            {
                                if(tipo_transporte == "AEREO")
                                {
                                    cves_areas_servicio = new string[] { "AH", "STA" };
                                }
                                else
                                {
                                    cves_areas_servicio = new string[] {  "AH", "STPVM" };
                                }
                                break;
                            }
                        }

                        using (GestionGL.Datos.GestionGLModel gestionglModel = new GestionGL.Datos.GestionGLModel())
                        {
                            double? folio_documento = gestionglModel.M_FORMATO.Where(x => x.FICHA == ficha_autorizador && x.STATUS == 3).Max(x => x.FOLIO);

                            if (folio_documento != null)
                            {
                                model.NoFolioDelegacionFirmas = folio_documento.Value.ToString();
                            }
                        }

                        model.AreasServicio = cves_areas_servicio;
                        model.PermisosAutorizador = GestionGlmodel.VW_C_D_AUTORIZADORES.Where(x => x.FICHA == ficha_autorizador && cves_areas_servicio.Contains(x.CVE_AREA_SERVICIO) && (x.FECHA_INICIO < fecha_subida && x.FECHA_FINAL > fecha_bajada) ).ToList();
                    }
                }

                resultado = PartialView(model);
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            return resultado;
        }

        [HttpGet]
        public ActionResult FirmaAutorizador(string FichaAutorizador)
        {
            ActionResult resultado = null;

            string noFolioDelegacionFirmas = "";
            string url_firma = "";

            using (GestionGL.Datos.GestionGLModel gestionglModel = new GestionGL.Datos.GestionGLModel())
            {
                double? folio_documento = gestionglModel.M_FORMATO.Where(x => x.FICHA == FichaAutorizador && x.STATUS == 3).Max(x => x.FOLIO);

                if (folio_documento != null)
                {
                    noFolioDelegacionFirmas = folio_documento.Value.ToString();
                    url_firma = String.Format("{0}{1}", System.Configuration.ConfigurationManager.AppSettings["urlFirmaDelegacionFirmas"], noFolioDelegacionFirmas);
                }
            }

            byte[] image_data = null;

            if (!String.IsNullOrEmpty(url_firma))
            {
                using (WebClient client = new WebClient())
                {
                    image_data = client.DownloadData(url_firma);
                }
            }


            if (image_data != null)
            {
                resultado = new FileContentResult(image_data, "image/jpeg");
            }
            else
            {
                resultado = null;
            }
            

            return resultado;
        }

        [HttpGet]
        public ActionResult Editar(string folioSolicitudIntegral)
        {
            string datosSolicitudIntegral = "";

            if (!String.IsNullOrEmpty(folioSolicitudIntegral))
            {
                JsonResult datosJsonSolicitudIntegral = ObtenerDatosEdicionSolicitudIntegral(folioSolicitudIntegral);

                if (datosJsonSolicitudIntegral != null)
                {
                    datosSolicitudIntegral = new JavaScriptSerializer().Serialize(datosJsonSolicitudIntegral.Data);
                }

            }
            else
            {
                SolicitudIntegralModel solicitud_integral_model = new SolicitudIntegralModel();

                if (solicitud_integral_model != null)
                {
                    SolicitudIntegralMVC.Models.UserLogon usuario_logueado = SolicitudIntegralMVC.Infraestructure.Utilerias.ObtenerUsuarioAutentificado(this.HttpContext);

                    if (Infraestructure.Utilerias.ObtenerTipoTrabajador(usuario_logueado.NombreUsuario) == "PEMEX")
                    {
                        JsonResult resultado = Json(new
                        {
                            result = new
                            {
                                SolicitudNueva = true,
                                FichaSolicitante = Infraestructure.Utilerias.FormatearFicha(usuario_logueado.NombreUsuario),
                                NombreSolicitante = usuario_logueado.NombreCompletoUsuario,
                                SubdivisionSolicitante = usuario_logueado.SubdivisionActual,
                                DescripcionSubdivisionSolicitante = usuario_logueado.DescripcionSubdivisionActual
                            }
                        }, JsonRequestBehavior.AllowGet);

                        datosSolicitudIntegral = new JavaScriptSerializer().Serialize(resultado.Data);
                    }

                    
                }
            }
            return View("Editar", ((object)datosSolicitudIntegral));
        }

        [HttpGet]
        public ActionResult Reprogramar(string folioSolicitudIntegral)
        {
            string datosSolicitudIntegral = "";

            if (!String.IsNullOrEmpty(folioSolicitudIntegral))
            {
                JsonResult datosJsonSolicitudIntegral = ObtenerDatosEdicionSolicitudIntegral(folioSolicitudIntegral);

                if (datosJsonSolicitudIntegral != null)
                {
                    datosSolicitudIntegral = new JavaScriptSerializer().Serialize(datosJsonSolicitudIntegral.Data);
                }

            }

            return View("Reprogramar", ((object)datosSolicitudIntegral));
        }

        [HttpGet]
        public ActionResult Diferir(string folioSolicitudIntegral)
        {
            string datosSolicitudIntegral = "";

            if (!String.IsNullOrEmpty(folioSolicitudIntegral))
            {
                JsonResult datosJsonSolicitudIntegral = ObtenerDatosEdicionSolicitudIntegral(folioSolicitudIntegral);

                if (datosJsonSolicitudIntegral != null)
                {
                    datosSolicitudIntegral = new JavaScriptSerializer().Serialize(datosJsonSolicitudIntegral.Data);
                }

            }
            return View("Diferir", ((object)datosSolicitudIntegral));
        }

        [HttpPost]
        public ActionResult Diferir(STPIntegral.Datos.TabSolicitudIntegral solicitud_integral, IEnumerable<STPIntegral.Datos.TabDetallesSolicitudIntegral> detalles_diferir, string folio_solicitud_integral_a_diferir)
        {
            ActionResult resultado = null;
            try
            {
                SolicitudIntegralMVC.Models.UserLogon usuario_logueado = SolicitudIntegralMVC.Infraestructure.Utilerias.ObtenerUsuarioAutentificado(this.HttpContext);

                solicitud_integral.FichaCaptura = usuario_logueado.NombreUsuario;
                solicitud_integral.NombreCaptura = usuario_logueado.NombreCompletoUsuario;
                solicitud_integral.SubdivisionCaptura = usuario_logueado.SubdivisionActual;
                solicitud_integral.NodoAtencion = usuario_logueado.NodoAtencionActual;

                STPIntegral.Datos.TabAuditoriaSolicitudIntegral Auditoria = null;
                Auditoria = ObtenerDatosAuditoria();

                STPIntegral.Negocios.Models.SolicitudIntegralModel solicitud_integral_respuesta = STPIntegral.Negocios.SolicitudIntegralController.DiferirPasajerosSolicitud(solicitud_integral, detalles_diferir, folio_solicitud_integral_a_diferir,Auditoria);

                STPIntegral.Negocios.Models.SolicitudIntegralModel solicitud_integral_a_diferir_respuesta = STPIntegral.Negocios.SolicitudIntegralController.ObtenerDatosEdicionSolicitudIntegral(folio_solicitud_integral_a_diferir, usuario_logueado.IdPermisoUaActual);

                if (Request.IsAjaxRequest())
                {
                    solicitud_integral_respuesta.SolicitudIntegral.TabDetallesSolicitudIntegral = null;
                    solicitud_integral_a_diferir_respuesta.SolicitudIntegral.TabDetallesSolicitudIntegral = null;
                    solicitud_integral_respuesta.SolicitudIntegral.TabAuditoriaSolicitudIntegral = null;
                    resultado = Json(new { SolicitudDiferida = solicitud_integral_respuesta, SolicitudADiferir =  solicitud_integral_a_diferir_respuesta }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            return resultado;
        }

        #region FUNCIONES PROPIAS DE LA SOLICITUD INTEGRAL

        [HttpGet]
        public ActionResult EditarSolicitudIntegral(string folioSolicitudIntegral)
        {
            ActionResult resultado = null;
            try
            {
                if (Request.IsAjaxRequest())
                {
                    resultado = ObtenerDatosEdicionSolicitudIntegral(folioSolicitudIntegral);
                }
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }

            return resultado;
        }

        private JsonResult ObtenerDatosEdicionSolicitudIntegral(string folioSolicitudIntegral)
        {
            JsonResult resultado = null;

            SolicitudIntegralMVC.Models.UserLogon usuario_logueado = SolicitudIntegralMVC.Infraestructure.Utilerias.ObtenerUsuarioAutentificado(this.HttpContext);

            SolicitudIntegralModel solicitud_integral_model = STPIntegral.Negocios.SolicitudIntegralController.ObtenerDatosEdicionSolicitudIntegral(folioSolicitudIntegral, usuario_logueado.IdPermisoUaActual);

            if (solicitud_integral_model != null)
            {
                solicitud_integral_model.SolicitudIntegral.TabAuditoriaSolicitudIntegral = null;
                solicitud_integral_model.SolicitudIntegral.TabDetallesSolicitudIntegral = null;

                resultado = Json(new
                {
                    result = new
                    {
                        SolicitudNueva = false,
                        SolicitudIntegral = solicitud_integral_model.SolicitudIntegral,
                        DetallesSolicitudIntegral = solicitud_integral_model.DetallesSolicitudIntegral,
                        ContratoSap = solicitud_integral_model.ContratoSap,
                        ItinerariosTransporte = solicitud_integral_model.ItinerariosTransporte,
                        InstalacionesOrigen = (from x in solicitud_integral_model.InstalacionesOrigen select new 
                            {
                                ID_C_M_INSTALACION = x.ID_C_M_INSTALACION,
                                SIGLAS_INSTALACION = x.SIGLAS_INSTALACION,
                                NOMBRE_INSTALACION = x.NOMBRE_INSTALACION
                            }),
                        InstalacionDestino = (from x in solicitud_integral_model.InstalacionesDestino select new 
                            {
                                ID_C_M_INSTALACION = x.ID_C_M_INSTALACION,
                                SIGLAS_INSTALACION = x.SIGLAS_INSTALACION,
                                NOMBRE_INSTALACION = x.NOMBRE_INSTALACION
                            }),
                        ListadoDireccionamientosDestinos = (from x in solicitud_integral_model.ListadoDireccionamientosDestinos select new 
                            {
                                CVE_CTA_MAYOR = x.CVE_CTA_MAYOR,
                                CVE_CTO_COSTOS = x.CVE_CTO_COSTOS,
                                CVE_CTO_GESTOR = x.CVE_CTO_GESTOR,
                                CVE_ELEMENTO_PEP = x.CVE_ELEMENTO_PEP,
                                CVE_FONDO = x.CVE_FONDO,
                                CVE_POSICION_PRESUPUESTARIA = x.CVE_POSICION_PRESUPUESTARIA,
                                CVE_PROGRAMA_PRESUPUESTAL = x.CVE_PROGRAMA_PRESUPUESTAL,
                                FIN = x.FIN,
                                INICIO = x.INICIO,
                                ID_C_M_INSTALACION = x.ID_C_M_INSTALACION,
                                SIGLAS_INSTALACION = x.SIGLAS_INSTALACION,
                                CVE_AREA_SERVICIO = x.CVE_AREA_SERVICIO,
                                PK_ID_AREAS_SERVICIO = x.PK_ID_AREAS_SERVICIO
                            }),
                        ListadoDireccionamientosOrigenes = (from x in solicitud_integral_model.ListadoDireccionamientosOrigenes select new 
                            {
                                CVE_CTA_MAYOR = x.CVE_CTA_MAYOR,
                                CVE_CTO_COSTOS = x.CVE_CTO_COSTOS,
                                CVE_CTO_GESTOR = x.CVE_CTO_GESTOR,
                                CVE_ELEMENTO_PEP = x.CVE_ELEMENTO_PEP,
                                CVE_FONDO = x.CVE_FONDO,
                                CVE_POSICION_PRESUPUESTARIA = x.CVE_POSICION_PRESUPUESTARIA,
                                CVE_PROGRAMA_PRESUPUESTAL = x.CVE_PROGRAMA_PRESUPUESTAL,
                                FIN = x.FIN,
                                INICIO = x.INICIO,
                                ID_C_M_INSTALACION = x.ID_C_M_INSTALACION,
                                SIGLAS_INSTALACION = x.SIGLAS_INSTALACION,
                                CVE_AREA_SERVICIO = x.CVE_AREA_SERVICIO,
                                PK_ID_AREAS_SERVICIO = x.PK_ID_AREAS_SERVICIO
                            })
                    }
                }, JsonRequestBehavior.AllowGet);
                
            }

            return resultado;
        }

        [HttpGet]
        public ActionResult ConsultaSolicitudesIntegrales(int page, int pageSize, string searchString, string ficha, DateTime? fecha)
        {
            ActionResult resultado = null;
            
            try
            {
                IEnumerable<STPIntegral.Datos.SI_Listado_Solicitudes_Result> listado_solicitudes = null;

                if (page <= 0)
                {
                    page = 1;
                }

                SolicitudIntegralMVC.Models.UserLogon usuario_logueado = SolicitudIntegralMVC.Infraestructure.Utilerias.ObtenerUsuarioAutentificado(this.HttpContext);
                
                int total_registros = 0;

                listado_solicitudes = STPIntegral.Negocios.SolicitudIntegralController.ConsultaSolicitudesIntegrales(page, pageSize, searchString,ficha,fecha,usuario_logueado.NodoAtencionActual, out total_registros, usuario_logueado.IdPermisoUaActual);

                if (Request.IsAjaxRequest())
                {
                    resultado = Json(new { result = listado_solicitudes, page = page, pageSize = pageSize, total_registros = total_registros }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }

            return resultado;
        }

        [HttpGet]
        public ActionResult ValidarSolicitante(string ficha_solictante, string tipo_solicitud, string tipo_transporte)
        {
            ActionResult resultado = null;
            try
            {
                SolicitudIntegralMVC.Models.UserLogon usuario_logueado = SolicitudIntegralMVC.Infraestructure.Utilerias.ObtenerUsuarioAutentificado(this.HttpContext);

                GestionGL.Datos.VW_D_USUARIOS_GENERAL usuario = STPIntegral.Negocios.SolicitudIntegralController.ValidarSolicitante(usuario_logueado.SubdivisionActual,ficha_solictante, tipo_solicitud, tipo_transporte);

                if (Request.IsAjaxRequest())
                {
                    if (usuario != null)
                    {
                        if (!string.IsNullOrEmpty(usuario.FICHA))
                            resultado = Json(new { result = new { NOMBRE = usuario.NOMBRE, FICHA = usuario.FICHA, SUBDIVISION = usuario.SUBDIVISION, DESCRIPCION_SUBDIVISION = usuario.DESCRIPCION_CTO_GESTOR } }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        resultado = Json(new { result = "" }, JsonRequestBehavior.AllowGet);
                    }
                    
                }
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }

            return resultado;
        }
        
        [HttpGet]
        public ActionResult ValidarFichaRFC(string ficha_rfc, string tipo_empleado)
        {
            ActionResult resultado = null;
            try
            {
                STPIntegral.Negocios.Models.EmpleadoPemexCia empleado_pemex_cia = STPIntegral.Negocios.SolicitudIntegralController.ValidarEmpleadoPemexCia(ficha_rfc, tipo_empleado);

                if (Request.IsAjaxRequest())
                {
                    resultado = Json(new { result = empleado_pemex_cia }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }

            return resultado;
        }

        [HttpGet]
        public ActionResult ValidarAutorizador(string ficha_autorizador, DateTime fecha_subida, DateTime fecha_bajada, string tipo_solicitud, string tipo_transporte)
        {
            ActionResult resultado = null;
            try
            {

                SolicitudIntegralMVC.Models.UserLogon usuario_logueado = SolicitudIntegralMVC.Infraestructure.Utilerias.ObtenerUsuarioAutentificado(this.HttpContext);

                GestionGL.Datos.VW_C_D_AUTORIZADORES usuario = STPIntegral.Negocios.SolicitudIntegralController.ValidarAutorizador(usuario_logueado.SubdivisionActual, ficha_autorizador, fecha_subida, fecha_bajada, tipo_solicitud, tipo_transporte);

                if (Request.IsAjaxRequest())
                {
                    resultado = Json(new { result = new { NOMBRE = usuario.NOMBRE_COMPLETO, FICHA = usuario.FICHA, SUBDIVISION = usuario.SUBDIVISION, DESCRIPCION_SUBDIVISION = usuario.DESCRIPCION_SUBDIVISION } }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }

            return resultado;
        }

        [HttpGet]
        public ActionResult ConsultarItinerarios(string tipo_solicitud_integral, string tipo_transporte, string tipo_servicio, DateTime fecha_servicio)
        {
            ActionResult resultado = null;
            try
            {
                SolicitudIntegralMVC.Models.UserLogon usuario_logueado = SolicitudIntegralMVC.Infraestructure.Utilerias.ObtenerUsuarioAutentificado(this.HttpContext);

                //TODO: Aplicar paso de parametros del contrato y subdivision.
                IEnumerable<STPIntegral.Datos.Vw_SoltIntItinerariosCapacidadDia_NOLOCK> itinerarios_disponibles = null; //ItinerariosTransporte.ConsultarItinerarios(tipo_transporte, tipo_servicio, fecha_servicio, usuario_logueado.NodoAtencionActual, "", "", "", "", true);
                IEnumerable<STPIntegral.Datos.Vw_SoltIntItinerariosCapacidadDia_NOLOCK> r = null;
                //var r = itinerarios_disponibles.Where(x => x.des_itinerario.Contains("EMER"));
                
                if (Request.IsAjaxRequest())
                {
                    if (tipo_servicio == "EMERGENCIA")
                    {
                        itinerarios_disponibles = ItinerariosTransporte.ConsultarItinerarios(tipo_solicitud_integral, tipo_transporte, "DISPOSICION", fecha_servicio, usuario_logueado.NodoAtencionActual, "", "", "", "", true);
                        r = itinerarios_disponibles.Where(x => x.des_itinerario.Contains("EMER"));
                    }
                    else
                    {
                        itinerarios_disponibles = ItinerariosTransporte.ConsultarItinerarios(tipo_solicitud_integral, tipo_transporte, tipo_servicio, fecha_servicio, usuario_logueado.NodoAtencionActual, "", "", "", "", true);
                        r = itinerarios_disponibles.Where(x => !x.des_itinerario.Contains("EMER"));
                    }

                    resultado = Json(new { result = r /*itinerarios_disponibles*/ }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }

            return resultado;
        }

        [HttpGet]
        public ActionResult ConsultarContrato(string numero_contrato, DateTime fecha_incio, DateTime fecha_fin)
        {
            ActionResult resultado = null;
            try
            {
                GestionGL.Datos.VW_C_CONTRATOS contrato = STPIntegral.Negocios.SolicitudIntegralController.ValidarContrato(numero_contrato,fecha_incio,fecha_fin);
                if (Request.IsAjaxRequest())
                {
                    resultado = Json(new { result = contrato }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            return resultado;
        }

        [HttpGet]
        public ActionResult ConsultarInstalacionesOrigen(string tipo_solicitud, string tipo_personal, string tipo_transporte, string tipo_servicio, int id_itinerario, string ficha_autorizador, DateTime fecha_subida, DateTime fecha_bajada)
        {
            ActionResult resultado = null;
            try
            {
                InstalacionesDestinoDireccionamientos listado_instalaciones_origen = STPIntegral.Negocios.SolicitudIntegralController.ConsultarOrigenes(tipo_solicitud, tipo_personal, tipo_transporte, tipo_servicio, id_itinerario, ficha_autorizador, fecha_subida, fecha_bajada);
                
                if (Request.IsAjaxRequest())
                {
                    
                    if(listado_instalaciones_origen != null)
                    {
                        var instalaciones_inst_origen = from x in listado_instalaciones_origen.InstalacionesDestino
                                                   select new
                                                       {
                                                           ID_C_M_INSTALACION = x.ID_C_M_INSTALACION,
                                                           NOMBRE_INSTALACION = x.NOMBRE_INSTALACION,
                                                           SIGLAS_INSTALACION = x.SIGLAS_INSTALACION
                                                       };

                        var direccionamientos_instalaciones_origen = from x in listado_instalaciones_origen.DireccionamientosInstalaciones
                                                                      select new
                                                                      {
                                                                          CVE_CTA_MAYOR = x.CVE_CTA_MAYOR,
                                                                          CVE_CTO_COSTOS = x.CVE_CTO_COSTOS,
                                                                          CVE_CTO_GESTOR = x.CVE_CTO_GESTOR,
                                                                          CVE_ELEMENTO_PEP = x.CVE_ELEMENTO_PEP,
                                                                          CVE_FONDO = x.CVE_FONDO,
                                                                          CVE_POSICION_PRESUPUESTARIA = x.CVE_POSICION_PRESUPUESTARIA,
                                                                          CVE_PROGRAMA_PRESUPUESTAL = x.CVE_PROGRAMA_PRESUPUESTAL,
                                                                          FIN = x.FIN,
                                                                          INICIO = x.INICIO,
                                                                          ID_C_M_INSTALACION = x.ID_C_M_INSTALACION,
                                                                          SIGLAS_INSTALACION = x.SIGLAS_INSTALACION,
                                                                          CVE_AREA_SERVICIO = x.CVE_AREA_SERVICIO,
                                                                          PK_ID_AREAS_SERVICIO = x.PK_ID_AREAS_SERVICIO
                                                                      };

                        resultado = Json(new { result = new { InstalacionesOrigen = instalaciones_inst_origen, DireccionamientosInstalaciones = direccionamientos_instalaciones_origen } }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        resultado = Json(new { result = new { InstalacionesOrigen = 0, DireccionamientosInstalaciones = 0 } }, JsonRequestBehavior.AllowGet);
                    }

                }
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            return resultado;
        }

        [HttpGet]
        public ActionResult ObtenerCapacidadHospedaje(int id_instalacion_destino, DateTime fecha_subida, DateTime fecha_bajada, string no_contrato, string folio_especial, string subdivision_autorizador)
        {
            ActionResult resultado = null;
            try
            {
                int capacidad_hospedaje = STPIntegral.Negocios.SolicitudIntegralController.ObtenerCapacidadHospedaje(id_instalacion_destino, fecha_subida, fecha_bajada, no_contrato, folio_especial, subdivision_autorizador);

                if (Request.IsAjaxRequest())
                {
                    resultado = Json(new { result = capacidad_hospedaje }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            return resultado;
        }

        [HttpGet]
        public ActionResult ObtenerCapacidadTransportePorItem(int id_itinerario, DateTime fecha_subida, string folio_especial_transporte, string no_contrato, string subdivision_autorizador)
        {
            ActionResult resultado = null;
            try
            {
                //TODO: Hay que arreglar esto para que tambien acepte la subdivision como parametro
                int capacidad_transporte = STPIntegral.Negocios.ItinerariosTransporte.ObtenerCapacidadTransportePorItem(id_itinerario, fecha_subida, folio_especial_transporte, no_contrato, subdivision_autorizador);
                
                if (Request.IsAjaxRequest())
                {
                    resultado = Json(new { result = capacidad_transporte }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            return resultado;
        }

        [HttpGet]
        public ActionResult ConsultarInstalacionesDestinos(string tipo_solicitud, string tipo_personal, string tipo_transporte, string tipo_servicio, int? id_itinerario, string ficha_autorizador, DateTime fecha_subida, DateTime fecha_bajada, int? id_instalacion_origen)
        {
            ActionResult resultado = null;
            try
            {
                STPIntegral.Negocios.Models.InstalacionesDestinoDireccionamientos instalaciones_destino_direccionamientos = STPIntegral.Negocios.SolicitudIntegralController.ConsultarDestinos(tipo_solicitud, tipo_personal, tipo_transporte, tipo_servicio, id_itinerario, ficha_autorizador, fecha_subida, fecha_bajada, id_instalacion_origen);
                
                if (Request.IsAjaxRequest())
                {


                    if (instalaciones_destino_direccionamientos.InstalacionesDestino != null)
                    {

                        var instalaciones_destino = from x in instalaciones_destino_direccionamientos.InstalacionesDestino
                                                                      select new
                                                                      {
                                                                          ID_C_M_INSTALACION = x.ID_C_M_INSTALACION,
                                                                          NOMBRE_INSTALACION = x.NOMBRE_INSTALACION,
                                                                          SIGLAS_INSTALACION = x.SIGLAS_INSTALACION
                                                                      };

                        var direccionamientos_instalaciones_destino = from x in instalaciones_destino_direccionamientos.DireccionamientosInstalaciones
                                                                      select new
                                                                      {
                                                                          CVE_CTA_MAYOR = x.CVE_CTA_MAYOR,
                                                                          CVE_CTO_COSTOS = x.CVE_CTO_COSTOS,
                                                                          CVE_CTO_GESTOR = x.CVE_CTO_GESTOR,
                                                                          CVE_ELEMENTO_PEP = x.CVE_ELEMENTO_PEP,
                                                                          CVE_FONDO = x.CVE_FONDO,
                                                                          CVE_POSICION_PRESUPUESTARIA = x.CVE_POSICION_PRESUPUESTARIA,
                                                                          CVE_PROGRAMA_PRESUPUESTAL = x.CVE_PROGRAMA_PRESUPUESTAL,
                                                                          FIN = x.FIN,
                                                                          INICIO = x.INICIO,
                                                                          ID_C_M_INSTALACION = x.ID_C_M_INSTALACION,
                                                                          SIGLAS_INSTALACION = x.SIGLAS_INSTALACION,
                                                                          CVE_AREA_SERVICIO = x.CVE_AREA_SERVICIO,
                                                                          PK_ID_AREAS_SERVICIO = x.PK_ID_AREAS_SERVICIO
                                                                      };

                        resultado = Json(new { result = new { InstalacionesDestino = instalaciones_destino, DireccionamientosInstalaciones = direccionamientos_instalaciones_destino } }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        resultado = Json(new { result = new { InstalacionesDestino = 0, DireccionamientosInstalaciones = 0 } }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            return resultado;
        }

        [HttpGet]
        public ActionResult ValidarEmpleadoDetalleSolicitud(string tipo_solicitud, string tipo_personal, string tipo_transporte, string tipo_servicio, int id_itinerario, string ficha_autorizador, DateTime fecha_subida, DateTime fecha_bajada, int id_instalacion_origen, int id_instalacion_destino, string ficha_rfc, string tipo_movimiento_personal, string folio_especial_transporte, string folio_especial_hospedaje, string numero_contrato, string subdivision_autorizador)
        {
            ActionResult resultado = null;
            try
            {
                STPIntegral.Datos.TabDetallesSolicitudIntegral detalle_solicitud = STPIntegral.Negocios.SolicitudIntegralController.ValidarEmpleadoDetalleSolicitud(tipo_solicitud, tipo_personal, tipo_transporte, tipo_servicio, id_itinerario, ficha_autorizador, fecha_subida, fecha_bajada, id_instalacion_origen, id_instalacion_destino, ficha_rfc, tipo_movimiento_personal, folio_especial_transporte, folio_especial_hospedaje, numero_contrato, subdivision_autorizador);
                
                if (Request.IsAjaxRequest())
                {
                    resultado = Json(new { result = detalle_solicitud }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            return resultado;
        }

        [HttpPost]
        public ActionResult ReprogramarSolicitudIntegral(STPIntegral.Datos.TabSolicitudIntegral solicitud_integral, IEnumerable<STPIntegral.Datos.TabDetallesSolicitudIntegral> detalles_solicitud_integral, DateTime nueva_fecha_subida, DateTime nueva_fecha_bajada)
        {
            ActionResult resultado = null;
            try
            {
                SolicitudIntegralMVC.Models.UserLogon usuario_logueado = SolicitudIntegralMVC.Infraestructure.Utilerias.ObtenerUsuarioAutentificado(this.HttpContext);
                
                solicitud_integral.FichaCaptura = usuario_logueado.NombreUsuario;
                solicitud_integral.NombreCaptura = usuario_logueado.NombreCompletoUsuario;
                solicitud_integral.SubdivisionCaptura = usuario_logueado.SubdivisionActual;
                solicitud_integral.NodoAtencion = usuario_logueado.NodoAtencionActual;

                STPIntegral.Datos.TabAuditoriaSolicitudIntegral Auditoria = null;
                Auditoria = ObtenerDatosAuditoria();
                //STPIntegral.Negocios.Models.SolicitudIntegralModel solicitud_integral_respuesta = null;
                STPIntegral.Negocios.Models.SolicitudIntegralModel solicitud_integral_respuesta = STPIntegral.Negocios.SolicitudIntegralController.ReprogramarSolicitud(solicitud_integral, detalles_solicitud_integral, nueva_fecha_subida, nueva_fecha_bajada,Auditoria);

                if (Request.IsAjaxRequest())
                {
                    solicitud_integral_respuesta.SolicitudIntegral.TabDetallesSolicitudIntegral = null;
                    solicitud_integral_respuesta.SolicitudIntegral.TabAuditoriaSolicitudIntegral = null;


                    resultado = Json(new { result = solicitud_integral_respuesta }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            return resultado;
        }

        public STPIntegral.Datos.TabAuditoriaSolicitudIntegral ObtenerDatosAuditoria()
        {
            
            SolicitudIntegralMVC.Models.UserLogon usuario_logueado = SolicitudIntegralMVC.Infraestructure.Utilerias.ObtenerUsuarioAutentificado(this.HttpContext);
            STPIntegral.Datos.TabAuditoriaSolicitudIntegral datosauditoria = new STPIntegral.Datos.TabAuditoriaSolicitudIntegral();

            datosauditoria.FechaAuditoria = DateTime.Now;
            datosauditoria.FichaRFCUsuario = usuario_logueado.NombreUsuario;
            datosauditoria.NombreUsuario = usuario_logueado.NombreCompletoUsuario;
            datosauditoria.SubdivisionUsuario = usuario_logueado.SubdivisionActual;
            datosauditoria.RolUsuario = usuario_logueado.PermisoUsuarioActual;
            datosauditoria.NodoAtencion = usuario_logueado.NodoAtencionActual;
            datosauditoria.IPUsuario = Request.UserHostAddress;
            return datosauditoria;
        }

        [HttpPost]
        public ActionResult AgregarEmpleadosSolicitudIntegral(STPIntegral.Datos.TabSolicitudIntegral solicitud_integral, IEnumerable<STPIntegral.Datos.TabDetallesSolicitudIntegral> detalles_solicitud_integral, string folio_especial_transporte_ida, string folio_especial_transporte_retorno, string folio_especial_hospedaje)
        {
            ActionResult resultado = null;
            try
            {
                SolicitudIntegralMVC.Models.UserLogon usuario_logueado = SolicitudIntegralMVC.Infraestructure.Utilerias.ObtenerUsuarioAutentificado(this.HttpContext);

                solicitud_integral.FichaCaptura = usuario_logueado.NombreUsuario;
                solicitud_integral.NombreCaptura = usuario_logueado.NombreCompletoUsuario;
                solicitud_integral.SubdivisionCaptura = usuario_logueado.SubdivisionActual;
                solicitud_integral.NodoAtencion = usuario_logueado.NodoAtencionActual;

                STPIntegral.Datos.TabAuditoriaSolicitudIntegral Auditoria = null;
                Auditoria = ObtenerDatosAuditoria();

                STPIntegral.Negocios.Models.SolicitudIntegralModel solicitud_integral_respuesta = STPIntegral.Negocios.SolicitudIntegralController.AgregarDetalleSolicitudIntegral(solicitud_integral, detalles_solicitud_integral, Auditoria, folio_especial_transporte_ida, folio_especial_transporte_retorno, folio_especial_hospedaje);

                if (Request.IsAjaxRequest())
                {
                    solicitud_integral_respuesta.SolicitudIntegral.TabDetallesSolicitudIntegral = null;
                    solicitud_integral_respuesta.SolicitudIntegral.TabAuditoriaSolicitudIntegral = null;

                    resultado = Json(new { result = solicitud_integral_respuesta }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            return resultado;
        }

        [HttpPost]
        public ActionResult ReemplazarDetalleSolicitudIntegral(int IdDetalleSolicitudIntegral, string NuevaFichaRFC)
        {
            ActionResult resultado = null;
            try
            {
                SolicitudIntegralMVC.Models.UserLogon usuario_logueado = SolicitudIntegralMVC.Infraestructure.Utilerias.ObtenerUsuarioAutentificado(this.HttpContext);

                STPIntegral.Datos.TabAuditoriaSolicitudIntegral Auditoria = null;
                Auditoria = ObtenerDatosAuditoria();

                List<STPIntegral.Datos.TabDetallesSolicitudIntegral> litado_detalles_solicitud_reemplazado = STPIntegral.Negocios.SolicitudIntegralController.ReemplazarPasajeroSolicitudIntegral(IdDetalleSolicitudIntegral, NuevaFichaRFC, Auditoria);

                if (Request.IsAjaxRequest())
                {
                    resultado = Json(new { result = litado_detalles_solicitud_reemplazado }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            return resultado;
        }

        [HttpPost]
        public ActionResult CambiarOrigenDestinoPasajero(STPIntegral.Datos.TabDetallesSolicitudIntegral detalleSolicitud)
        {
            ActionResult resultado = null;
            try
            {

                STPIntegral.Datos.TabAuditoriaSolicitudIntegral Auditoria = null;
                Auditoria = ObtenerDatosAuditoria();
               // STPIntegral.Negocios.Models.SolicitudIntegralModel solicitud_integral_respuesta = null;
                List<STPIntegral.Datos.TabDetallesSolicitudIntegral> detalles_modificados = STPIntegral.Negocios.SolicitudIntegralController.CambiarOrigenDestinoPasajero(detalleSolicitud, Auditoria);

                if (Request.IsAjaxRequest())
                {
                    resultado = Json(new { result = detalles_modificados }, JsonRequestBehavior.AllowGet);
                }


            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            return resultado;
        }

        
        [HttpPost]
        public ActionResult EliminarDetalleSolicitudIntegral(int IdDetalleSolicitudIntegral)
        {
            ActionResult resultado = null;
            try
            {
                SolicitudIntegralMVC.Models.UserLogon usuario_logueado = SolicitudIntegralMVC.Infraestructure.Utilerias.ObtenerUsuarioAutentificado(this.HttpContext);

                STPIntegral.Datos.TabAuditoriaSolicitudIntegral Auditoria = null;
                Auditoria = ObtenerDatosAuditoria();

                string operacion = "";

                List<STPIntegral.Datos.TabDetallesSolicitudIntegral> detallesSolicitudEliminados = STPIntegral.Negocios.SolicitudIntegralController.EliminarDetalleSolicitudIntegral(IdDetalleSolicitudIntegral, usuario_logueado.IdPermisoUaActual,Auditoria, out operacion);

                if (Request.IsAjaxRequest())
                {
                    resultado = Json(new { result = detallesSolicitudEliminados, operacion = operacion }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            return resultado;
        }

        [HttpPost]
        public ActionResult CancelarSolicitudIntegral(int idSolicitudIntegral)
        {
            ActionResult resultado = null;
            try
            {
                SolicitudIntegralMVC.Models.UserLogon usuario_logueado = SolicitudIntegralMVC.Infraestructure.Utilerias.ObtenerUsuarioAutentificado(this.HttpContext);

                STPIntegral.Datos.TabAuditoriaSolicitudIntegral Auditoria = null;
                Auditoria = ObtenerDatosAuditoria();

                STPIntegral.Datos.TabSolicitudIntegral solicitudIntegral = STPIntegral.Negocios.SolicitudIntegralController.CancelarSolicituIntegral(idSolicitudIntegral,usuario_logueado.IdPermisoUaActual,Auditoria);
                
                solicitudIntegral.TabAuditoriaSolicitudIntegral = null;

                if (Request.IsAjaxRequest())
                {
                    resultado = Json(new { result = solicitudIntegral }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            return resultado;
        }

        [HttpPost]
        public ActionResult ConfirmarSolicitudIntegral(int idSolicitudIntegral)
        {
            ActionResult resultado = null;
            try
            {
                SolicitudIntegralMVC.Models.UserLogon usuario_logueado = SolicitudIntegralMVC.Infraestructure.Utilerias.ObtenerUsuarioAutentificado(this.HttpContext);

                STPIntegral.Datos.TabAuditoriaSolicitudIntegral Auditoria = null;
                Auditoria = ObtenerDatosAuditoria();
                
                STPIntegral.Datos.TabSolicitudIntegral solicitudIntegral = STPIntegral.Negocios.SolicitudIntegralController.ConfirmarSolicitudIntegral(idSolicitudIntegral, usuario_logueado.IdPermisoUaActual,Auditoria);

                if (Request.IsAjaxRequest())
                {
                    resultado = Json(new { result = solicitudIntegral }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            return resultado;
        }

        [HttpPost]
        public ActionResult ConfirmacionMultipleSolicitudesIntegrales(List<int> idSolicitudesIntegrales)
        {
            ActionResult resultado = null;
            try
            {
                System.Text.StringBuilder mensaje = new System.Text.StringBuilder();
                
                if (idSolicitudesIntegrales == null || (idSolicitudesIntegrales != null & idSolicitudesIntegrales.Count <= 0))
                {
                    throw new ShowCaseException("No se ha especifico ninguna solicitud para ser confirmada", null, TypeMessage.warning, "");
                }
                
                SolicitudIntegralMVC.Models.UserLogon usuario_logueado = SolicitudIntegralMVC.Infraestructure.Utilerias.ObtenerUsuarioAutentificado(this.HttpContext);

                STPIntegral.Datos.TabAuditoriaSolicitudIntegral Auditoria = null;
                Auditoria = ObtenerDatosAuditoria();

                List<string> solicitudesConfirmadas = new List<string>();
                List<string> solicitudesNoConfirmadas = new List<string>();

                foreach (int id_solicitud in idSolicitudesIntegrales)
                {
                    //TODO: FALTA APLICAR ALGUN TIPO DE CONTROL EN CASO DE QUE ALGUNA SOLICITUD NO SE PUEDA CONFIRMAR
                    try
                    {
                        STPIntegral.Datos.TabSolicitudIntegral solicitudIntegral = STPIntegral.Negocios.SolicitudIntegralController.ConfirmarSolicitudIntegral(id_solicitud, usuario_logueado.IdPermisoUaActual,Auditoria);
                        solicitudesConfirmadas.Add(solicitudIntegral.FolioSolicitudIntegral);
                    }
                    catch(System.Exception ex)
                    {
                        solicitudesNoConfirmadas.Add(id_solicitud.ToString());
                    }
                }

                mensaje.AppendLine("Se han confirmado las siguientes solicitudes: ");

                foreach (string folio_solicitud in solicitudesConfirmadas)
                {
                    mensaje.Append(folio_solicitud + " ");
                }

                if (Request.IsAjaxRequest())
                {
                    resultado = Json(new { result = mensaje.ToString() }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            return resultado;
        }

        [HttpGet]
        public ActionResult ObtenerCapacidadesReprogramacionSolicitudIntegral(int idSolicitudIntegral, DateTime? nuevaFechaSubida, DateTime? nuevaFechaBajada)
        {
            ActionResult resultado = null;
            
            try
            {
                List<STPIntegral.Negocios.Models.CapacidadReprogramacionSolicitudIntegral> listado_capacidades_solicitud_integral = STPIntegral.Negocios.SolicitudIntegralController.ObtenerCapacidadesReprogramacionSolicitudIntegral(idSolicitudIntegral, nuevaFechaSubida.Value, nuevaFechaBajada);

                if (Request.IsAjaxRequest())
                {
                    resultado = Json(new { result = listado_capacidades_solicitud_integral }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            return resultado;
        }

        [HttpGet]
        public ActionResult ObtenerCapacidadesDiferirSolicitudIntegral(int idSolicitudIntegral, int idItinerarioIdaDiferir, int idItinerarioReotrnoDiferir, string subdivisionAutorizador, DateTime? nuevaFechaSubida, DateTime? nuevaFechaBajada)
        {
            ActionResult resultado = null;

            try
            {
                List<STPIntegral.Negocios.Models.CapacidadReprogramacionSolicitudIntegral> listado_capacidades_solicitud_integral = STPIntegral.Negocios.SolicitudIntegralController.ObtenerCapacidadesDiferirSolicitudIntegral(idSolicitudIntegral, idItinerarioIdaDiferir, idItinerarioReotrnoDiferir, subdivisionAutorizador, nuevaFechaSubida.Value, nuevaFechaBajada.Value);

                if (Request.IsAjaxRequest())
                {
                    resultado = Json(new { result = listado_capacidades_solicitud_integral }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            return resultado;
        }

        [HttpPost]
        public ActionResult GuardarDetalleObservaciones(int idDetalleSolicitudIntegral, string FichaOcupante, string Observaciones, bool LlevaMaterial)
        {
            ActionResult resultado = null;

            try
            {
                STPIntegral.Datos.TabAuditoriaSolicitudIntegral Auditoria = null;
                Auditoria = ObtenerDatosAuditoria();
                STPIntegral.Negocios.SolicitudIntegralController.GuardarDetalleObservaciones(idDetalleSolicitudIntegral, FichaOcupante, Observaciones, LlevaMaterial,Auditoria);
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }

            return resultado;
        }

        //Consulta de Capacidades Folio Especial
        [HttpGet]
        public ActionResult ObtenerCapacidadFolioEspecialTransporte(int idItinerarioTransporte, DateTime fechaServicio, string folioEspecial)
        {
            ActionResult resultado = null;

            try
            {
                int capacidad_folio_especal = STPIntegral.Negocios.ItinerariosTransporte.ObtenerCapacidadFolioEspecialPorItem(idItinerarioTransporte, fechaServicio, folioEspecial);

                if (Request.IsAjaxRequest())
                {
                    resultado = Json(new { result = capacidad_folio_especal }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            return resultado;
        }

        [HttpGet]
        public ActionResult ObtenerCapacidadFolioEspecialHospedaje(int idInstalacionDestino, DateTime fechaSubida, DateTime fechaBajada, string folioEspecial)
        {
            ActionResult resultado = null;

            try
            {
                int capacidad_folio_especal = STPIntegral.Negocios.SolicitudIntegralController.ObtenerCapacidadFolioEspecialHospedaje(idInstalacionDestino, fechaSubida, fechaBajada, folioEspecial);

                if (Request.IsAjaxRequest())
                {
                    resultado = Json(new { result = capacidad_folio_especal }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            return resultado;
        }

        #endregion

        #region Imprimir Reportes Solicitud Integral

        [HttpGet]
        public ActionResult ImprimirSolicitudIntegral(string folioSolicitudIntegral)
        {

            byte[] resultado = ImpimirReporteSolicitudIntegralEnPDF(folioSolicitudIntegral);

            return new BinaryContentResult()
            {
                FileName = String.Format("Solicitud {0}.pdf",folioSolicitudIntegral),
                ContentType = "application/pdf",
                Content = resultado
            };
        }

        [HttpGet]
        public ActionResult ImpimirReporteBoletosSolicitudIntegral(List<string> folioSolicitudIntegral)
        {
            byte[] resultado = ImpimirReporteBoletosSolicitudIntegralEnPDF(folioSolicitudIntegral);

            string nombre_archivo = "";
            if (folioSolicitudIntegral != null && folioSolicitudIntegral.Count > 0)
            {
                string[] folios = folioSolicitudIntegral[0].Split(',');
                if (folios.Count() == 1)
                {
                    nombre_archivo = String.Format("BoletosSolicitud {0}", folioSolicitudIntegral[0]);
                }
                else
                {
                    nombre_archivo = String.Format("BoletosSolicitudes Multiples");
                }
            }

            return new BinaryContentResult()
            {
                FileName = nombre_archivo,
                ContentType = "application/pdf",
                Content = resultado
            };
        }

        private byte [] ImpimirReporteSolicitudIntegralEnPDF(string folio_solicitud_integral)
        {
            ////ESTA CONFIGURACION ESTA GUARDADA EN EL WEBCONFIG DENTRO DE LA SECCION <appSettings>
            string usuario_dominio_reporting = System.Configuration.ConfigurationManager.AppSettings["user_Directorio_Activo"];
            string password_dominio_reporting = System.Configuration.ConfigurationManager.AppSettings["password_Directorio_Activo"];
            string dominio_usuario_reporting = System.Configuration.ConfigurationManager.AppSettings["Dominio"];
            string ruta_reportes_solicitudes = System.Configuration.ConfigurationManager.AppSettings["ruta_reportes_solicitud_integral"];

            string reportPath = "";

            System.Random generator = new Random();

            SolicitudIntegralMVC.WebServiceReportingServices.ParameterValue[] parameters = null;

            reportPath = String.Format("{0}RptSolicitud_Integral", ruta_reportes_solicitudes);
            parameters = new SolicitudIntegralMVC.WebServiceReportingServices.ParameterValue[1];

            //En este caso, estos parametros son comunes en cualquier caso para los dos reportes.
            parameters[0] = new SolicitudIntegralMVC.WebServiceReportingServices.ParameterValue();
            parameters[0].Name = "SOLICITUD";
            parameters[0].Value = folio_solicitud_integral;

            byte[] output;
            string extension, mimeType, encoding;
            SolicitudIntegralMVC.WebServiceReportingServices.Warning[] warnings;
            string[] streamIds;

            SolicitudIntegralMVC.ReportingServices.ReportExporter.Export(
                "ReportExecutionServiceSoap" /* name of the WCF endpoint from Web.config */,
                new System.Net.NetworkCredential(usuario_dominio_reporting, password_dominio_reporting, dominio_usuario_reporting),
                reportPath,
                parameters,
                SolicitudIntegralMVC.ReportingServices.ExportFormat.PDF,
                out output,
                out extension,
                out mimeType,
                out encoding,
                out warnings,
                out streamIds
            );

            return output;
        }

        private byte [] ImpimirReporteBoletosSolicitudIntegralEnPDF(List<string> folios_solicitudes_integrales)
        {
            ////ESTA CONFIGURACION ESTA GUARDADA EN EL WEBCONFIG DENTRO DE LA SECCION <appSettings>
            string usuario_dominio_reporting = System.Configuration.ConfigurationManager.AppSettings["user_Directorio_Activo"];
            string password_dominio_reporting = System.Configuration.ConfigurationManager.AppSettings["password_Directorio_Activo"];
            string dominio_usuario_reporting = System.Configuration.ConfigurationManager.AppSettings["Dominio"];
            string ruta_reportes_solicitudes = System.Configuration.ConfigurationManager.AppSettings["ruta_reportes_solicitud_integral"];

            string reportPath = "";

            System.Random generator = new Random();

            SolicitudIntegralMVC.WebServiceReportingServices.ParameterValue[] parameters = null;

            reportPath = String.Format("{0}RptBoleto", ruta_reportes_solicitudes);
            parameters = new SolicitudIntegralMVC.WebServiceReportingServices.ParameterValue[1];

            //En este caso, estos parametros son comunes en cualquier caso para los dos reportes.
            System.Text.StringBuilder folios_parametros = new System.Text.StringBuilder();

            for(int i = 0; i < folios_solicitudes_integrales.Count;++i)
            {
                folios_parametros.Append(String.Format("{0}",folios_solicitudes_integrales[i]));

                if( (i+1) < folios_solicitudes_integrales.Count )
                {
                    folios_parametros.Append(",");
                }
            }

            string[] folios = folios_parametros.ToString().Split(',');

            parameters = new SolicitudIntegralMVC.WebServiceReportingServices.ParameterValue[folios.Count()];

            for (int i = 0; i < folios.Count(); ++i)
            {
                parameters[i] = new SolicitudIntegralMVC.WebServiceReportingServices.ParameterValue();
                parameters[i].Name = "SOLICITUD";
                parameters[i].Value = folios[i];
            }
            

            byte[] output;
            string extension, mimeType, encoding;
            SolicitudIntegralMVC.WebServiceReportingServices.Warning[] warnings;
            string[] streamIds;


            var rsClient = new SolicitudIntegralMVC.WebServiceReportingServices.ReportExecutionServiceSoapClient();

            System.Net.NetworkCredential client = new System.Net.NetworkCredential(usuario_dominio_reporting, password_dominio_reporting, dominio_usuario_reporting);

            rsClient.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(usuario_dominio_reporting, password_dominio_reporting, dominio_usuario_reporting);
            rsClient.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

            SolicitudIntegralMVC.ReportingServices.ReportExporter.Export(
                "ReportExecutionServiceSoap" /* name of the WCF endpoint from Web.config */,
                new System.Net.NetworkCredential(usuario_dominio_reporting, password_dominio_reporting, dominio_usuario_reporting),
                reportPath,
                parameters,
                SolicitudIntegralMVC.ReportingServices.ExportFormat.PDF,
                out output,
                out extension,
                out mimeType,
                out encoding,
                out warnings,
                out streamIds
            );

            //Estas instrucciones es la que hacen que esta página mande al cliente el pdf que nos trajo el web services de reporting
            return output;
        }

        #endregion
    }
}

