﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SolicitudIntegralMVC.Infraestructure;
using SolicitudIntegralMVC.Infraestructure.Concrete;

namespace SolicitudIntegralMVC.Controllers
{
    [Authorize]
    [AjaxAuthorizationAttribute]
    [CustomAuthSessionRoleAttribute("VENTANILLA", "USUARIO")]
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            string user_domain = Request.LogonUserIdentity.Name;
            string user =  this.HttpContext.User.Identity.Name.ToString();
            return View();
        }

    }
}

