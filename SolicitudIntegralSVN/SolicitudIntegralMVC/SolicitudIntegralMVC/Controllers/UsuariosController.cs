﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SolicitudIntegralMVC.Infraestructure;
using SolicitudIntegralMVC.Infraestructure.Concrete;
using STPIntegral.Negocios;

namespace SolicitudIntegralMVC.Controllers
{

    [Authorize]
    [AjaxAuthorizationAttribute]
    [CustomAuthSessionRoleAttribute("VENTANILLA", "USUARIO")]
    public class UsuariosController : Controller
    {
        //
        // GET: /Usuarios/

        [HttpGet]
        public ActionResult Index()
        {
            Models.CatalogoUsuarioViewModel usuarioViewModel = ObtenerUsuarioViewModel();

            return View(usuarioViewModel);
        }

        private Models.CatalogoUsuarioViewModel ObtenerUsuarioViewModel()
        {
            SolicitudIntegralMVC.Models.UserLogon usuario_logueado = SolicitudIntegralMVC.Infraestructure.Utilerias.ObtenerUsuarioAutentificado(this.HttpContext);
            GestionGL.Datos.M_USUARIOS usuario = STPIntegral.Negocios.UsuariosController.ObtenerMUsuario(usuario_logueado.IdMUsuarioActual);
            Models.CatalogoUsuarioViewModel usuarioViewModel = new Models.CatalogoUsuarioViewModel()
            {
                IdUsuario = usuario.ID_M_USUARIO,
                NombreCompleto = usuario.NOMBRE,
                NombreUsuario = usuario.FICHA,
                NuevaContrasenia = "",
                UsuarioDominio = usuario.DOMINIO
            };

            return usuarioViewModel;
        }

        [HttpPost]
        public ActionResult CambiarContrasenia(Models.CatalogoUsuarioViewModel usuario)
        {
            string mensaje = "";

            try
            {
                if (String.IsNullOrEmpty(usuario.ContaseniaAnteriorCambioContrasenia))
                {
                    usuario.ContaseniaAnteriorCambioDominio = "";
                }

                STPIntegral.Negocios.UsuariosController.CambiarContrasenia(usuario.IdUsuario, usuario.ContaseniaAnteriorCambioContrasenia, usuario.NuevaContrasenia);
                mensaje = "Se ha modificado correctamente la contraseña del usuario";
            }
            catch (ShowCaseException ex)
            {
                mensaje = String.Format("Ocurrio el siguiente error al actualizar: {0}", ex.Message);
            }
            catch (System.Exception ex)
            {
                mensaje = String.Format("Ocurrio el siguiente error al actualizar: {0}", ex.Message);
            }

            Models.CatalogoUsuarioViewModel usuarioViewModel = ObtenerUsuarioViewModel();

            TempData["Mensaje"] = mensaje;

            return RedirectToAction("Index",usuarioViewModel);
        }

        [HttpPost]
        public ActionResult CambiarUsuarioDominio(Models.CatalogoUsuarioViewModel usuario)
        {
            string mensaje = "";
            try
            {
                if (String.IsNullOrEmpty(usuario.ContaseniaAnteriorCambioDominio))
                {
                    usuario.ContaseniaAnteriorCambioDominio = "";
                }

                STPIntegral.Negocios.UsuariosController.ActualizarNombreUsuarioDominio(usuario.IdUsuario, usuario.ContaseniaAnteriorCambioDominio, usuario.UsuarioDominio);
                mensaje = "Se ha modificado correctamente el dominio del usuario";
            }
            catch (ShowCaseException ex)
            {
                mensaje = String.Format("Ocurrio el siguiente error al actualizar: {0}",ex.Message);
            }
            catch (System.Exception ex)
            {
                mensaje = String.Format("Ocurrio el siguiente error al actualizar: {0}", ex.Message);
            }

            Models.CatalogoUsuarioViewModel usuarioViewModel = ObtenerUsuarioViewModel();

            TempData["Mensaje"] = mensaje;

            return RedirectToAction("Index", usuarioViewModel);
        }
    }
}
