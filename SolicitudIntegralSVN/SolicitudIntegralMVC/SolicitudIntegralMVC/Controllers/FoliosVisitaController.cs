﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using STPIntegral.Negocios;

namespace SolicitudIntegralMVC.Controllers
{
    public class FoliosVisitaController : Controller
    {
        //
        // GET: /FoliosVisita/

        [HttpGet]
        public ActionResult Index()
        {
            STPIntegral.Datos.TabFolioVisitas folio_nuevo = null;
            folio_nuevo = new STPIntegral.Datos.TabFolioVisitas();
            folio_nuevo.FechaInicio = DateTime.MinValue;
            folio_nuevo.FechaFin = DateTime.MinValue;
            return View(folio_nuevo);
        }

        [HttpPost]
        public ActionResult Index(string FolioVisitaData)
        {
            string[] folioVisita = FolioVisitaData.Split('|');
            int IdFolioVisitaConvertido=0;
            DateTime FechaInicioConvertido = DateTime.MinValue;
            DateTime FechaFinConvertido = DateTime.MinValue;
            Int32.TryParse(folioVisita[0], out IdFolioVisitaConvertido);
            DateTime.TryParse(folioVisita[3], out FechaInicioConvertido);
            DateTime.TryParse(folioVisita[4], out FechaFinConvertido);

            STPIntegral.Datos.TabFolioVisitas folioVisitaEditar = null;
            folioVisitaEditar = new STPIntegral.Datos.TabFolioVisitas();
            
            folioVisitaEditar.IdFolioVisita = IdFolioVisitaConvertido;
            folioVisitaEditar.Foliodocumento = folioVisita[1] == null ? "" : folioVisita[1];
            folioVisitaEditar.FolioVisita = folioVisita[2] == null ? "" : folioVisita[2];
            folioVisitaEditar.FechaInicio = FechaInicioConvertido;
            folioVisitaEditar.FechaFin = FechaFinConvertido;
            folioVisitaEditar.RfcCompania = folioVisita[5] == null ? "" : folioVisita[5];
            folioVisitaEditar.NoAcreedorSap = folioVisita[6] == null ? "" : folioVisita[6];
            folioVisitaEditar.NombreCompania = folioVisita[7] == null ? "" : folioVisita[7];
            folioVisitaEditar.FichaSolicitante = folioVisita[8] == null ? "" : folioVisita[8];
            folioVisitaEditar.FichaAutorizador = folioVisita[9] == null ? "" : folioVisita[9];
            folioVisitaEditar.Motivo = folioVisita[10] == null ? "" : folioVisita[10];

            return View(folioVisitaEditar);
        }

        [HttpPost]
        public ActionResult GuardarFolioVisita(STPIntegral.Datos.TabFolioVisitas folioVisita)
        {
            //folioVisita.EstadoFolioVisita = "0000012215421564654";
            //folioVisita.FichaAutorizador = "02145123456";

            //return View("Index", folioVisita);

            SolicitudIntegralMVC.Models.UserLogon usuario_logueado = SolicitudIntegralMVC.Infraestructure.Utilerias.ObtenerUsuarioAutentificado(this.HttpContext);
            ActionResult resultado = null;
            folioVisita.NodoTransporte = usuario_logueado.NodoAtencionActual;
            folioVisita.FichaCaptura = usuario_logueado.NombreUsuario;
            folioVisita.FechaCaptura = DateTime.Now;
            folioVisita.EstadoFolioVisita = "Activo";
            try
            {
                folioVisita = STPIntegral.Negocios.FoliosVisita.GuardarFolio(folioVisita);

                if (Request.IsAjaxRequest())
                {
                    resultado = Json(new { result = folioVisita, mensaje = String.Format("Se ha guardado el siguiente folio de visita: [{0}]", folioVisita.FolioVisita) }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }


            return resultado;
        }


        [HttpGet]
        public ActionResult Listado()
        {

            List<STPIntegral.Datos.TabFolioVisitas> folios_visita = null;

            return View(folios_visita);
        }

        [HttpGet]
        public ActionResult BusquedaFoliosVisita(string fraseBusqueda)
        {
            ActionResult resultado = null;

            try
            {
                List<STPIntegral.Datos.TabFolioVisitas> folios_visita = null;

                using (STPIntegral.Datos.STPIntegralModel stpIntegralModel = new STPIntegral.Datos.STPIntegralModel())
                {
                    folios_visita = stpIntegralModel.TabFolioVisitas.Where(x => x.BorradoLogico == false && x.FolioVisita.Contains(fraseBusqueda)).ToList();
                }

                if (Request.IsAjaxRequest())
                {
                    resultado = Json(new { result = folios_visita }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }

            return resultado;
        }
        
        [HttpGet]
        public ActionResult ConsultaAutorizador(string ficha_autorizador)
        {
            ActionResult resultado = null;

            try
            {
                GestionGL.Datos.C_EMPLEADOS_PEMEX empleado = null;

                using (GestionGL.Datos.GestionGLModel gestionGlModel = new GestionGL.Datos.GestionGLModel())
                {
                    empleado = gestionGlModel.C_EMPLEADOS_PEMEX.Where(x => x.FICHA==ficha_autorizador).FirstOrDefault();
                }

                if (empleado == null)
                {
                    throw new System.Exception("No se encontro ningun autorizador con esa ficha");
                }

                if (Request.IsAjaxRequest())
                {
                    resultado = Json(new { result = empleado }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }



            return resultado;
        }

        public ActionResult ConsultaSolicitante(string ficha_solicitante)
        {
            ActionResult resultado = null;

            try
            {
                GestionGL.Datos.C_EMPLEADOS_PEMEX empleado = null;

                using (GestionGL.Datos.GestionGLModel gestionGlModel = new GestionGL.Datos.GestionGLModel())
                {
                    empleado = gestionGlModel.C_EMPLEADOS_PEMEX.Where(x => x.FICHA==ficha_solicitante).FirstOrDefault();
                }

                if (empleado == null)
                {
                    throw new System.Exception("No se encontro ningun solicitante con esa ficha");
                }

                if (Request.IsAjaxRequest())
                {
                    resultado = Json(new { result = empleado }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }



            return resultado;
        }
    }
}
