﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using SolicitudIntegralMVC.Infraestructure;
using SolicitudIntegralMVC.Infraestructure.Abstract;
using STPIntegral.Negocios;

namespace SolicitudIntegralMVC.Controllers
{
    [AllowAnonymous]
    public class LogOnController : Controller
    {
        //
        // GET: /LogOn/

        public LogOnController()
        {
        }

        public ActionResult Index()
        {
            SolicitudIntegralMVC.Models.UserLogonModel loginModel = new Models.UserLogonModel();

            loginModel.UsuarioDominio = ObtenerUsuarioDominio();

            Session["pre_login_user"] = loginModel;

            return View(loginModel);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult ValidarUsuarioPassword(SolicitudIntegralMVC.Models.UserLogonModel user_login)
        {
            if (STPIntegral.Negocios.Utilerias.ObtenerTipoTrabajador(user_login.UsuarioFormulario.NombreUsuario) == "PEMEX")
            {
                user_login.UsuarioFormulario.NombreUsuario = STPIntegral.Negocios.Utilerias.FormatearFichaRfc(user_login.UsuarioFormulario.NombreUsuario);
            }

            user_login.UsuarioDominio = ObtenerUsuarioDominio();
            user_login.UsuarioFormulario = ValidarUsuarioPassword(user_login.UsuarioFormulario.NombreUsuario, user_login.UsuarioFormulario.Password);

            if (user_login.UsuarioFormulario != null && user_login.UsuarioFormulario.ListadoPermisosSubdivisionesDisponibles != null && user_login.UsuarioFormulario.ListadoPermisosSubdivisionesDisponibles.Count <= 0)
            {
                TempData["mensaje_error"] = "Usuario o Contraseña incorrectos";
            }
            else
            {
                Session["pre_login_user"] = user_login;
            }


            return View("index",user_login);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult LogInUsuarioFormulario(SolicitudIntegralMVC.Models.PermisosSubdivisionesDisponiblesUsuario permiso)
        {
            ActionResult resultado = null;

            if (Session["pre_login_user"] != null)
            {
                SolicitudIntegralMVC.Models.UserLogonModel pre_login_user = Session["pre_login_user"] as SolicitudIntegralMVC.Models.UserLogonModel;

                if (pre_login_user != null && pre_login_user.UsuarioFormulario.ListadoPermisosSubdivisionesDisponibles != null && pre_login_user.UsuarioFormulario.ListadoPermisosSubdivisionesDisponibles.Any(x => x.IdPermisoUa == permiso.IdPermisoUa))
                {
                    FormsAuthentication.SetAuthCookie(pre_login_user.UsuarioFormulario.NombreUsuario, false);

                    SolicitudIntegralMVC.Models.PermisosSubdivisionesDisponiblesUsuario permiso_actual = pre_login_user.UsuarioFormulario.ListadoPermisosSubdivisionesDisponibles.Single(x => x.IdPermisoUa == permiso.IdPermisoUa);

                    pre_login_user.UsuarioFormulario.NodoAtencionActual = String.IsNullOrEmpty(permiso_actual.NodoAtencion) || (!String.IsNullOrEmpty(permiso_actual.NodoAtencion) && permiso_actual.NodoAtencion.Trim() == "") ? "CME" : permiso_actual.NodoAtencion;
                    
                    pre_login_user.UsuarioFormulario.IdPermisoUaActual = permiso_actual.IdPermisoUa;
                    pre_login_user.UsuarioFormulario.IdDUsuarioActual = permiso_actual.IdDUsuario;
                    pre_login_user.UsuarioFormulario.IdMUsuarioActual = permiso_actual.IdMUsuario;

                    pre_login_user.UsuarioFormulario.PermisoUsuarioActual = permiso_actual.Permiso.Trim('[', ']');

                    pre_login_user.UsuarioFormulario.CveActivoActual = permiso_actual.CveActivo;
                    pre_login_user.UsuarioFormulario.CveRegionActual = permiso_actual.CveRegion;
                    pre_login_user.UsuarioFormulario.DescripcionActivoActual = permiso_actual.DescripcionActivo;
                    pre_login_user.UsuarioFormulario.DescripcionRegionActual = permiso_actual.DescripcionRegion;
                    pre_login_user.UsuarioFormulario.SubdivisionActual = permiso_actual.Subdivision;
                    pre_login_user.UsuarioFormulario.DescripcionSubdivisionActual = permiso_actual.DescripcionSubdivision;

                    if (String.IsNullOrEmpty(pre_login_user.UsuarioFormulario.NodoAtencionActual))
                    {
                        pre_login_user.UsuarioFormulario.NodoAtencionActual = "CME";
                    }

                    SolicitudIntegralMVC.Infraestructure.Utilerias.GuardarUsuarioAutentificado(this.HttpContext, pre_login_user.UsuarioFormulario);

                    SolicitudIntegralMVC.Infraestructure.Concrete.FormsAuthProvider authProvider = new Infraestructure.Concrete.FormsAuthProvider();
                    authProvider.Authenticate(pre_login_user.UsuarioFormulario.NombreUsuario, pre_login_user.UsuarioFormulario.Password);
                    
                    resultado = RedirectToAction("Index","Home");
                }
            }
            else
            {
                TempData["mensaje_error"] = "Ocurrio un error con el intento de acceder, por favor intentelo de nuevo";
                resultado = View("index");
            }

            return resultado;
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult LogInUsuarioDominio(SolicitudIntegralMVC.Models.PermisosSubdivisionesDisponiblesUsuario permiso)
        {
            ActionResult resultado = null;

            if (Session["pre_login_user"] != null)
            {
                SolicitudIntegralMVC.Models.UserLogonModel pre_login_user = Session["pre_login_user"] as SolicitudIntegralMVC.Models.UserLogonModel;
                 
                if (pre_login_user != null && pre_login_user.UsuarioDominio.ListadoPermisosSubdivisionesDisponibles != null && pre_login_user.UsuarioDominio.ListadoPermisosSubdivisionesDisponibles.Any(x => x.IdPermisoUa == permiso.IdPermisoUa))
                {
                    FormsAuthentication.SetAuthCookie(pre_login_user.UsuarioDominio.NombreUsuario, false);

                    SolicitudIntegralMVC.Models.PermisosSubdivisionesDisponiblesUsuario permiso_actual = pre_login_user.UsuarioDominio.ListadoPermisosSubdivisionesDisponibles.First(x => x.IdPermisoUa == permiso.IdPermisoUa);

                    pre_login_user.UsuarioFormulario.NodoAtencionActual = String.IsNullOrEmpty(permiso_actual.NodoAtencion) || (!String.IsNullOrEmpty(permiso_actual.NodoAtencion) && permiso_actual.NodoAtencion.Trim() == "") ? "CME" : permiso_actual.NodoAtencion;

                    pre_login_user.UsuarioDominio.IdPermisoUaActual = permiso_actual.IdPermisoUa;
                    pre_login_user.UsuarioFormulario.IdDUsuarioActual = permiso_actual.IdDUsuario;
                    pre_login_user.UsuarioFormulario.IdMUsuarioActual = permiso_actual.IdMUsuario;

                    pre_login_user.UsuarioDominio.PermisoUsuarioActual = permiso_actual.Permiso.Trim('[', ']');

                    pre_login_user.UsuarioDominio.CveActivoActual = permiso_actual.CveActivo;
                    pre_login_user.UsuarioDominio.CveRegionActual = permiso_actual.CveRegion;
                    pre_login_user.UsuarioDominio.DescripcionActivoActual = permiso_actual.DescripcionActivo;
                    pre_login_user.UsuarioDominio.DescripcionRegionActual = permiso_actual.DescripcionRegion;
                    pre_login_user.UsuarioDominio.SubdivisionActual = permiso_actual.Subdivision;
                    pre_login_user.UsuarioDominio.DescripcionSubdivisionActual = permiso_actual.DescripcionSubdivision;
                    pre_login_user.UsuarioDominio.SiglasActivoActual = permiso_actual.SiglasActivo;
                    pre_login_user.UsuarioDominio.SiglasRegionActual = permiso_actual.SiglasRegion;

                    if (String.IsNullOrEmpty(pre_login_user.UsuarioFormulario.NodoAtencionActual))
                    {
                        pre_login_user.UsuarioFormulario.NodoAtencionActual = "CME";
                    }

                    SolicitudIntegralMVC.Infraestructure.Utilerias.GuardarUsuarioAutentificado(this.HttpContext, pre_login_user.UsuarioDominio);

                    SolicitudIntegralMVC.Infraestructure.Concrete.FormsAuthProvider authProvider = new Infraestructure.Concrete.FormsAuthProvider();

                    authProvider.Authenticate(pre_login_user.UsuarioDominio.NombreUsuario, pre_login_user.UsuarioDominio.Password);

                    resultado = RedirectToAction("Index", "Home");
                }
            }
            else
            {
                TempData["mensaje_error"] = "Ocurrio un error con el intento de acceder, por favor intentelo de nuevo";
                resultado = View("index");
            }

            return resultado;
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult RelacionarCuentaUsuarioDominio()
        {
            return PartialView();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult RelacionarCuentaUsuarioDominio(string dominio, string usuario, string password)
        {
            ActionResult resultado = null;

            try
            {
                SolicitudIntegralMVC.Models.UserLogon permiso = ValidarUsuarioPassword(usuario, password);

                if (permiso != null)
                {
                    STPIntegral.Negocios.UsuariosController.ActualizarNombreUsuarioDominio(permiso.IdMUsuarioActual, password, dominio);
                }
                else
                {
                    throw new ShowCaseException("Usuario o contraseña invalidos", null, TypeMessage.warning, "");
                }

                resultado = Json(new { result = "OK", mensaje = "La cuenta del dominio se asigno exitosamente." }, JsonRequestBehavior.AllowGet);
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }

            return resultado;
        }

        [HttpPost]
        public ActionResult LogOut()
        {
            Session.Clear();
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home", new { area = "" });
        }

        [AllowAnonymous]
        public SolicitudIntegralMVC.Models.UserLogon ObtenerUsuarioDominio()
        {
            SolicitudIntegralMVC.Models.UserLogon usuario = null;

            if (!String.IsNullOrEmpty(Request.LogonUserIdentity.Name))
            {
                GestionGL.Datos.M_USUARIOS usuario_gestion_gl = null;

                List<GestionGL.Datos.VW_USUARIOS_PERMISOS_GENERAL> listado_permisos_usuario = null;

                using (GestionGL.Datos.GestionGLModel gestion_gl_model = new GestionGL.Datos.GestionGLModel())
                {
                    usuario_gestion_gl = gestion_gl_model.M_USUARIOS.FirstOrDefault(x => x.DOMINIO == Request.LogonUserIdentity.Name);

                    if (usuario_gestion_gl != null)
                    {
                        listado_permisos_usuario = gestion_gl_model.VW_USUARIOS_PERMISOS_GENERAL.Where(x => x.ID_M_USUARIO == usuario_gestion_gl.ID_M_USUARIO && x.CVE_AREA_SERVICIO == "SOLINT").ToList();
                    }
                }

                List<STPIntegral.Datos.stp_c_nodos> nodos_atencion = null;
                using (STPIntegral.Datos.STPIntegralModel stp_integral_model = new STPIntegral.Datos.STPIntegralModel())
                {
                    nodos_atencion = stp_integral_model.stp_c_nodos.Where(x => x.id_nodo > 0).ToList();
                }

                usuario = new Models.UserLogon(usuario_gestion_gl, listado_permisos_usuario, nodos_atencion);
            }

            return usuario;
        }

        [AllowAnonymous]
        public SolicitudIntegralMVC.Models.UserLogon ValidarUsuarioPassword(string nombre_usuario, string password)
        {
            SolicitudIntegralMVC.Models.UserLogon usuario = null;

            if (!String.IsNullOrEmpty(Request.LogonUserIdentity.Name))
            {
                GestionGL.Datos.M_USUARIOS usuario_gestion_gl = null;
                //List<GestionGL.Datos.VW_D_USUARIOS_GENERAL> listado_d_usuarios_subdivisiones = null;

                List<GestionGL.Datos.VW_USUARIOS_PERMISOS_GENERAL> listado_permisos_usuario = null;

                using (GestionGL.Datos.GestionGLModel gestion_gl_model = new GestionGL.Datos.GestionGLModel())
                {
                    usuario_gestion_gl = gestion_gl_model.M_USUARIOS.FirstOrDefault(x => x.FICHA == nombre_usuario && x.PASS == password);

                    if (usuario_gestion_gl != null)
                    {
                        listado_permisos_usuario = gestion_gl_model.VW_USUARIOS_PERMISOS_GENERAL.Where(x => x.ID_M_USUARIO == usuario_gestion_gl.ID_M_USUARIO && x.CVE_AREA_SERVICIO == "SOLINT" && x.DESCRIPCION_C_PERMISO != "USUARIO").ToList();
                    }
                }

                List<STPIntegral.Datos.stp_c_nodos> nodos_atencion = null;
                using (STPIntegral.Datos.STPIntegralModel stp_integral_model = new STPIntegral.Datos.STPIntegralModel())
                {
                    nodos_atencion = stp_integral_model.stp_c_nodos.Where(x => x.id_nodo > 0).ToList();
                }

                usuario = new Models.UserLogon(usuario_gestion_gl, listado_permisos_usuario, nodos_atencion);
            }

            return usuario;
        }

        [Authorize]
        [AjaxAuthorizationAttribute]
        public ActionResult MenuUsuarioNavegacion()
        {
            SolicitudIntegralMVC.Models.UserLogon usuarioLogueado = null;
            
            if (this.HttpContext.User.Identity.Name != "")
            {
                usuarioLogueado = SolicitudIntegralMVC.Infraestructure.Utilerias.ObtenerUsuarioAutentificado(this.HttpContext);
            }
            else
            {
                Session.Clear();
                FormsAuthentication.SignOut();
            }

            return PartialView("menuUsuarios", usuarioLogueado);
        }

        [Authorize]
        [AjaxAuthorizationAttribute]
        public ActionResult MenuNodosAtencion()
        {
            SolicitudIntegralMVC.Models.UserLogon usuarioLogueado = null;

            if (this.HttpContext.User.Identity.Name != "")
            {
                usuarioLogueado = SolicitudIntegralMVC.Infraestructure.Utilerias.ObtenerUsuarioAutentificado(this.HttpContext);
            }
            else
            {
                Session.Clear();
                FormsAuthentication.SignOut();
            }

            return PartialView("nodosAtencion", usuarioLogueado);
        }

        [Authorize]
        [AjaxAuthorization]
        public ActionResult CambiarPermiso(int id)
        {
            ActionResult resultado;
            
            SolicitudIntegralMVC.Models.UserLogon usuarioLogueado = SolicitudIntegralMVC.Infraestructure.Utilerias.ObtenerUsuarioAutentificado(this.HttpContext);
            SolicitudIntegralMVC.Models.PermisosSubdivisionesDisponiblesUsuario permisos_usuario = usuarioLogueado.ListadoPermisosSubdivisionesDisponibles.FirstOrDefault(x => x.IdPermisoUa == id);

            if (usuarioLogueado != null && permisos_usuario != null)
            {
                usuarioLogueado.NodoAtencionActual = String.IsNullOrEmpty(permisos_usuario.NodoAtencion) ? usuarioLogueado.NodoAtencionActual : permisos_usuario.NodoAtencion;
                
                usuarioLogueado.IdPermisoUaActual = permisos_usuario.IdPermisoUa;
                usuarioLogueado.IdDUsuarioActual = permisos_usuario.IdDUsuario;
                usuarioLogueado.IdMUsuarioActual = permisos_usuario.IdMUsuario;

                usuarioLogueado.PermisoUsuarioActual = permisos_usuario.Permiso.Trim('[', ']');

                usuarioLogueado.CveActivoActual = permisos_usuario.CveActivo;
                usuarioLogueado.CveRegionActual = permisos_usuario.CveRegion;
                usuarioLogueado.DescripcionActivoActual = permisos_usuario.DescripcionActivo;
                usuarioLogueado.DescripcionRegionActual = permisos_usuario.DescripcionRegion;
                usuarioLogueado.SubdivisionActual = permisos_usuario.Subdivision;
                usuarioLogueado.DescripcionSubdivisionActual = permisos_usuario.DescripcionSubdivision;
                usuarioLogueado.SiglasActivoActual = permisos_usuario.SiglasActivo;
                usuarioLogueado.SiglasRegionActual = permisos_usuario.SiglasRegion;

                if (String.IsNullOrEmpty(usuarioLogueado.NodoAtencionActual))
                {
                    usuarioLogueado.NodoAtencionActual = "CME";
                }

                SolicitudIntegralMVC.Infraestructure.Utilerias.GuardarUsuarioAutentificado(this.HttpContext, usuarioLogueado);

                TempData["mensaje"] = String.Format("Se ha cambiado su cuenta de usuario a: {0} - {1} {2}, Nodo Atención: {3}", usuarioLogueado.PermisoUsuarioActual, usuarioLogueado.SubdivisionActual, usuarioLogueado.DescripcionSubdivisionActual, usuarioLogueado.NodoAtencionActual);
            }
            else
            {
                TempData["mensaje_error"] = String.Format("No se pudo cambiar de permiso/subdivision su cuenta de usuario");
            }


            resultado = RedirectToAction("Index","Home");

            return  resultado;
        }

        [Authorize]
        [AjaxAuthorization]
        public ActionResult CambiarNodo(string cve_nodo)
        {
            ActionResult resultado;

            SolicitudIntegralMVC.Models.UserLogon usuarioLogueado = SolicitudIntegralMVC.Infraestructure.Utilerias.ObtenerUsuarioAutentificado(this.HttpContext);

            if(usuarioLogueado != null && !String.IsNullOrEmpty(cve_nodo))
            {
                string nodo_atencion_anterior = usuarioLogueado.NodoAtencionActual;
                usuarioLogueado.NodoAtencionActual = cve_nodo;

                if (String.IsNullOrEmpty(usuarioLogueado.NodoAtencionActual))
                {
                    usuarioLogueado.NodoAtencionActual = "CME";
                }

                TempData["mensaje"] = String.Format("Se ha cambiado el nodo de atención de {0} a {1}",nodo_atencion_anterior, usuarioLogueado.NodoAtencionActual);

                SolicitudIntegralMVC.Infraestructure.Utilerias.GuardarUsuarioAutentificado(HttpContext,usuarioLogueado);
            }
            else
            {
                TempData["mensaje_error"] = String.Format("No se pudo cambiar el nodo de atención su cuenta de usuario");
            }


            resultado = RedirectToAction("Index", "Home");

            return resultado;
        }

        [Authorize]
        [AjaxAuthorization]
        public ActionResult InfoUsuario()
        {
            SolicitudIntegralMVC.Models.UserLogon usuarioLogueado = SolicitudIntegralMVC.Infraestructure.Utilerias.ObtenerUsuarioAutentificado(this.HttpContext);
            return PartialView(usuarioLogueado);
        }

        public static bool CurrentUserIsInRol(string[] rol_usuario, HttpSessionStateBase session)
        {
            bool resultado = false;

            SolicitudIntegralMVC.Models.UserLogon usuarioLogueado = SolicitudIntegralMVC.Infraestructure.Utilerias.ObtenerUsuarioAutentificado(session);

            if (rol_usuario != null && rol_usuario.Count() > 0 && usuarioLogueado != null)
            {
                resultado = rol_usuario.Contains(usuarioLogueado.PermisoUsuarioActual) ? true : false;
            }

            return resultado;
        }
    }
}
