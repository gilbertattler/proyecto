﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SolicitudIntegralMVC.Controllers
{
    public class FirmaElectronicaController : Controller
    {
        //
        // GET: /FirmaElectronica/

        [HttpGet]
        public ActionResult Index(string FolioSolicitud)
        {
            STPIntegral.Datos.TabSolicitudIntegral solicitud_integral = null;

            if (FolioSolicitud != null)
            {
                using (STPIntegral.Datos.STPIntegralModel stpIntegralModel = new STPIntegral.Datos.STPIntegralModel())
                {
                    solicitud_integral = stpIntegralModel.TabSolicitudIntegral.FirstOrDefault(x => x.FolioSolicitudIntegral == FolioSolicitud);
                }
            }


            return View(solicitud_integral);
        }

        [HttpPost]
        public ActionResult CrearFichaCustodia()
        {
            ActionResult resultado = null;

            SUFE.Contenidos.ContenidosSoapClient contenidos = new SUFE.Contenidos.ContenidosSoapClient();
            SUFE.Contenidos.CredencialesHeader credenciales_header = new SUFE.Contenidos.CredencialesHeader();
            SUFE.Contenidos.Contenidos contenido = new SUFE.Contenidos.Contenidos();

            //contenido.Sha256B64;

            contenidos.Agregar(new SUFE.Contenidos.CredencialesHeader() { }, new SUFE.Contenidos.Contenidos() { });

            int i = 0;

            return resultado;
        }

        [HttpGet]
        public ActionResult ObtenerHash()
        {
            ActionResult resultado = null;

            SUFE.Contenidos.ContenidosSoapClient contenidos = new SUFE.Contenidos.ContenidosSoapClient();
            
            SUFE.Contenidos.LlavePrimaria llave_primaria = new SUFE.Contenidos.LlavePrimaria();
            SUFE.Contenidos.CredencialesHeader credenciales = new SUFE.Contenidos.CredencialesHeader();

            credenciales.AppKey = "Test";

            llave_primaria.SistemaOrigen = "Test";
            llave_primaria.TipoDocumento = "Solicitud Integral";
            llave_primaria.FDocumento = String.Format("{0:yyyy-MM-dd}",System.DateTime.Now);
            llave_primaria.IDDocumento = "SI-10062013-0004";

            SUFE.Contenidos.Contenidos[] contenidos_respuesta = null;
            contenidos_respuesta = contenidos.ObtenerHash(credenciales, llave_primaria);

            return resultado;
        }

    }
}
