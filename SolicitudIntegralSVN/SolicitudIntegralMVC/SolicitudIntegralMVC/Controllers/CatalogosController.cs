﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SolicitudIntegralMVC.Infraestructure;
using SolicitudIntegralMVC.Infraestructure.Concrete;
using STPIntegral.Negocios;

namespace SolicitudIntegralMVC.Controllers
{
    [Authorize]
    [AjaxAuthorizationAttribute]
    [CustomAuthSessionRoleAttribute("VENTANILLA", "USUARIO")]
    public class CatalogosController : Controller
    {
        //
        // GET: /Catalogos/

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult BusquedaCompanias()
        {
            return PartialView();
        }


        [HttpGet]
        public ActionResult EmpleadosCompania()
        {
            return PartialView();
        }

        [HttpGet]
        public ActionResult EmpleadosPemex()
        {
            return PartialView();
        }

        [HttpGet]
        public ActionResult ConsultaCompanias(string buscar, string buscarPor)
        {
            ActionResult resultado = null;

            try
            {
                List<GestionGL.Datos.C_ACREEDORES> listado_companias = null;

                using (GestionGL.Datos.GestionGLModel gestionGlModel = new GestionGL.Datos.GestionGLModel())
                {
                    switch (buscarPor)
                    {
                        case "NombreCompania":
                            listado_companias = gestionGlModel.C_ACREEDORES.Where(x => x.DESCRIPCION_ACREEDOR.Contains(buscar)).Take(100).ToList();
                            break;
                        case "Rfc":
                            listado_companias = gestionGlModel.C_ACREEDORES.Where(x => x.RFC_ACREEDOR.Contains(buscar)).Take(100).ToList();
                            break;
                        case "NoAcreedor":
                            listado_companias = gestionGlModel.C_ACREEDORES.Where(x => x.CVE_ACREEDOR.Contains(buscar)).Take(100).ToList();
                            break;
                    }

                    
                }
                
                if (Request.IsAjaxRequest())
                {
                    resultado = Json(new { result = listado_companias }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }

            return resultado;
        }

        [HttpPost]
        public ActionResult GuardarEmpleadoCompania(GestionGL.Datos.C_EMPLEADOS_CIA empleadoCia)
        {
            ActionResult resultado = null;

            try
            {
                empleadoCia = STPIntegral.Negocios.CEmpleadoCia.GuardarEmpleado(empleadoCia);

                if (Request.IsAjaxRequest())
                {
                    resultado = Json(new { result = empleadoCia, mensaje = String.Format("Se han guardado los datos del empleado: [{0}] {1} {2}",empleadoCia.RFC_FM3, empleadoCia.NOMBRES, empleadoCia.APELLIDOS) }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }

            return resultado;
        }

        [HttpPost]
        public ActionResult GuardarEmpleadoPemex(GestionGL.Datos.C_EMPLEADOS_PEMEX empleadoPemex)
        {
            ActionResult resultado = null;

            try
            {
                empleadoPemex = STPIntegral.Negocios.CEmpleadoPemex.GuardarEmpleado(empleadoPemex);

                if (Request.IsAjaxRequest())
                {
                    resultado = Json(new { result = empleadoPemex, mensaje = String.Format("Se han guardado los datos del empleado: [{0}] {1} {2}", empleadoPemex.FICHA, empleadoPemex.NOMBRES, empleadoPemex.APELLIDOS) }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ShowCaseException ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "ApplicationException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.AddHeader("RESPONSE_STATUS", "UnexpectedException");
                TempData["Error"] = ex;
                resultado = Request.IsAjaxRequest() ? (ActionResult)Content(ShowCaseException.SerializeException(ex)) : (ActionResult)RedirectToAction("Index");
            }


            return resultado;
        }
    }
}
