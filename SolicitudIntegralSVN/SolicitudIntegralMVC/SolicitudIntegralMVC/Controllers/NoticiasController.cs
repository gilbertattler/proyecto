﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SolicitudIntegralMVC.Controllers
{
    public class NoticiasController : Controller
    {
        //
        // GET: /Noticias/

        public ActionResult ListadoNoticias()
        {
            IEnumerable<STPIntegral.Datos.stp_c_mensajes> listados_mensajes = STPIntegral.Negocios.MensajesController.ObtenerNoticiasActualesParaSolicitudIntegral();
            return PartialView(listados_mensajes);
        }

    }
}
