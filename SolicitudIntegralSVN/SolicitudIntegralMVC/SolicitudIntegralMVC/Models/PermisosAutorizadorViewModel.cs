﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SolicitudIntegralMVC.Models
{
    public class PermisosAutorizadorViewModel
    {
        public string FichaAutorizador { get; set; }
        public string NombreAutorizador { get; set; }
        public string[] AreasServicio { get; set; }
        public string NoFolioDelegacionFirmas { get; set; }

        public string UrlFirmaDelegacionFirmas
        {
            get
            {
                string resultado = "";

                if (!String.IsNullOrEmpty(NoFolioDelegacionFirmas))
                {
                    resultado = String.Format("{0}{1}", System.Configuration.ConfigurationManager.AppSettings["urlFirmaDelegacionFirmas"], NoFolioDelegacionFirmas);
                }
                else
                {
                    resultado = "";
                }

                return resultado;
            }
        }

        public string UrlDocumentoDelegacionFirmas
        {
            get
            {
                string resultado = "";

                if (!String.IsNullOrEmpty(NoFolioDelegacionFirmas))
                {
                    resultado = String.Format("{0}{1}", System.Configuration.ConfigurationManager.AppSettings["urlDocumentoDelegacionFirmas"], NoFolioDelegacionFirmas);
                }
                else
                {
                    resultado = "";
                }

                return resultado;
            }
        }

        public IEnumerable<GestionGL.Datos.VW_C_D_AUTORIZADORES> PermisosAutorizador { get; set; }
    }
}