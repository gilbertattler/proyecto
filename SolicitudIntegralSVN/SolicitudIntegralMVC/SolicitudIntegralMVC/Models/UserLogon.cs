﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SolicitudIntegralMVC.Models
{
    public class UserLogon
    {
        public string NombreUsuario { get; set; }
        public string NombreUsuarioDominio { get; set; }
        public string Password { get; set; }
        public string NombreCompletoUsuario { get; set; }
        public string CorreoUsuario { get; set; }
        public string TipoUsuario { get; set; }

        public string NodoAtencionActual { get; set; }

        public int IdMUsuarioActual { get; set; }
        public int IdPermisoUaActual { get; set; }
        public int IdDUsuarioActual { get; set; }

        public string PermisoUsuarioActual { get; set; }

        public string SubdivisionActual { get; set; }
        public string DescripcionSubdivisionActual { get; set; }

        public string CveActivoActual { get; set; }
        public string SiglasActivoActual { get; set; }
        public string DescripcionActivoActual { get; set; }
        
        public string CveRegionActual { get; set; }
        public string SiglasRegionActual { get; set; }
        public string DescripcionRegionActual { get; set; }


        public List<PermisosSubdivisionesDisponiblesUsuario> ListadoPermisosSubdivisionesDisponibles { get; set; }
        public List<STPIntegral.Datos.stp_c_nodos> NodosAtencion { get; set; }

        public UserLogon()
        {
            ListadoPermisosSubdivisionesDisponibles = new List<PermisosSubdivisionesDisponiblesUsuario>();
            NodoAtencionActual = "CME";
        }

        public UserLogon(GestionGL.Datos.M_USUARIOS usuario_gestion_gl, List<GestionGL.Datos.VW_USUARIOS_PERMISOS_GENERAL> listado_permisos_usuario, List<STPIntegral.Datos.stp_c_nodos> nodos_atencion)
        {
            if (listado_permisos_usuario != null && listado_permisos_usuario.Count() > 0)
            {
                NombreCompletoUsuario = usuario_gestion_gl.NOMBRE;
                NombreUsuario = usuario_gestion_gl.FICHA;
                CorreoUsuario = usuario_gestion_gl.CORREO;
                NombreUsuarioDominio = usuario_gestion_gl.DOMINIO;
                TipoUsuario = usuario_gestion_gl.CIA == "PEMEX" ? "PEMEX" : "COMPAÑIA";

                NodoAtencionActual = listado_permisos_usuario[0].NODO_D_USUARIO;

                IdMUsuarioActual = listado_permisos_usuario[0].ID_M_USUARIO;
                IdPermisoUaActual = listado_permisos_usuario[0].ID_PERMISOS_UA;
                IdDUsuarioActual = listado_permisos_usuario[0].ID_D_USUARIO;

                PermisoUsuarioActual = listado_permisos_usuario[0].CVE_PERMISO_C_PERMISO;

                SubdivisionActual = listado_permisos_usuario[0].SUBDIVISION;
                DescripcionSubdivisionActual = listado_permisos_usuario[0].DESCRIPCION_SUBDIVISION;

                CveActivoActual = listado_permisos_usuario[0].CVE_ACTIVO;
                CveRegionActual = listado_permisos_usuario[0].CVE_REGION;
                SiglasActivoActual = listado_permisos_usuario[0].SIGLAS_ACTIVO;
                SiglasRegionActual = listado_permisos_usuario[0].SIGLAS_REGION;

                DescripcionActivoActual = listado_permisos_usuario[0].NOMBRE_ACTIVO;
                DescripcionRegionActual = listado_permisos_usuario[0].NOMBRE_REGION;

                ListadoPermisosSubdivisionesDisponibles = new List<PermisosSubdivisionesDisponiblesUsuario>();

                foreach (GestionGL.Datos.VW_USUARIOS_PERMISOS_GENERAL permiso_usuario in listado_permisos_usuario)
                {
                    ListadoPermisosSubdivisionesDisponibles.Add
                    (
                        new Models.PermisosSubdivisionesDisponiblesUsuario()
                        {
                            NodoAtencion = permiso_usuario.NODO_D_USUARIO,
                            IdPermisoUa = permiso_usuario.ID_PERMISOS_UA,
                            IdDUsuario = permiso_usuario.ID_D_USUARIO,
                            IdMUsuario = permiso_usuario.ID_M_USUARIO,
                            CveActivo = permiso_usuario.CVE_ACTIVO,
                            CveRegion = permiso_usuario.CVE_REGION,
                            SiglasActivo = permiso_usuario.SIGLAS_ACTIVO,
                            SiglasRegion = permiso_usuario.SIGLAS_REGION,
                            DescripcionActivo = permiso_usuario.NOMBRE_ACTIVO,
                            DescripcionRegion = permiso_usuario.NOMBRE_REGION,
                            DescripcionSubdivision = permiso_usuario.DESCRIPCION_SUBDIVISION,
                            Permiso = permiso_usuario.CVE_PERMISO_C_PERMISO,
                            Subdivision = permiso_usuario.SUBDIVISION
                        }
                    );
                }

                NodosAtencion = (from x in nodos_atencion orderby x.cve_nodo select x).ToList();
            }

            if (String.IsNullOrEmpty(NodoAtencionActual))
            {
                NodoAtencionActual = "CME";
            }
        }
    }
}