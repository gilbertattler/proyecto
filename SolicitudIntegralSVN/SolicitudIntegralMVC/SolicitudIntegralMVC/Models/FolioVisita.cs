﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SolicitudIntegralMVC.Models
{
    public class FolioVisita
    {
        public string IdFolioVisita { get; set; }
        public string Folio { get; set; }
        public string FichaSolicitante { get; set; }
        public string FichaAutorizador { get; set; }
        public string Motivo { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }
        public string FinchaCaptura { get; set; }
        public DateTime FechaCaptura { get; set; }
        public string NodoTransporte { get; set; }
        public string EstadoFolioVisita { get; set; }
        public string FolioDocumento { get; set; }
        public string RFCCompania { get; set; }
        public string NombreCompania { get; set; }
        public string NoAcreedorSAP { get; set; }
        public bool BorradoLogico { get; set; }
    }
}