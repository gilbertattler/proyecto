﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SolicitudIntegralMVC.Models
{
    public class ItinerariosDiaViewModel
    {
        public string TipoTransporte;
        public string TipoServicio;
        public string NodoAtencion;
        public string TipoSolicitudIntegral;
        public DateTime FechaConsulta;
        public IEnumerable<STPIntegral.Datos.Vw_SoltIntItinerariosCapacidadDia_NOLOCK> ItinerariosCapacidadDia;
    }
}