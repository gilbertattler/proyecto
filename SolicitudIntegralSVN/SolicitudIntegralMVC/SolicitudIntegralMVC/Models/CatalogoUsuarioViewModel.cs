﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SolicitudIntegralMVC.Models
{
    public class CatalogoUsuarioViewModel
    {
        public int IdUsuario { get; set; }
        public string NombreUsuario { get; set; }
        public string NombreCompleto { get; set; }
        public string ContaseniaAnteriorCambioContrasenia { get; set; }
        public string NuevaContrasenia { get; set; }
        public string ContaseniaAnteriorCambioDominio { get; set; }
        public string UsuarioDominio { get; set; }
    }
}