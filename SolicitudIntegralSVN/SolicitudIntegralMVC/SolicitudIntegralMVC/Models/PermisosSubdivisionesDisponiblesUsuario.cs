﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SolicitudIntegralMVC.Models
{
    public class PermisosSubdivisionesDisponiblesUsuario
    {
        public int IdPermisoUa { get; set; }

        public int IdDUsuario { get; set; }
        public int IdMUsuario { get; set; }
        
        public string Permiso { get; set; }

        public string NodoAtencion { get; set; }
        
        public string Subdivision { get; set; }
        public string DescripcionSubdivision { get; set; }

        public string CveActivo { get; set; }
        public string DescripcionActivo { get; set; }
        public string SiglasActivo { get; set; }

        public string CveRegion { get; set; }
        public string DescripcionRegion { get; set; }
        public string SiglasRegion { get; set; }
    }
}