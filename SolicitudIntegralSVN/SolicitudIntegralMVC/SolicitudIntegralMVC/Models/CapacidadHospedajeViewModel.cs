﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SolicitudIntegralMVC.Models
{

    public class CapacidadHospedajePorDia
    {
        public DateTime Fecha;
        public int CapacidadFondoComunDisponible;
        public int CapacidadFolioEspecial;
        public int CapacidadContrato;
        public int CapacidadSubdivision;
        public bool MenorCapacidad;
        
        public int CapacidadTotal
        {
            get
            {
                return CapacidadFondoComunDisponible + CapacidadFolioEspecial + CapacidadContrato + CapacidadSubdivision;
            }
        }

        public CapacidadHospedajePorDia()
        {
            CapacidadFondoComunDisponible = 0;
            CapacidadFolioEspecial = 0;
            CapacidadContrato = 0;
            CapacidadSubdivision = 0;
        }
    }

    public class CapacidadHospedajeViewModel
    {
        public string NombreInstalacionDestino;
        public DateTime FechaSubida;
        public DateTime FechaBajada;
        public string SubdivisionAutorizador;
        public string DescripcionSubdivision;
        public string NoContrato;
        public string FolioEspecial;
        public List<CapacidadHospedajePorDia> ListadoCapacidadesPorDia;

        public int TotalCantidadDisponible
        {
            get
            {
                int resultado = 0;

                if (ListadoCapacidadesPorDia != null && ListadoCapacidadesPorDia.Count > 0)
                {
                    resultado = ListadoCapacidadesPorDia.Min(x => x.CapacidadTotal);
                }
                return resultado;
            }
        }

        public void ProcesarDetallesCapacidadesHospedaje(IEnumerable<STPIntegral.Datos.VW_TabReservacionesCapacidadHospedaje_NOLOCK> reservaciones_hospedaje)
        {
            ListadoCapacidadesPorDia = new List<CapacidadHospedajePorDia>();

            //Primero obtenemos todas las fechas disponibles
            List<DateTime> fechas_capacidades = reservaciones_hospedaje.Select(x => x.FechaCapacidad).Distinct().ToList();
            fechas_capacidades = fechas_capacidades.OrderBy(x => x.Date).ToList();

            List<STPIntegral.Datos.VW_TabReservacionesCapacidadHospedaje_NOLOCK> reservaciones_consulta = null;
            
            foreach (DateTime fecha_consulta in fechas_capacidades)
            {
                reservaciones_consulta = reservaciones_hospedaje.Where(x => x.FechaCapacidad == fecha_consulta).ToList();

                CapacidadHospedajePorDia nueva_capacidad_hospedaje_dia = new CapacidadHospedajePorDia();

                nueva_capacidad_hospedaje_dia.Fecha = fecha_consulta;

                foreach (STPIntegral.Datos.VW_TabReservacionesCapacidadHospedaje_NOLOCK detalle_reservacion in reservaciones_consulta)
                {
                    switch (detalle_reservacion.TipoReservacion)
                    {
                        case "LUGARES COMUNES":
                        case "FONDO COMUN":
                        {
                            nueva_capacidad_hospedaje_dia.CapacidadFondoComunDisponible += detalle_reservacion.TotalDisponibles.Value;
                            break;
                        }
                        case "SUBDIVISION":
                        {
                            nueva_capacidad_hospedaje_dia.CapacidadSubdivision += detalle_reservacion.TotalDisponibles.Value;
                            break;
                        }
                        case "CONTRATO":
                        {
                            nueva_capacidad_hospedaje_dia.CapacidadContrato += detalle_reservacion.TotalDisponibles.Value;
                            break;
                        }
                        case "FOLIO ESPECIAL":
                        {
                            nueva_capacidad_hospedaje_dia.CapacidadFolioEspecial += detalle_reservacion.TotalDisponibles.Value;
                            break;
                        }
                    }
                }
                ListadoCapacidadesPorDia.Add(nueva_capacidad_hospedaje_dia);
            }

            DateTime ultima_fecha_en_bd = fechas_capacidades.Max(x => x.Date);

            while(ultima_fecha_en_bd < FechaBajada)
            {
                ultima_fecha_en_bd = ultima_fecha_en_bd.AddDays(1);
                
                CapacidadHospedajePorDia nueva_capacidad_hospedaje_dia = new CapacidadHospedajePorDia();
                nueva_capacidad_hospedaje_dia.Fecha = ultima_fecha_en_bd;
                ListadoCapacidadesPorDia.Add(nueva_capacidad_hospedaje_dia);
            }

            //Determinando el primer registro menor
            
            if(ListadoCapacidadesPorDia != null && ListadoCapacidadesPorDia.Count > 0)
            {
                int capacidad_menor = ListadoCapacidadesPorDia.Min(x => x.CapacidadTotal);
                CapacidadHospedajePorDia capacidades_menores = ListadoCapacidadesPorDia.First(x => x.CapacidadTotal == capacidad_menor);
                capacidades_menores.MenorCapacidad = true;
            }
        
        }
    }
}