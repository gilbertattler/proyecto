﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SolicitudIntegralMVC.Models
{
    public class CapacidadItinerariosDiaViewModel
    {
        public string NombreItinerario;
        public string CveItinerario;
        public string TipoTransporte;
        public DateTime FechaConsulta;
        public string SubdivisionAutorizador;
        public string DescripcionSubdivision;
        public string NoContrato;
        public string FolioEspecial;
        public string NodoAtencion;
        public int CantidadDisponibleFondoComun;

        public int TotalCantidadDisponible
        {
            get
            {
                int resultado = 0;

                if (ItinerariosCapacidadDetallesDia != null)
                {
                    foreach (CapacidadDetallesItineriosDiaViewModel detalle in ItinerariosCapacidadDetallesDia)
                    {
                        resultado += detalle.CapacidadEspecialDisponible;
                    }
                }

                return resultado;
            }
        }

        public List<CapacidadDetallesItineriosDiaViewModel> ItinerariosCapacidadDetallesDia;

        public void ProcesarDetallesItinerariosEncontrados(IEnumerable<STPIntegral.Datos.Vw_SoltIntItinerariosCapacidadDia_NOLOCK> itinerarios_capacidad_detalles_dia)
        {
            CapacidadDetallesItineriosDiaViewModel registro_nuevo = null;
            
            ItinerariosCapacidadDetallesDia = new List<CapacidadDetallesItineriosDiaViewModel>();

            //Agregando el Registro del Fondo COMUN
            registro_nuevo = new CapacidadDetallesItineriosDiaViewModel();

            registro_nuevo.TipoCapacidadEspecial = "LUGARES COMUNES";
            registro_nuevo.CapacidadEspecialDisponible = CantidadDisponibleFondoComun;
            registro_nuevo.IdentificadorCapacidadEspecial = "";
            registro_nuevo.ObservacionesIdentificadorEspecial = "";
            registro_nuevo.DescripcionIdentificadorEspecial = "";

            ItinerariosCapacidadDetallesDia.Add(registro_nuevo);

            //AGREGANDO LOS DIFERENTES ESPACIOS ENCONTRADOS (CONTRATO, SUBDIVISION, FOLIO ESPECIAL)
            foreach(STPIntegral.Datos.Vw_SoltIntItinerariosCapacidadDia_NOLOCK itinerario_capacidad_detalle in itinerarios_capacidad_detalles_dia)
            {
                registro_nuevo = new CapacidadDetallesItineriosDiaViewModel();

                if(itinerario_capacidad_detalle.ID_C_CONTRATO.HasValue)
                {
                    registro_nuevo.TipoCapacidadEspecial = "CONTRATO";
                    registro_nuevo.CapacidadEspecialDisponible = itinerario_capacidad_detalle.capacidad_disponible.Value;
                    registro_nuevo.IdentificadorCapacidadEspecial = itinerario_capacidad_detalle.CONTRATO;
                    registro_nuevo.ObservacionesIdentificadorEspecial = itinerario_capacidad_detalle.observaciones_dist;
                    registro_nuevo.DescripcionIdentificadorEspecial = "";

                    ItinerariosCapacidadDetallesDia.Add(registro_nuevo);

                    continue;
                }

                if (itinerario_capacidad_detalle.ID_C_SUBDIVISION.HasValue)
                {
                    registro_nuevo.TipoCapacidadEspecial = "SUBDIVISION";
                    registro_nuevo.CapacidadEspecialDisponible = itinerario_capacidad_detalle.capacidad_disponible.Value;
                    registro_nuevo.IdentificadorCapacidadEspecial = itinerario_capacidad_detalle.SUBDIVISION;
                    registro_nuevo.ObservacionesIdentificadorEspecial = itinerario_capacidad_detalle.observaciones_dist;
                    registro_nuevo.DescripcionIdentificadorEspecial = "";

                    ItinerariosCapacidadDetallesDia.Add(registro_nuevo);

                    continue;
                }

                if (!String.IsNullOrEmpty(itinerario_capacidad_detalle.folio_especial))
                {
                    registro_nuevo.TipoCapacidadEspecial = "FOLIO ESPECIAL";
                    registro_nuevo.CapacidadEspecialDisponible = itinerario_capacidad_detalle.capacidad_disponible.Value;
                    registro_nuevo.IdentificadorCapacidadEspecial = itinerario_capacidad_detalle.folio_especial;
                    registro_nuevo.ObservacionesIdentificadorEspecial = itinerario_capacidad_detalle.observaciones_dist;
                    registro_nuevo.DescripcionIdentificadorEspecial = "";

                    ItinerariosCapacidadDetallesDia.Add(registro_nuevo);

                    continue;
                }
            }
        }

    }

    public class CapacidadDetallesItineriosDiaViewModel
    {
        public string TipoCapacidadEspecial;
        public string IdentificadorCapacidadEspecial;
        public string DescripcionIdentificadorEspecial;
        public string ObservacionesIdentificadorEspecial;
        public int CapacidadEspecialDisponible;
    }
}