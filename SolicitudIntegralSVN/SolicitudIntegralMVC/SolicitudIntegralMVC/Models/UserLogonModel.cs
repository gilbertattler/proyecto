﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SolicitudIntegralMVC.Models
{
    public class UserLogonModel
    {
        public UserLogon UsuarioDominio { get; set; }
        public UserLogon UsuarioFormulario { get; set; }

        public UserLogonModel()
        {
            UsuarioDominio = new UserLogon();
            UsuarioFormulario = new UserLogon();
        }
    }
}