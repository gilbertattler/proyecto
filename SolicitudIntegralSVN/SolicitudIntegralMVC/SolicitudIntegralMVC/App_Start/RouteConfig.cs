﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SolicitudIntegralMVC
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("elmah.axd");
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute(name: "FirmaElectronicoCrearDocumento", url: "FirmaElectronica/CrearFichaCustodia", defaults: new { controller = "FirmaElectronica", action = "CrearFichaCustodia" });

            routes.MapRoute(name: "ConsultaAutorizador", url: "FoliosVisita/ConsultaAutorizador/{ficha_autorizador}", defaults: new { controller = "FoliosVisita", action = "ConsultaAutorizador", ficha_autorizador = UrlParameter.Optional });
            routes.MapRoute(name: "FirmaElectronicaIndex", url: "FirmaElectronica/Firmar/{FolioSolicitud}", defaults: new { controller = "FirmaElectronica", action = "Index", FolioSolicitud = UrlParameter.Optional });
            routes.MapRoute(name: "CambioDeNodoAtencion", url: "LogOn/CambiarNodo/{cve_nodo}", defaults: new { controller = "LogOn", action = "CambiarNodo", cve_nodo = UrlParameter.Optional });
            routes.MapRoute(name: "FirmaAutorizador", url: "SolicitudIntegral/FirmaAutorizador/{FichaAutorizador}", defaults: new { controller = "SolicitudIntegral", action = "FirmaAutorizador", FichaAutorizador = UrlParameter.Optional });
            routes.MapRoute(name: "VistaCapacidadHospedajePorRangoFechas", url: "SolicitudIntegral/VistaCapacidadHospedajePorRangoFechas/{fk_id_instalacion_destino}/{fecha_subida}/{fecha_bajada}/{subdivision_autorizador}/{contrato}/{folio_especial}", defaults: new { controller = "SolicitudIntegral", action = "VistaCapacidadHospedajePorRangoFechas", fk_id_instalacion_destino = UrlParameter.Optional, fecha_subida = UrlParameter.Optional, fecha_bajada = UrlParameter.Optional, subdivision_autorizador = UrlParameter.Optional, contrato = UrlParameter.Optional, folio_especial = UrlParameter.Optional });
            routes.MapRoute(name: "VistaDetallesEmpleado", url: "SolicitudIntegral/VistaDetallesEmpleado/{id_detalle_empleado}", defaults: new { controller = "SolicitudIntegral", action = "VistaDetallesEmpleado", id_detalle_empleado = UrlParameter.Optional });
            routes.MapRoute(name: "VistaItinerarioCapacidadDetallePorDia", url: "SolicitudIntegral/VistaItinerarioCapacidadDetallePorDia/{tipo_transporte}/{id_itinerario}/{fecha_servicio}/{subdivision_autorizador}/{contrato}/{folio_especial}", defaults: new { controller = "SolicitudIntegral", action = "VistaItinerarioCapacidadDetallePorDia", tipo_transporte = UrlParameter.Optional, id_itinerario = UrlParameter.Optional, fecha_servicio = UrlParameter.Optional, subdivision_autorizador = UrlParameter.Optional, contrato = UrlParameter.Optional, folio_especial = UrlParameter.Optional });
            routes.MapRoute(name: "VistaPermisosAutorizador", url: "SolicitudIntegral/VistaPermisosAutorizador/{tipo_servicio}/{tipo_transporte}/{ficha_autorizador}/{fecha_subida}/{fecha_bajada}", defaults: new { controller = "SolicitudIntegral", action = "VistaPermisosAutorizador", tipo_servicio = UrlParameter.Optional, tipo_transporte = UrlParameter.Optional, ficha_autorizador = UrlParameter.Optional, fecha_subida = UrlParameter.Optional, fecha_bajada = UrlParameter.Optional });
            routes.MapRoute(name: "VistaItinerariosPorDia", url: "SolicitudIntegral/VistaItinerariosPorDia/{tipo_transporte}/{tipo_servicio}/{fecha_servicio}/{subdivision}/{contrato}/{folio_especial_ida}/{folio_especial_regreso}", defaults: new { controller = "SolicitudIntegral", action = "VistaItinerariosPorDia", tipo_transporte = UrlParameter.Optional, tipo_servicio = UrlParameter.Optional, fecha_servicio = UrlParameter.Optional, subdivision = UrlParameter.Optional, contrato = UrlParameter.Optional, folio_especial_ida = UrlParameter.Optional, folio_especial_regreso = UrlParameter.Optional });
            routes.MapRoute(name: "ImprimirSolicitud", url: "SolicitudIntegral/Imprimir/{folioSolicitudIntegral}", defaults: new { controller = "SolicitudIntegral", action = "ImprimirSolicitudIntegral", folioSolicitudIntegral = UrlParameter.Optional });
            routes.MapRoute(name: "ImprimirBoletos", url: "SolicitudIntegral/ImprimirBoletos/{folioSolicitudIntegral}", defaults: new { controller = "SolicitudIntegral", action = "ImpimirReporteBoletosSolicitudIntegral", folioSolicitudIntegral = UrlParameter.Optional });
            routes.MapRoute(name: "NuevaSolicitud", url: "SolicitudIntegral/Nueva", defaults: new { controller = "SolicitudIntegral", action = "Editar" });
            routes.MapRoute(name: "EditarSolicitud", url: "SolicitudIntegral/Editar/{folioSolicitudIntegral}", defaults: new { controller = "SolicitudIntegral", action = "Editar", folioSolicitudIntegral = UrlParameter.Optional });
            routes.MapRoute(name: "ReprogramarSolicitud", url: "SolicitudIntegral/Reprogramar/{folioSolicitudIntegral}", defaults: new { controller = "SolicitudIntegral", action = "Reprogramar", folioSolicitudIntegral = UrlParameter.Optional });
            routes.MapRoute(name: "DiferirSolicitud", url: "SolicitudIntegral/Diferir/{folioSolicitudIntegral}", defaults: new { controller = "SolicitudIntegral", action = "Diferir", folioSolicitudIntegral = UrlParameter.Optional });

            //routes.MapRoute(name: "FirmaElectronica", url: "FirmaElectronica/{action}/{id}", defaults: new { controller = "FirmaElectronica", action = "Index", id = UrlParameter.Optional });
            routes.MapRoute(name: "Default",url: "{controller}/{action}/{id}",defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional });
        }
    }
}