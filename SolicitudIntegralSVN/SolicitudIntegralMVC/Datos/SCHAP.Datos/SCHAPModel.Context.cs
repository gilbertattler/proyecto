﻿//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SCHAP.Datos
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class SCHAPModel : DbContext
    {
        public SCHAPModel()
            : base("name=SCHAPModel")
        {
            this.Configuration.LazyLoadingEnabled = false;
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<AUTENTIFICACION_USUARIOS> AUTENTIFICACION_USUARIOS { get; set; }
        public DbSet<C_EMBARCACION> C_EMBARCACION { get; set; }
        public DbSet<CAT_AREAS> CAT_AREAS { get; set; }
        public DbSet<CAT_BOTES> CAT_BOTES { get; set; }
        public DbSet<CAT_CABINAS> CAT_CABINAS { get; set; }
        public DbSet<CAT_CAMAS> CAT_CAMAS { get; set; }
        public DbSet<CAT_CATEGORIAS> CAT_CATEGORIAS { get; set; }
        public DbSet<CAT_CATEGORIAS_VIDEOS> CAT_CATEGORIAS_VIDEOS { get; set; }
        public DbSet<CAT_CLASIFICACION_INSTALACIONES> CAT_CLASIFICACION_INSTALACIONES { get; set; }
        public DbSet<CAT_COMPANIAS> CAT_COMPANIAS { get; set; }
        public DbSet<CAT_CONTRATOS_AH> CAT_CONTRATOS_AH { get; set; }
        public DbSet<CAT_CONTRATOS_AH_INSTALACIONES> CAT_CONTRATOS_AH_INSTALACIONES { get; set; }
        public DbSet<CAT_D_RUTAS> CAT_D_RUTAS { get; set; }
        public DbSet<CAT_DESTINO_DIA> CAT_DESTINO_DIA { get; set; }
        public DbSet<CAT_DETALLES_CONTRATOS_AH> CAT_DETALLES_CONTRATOS_AH { get; set; }
        public DbSet<CAT_INSUMOS> CAT_INSUMOS { get; set; }
        public DbSet<CAT_M_DESTINO_DIA> CAT_M_DESTINO_DIA { get; set; }
        public DbSet<CAT_M_RUTAS> CAT_M_RUTAS { get; set; }
        public DbSet<CAT_MARCAS> CAT_MARCAS { get; set; }
        public DbSet<CAT_MATERIAL> CAT_MATERIAL { get; set; }
        public DbSet<CAT_OPERACION> CAT_OPERACION { get; set; }
        public DbSet<CAT_PARTIDAS_CONTRATOS> CAT_PARTIDAS_CONTRATOS { get; set; }
        public DbSet<CAT_PUNTOSREUNION> CAT_PUNTOSREUNION { get; set; }
        public DbSet<CAT_RAZONES> CAT_RAZONES { get; set; }
        public DbSet<CAT_SUBCATEGORIAS> CAT_SUBCATEGORIAS { get; set; }
        public DbSet<CAT_SUPERVISORES_CONTRATOS_AH> CAT_SUPERVISORES_CONTRATOS_AH { get; set; }
        public DbSet<CAT_TURNOS> CAT_TURNOS { get; set; }
        public DbSet<CAT_UNIDADES_MEDIDA> CAT_UNIDADES_MEDIDA { get; set; }
        public DbSet<CAT_VIDEOS_AYUDA> CAT_VIDEOS_AYUDA { get; set; }
        public DbSet<CONTRATOS> CONTRATOS { get; set; }
        public DbSet<CP_OCUPA_CARGA> CP_OCUPA_CARGA { get; set; }
        public DbSet<CTRL_AVISOS> CTRL_AVISOS { get; set; }
        public DbSet<CUSTODIOS> CUSTODIOS { get; set; }
        public DbSet<D_CONTROL_ACT> D_CONTROL_ACT { get; set; }
        public DbSet<D_SOLICITUDES> D_SOLICITUDES { get; set; }
        public DbSet<D_SOLICITUDES_HIST> D_SOLICITUDES_HIST { get; set; }
        public DbSet<dtproperties> dtproperties { get; set; }
        public DbSet<ENTREGA_SOL> ENTREGA_SOL { get; set; }
        public DbSet<G_CONTROL_ACT> G_CONTROL_ACT { get; set; }
        public DbSet<HIST_USUARIO> HIST_USUARIO { get; set; }
        public DbSet<INS_CUSTODIO> INS_CUSTODIO { get; set; }
        public DbSet<INSTALACIONES_CONTRATOS> INSTALACIONES_CONTRATOS { get; set; }
        public DbSet<M_PLATAFORMA> M_PLATAFORMA { get; set; }
        public DbSet<M_SOLICITUDES> M_SOLICITUDES { get; set; }
        public DbSet<M_SOLICITUDES_HIST> M_SOLICITUDES_HIST { get; set; }
        public DbSet<ORDENES_SERVICIO> ORDENES_SERVICIO { get; set; }
        public DbSet<PARTIDAS> PARTIDAS { get; set; }
        public DbSet<PREH_AUDITORIA_DIARIA> PREH_AUDITORIA_DIARIA { get; set; }
        public DbSet<PREH_AUDITORIA_DIARIA_REGION> PREH_AUDITORIA_DIARIA_REGION { get; set; }
        public DbSet<PREH_CATALOGO_CENTRO_DEPTO> PREH_CATALOGO_CENTRO_DEPTO { get; set; }
        public DbSet<PREH_CONFIGURACIONES> PREH_CONFIGURACIONES { get; set; }
        public DbSet<PREH_CORTES_OCUPACIONALES> PREH_CORTES_OCUPACIONALES { get; set; }
        public DbSet<PREH_FOTOGRAFIAS_OCUPACIONAL> PREH_FOTOGRAFIAS_OCUPACIONAL { get; set; }
        public DbSet<PREH_FOTOGRAFIAS_SITUACION_OCUPACIONAL> PREH_FOTOGRAFIAS_SITUACION_OCUPACIONAL { get; set; }
        public DbSet<PREH_INSTALACIONES_MARINAS> PREH_INSTALACIONES_MARINAS { get; set; }
        public DbSet<PREH_LANCHAS> PREH_LANCHAS { get; set; }
        public DbSet<PREH_MOVIMIENTOS_TRABAJADOR> PREH_MOVIMIENTOS_TRABAJADOR { get; set; }
        public DbSet<PREH_OCUPANTES_EMBARCACIONES_CONTRATOS_INDIRECTOS> PREH_OCUPANTES_EMBARCACIONES_CONTRATOS_INDIRECTOS { get; set; }
        public DbSet<PREH_OCUPANTES_EMBARCACIONES_GL> PREH_OCUPANTES_EMBARCACIONES_GL { get; set; }
        public DbSet<PREH_PASAJEROS> PREH_PASAJEROS { get; set; }
        public DbSet<PREH_PLANTILLAS_RECIBOS_VIATICOS> PREH_PLANTILLAS_RECIBOS_VIATICOS { get; set; }
        public DbSet<PREH_PRE_RECIBOS_VIATICOS> PREH_PRE_RECIBOS_VIATICOS { get; set; }
        public DbSet<PREH_PROGRAMACION_EMBARCACIONES> PREH_PROGRAMACION_EMBARCACIONES { get; set; }
        public DbSet<PREH_RECIBOS_VIATICOS> PREH_RECIBOS_VIATICOS { get; set; }
        public DbSet<PREH_RUTAS_PROGRAMACION> PREH_RUTAS_PROGRAMACION { get; set; }
        public DbSet<PREH_SEGUIMIENTO_CONTINGENCIAS> PREH_SEGUIMIENTO_CONTINGENCIAS { get; set; }
        public DbSet<PREH_SEGURIDAD_VC> PREH_SEGURIDAD_VC { get; set; }
        public DbSet<PREH_SUBCGESTOR> PREH_SUBCGESTOR { get; set; }
        public DbSet<PREH_TITULOS_ORDEN_REPORTES> PREH_TITULOS_ORDEN_REPORTES { get; set; }
        public DbSet<PREH_VERSION> PREH_VERSION { get; set; }
        public DbSet<REGISTRA_CONTROL> REGISTRA_CONTROL { get; set; }
        public DbSet<RES_CAT_INSUMOS> RES_CAT_INSUMOS { get; set; }
        public DbSet<RUTAS> RUTAS { get; set; }
        public DbSet<SOLHOSP_ANEXO> SOLHOSP_ANEXO { get; set; }
        public DbSet<SOLHOSP_AUDITORIA> SOLHOSP_AUDITORIA { get; set; }
        public DbSet<SOLHOSP_D_GUARDIAS> SOLHOSP_D_GUARDIAS { get; set; }
        public DbSet<SOLHOSP_DIRECCIONAMIENTO_LOCAL> SOLHOSP_DIRECCIONAMIENTO_LOCAL { get; set; }
        public DbSet<SOLHOSP_EMPLE_PEMEX> SOLHOSP_EMPLE_PEMEX { get; set; }
        public DbSet<SOLHOSP_HUESPEDES> SOLHOSP_HUESPEDES { get; set; }
        public DbSet<SOLHOSP_M_GUARDIAS> SOLHOSP_M_GUARDIAS { get; set; }
        public DbSet<SOLHOSP_PLATAFORMA_X> SOLHOSP_PLATAFORMA_X { get; set; }
        public DbSet<SOLHOSP_PLATAFORMAS_ACTIVAS> SOLHOSP_PLATAFORMAS_ACTIVAS { get; set; }
        public DbSet<SOLHOSP_SOLICITUD> SOLHOSP_SOLICITUD { get; set; }
        public DbSet<SOLHOSP_SUBDIVISION_INTEGRAL> SOLHOSP_SUBDIVISION_INTEGRAL { get; set; }
        public DbSet<SOLICITUDES_HISTORICAS> SOLICITUDES_HISTORICAS { get; set; }
        public DbSet<SOLICITUDES_HISTORICAS2> SOLICITUDES_HISTORICAS2 { get; set; }
        public DbSet<sysdiagrams> sysdiagrams { get; set; }
        public DbSet<TAB_AUDITORIA_SCHAP> TAB_AUDITORIA_SCHAP { get; set; }
        public DbSet<TAB_COMIDAS> TAB_COMIDAS { get; set; }
        public DbSet<TAB_CONFIGURACION_PARTIDAS_CONTRATOS_COSTEO> TAB_CONFIGURACION_PARTIDAS_CONTRATOS_COSTEO { get; set; }
        public DbSet<TAB_CONFIGURACIONES> TAB_CONFIGURACIONES { get; set; }
        public DbSet<TAB_CORTE_DIARIO> TAB_CORTE_DIARIO { get; set; }
        public DbSet<TAB_CORTE_DIARIO_SOLICITUDES> TAB_CORTE_DIARIO_SOLICITUDES { get; set; }
        public DbSet<TAB_DETALLES_FORMULAS_COSTEO> TAB_DETALLES_FORMULAS_COSTEO { get; set; }
        public DbSet<TAB_FOLIOS_FORANEOS> TAB_FOLIOS_FORANEOS { get; set; }
        public DbSet<TAB_FOLIOS_VISITA> TAB_FOLIOS_VISITA { get; set; }
        public DbSet<TAB_FORMULAS_COSTEO> TAB_FORMULAS_COSTEO { get; set; }
        public DbSet<TAB_INSTALACIONES_FORMULAS_COSTOS> TAB_INSTALACIONES_FORMULAS_COSTOS { get; set; }
        public DbSet<TAB_MAPEOS_SIGLAS_TRANSPORTE> TAB_MAPEOS_SIGLAS_TRANSPORTE { get; set; }
        public DbSet<TAB_MAPEOS_SOLICITUDES_COMPARTIDAS> TAB_MAPEOS_SOLICITUDES_COMPARTIDAS { get; set; }
        public DbSet<TAB_MAPEOS_SOLICITUDES_HOSPEDAJE> TAB_MAPEOS_SOLICITUDES_HOSPEDAJE { get; set; }
        public DbSet<TAB_NOTICIAS> TAB_NOTICIAS { get; set; }
        public DbSet<TAB_OCUPANTES> TAB_OCUPANTES { get; set; }
        public DbSet<TAB_OCUPANTES_HISTORICO> TAB_OCUPANTES_HISTORICO { get; set; }
        public DbSet<TAB_PARAMETROS_COSTEO_CORTE_DIARIO> TAB_PARAMETROS_COSTEO_CORTE_DIARIO { get; set; }
        public DbSet<TAB_PARAMETROS_FORMULA_CORTE_DIARIO> TAB_PARAMETROS_FORMULA_CORTE_DIARIO { get; set; }
        public DbSet<TAB_PERMISOS> TAB_PERMISOS { get; set; }
        public DbSet<TAB_PERMISOS_CAT_CLASIFICACION_INSTALACIONES_GESTION_GL> TAB_PERMISOS_CAT_CLASIFICACION_INSTALACIONES_GESTION_GL { get; set; }
        public DbSet<TAB_PERMISOS_GESTION_GL> TAB_PERMISOS_GESTION_GL { get; set; }
        public DbSet<TAB_PERMISOS_PREH_INSTALACIONES_MARINAS> TAB_PERMISOS_PREH_INSTALACIONES_MARINAS { get; set; }
        public DbSet<TAB_PERMISOS_PREH_INSTALACIONES_MARINAS_GESTION_GL> TAB_PERMISOS_PREH_INSTALACIONES_MARINAS_GESTION_GL { get; set; }
        public DbSet<TAB_PERMISOS_TEMP> TAB_PERMISOS_TEMP { get; set; }
        public DbSet<TAB_PERNOCTAS> TAB_PERNOCTAS { get; set; }
        public DbSet<TAB_PLATAFORMAS_PRUEBA> TAB_PLATAFORMAS_PRUEBA { get; set; }
        public DbSet<TAB_SIADMAN> TAB_SIADMAN { get; set; }
        public DbSet<TAB_USUARIOS_FORANEOS> TAB_USUARIOS_FORANEOS { get; set; }
        public DbSet<TAB_VIAJES> TAB_VIAJES { get; set; }
        public DbSet<TEMP_OCUPANTES_JUPITER> TEMP_OCUPANTES_JUPITER { get; set; }
        public DbSet<V_D_USUARIOS_AH_CGESTOR_SUBDIVISION_Temp> V_D_USUARIOS_AH_CGESTOR_SUBDIVISION_Temp { get; set; }
        public DbSet<CP_CARGA> CP_CARGA { get; set; }
        public DbSet<EVACUACION> EVACUACION { get; set; }
        public DbSet<GESTION_GL_V_PREH_INSTALACIONES_MARINAS> GESTION_GL_V_PREH_INSTALACIONES_MARINAS { get; set; }
        public DbSet<GESTION_GL_VW_INSTALACIONES_GENERAL> GESTION_GL_VW_INSTALACIONES_GENERAL { get; set; }
        public DbSet<INSTALACIONES_SUNPEP> INSTALACIONES_SUNPEP { get; set; }
        public DbSet<PREH_AUDITORIA_DIARIA_F> PREH_AUDITORIA_DIARIA_F { get; set; }
        public DbSet<PREH_INSTALACIONES_MARINAS_F> PREH_INSTALACIONES_MARINAS_F { get; set; }
        public DbSet<PREH_OPTIMIZADOR> PREH_OPTIMIZADOR { get; set; }
        public DbSet<PREH_RPT_TOTAL_PERS_COSTA_AFUERA> PREH_RPT_TOTAL_PERS_COSTA_AFUERA { get; set; }
        public DbSet<PREH_RPT_TOTAL_PERS_COSTA_AFUERA_F> PREH_RPT_TOTAL_PERS_COSTA_AFUERA_F { get; set; }
        public DbSet<PREH_RPT_TOTAL_PERS_COSTA_AFUERA_P> PREH_RPT_TOTAL_PERS_COSTA_AFUERA_P { get; set; }
        public DbSet<PREH_V_CONTINGENCIA_PLANTILLA> PREH_V_CONTINGENCIA_PLANTILLA { get; set; }
        public DbSet<PREH_V_EMBARCACIONES_EVACUA_CONSULTA_SIADMAN> PREH_V_EMBARCACIONES_EVACUA_CONSULTA_SIADMAN { get; set; }
        public DbSet<PREH_V_FOTO_INTS_MOVI> PREH_V_FOTO_INTS_MOVI { get; set; }
        public DbSet<PREH_V_FOTO_VERSION_INSTALACION> PREH_V_FOTO_VERSION_INSTALACION { get; set; }
        public DbSet<PREH_V_INS_VOCU> PREH_V_INS_VOCU { get; set; }
        public DbSet<PREH_V_INS_VOCU_F> PREH_V_INS_VOCU_F { get; set; }
        public DbSet<PREH_V_INS_VOCU_FASES> PREH_V_INS_VOCU_FASES { get; set; }
        public DbSet<PREH_V_INS_VOCU_FASES_F> PREH_V_INS_VOCU_FASES_F { get; set; }
        public DbSet<PREH_V_INS_VOCU_FASES_P> PREH_V_INS_VOCU_FASES_P { get; set; }
        public DbSet<PREH_V_INST_MARINAS_AUDIT> PREH_V_INST_MARINAS_AUDIT { get; set; }
        public DbSet<PREH_V_INSTALACIONES_CORTES> PREH_V_INSTALACIONES_CORTES { get; set; }
        public DbSet<PREH_V_INSTALACIONES_MARINAS_UBIGMI> PREH_V_INSTALACIONES_MARINAS_UBIGMI { get; set; }
        public DbSet<PREH_V_INSTALACIONES_MARINAS_UBIGMI_F> PREH_V_INSTALACIONES_MARINAS_UBIGMI_F { get; set; }
        public DbSet<PREH_V_MOVIMIENTOS_POR_FICHA> PREH_V_MOVIMIENTOS_POR_FICHA { get; set; }
        public DbSet<PREH_V_OCU_BARCOS_OTROS_BARCOS> PREH_V_OCU_BARCOS_OTROS_BARCOS { get; set; }
        public DbSet<PREH_V_OCU_BARCOS_OTROS_BARCOS_F> PREH_V_OCU_BARCOS_OTROS_BARCOS_F { get; set; }
        public DbSet<PREH_V_OCU_BARCOS_SIDOE> PREH_V_OCU_BARCOS_SIDOE { get; set; }
        public DbSet<PREH_V_OCU_BARCOS_SIDOE_F> PREH_V_OCU_BARCOS_SIDOE_F { get; set; }
        public DbSet<PREH_V_OCU_BARCOS_SIN_SIDOE> PREH_V_OCU_BARCOS_SIN_SIDOE { get; set; }
        public DbSet<PREH_V_OCU_BARCOS_SIN_SIDOE_F> PREH_V_OCU_BARCOS_SIN_SIDOE_F { get; set; }
        public DbSet<PREH_V_OCU_TODOS_BARCOS> PREH_V_OCU_TODOS_BARCOS { get; set; }
        public DbSet<PREH_V_OCU_TODOS_BARCOS_F> PREH_V_OCU_TODOS_BARCOS_F { get; set; }
        public DbSet<PREH_V_OCUPANTES_EMBARCACIONES_CONTRATOS_INDIRECTOS> PREH_V_OCUPANTES_EMBARCACIONES_CONTRATOS_INDIRECTOS { get; set; }
        public DbSet<PREH_V_OCUPANTES_EMBARCACIONES_GL> PREH_V_OCUPANTES_EMBARCACIONES_GL { get; set; }
        public DbSet<PREH_V_PASAJEROS> PREH_V_PASAJEROS { get; set; }
        public DbSet<PREH_V_PERSONAL_A_EVACUAR_POR_LANCHA> PREH_V_PERSONAL_A_EVACUAR_POR_LANCHA { get; set; }
        public DbSet<PREH_V_PERSONAL_EVACUAR_POR_LANCHA_INSTALACIONES> PREH_V_PERSONAL_EVACUAR_POR_LANCHA_INSTALACIONES { get; set; }
        public DbSet<PREH_V_PERSONAL_EVACUAR_POR_LANCHA_INSTALACIONES_REAL> PREH_V_PERSONAL_EVACUAR_POR_LANCHA_INSTALACIONES_REAL { get; set; }
        public DbSet<PREH_V_PROGRAMACION_EMBARCACIONES> PREH_V_PROGRAMACION_EMBARCACIONES { get; set; }
        public DbSet<PREH_V_PROGRAMACION_RUTAS> PREH_V_PROGRAMACION_RUTAS { get; set; }
        public DbSet<PREH_V_REPORTE_BARCOS> PREH_V_REPORTE_BARCOS { get; set; }
        public DbSet<PREH_V_REPORTE_BARCOS_F> PREH_V_REPORTE_BARCOS_F { get; set; }
        public DbSet<PREH_V_REPORTES_OCUPANTES_EMBARCACIONES> PREH_V_REPORTES_OCUPANTES_EMBARCACIONES { get; set; }
        public DbSet<PREH_V_REPORTES_OCUPANTES_EMBARCACIONES_CONTRATOS_INDIRECTOS> PREH_V_REPORTES_OCUPANTES_EMBARCACIONES_CONTRATOS_INDIRECTOS { get; set; }
        public DbSet<PREH_V_REPORTES_OCUPANTES_PLATAFORMAS> PREH_V_REPORTES_OCUPANTES_PLATAFORMAS { get; set; }
        public DbSet<PREH_V_REPORTES_TOTAL_OCUPANTES_EMBARCACIONES_CONTRATOS_INDIRECTOS> PREH_V_REPORTES_TOTAL_OCUPANTES_EMBARCACIONES_CONTRATOS_INDIRECTOS { get; set; }
        public DbSet<PREH_V_REPORTES_TOTAL_OCUPANTES_EMBARCACIONES_PLATAFORMAS> PREH_V_REPORTES_TOTAL_OCUPANTES_EMBARCACIONES_PLATAFORMAS { get; set; }
        public DbSet<PREH_V_RPT_OCU_PLAT_ETA> PREH_V_RPT_OCU_PLAT_ETA { get; set; }
        public DbSet<PREH_V_RPT_OCU_PLAT_ETA_F> PREH_V_RPT_OCU_PLAT_ETA_F { get; set; }
        public DbSet<PREH_V_RPT_OCU_PLAT_ETA_P> PREH_V_RPT_OCU_PLAT_ETA_P { get; set; }
        public DbSet<PREH_V_SCHAP_CP_OCUPA_CARGA> PREH_V_SCHAP_CP_OCUPA_CARGA { get; set; }
        public DbSet<PREH_V_SCHAP_TAB_OCUPANTES> PREH_V_SCHAP_TAB_OCUPANTES { get; set; }
        public DbSet<PREH_V_SITUACION_OCUPACIONAL> PREH_V_SITUACION_OCUPACIONAL { get; set; }
        public DbSet<PREH_V_SITUACION_OCUPACIONAL_F> PREH_V_SITUACION_OCUPACIONAL_F { get; set; }
        public DbSet<PREH_V_SITUACION_OCUPACIONAL_FOTO> PREH_V_SITUACION_OCUPACIONAL_FOTO { get; set; }
        public DbSet<PREH_V_TAB_OCUPANTES_NIVEL> PREH_V_TAB_OCUPANTES_NIVEL { get; set; }
        public DbSet<PREH_V_TOTAL_PERSONAL_A_EVACUAR_POR_LANCHA> PREH_V_TOTAL_PERSONAL_A_EVACUAR_POR_LANCHA { get; set; }
        public DbSet<PREH_V_VCONPLA_VVIACGE> PREH_V_VCONPLA_VVIACGE { get; set; }
        public DbSet<PREH_V_VIATICOS_CGESTOR> PREH_V_VIATICOS_CGESTOR { get; set; }
        public DbSet<RH_D_RECIBOS_VIATICOS> RH_D_RECIBOS_VIATICOS { get; set; }
        public DbSet<RH_INSTALACIONES> RH_INSTALACIONES { get; set; }
        public DbSet<RH_M_RECIBOS_VIATICOS> RH_M_RECIBOS_VIATICOS { get; set; }
        public DbSet<RH_SIT_OCUPACIONAL> RH_SIT_OCUPACIONAL { get; set; }
        public DbSet<RH_SIT_OCUPACIONAL_FOTOGRAFIA> RH_SIT_OCUPACIONAL_FOTOGRAFIA { get; set; }
        public DbSet<SOLHOSP_ACTIVO> SOLHOSP_ACTIVO { get; set; }
        public DbSet<SOLHOSP_PLATAFORMAS> SOLHOSP_PLATAFORMAS { get; set; }
        public DbSet<SOLHOSP_PLATAFORMAS_X> SOLHOSP_PLATAFORMAS_X { get; set; }
        public DbSet<SOLHOSP_REGION> SOLHOSP_REGION { get; set; }
        public DbSet<SOLHOSP_SUBDI_INTEGRAL> SOLHOSP_SUBDI_INTEGRAL { get; set; }
        public DbSet<SOLHOSP_SUBDIVISIONES> SOLHOSP_SUBDIVISIONES { get; set; }
        public DbSet<SOLHOSP_USUARIOS> SOLHOSP_USUARIOS { get; set; }
        public DbSet<SOLHOSP_V_Folios> SOLHOSP_V_Folios { get; set; }
        public DbSet<SOLHOSP_V_GUARDIAS> SOLHOSP_V_GUARDIAS { get; set; }
        public DbSet<SOLHOSP_V_M_Guardias> SOLHOSP_V_M_Guardias { get; set; }
        public DbSet<SOLHOSP_V_SOLICITUD_HUESPEDES> SOLHOSP_V_SOLICITUD_HUESPEDES { get; set; }
        public DbSet<SOLHOSP_V_SOLICITUD_HUESPEDES_CARGA_FLOTELES> SOLHOSP_V_SOLICITUD_HUESPEDES_CARGA_FLOTELES { get; set; }
        public DbSet<SOLHOSP_V_SOLICITUDES> SOLHOSP_V_SOLICITUDES { get; set; }
        public DbSet<SOLHOSP_V_SOLICITUDES_INSTALACIONES> SOLHOSP_V_SOLICITUDES_INSTALACIONES { get; set; }
        public DbSet<SUB_CGESTOR> SUB_CGESTOR { get; set; }
        public DbSet<TAB_SIADMAN_F> TAB_SIADMAN_F { get; set; }
        public DbSet<V_AUDITORIA_SCHAP> V_AUDITORIA_SCHAP { get; set; }
        public DbSet<V_BOT_PUN> V_BOT_PUN { get; set; }
        public DbSet<V_CAB_CAM> V_CAB_CAM { get; set; }
        public DbSet<V_CAB_CAM_OCU> V_CAB_CAM_OCU { get; set; }
        public DbSet<V_CAM_BOT> V_CAM_BOT { get; set; }
        public DbSet<V_CAPACIDAD_OCUPACION_BOTES> V_CAPACIDAD_OCUPACION_BOTES { get; set; }
        public DbSet<V_CONTRATOS_AH> V_CONTRATOS_AH { get; set; }
        public DbSet<V_CONTRATOS_AH_SUPERVISORES> V_CONTRATOS_AH_SUPERVISORES { get; set; }
        public DbSet<V_D_USUARIOS_AH_CGESTOR_SUBDIVISION> V_D_USUARIOS_AH_CGESTOR_SUBDIVISION { get; set; }
        public DbSet<V_DETALLES_CONTRATOS_AH> V_DETALLES_CONTRATOS_AH { get; set; }
        public DbSet<V_DIRECCIONAMIENTOS_FICHA_AUTORIZADOR_CENTRO_GESTOR> V_DIRECCIONAMIENTOS_FICHA_AUTORIZADOR_CENTRO_GESTOR { get; set; }
        public DbSet<V_DIRECCIONAMIENTOS_FICHA_SOLICITANTE_AUTORIZADOR_CENTRO_GESTOR> V_DIRECCIONAMIENTOS_FICHA_SOLICITANTE_AUTORIZADOR_CENTRO_GESTOR { get; set; }
        public DbSet<V_DIRECCIONAMIENTOS_FICHA_SOLICITANTE_CENTRO_GESTOR> V_DIRECCIONAMIENTOS_FICHA_SOLICITANTE_CENTRO_GESTOR { get; set; }
        public DbSet<V_FOLFOR_USUFOR_OCU> V_FOLFOR_USUFOR_OCU { get; set; }
        public DbSet<V_M_PLATAFORMA> V_M_PLATAFORMA { get; set; }
        public DbSet<V_MAPEOS_SIGLAS_TRANSPORTE> V_MAPEOS_SIGLAS_TRANSPORTE { get; set; }
        public DbSet<V_MAPEOS_SOLICITUDES_COMPARTIDAS> V_MAPEOS_SOLICITUDES_COMPARTIDAS { get; set; }
        public DbSet<V_MOVIMIENTOS_ENTRADA_SALIDA> V_MOVIMIENTOS_ENTRADA_SALIDA { get; set; }
        public DbSet<V_NOTICIAS> V_NOTICIAS { get; set; }
        public DbSet<V_OCU_CAB_CAM_BOT> V_OCU_CAB_CAM_BOT { get; set; }
        public DbSet<V_PERSONAL_A_BORDO_GMI> V_PERSONAL_A_BORDO_GMI { get; set; }
        public DbSet<V_PERSONAL_A_BORDO_GMI_SOLICITANTE> V_PERSONAL_A_BORDO_GMI_SOLICITANTE { get; set; }
        public DbSet<V_PERSONAL_A_BORDO_GMI_SOLICITANTE_AUTORIZADOR> V_PERSONAL_A_BORDO_GMI_SOLICITANTE_AUTORIZADOR { get; set; }
        public DbSet<V_PREH_EVACUACION_OPTIMIZADOR> V_PREH_EVACUACION_OPTIMIZADOR { get; set; }
        public DbSet<V_RESUMEN_CORTE_SOLICITUDES_ALIMENTOS> V_RESUMEN_CORTE_SOLICITUDES_ALIMENTOS { get; set; }
        public DbSet<V_RESUMEN_CORTE_SOLICITUDES_PERNOCTAS> V_RESUMEN_CORTE_SOLICITUDES_PERNOCTAS { get; set; }
        public DbSet<V_RH_HUESPED_FASE> V_RH_HUESPED_FASE { get; set; }
        public DbSet<V_SOL_AEREO> V_SOL_AEREO { get; set; }
        public DbSet<V_SOL_AEREO_T> V_SOL_AEREO_T { get; set; }
        public DbSet<V_SOL_MARIT_T> V_SOL_MARIT_T { get; set; }
        public DbSet<V_SOLICITUDES_FOLIOS_TRANSPORTE> V_SOLICITUDES_FOLIOS_TRANSPORTE { get; set; }
        public DbSet<V_TAB_OCUPANTES_ABORDO> V_TAB_OCUPANTES_ABORDO { get; set; }
        public DbSet<V_TOD_CAB_CAM_UTILIZADAS_ADM> V_TOD_CAB_CAM_UTILIZADAS_ADM { get; set; }
        public DbSet<V_TOD_CAB_UTILIZADAS_ADM> V_TOD_CAB_UTILIZADAS_ADM { get; set; }
        public DbSet<V_TOD_CAM_CAB_OCU> V_TOD_CAM_CAB_OCU { get; set; }
        public DbSet<V_TOD_CAM_CAB_OCU_REPORTES> V_TOD_CAM_CAB_OCU_REPORTES { get; set; }
        public DbSet<V_TOD_CAM_CAB_OCU_REPORTES_SUBDIVISIONES> V_TOD_CAM_CAB_OCU_REPORTES_SUBDIVISIONES { get; set; }
        public DbSet<V_TOD_CAM_CAB_OCU_SUBDIVISIONES> V_TOD_CAM_CAB_OCU_SUBDIVISIONES { get; set; }
        public DbSet<V_TOD_CAM_CAB_TOD_OCU> V_TOD_CAM_CAB_TOD_OCU { get; set; }
        public DbSet<V_TOD_OCU_CAB_CAM_BOT> V_TOD_OCU_CAB_CAM_BOT { get; set; }
        public DbSet<V_TOD_OCU_CAB_CAM_BOT_PLAT> V_TOD_OCU_CAB_CAM_BOT_PLAT { get; set; }
        public DbSet<V_TOD_OCU_CAB_CAM_BOT_REPORTES> V_TOD_OCU_CAB_CAM_BOT_REPORTES { get; set; }
        public DbSet<VW_ACTIVIDADES> VW_ACTIVIDADES { get; set; }
        public DbSet<VW_C_EMBARCACIONES> VW_C_EMBARCACIONES { get; set; }
        public DbSet<VW_CAT_INSUMO> VW_CAT_INSUMO { get; set; }
        public DbSet<VW_CLASIF_FAMI> VW_CLASIF_FAMI { get; set; }
        public DbSet<VW_CORTES_DIARIOS_INSTALACIONES_GESTION_GL> VW_CORTES_DIARIOS_INSTALACIONES_GESTION_GL { get; set; }
        public DbSet<VW_D_SOLICITUD> VW_D_SOLICITUD { get; set; }
        public DbSet<VW_DESPACHOS> VW_DESPACHOS { get; set; }
        public DbSet<VW_DESTINOS> VW_DESTINOS { get; set; }
        public DbSet<VW_DIA_INS> VW_DIA_INS { get; set; }
        public DbSet<VW_EMBARCACION_CIA> VW_EMBARCACION_CIA { get; set; }
        public DbSet<VW_INS_CUSTODIO> VW_INS_CUSTODIO { get; set; }
        public DbSet<VW_INSTALACIONES_NE> VW_INSTALACIONES_NE { get; set; }
        public DbSet<VW_M_SOL_BUS> VW_M_SOL_BUS { get; set; }
        public DbSet<VW_M_SOLICITUD> VW_M_SOLICITUD { get; set; }
        public DbSet<VW_OCUPANTES_EMBARCACIONES_ACTIVOS> VW_OCUPANTES_EMBARCACIONES_ACTIVOS { get; set; }
        public DbSet<VW_R_CUSTODIOS> VW_R_CUSTODIOS { get; set; }
        public DbSet<VW_R_DESPACHO> VW_R_DESPACHO { get; set; }
        public DbSet<VW_R_SOLGEN> VW_R_SOLGEN { get; set; }
        public DbSet<VW_R_SOLGEN_HIST> VW_R_SOLGEN_HIST { get; set; }
        public DbSet<VW_RUTA_DIA_INS> VW_RUTA_DIA_INS { get; set; }
        public DbSet<VW_RUTAS> VW_RUTAS { get; set; }
        public DbSet<VW_TIME_TOT_ACTS> VW_TIME_TOT_ACTS { get; set; }
        public DbSet<VW_VIAJES> VW_VIAJES { get; set; }
    }
}
