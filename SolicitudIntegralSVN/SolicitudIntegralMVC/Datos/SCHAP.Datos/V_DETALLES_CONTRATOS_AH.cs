//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SCHAP.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class V_DETALLES_CONTRATOS_AH
    {
        public int IdDetalleContratoAH { get; set; }
        public int FkIdContratoAH { get; set; }
        public int FkIdPartidaContrato { get; set; }
        public decimal PrecioPartida { get; set; }
        public string NombreCortoPartida { get; set; }
        public string DescripcionPartidaContrato { get; set; }
        public string UnidadMedida { get; set; }
        public string Moneda { get; set; }
    }
}
