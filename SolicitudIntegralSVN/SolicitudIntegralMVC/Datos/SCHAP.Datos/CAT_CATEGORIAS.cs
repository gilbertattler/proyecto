//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SCHAP.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class CAT_CATEGORIAS
    {
        public CAT_CATEGORIAS()
        {
            this.CAT_SUBCATEGORIAS = new HashSet<CAT_SUBCATEGORIAS>();
        }
    
        public int PK_ID_CATEGORIA { get; set; }
        public string NOMBRE { get; set; }
        public Nullable<bool> BORRADO_LOGICO { get; set; }
    
        public virtual ICollection<CAT_SUBCATEGORIAS> CAT_SUBCATEGORIAS { get; set; }
    }
}
