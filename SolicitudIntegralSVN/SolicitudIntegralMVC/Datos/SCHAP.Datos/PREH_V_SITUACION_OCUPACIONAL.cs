//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SCHAP.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class PREH_V_SITUACION_OCUPACIONAL
    {
        public string CVE_PLATAFORMA { get; set; }
        public string FIC_RFC_USUARIO { get; set; }
        public string NOM_USUARIO { get; set; }
        public string NOM_CIA { get; set; }
        public string TIP_TRANPORTE { get; set; }
        public string FASE_CONTINGENCIA { get; set; }
        public string CENTRO_NOMINA { get; set; }
        public string DEPTO_NOMINA { get; set; }
        public string NOM_CATEGORIA { get; set; }
        public string TIP_EMPLEADO { get; set; }
        public string CVE_CIA { get; set; }
        public string ROL_OCUPANTE { get; set; }
        public string FOLIO_LIBRETA_MAR { get; set; }
        public string FECHA_VIGENCIA_LIBRETA { get; set; }
    }
}
