//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SCHAP.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class VW_CORTES_DIARIOS_INSTALACIONES_GESTION_GL
    {
        public int PK_ID_CORTEDIARIO { get; set; }
        public int FK_C_M_INSTALACION { get; set; }
        public string CVE_PLATAFORMA { get; set; }
        public string NOMBRE_PLATAFORMA { get; set; }
        public System.DateTime FECHA_CIERRE { get; set; }
        public int PERS_PEP { get; set; }
        public int PERS_CIA { get; set; }
        public int PERS_COM { get; set; }
        public int PERS_TRIP { get; set; }
        public int TOTAL_HUESPEDES { get; set; }
        public int TOTAL_VISITAS { get; set; }
        public int TOTAL_FORANEOS { get; set; }
        public int TOTAL_ETAPA_PARCIAL { get; set; }
        public int TOTAL_ETAPA_TOTAL { get; set; }
        public int TOTAL_ETAPA_TRIPULACION { get; set; }
        public int TOTAL_HUESPEDES_PEMEX { get; set; }
        public int TOTAL_HUESPEDES_COMPANIA { get; set; }
        public int TOTAL_HUESPEDES_COMODATARIO { get; set; }
        public int TOTAL_HUESPEDES_TRIPULACION { get; set; }
        public int TOTAL_ENTRADAS { get; set; }
        public int TOTAL_SALIDAS { get; set; }
        public int CAMAS_OCUPADAS { get; set; }
        public int CAMAS_CALIENTES { get; set; }
        public int CAMAS_PROSINOCUP { get; set; }
        public int CAMAS_DISPONIBLES { get; set; }
        public int DESAYUNOS { get; set; }
        public int COMIDAS { get; set; }
        public int CENAS { get; set; }
        public int CENAS_MEDIANOCHE { get; set; }
        public int COMIDAS_ADICIONALES { get; set; }
        public int CAMAS_FUERA_DE_SERVICIO { get; set; }
        public int TOTAL_CAMAS_BASE { get; set; }
        public int TOTAL_CAMAS_CONTENEDOR { get; set; }
        public int TOTAL_CAMAS_TEMPORALES { get; set; }
        public int TOTAL_PERNOCTAS { get; set; }
        public int TOTAL_DESAYUNOS_ANEXO3 { get; set; }
        public int TOTAL_COMIDAS_ANEXO3 { get; set; }
        public int TOTAL_CENAS_ANEXO3 { get; set; }
        public int TOTAL_CENAS_MEDIA_NOCHE_ANEXO3 { get; set; }
        public int TOTAL_HOTELERIA_ANEXO3 { get; set; }
        public int TOTAL_ALIMENTOS_ANEXO3 { get; set; }
        public string OBSERVACIONES { get; set; }
        public string FORMULA_COSTOS_APLICADA { get; set; }
        public string PARAMETROS_FORMULA_COSTOS { get; set; }
        public decimal RESULTADO_EVALUACION_FORMULA_COSTOS { get; set; }
        public int ID_C_M_INSTALACION { get; set; }
        public Nullable<int> FK_ID_C_M_INSTALACION_UBICACION_ACTUAL { get; set; }
        public Nullable<int> ID_C_COMPLEJO { get; set; }
        public Nullable<int> ID_C_SECTOR { get; set; }
        public string SIGLAS_INSTALACION_UBICACION_ACTUAL { get; set; }
        public string NOMBRE_INSTALACION_UBICACION_ACTUAL { get; set; }
        public string CVE_REGION { get; set; }
        public string SIGLAS_REGION { get; set; }
        public string NOMBRE_REGION { get; set; }
        public string CVE_ACTIVO { get; set; }
        public string SIGLAS_ACTIVO { get; set; }
        public string NOMBRE_ACTIVO { get; set; }
        public string SIGLAS_COMPLEJO { get; set; }
        public string NOMBRE_COMPLEJO { get; set; }
        public string CVE_SECTOR { get; set; }
        public string NOMBRE_SECTOR { get; set; }
        public string SIGLAS_INSTALACION { get; set; }
        public string NOMBRE_INSTALACION { get; set; }
        public string CIA_PROPIETARIA { get; set; }
        public Nullable<System.DateTime> FECHA_INICIO_OPERACION { get; set; }
        public string NO_EQUIPO { get; set; }
        public bool BORRADO_LOGICO { get; set; }
        public string NOMBRE_CLASIFICACION_PROCESO { get; set; }
        public string NOMBRE_CLASIFICACION_SERVICIO { get; set; }
        public string NOMBRE_TIPO_ESTRUCTURA { get; set; }
        public string FIJA_MOVIL { get; set; }
        public bool HABITADA { get; set; }
    }
}
