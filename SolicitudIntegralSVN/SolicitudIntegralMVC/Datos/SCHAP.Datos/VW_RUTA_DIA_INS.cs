//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SCHAP.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class VW_RUTA_DIA_INS
    {
        public int PK_ID_P_RUTA { get; set; }
        public Nullable<int> PK_ID_RUTA { get; set; }
        public Nullable<System.DateTime> FECHA_RUTA { get; set; }
        public int PK_ID_D_RUTA { get; set; }
        public string SIGLAS { get; set; }
        public string DIA_SEMANA { get; set; }
        public Nullable<int> ID_M_INSTALACION { get; set; }
    }
}
