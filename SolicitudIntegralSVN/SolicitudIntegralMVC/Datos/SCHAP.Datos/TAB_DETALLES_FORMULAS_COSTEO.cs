//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SCHAP.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class TAB_DETALLES_FORMULAS_COSTEO
    {
        public int IdDetalleFomulaCosteo { get; set; }
        public int FkIdFormulaCosteo { get; set; }
        public string NombreParametro { get; set; }
        public string ValorParametro { get; set; }
        public string DescripcionParametro { get; set; }
        public string TipoParametro { get; set; }
    
        public virtual TAB_FORMULAS_COSTEO TAB_FORMULAS_COSTEO { get; set; }
    }
}
