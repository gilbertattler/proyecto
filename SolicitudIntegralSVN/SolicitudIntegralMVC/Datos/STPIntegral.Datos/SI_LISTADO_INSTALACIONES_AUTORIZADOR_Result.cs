//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace STPIntegral.Datos
{
    using System;
    
    public partial class SI_LISTADO_INSTALACIONES_AUTORIZADOR_Result
    {
        public string NOMBRE_INSTALACION { get; set; }
        public string PERMISO_STA { get; set; }
        public string PERMISO_STPVM { get; set; }
        public string PERMISO_AH { get; set; }
        public string DIREC_STA { get; set; }
        public string DIREC_STPVM { get; set; }
        public string DIREC_AH { get; set; }
    }
}
