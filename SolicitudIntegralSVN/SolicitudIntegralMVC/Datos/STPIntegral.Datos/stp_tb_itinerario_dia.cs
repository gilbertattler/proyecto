//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace STPIntegral.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class stp_tb_itinerario_dia
    {
        public int id_itinerario_dia { get; set; }
        public Nullable<int> id_itinerario { get; set; }
        public Nullable<System.DateTime> fecha_itinerario_dia { get; set; }
        public Nullable<System.DateTime> horario_itinerario { get; set; }
        public Nullable<System.DateTime> horario_recepcion { get; set; }
        public Nullable<int> dias_recepcion { get; set; }
        public string fic_actua { get; set; }
        public System.DateTime fec_actua { get; set; }
        public bool borrado_logico { get; set; }
    
        public virtual stp_c_itinerario_base stp_c_itinerario_base { get; set; }
    }
}
