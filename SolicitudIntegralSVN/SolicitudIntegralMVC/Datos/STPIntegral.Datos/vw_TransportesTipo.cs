//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace STPIntegral.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_TransportesTipo
    {
        public int IdTransporte { get; set; }
        public int ID_TIPO_TRASPORTE { get; set; }
        public string CveTransporte { get; set; }
        public string Placa { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public string CVE_TIPO_TRANSPORTE { get; set; }
        public int Capacidad { get; set; }
        public string TarjetaCirculacion { get; set; }
        public System.DateTime VigenciaTarjetaCirculacion { get; set; }
        public string Tenencia { get; set; }
        public System.DateTime VigenciaTenencia { get; set; }
        public string NumeroSerie { get; set; }
        public System.DateTime VigenciaSeguro { get; set; }
        public string CompaniaSeguro { get; set; }
        public string CartaFactura { get; set; }
        public bool BorradoLogicoTransporte { get; set; }
        public string ContratoSAP { get; set; }
        public string Convenio { get; set; }
        public string Costo { get; set; }
        public string Status { get; set; }
        public string Nodo { get; set; }
    }
}
