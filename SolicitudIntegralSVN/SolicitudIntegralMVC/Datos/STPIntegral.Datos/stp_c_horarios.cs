//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace STPIntegral.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class stp_c_horarios
    {
        public int id_horario { get; set; }
        public System.TimeSpan hora_inicio { get; set; }
        public System.TimeSpan hora_fin { get; set; }
        public System.DateTime fec_inicio_verano { get; set; }
        public System.DateTime fec_fin_verano { get; set; }
        public System.TimeSpan hora_inicio_verano { get; set; }
        public System.TimeSpan hora_fin_verano { get; set; }
        public string fic_actua { get; set; }
        public System.DateTime fec_actua { get; set; }
        public bool borrado_logico { get; set; }
    }
}
