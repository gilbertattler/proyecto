//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace STPIntegral.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class VW_stp_c_itinerarios
    {
        public int id_itinerario { get; set; }
        public int fk_tipo_servicio { get; set; }
        public int fk_nodo_origen { get; set; }
        public int fk_nodo_destino { get; set; }
        public int fk_nodo_atencion { get; set; }
        public int fk_area_servicio { get; set; }
        public string cve_tipo_servicio { get; set; }
        public string des_tipo_servicio { get; set; }
        public string cve_itinerario { get; set; }
        public string des_itinerario { get; set; }
        public Nullable<System.DateTime> horario_itinerario { get; set; }
        public Nullable<System.DateTime> horario_recepcion { get; set; }
        public Nullable<int> dias_recepcion { get; set; }
        public int horas_candado { get; set; }
        public string cve_nodo_origen { get; set; }
        public string des_nodo_origen { get; set; }
        public string cve_nodo_destino { get; set; }
        public string des_nodo_destino { get; set; }
        public string cve_nodo_atencion { get; set; }
        public string des_nodo_atencion { get; set; }
        public bool borrado_logico { get; set; }
        public bool Eliminar_cron { get; set; }
        public int fk_ruta { get; set; }
    }
}
