//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace STPIntegral.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class TabAuditoriaSolicitudIntegral
    {
        public int IdAuditoriaSolicitudIntegral { get; set; }
        public int FkIdSolicitudIntegral { get; set; }
        public System.DateTime FechaAuditoria { get; set; }
        public string FichaRFCUsuario { get; set; }
        public string NombreUsuario { get; set; }
        public string SubdivisionUsuario { get; set; }
        public string RolUsuario { get; set; }
        public string NodoAtencion { get; set; }
        public string IPUsuario { get; set; }
        public string TipoMovimientoAuditoria { get; set; }
        public string DescripcionAuditoria { get; set; }
        public string ObjetoAntesModificacion { get; set; }
        public string ObjetoDespuesModificacion { get; set; }
    
        public virtual TabSolicitudIntegral TabSolicitudIntegral { get; set; }
    }
}
