//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace STPIntegral.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class VW_stp_c_itinerarios_semana
    {
        public int id_itinerario_semana { get; set; }
        public int id_itinerario { get; set; }
        public string tipo_semana { get; set; }
        public string des_itinerario { get; set; }
        public System.DateTime fecha_inicio { get; set; }
        public System.DateTime fecha_fin { get; set; }
        public Nullable<System.DateTime> fecha_creado { get; set; }
        public Nullable<bool> liberado { get; set; }
    }
}
