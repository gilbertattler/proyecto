//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace STPIntegral.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class ah_tb_capacidad_reservacion_instalacion
    {
        public int IdAhTbCapacidadReservacionInstalacion { get; set; }
        public int FkIdCMInstalacion { get; set; }
        public System.DateTime FechaReservacion { get; set; }
        public int CapacidadTotalReservaciones { get; set; }
        public int CapacidadTotalInstalacion { get; set; }
        public int CapacidadOcupadaReservaciones { get; set; }
        public int CapacidadDisponibleReservaciones { get; set; }
    }
}
