//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace STPIntegral.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class VW_GeneradorAutobuses
    {
        public string CVE_TIPO_TRANSPORTE { get; set; }
        public string CveViaje { get; set; }
        public string CveTransporte { get; set; }
        public string CveServicio { get; set; }
        public bool Cobro { get; set; }
        public string NOMBRE_ACTIVO { get; set; }
        public string SIGLAS_ACTIVO { get; set; }
        public System.DateTime Fecha { get; set; }
        public string Costo { get; set; }
        public string Clave { get; set; }
        public string Nodo { get; set; }
    }
}
