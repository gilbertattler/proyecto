//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace STPIntegral.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class stp_tb_estimacion
    {
        public int id_estimacion { get; set; }
        public int fk_vuelo { get; set; }
        public bool estado_cierre { get; set; }
        public System.DateTime hora_inicio { get; set; }
        public System.DateTime hora_fin { get; set; }
        public int num_aterrizaje { get; set; }
        public int fk_origen { get; set; }
        public Nullable<int> fk_destino { get; set; }
        public System.DateTime hora_llegada { get; set; }
        public Nullable<System.DateTime> hora_salida { get; set; }
        public int fk_actividad { get; set; }
        public decimal distancia { get; set; }
        public int pax_abordo { get; set; }
        public int pax_suben { get; set; }
        public int pax_bajan { get; set; }
        public int compensados { get; set; }
        public Nullable<decimal> horas_diurnas { get; set; }
        public Nullable<decimal> horas_nocturnas { get; set; }
        public Nullable<decimal> km_diurno { get; set; }
        public Nullable<decimal> km_nocturno { get; set; }
        public Nullable<decimal> km_pax_diurno { get; set; }
        public Nullable<decimal> km_pax_nocturno { get; set; }
        public Nullable<decimal> hora_espera_diurna { get; set; }
        public Nullable<decimal> hora_espera_nocturna { get; set; }
        public Nullable<decimal> ct_espera_diurna { get; set; }
        public Nullable<decimal> ct_espera_nocturna { get; set; }
        public Nullable<decimal> ct_espera_tot { get; set; }
        public Nullable<int> carga { get; set; }
        public Nullable<decimal> carga_diurna { get; set; }
        public Nullable<decimal> carga_nocturna { get; set; }
        public Nullable<decimal> ct_carga { get; set; }
        public Nullable<int> pernocta { get; set; }
        public Nullable<decimal> ct_pernocta { get; set; }
        public Nullable<decimal> ct_pax_diurno { get; set; }
        public Nullable<decimal> ct_pax_nocturno { get; set; }
        public Nullable<decimal> importe_secuencia { get; set; }
        public string tipo_costeo { get; set; }
        public string fic_actua { get; set; }
        public System.DateTime fec_actua { get; set; }
    
        public virtual stp_c_actividades stp_c_actividades { get; set; }
        public virtual stp_tb_vuelos stp_tb_vuelos { get; set; }
    }
}
