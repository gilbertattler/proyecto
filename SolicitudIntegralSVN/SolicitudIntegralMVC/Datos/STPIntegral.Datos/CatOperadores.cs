//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace STPIntegral.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class CatOperadores
    {
        public int id_operador { get; set; }
        public string ficha_rfc { get; set; }
        public string nombre { get; set; }
        public string licencia { get; set; }
        public string tipo_licencia { get; set; }
        public System.DateTime vigencia_licencia { get; set; }
        public Nullable<int> fk_nodo { get; set; }
        public string fic_actua { get; set; }
        public System.DateTime fec_actua { get; set; }
        public bool borrado_logico { get; set; }
    }
}
