using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace TransporteTerrestre.RhmCases.Datos{
    /// <summary>
    /// Strongly-typed collection for the VCasesCursosEventMaest class.
    /// </summary>
    [Serializable]
    public partial class VCasesCursosEventMaestCollection : ReadOnlyList<VCasesCursosEventMaest, VCasesCursosEventMaestCollection>
    {        
        public VCasesCursosEventMaestCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the V_CASES_CURSOS_EVENT_MAEST view.
    /// </summary>
    [Serializable]
    public partial class VCasesCursosEventMaest : ReadOnlyRecord<VCasesCursosEventMaest>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("V_CASES_CURSOS_EVENT_MAEST", TableType.View, DataService.GetInstance("pvdRhmCases"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"rh";
                //columns
                
                TableSchema.TableColumn colvarCveCurso = new TableSchema.TableColumn(schema);
                colvarCveCurso.ColumnName = "CVE_CURSO";
                colvarCveCurso.DataType = DbType.AnsiString;
                colvarCveCurso.MaxLength = 10;
                colvarCveCurso.AutoIncrement = false;
                colvarCveCurso.IsNullable = false;
                colvarCveCurso.IsPrimaryKey = false;
                colvarCveCurso.IsForeignKey = false;
                colvarCveCurso.IsReadOnly = false;
                
                schema.Columns.Add(colvarCveCurso);
                
                TableSchema.TableColumn colvarDesCurso = new TableSchema.TableColumn(schema);
                colvarDesCurso.ColumnName = "DES_CURSO";
                colvarDesCurso.DataType = DbType.AnsiString;
                colvarDesCurso.MaxLength = 50;
                colvarDesCurso.AutoIncrement = false;
                colvarDesCurso.IsNullable = false;
                colvarDesCurso.IsPrimaryKey = false;
                colvarDesCurso.IsForeignKey = false;
                colvarDesCurso.IsReadOnly = false;
                
                schema.Columns.Add(colvarDesCurso);
                
                TableSchema.TableColumn colvarCveSap = new TableSchema.TableColumn(schema);
                colvarCveSap.ColumnName = "CVE_SAP";
                colvarCveSap.DataType = DbType.AnsiString;
                colvarCveSap.MaxLength = 30;
                colvarCveSap.AutoIncrement = false;
                colvarCveSap.IsNullable = false;
                colvarCveSap.IsPrimaryKey = false;
                colvarCveSap.IsForeignKey = false;
                colvarCveSap.IsReadOnly = false;
                
                schema.Columns.Add(colvarCveSap);
                
                TableSchema.TableColumn colvarNumHoras = new TableSchema.TableColumn(schema);
                colvarNumHoras.ColumnName = "NUM_HORAS";
                colvarNumHoras.DataType = DbType.Int32;
                colvarNumHoras.MaxLength = 0;
                colvarNumHoras.AutoIncrement = false;
                colvarNumHoras.IsNullable = false;
                colvarNumHoras.IsPrimaryKey = false;
                colvarNumHoras.IsForeignKey = false;
                colvarNumHoras.IsReadOnly = false;
                
                schema.Columns.Add(colvarNumHoras);
                
                TableSchema.TableColumn colvarPorTeori = new TableSchema.TableColumn(schema);
                colvarPorTeori.ColumnName = "POR_TEORI";
                colvarPorTeori.DataType = DbType.Int32;
                colvarPorTeori.MaxLength = 0;
                colvarPorTeori.AutoIncrement = false;
                colvarPorTeori.IsNullable = false;
                colvarPorTeori.IsPrimaryKey = false;
                colvarPorTeori.IsForeignKey = false;
                colvarPorTeori.IsReadOnly = false;
                
                schema.Columns.Add(colvarPorTeori);
                
                TableSchema.TableColumn colvarPorPract = new TableSchema.TableColumn(schema);
                colvarPorPract.ColumnName = "POR_PRACT";
                colvarPorPract.DataType = DbType.Int32;
                colvarPorPract.MaxLength = 0;
                colvarPorPract.AutoIncrement = false;
                colvarPorPract.IsNullable = false;
                colvarPorPract.IsPrimaryKey = false;
                colvarPorPract.IsForeignKey = false;
                colvarPorPract.IsReadOnly = false;
                
                schema.Columns.Add(colvarPorPract);
                
                TableSchema.TableColumn colvarCveUnida = new TableSchema.TableColumn(schema);
                colvarCveUnida.ColumnName = "CVE_UNIDA";
                colvarCveUnida.DataType = DbType.AnsiString;
                colvarCveUnida.MaxLength = 4;
                colvarCveUnida.AutoIncrement = false;
                colvarCveUnida.IsNullable = true;
                colvarCveUnida.IsPrimaryKey = false;
                colvarCveUnida.IsForeignKey = false;
                colvarCveUnida.IsReadOnly = false;
                
                schema.Columns.Add(colvarCveUnida);
                
                TableSchema.TableColumn colvarFecInici = new TableSchema.TableColumn(schema);
                colvarFecInici.ColumnName = "FEC_INICI";
                colvarFecInici.DataType = DbType.AnsiString;
                colvarFecInici.MaxLength = 8;
                colvarFecInici.AutoIncrement = false;
                colvarFecInici.IsNullable = false;
                colvarFecInici.IsPrimaryKey = false;
                colvarFecInici.IsForeignKey = false;
                colvarFecInici.IsReadOnly = false;
                
                schema.Columns.Add(colvarFecInici);
                
                TableSchema.TableColumn colvarFecTermi = new TableSchema.TableColumn(schema);
                colvarFecTermi.ColumnName = "FEC_TERMI";
                colvarFecTermi.DataType = DbType.AnsiString;
                colvarFecTermi.MaxLength = 8;
                colvarFecTermi.AutoIncrement = false;
                colvarFecTermi.IsNullable = false;
                colvarFecTermi.IsPrimaryKey = false;
                colvarFecTermi.IsForeignKey = false;
                colvarFecTermi.IsReadOnly = false;
                
                schema.Columns.Add(colvarFecTermi);
                
                TableSchema.TableColumn colvarTipDesemP = new TableSchema.TableColumn(schema);
                colvarTipDesemP.ColumnName = "TIP_DESEM_P";
                colvarTipDesemP.DataType = DbType.Int32;
                colvarTipDesemP.MaxLength = 0;
                colvarTipDesemP.AutoIncrement = false;
                colvarTipDesemP.IsNullable = true;
                colvarTipDesemP.IsPrimaryKey = false;
                colvarTipDesemP.IsForeignKey = false;
                colvarTipDesemP.IsReadOnly = false;
                
                schema.Columns.Add(colvarTipDesemP);
                
                TableSchema.TableColumn colvarTipDesemT = new TableSchema.TableColumn(schema);
                colvarTipDesemT.ColumnName = "TIP_DESEM_T";
                colvarTipDesemT.DataType = DbType.Int32;
                colvarTipDesemT.MaxLength = 0;
                colvarTipDesemT.AutoIncrement = false;
                colvarTipDesemT.IsNullable = true;
                colvarTipDesemT.IsPrimaryKey = false;
                colvarTipDesemT.IsForeignKey = false;
                colvarTipDesemT.IsReadOnly = false;
                
                schema.Columns.Add(colvarTipDesemT);
                
                TableSchema.TableColumn colvarTipDesem = new TableSchema.TableColumn(schema);
                colvarTipDesem.ColumnName = "TIP_DESEM";
                colvarTipDesem.DataType = DbType.Int32;
                colvarTipDesem.MaxLength = 0;
                colvarTipDesem.AutoIncrement = false;
                colvarTipDesem.IsNullable = true;
                colvarTipDesem.IsPrimaryKey = false;
                colvarTipDesem.IsForeignKey = false;
                colvarTipDesem.IsReadOnly = false;
                
                schema.Columns.Add(colvarTipDesem);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["pvdRhmCases"].AddSchema("V_CASES_CURSOS_EVENT_MAEST",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public VCasesCursosEventMaest()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public VCasesCursosEventMaest(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public VCasesCursosEventMaest(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public VCasesCursosEventMaest(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("CveCurso")]
        [Bindable(true)]
        public string CveCurso 
	    {
		    get
		    {
			    return GetColumnValue<string>("CVE_CURSO");
		    }
            set 
		    {
			    SetColumnValue("CVE_CURSO", value);
            }
        }
	      
        [XmlAttribute("DesCurso")]
        [Bindable(true)]
        public string DesCurso 
	    {
		    get
		    {
			    return GetColumnValue<string>("DES_CURSO");
		    }
            set 
		    {
			    SetColumnValue("DES_CURSO", value);
            }
        }
	      
        [XmlAttribute("CveSap")]
        [Bindable(true)]
        public string CveSap 
	    {
		    get
		    {
			    return GetColumnValue<string>("CVE_SAP");
		    }
            set 
		    {
			    SetColumnValue("CVE_SAP", value);
            }
        }
	      
        [XmlAttribute("NumHoras")]
        [Bindable(true)]
        public int NumHoras 
	    {
		    get
		    {
			    return GetColumnValue<int>("NUM_HORAS");
		    }
            set 
		    {
			    SetColumnValue("NUM_HORAS", value);
            }
        }
	      
        [XmlAttribute("PorTeori")]
        [Bindable(true)]
        public int PorTeori 
	    {
		    get
		    {
			    return GetColumnValue<int>("POR_TEORI");
		    }
            set 
		    {
			    SetColumnValue("POR_TEORI", value);
            }
        }
	      
        [XmlAttribute("PorPract")]
        [Bindable(true)]
        public int PorPract 
	    {
		    get
		    {
			    return GetColumnValue<int>("POR_PRACT");
		    }
            set 
		    {
			    SetColumnValue("POR_PRACT", value);
            }
        }
	      
        [XmlAttribute("CveUnida")]
        [Bindable(true)]
        public string CveUnida 
	    {
		    get
		    {
			    return GetColumnValue<string>("CVE_UNIDA");
		    }
            set 
		    {
			    SetColumnValue("CVE_UNIDA", value);
            }
        }
	      
        [XmlAttribute("FecInici")]
        [Bindable(true)]
        public string FecInici 
	    {
		    get
		    {
			    return GetColumnValue<string>("FEC_INICI");
		    }
            set 
		    {
			    SetColumnValue("FEC_INICI", value);
            }
        }
	      
        [XmlAttribute("FecTermi")]
        [Bindable(true)]
        public string FecTermi 
	    {
		    get
		    {
			    return GetColumnValue<string>("FEC_TERMI");
		    }
            set 
		    {
			    SetColumnValue("FEC_TERMI", value);
            }
        }
	      
        [XmlAttribute("TipDesemP")]
        [Bindable(true)]
        public int? TipDesemP 
	    {
		    get
		    {
			    return GetColumnValue<int?>("TIP_DESEM_P");
		    }
            set 
		    {
			    SetColumnValue("TIP_DESEM_P", value);
            }
        }
	      
        [XmlAttribute("TipDesemT")]
        [Bindable(true)]
        public int? TipDesemT 
	    {
		    get
		    {
			    return GetColumnValue<int?>("TIP_DESEM_T");
		    }
            set 
		    {
			    SetColumnValue("TIP_DESEM_T", value);
            }
        }
	      
        [XmlAttribute("TipDesem")]
        [Bindable(true)]
        public int? TipDesem 
	    {
		    get
		    {
			    return GetColumnValue<int?>("TIP_DESEM");
		    }
            set 
		    {
			    SetColumnValue("TIP_DESEM", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string CveCurso = @"CVE_CURSO";
            
            public static string DesCurso = @"DES_CURSO";
            
            public static string CveSap = @"CVE_SAP";
            
            public static string NumHoras = @"NUM_HORAS";
            
            public static string PorTeori = @"POR_TEORI";
            
            public static string PorPract = @"POR_PRACT";
            
            public static string CveUnida = @"CVE_UNIDA";
            
            public static string FecInici = @"FEC_INICI";
            
            public static string FecTermi = @"FEC_TERMI";
            
            public static string TipDesemP = @"TIP_DESEM_P";
            
            public static string TipDesemT = @"TIP_DESEM_T";
            
            public static string TipDesem = @"TIP_DESEM";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}
