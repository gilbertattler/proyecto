using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace TransporteTerrestre.RhmCases.Datos
{
    /// <summary>
    /// Controller class for CASES_OBSERVACIONES
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class CasesObservacionesController
    {
        // Preload our schema..
        CasesObservaciones thisSchemaLoad = new CasesObservaciones();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public CasesObservacionesCollection FetchAll()
        {
            CasesObservacionesCollection coll = new CasesObservacionesCollection();
            Query qry = new Query(CasesObservaciones.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public CasesObservacionesCollection FetchByID(object IdObservacion)
        {
            CasesObservacionesCollection coll = new CasesObservacionesCollection().Where("ID_OBSERVACION", IdObservacion).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public CasesObservacionesCollection FetchByQuery(Query qry)
        {
            CasesObservacionesCollection coll = new CasesObservacionesCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdObservacion)
        {
            return (CasesObservaciones.Delete(IdObservacion) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdObservacion)
        {
            return (CasesObservaciones.Destroy(IdObservacion) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string CveSap,string Ficha,string Fecha,string Observacion,string Origen,string FicActua,string FecActua)
	    {
		    CasesObservaciones item = new CasesObservaciones();
		    
            item.CveSap = CveSap;
            
            item.Ficha = Ficha;
            
            item.Fecha = Fecha;
            
            item.Observacion = Observacion;
            
            item.Origen = Origen;
            
            item.FicActua = FicActua;
            
            item.FecActua = FecActua;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdObservacion,string CveSap,string Ficha,string Fecha,string Observacion,string Origen,string FicActua,string FecActua)
	    {
		    CasesObservaciones item = new CasesObservaciones();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdObservacion = IdObservacion;
				
			item.CveSap = CveSap;
				
			item.Ficha = Ficha;
				
			item.Fecha = Fecha;
				
			item.Observacion = Observacion;
				
			item.Origen = Origen;
				
			item.FicActua = FicActua;
				
			item.FecActua = FecActua;
				
	        item.Save(UserName);
	    }
    }
}
