using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace TransporteTerrestre.RhmCases.Datos
{
	/// <summary>
	/// Strongly-typed collection for the CasesDicmeDetalles class.
	/// </summary>
    [Serializable]
	public partial class CasesDicmeDetallesCollection : ActiveList<CasesDicmeDetalles, CasesDicmeDetallesCollection>
	{	   
		public CasesDicmeDetallesCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>CasesDicmeDetallesCollection</returns>
		public CasesDicmeDetallesCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                CasesDicmeDetalles o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the CASES_DICME_DETALLES table.
	/// </summary>
	[Serializable]
	public partial class CasesDicmeDetalles : ActiveRecord<CasesDicmeDetalles>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public CasesDicmeDetalles()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public CasesDicmeDetalles(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public CasesDicmeDetalles(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public CasesDicmeDetalles(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("CASES_DICME_DETALLES", TableType.Table, DataService.GetInstance("pvdRhmCases"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"rh";
				//columns
				
				TableSchema.TableColumn colvarFicha = new TableSchema.TableColumn(schema);
				colvarFicha.ColumnName = "FICHA";
				colvarFicha.DataType = DbType.AnsiString;
				colvarFicha.MaxLength = 8;
				colvarFicha.AutoIncrement = false;
				colvarFicha.IsNullable = false;
				colvarFicha.IsPrimaryKey = true;
				colvarFicha.IsForeignKey = true;
				colvarFicha.IsReadOnly = false;
				colvarFicha.DefaultSetting = @"";
				
					colvarFicha.ForeignKeyTableName = "CASES_DICME";
				schema.Columns.Add(colvarFicha);
				
				TableSchema.TableColumn colvarCveSap = new TableSchema.TableColumn(schema);
				colvarCveSap.ColumnName = "CVE_SAP";
				colvarCveSap.DataType = DbType.AnsiString;
				colvarCveSap.MaxLength = 30;
				colvarCveSap.AutoIncrement = false;
				colvarCveSap.IsNullable = false;
				colvarCveSap.IsPrimaryKey = true;
				colvarCveSap.IsForeignKey = true;
				colvarCveSap.IsReadOnly = false;
				colvarCveSap.DefaultSetting = @"";
				
					colvarCveSap.ForeignKeyTableName = "CASES_DICME";
				schema.Columns.Add(colvarCveSap);
				
				TableSchema.TableColumn colvarFecDicta = new TableSchema.TableColumn(schema);
				colvarFecDicta.ColumnName = "FEC_DICTA";
				colvarFecDicta.DataType = DbType.AnsiString;
				colvarFecDicta.MaxLength = 8;
				colvarFecDicta.AutoIncrement = false;
				colvarFecDicta.IsNullable = false;
				colvarFecDicta.IsPrimaryKey = true;
				colvarFecDicta.IsForeignKey = false;
				colvarFecDicta.IsReadOnly = false;
				colvarFecDicta.DefaultSetting = @"";
				colvarFecDicta.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFecDicta);
				
				TableSchema.TableColumn colvarFicActua = new TableSchema.TableColumn(schema);
				colvarFicActua.ColumnName = "FIC_ACTUA";
				colvarFicActua.DataType = DbType.AnsiString;
				colvarFicActua.MaxLength = 8;
				colvarFicActua.AutoIncrement = false;
				colvarFicActua.IsNullable = false;
				colvarFicActua.IsPrimaryKey = false;
				colvarFicActua.IsForeignKey = false;
				colvarFicActua.IsReadOnly = false;
				colvarFicActua.DefaultSetting = @"";
				colvarFicActua.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFicActua);
				
				TableSchema.TableColumn colvarFecActua = new TableSchema.TableColumn(schema);
				colvarFecActua.ColumnName = "FEC_ACTUA";
				colvarFecActua.DataType = DbType.AnsiString;
				colvarFecActua.MaxLength = 8;
				colvarFecActua.AutoIncrement = false;
				colvarFecActua.IsNullable = false;
				colvarFecActua.IsPrimaryKey = false;
				colvarFecActua.IsForeignKey = false;
				colvarFecActua.IsReadOnly = false;
				colvarFecActua.DefaultSetting = @"";
				colvarFecActua.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFecActua);
				
				TableSchema.TableColumn colvarCveDicme = new TableSchema.TableColumn(schema);
				colvarCveDicme.ColumnName = "CVE_DICME";
				colvarCveDicme.DataType = DbType.AnsiString;
				colvarCveDicme.MaxLength = 5;
				colvarCveDicme.AutoIncrement = false;
				colvarCveDicme.IsNullable = true;
				colvarCveDicme.IsPrimaryKey = false;
				colvarCveDicme.IsForeignKey = false;
				colvarCveDicme.IsReadOnly = false;
				colvarCveDicme.DefaultSetting = @"";
				colvarCveDicme.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCveDicme);
				
				TableSchema.TableColumn colvarDesDicme = new TableSchema.TableColumn(schema);
				colvarDesDicme.ColumnName = "DES_DICME";
				colvarDesDicme.DataType = DbType.AnsiString;
				colvarDesDicme.MaxLength = 50;
				colvarDesDicme.AutoIncrement = false;
				colvarDesDicme.IsNullable = true;
				colvarDesDicme.IsPrimaryKey = false;
				colvarDesDicme.IsForeignKey = false;
				colvarDesDicme.IsReadOnly = false;
				colvarDesDicme.DefaultSetting = @"";
				colvarDesDicme.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDesDicme);
				
				TableSchema.TableColumn colvarIdObservacion = new TableSchema.TableColumn(schema);
				colvarIdObservacion.ColumnName = "ID_OBSERVACION";
				colvarIdObservacion.DataType = DbType.Int32;
				colvarIdObservacion.MaxLength = 0;
				colvarIdObservacion.AutoIncrement = false;
				colvarIdObservacion.IsNullable = true;
				colvarIdObservacion.IsPrimaryKey = false;
				colvarIdObservacion.IsForeignKey = false;
				colvarIdObservacion.IsReadOnly = false;
				colvarIdObservacion.DefaultSetting = @"";
				colvarIdObservacion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdObservacion);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["pvdRhmCases"].AddSchema("CASES_DICME_DETALLES",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Ficha")]
		[Bindable(true)]
		public string Ficha 
		{
			get { return GetColumnValue<string>(Columns.Ficha); }
			set { SetColumnValue(Columns.Ficha, value); }
		}
		  
		[XmlAttribute("CveSap")]
		[Bindable(true)]
		public string CveSap 
		{
			get { return GetColumnValue<string>(Columns.CveSap); }
			set { SetColumnValue(Columns.CveSap, value); }
		}
		  
		[XmlAttribute("FecDicta")]
		[Bindable(true)]
		public string FecDicta 
		{
			get { return GetColumnValue<string>(Columns.FecDicta); }
			set { SetColumnValue(Columns.FecDicta, value); }
		}
		  
		[XmlAttribute("FicActua")]
		[Bindable(true)]
		public string FicActua 
		{
			get { return GetColumnValue<string>(Columns.FicActua); }
			set { SetColumnValue(Columns.FicActua, value); }
		}
		  
		[XmlAttribute("FecActua")]
		[Bindable(true)]
		public string FecActua 
		{
			get { return GetColumnValue<string>(Columns.FecActua); }
			set { SetColumnValue(Columns.FecActua, value); }
		}
		  
		[XmlAttribute("CveDicme")]
		[Bindable(true)]
		public string CveDicme 
		{
			get { return GetColumnValue<string>(Columns.CveDicme); }
			set { SetColumnValue(Columns.CveDicme, value); }
		}
		  
		[XmlAttribute("DesDicme")]
		[Bindable(true)]
		public string DesDicme 
		{
			get { return GetColumnValue<string>(Columns.DesDicme); }
			set { SetColumnValue(Columns.DesDicme, value); }
		}
		  
		[XmlAttribute("IdObservacion")]
		[Bindable(true)]
		public int? IdObservacion 
		{
			get { return GetColumnValue<int?>(Columns.IdObservacion); }
			set { SetColumnValue(Columns.IdObservacion, value); }
		}
		
		#endregion
		
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a CasesDicme ActiveRecord object related to this CasesDicmeDetalles
		/// 
		/// </summary>
		public TransporteTerrestre.RhmCases.Datos.CasesDicme CasesDicme
		{
			get { return TransporteTerrestre.RhmCases.Datos.CasesDicme.FetchByID(this.CveSap); }
			set { SetColumnValue("CVE_SAP", value.Ficha); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varFicha,string varCveSap,string varFecDicta,string varFicActua,string varFecActua,string varCveDicme,string varDesDicme,int? varIdObservacion)
		{
			CasesDicmeDetalles item = new CasesDicmeDetalles();
			
			item.Ficha = varFicha;
			
			item.CveSap = varCveSap;
			
			item.FecDicta = varFecDicta;
			
			item.FicActua = varFicActua;
			
			item.FecActua = varFecActua;
			
			item.CveDicme = varCveDicme;
			
			item.DesDicme = varDesDicme;
			
			item.IdObservacion = varIdObservacion;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(string varFicha,string varCveSap,string varFecDicta,string varFicActua,string varFecActua,string varCveDicme,string varDesDicme,int? varIdObservacion)
		{
			CasesDicmeDetalles item = new CasesDicmeDetalles();
			
				item.Ficha = varFicha;
			
				item.CveSap = varCveSap;
			
				item.FecDicta = varFecDicta;
			
				item.FicActua = varFicActua;
			
				item.FecActua = varFecActua;
			
				item.CveDicme = varCveDicme;
			
				item.DesDicme = varDesDicme;
			
				item.IdObservacion = varIdObservacion;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn FichaColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CveSapColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn FecDictaColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn FicActuaColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn FecActuaColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn CveDicmeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn DesDicmeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn IdObservacionColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Ficha = @"FICHA";
			 public static string CveSap = @"CVE_SAP";
			 public static string FecDicta = @"FEC_DICTA";
			 public static string FicActua = @"FIC_ACTUA";
			 public static string FecActua = @"FEC_ACTUA";
			 public static string CveDicme = @"CVE_DICME";
			 public static string DesDicme = @"DES_DICME";
			 public static string IdObservacion = @"ID_OBSERVACION";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}
