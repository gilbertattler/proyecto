using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace TransporteTerrestre.RhmCases.Datos{
    /// <summary>
    /// Strongly-typed collection for the ViwJubre class.
    /// </summary>
    [Serializable]
    public partial class ViwJubreCollection : ReadOnlyList<ViwJubre, ViwJubreCollection>
    {        
        public ViwJubreCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the VIW_JUBRE view.
    /// </summary>
    [Serializable]
    public partial class ViwJubre : ReadOnlyRecord<ViwJubre>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("VIW_JUBRE", TableType.View, DataService.GetInstance("pvdRhmCases"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"rh";
                //columns
                
                TableSchema.TableColumn colvarDesCorh = new TableSchema.TableColumn(schema);
                colvarDesCorh.ColumnName = "des_corh";
                colvarDesCorh.DataType = DbType.AnsiString;
                colvarDesCorh.MaxLength = 50;
                colvarDesCorh.AutoIncrement = false;
                colvarDesCorh.IsNullable = true;
                colvarDesCorh.IsPrimaryKey = false;
                colvarDesCorh.IsForeignKey = false;
                colvarDesCorh.IsReadOnly = false;
                
                schema.Columns.Add(colvarDesCorh);
                
                TableSchema.TableColumn colvarTotal = new TableSchema.TableColumn(schema);
                colvarTotal.ColumnName = "Total";
                colvarTotal.DataType = DbType.Int32;
                colvarTotal.MaxLength = 0;
                colvarTotal.AutoIncrement = false;
                colvarTotal.IsNullable = true;
                colvarTotal.IsPrimaryKey = false;
                colvarTotal.IsForeignKey = false;
                colvarTotal.IsReadOnly = false;
                
                schema.Columns.Add(colvarTotal);
                
                TableSchema.TableColumn colvarRevAct = new TableSchema.TableColumn(schema);
                colvarRevAct.ColumnName = "RevAct";
                colvarRevAct.DataType = DbType.Int32;
                colvarRevAct.MaxLength = 0;
                colvarRevAct.AutoIncrement = false;
                colvarRevAct.IsNullable = true;
                colvarRevAct.IsPrimaryKey = false;
                colvarRevAct.IsForeignKey = false;
                colvarRevAct.IsReadOnly = false;
                
                schema.Columns.Add(colvarRevAct);
                
                TableSchema.TableColumn colvarPorAva = new TableSchema.TableColumn(schema);
                colvarPorAva.ColumnName = "PorAva";
                colvarPorAva.DataType = DbType.Int32;
                colvarPorAva.MaxLength = 0;
                colvarPorAva.AutoIncrement = false;
                colvarPorAva.IsNullable = true;
                colvarPorAva.IsPrimaryKey = false;
                colvarPorAva.IsForeignKey = false;
                colvarPorAva.IsReadOnly = false;
                
                schema.Columns.Add(colvarPorAva);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["pvdRhmCases"].AddSchema("VIW_JUBRE",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViwJubre()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViwJubre(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViwJubre(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViwJubre(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("DesCorh")]
        [Bindable(true)]
        public string DesCorh 
	    {
		    get
		    {
			    return GetColumnValue<string>("des_corh");
		    }
            set 
		    {
			    SetColumnValue("des_corh", value);
            }
        }
	      
        [XmlAttribute("Total")]
        [Bindable(true)]
        public int? Total 
	    {
		    get
		    {
			    return GetColumnValue<int?>("Total");
		    }
            set 
		    {
			    SetColumnValue("Total", value);
            }
        }
	      
        [XmlAttribute("RevAct")]
        [Bindable(true)]
        public int? RevAct 
	    {
		    get
		    {
			    return GetColumnValue<int?>("RevAct");
		    }
            set 
		    {
			    SetColumnValue("RevAct", value);
            }
        }
	      
        [XmlAttribute("PorAva")]
        [Bindable(true)]
        public int? PorAva 
	    {
		    get
		    {
			    return GetColumnValue<int?>("PorAva");
		    }
            set 
		    {
			    SetColumnValue("PorAva", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string DesCorh = @"des_corh";
            
            public static string Total = @"Total";
            
            public static string RevAct = @"RevAct";
            
            public static string PorAva = @"PorAva";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}
