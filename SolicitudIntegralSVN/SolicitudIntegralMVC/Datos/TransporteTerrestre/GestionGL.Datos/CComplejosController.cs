using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace TransporteTerrestre.GestionGL.Datos
{
    /// <summary>
    /// Controller class for C_COMPLEJOS
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class CComplejosController
    {
        // Preload our schema..
        CComplejos thisSchemaLoad = new CComplejos();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public CComplejosCollection FetchAll()
        {
            CComplejosCollection coll = new CComplejosCollection();
            Query qry = new Query(CComplejos.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public CComplejosCollection FetchByID(object IdCComplejo)
        {
            CComplejosCollection coll = new CComplejosCollection().Where("ID_C_COMPLEJO", IdCComplejo).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public CComplejosCollection FetchByQuery(Query qry)
        {
            CComplejosCollection coll = new CComplejosCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdCComplejo)
        {
            return (CComplejos.Delete(IdCComplejo) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdCComplejo)
        {
            return (CComplejos.Destroy(IdCComplejo) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(int? IdCActivo,string SiglasComplejo,string NombreComplejo,bool BorradoLogico)
	    {
		    CComplejos item = new CComplejos();
		    
            item.IdCActivo = IdCActivo;
            
            item.SiglasComplejo = SiglasComplejo;
            
            item.NombreComplejo = NombreComplejo;
            
            item.BorradoLogico = BorradoLogico;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdCComplejo,int? IdCActivo,string SiglasComplejo,string NombreComplejo,bool BorradoLogico)
	    {
		    CComplejos item = new CComplejos();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdCComplejo = IdCComplejo;
				
			item.IdCActivo = IdCActivo;
				
			item.SiglasComplejo = SiglasComplejo;
				
			item.NombreComplejo = NombreComplejo;
				
			item.BorradoLogico = BorradoLogico;
				
	        item.Save(UserName);
	    }
    }
}
