using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace TransporteTerrestre.GestionGL.Datos
{
    /// <summary>
    /// Controller class for C_REGIONES
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class CRegionesController
    {
        // Preload our schema..
        CRegiones thisSchemaLoad = new CRegiones();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public CRegionesCollection FetchAll()
        {
            CRegionesCollection coll = new CRegionesCollection();
            Query qry = new Query(CRegiones.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public CRegionesCollection FetchByID(object IdCRegion)
        {
            CRegionesCollection coll = new CRegionesCollection().Where("ID_C_REGION", IdCRegion).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public CRegionesCollection FetchByQuery(Query qry)
        {
            CRegionesCollection coll = new CRegionesCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdCRegion)
        {
            return (CRegiones.Delete(IdCRegion) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdCRegion)
        {
            return (CRegiones.Destroy(IdCRegion) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string CveRegion,string SiglasRegion,string NombreRegion,bool BorradoLogico)
	    {
		    CRegiones item = new CRegiones();
		    
            item.CveRegion = CveRegion;
            
            item.SiglasRegion = SiglasRegion;
            
            item.NombreRegion = NombreRegion;
            
            item.BorradoLogico = BorradoLogico;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdCRegion,string CveRegion,string SiglasRegion,string NombreRegion,bool BorradoLogico)
	    {
		    CRegiones item = new CRegiones();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdCRegion = IdCRegion;
				
			item.CveRegion = CveRegion;
				
			item.SiglasRegion = SiglasRegion;
				
			item.NombreRegion = NombreRegion;
				
			item.BorradoLogico = BorradoLogico;
				
	        item.Save(UserName);
	    }
    }
}
