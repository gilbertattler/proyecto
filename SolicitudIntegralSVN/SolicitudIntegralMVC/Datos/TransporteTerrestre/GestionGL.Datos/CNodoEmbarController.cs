using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace TransporteTerrestre.GestionGL.Datos
{
    /// <summary>
    /// Controller class for C_Nodo_Embar
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class CNodoEmbarController
    {
        // Preload our schema..
        CNodoEmbar thisSchemaLoad = new CNodoEmbar();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public CNodoEmbarCollection FetchAll()
        {
            CNodoEmbarCollection coll = new CNodoEmbarCollection();
            Query qry = new Query(CNodoEmbar.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public CNodoEmbarCollection FetchByID(object IdNodoEmbar)
        {
            CNodoEmbarCollection coll = new CNodoEmbarCollection().Where("Id_Nodo_Embar", IdNodoEmbar).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public CNodoEmbarCollection FetchByQuery(Query qry)
        {
            CNodoEmbarCollection coll = new CNodoEmbarCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdNodoEmbar)
        {
            return (CNodoEmbar.Delete(IdNodoEmbar) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdNodoEmbar)
        {
            return (CNodoEmbar.Destroy(IdNodoEmbar) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(int FkIdAreaServicio,int FkIdEmbar,string Nodo,bool BorradoLogico)
	    {
		    CNodoEmbar item = new CNodoEmbar();
		    
            item.FkIdAreaServicio = FkIdAreaServicio;
            
            item.FkIdEmbar = FkIdEmbar;
            
            item.Nodo = Nodo;
            
            item.BorradoLogico = BorradoLogico;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdNodoEmbar,int FkIdAreaServicio,int FkIdEmbar,string Nodo,bool BorradoLogico)
	    {
		    CNodoEmbar item = new CNodoEmbar();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdNodoEmbar = IdNodoEmbar;
				
			item.FkIdAreaServicio = FkIdAreaServicio;
				
			item.FkIdEmbar = FkIdEmbar;
				
			item.Nodo = Nodo;
				
			item.BorradoLogico = BorradoLogico;
				
	        item.Save(UserName);
	    }
    }
}
