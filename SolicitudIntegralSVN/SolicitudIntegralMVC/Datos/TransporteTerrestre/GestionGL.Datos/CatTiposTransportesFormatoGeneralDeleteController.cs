using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace TransporteTerrestre.GestionGL.Datos
{
    /// <summary>
    /// Controller class for CAT_TIPOS_TRANSPORTES_FORMATO_GENERAL_DELETE
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class CatTiposTransportesFormatoGeneralDeleteController
    {
        // Preload our schema..
        CatTiposTransportesFormatoGeneralDelete thisSchemaLoad = new CatTiposTransportesFormatoGeneralDelete();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public CatTiposTransportesFormatoGeneralDeleteCollection FetchAll()
        {
            CatTiposTransportesFormatoGeneralDeleteCollection coll = new CatTiposTransportesFormatoGeneralDeleteCollection();
            Query qry = new Query(CatTiposTransportesFormatoGeneralDelete.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public CatTiposTransportesFormatoGeneralDeleteCollection FetchByID(object IdTransporteMTrasnsporte)
        {
            CatTiposTransportesFormatoGeneralDeleteCollection coll = new CatTiposTransportesFormatoGeneralDeleteCollection().Where("ID_TRANSPORTE_M_TRASNSPORTE", IdTransporteMTrasnsporte).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public CatTiposTransportesFormatoGeneralDeleteCollection FetchByQuery(Query qry)
        {
            CatTiposTransportesFormatoGeneralDeleteCollection coll = new CatTiposTransportesFormatoGeneralDeleteCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdTransporteMTrasnsporte)
        {
            return (CatTiposTransportesFormatoGeneralDelete.Delete(IdTransporteMTrasnsporte) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdTransporteMTrasnsporte)
        {
            return (CatTiposTransportesFormatoGeneralDelete.Destroy(IdTransporteMTrasnsporte) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(int IdTransporteMTrasnsporte,int IdTipoTransporteGeneral,int IdTipoTrasporteRegistro,int IdTipoTransporteDetalle,string DescripcionColumna)
	    {
		    CatTiposTransportesFormatoGeneralDelete item = new CatTiposTransportesFormatoGeneralDelete();
		    
            item.IdTransporteMTrasnsporte = IdTransporteMTrasnsporte;
            
            item.IdTipoTransporteGeneral = IdTipoTransporteGeneral;
            
            item.IdTipoTrasporteRegistro = IdTipoTrasporteRegistro;
            
            item.IdTipoTransporteDetalle = IdTipoTransporteDetalle;
            
            item.DescripcionColumna = DescripcionColumna;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdTransporteMTrasnsporte,int IdTipoTransporteGeneral,int IdTipoTrasporteRegistro,int IdTipoTransporteDetalle,string DescripcionColumna)
	    {
		    CatTiposTransportesFormatoGeneralDelete item = new CatTiposTransportesFormatoGeneralDelete();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdTransporteMTrasnsporte = IdTransporteMTrasnsporte;
				
			item.IdTipoTransporteGeneral = IdTipoTransporteGeneral;
				
			item.IdTipoTrasporteRegistro = IdTipoTrasporteRegistro;
				
			item.IdTipoTransporteDetalle = IdTipoTransporteDetalle;
				
			item.DescripcionColumna = DescripcionColumna;
				
	        item.Save(UserName);
	    }
    }
}
