using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace TransporteTerrestre.GestionGL.Datos
{
	/// <summary>
	/// Strongly-typed collection for the DirCatFondo class.
	/// </summary>
    [Serializable]
	public partial class DirCatFondoCollection : ActiveList<DirCatFondo, DirCatFondoCollection>
	{	   
		public DirCatFondoCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>DirCatFondoCollection</returns>
		public DirCatFondoCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                DirCatFondo o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the DIR_CAT_FONDO table.
	/// </summary>
	[Serializable]
	public partial class DirCatFondo : ActiveRecord<DirCatFondo>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public DirCatFondo()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public DirCatFondo(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public DirCatFondo(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public DirCatFondo(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("DIR_CAT_FONDO", TableType.Table, DataService.GetInstance("pvdGestionGL"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarPkIdFondo = new TableSchema.TableColumn(schema);
				colvarPkIdFondo.ColumnName = "PK_ID_FONDO";
				colvarPkIdFondo.DataType = DbType.Int32;
				colvarPkIdFondo.MaxLength = 0;
				colvarPkIdFondo.AutoIncrement = true;
				colvarPkIdFondo.IsNullable = false;
				colvarPkIdFondo.IsPrimaryKey = true;
				colvarPkIdFondo.IsForeignKey = false;
				colvarPkIdFondo.IsReadOnly = false;
				colvarPkIdFondo.DefaultSetting = @"";
				colvarPkIdFondo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPkIdFondo);
				
				TableSchema.TableColumn colvarCveFondo = new TableSchema.TableColumn(schema);
				colvarCveFondo.ColumnName = "CVE_FONDO";
				colvarCveFondo.DataType = DbType.AnsiString;
				colvarCveFondo.MaxLength = 30;
				colvarCveFondo.AutoIncrement = false;
				colvarCveFondo.IsNullable = false;
				colvarCveFondo.IsPrimaryKey = false;
				colvarCveFondo.IsForeignKey = false;
				colvarCveFondo.IsReadOnly = false;
				colvarCveFondo.DefaultSetting = @"";
				colvarCveFondo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCveFondo);
				
				TableSchema.TableColumn colvarDescripcionFondo = new TableSchema.TableColumn(schema);
				colvarDescripcionFondo.ColumnName = "DESCRIPCION_FONDO";
				colvarDescripcionFondo.DataType = DbType.AnsiString;
				colvarDescripcionFondo.MaxLength = 100;
				colvarDescripcionFondo.AutoIncrement = false;
				colvarDescripcionFondo.IsNullable = false;
				colvarDescripcionFondo.IsPrimaryKey = false;
				colvarDescripcionFondo.IsForeignKey = false;
				colvarDescripcionFondo.IsReadOnly = false;
				colvarDescripcionFondo.DefaultSetting = @"";
				colvarDescripcionFondo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDescripcionFondo);
				
				TableSchema.TableColumn colvarBorradoLogico = new TableSchema.TableColumn(schema);
				colvarBorradoLogico.ColumnName = "BORRADO_LOGICO";
				colvarBorradoLogico.DataType = DbType.Boolean;
				colvarBorradoLogico.MaxLength = 0;
				colvarBorradoLogico.AutoIncrement = false;
				colvarBorradoLogico.IsNullable = false;
				colvarBorradoLogico.IsPrimaryKey = false;
				colvarBorradoLogico.IsForeignKey = false;
				colvarBorradoLogico.IsReadOnly = false;
				colvarBorradoLogico.DefaultSetting = @"";
				colvarBorradoLogico.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBorradoLogico);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["pvdGestionGL"].AddSchema("DIR_CAT_FONDO",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("PkIdFondo")]
		[Bindable(true)]
		public int PkIdFondo 
		{
			get { return GetColumnValue<int>(Columns.PkIdFondo); }
			set { SetColumnValue(Columns.PkIdFondo, value); }
		}
		  
		[XmlAttribute("CveFondo")]
		[Bindable(true)]
		public string CveFondo 
		{
			get { return GetColumnValue<string>(Columns.CveFondo); }
			set { SetColumnValue(Columns.CveFondo, value); }
		}
		  
		[XmlAttribute("DescripcionFondo")]
		[Bindable(true)]
		public string DescripcionFondo 
		{
			get { return GetColumnValue<string>(Columns.DescripcionFondo); }
			set { SetColumnValue(Columns.DescripcionFondo, value); }
		}
		  
		[XmlAttribute("BorradoLogico")]
		[Bindable(true)]
		public bool BorradoLogico 
		{
			get { return GetColumnValue<bool>(Columns.BorradoLogico); }
			set { SetColumnValue(Columns.BorradoLogico, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public TransporteTerrestre.GestionGL.Datos.DirPredireccionamientoCollection DirPredireccionamientoRecords()
		{
			return new TransporteTerrestre.GestionGL.Datos.DirPredireccionamientoCollection().Where(DirPredireccionamiento.Columns.PkIdFondo, PkIdFondo).Load();
		}
		#endregion
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varCveFondo,string varDescripcionFondo,bool varBorradoLogico)
		{
			DirCatFondo item = new DirCatFondo();
			
			item.CveFondo = varCveFondo;
			
			item.DescripcionFondo = varDescripcionFondo;
			
			item.BorradoLogico = varBorradoLogico;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varPkIdFondo,string varCveFondo,string varDescripcionFondo,bool varBorradoLogico)
		{
			DirCatFondo item = new DirCatFondo();
			
				item.PkIdFondo = varPkIdFondo;
			
				item.CveFondo = varCveFondo;
			
				item.DescripcionFondo = varDescripcionFondo;
			
				item.BorradoLogico = varBorradoLogico;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn PkIdFondoColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CveFondoColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn DescripcionFondoColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn BorradoLogicoColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string PkIdFondo = @"PK_ID_FONDO";
			 public static string CveFondo = @"CVE_FONDO";
			 public static string DescripcionFondo = @"DESCRIPCION_FONDO";
			 public static string BorradoLogico = @"BORRADO_LOGICO";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
