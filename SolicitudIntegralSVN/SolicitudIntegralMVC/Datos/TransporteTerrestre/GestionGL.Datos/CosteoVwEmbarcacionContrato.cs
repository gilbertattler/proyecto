using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace TransporteTerrestre.GestionGL.Datos{
    /// <summary>
    /// Strongly-typed collection for the CosteoVwEmbarcacionContrato class.
    /// </summary>
    [Serializable]
    public partial class CosteoVwEmbarcacionContratoCollection : ReadOnlyList<CosteoVwEmbarcacionContrato, CosteoVwEmbarcacionContratoCollection>
    {        
        public CosteoVwEmbarcacionContratoCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the COSTEO_VW_EMBARCACION_CONTRATO view.
    /// </summary>
    [Serializable]
    public partial class CosteoVwEmbarcacionContrato : ReadOnlyRecord<CosteoVwEmbarcacionContrato>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("COSTEO_VW_EMBARCACION_CONTRATO", TableType.View, DataService.GetInstance("pvdGestionGL"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarIdEmbar = new TableSchema.TableColumn(schema);
                colvarIdEmbar.ColumnName = "Id_Embar";
                colvarIdEmbar.DataType = DbType.Int32;
                colvarIdEmbar.MaxLength = 0;
                colvarIdEmbar.AutoIncrement = false;
                colvarIdEmbar.IsNullable = false;
                colvarIdEmbar.IsPrimaryKey = false;
                colvarIdEmbar.IsForeignKey = false;
                colvarIdEmbar.IsReadOnly = false;
                
                schema.Columns.Add(colvarIdEmbar);
                
                TableSchema.TableColumn colvarFkIdEmbar = new TableSchema.TableColumn(schema);
                colvarFkIdEmbar.ColumnName = "Fk_Id_Embar";
                colvarFkIdEmbar.DataType = DbType.Int32;
                colvarFkIdEmbar.MaxLength = 0;
                colvarFkIdEmbar.AutoIncrement = false;
                colvarFkIdEmbar.IsNullable = false;
                colvarFkIdEmbar.IsPrimaryKey = false;
                colvarFkIdEmbar.IsForeignKey = false;
                colvarFkIdEmbar.IsReadOnly = false;
                
                schema.Columns.Add(colvarFkIdEmbar);
                
                TableSchema.TableColumn colvarCveEmbarcacion = new TableSchema.TableColumn(schema);
                colvarCveEmbarcacion.ColumnName = "Cve_Embarcacion";
                colvarCveEmbarcacion.DataType = DbType.AnsiString;
                colvarCveEmbarcacion.MaxLength = 50;
                colvarCveEmbarcacion.AutoIncrement = false;
                colvarCveEmbarcacion.IsNullable = false;
                colvarCveEmbarcacion.IsPrimaryKey = false;
                colvarCveEmbarcacion.IsForeignKey = false;
                colvarCveEmbarcacion.IsReadOnly = false;
                
                schema.Columns.Add(colvarCveEmbarcacion);
                
                TableSchema.TableColumn colvarNomEmbarcacion = new TableSchema.TableColumn(schema);
                colvarNomEmbarcacion.ColumnName = "Nom_Embarcacion";
                colvarNomEmbarcacion.DataType = DbType.AnsiString;
                colvarNomEmbarcacion.MaxLength = 40;
                colvarNomEmbarcacion.AutoIncrement = false;
                colvarNomEmbarcacion.IsNullable = false;
                colvarNomEmbarcacion.IsPrimaryKey = false;
                colvarNomEmbarcacion.IsForeignKey = false;
                colvarNomEmbarcacion.IsReadOnly = false;
                
                schema.Columns.Add(colvarNomEmbarcacion);
                
                TableSchema.TableColumn colvarContrato = new TableSchema.TableColumn(schema);
                colvarContrato.ColumnName = "Contrato";
                colvarContrato.DataType = DbType.AnsiString;
                colvarContrato.MaxLength = 50;
                colvarContrato.AutoIncrement = false;
                colvarContrato.IsNullable = false;
                colvarContrato.IsPrimaryKey = false;
                colvarContrato.IsForeignKey = false;
                colvarContrato.IsReadOnly = false;
                
                schema.Columns.Add(colvarContrato);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["pvdGestionGL"].AddSchema("COSTEO_VW_EMBARCACION_CONTRATO",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public CosteoVwEmbarcacionContrato()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public CosteoVwEmbarcacionContrato(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public CosteoVwEmbarcacionContrato(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public CosteoVwEmbarcacionContrato(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("IdEmbar")]
        [Bindable(true)]
        public int IdEmbar 
	    {
		    get
		    {
			    return GetColumnValue<int>("Id_Embar");
		    }
            set 
		    {
			    SetColumnValue("Id_Embar", value);
            }
        }
	      
        [XmlAttribute("FkIdEmbar")]
        [Bindable(true)]
        public int FkIdEmbar 
	    {
		    get
		    {
			    return GetColumnValue<int>("Fk_Id_Embar");
		    }
            set 
		    {
			    SetColumnValue("Fk_Id_Embar", value);
            }
        }
	      
        [XmlAttribute("CveEmbarcacion")]
        [Bindable(true)]
        public string CveEmbarcacion 
	    {
		    get
		    {
			    return GetColumnValue<string>("Cve_Embarcacion");
		    }
            set 
		    {
			    SetColumnValue("Cve_Embarcacion", value);
            }
        }
	      
        [XmlAttribute("NomEmbarcacion")]
        [Bindable(true)]
        public string NomEmbarcacion 
	    {
		    get
		    {
			    return GetColumnValue<string>("Nom_Embarcacion");
		    }
            set 
		    {
			    SetColumnValue("Nom_Embarcacion", value);
            }
        }
	      
        [XmlAttribute("Contrato")]
        [Bindable(true)]
        public string Contrato 
	    {
		    get
		    {
			    return GetColumnValue<string>("Contrato");
		    }
            set 
		    {
			    SetColumnValue("Contrato", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string IdEmbar = @"Id_Embar";
            
            public static string FkIdEmbar = @"Fk_Id_Embar";
            
            public static string CveEmbarcacion = @"Cve_Embarcacion";
            
            public static string NomEmbarcacion = @"Nom_Embarcacion";
            
            public static string Contrato = @"Contrato";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}
