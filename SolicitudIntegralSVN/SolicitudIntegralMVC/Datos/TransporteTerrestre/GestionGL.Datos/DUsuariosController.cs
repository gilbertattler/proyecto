using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace TransporteTerrestre.GestionGL.Datos
{
    /// <summary>
    /// Controller class for D_USUARIOS
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class DUsuariosController
    {
        // Preload our schema..
        DUsuarios thisSchemaLoad = new DUsuarios();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public DUsuariosCollection FetchAll()
        {
            DUsuariosCollection coll = new DUsuariosCollection();
            Query qry = new Query(DUsuarios.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public DUsuariosCollection FetchByID(object IdDUsuario)
        {
            DUsuariosCollection coll = new DUsuariosCollection().Where("ID_D_USUARIO", IdDUsuario).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public DUsuariosCollection FetchByQuery(Query qry)
        {
            DUsuariosCollection coll = new DUsuariosCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdDUsuario)
        {
            return (DUsuarios.Delete(IdDUsuario) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdDUsuario)
        {
            return (DUsuarios.Destroy(IdDUsuario) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(int IdAreaServicio,int IdMUsuario,int? IdCSubdivisionesContratosCentrosGestores,DateTime? FecInicio,DateTime? FecFin,string Nodo,string CveBajal,DateTime? FecActua,string FicActua)
	    {
		    DUsuarios item = new DUsuarios();
		    
            item.IdAreaServicio = IdAreaServicio;
            
            item.IdMUsuario = IdMUsuario;
            
            item.IdCSubdivisionesContratosCentrosGestores = IdCSubdivisionesContratosCentrosGestores;
            
            item.FecInicio = FecInicio;
            
            item.FecFin = FecFin;
            
            item.Nodo = Nodo;
            
            item.CveBajal = CveBajal;
            
            item.FecActua = FecActua;
            
            item.FicActua = FicActua;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdDUsuario,int IdAreaServicio,int IdMUsuario,int? IdCSubdivisionesContratosCentrosGestores,DateTime? FecInicio,DateTime? FecFin,string Nodo,string CveBajal,DateTime? FecActua,string FicActua)
	    {
		    DUsuarios item = new DUsuarios();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdDUsuario = IdDUsuario;
				
			item.IdAreaServicio = IdAreaServicio;
				
			item.IdMUsuario = IdMUsuario;
				
			item.IdCSubdivisionesContratosCentrosGestores = IdCSubdivisionesContratosCentrosGestores;
				
			item.FecInicio = FecInicio;
				
			item.FecFin = FecFin;
				
			item.Nodo = Nodo;
				
			item.CveBajal = CveBajal;
				
			item.FecActua = FecActua;
				
			item.FicActua = FicActua;
				
	        item.Save(UserName);
	    }
    }
}
