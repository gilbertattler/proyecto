using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace TransporteTerrestre.GestionGL.Datos{
    /// <summary>
    /// Strongly-typed collection for the VwStmPozos class.
    /// </summary>
    [Serializable]
    public partial class VwStmPozosCollection : ReadOnlyList<VwStmPozos, VwStmPozosCollection>
    {        
        public VwStmPozosCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the VW_STM_POZOS view.
    /// </summary>
    [Serializable]
    public partial class VwStmPozos : ReadOnlyRecord<VwStmPozos>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("VW_STM_POZOS", TableType.View, DataService.GetInstance("pvdGestionGL"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarIdCMInstalacion = new TableSchema.TableColumn(schema);
                colvarIdCMInstalacion.ColumnName = "ID_C_M_INSTALACION";
                colvarIdCMInstalacion.DataType = DbType.Int32;
                colvarIdCMInstalacion.MaxLength = 0;
                colvarIdCMInstalacion.AutoIncrement = false;
                colvarIdCMInstalacion.IsNullable = false;
                colvarIdCMInstalacion.IsPrimaryKey = false;
                colvarIdCMInstalacion.IsForeignKey = false;
                colvarIdCMInstalacion.IsReadOnly = false;
                
                schema.Columns.Add(colvarIdCMInstalacion);
                
                TableSchema.TableColumn colvarIdCPozo = new TableSchema.TableColumn(schema);
                colvarIdCPozo.ColumnName = "ID_C_POZO";
                colvarIdCPozo.DataType = DbType.Int32;
                colvarIdCPozo.MaxLength = 0;
                colvarIdCPozo.AutoIncrement = false;
                colvarIdCPozo.IsNullable = false;
                colvarIdCPozo.IsPrimaryKey = false;
                colvarIdCPozo.IsForeignKey = false;
                colvarIdCPozo.IsReadOnly = false;
                
                schema.Columns.Add(colvarIdCPozo);
                
                TableSchema.TableColumn colvarSigPlat = new TableSchema.TableColumn(schema);
                colvarSigPlat.ColumnName = "SIG_PLAT";
                colvarSigPlat.DataType = DbType.AnsiString;
                colvarSigPlat.MaxLength = 6;
                colvarSigPlat.AutoIncrement = false;
                colvarSigPlat.IsNullable = true;
                colvarSigPlat.IsPrimaryKey = false;
                colvarSigPlat.IsForeignKey = false;
                colvarSigPlat.IsReadOnly = false;
                
                schema.Columns.Add(colvarSigPlat);
                
                TableSchema.TableColumn colvarSiglasArea = new TableSchema.TableColumn(schema);
                colvarSiglasArea.ColumnName = "SIGLAS_AREA";
                colvarSiglasArea.DataType = DbType.AnsiString;
                colvarSiglasArea.MaxLength = 80;
                colvarSiglasArea.AutoIncrement = false;
                colvarSiglasArea.IsNullable = false;
                colvarSiglasArea.IsPrimaryKey = false;
                colvarSiglasArea.IsForeignKey = false;
                colvarSiglasArea.IsReadOnly = false;
                
                schema.Columns.Add(colvarSiglasArea);
                
                TableSchema.TableColumn colvarPzoDescripcion = new TableSchema.TableColumn(schema);
                colvarPzoDescripcion.ColumnName = "PZO_DESCRIPCION";
                colvarPzoDescripcion.DataType = DbType.AnsiString;
                colvarPzoDescripcion.MaxLength = 60;
                colvarPzoDescripcion.AutoIncrement = false;
                colvarPzoDescripcion.IsNullable = true;
                colvarPzoDescripcion.IsPrimaryKey = false;
                colvarPzoDescripcion.IsForeignKey = false;
                colvarPzoDescripcion.IsReadOnly = false;
                
                schema.Columns.Add(colvarPzoDescripcion);
                
                TableSchema.TableColumn colvarPzoTarea = new TableSchema.TableColumn(schema);
                colvarPzoTarea.ColumnName = "PZO_TAREA";
                colvarPzoTarea.DataType = DbType.AnsiString;
                colvarPzoTarea.MaxLength = 1;
                colvarPzoTarea.AutoIncrement = false;
                colvarPzoTarea.IsNullable = true;
                colvarPzoTarea.IsPrimaryKey = false;
                colvarPzoTarea.IsForeignKey = false;
                colvarPzoTarea.IsReadOnly = false;
                
                schema.Columns.Add(colvarPzoTarea);
                
                TableSchema.TableColumn colvarPzoFechaInicioPerforacion = new TableSchema.TableColumn(schema);
                colvarPzoFechaInicioPerforacion.ColumnName = "PZO_FECHA_INICIO_PERFORACION";
                colvarPzoFechaInicioPerforacion.DataType = DbType.DateTime;
                colvarPzoFechaInicioPerforacion.MaxLength = 0;
                colvarPzoFechaInicioPerforacion.AutoIncrement = false;
                colvarPzoFechaInicioPerforacion.IsNullable = true;
                colvarPzoFechaInicioPerforacion.IsPrimaryKey = false;
                colvarPzoFechaInicioPerforacion.IsForeignKey = false;
                colvarPzoFechaInicioPerforacion.IsReadOnly = false;
                
                schema.Columns.Add(colvarPzoFechaInicioPerforacion);
                
                TableSchema.TableColumn colvarPzoFechaFinPerforacion = new TableSchema.TableColumn(schema);
                colvarPzoFechaFinPerforacion.ColumnName = "PZO_FECHA_FIN_PERFORACION";
                colvarPzoFechaFinPerforacion.DataType = DbType.DateTime;
                colvarPzoFechaFinPerforacion.MaxLength = 0;
                colvarPzoFechaFinPerforacion.AutoIncrement = false;
                colvarPzoFechaFinPerforacion.IsNullable = true;
                colvarPzoFechaFinPerforacion.IsPrimaryKey = false;
                colvarPzoFechaFinPerforacion.IsForeignKey = false;
                colvarPzoFechaFinPerforacion.IsReadOnly = false;
                
                schema.Columns.Add(colvarPzoFechaFinPerforacion);
                
                TableSchema.TableColumn colvarPzoFechaInicioTerminacion = new TableSchema.TableColumn(schema);
                colvarPzoFechaInicioTerminacion.ColumnName = "PZO_FECHA_INICIO_TERMINACION";
                colvarPzoFechaInicioTerminacion.DataType = DbType.DateTime;
                colvarPzoFechaInicioTerminacion.MaxLength = 0;
                colvarPzoFechaInicioTerminacion.AutoIncrement = false;
                colvarPzoFechaInicioTerminacion.IsNullable = true;
                colvarPzoFechaInicioTerminacion.IsPrimaryKey = false;
                colvarPzoFechaInicioTerminacion.IsForeignKey = false;
                colvarPzoFechaInicioTerminacion.IsReadOnly = false;
                
                schema.Columns.Add(colvarPzoFechaInicioTerminacion);
                
                TableSchema.TableColumn colvarPzoFechaFinTerminacion = new TableSchema.TableColumn(schema);
                colvarPzoFechaFinTerminacion.ColumnName = "PZO_FECHA_FIN_TERMINACION";
                colvarPzoFechaFinTerminacion.DataType = DbType.DateTime;
                colvarPzoFechaFinTerminacion.MaxLength = 0;
                colvarPzoFechaFinTerminacion.AutoIncrement = false;
                colvarPzoFechaFinTerminacion.IsNullable = true;
                colvarPzoFechaFinTerminacion.IsPrimaryKey = false;
                colvarPzoFechaFinTerminacion.IsForeignKey = false;
                colvarPzoFechaFinTerminacion.IsReadOnly = false;
                
                schema.Columns.Add(colvarPzoFechaFinTerminacion);
                
                TableSchema.TableColumn colvarBorradoLogico = new TableSchema.TableColumn(schema);
                colvarBorradoLogico.ColumnName = "BORRADO_LOGICO";
                colvarBorradoLogico.DataType = DbType.Boolean;
                colvarBorradoLogico.MaxLength = 0;
                colvarBorradoLogico.AutoIncrement = false;
                colvarBorradoLogico.IsNullable = false;
                colvarBorradoLogico.IsPrimaryKey = false;
                colvarBorradoLogico.IsForeignKey = false;
                colvarBorradoLogico.IsReadOnly = false;
                
                schema.Columns.Add(colvarBorradoLogico);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["pvdGestionGL"].AddSchema("VW_STM_POZOS",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public VwStmPozos()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public VwStmPozos(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public VwStmPozos(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public VwStmPozos(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("IdCMInstalacion")]
        [Bindable(true)]
        public int IdCMInstalacion 
	    {
		    get
		    {
			    return GetColumnValue<int>("ID_C_M_INSTALACION");
		    }
            set 
		    {
			    SetColumnValue("ID_C_M_INSTALACION", value);
            }
        }
	      
        [XmlAttribute("IdCPozo")]
        [Bindable(true)]
        public int IdCPozo 
	    {
		    get
		    {
			    return GetColumnValue<int>("ID_C_POZO");
		    }
            set 
		    {
			    SetColumnValue("ID_C_POZO", value);
            }
        }
	      
        [XmlAttribute("SigPlat")]
        [Bindable(true)]
        public string SigPlat 
	    {
		    get
		    {
			    return GetColumnValue<string>("SIG_PLAT");
		    }
            set 
		    {
			    SetColumnValue("SIG_PLAT", value);
            }
        }
	      
        [XmlAttribute("SiglasArea")]
        [Bindable(true)]
        public string SiglasArea 
	    {
		    get
		    {
			    return GetColumnValue<string>("SIGLAS_AREA");
		    }
            set 
		    {
			    SetColumnValue("SIGLAS_AREA", value);
            }
        }
	      
        [XmlAttribute("PzoDescripcion")]
        [Bindable(true)]
        public string PzoDescripcion 
	    {
		    get
		    {
			    return GetColumnValue<string>("PZO_DESCRIPCION");
		    }
            set 
		    {
			    SetColumnValue("PZO_DESCRIPCION", value);
            }
        }
	      
        [XmlAttribute("PzoTarea")]
        [Bindable(true)]
        public string PzoTarea 
	    {
		    get
		    {
			    return GetColumnValue<string>("PZO_TAREA");
		    }
            set 
		    {
			    SetColumnValue("PZO_TAREA", value);
            }
        }
	      
        [XmlAttribute("PzoFechaInicioPerforacion")]
        [Bindable(true)]
        public DateTime? PzoFechaInicioPerforacion 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("PZO_FECHA_INICIO_PERFORACION");
		    }
            set 
		    {
			    SetColumnValue("PZO_FECHA_INICIO_PERFORACION", value);
            }
        }
	      
        [XmlAttribute("PzoFechaFinPerforacion")]
        [Bindable(true)]
        public DateTime? PzoFechaFinPerforacion 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("PZO_FECHA_FIN_PERFORACION");
		    }
            set 
		    {
			    SetColumnValue("PZO_FECHA_FIN_PERFORACION", value);
            }
        }
	      
        [XmlAttribute("PzoFechaInicioTerminacion")]
        [Bindable(true)]
        public DateTime? PzoFechaInicioTerminacion 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("PZO_FECHA_INICIO_TERMINACION");
		    }
            set 
		    {
			    SetColumnValue("PZO_FECHA_INICIO_TERMINACION", value);
            }
        }
	      
        [XmlAttribute("PzoFechaFinTerminacion")]
        [Bindable(true)]
        public DateTime? PzoFechaFinTerminacion 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("PZO_FECHA_FIN_TERMINACION");
		    }
            set 
		    {
			    SetColumnValue("PZO_FECHA_FIN_TERMINACION", value);
            }
        }
	      
        [XmlAttribute("BorradoLogico")]
        [Bindable(true)]
        public bool BorradoLogico 
	    {
		    get
		    {
			    return GetColumnValue<bool>("BORRADO_LOGICO");
		    }
            set 
		    {
			    SetColumnValue("BORRADO_LOGICO", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string IdCMInstalacion = @"ID_C_M_INSTALACION";
            
            public static string IdCPozo = @"ID_C_POZO";
            
            public static string SigPlat = @"SIG_PLAT";
            
            public static string SiglasArea = @"SIGLAS_AREA";
            
            public static string PzoDescripcion = @"PZO_DESCRIPCION";
            
            public static string PzoTarea = @"PZO_TAREA";
            
            public static string PzoFechaInicioPerforacion = @"PZO_FECHA_INICIO_PERFORACION";
            
            public static string PzoFechaFinPerforacion = @"PZO_FECHA_FIN_PERFORACION";
            
            public static string PzoFechaInicioTerminacion = @"PZO_FECHA_INICIO_TERMINACION";
            
            public static string PzoFechaFinTerminacion = @"PZO_FECHA_FIN_TERMINACION";
            
            public static string BorradoLogico = @"BORRADO_LOGICO";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}
