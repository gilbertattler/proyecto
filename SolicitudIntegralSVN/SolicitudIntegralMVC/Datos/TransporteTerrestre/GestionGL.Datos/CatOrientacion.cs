using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace TransporteTerrestre.GestionGL.Datos
{
	/// <summary>
	/// Strongly-typed collection for the CatOrientacion class.
	/// </summary>
    [Serializable]
	public partial class CatOrientacionCollection : ActiveList<CatOrientacion, CatOrientacionCollection>
	{	   
		public CatOrientacionCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>CatOrientacionCollection</returns>
		public CatOrientacionCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                CatOrientacion o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the Cat_Orientacion table.
	/// </summary>
	[Serializable]
	public partial class CatOrientacion : ActiveRecord<CatOrientacion>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public CatOrientacion()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public CatOrientacion(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public CatOrientacion(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public CatOrientacion(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("Cat_Orientacion", TableType.Table, DataService.GetInstance("pvdGestionGL"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarIIDOrientacion = new TableSchema.TableColumn(schema);
				colvarIIDOrientacion.ColumnName = "i_IDOrientacion";
				colvarIIDOrientacion.DataType = DbType.Int32;
				colvarIIDOrientacion.MaxLength = 0;
				colvarIIDOrientacion.AutoIncrement = true;
				colvarIIDOrientacion.IsNullable = false;
				colvarIIDOrientacion.IsPrimaryKey = true;
				colvarIIDOrientacion.IsForeignKey = false;
				colvarIIDOrientacion.IsReadOnly = false;
				colvarIIDOrientacion.DefaultSetting = @"";
				colvarIIDOrientacion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIIDOrientacion);
				
				TableSchema.TableColumn colvarVcNomOrientacion = new TableSchema.TableColumn(schema);
				colvarVcNomOrientacion.ColumnName = "vc_NomOrientacion";
				colvarVcNomOrientacion.DataType = DbType.AnsiString;
				colvarVcNomOrientacion.MaxLength = 50;
				colvarVcNomOrientacion.AutoIncrement = false;
				colvarVcNomOrientacion.IsNullable = false;
				colvarVcNomOrientacion.IsPrimaryKey = false;
				colvarVcNomOrientacion.IsForeignKey = false;
				colvarVcNomOrientacion.IsReadOnly = false;
				colvarVcNomOrientacion.DefaultSetting = @"";
				colvarVcNomOrientacion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVcNomOrientacion);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["pvdGestionGL"].AddSchema("Cat_Orientacion",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("IIDOrientacion")]
		[Bindable(true)]
		public int IIDOrientacion 
		{
			get { return GetColumnValue<int>(Columns.IIDOrientacion); }
			set { SetColumnValue(Columns.IIDOrientacion, value); }
		}
		  
		[XmlAttribute("VcNomOrientacion")]
		[Bindable(true)]
		public string VcNomOrientacion 
		{
			get { return GetColumnValue<string>(Columns.VcNomOrientacion); }
			set { SetColumnValue(Columns.VcNomOrientacion, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varVcNomOrientacion)
		{
			CatOrientacion item = new CatOrientacion();
			
			item.VcNomOrientacion = varVcNomOrientacion;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varIIDOrientacion,string varVcNomOrientacion)
		{
			CatOrientacion item = new CatOrientacion();
			
				item.IIDOrientacion = varIIDOrientacion;
			
				item.VcNomOrientacion = varVcNomOrientacion;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IIDOrientacionColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn VcNomOrientacionColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string IIDOrientacion = @"i_IDOrientacion";
			 public static string VcNomOrientacion = @"vc_NomOrientacion";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}
