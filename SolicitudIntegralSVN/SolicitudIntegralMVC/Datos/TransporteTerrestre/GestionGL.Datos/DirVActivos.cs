using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace TransporteTerrestre.GestionGL.Datos{
    /// <summary>
    /// Strongly-typed collection for the DirVActivos class.
    /// </summary>
    [Serializable]
    public partial class DirVActivosCollection : ReadOnlyList<DirVActivos, DirVActivosCollection>
    {        
        public DirVActivosCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the DIR_V_ACTIVOS view.
    /// </summary>
    [Serializable]
    public partial class DirVActivos : ReadOnlyRecord<DirVActivos>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("DIR_V_ACTIVOS", TableType.View, DataService.GetInstance("pvdGestionGL"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarCveRegion = new TableSchema.TableColumn(schema);
                colvarCveRegion.ColumnName = "CVE_REGION";
                colvarCveRegion.DataType = DbType.AnsiString;
                colvarCveRegion.MaxLength = 2;
                colvarCveRegion.AutoIncrement = false;
                colvarCveRegion.IsNullable = true;
                colvarCveRegion.IsPrimaryKey = false;
                colvarCveRegion.IsForeignKey = false;
                colvarCveRegion.IsReadOnly = false;
                
                schema.Columns.Add(colvarCveRegion);
                
                TableSchema.TableColumn colvarCveActivo = new TableSchema.TableColumn(schema);
                colvarCveActivo.ColumnName = "CVE_ACTIVO";
                colvarCveActivo.DataType = DbType.AnsiString;
                colvarCveActivo.MaxLength = 18;
                colvarCveActivo.AutoIncrement = false;
                colvarCveActivo.IsNullable = true;
                colvarCveActivo.IsPrimaryKey = false;
                colvarCveActivo.IsForeignKey = false;
                colvarCveActivo.IsReadOnly = false;
                
                schema.Columns.Add(colvarCveActivo);
                
                TableSchema.TableColumn colvarNombreActivo = new TableSchema.TableColumn(schema);
                colvarNombreActivo.ColumnName = "NOMBRE_ACTIVO";
                colvarNombreActivo.DataType = DbType.AnsiString;
                colvarNombreActivo.MaxLength = 150;
                colvarNombreActivo.AutoIncrement = false;
                colvarNombreActivo.IsNullable = true;
                colvarNombreActivo.IsPrimaryKey = false;
                colvarNombreActivo.IsForeignKey = false;
                colvarNombreActivo.IsReadOnly = false;
                
                schema.Columns.Add(colvarNombreActivo);
                
                TableSchema.TableColumn colvarCveDs = new TableSchema.TableColumn(schema);
                colvarCveDs.ColumnName = "CVE_DS";
                colvarCveDs.DataType = DbType.AnsiString;
                colvarCveDs.MaxLength = 20;
                colvarCveDs.AutoIncrement = false;
                colvarCveDs.IsNullable = true;
                colvarCveDs.IsPrimaryKey = false;
                colvarCveDs.IsForeignKey = false;
                colvarCveDs.IsReadOnly = false;
                
                schema.Columns.Add(colvarCveDs);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["pvdGestionGL"].AddSchema("DIR_V_ACTIVOS",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public DirVActivos()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public DirVActivos(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public DirVActivos(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public DirVActivos(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("CveRegion")]
        [Bindable(true)]
        public string CveRegion 
	    {
		    get
		    {
			    return GetColumnValue<string>("CVE_REGION");
		    }
            set 
		    {
			    SetColumnValue("CVE_REGION", value);
            }
        }
	      
        [XmlAttribute("CveActivo")]
        [Bindable(true)]
        public string CveActivo 
	    {
		    get
		    {
			    return GetColumnValue<string>("CVE_ACTIVO");
		    }
            set 
		    {
			    SetColumnValue("CVE_ACTIVO", value);
            }
        }
	      
        [XmlAttribute("NombreActivo")]
        [Bindable(true)]
        public string NombreActivo 
	    {
		    get
		    {
			    return GetColumnValue<string>("NOMBRE_ACTIVO");
		    }
            set 
		    {
			    SetColumnValue("NOMBRE_ACTIVO", value);
            }
        }
	      
        [XmlAttribute("CveDs")]
        [Bindable(true)]
        public string CveDs 
	    {
		    get
		    {
			    return GetColumnValue<string>("CVE_DS");
		    }
            set 
		    {
			    SetColumnValue("CVE_DS", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string CveRegion = @"CVE_REGION";
            
            public static string CveActivo = @"CVE_ACTIVO";
            
            public static string NombreActivo = @"NOMBRE_ACTIVO";
            
            public static string CveDs = @"CVE_DS";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}
