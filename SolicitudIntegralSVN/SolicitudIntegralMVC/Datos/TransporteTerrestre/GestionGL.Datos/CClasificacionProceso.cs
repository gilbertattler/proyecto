using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace TransporteTerrestre.GestionGL.Datos
{
	/// <summary>
	/// Strongly-typed collection for the CClasificacionProceso class.
	/// </summary>
    [Serializable]
	public partial class CClasificacionProcesoCollection : ActiveList<CClasificacionProceso, CClasificacionProcesoCollection>
	{	   
		public CClasificacionProcesoCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>CClasificacionProcesoCollection</returns>
		public CClasificacionProcesoCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                CClasificacionProceso o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the C_CLASIFICACION_PROCESO table.
	/// </summary>
	[Serializable]
	public partial class CClasificacionProceso : ActiveRecord<CClasificacionProceso>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public CClasificacionProceso()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public CClasificacionProceso(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public CClasificacionProceso(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public CClasificacionProceso(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("C_CLASIFICACION_PROCESO", TableType.Table, DataService.GetInstance("pvdGestionGL"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarIdCClasificacionProceso = new TableSchema.TableColumn(schema);
				colvarIdCClasificacionProceso.ColumnName = "ID_C_CLASIFICACION_PROCESO";
				colvarIdCClasificacionProceso.DataType = DbType.Int32;
				colvarIdCClasificacionProceso.MaxLength = 0;
				colvarIdCClasificacionProceso.AutoIncrement = true;
				colvarIdCClasificacionProceso.IsNullable = false;
				colvarIdCClasificacionProceso.IsPrimaryKey = true;
				colvarIdCClasificacionProceso.IsForeignKey = false;
				colvarIdCClasificacionProceso.IsReadOnly = false;
				colvarIdCClasificacionProceso.DefaultSetting = @"";
				colvarIdCClasificacionProceso.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdCClasificacionProceso);
				
				TableSchema.TableColumn colvarNombreClasificacionProceso = new TableSchema.TableColumn(schema);
				colvarNombreClasificacionProceso.ColumnName = "NOMBRE_CLASIFICACION_PROCESO";
				colvarNombreClasificacionProceso.DataType = DbType.AnsiString;
				colvarNombreClasificacionProceso.MaxLength = 60;
				colvarNombreClasificacionProceso.AutoIncrement = false;
				colvarNombreClasificacionProceso.IsNullable = false;
				colvarNombreClasificacionProceso.IsPrimaryKey = false;
				colvarNombreClasificacionProceso.IsForeignKey = false;
				colvarNombreClasificacionProceso.IsReadOnly = false;
				colvarNombreClasificacionProceso.DefaultSetting = @"";
				colvarNombreClasificacionProceso.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNombreClasificacionProceso);
				
				TableSchema.TableColumn colvarBorradoLogico = new TableSchema.TableColumn(schema);
				colvarBorradoLogico.ColumnName = "BORRADO_LOGICO";
				colvarBorradoLogico.DataType = DbType.Boolean;
				colvarBorradoLogico.MaxLength = 0;
				colvarBorradoLogico.AutoIncrement = false;
				colvarBorradoLogico.IsNullable = false;
				colvarBorradoLogico.IsPrimaryKey = false;
				colvarBorradoLogico.IsForeignKey = false;
				colvarBorradoLogico.IsReadOnly = false;
				colvarBorradoLogico.DefaultSetting = @"";
				colvarBorradoLogico.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBorradoLogico);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["pvdGestionGL"].AddSchema("C_CLASIFICACION_PROCESO",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("IdCClasificacionProceso")]
		[Bindable(true)]
		public int IdCClasificacionProceso 
		{
			get { return GetColumnValue<int>(Columns.IdCClasificacionProceso); }
			set { SetColumnValue(Columns.IdCClasificacionProceso, value); }
		}
		  
		[XmlAttribute("NombreClasificacionProceso")]
		[Bindable(true)]
		public string NombreClasificacionProceso 
		{
			get { return GetColumnValue<string>(Columns.NombreClasificacionProceso); }
			set { SetColumnValue(Columns.NombreClasificacionProceso, value); }
		}
		  
		[XmlAttribute("BorradoLogico")]
		[Bindable(true)]
		public bool BorradoLogico 
		{
			get { return GetColumnValue<bool>(Columns.BorradoLogico); }
			set { SetColumnValue(Columns.BorradoLogico, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public TransporteTerrestre.GestionGL.Datos.CMInstalacionesCollection CMInstalacionesRecords()
		{
			return new TransporteTerrestre.GestionGL.Datos.CMInstalacionesCollection().Where(CMInstalaciones.Columns.IdCClasificacionProceso, IdCClasificacionProceso).Load();
		}
		#endregion
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varNombreClasificacionProceso,bool varBorradoLogico)
		{
			CClasificacionProceso item = new CClasificacionProceso();
			
			item.NombreClasificacionProceso = varNombreClasificacionProceso;
			
			item.BorradoLogico = varBorradoLogico;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varIdCClasificacionProceso,string varNombreClasificacionProceso,bool varBorradoLogico)
		{
			CClasificacionProceso item = new CClasificacionProceso();
			
				item.IdCClasificacionProceso = varIdCClasificacionProceso;
			
				item.NombreClasificacionProceso = varNombreClasificacionProceso;
			
				item.BorradoLogico = varBorradoLogico;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdCClasificacionProcesoColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn NombreClasificacionProcesoColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn BorradoLogicoColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string IdCClasificacionProceso = @"ID_C_CLASIFICACION_PROCESO";
			 public static string NombreClasificacionProceso = @"NOMBRE_CLASIFICACION_PROCESO";
			 public static string BorradoLogico = @"BORRADO_LOGICO";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
