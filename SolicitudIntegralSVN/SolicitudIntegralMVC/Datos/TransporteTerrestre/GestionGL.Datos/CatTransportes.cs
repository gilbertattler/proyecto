using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace TransporteTerrestre.GestionGL.Datos
{
	/// <summary>
	/// Strongly-typed collection for the CatTransportes class.
	/// </summary>
    [Serializable]
	public partial class CatTransportesCollection : ActiveList<CatTransportes, CatTransportesCollection>
	{	   
		public CatTransportesCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>CatTransportesCollection</returns>
		public CatTransportesCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                CatTransportes o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the CatTransportes table.
	/// </summary>
	[Serializable]
	public partial class CatTransportes : ActiveRecord<CatTransportes>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public CatTransportes()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public CatTransportes(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public CatTransportes(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public CatTransportes(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("CatTransportes", TableType.Table, DataService.GetInstance("pvdGestionGL"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarIdTransporte = new TableSchema.TableColumn(schema);
				colvarIdTransporte.ColumnName = "IdTransporte";
				colvarIdTransporte.DataType = DbType.Int32;
				colvarIdTransporte.MaxLength = 0;
				colvarIdTransporte.AutoIncrement = true;
				colvarIdTransporte.IsNullable = false;
				colvarIdTransporte.IsPrimaryKey = true;
				colvarIdTransporte.IsForeignKey = false;
				colvarIdTransporte.IsReadOnly = false;
				colvarIdTransporte.DefaultSetting = @"";
				colvarIdTransporte.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdTransporte);
				
				TableSchema.TableColumn colvarFkTipoTransporte = new TableSchema.TableColumn(schema);
				colvarFkTipoTransporte.ColumnName = "FkTipoTransporte";
				colvarFkTipoTransporte.DataType = DbType.Int32;
				colvarFkTipoTransporte.MaxLength = 0;
				colvarFkTipoTransporte.AutoIncrement = false;
				colvarFkTipoTransporte.IsNullable = false;
				colvarFkTipoTransporte.IsPrimaryKey = false;
				colvarFkTipoTransporte.IsForeignKey = false;
				colvarFkTipoTransporte.IsReadOnly = false;
				colvarFkTipoTransporte.DefaultSetting = @"";
				colvarFkTipoTransporte.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFkTipoTransporte);
				
				TableSchema.TableColumn colvarCveTransporte = new TableSchema.TableColumn(schema);
				colvarCveTransporte.ColumnName = "CveTransporte";
				colvarCveTransporte.DataType = DbType.AnsiString;
				colvarCveTransporte.MaxLength = 20;
				colvarCveTransporte.AutoIncrement = false;
				colvarCveTransporte.IsNullable = false;
				colvarCveTransporte.IsPrimaryKey = false;
				colvarCveTransporte.IsForeignKey = false;
				colvarCveTransporte.IsReadOnly = false;
				colvarCveTransporte.DefaultSetting = @"";
				colvarCveTransporte.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCveTransporte);
				
				TableSchema.TableColumn colvarPlaca = new TableSchema.TableColumn(schema);
				colvarPlaca.ColumnName = "Placa";
				colvarPlaca.DataType = DbType.AnsiString;
				colvarPlaca.MaxLength = 15;
				colvarPlaca.AutoIncrement = false;
				colvarPlaca.IsNullable = false;
				colvarPlaca.IsPrimaryKey = false;
				colvarPlaca.IsForeignKey = false;
				colvarPlaca.IsReadOnly = false;
				colvarPlaca.DefaultSetting = @"";
				colvarPlaca.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPlaca);
				
				TableSchema.TableColumn colvarMarca = new TableSchema.TableColumn(schema);
				colvarMarca.ColumnName = "Marca";
				colvarMarca.DataType = DbType.AnsiString;
				colvarMarca.MaxLength = 20;
				colvarMarca.AutoIncrement = false;
				colvarMarca.IsNullable = false;
				colvarMarca.IsPrimaryKey = false;
				colvarMarca.IsForeignKey = false;
				colvarMarca.IsReadOnly = false;
				colvarMarca.DefaultSetting = @"";
				colvarMarca.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMarca);
				
				TableSchema.TableColumn colvarModelo = new TableSchema.TableColumn(schema);
				colvarModelo.ColumnName = "Modelo";
				colvarModelo.DataType = DbType.AnsiString;
				colvarModelo.MaxLength = 10;
				colvarModelo.AutoIncrement = false;
				colvarModelo.IsNullable = false;
				colvarModelo.IsPrimaryKey = false;
				colvarModelo.IsForeignKey = false;
				colvarModelo.IsReadOnly = false;
				colvarModelo.DefaultSetting = @"";
				colvarModelo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModelo);
				
				TableSchema.TableColumn colvarCapacidad = new TableSchema.TableColumn(schema);
				colvarCapacidad.ColumnName = "Capacidad";
				colvarCapacidad.DataType = DbType.Int32;
				colvarCapacidad.MaxLength = 0;
				colvarCapacidad.AutoIncrement = false;
				colvarCapacidad.IsNullable = false;
				colvarCapacidad.IsPrimaryKey = false;
				colvarCapacidad.IsForeignKey = false;
				colvarCapacidad.IsReadOnly = false;
				colvarCapacidad.DefaultSetting = @"";
				colvarCapacidad.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCapacidad);
				
				TableSchema.TableColumn colvarTarjetaCirculacion = new TableSchema.TableColumn(schema);
				colvarTarjetaCirculacion.ColumnName = "TarjetaCirculacion";
				colvarTarjetaCirculacion.DataType = DbType.AnsiString;
				colvarTarjetaCirculacion.MaxLength = 20;
				colvarTarjetaCirculacion.AutoIncrement = false;
				colvarTarjetaCirculacion.IsNullable = false;
				colvarTarjetaCirculacion.IsPrimaryKey = false;
				colvarTarjetaCirculacion.IsForeignKey = false;
				colvarTarjetaCirculacion.IsReadOnly = false;
				colvarTarjetaCirculacion.DefaultSetting = @"";
				colvarTarjetaCirculacion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTarjetaCirculacion);
				
				TableSchema.TableColumn colvarVigenciaTarjetaCirculacion = new TableSchema.TableColumn(schema);
				colvarVigenciaTarjetaCirculacion.ColumnName = "VigenciaTarjetaCirculacion";
				colvarVigenciaTarjetaCirculacion.DataType = DbType.DateTime;
				colvarVigenciaTarjetaCirculacion.MaxLength = 0;
				colvarVigenciaTarjetaCirculacion.AutoIncrement = false;
				colvarVigenciaTarjetaCirculacion.IsNullable = false;
				colvarVigenciaTarjetaCirculacion.IsPrimaryKey = false;
				colvarVigenciaTarjetaCirculacion.IsForeignKey = false;
				colvarVigenciaTarjetaCirculacion.IsReadOnly = false;
				colvarVigenciaTarjetaCirculacion.DefaultSetting = @"";
				colvarVigenciaTarjetaCirculacion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVigenciaTarjetaCirculacion);
				
				TableSchema.TableColumn colvarTenencia = new TableSchema.TableColumn(schema);
				colvarTenencia.ColumnName = "Tenencia";
				colvarTenencia.DataType = DbType.AnsiString;
				colvarTenencia.MaxLength = 15;
				colvarTenencia.AutoIncrement = false;
				colvarTenencia.IsNullable = false;
				colvarTenencia.IsPrimaryKey = false;
				colvarTenencia.IsForeignKey = false;
				colvarTenencia.IsReadOnly = false;
				colvarTenencia.DefaultSetting = @"";
				colvarTenencia.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTenencia);
				
				TableSchema.TableColumn colvarVigenciaTenencia = new TableSchema.TableColumn(schema);
				colvarVigenciaTenencia.ColumnName = "VigenciaTenencia";
				colvarVigenciaTenencia.DataType = DbType.DateTime;
				colvarVigenciaTenencia.MaxLength = 0;
				colvarVigenciaTenencia.AutoIncrement = false;
				colvarVigenciaTenencia.IsNullable = false;
				colvarVigenciaTenencia.IsPrimaryKey = false;
				colvarVigenciaTenencia.IsForeignKey = false;
				colvarVigenciaTenencia.IsReadOnly = false;
				colvarVigenciaTenencia.DefaultSetting = @"";
				colvarVigenciaTenencia.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVigenciaTenencia);
				
				TableSchema.TableColumn colvarNumeroSerie = new TableSchema.TableColumn(schema);
				colvarNumeroSerie.ColumnName = "NumeroSerie";
				colvarNumeroSerie.DataType = DbType.AnsiString;
				colvarNumeroSerie.MaxLength = 50;
				colvarNumeroSerie.AutoIncrement = false;
				colvarNumeroSerie.IsNullable = false;
				colvarNumeroSerie.IsPrimaryKey = false;
				colvarNumeroSerie.IsForeignKey = false;
				colvarNumeroSerie.IsReadOnly = false;
				colvarNumeroSerie.DefaultSetting = @"";
				colvarNumeroSerie.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNumeroSerie);
				
				TableSchema.TableColumn colvarVigenciaSeguro = new TableSchema.TableColumn(schema);
				colvarVigenciaSeguro.ColumnName = "VigenciaSeguro";
				colvarVigenciaSeguro.DataType = DbType.DateTime;
				colvarVigenciaSeguro.MaxLength = 0;
				colvarVigenciaSeguro.AutoIncrement = false;
				colvarVigenciaSeguro.IsNullable = false;
				colvarVigenciaSeguro.IsPrimaryKey = false;
				colvarVigenciaSeguro.IsForeignKey = false;
				colvarVigenciaSeguro.IsReadOnly = false;
				colvarVigenciaSeguro.DefaultSetting = @"";
				colvarVigenciaSeguro.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVigenciaSeguro);
				
				TableSchema.TableColumn colvarCompaniaSeguro = new TableSchema.TableColumn(schema);
				colvarCompaniaSeguro.ColumnName = "CompaniaSeguro";
				colvarCompaniaSeguro.DataType = DbType.AnsiString;
				colvarCompaniaSeguro.MaxLength = 40;
				colvarCompaniaSeguro.AutoIncrement = false;
				colvarCompaniaSeguro.IsNullable = false;
				colvarCompaniaSeguro.IsPrimaryKey = false;
				colvarCompaniaSeguro.IsForeignKey = false;
				colvarCompaniaSeguro.IsReadOnly = false;
				colvarCompaniaSeguro.DefaultSetting = @"";
				colvarCompaniaSeguro.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompaniaSeguro);
				
				TableSchema.TableColumn colvarCartaFactura = new TableSchema.TableColumn(schema);
				colvarCartaFactura.ColumnName = "CartaFactura";
				colvarCartaFactura.DataType = DbType.AnsiString;
				colvarCartaFactura.MaxLength = 30;
				colvarCartaFactura.AutoIncrement = false;
				colvarCartaFactura.IsNullable = false;
				colvarCartaFactura.IsPrimaryKey = false;
				colvarCartaFactura.IsForeignKey = false;
				colvarCartaFactura.IsReadOnly = false;
				colvarCartaFactura.DefaultSetting = @"";
				colvarCartaFactura.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCartaFactura);
				
				TableSchema.TableColumn colvarContratoSAP = new TableSchema.TableColumn(schema);
				colvarContratoSAP.ColumnName = "ContratoSAP";
				colvarContratoSAP.DataType = DbType.AnsiString;
				colvarContratoSAP.MaxLength = 20;
				colvarContratoSAP.AutoIncrement = false;
				colvarContratoSAP.IsNullable = false;
				colvarContratoSAP.IsPrimaryKey = false;
				colvarContratoSAP.IsForeignKey = false;
				colvarContratoSAP.IsReadOnly = false;
				colvarContratoSAP.DefaultSetting = @"";
				colvarContratoSAP.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContratoSAP);
				
				TableSchema.TableColumn colvarConvenio = new TableSchema.TableColumn(schema);
				colvarConvenio.ColumnName = "Convenio";
				colvarConvenio.DataType = DbType.AnsiString;
				colvarConvenio.MaxLength = 20;
				colvarConvenio.AutoIncrement = false;
				colvarConvenio.IsNullable = false;
				colvarConvenio.IsPrimaryKey = false;
				colvarConvenio.IsForeignKey = false;
				colvarConvenio.IsReadOnly = false;
				colvarConvenio.DefaultSetting = @"";
				colvarConvenio.ForeignKeyTableName = "";
				schema.Columns.Add(colvarConvenio);
				
				TableSchema.TableColumn colvarCosto = new TableSchema.TableColumn(schema);
				colvarCosto.ColumnName = "Costo";
				colvarCosto.DataType = DbType.AnsiString;
				colvarCosto.MaxLength = 15;
				colvarCosto.AutoIncrement = false;
				colvarCosto.IsNullable = false;
				colvarCosto.IsPrimaryKey = false;
				colvarCosto.IsForeignKey = false;
				colvarCosto.IsReadOnly = false;
				colvarCosto.DefaultSetting = @"";
				colvarCosto.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCosto);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "Status";
				colvarStatus.DataType = DbType.AnsiString;
				colvarStatus.MaxLength = 30;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarBorradoLogico = new TableSchema.TableColumn(schema);
				colvarBorradoLogico.ColumnName = "BorradoLogico";
				colvarBorradoLogico.DataType = DbType.Boolean;
				colvarBorradoLogico.MaxLength = 0;
				colvarBorradoLogico.AutoIncrement = false;
				colvarBorradoLogico.IsNullable = false;
				colvarBorradoLogico.IsPrimaryKey = false;
				colvarBorradoLogico.IsForeignKey = false;
				colvarBorradoLogico.IsReadOnly = false;
				colvarBorradoLogico.DefaultSetting = @"";
				colvarBorradoLogico.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBorradoLogico);
				
				TableSchema.TableColumn colvarNodo = new TableSchema.TableColumn(schema);
				colvarNodo.ColumnName = "Nodo";
				colvarNodo.DataType = DbType.String;
				colvarNodo.MaxLength = 50;
				colvarNodo.AutoIncrement = false;
				colvarNodo.IsNullable = true;
				colvarNodo.IsPrimaryKey = false;
				colvarNodo.IsForeignKey = false;
				colvarNodo.IsReadOnly = false;
				colvarNodo.DefaultSetting = @"";
				colvarNodo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNodo);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["pvdGestionGL"].AddSchema("CatTransportes",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("IdTransporte")]
		[Bindable(true)]
		public int IdTransporte 
		{
			get { return GetColumnValue<int>(Columns.IdTransporte); }
			set { SetColumnValue(Columns.IdTransporte, value); }
		}
		  
		[XmlAttribute("FkTipoTransporte")]
		[Bindable(true)]
		public int FkTipoTransporte 
		{
			get { return GetColumnValue<int>(Columns.FkTipoTransporte); }
			set { SetColumnValue(Columns.FkTipoTransporte, value); }
		}
		  
		[XmlAttribute("CveTransporte")]
		[Bindable(true)]
		public string CveTransporte 
		{
			get { return GetColumnValue<string>(Columns.CveTransporte); }
			set { SetColumnValue(Columns.CveTransporte, value); }
		}
		  
		[XmlAttribute("Placa")]
		[Bindable(true)]
		public string Placa 
		{
			get { return GetColumnValue<string>(Columns.Placa); }
			set { SetColumnValue(Columns.Placa, value); }
		}
		  
		[XmlAttribute("Marca")]
		[Bindable(true)]
		public string Marca 
		{
			get { return GetColumnValue<string>(Columns.Marca); }
			set { SetColumnValue(Columns.Marca, value); }
		}
		  
		[XmlAttribute("Modelo")]
		[Bindable(true)]
		public string Modelo 
		{
			get { return GetColumnValue<string>(Columns.Modelo); }
			set { SetColumnValue(Columns.Modelo, value); }
		}
		  
		[XmlAttribute("Capacidad")]
		[Bindable(true)]
		public int Capacidad 
		{
			get { return GetColumnValue<int>(Columns.Capacidad); }
			set { SetColumnValue(Columns.Capacidad, value); }
		}
		  
		[XmlAttribute("TarjetaCirculacion")]
		[Bindable(true)]
		public string TarjetaCirculacion 
		{
			get { return GetColumnValue<string>(Columns.TarjetaCirculacion); }
			set { SetColumnValue(Columns.TarjetaCirculacion, value); }
		}
		  
		[XmlAttribute("VigenciaTarjetaCirculacion")]
		[Bindable(true)]
		public DateTime VigenciaTarjetaCirculacion 
		{
			get { return GetColumnValue<DateTime>(Columns.VigenciaTarjetaCirculacion); }
			set { SetColumnValue(Columns.VigenciaTarjetaCirculacion, value); }
		}
		  
		[XmlAttribute("Tenencia")]
		[Bindable(true)]
		public string Tenencia 
		{
			get { return GetColumnValue<string>(Columns.Tenencia); }
			set { SetColumnValue(Columns.Tenencia, value); }
		}
		  
		[XmlAttribute("VigenciaTenencia")]
		[Bindable(true)]
		public DateTime VigenciaTenencia 
		{
			get { return GetColumnValue<DateTime>(Columns.VigenciaTenencia); }
			set { SetColumnValue(Columns.VigenciaTenencia, value); }
		}
		  
		[XmlAttribute("NumeroSerie")]
		[Bindable(true)]
		public string NumeroSerie 
		{
			get { return GetColumnValue<string>(Columns.NumeroSerie); }
			set { SetColumnValue(Columns.NumeroSerie, value); }
		}
		  
		[XmlAttribute("VigenciaSeguro")]
		[Bindable(true)]
		public DateTime VigenciaSeguro 
		{
			get { return GetColumnValue<DateTime>(Columns.VigenciaSeguro); }
			set { SetColumnValue(Columns.VigenciaSeguro, value); }
		}
		  
		[XmlAttribute("CompaniaSeguro")]
		[Bindable(true)]
		public string CompaniaSeguro 
		{
			get { return GetColumnValue<string>(Columns.CompaniaSeguro); }
			set { SetColumnValue(Columns.CompaniaSeguro, value); }
		}
		  
		[XmlAttribute("CartaFactura")]
		[Bindable(true)]
		public string CartaFactura 
		{
			get { return GetColumnValue<string>(Columns.CartaFactura); }
			set { SetColumnValue(Columns.CartaFactura, value); }
		}
		  
		[XmlAttribute("ContratoSAP")]
		[Bindable(true)]
		public string ContratoSAP 
		{
			get { return GetColumnValue<string>(Columns.ContratoSAP); }
			set { SetColumnValue(Columns.ContratoSAP, value); }
		}
		  
		[XmlAttribute("Convenio")]
		[Bindable(true)]
		public string Convenio 
		{
			get { return GetColumnValue<string>(Columns.Convenio); }
			set { SetColumnValue(Columns.Convenio, value); }
		}
		  
		[XmlAttribute("Costo")]
		[Bindable(true)]
		public string Costo 
		{
			get { return GetColumnValue<string>(Columns.Costo); }
			set { SetColumnValue(Columns.Costo, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public string Status 
		{
			get { return GetColumnValue<string>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("BorradoLogico")]
		[Bindable(true)]
		public bool BorradoLogico 
		{
			get { return GetColumnValue<bool>(Columns.BorradoLogico); }
			set { SetColumnValue(Columns.BorradoLogico, value); }
		}
		  
		[XmlAttribute("Nodo")]
		[Bindable(true)]
		public string Nodo 
		{
			get { return GetColumnValue<string>(Columns.Nodo); }
			set { SetColumnValue(Columns.Nodo, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(int varFkTipoTransporte,string varCveTransporte,string varPlaca,string varMarca,string varModelo,int varCapacidad,string varTarjetaCirculacion,DateTime varVigenciaTarjetaCirculacion,string varTenencia,DateTime varVigenciaTenencia,string varNumeroSerie,DateTime varVigenciaSeguro,string varCompaniaSeguro,string varCartaFactura,string varContratoSAP,string varConvenio,string varCosto,string varStatus,bool varBorradoLogico,string varNodo)
		{
			CatTransportes item = new CatTransportes();
			
			item.FkTipoTransporte = varFkTipoTransporte;
			
			item.CveTransporte = varCveTransporte;
			
			item.Placa = varPlaca;
			
			item.Marca = varMarca;
			
			item.Modelo = varModelo;
			
			item.Capacidad = varCapacidad;
			
			item.TarjetaCirculacion = varTarjetaCirculacion;
			
			item.VigenciaTarjetaCirculacion = varVigenciaTarjetaCirculacion;
			
			item.Tenencia = varTenencia;
			
			item.VigenciaTenencia = varVigenciaTenencia;
			
			item.NumeroSerie = varNumeroSerie;
			
			item.VigenciaSeguro = varVigenciaSeguro;
			
			item.CompaniaSeguro = varCompaniaSeguro;
			
			item.CartaFactura = varCartaFactura;
			
			item.ContratoSAP = varContratoSAP;
			
			item.Convenio = varConvenio;
			
			item.Costo = varCosto;
			
			item.Status = varStatus;
			
			item.BorradoLogico = varBorradoLogico;
			
			item.Nodo = varNodo;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varIdTransporte,int varFkTipoTransporte,string varCveTransporte,string varPlaca,string varMarca,string varModelo,int varCapacidad,string varTarjetaCirculacion,DateTime varVigenciaTarjetaCirculacion,string varTenencia,DateTime varVigenciaTenencia,string varNumeroSerie,DateTime varVigenciaSeguro,string varCompaniaSeguro,string varCartaFactura,string varContratoSAP,string varConvenio,string varCosto,string varStatus,bool varBorradoLogico,string varNodo)
		{
			CatTransportes item = new CatTransportes();
			
				item.IdTransporte = varIdTransporte;
			
				item.FkTipoTransporte = varFkTipoTransporte;
			
				item.CveTransporte = varCveTransporte;
			
				item.Placa = varPlaca;
			
				item.Marca = varMarca;
			
				item.Modelo = varModelo;
			
				item.Capacidad = varCapacidad;
			
				item.TarjetaCirculacion = varTarjetaCirculacion;
			
				item.VigenciaTarjetaCirculacion = varVigenciaTarjetaCirculacion;
			
				item.Tenencia = varTenencia;
			
				item.VigenciaTenencia = varVigenciaTenencia;
			
				item.NumeroSerie = varNumeroSerie;
			
				item.VigenciaSeguro = varVigenciaSeguro;
			
				item.CompaniaSeguro = varCompaniaSeguro;
			
				item.CartaFactura = varCartaFactura;
			
				item.ContratoSAP = varContratoSAP;
			
				item.Convenio = varConvenio;
			
				item.Costo = varCosto;
			
				item.Status = varStatus;
			
				item.BorradoLogico = varBorradoLogico;
			
				item.Nodo = varNodo;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdTransporteColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn FkTipoTransporteColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CveTransporteColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn PlacaColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn MarcaColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ModeloColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CapacidadColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn TarjetaCirculacionColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn VigenciaTarjetaCirculacionColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn TenenciaColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn VigenciaTenenciaColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn NumeroSerieColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn VigenciaSeguroColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn CompaniaSeguroColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn CartaFacturaColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn ContratoSAPColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn ConvenioColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn CostoColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn BorradoLogicoColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        public static TableSchema.TableColumn NodoColumn
        {
            get { return Schema.Columns[20]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string IdTransporte = @"IdTransporte";
			 public static string FkTipoTransporte = @"FkTipoTransporte";
			 public static string CveTransporte = @"CveTransporte";
			 public static string Placa = @"Placa";
			 public static string Marca = @"Marca";
			 public static string Modelo = @"Modelo";
			 public static string Capacidad = @"Capacidad";
			 public static string TarjetaCirculacion = @"TarjetaCirculacion";
			 public static string VigenciaTarjetaCirculacion = @"VigenciaTarjetaCirculacion";
			 public static string Tenencia = @"Tenencia";
			 public static string VigenciaTenencia = @"VigenciaTenencia";
			 public static string NumeroSerie = @"NumeroSerie";
			 public static string VigenciaSeguro = @"VigenciaSeguro";
			 public static string CompaniaSeguro = @"CompaniaSeguro";
			 public static string CartaFactura = @"CartaFactura";
			 public static string ContratoSAP = @"ContratoSAP";
			 public static string Convenio = @"Convenio";
			 public static string Costo = @"Costo";
			 public static string Status = @"Status";
			 public static string BorradoLogico = @"BorradoLogico";
			 public static string Nodo = @"Nodo";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}
