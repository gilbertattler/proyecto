using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace TransporteTerrestre.GestionGL.Datos
{
	/// <summary>
	/// Strongly-typed collection for the DirTabMigracionCuentaMayor class.
	/// </summary>
    [Serializable]
	public partial class DirTabMigracionCuentaMayorCollection : ActiveList<DirTabMigracionCuentaMayor, DirTabMigracionCuentaMayorCollection>
	{	   
		public DirTabMigracionCuentaMayorCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>DirTabMigracionCuentaMayorCollection</returns>
		public DirTabMigracionCuentaMayorCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                DirTabMigracionCuentaMayor o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the DIR_TAB_MIGRACION_CUENTA_MAYOR table.
	/// </summary>
	[Serializable]
	public partial class DirTabMigracionCuentaMayor : ActiveRecord<DirTabMigracionCuentaMayor>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public DirTabMigracionCuentaMayor()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public DirTabMigracionCuentaMayor(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public DirTabMigracionCuentaMayor(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public DirTabMigracionCuentaMayor(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("DIR_TAB_MIGRACION_CUENTA_MAYOR", TableType.Table, DataService.GetInstance("pvdGestionGL"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarPkIdCuentaMayor = new TableSchema.TableColumn(schema);
				colvarPkIdCuentaMayor.ColumnName = "PK_ID_CUENTA_MAYOR";
				colvarPkIdCuentaMayor.DataType = DbType.Int32;
				colvarPkIdCuentaMayor.MaxLength = 0;
				colvarPkIdCuentaMayor.AutoIncrement = true;
				colvarPkIdCuentaMayor.IsNullable = false;
				colvarPkIdCuentaMayor.IsPrimaryKey = true;
				colvarPkIdCuentaMayor.IsForeignKey = false;
				colvarPkIdCuentaMayor.IsReadOnly = false;
				colvarPkIdCuentaMayor.DefaultSetting = @"";
				colvarPkIdCuentaMayor.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPkIdCuentaMayor);
				
				TableSchema.TableColumn colvarCuentaMayor = new TableSchema.TableColumn(schema);
				colvarCuentaMayor.ColumnName = "CUENTA_MAYOR";
				colvarCuentaMayor.DataType = DbType.AnsiString;
				colvarCuentaMayor.MaxLength = 50;
				colvarCuentaMayor.AutoIncrement = false;
				colvarCuentaMayor.IsNullable = false;
				colvarCuentaMayor.IsPrimaryKey = false;
				colvarCuentaMayor.IsForeignKey = false;
				colvarCuentaMayor.IsReadOnly = false;
				colvarCuentaMayor.DefaultSetting = @"";
				colvarCuentaMayor.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCuentaMayor);
				
				TableSchema.TableColumn colvarDescripcion = new TableSchema.TableColumn(schema);
				colvarDescripcion.ColumnName = "DESCRIPCION";
				colvarDescripcion.DataType = DbType.AnsiString;
				colvarDescripcion.MaxLength = 100;
				colvarDescripcion.AutoIncrement = false;
				colvarDescripcion.IsNullable = false;
				colvarDescripcion.IsPrimaryKey = false;
				colvarDescripcion.IsForeignKey = false;
				colvarDescripcion.IsReadOnly = false;
				colvarDescripcion.DefaultSetting = @"";
				colvarDescripcion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDescripcion);
				
				TableSchema.TableColumn colvarPosicion = new TableSchema.TableColumn(schema);
				colvarPosicion.ColumnName = "POSICION";
				colvarPosicion.DataType = DbType.AnsiString;
				colvarPosicion.MaxLength = 50;
				colvarPosicion.AutoIncrement = false;
				colvarPosicion.IsNullable = false;
				colvarPosicion.IsPrimaryKey = false;
				colvarPosicion.IsForeignKey = false;
				colvarPosicion.IsReadOnly = false;
				colvarPosicion.DefaultSetting = @"";
				colvarPosicion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPosicion);
				
				TableSchema.TableColumn colvarValido = new TableSchema.TableColumn(schema);
				colvarValido.ColumnName = "VALIDO";
				colvarValido.DataType = DbType.Boolean;
				colvarValido.MaxLength = 0;
				colvarValido.AutoIncrement = false;
				colvarValido.IsNullable = false;
				colvarValido.IsPrimaryKey = false;
				colvarValido.IsForeignKey = false;
				colvarValido.IsReadOnly = false;
				
						colvarValido.DefaultSetting = @"((0))";
				colvarValido.ForeignKeyTableName = "";
				schema.Columns.Add(colvarValido);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["pvdGestionGL"].AddSchema("DIR_TAB_MIGRACION_CUENTA_MAYOR",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("PkIdCuentaMayor")]
		[Bindable(true)]
		public int PkIdCuentaMayor 
		{
			get { return GetColumnValue<int>(Columns.PkIdCuentaMayor); }
			set { SetColumnValue(Columns.PkIdCuentaMayor, value); }
		}
		  
		[XmlAttribute("CuentaMayor")]
		[Bindable(true)]
		public string CuentaMayor 
		{
			get { return GetColumnValue<string>(Columns.CuentaMayor); }
			set { SetColumnValue(Columns.CuentaMayor, value); }
		}
		  
		[XmlAttribute("Descripcion")]
		[Bindable(true)]
		public string Descripcion 
		{
			get { return GetColumnValue<string>(Columns.Descripcion); }
			set { SetColumnValue(Columns.Descripcion, value); }
		}
		  
		[XmlAttribute("Posicion")]
		[Bindable(true)]
		public string Posicion 
		{
			get { return GetColumnValue<string>(Columns.Posicion); }
			set { SetColumnValue(Columns.Posicion, value); }
		}
		  
		[XmlAttribute("Valido")]
		[Bindable(true)]
		public bool Valido 
		{
			get { return GetColumnValue<bool>(Columns.Valido); }
			set { SetColumnValue(Columns.Valido, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varCuentaMayor,string varDescripcion,string varPosicion,bool varValido)
		{
			DirTabMigracionCuentaMayor item = new DirTabMigracionCuentaMayor();
			
			item.CuentaMayor = varCuentaMayor;
			
			item.Descripcion = varDescripcion;
			
			item.Posicion = varPosicion;
			
			item.Valido = varValido;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varPkIdCuentaMayor,string varCuentaMayor,string varDescripcion,string varPosicion,bool varValido)
		{
			DirTabMigracionCuentaMayor item = new DirTabMigracionCuentaMayor();
			
				item.PkIdCuentaMayor = varPkIdCuentaMayor;
			
				item.CuentaMayor = varCuentaMayor;
			
				item.Descripcion = varDescripcion;
			
				item.Posicion = varPosicion;
			
				item.Valido = varValido;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn PkIdCuentaMayorColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CuentaMayorColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn DescripcionColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn PosicionColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ValidoColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string PkIdCuentaMayor = @"PK_ID_CUENTA_MAYOR";
			 public static string CuentaMayor = @"CUENTA_MAYOR";
			 public static string Descripcion = @"DESCRIPCION";
			 public static string Posicion = @"POSICION";
			 public static string Valido = @"VALIDO";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}
