using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace TransporteTerrestre.GestionGL.Datos
{
    /// <summary>
    /// Controller class for C_D_AUTORIZADORES_CONTINGENCIA
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class CDAutorizadoresContingenciaController
    {
        // Preload our schema..
        CDAutorizadoresContingencia thisSchemaLoad = new CDAutorizadoresContingencia();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public CDAutorizadoresContingenciaCollection FetchAll()
        {
            CDAutorizadoresContingenciaCollection coll = new CDAutorizadoresContingenciaCollection();
            Query qry = new Query(CDAutorizadoresContingencia.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public CDAutorizadoresContingenciaCollection FetchByID(object IdCDAutorizadorContingencia)
        {
            CDAutorizadoresContingenciaCollection coll = new CDAutorizadoresContingenciaCollection().Where("ID_C_D_AUTORIZADOR_CONTINGENCIA", IdCDAutorizadorContingencia).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public CDAutorizadoresContingenciaCollection FetchByQuery(Query qry)
        {
            CDAutorizadoresContingenciaCollection coll = new CDAutorizadoresContingenciaCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdCDAutorizadorContingencia)
        {
            return (CDAutorizadoresContingencia.Delete(IdCDAutorizadorContingencia) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdCDAutorizadorContingencia)
        {
            return (CDAutorizadoresContingencia.Destroy(IdCDAutorizadorContingencia) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(int IdCMAutorizadorContingencia,int IdAreaServicio,int IdCSubdivision,DateTime FecActua,string FicActua,bool BorradoLogico)
	    {
		    CDAutorizadoresContingencia item = new CDAutorizadoresContingencia();
		    
            item.IdCMAutorizadorContingencia = IdCMAutorizadorContingencia;
            
            item.IdAreaServicio = IdAreaServicio;
            
            item.IdCSubdivision = IdCSubdivision;
            
            item.FecActua = FecActua;
            
            item.FicActua = FicActua;
            
            item.BorradoLogico = BorradoLogico;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdCDAutorizadorContingencia,int IdCMAutorizadorContingencia,int IdAreaServicio,int IdCSubdivision,DateTime FecActua,string FicActua,bool BorradoLogico)
	    {
		    CDAutorizadoresContingencia item = new CDAutorizadoresContingencia();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdCDAutorizadorContingencia = IdCDAutorizadorContingencia;
				
			item.IdCMAutorizadorContingencia = IdCMAutorizadorContingencia;
				
			item.IdAreaServicio = IdAreaServicio;
				
			item.IdCSubdivision = IdCSubdivision;
				
			item.FecActua = FecActua;
				
			item.FicActua = FicActua;
				
			item.BorradoLogico = BorradoLogico;
				
	        item.Save(UserName);
	    }
    }
}
