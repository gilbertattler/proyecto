using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace TransporteTerrestre.GestionGL.Datos
{
    /// <summary>
    /// Controller class for DIR_CAT_PROGRAMA_PRESUPUESTAL
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class DirCatProgramaPresupuestalController
    {
        // Preload our schema..
        DirCatProgramaPresupuestal thisSchemaLoad = new DirCatProgramaPresupuestal();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public DirCatProgramaPresupuestalCollection FetchAll()
        {
            DirCatProgramaPresupuestalCollection coll = new DirCatProgramaPresupuestalCollection();
            Query qry = new Query(DirCatProgramaPresupuestal.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public DirCatProgramaPresupuestalCollection FetchByID(object PkIdProgramaPresupuestal)
        {
            DirCatProgramaPresupuestalCollection coll = new DirCatProgramaPresupuestalCollection().Where("PK_ID_PROGRAMA_PRESUPUESTAL", PkIdProgramaPresupuestal).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public DirCatProgramaPresupuestalCollection FetchByQuery(Query qry)
        {
            DirCatProgramaPresupuestalCollection coll = new DirCatProgramaPresupuestalCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object PkIdProgramaPresupuestal)
        {
            return (DirCatProgramaPresupuestal.Delete(PkIdProgramaPresupuestal) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object PkIdProgramaPresupuestal)
        {
            return (DirCatProgramaPresupuestal.Destroy(PkIdProgramaPresupuestal) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string CveProgramaPresupuestal,string DescripcionProgramaPresupuestal,string Origen,bool BorradoLogico)
	    {
		    DirCatProgramaPresupuestal item = new DirCatProgramaPresupuestal();
		    
            item.CveProgramaPresupuestal = CveProgramaPresupuestal;
            
            item.DescripcionProgramaPresupuestal = DescripcionProgramaPresupuestal;
            
            item.Origen = Origen;
            
            item.BorradoLogico = BorradoLogico;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int PkIdProgramaPresupuestal,string CveProgramaPresupuestal,string DescripcionProgramaPresupuestal,string Origen,bool BorradoLogico)
	    {
		    DirCatProgramaPresupuestal item = new DirCatProgramaPresupuestal();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.PkIdProgramaPresupuestal = PkIdProgramaPresupuestal;
				
			item.CveProgramaPresupuestal = CveProgramaPresupuestal;
				
			item.DescripcionProgramaPresupuestal = DescripcionProgramaPresupuestal;
				
			item.Origen = Origen;
				
			item.BorradoLogico = BorradoLogico;
				
	        item.Save(UserName);
	    }
    }
}
