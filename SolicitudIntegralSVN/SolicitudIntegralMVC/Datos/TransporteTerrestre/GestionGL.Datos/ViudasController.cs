using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace TransporteTerrestre.GestionGL.Datos
{
    /// <summary>
    /// Controller class for Viudas
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class ViudasController
    {
        // Preload our schema..
        Viudas thisSchemaLoad = new Viudas();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public ViudasCollection FetchAll()
        {
            ViudasCollection coll = new ViudasCollection();
            Query qry = new Query(Viudas.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public ViudasCollection FetchByID(object IdViudas)
        {
            ViudasCollection coll = new ViudasCollection().Where("IdViudas", IdViudas).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public ViudasCollection FetchByQuery(Query qry)
        {
            ViudasCollection coll = new ViudasCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdViudas)
        {
            return (Viudas.Delete(IdViudas) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdViudas)
        {
            return (Viudas.Destroy(IdViudas) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string Siglas,string Observaciones,int? Estado,int? IdMInstalacion,string FichaActualizacion,DateTime? FechaActualizacion)
	    {
		    Viudas item = new Viudas();
		    
            item.Siglas = Siglas;
            
            item.Observaciones = Observaciones;
            
            item.Estado = Estado;
            
            item.IdMInstalacion = IdMInstalacion;
            
            item.FichaActualizacion = FichaActualizacion;
            
            item.FechaActualizacion = FechaActualizacion;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdViudas,string Siglas,string Observaciones,int? Estado,int? IdMInstalacion,string FichaActualizacion,DateTime? FechaActualizacion)
	    {
		    Viudas item = new Viudas();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdViudas = IdViudas;
				
			item.Siglas = Siglas;
				
			item.Observaciones = Observaciones;
				
			item.Estado = Estado;
				
			item.IdMInstalacion = IdMInstalacion;
				
			item.FichaActualizacion = FichaActualizacion;
				
			item.FechaActualizacion = FechaActualizacion;
				
	        item.Save(UserName);
	    }
    }
}
