using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace TransporteTerrestre.GestionGL.Datos{
    /// <summary>
    /// Strongly-typed collection for the VwCMInstalacionesGral class.
    /// </summary>
    [Serializable]
    public partial class VwCMInstalacionesGralCollection : ReadOnlyList<VwCMInstalacionesGral, VwCMInstalacionesGralCollection>
    {        
        public VwCMInstalacionesGralCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the VW_C_M_INSTALACIONES_GRAL view.
    /// </summary>
    [Serializable]
    public partial class VwCMInstalacionesGral : ReadOnlyRecord<VwCMInstalacionesGral>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("VW_C_M_INSTALACIONES_GRAL", TableType.View, DataService.GetInstance("pvdGestionGL"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarIdCMInstalacion = new TableSchema.TableColumn(schema);
                colvarIdCMInstalacion.ColumnName = "ID_C_M_INSTALACION";
                colvarIdCMInstalacion.DataType = DbType.Int32;
                colvarIdCMInstalacion.MaxLength = 0;
                colvarIdCMInstalacion.AutoIncrement = false;
                colvarIdCMInstalacion.IsNullable = false;
                colvarIdCMInstalacion.IsPrimaryKey = false;
                colvarIdCMInstalacion.IsForeignKey = false;
                colvarIdCMInstalacion.IsReadOnly = false;
                
                schema.Columns.Add(colvarIdCMInstalacion);
                
                TableSchema.TableColumn colvarFkIdCMInstalacionUbicacionActual = new TableSchema.TableColumn(schema);
                colvarFkIdCMInstalacionUbicacionActual.ColumnName = "FK_ID_C_M_INSTALACION_UBICACION_ACTUAL";
                colvarFkIdCMInstalacionUbicacionActual.DataType = DbType.Int32;
                colvarFkIdCMInstalacionUbicacionActual.MaxLength = 0;
                colvarFkIdCMInstalacionUbicacionActual.AutoIncrement = false;
                colvarFkIdCMInstalacionUbicacionActual.IsNullable = true;
                colvarFkIdCMInstalacionUbicacionActual.IsPrimaryKey = false;
                colvarFkIdCMInstalacionUbicacionActual.IsForeignKey = false;
                colvarFkIdCMInstalacionUbicacionActual.IsReadOnly = false;
                
                schema.Columns.Add(colvarFkIdCMInstalacionUbicacionActual);
                
                TableSchema.TableColumn colvarIdCComplejo = new TableSchema.TableColumn(schema);
                colvarIdCComplejo.ColumnName = "ID_C_COMPLEJO";
                colvarIdCComplejo.DataType = DbType.Int32;
                colvarIdCComplejo.MaxLength = 0;
                colvarIdCComplejo.AutoIncrement = false;
                colvarIdCComplejo.IsNullable = true;
                colvarIdCComplejo.IsPrimaryKey = false;
                colvarIdCComplejo.IsForeignKey = false;
                colvarIdCComplejo.IsReadOnly = false;
                
                schema.Columns.Add(colvarIdCComplejo);
                
                TableSchema.TableColumn colvarIdCSector = new TableSchema.TableColumn(schema);
                colvarIdCSector.ColumnName = "ID_C_SECTOR";
                colvarIdCSector.DataType = DbType.Int32;
                colvarIdCSector.MaxLength = 0;
                colvarIdCSector.AutoIncrement = false;
                colvarIdCSector.IsNullable = true;
                colvarIdCSector.IsPrimaryKey = false;
                colvarIdCSector.IsForeignKey = false;
                colvarIdCSector.IsReadOnly = false;
                
                schema.Columns.Add(colvarIdCSector);
                
                TableSchema.TableColumn colvarSiglasInstalacionUbicacionActual = new TableSchema.TableColumn(schema);
                colvarSiglasInstalacionUbicacionActual.ColumnName = "SIGLAS_INSTALACION_UBICACION_ACTUAL";
                colvarSiglasInstalacionUbicacionActual.DataType = DbType.AnsiString;
                colvarSiglasInstalacionUbicacionActual.MaxLength = 6;
                colvarSiglasInstalacionUbicacionActual.AutoIncrement = false;
                colvarSiglasInstalacionUbicacionActual.IsNullable = true;
                colvarSiglasInstalacionUbicacionActual.IsPrimaryKey = false;
                colvarSiglasInstalacionUbicacionActual.IsForeignKey = false;
                colvarSiglasInstalacionUbicacionActual.IsReadOnly = false;
                
                schema.Columns.Add(colvarSiglasInstalacionUbicacionActual);
                
                TableSchema.TableColumn colvarNombreInstalacionUbicacionActual = new TableSchema.TableColumn(schema);
                colvarNombreInstalacionUbicacionActual.ColumnName = "NOMBRE_INSTALACION_UBICACION_ACTUAL";
                colvarNombreInstalacionUbicacionActual.DataType = DbType.AnsiString;
                colvarNombreInstalacionUbicacionActual.MaxLength = 80;
                colvarNombreInstalacionUbicacionActual.AutoIncrement = false;
                colvarNombreInstalacionUbicacionActual.IsNullable = true;
                colvarNombreInstalacionUbicacionActual.IsPrimaryKey = false;
                colvarNombreInstalacionUbicacionActual.IsForeignKey = false;
                colvarNombreInstalacionUbicacionActual.IsReadOnly = false;
                
                schema.Columns.Add(colvarNombreInstalacionUbicacionActual);
                
                TableSchema.TableColumn colvarCveRegion = new TableSchema.TableColumn(schema);
                colvarCveRegion.ColumnName = "CVE_REGION";
                colvarCveRegion.DataType = DbType.AnsiString;
                colvarCveRegion.MaxLength = 2;
                colvarCveRegion.AutoIncrement = false;
                colvarCveRegion.IsNullable = true;
                colvarCveRegion.IsPrimaryKey = false;
                colvarCveRegion.IsForeignKey = false;
                colvarCveRegion.IsReadOnly = false;
                
                schema.Columns.Add(colvarCveRegion);
                
                TableSchema.TableColumn colvarSiglasRegion = new TableSchema.TableColumn(schema);
                colvarSiglasRegion.ColumnName = "SIGLAS_REGION";
                colvarSiglasRegion.DataType = DbType.AnsiString;
                colvarSiglasRegion.MaxLength = 10;
                colvarSiglasRegion.AutoIncrement = false;
                colvarSiglasRegion.IsNullable = true;
                colvarSiglasRegion.IsPrimaryKey = false;
                colvarSiglasRegion.IsForeignKey = false;
                colvarSiglasRegion.IsReadOnly = false;
                
                schema.Columns.Add(colvarSiglasRegion);
                
                TableSchema.TableColumn colvarNombreRegion = new TableSchema.TableColumn(schema);
                colvarNombreRegion.ColumnName = "NOMBRE_REGION";
                colvarNombreRegion.DataType = DbType.AnsiString;
                colvarNombreRegion.MaxLength = 150;
                colvarNombreRegion.AutoIncrement = false;
                colvarNombreRegion.IsNullable = true;
                colvarNombreRegion.IsPrimaryKey = false;
                colvarNombreRegion.IsForeignKey = false;
                colvarNombreRegion.IsReadOnly = false;
                
                schema.Columns.Add(colvarNombreRegion);
                
                TableSchema.TableColumn colvarCveActivo = new TableSchema.TableColumn(schema);
                colvarCveActivo.ColumnName = "CVE_ACTIVO";
                colvarCveActivo.DataType = DbType.AnsiString;
                colvarCveActivo.MaxLength = 18;
                colvarCveActivo.AutoIncrement = false;
                colvarCveActivo.IsNullable = true;
                colvarCveActivo.IsPrimaryKey = false;
                colvarCveActivo.IsForeignKey = false;
                colvarCveActivo.IsReadOnly = false;
                
                schema.Columns.Add(colvarCveActivo);
                
                TableSchema.TableColumn colvarSiglasActivo = new TableSchema.TableColumn(schema);
                colvarSiglasActivo.ColumnName = "SIGLAS_ACTIVO";
                colvarSiglasActivo.DataType = DbType.AnsiString;
                colvarSiglasActivo.MaxLength = 50;
                colvarSiglasActivo.AutoIncrement = false;
                colvarSiglasActivo.IsNullable = true;
                colvarSiglasActivo.IsPrimaryKey = false;
                colvarSiglasActivo.IsForeignKey = false;
                colvarSiglasActivo.IsReadOnly = false;
                
                schema.Columns.Add(colvarSiglasActivo);
                
                TableSchema.TableColumn colvarNombreActivo = new TableSchema.TableColumn(schema);
                colvarNombreActivo.ColumnName = "NOMBRE_ACTIVO";
                colvarNombreActivo.DataType = DbType.AnsiString;
                colvarNombreActivo.MaxLength = 150;
                colvarNombreActivo.AutoIncrement = false;
                colvarNombreActivo.IsNullable = true;
                colvarNombreActivo.IsPrimaryKey = false;
                colvarNombreActivo.IsForeignKey = false;
                colvarNombreActivo.IsReadOnly = false;
                
                schema.Columns.Add(colvarNombreActivo);
                
                TableSchema.TableColumn colvarSiglasComplejo = new TableSchema.TableColumn(schema);
                colvarSiglasComplejo.ColumnName = "SIGLAS_COMPLEJO";
                colvarSiglasComplejo.DataType = DbType.AnsiString;
                colvarSiglasComplejo.MaxLength = 10;
                colvarSiglasComplejo.AutoIncrement = false;
                colvarSiglasComplejo.IsNullable = true;
                colvarSiglasComplejo.IsPrimaryKey = false;
                colvarSiglasComplejo.IsForeignKey = false;
                colvarSiglasComplejo.IsReadOnly = false;
                
                schema.Columns.Add(colvarSiglasComplejo);
                
                TableSchema.TableColumn colvarNombreComplejo = new TableSchema.TableColumn(schema);
                colvarNombreComplejo.ColumnName = "NOMBRE_COMPLEJO";
                colvarNombreComplejo.DataType = DbType.AnsiString;
                colvarNombreComplejo.MaxLength = 120;
                colvarNombreComplejo.AutoIncrement = false;
                colvarNombreComplejo.IsNullable = true;
                colvarNombreComplejo.IsPrimaryKey = false;
                colvarNombreComplejo.IsForeignKey = false;
                colvarNombreComplejo.IsReadOnly = false;
                
                schema.Columns.Add(colvarNombreComplejo);
                
                TableSchema.TableColumn colvarCveSector = new TableSchema.TableColumn(schema);
                colvarCveSector.ColumnName = "CVE_SECTOR";
                colvarCveSector.DataType = DbType.AnsiString;
                colvarCveSector.MaxLength = 10;
                colvarCveSector.AutoIncrement = false;
                colvarCveSector.IsNullable = true;
                colvarCveSector.IsPrimaryKey = false;
                colvarCveSector.IsForeignKey = false;
                colvarCveSector.IsReadOnly = false;
                
                schema.Columns.Add(colvarCveSector);
                
                TableSchema.TableColumn colvarNombreSector = new TableSchema.TableColumn(schema);
                colvarNombreSector.ColumnName = "NOMBRE_SECTOR";
                colvarNombreSector.DataType = DbType.AnsiString;
                colvarNombreSector.MaxLength = 60;
                colvarNombreSector.AutoIncrement = false;
                colvarNombreSector.IsNullable = true;
                colvarNombreSector.IsPrimaryKey = false;
                colvarNombreSector.IsForeignKey = false;
                colvarNombreSector.IsReadOnly = false;
                
                schema.Columns.Add(colvarNombreSector);
                
                TableSchema.TableColumn colvarSiglasInstalacion = new TableSchema.TableColumn(schema);
                colvarSiglasInstalacion.ColumnName = "SIGLAS_INSTALACION";
                colvarSiglasInstalacion.DataType = DbType.AnsiString;
                colvarSiglasInstalacion.MaxLength = 6;
                colvarSiglasInstalacion.AutoIncrement = false;
                colvarSiglasInstalacion.IsNullable = true;
                colvarSiglasInstalacion.IsPrimaryKey = false;
                colvarSiglasInstalacion.IsForeignKey = false;
                colvarSiglasInstalacion.IsReadOnly = false;
                
                schema.Columns.Add(colvarSiglasInstalacion);
                
                TableSchema.TableColumn colvarNombreInstalacion = new TableSchema.TableColumn(schema);
                colvarNombreInstalacion.ColumnName = "NOMBRE_INSTALACION";
                colvarNombreInstalacion.DataType = DbType.AnsiString;
                colvarNombreInstalacion.MaxLength = 80;
                colvarNombreInstalacion.AutoIncrement = false;
                colvarNombreInstalacion.IsNullable = false;
                colvarNombreInstalacion.IsPrimaryKey = false;
                colvarNombreInstalacion.IsForeignKey = false;
                colvarNombreInstalacion.IsReadOnly = false;
                
                schema.Columns.Add(colvarNombreInstalacion);
                
                TableSchema.TableColumn colvarCiaPropietaria = new TableSchema.TableColumn(schema);
                colvarCiaPropietaria.ColumnName = "CIA_PROPIETARIA";
                colvarCiaPropietaria.DataType = DbType.AnsiString;
                colvarCiaPropietaria.MaxLength = 40;
                colvarCiaPropietaria.AutoIncrement = false;
                colvarCiaPropietaria.IsNullable = true;
                colvarCiaPropietaria.IsPrimaryKey = false;
                colvarCiaPropietaria.IsForeignKey = false;
                colvarCiaPropietaria.IsReadOnly = false;
                
                schema.Columns.Add(colvarCiaPropietaria);
                
                TableSchema.TableColumn colvarFechaInicioOperacion = new TableSchema.TableColumn(schema);
                colvarFechaInicioOperacion.ColumnName = "FECHA_INICIO_OPERACION";
                colvarFechaInicioOperacion.DataType = DbType.DateTime;
                colvarFechaInicioOperacion.MaxLength = 0;
                colvarFechaInicioOperacion.AutoIncrement = false;
                colvarFechaInicioOperacion.IsNullable = true;
                colvarFechaInicioOperacion.IsPrimaryKey = false;
                colvarFechaInicioOperacion.IsForeignKey = false;
                colvarFechaInicioOperacion.IsReadOnly = false;
                
                schema.Columns.Add(colvarFechaInicioOperacion);
                
                TableSchema.TableColumn colvarNoEquipo = new TableSchema.TableColumn(schema);
                colvarNoEquipo.ColumnName = "NO_EQUIPO";
                colvarNoEquipo.DataType = DbType.AnsiString;
                colvarNoEquipo.MaxLength = 10;
                colvarNoEquipo.AutoIncrement = false;
                colvarNoEquipo.IsNullable = true;
                colvarNoEquipo.IsPrimaryKey = false;
                colvarNoEquipo.IsForeignKey = false;
                colvarNoEquipo.IsReadOnly = false;
                
                schema.Columns.Add(colvarNoEquipo);
                
                TableSchema.TableColumn colvarBorradoLogico = new TableSchema.TableColumn(schema);
                colvarBorradoLogico.ColumnName = "BORRADO_LOGICO";
                colvarBorradoLogico.DataType = DbType.Boolean;
                colvarBorradoLogico.MaxLength = 0;
                colvarBorradoLogico.AutoIncrement = false;
                colvarBorradoLogico.IsNullable = false;
                colvarBorradoLogico.IsPrimaryKey = false;
                colvarBorradoLogico.IsForeignKey = false;
                colvarBorradoLogico.IsReadOnly = false;
                
                schema.Columns.Add(colvarBorradoLogico);
                
                TableSchema.TableColumn colvarNombreClasificacionProceso = new TableSchema.TableColumn(schema);
                colvarNombreClasificacionProceso.ColumnName = "NOMBRE_CLASIFICACION_PROCESO";
                colvarNombreClasificacionProceso.DataType = DbType.AnsiString;
                colvarNombreClasificacionProceso.MaxLength = 60;
                colvarNombreClasificacionProceso.AutoIncrement = false;
                colvarNombreClasificacionProceso.IsNullable = true;
                colvarNombreClasificacionProceso.IsPrimaryKey = false;
                colvarNombreClasificacionProceso.IsForeignKey = false;
                colvarNombreClasificacionProceso.IsReadOnly = false;
                
                schema.Columns.Add(colvarNombreClasificacionProceso);
                
                TableSchema.TableColumn colvarNombreClasificacionServicio = new TableSchema.TableColumn(schema);
                colvarNombreClasificacionServicio.ColumnName = "NOMBRE_CLASIFICACION_SERVICIO";
                colvarNombreClasificacionServicio.DataType = DbType.AnsiString;
                colvarNombreClasificacionServicio.MaxLength = 150;
                colvarNombreClasificacionServicio.AutoIncrement = false;
                colvarNombreClasificacionServicio.IsNullable = true;
                colvarNombreClasificacionServicio.IsPrimaryKey = false;
                colvarNombreClasificacionServicio.IsForeignKey = false;
                colvarNombreClasificacionServicio.IsReadOnly = false;
                
                schema.Columns.Add(colvarNombreClasificacionServicio);
                
                TableSchema.TableColumn colvarNombreTipoEstructura = new TableSchema.TableColumn(schema);
                colvarNombreTipoEstructura.ColumnName = "NOMBRE_TIPO_ESTRUCTURA";
                colvarNombreTipoEstructura.DataType = DbType.AnsiString;
                colvarNombreTipoEstructura.MaxLength = 120;
                colvarNombreTipoEstructura.AutoIncrement = false;
                colvarNombreTipoEstructura.IsNullable = true;
                colvarNombreTipoEstructura.IsPrimaryKey = false;
                colvarNombreTipoEstructura.IsForeignKey = false;
                colvarNombreTipoEstructura.IsReadOnly = false;
                
                schema.Columns.Add(colvarNombreTipoEstructura);
                
                TableSchema.TableColumn colvarFijaMovil = new TableSchema.TableColumn(schema);
                colvarFijaMovil.ColumnName = "FIJA_MOVIL";
                colvarFijaMovil.DataType = DbType.AnsiString;
                colvarFijaMovil.MaxLength = 50;
                colvarFijaMovil.AutoIncrement = false;
                colvarFijaMovil.IsNullable = true;
                colvarFijaMovil.IsPrimaryKey = false;
                colvarFijaMovil.IsForeignKey = false;
                colvarFijaMovil.IsReadOnly = false;
                
                schema.Columns.Add(colvarFijaMovil);
                
                TableSchema.TableColumn colvarHabitada = new TableSchema.TableColumn(schema);
                colvarHabitada.ColumnName = "HABITADA";
                colvarHabitada.DataType = DbType.Boolean;
                colvarHabitada.MaxLength = 0;
                colvarHabitada.AutoIncrement = false;
                colvarHabitada.IsNullable = false;
                colvarHabitada.IsPrimaryKey = false;
                colvarHabitada.IsForeignKey = false;
                colvarHabitada.IsReadOnly = false;
                
                schema.Columns.Add(colvarHabitada);
                
                TableSchema.TableColumn colvarGradoLatitud = new TableSchema.TableColumn(schema);
                colvarGradoLatitud.ColumnName = "GRADO_LATITUD";
                colvarGradoLatitud.DataType = DbType.Decimal;
                colvarGradoLatitud.MaxLength = 0;
                colvarGradoLatitud.AutoIncrement = false;
                colvarGradoLatitud.IsNullable = true;
                colvarGradoLatitud.IsPrimaryKey = false;
                colvarGradoLatitud.IsForeignKey = false;
                colvarGradoLatitud.IsReadOnly = false;
                
                schema.Columns.Add(colvarGradoLatitud);
                
                TableSchema.TableColumn colvarMinutoLatitud = new TableSchema.TableColumn(schema);
                colvarMinutoLatitud.ColumnName = "MINUTO_LATITUD";
                colvarMinutoLatitud.DataType = DbType.Decimal;
                colvarMinutoLatitud.MaxLength = 0;
                colvarMinutoLatitud.AutoIncrement = false;
                colvarMinutoLatitud.IsNullable = true;
                colvarMinutoLatitud.IsPrimaryKey = false;
                colvarMinutoLatitud.IsForeignKey = false;
                colvarMinutoLatitud.IsReadOnly = false;
                
                schema.Columns.Add(colvarMinutoLatitud);
                
                TableSchema.TableColumn colvarSegundoLatitud = new TableSchema.TableColumn(schema);
                colvarSegundoLatitud.ColumnName = "SEGUNDO_LATITUD";
                colvarSegundoLatitud.DataType = DbType.Decimal;
                colvarSegundoLatitud.MaxLength = 0;
                colvarSegundoLatitud.AutoIncrement = false;
                colvarSegundoLatitud.IsNullable = true;
                colvarSegundoLatitud.IsPrimaryKey = false;
                colvarSegundoLatitud.IsForeignKey = false;
                colvarSegundoLatitud.IsReadOnly = false;
                
                schema.Columns.Add(colvarSegundoLatitud);
                
                TableSchema.TableColumn colvarGradoLongitud = new TableSchema.TableColumn(schema);
                colvarGradoLongitud.ColumnName = "GRADO_LONGITUD";
                colvarGradoLongitud.DataType = DbType.Decimal;
                colvarGradoLongitud.MaxLength = 0;
                colvarGradoLongitud.AutoIncrement = false;
                colvarGradoLongitud.IsNullable = true;
                colvarGradoLongitud.IsPrimaryKey = false;
                colvarGradoLongitud.IsForeignKey = false;
                colvarGradoLongitud.IsReadOnly = false;
                
                schema.Columns.Add(colvarGradoLongitud);
                
                TableSchema.TableColumn colvarMinutoLongitud = new TableSchema.TableColumn(schema);
                colvarMinutoLongitud.ColumnName = "MINUTO_LONGITUD";
                colvarMinutoLongitud.DataType = DbType.Decimal;
                colvarMinutoLongitud.MaxLength = 0;
                colvarMinutoLongitud.AutoIncrement = false;
                colvarMinutoLongitud.IsNullable = true;
                colvarMinutoLongitud.IsPrimaryKey = false;
                colvarMinutoLongitud.IsForeignKey = false;
                colvarMinutoLongitud.IsReadOnly = false;
                
                schema.Columns.Add(colvarMinutoLongitud);
                
                TableSchema.TableColumn colvarSegundoLongitud = new TableSchema.TableColumn(schema);
                colvarSegundoLongitud.ColumnName = "SEGUNDO_LONGITUD";
                colvarSegundoLongitud.DataType = DbType.Decimal;
                colvarSegundoLongitud.MaxLength = 0;
                colvarSegundoLongitud.AutoIncrement = false;
                colvarSegundoLongitud.IsNullable = true;
                colvarSegundoLongitud.IsPrimaryKey = false;
                colvarSegundoLongitud.IsForeignKey = false;
                colvarSegundoLongitud.IsReadOnly = false;
                
                schema.Columns.Add(colvarSegundoLongitud);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["pvdGestionGL"].AddSchema("VW_C_M_INSTALACIONES_GRAL",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public VwCMInstalacionesGral()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public VwCMInstalacionesGral(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public VwCMInstalacionesGral(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public VwCMInstalacionesGral(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("IdCMInstalacion")]
        [Bindable(true)]
        public int IdCMInstalacion 
	    {
		    get
		    {
			    return GetColumnValue<int>("ID_C_M_INSTALACION");
		    }
            set 
		    {
			    SetColumnValue("ID_C_M_INSTALACION", value);
            }
        }
	      
        [XmlAttribute("FkIdCMInstalacionUbicacionActual")]
        [Bindable(true)]
        public int? FkIdCMInstalacionUbicacionActual 
	    {
		    get
		    {
			    return GetColumnValue<int?>("FK_ID_C_M_INSTALACION_UBICACION_ACTUAL");
		    }
            set 
		    {
			    SetColumnValue("FK_ID_C_M_INSTALACION_UBICACION_ACTUAL", value);
            }
        }
	      
        [XmlAttribute("IdCComplejo")]
        [Bindable(true)]
        public int? IdCComplejo 
	    {
		    get
		    {
			    return GetColumnValue<int?>("ID_C_COMPLEJO");
		    }
            set 
		    {
			    SetColumnValue("ID_C_COMPLEJO", value);
            }
        }
	      
        [XmlAttribute("IdCSector")]
        [Bindable(true)]
        public int? IdCSector 
	    {
		    get
		    {
			    return GetColumnValue<int?>("ID_C_SECTOR");
		    }
            set 
		    {
			    SetColumnValue("ID_C_SECTOR", value);
            }
        }
	      
        [XmlAttribute("SiglasInstalacionUbicacionActual")]
        [Bindable(true)]
        public string SiglasInstalacionUbicacionActual 
	    {
		    get
		    {
			    return GetColumnValue<string>("SIGLAS_INSTALACION_UBICACION_ACTUAL");
		    }
            set 
		    {
			    SetColumnValue("SIGLAS_INSTALACION_UBICACION_ACTUAL", value);
            }
        }
	      
        [XmlAttribute("NombreInstalacionUbicacionActual")]
        [Bindable(true)]
        public string NombreInstalacionUbicacionActual 
	    {
		    get
		    {
			    return GetColumnValue<string>("NOMBRE_INSTALACION_UBICACION_ACTUAL");
		    }
            set 
		    {
			    SetColumnValue("NOMBRE_INSTALACION_UBICACION_ACTUAL", value);
            }
        }
	      
        [XmlAttribute("CveRegion")]
        [Bindable(true)]
        public string CveRegion 
	    {
		    get
		    {
			    return GetColumnValue<string>("CVE_REGION");
		    }
            set 
		    {
			    SetColumnValue("CVE_REGION", value);
            }
        }
	      
        [XmlAttribute("SiglasRegion")]
        [Bindable(true)]
        public string SiglasRegion 
	    {
		    get
		    {
			    return GetColumnValue<string>("SIGLAS_REGION");
		    }
            set 
		    {
			    SetColumnValue("SIGLAS_REGION", value);
            }
        }
	      
        [XmlAttribute("NombreRegion")]
        [Bindable(true)]
        public string NombreRegion 
	    {
		    get
		    {
			    return GetColumnValue<string>("NOMBRE_REGION");
		    }
            set 
		    {
			    SetColumnValue("NOMBRE_REGION", value);
            }
        }
	      
        [XmlAttribute("CveActivo")]
        [Bindable(true)]
        public string CveActivo 
	    {
		    get
		    {
			    return GetColumnValue<string>("CVE_ACTIVO");
		    }
            set 
		    {
			    SetColumnValue("CVE_ACTIVO", value);
            }
        }
	      
        [XmlAttribute("SiglasActivo")]
        [Bindable(true)]
        public string SiglasActivo 
	    {
		    get
		    {
			    return GetColumnValue<string>("SIGLAS_ACTIVO");
		    }
            set 
		    {
			    SetColumnValue("SIGLAS_ACTIVO", value);
            }
        }
	      
        [XmlAttribute("NombreActivo")]
        [Bindable(true)]
        public string NombreActivo 
	    {
		    get
		    {
			    return GetColumnValue<string>("NOMBRE_ACTIVO");
		    }
            set 
		    {
			    SetColumnValue("NOMBRE_ACTIVO", value);
            }
        }
	      
        [XmlAttribute("SiglasComplejo")]
        [Bindable(true)]
        public string SiglasComplejo 
	    {
		    get
		    {
			    return GetColumnValue<string>("SIGLAS_COMPLEJO");
		    }
            set 
		    {
			    SetColumnValue("SIGLAS_COMPLEJO", value);
            }
        }
	      
        [XmlAttribute("NombreComplejo")]
        [Bindable(true)]
        public string NombreComplejo 
	    {
		    get
		    {
			    return GetColumnValue<string>("NOMBRE_COMPLEJO");
		    }
            set 
		    {
			    SetColumnValue("NOMBRE_COMPLEJO", value);
            }
        }
	      
        [XmlAttribute("CveSector")]
        [Bindable(true)]
        public string CveSector 
	    {
		    get
		    {
			    return GetColumnValue<string>("CVE_SECTOR");
		    }
            set 
		    {
			    SetColumnValue("CVE_SECTOR", value);
            }
        }
	      
        [XmlAttribute("NombreSector")]
        [Bindable(true)]
        public string NombreSector 
	    {
		    get
		    {
			    return GetColumnValue<string>("NOMBRE_SECTOR");
		    }
            set 
		    {
			    SetColumnValue("NOMBRE_SECTOR", value);
            }
        }
	      
        [XmlAttribute("SiglasInstalacion")]
        [Bindable(true)]
        public string SiglasInstalacion 
	    {
		    get
		    {
			    return GetColumnValue<string>("SIGLAS_INSTALACION");
		    }
            set 
		    {
			    SetColumnValue("SIGLAS_INSTALACION", value);
            }
        }
	      
        [XmlAttribute("NombreInstalacion")]
        [Bindable(true)]
        public string NombreInstalacion 
	    {
		    get
		    {
			    return GetColumnValue<string>("NOMBRE_INSTALACION");
		    }
            set 
		    {
			    SetColumnValue("NOMBRE_INSTALACION", value);
            }
        }
	      
        [XmlAttribute("CiaPropietaria")]
        [Bindable(true)]
        public string CiaPropietaria 
	    {
		    get
		    {
			    return GetColumnValue<string>("CIA_PROPIETARIA");
		    }
            set 
		    {
			    SetColumnValue("CIA_PROPIETARIA", value);
            }
        }
	      
        [XmlAttribute("FechaInicioOperacion")]
        [Bindable(true)]
        public DateTime? FechaInicioOperacion 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("FECHA_INICIO_OPERACION");
		    }
            set 
		    {
			    SetColumnValue("FECHA_INICIO_OPERACION", value);
            }
        }
	      
        [XmlAttribute("NoEquipo")]
        [Bindable(true)]
        public string NoEquipo 
	    {
		    get
		    {
			    return GetColumnValue<string>("NO_EQUIPO");
		    }
            set 
		    {
			    SetColumnValue("NO_EQUIPO", value);
            }
        }
	      
        [XmlAttribute("BorradoLogico")]
        [Bindable(true)]
        public bool BorradoLogico 
	    {
		    get
		    {
			    return GetColumnValue<bool>("BORRADO_LOGICO");
		    }
            set 
		    {
			    SetColumnValue("BORRADO_LOGICO", value);
            }
        }
	      
        [XmlAttribute("NombreClasificacionProceso")]
        [Bindable(true)]
        public string NombreClasificacionProceso 
	    {
		    get
		    {
			    return GetColumnValue<string>("NOMBRE_CLASIFICACION_PROCESO");
		    }
            set 
		    {
			    SetColumnValue("NOMBRE_CLASIFICACION_PROCESO", value);
            }
        }
	      
        [XmlAttribute("NombreClasificacionServicio")]
        [Bindable(true)]
        public string NombreClasificacionServicio 
	    {
		    get
		    {
			    return GetColumnValue<string>("NOMBRE_CLASIFICACION_SERVICIO");
		    }
            set 
		    {
			    SetColumnValue("NOMBRE_CLASIFICACION_SERVICIO", value);
            }
        }
	      
        [XmlAttribute("NombreTipoEstructura")]
        [Bindable(true)]
        public string NombreTipoEstructura 
	    {
		    get
		    {
			    return GetColumnValue<string>("NOMBRE_TIPO_ESTRUCTURA");
		    }
            set 
		    {
			    SetColumnValue("NOMBRE_TIPO_ESTRUCTURA", value);
            }
        }
	      
        [XmlAttribute("FijaMovil")]
        [Bindable(true)]
        public string FijaMovil 
	    {
		    get
		    {
			    return GetColumnValue<string>("FIJA_MOVIL");
		    }
            set 
		    {
			    SetColumnValue("FIJA_MOVIL", value);
            }
        }
	      
        [XmlAttribute("Habitada")]
        [Bindable(true)]
        public bool Habitada 
	    {
		    get
		    {
			    return GetColumnValue<bool>("HABITADA");
		    }
            set 
		    {
			    SetColumnValue("HABITADA", value);
            }
        }
	      
        [XmlAttribute("GradoLatitud")]
        [Bindable(true)]
        public decimal? GradoLatitud 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("GRADO_LATITUD");
		    }
            set 
		    {
			    SetColumnValue("GRADO_LATITUD", value);
            }
        }
	      
        [XmlAttribute("MinutoLatitud")]
        [Bindable(true)]
        public decimal? MinutoLatitud 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("MINUTO_LATITUD");
		    }
            set 
		    {
			    SetColumnValue("MINUTO_LATITUD", value);
            }
        }
	      
        [XmlAttribute("SegundoLatitud")]
        [Bindable(true)]
        public decimal? SegundoLatitud 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("SEGUNDO_LATITUD");
		    }
            set 
		    {
			    SetColumnValue("SEGUNDO_LATITUD", value);
            }
        }
	      
        [XmlAttribute("GradoLongitud")]
        [Bindable(true)]
        public decimal? GradoLongitud 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("GRADO_LONGITUD");
		    }
            set 
		    {
			    SetColumnValue("GRADO_LONGITUD", value);
            }
        }
	      
        [XmlAttribute("MinutoLongitud")]
        [Bindable(true)]
        public decimal? MinutoLongitud 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("MINUTO_LONGITUD");
		    }
            set 
		    {
			    SetColumnValue("MINUTO_LONGITUD", value);
            }
        }
	      
        [XmlAttribute("SegundoLongitud")]
        [Bindable(true)]
        public decimal? SegundoLongitud 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("SEGUNDO_LONGITUD");
		    }
            set 
		    {
			    SetColumnValue("SEGUNDO_LONGITUD", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string IdCMInstalacion = @"ID_C_M_INSTALACION";
            
            public static string FkIdCMInstalacionUbicacionActual = @"FK_ID_C_M_INSTALACION_UBICACION_ACTUAL";
            
            public static string IdCComplejo = @"ID_C_COMPLEJO";
            
            public static string IdCSector = @"ID_C_SECTOR";
            
            public static string SiglasInstalacionUbicacionActual = @"SIGLAS_INSTALACION_UBICACION_ACTUAL";
            
            public static string NombreInstalacionUbicacionActual = @"NOMBRE_INSTALACION_UBICACION_ACTUAL";
            
            public static string CveRegion = @"CVE_REGION";
            
            public static string SiglasRegion = @"SIGLAS_REGION";
            
            public static string NombreRegion = @"NOMBRE_REGION";
            
            public static string CveActivo = @"CVE_ACTIVO";
            
            public static string SiglasActivo = @"SIGLAS_ACTIVO";
            
            public static string NombreActivo = @"NOMBRE_ACTIVO";
            
            public static string SiglasComplejo = @"SIGLAS_COMPLEJO";
            
            public static string NombreComplejo = @"NOMBRE_COMPLEJO";
            
            public static string CveSector = @"CVE_SECTOR";
            
            public static string NombreSector = @"NOMBRE_SECTOR";
            
            public static string SiglasInstalacion = @"SIGLAS_INSTALACION";
            
            public static string NombreInstalacion = @"NOMBRE_INSTALACION";
            
            public static string CiaPropietaria = @"CIA_PROPIETARIA";
            
            public static string FechaInicioOperacion = @"FECHA_INICIO_OPERACION";
            
            public static string NoEquipo = @"NO_EQUIPO";
            
            public static string BorradoLogico = @"BORRADO_LOGICO";
            
            public static string NombreClasificacionProceso = @"NOMBRE_CLASIFICACION_PROCESO";
            
            public static string NombreClasificacionServicio = @"NOMBRE_CLASIFICACION_SERVICIO";
            
            public static string NombreTipoEstructura = @"NOMBRE_TIPO_ESTRUCTURA";
            
            public static string FijaMovil = @"FIJA_MOVIL";
            
            public static string Habitada = @"HABITADA";
            
            public static string GradoLatitud = @"GRADO_LATITUD";
            
            public static string MinutoLatitud = @"MINUTO_LATITUD";
            
            public static string SegundoLatitud = @"SEGUNDO_LATITUD";
            
            public static string GradoLongitud = @"GRADO_LONGITUD";
            
            public static string MinutoLongitud = @"MINUTO_LONGITUD";
            
            public static string SegundoLongitud = @"SEGUNDO_LONGITUD";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}
