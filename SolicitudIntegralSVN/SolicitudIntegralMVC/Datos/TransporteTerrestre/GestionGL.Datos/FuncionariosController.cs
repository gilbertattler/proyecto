using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace TransporteTerrestre.GestionGL.Datos
{
    /// <summary>
    /// Controller class for FUNCIONARIOS
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class FuncionariosController
    {
        // Preload our schema..
        Funcionarios thisSchemaLoad = new Funcionarios();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public FuncionariosCollection FetchAll()
        {
            FuncionariosCollection coll = new FuncionariosCollection();
            Query qry = new Query(Funcionarios.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public FuncionariosCollection FetchByID(object IdFuncionario)
        {
            FuncionariosCollection coll = new FuncionariosCollection().Where("ID_FUNCIONARIO", IdFuncionario).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public FuncionariosCollection FetchByQuery(Query qry)
        {
            FuncionariosCollection coll = new FuncionariosCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdFuncionario)
        {
            return (Funcionarios.Delete(IdFuncionario) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdFuncionario)
        {
            return (Funcionarios.Destroy(IdFuncionario) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string Ficha,string Nombre,string Subdivision,string Cia,string Email,string Tel,string Area,string Cgestor,bool BorradoLogico,string FichaActualiza,DateTime FechaActualiza)
	    {
		    Funcionarios item = new Funcionarios();
		    
            item.Ficha = Ficha;
            
            item.Nombre = Nombre;
            
            item.Subdivision = Subdivision;
            
            item.Cia = Cia;
            
            item.Email = Email;
            
            item.Tel = Tel;
            
            item.Area = Area;
            
            item.Cgestor = Cgestor;
            
            item.BorradoLogico = BorradoLogico;
            
            item.FichaActualiza = FichaActualiza;
            
            item.FechaActualiza = FechaActualiza;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdFuncionario,string Ficha,string Nombre,string Subdivision,string Cia,string Email,string Tel,string Area,string Cgestor,bool BorradoLogico,string FichaActualiza,DateTime FechaActualiza)
	    {
		    Funcionarios item = new Funcionarios();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdFuncionario = IdFuncionario;
				
			item.Ficha = Ficha;
				
			item.Nombre = Nombre;
				
			item.Subdivision = Subdivision;
				
			item.Cia = Cia;
				
			item.Email = Email;
				
			item.Tel = Tel;
				
			item.Area = Area;
				
			item.Cgestor = Cgestor;
				
			item.BorradoLogico = BorradoLogico;
				
			item.FichaActualiza = FichaActualiza;
				
			item.FechaActualiza = FechaActualiza;
				
	        item.Save(UserName);
	    }
    }
}
