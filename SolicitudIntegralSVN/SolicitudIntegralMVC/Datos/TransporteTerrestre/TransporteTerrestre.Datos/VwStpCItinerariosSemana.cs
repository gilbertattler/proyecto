using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace TransporteTerrestre.Datos{
    /// <summary>
    /// Strongly-typed collection for the VwStpCItinerariosSemana class.
    /// </summary>
    [Serializable]
    public partial class VwStpCItinerariosSemanaCollection : ReadOnlyList<VwStpCItinerariosSemana, VwStpCItinerariosSemanaCollection>
    {        
        public VwStpCItinerariosSemanaCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the VW_stp_c_itinerarios_semana view.
    /// </summary>
    [Serializable]
    public partial class VwStpCItinerariosSemana : ReadOnlyRecord<VwStpCItinerariosSemana>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("VW_stp_c_itinerarios_semana", TableType.View, DataService.GetInstance("pvdSTPTerrestre"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarIdItinerarioSemana = new TableSchema.TableColumn(schema);
                colvarIdItinerarioSemana.ColumnName = "id_itinerario_semana";
                colvarIdItinerarioSemana.DataType = DbType.Int32;
                colvarIdItinerarioSemana.MaxLength = 0;
                colvarIdItinerarioSemana.AutoIncrement = false;
                colvarIdItinerarioSemana.IsNullable = false;
                colvarIdItinerarioSemana.IsPrimaryKey = false;
                colvarIdItinerarioSemana.IsForeignKey = false;
                colvarIdItinerarioSemana.IsReadOnly = false;
                
                schema.Columns.Add(colvarIdItinerarioSemana);
                
                TableSchema.TableColumn colvarIdItinerario = new TableSchema.TableColumn(schema);
                colvarIdItinerario.ColumnName = "id_itinerario";
                colvarIdItinerario.DataType = DbType.Int32;
                colvarIdItinerario.MaxLength = 0;
                colvarIdItinerario.AutoIncrement = false;
                colvarIdItinerario.IsNullable = false;
                colvarIdItinerario.IsPrimaryKey = false;
                colvarIdItinerario.IsForeignKey = false;
                colvarIdItinerario.IsReadOnly = false;
                
                schema.Columns.Add(colvarIdItinerario);
                
                TableSchema.TableColumn colvarTipoSemana = new TableSchema.TableColumn(schema);
                colvarTipoSemana.ColumnName = "tipo_semana";
                colvarTipoSemana.DataType = DbType.AnsiStringFixedLength;
                colvarTipoSemana.MaxLength = 1;
                colvarTipoSemana.AutoIncrement = false;
                colvarTipoSemana.IsNullable = true;
                colvarTipoSemana.IsPrimaryKey = false;
                colvarTipoSemana.IsForeignKey = false;
                colvarTipoSemana.IsReadOnly = false;
                
                schema.Columns.Add(colvarTipoSemana);
                
                TableSchema.TableColumn colvarDesItinerario = new TableSchema.TableColumn(schema);
                colvarDesItinerario.ColumnName = "des_itinerario";
                colvarDesItinerario.DataType = DbType.AnsiString;
                colvarDesItinerario.MaxLength = 50;
                colvarDesItinerario.AutoIncrement = false;
                colvarDesItinerario.IsNullable = false;
                colvarDesItinerario.IsPrimaryKey = false;
                colvarDesItinerario.IsForeignKey = false;
                colvarDesItinerario.IsReadOnly = false;
                
                schema.Columns.Add(colvarDesItinerario);
                
                TableSchema.TableColumn colvarFechaInicio = new TableSchema.TableColumn(schema);
                colvarFechaInicio.ColumnName = "fecha_inicio";
                colvarFechaInicio.DataType = DbType.DateTime;
                colvarFechaInicio.MaxLength = 0;
                colvarFechaInicio.AutoIncrement = false;
                colvarFechaInicio.IsNullable = false;
                colvarFechaInicio.IsPrimaryKey = false;
                colvarFechaInicio.IsForeignKey = false;
                colvarFechaInicio.IsReadOnly = false;
                
                schema.Columns.Add(colvarFechaInicio);
                
                TableSchema.TableColumn colvarFechaFin = new TableSchema.TableColumn(schema);
                colvarFechaFin.ColumnName = "fecha_fin";
                colvarFechaFin.DataType = DbType.DateTime;
                colvarFechaFin.MaxLength = 0;
                colvarFechaFin.AutoIncrement = false;
                colvarFechaFin.IsNullable = false;
                colvarFechaFin.IsPrimaryKey = false;
                colvarFechaFin.IsForeignKey = false;
                colvarFechaFin.IsReadOnly = false;
                
                schema.Columns.Add(colvarFechaFin);
                
                TableSchema.TableColumn colvarFechaCreado = new TableSchema.TableColumn(schema);
                colvarFechaCreado.ColumnName = "fecha_creado";
                colvarFechaCreado.DataType = DbType.DateTime;
                colvarFechaCreado.MaxLength = 0;
                colvarFechaCreado.AutoIncrement = false;
                colvarFechaCreado.IsNullable = true;
                colvarFechaCreado.IsPrimaryKey = false;
                colvarFechaCreado.IsForeignKey = false;
                colvarFechaCreado.IsReadOnly = false;
                
                schema.Columns.Add(colvarFechaCreado);
                
                TableSchema.TableColumn colvarLiberado = new TableSchema.TableColumn(schema);
                colvarLiberado.ColumnName = "liberado";
                colvarLiberado.DataType = DbType.Boolean;
                colvarLiberado.MaxLength = 0;
                colvarLiberado.AutoIncrement = false;
                colvarLiberado.IsNullable = true;
                colvarLiberado.IsPrimaryKey = false;
                colvarLiberado.IsForeignKey = false;
                colvarLiberado.IsReadOnly = false;
                
                schema.Columns.Add(colvarLiberado);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["pvdSTPTerrestre"].AddSchema("VW_stp_c_itinerarios_semana",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public VwStpCItinerariosSemana()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public VwStpCItinerariosSemana(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public VwStpCItinerariosSemana(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public VwStpCItinerariosSemana(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("IdItinerarioSemana")]
        [Bindable(true)]
        public int IdItinerarioSemana 
	    {
		    get
		    {
			    return GetColumnValue<int>("id_itinerario_semana");
		    }
            set 
		    {
			    SetColumnValue("id_itinerario_semana", value);
            }
        }
	      
        [XmlAttribute("IdItinerario")]
        [Bindable(true)]
        public int IdItinerario 
	    {
		    get
		    {
			    return GetColumnValue<int>("id_itinerario");
		    }
            set 
		    {
			    SetColumnValue("id_itinerario", value);
            }
        }
	      
        [XmlAttribute("TipoSemana")]
        [Bindable(true)]
        public string TipoSemana 
	    {
		    get
		    {
			    return GetColumnValue<string>("tipo_semana");
		    }
            set 
		    {
			    SetColumnValue("tipo_semana", value);
            }
        }
	      
        [XmlAttribute("DesItinerario")]
        [Bindable(true)]
        public string DesItinerario 
	    {
		    get
		    {
			    return GetColumnValue<string>("des_itinerario");
		    }
            set 
		    {
			    SetColumnValue("des_itinerario", value);
            }
        }
	      
        [XmlAttribute("FechaInicio")]
        [Bindable(true)]
        public DateTime FechaInicio 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("fecha_inicio");
		    }
            set 
		    {
			    SetColumnValue("fecha_inicio", value);
            }
        }
	      
        [XmlAttribute("FechaFin")]
        [Bindable(true)]
        public DateTime FechaFin 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("fecha_fin");
		    }
            set 
		    {
			    SetColumnValue("fecha_fin", value);
            }
        }
	      
        [XmlAttribute("FechaCreado")]
        [Bindable(true)]
        public DateTime? FechaCreado 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("fecha_creado");
		    }
            set 
		    {
			    SetColumnValue("fecha_creado", value);
            }
        }
	      
        [XmlAttribute("Liberado")]
        [Bindable(true)]
        public bool? Liberado 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("liberado");
		    }
            set 
		    {
			    SetColumnValue("liberado", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string IdItinerarioSemana = @"id_itinerario_semana";
            
            public static string IdItinerario = @"id_itinerario";
            
            public static string TipoSemana = @"tipo_semana";
            
            public static string DesItinerario = @"des_itinerario";
            
            public static string FechaInicio = @"fecha_inicio";
            
            public static string FechaFin = @"fecha_fin";
            
            public static string FechaCreado = @"fecha_creado";
            
            public static string Liberado = @"liberado";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}
