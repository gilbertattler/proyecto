using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace TransporteTerrestre.Datos{
    /// <summary>
    /// Strongly-typed collection for the VwStpItinerariosPorFecha class.
    /// </summary>
    [Serializable]
    public partial class VwStpItinerariosPorFechaCollection : ReadOnlyList<VwStpItinerariosPorFecha, VwStpItinerariosPorFechaCollection>
    {        
        public VwStpItinerariosPorFechaCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the VW_STP_ITINERARIOS_POR_FECHA view.
    /// </summary>
    [Serializable]
    public partial class VwStpItinerariosPorFecha : ReadOnlyRecord<VwStpItinerariosPorFecha>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("VW_STP_ITINERARIOS_POR_FECHA", TableType.View, DataService.GetInstance("pvdSTPTerrestre"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarIdItinerario = new TableSchema.TableColumn(schema);
                colvarIdItinerario.ColumnName = "id_itinerario";
                colvarIdItinerario.DataType = DbType.Int32;
                colvarIdItinerario.MaxLength = 0;
                colvarIdItinerario.AutoIncrement = false;
                colvarIdItinerario.IsNullable = false;
                colvarIdItinerario.IsPrimaryKey = false;
                colvarIdItinerario.IsForeignKey = false;
                colvarIdItinerario.IsReadOnly = false;
                
                schema.Columns.Add(colvarIdItinerario);
                
                TableSchema.TableColumn colvarFkTipoServicio = new TableSchema.TableColumn(schema);
                colvarFkTipoServicio.ColumnName = "fk_tipo_servicio";
                colvarFkTipoServicio.DataType = DbType.Int32;
                colvarFkTipoServicio.MaxLength = 0;
                colvarFkTipoServicio.AutoIncrement = false;
                colvarFkTipoServicio.IsNullable = false;
                colvarFkTipoServicio.IsPrimaryKey = false;
                colvarFkTipoServicio.IsForeignKey = false;
                colvarFkTipoServicio.IsReadOnly = false;
                
                schema.Columns.Add(colvarFkTipoServicio);
                
                TableSchema.TableColumn colvarFkAreaServicio = new TableSchema.TableColumn(schema);
                colvarFkAreaServicio.ColumnName = "fk_area_servicio";
                colvarFkAreaServicio.DataType = DbType.Int32;
                colvarFkAreaServicio.MaxLength = 0;
                colvarFkAreaServicio.AutoIncrement = false;
                colvarFkAreaServicio.IsNullable = false;
                colvarFkAreaServicio.IsPrimaryKey = false;
                colvarFkAreaServicio.IsForeignKey = false;
                colvarFkAreaServicio.IsReadOnly = false;
                
                schema.Columns.Add(colvarFkAreaServicio);
                
                TableSchema.TableColumn colvarFkNodoAtencion = new TableSchema.TableColumn(schema);
                colvarFkNodoAtencion.ColumnName = "fk_nodo_atencion";
                colvarFkNodoAtencion.DataType = DbType.Int32;
                colvarFkNodoAtencion.MaxLength = 0;
                colvarFkNodoAtencion.AutoIncrement = false;
                colvarFkNodoAtencion.IsNullable = false;
                colvarFkNodoAtencion.IsPrimaryKey = false;
                colvarFkNodoAtencion.IsForeignKey = false;
                colvarFkNodoAtencion.IsReadOnly = false;
                
                schema.Columns.Add(colvarFkNodoAtencion);
                
                TableSchema.TableColumn colvarDesItinerario = new TableSchema.TableColumn(schema);
                colvarDesItinerario.ColumnName = "des_itinerario";
                colvarDesItinerario.DataType = DbType.AnsiString;
                colvarDesItinerario.MaxLength = 50;
                colvarDesItinerario.AutoIncrement = false;
                colvarDesItinerario.IsNullable = false;
                colvarDesItinerario.IsPrimaryKey = false;
                colvarDesItinerario.IsForeignKey = false;
                colvarDesItinerario.IsReadOnly = false;
                
                schema.Columns.Add(colvarDesItinerario);
                
                TableSchema.TableColumn colvarCveItinerario = new TableSchema.TableColumn(schema);
                colvarCveItinerario.ColumnName = "cve_itinerario";
                colvarCveItinerario.DataType = DbType.AnsiString;
                colvarCveItinerario.MaxLength = 5;
                colvarCveItinerario.AutoIncrement = false;
                colvarCveItinerario.IsNullable = false;
                colvarCveItinerario.IsPrimaryKey = false;
                colvarCveItinerario.IsForeignKey = false;
                colvarCveItinerario.IsReadOnly = false;
                
                schema.Columns.Add(colvarCveItinerario);
                
                TableSchema.TableColumn colvarCveNodoOrigen = new TableSchema.TableColumn(schema);
                colvarCveNodoOrigen.ColumnName = "cve_nodo_origen";
                colvarCveNodoOrigen.DataType = DbType.AnsiString;
                colvarCveNodoOrigen.MaxLength = 5;
                colvarCveNodoOrigen.AutoIncrement = false;
                colvarCveNodoOrigen.IsNullable = false;
                colvarCveNodoOrigen.IsPrimaryKey = false;
                colvarCveNodoOrigen.IsForeignKey = false;
                colvarCveNodoOrigen.IsReadOnly = false;
                
                schema.Columns.Add(colvarCveNodoOrigen);
                
                TableSchema.TableColumn colvarCveNodoDestino = new TableSchema.TableColumn(schema);
                colvarCveNodoDestino.ColumnName = "cve_nodo_destino";
                colvarCveNodoDestino.DataType = DbType.AnsiString;
                colvarCveNodoDestino.MaxLength = 5;
                colvarCveNodoDestino.AutoIncrement = false;
                colvarCveNodoDestino.IsNullable = false;
                colvarCveNodoDestino.IsPrimaryKey = false;
                colvarCveNodoDestino.IsForeignKey = false;
                colvarCveNodoDestino.IsReadOnly = false;
                
                schema.Columns.Add(colvarCveNodoDestino);
                
                TableSchema.TableColumn colvarCapacidad = new TableSchema.TableColumn(schema);
                colvarCapacidad.ColumnName = "capacidad";
                colvarCapacidad.DataType = DbType.Int32;
                colvarCapacidad.MaxLength = 0;
                colvarCapacidad.AutoIncrement = false;
                colvarCapacidad.IsNullable = false;
                colvarCapacidad.IsPrimaryKey = false;
                colvarCapacidad.IsForeignKey = false;
                colvarCapacidad.IsReadOnly = false;
                
                schema.Columns.Add(colvarCapacidad);
                
                TableSchema.TableColumn colvarCapacidadDisponible = new TableSchema.TableColumn(schema);
                colvarCapacidadDisponible.ColumnName = "capacidad_disponible";
                colvarCapacidadDisponible.DataType = DbType.Int32;
                colvarCapacidadDisponible.MaxLength = 0;
                colvarCapacidadDisponible.AutoIncrement = false;
                colvarCapacidadDisponible.IsNullable = true;
                colvarCapacidadDisponible.IsPrimaryKey = false;
                colvarCapacidadDisponible.IsForeignKey = false;
                colvarCapacidadDisponible.IsReadOnly = false;
                
                schema.Columns.Add(colvarCapacidadDisponible);
                
                TableSchema.TableColumn colvarDiasRecepcion = new TableSchema.TableColumn(schema);
                colvarDiasRecepcion.ColumnName = "dias_recepcion";
                colvarDiasRecepcion.DataType = DbType.Int32;
                colvarDiasRecepcion.MaxLength = 0;
                colvarDiasRecepcion.AutoIncrement = false;
                colvarDiasRecepcion.IsNullable = true;
                colvarDiasRecepcion.IsPrimaryKey = false;
                colvarDiasRecepcion.IsForeignKey = false;
                colvarDiasRecepcion.IsReadOnly = false;
                
                schema.Columns.Add(colvarDiasRecepcion);
                
                TableSchema.TableColumn colvarHorarioRecepcion = new TableSchema.TableColumn(schema);
                colvarHorarioRecepcion.ColumnName = "horario_recepcion";
                colvarHorarioRecepcion.DataType = DbType.DateTime;
                colvarHorarioRecepcion.MaxLength = 0;
                colvarHorarioRecepcion.AutoIncrement = false;
                colvarHorarioRecepcion.IsNullable = true;
                colvarHorarioRecepcion.IsPrimaryKey = false;
                colvarHorarioRecepcion.IsForeignKey = false;
                colvarHorarioRecepcion.IsReadOnly = false;
                
                schema.Columns.Add(colvarHorarioRecepcion);
                
                TableSchema.TableColumn colvarHorarioItinerario = new TableSchema.TableColumn(schema);
                colvarHorarioItinerario.ColumnName = "horario_itinerario";
                colvarHorarioItinerario.DataType = DbType.DateTime;
                colvarHorarioItinerario.MaxLength = 0;
                colvarHorarioItinerario.AutoIncrement = false;
                colvarHorarioItinerario.IsNullable = true;
                colvarHorarioItinerario.IsPrimaryKey = false;
                colvarHorarioItinerario.IsForeignKey = false;
                colvarHorarioItinerario.IsReadOnly = false;
                
                schema.Columns.Add(colvarHorarioItinerario);
                
                TableSchema.TableColumn colvarFecha = new TableSchema.TableColumn(schema);
                colvarFecha.ColumnName = "fecha";
                colvarFecha.DataType = DbType.DateTime;
                colvarFecha.MaxLength = 0;
                colvarFecha.AutoIncrement = false;
                colvarFecha.IsNullable = false;
                colvarFecha.IsPrimaryKey = false;
                colvarFecha.IsForeignKey = false;
                colvarFecha.IsReadOnly = false;
                
                schema.Columns.Add(colvarFecha);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["pvdSTPTerrestre"].AddSchema("VW_STP_ITINERARIOS_POR_FECHA",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public VwStpItinerariosPorFecha()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public VwStpItinerariosPorFecha(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public VwStpItinerariosPorFecha(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public VwStpItinerariosPorFecha(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("IdItinerario")]
        [Bindable(true)]
        public int IdItinerario 
	    {
		    get
		    {
			    return GetColumnValue<int>("id_itinerario");
		    }
            set 
		    {
			    SetColumnValue("id_itinerario", value);
            }
        }
	      
        [XmlAttribute("FkTipoServicio")]
        [Bindable(true)]
        public int FkTipoServicio 
	    {
		    get
		    {
			    return GetColumnValue<int>("fk_tipo_servicio");
		    }
            set 
		    {
			    SetColumnValue("fk_tipo_servicio", value);
            }
        }
	      
        [XmlAttribute("FkAreaServicio")]
        [Bindable(true)]
        public int FkAreaServicio 
	    {
		    get
		    {
			    return GetColumnValue<int>("fk_area_servicio");
		    }
            set 
		    {
			    SetColumnValue("fk_area_servicio", value);
            }
        }
	      
        [XmlAttribute("FkNodoAtencion")]
        [Bindable(true)]
        public int FkNodoAtencion 
	    {
		    get
		    {
			    return GetColumnValue<int>("fk_nodo_atencion");
		    }
            set 
		    {
			    SetColumnValue("fk_nodo_atencion", value);
            }
        }
	      
        [XmlAttribute("DesItinerario")]
        [Bindable(true)]
        public string DesItinerario 
	    {
		    get
		    {
			    return GetColumnValue<string>("des_itinerario");
		    }
            set 
		    {
			    SetColumnValue("des_itinerario", value);
            }
        }
	      
        [XmlAttribute("CveItinerario")]
        [Bindable(true)]
        public string CveItinerario 
	    {
		    get
		    {
			    return GetColumnValue<string>("cve_itinerario");
		    }
            set 
		    {
			    SetColumnValue("cve_itinerario", value);
            }
        }
	      
        [XmlAttribute("CveNodoOrigen")]
        [Bindable(true)]
        public string CveNodoOrigen 
	    {
		    get
		    {
			    return GetColumnValue<string>("cve_nodo_origen");
		    }
            set 
		    {
			    SetColumnValue("cve_nodo_origen", value);
            }
        }
	      
        [XmlAttribute("CveNodoDestino")]
        [Bindable(true)]
        public string CveNodoDestino 
	    {
		    get
		    {
			    return GetColumnValue<string>("cve_nodo_destino");
		    }
            set 
		    {
			    SetColumnValue("cve_nodo_destino", value);
            }
        }
	      
        [XmlAttribute("Capacidad")]
        [Bindable(true)]
        public int Capacidad 
	    {
		    get
		    {
			    return GetColumnValue<int>("capacidad");
		    }
            set 
		    {
			    SetColumnValue("capacidad", value);
            }
        }
	      
        [XmlAttribute("CapacidadDisponible")]
        [Bindable(true)]
        public int? CapacidadDisponible 
	    {
		    get
		    {
			    return GetColumnValue<int?>("capacidad_disponible");
		    }
            set 
		    {
			    SetColumnValue("capacidad_disponible", value);
            }
        }
	      
        [XmlAttribute("DiasRecepcion")]
        [Bindable(true)]
        public int? DiasRecepcion 
	    {
		    get
		    {
			    return GetColumnValue<int?>("dias_recepcion");
		    }
            set 
		    {
			    SetColumnValue("dias_recepcion", value);
            }
        }
	      
        [XmlAttribute("HorarioRecepcion")]
        [Bindable(true)]
        public DateTime? HorarioRecepcion 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("horario_recepcion");
		    }
            set 
		    {
			    SetColumnValue("horario_recepcion", value);
            }
        }
	      
        [XmlAttribute("HorarioItinerario")]
        [Bindable(true)]
        public DateTime? HorarioItinerario 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("horario_itinerario");
		    }
            set 
		    {
			    SetColumnValue("horario_itinerario", value);
            }
        }
	      
        [XmlAttribute("Fecha")]
        [Bindable(true)]
        public DateTime Fecha 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("fecha");
		    }
            set 
		    {
			    SetColumnValue("fecha", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string IdItinerario = @"id_itinerario";
            
            public static string FkTipoServicio = @"fk_tipo_servicio";
            
            public static string FkAreaServicio = @"fk_area_servicio";
            
            public static string FkNodoAtencion = @"fk_nodo_atencion";
            
            public static string DesItinerario = @"des_itinerario";
            
            public static string CveItinerario = @"cve_itinerario";
            
            public static string CveNodoOrigen = @"cve_nodo_origen";
            
            public static string CveNodoDestino = @"cve_nodo_destino";
            
            public static string Capacidad = @"capacidad";
            
            public static string CapacidadDisponible = @"capacidad_disponible";
            
            public static string DiasRecepcion = @"dias_recepcion";
            
            public static string HorarioRecepcion = @"horario_recepcion";
            
            public static string HorarioItinerario = @"horario_itinerario";
            
            public static string Fecha = @"fecha";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}
