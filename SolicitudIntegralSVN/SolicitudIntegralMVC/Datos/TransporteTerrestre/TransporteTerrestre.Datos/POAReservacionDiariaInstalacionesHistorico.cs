using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace TransporteTerrestre.Datos
{
	/// <summary>
	/// Strongly-typed collection for the POAReservacionDiariaInstalacionesHistorico class.
	/// </summary>
    [Serializable]
	public partial class POAReservacionDiariaInstalacionesHistoricoCollection : ActiveList<POAReservacionDiariaInstalacionesHistorico, POAReservacionDiariaInstalacionesHistoricoCollection>
	{	   
		public POAReservacionDiariaInstalacionesHistoricoCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>POAReservacionDiariaInstalacionesHistoricoCollection</returns>
		public POAReservacionDiariaInstalacionesHistoricoCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                POAReservacionDiariaInstalacionesHistorico o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the POAReservacionDiariaInstalacionesHistorico table.
	/// </summary>
	[Serializable]
	public partial class POAReservacionDiariaInstalacionesHistorico : ActiveRecord<POAReservacionDiariaInstalacionesHistorico>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public POAReservacionDiariaInstalacionesHistorico()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public POAReservacionDiariaInstalacionesHistorico(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public POAReservacionDiariaInstalacionesHistorico(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public POAReservacionDiariaInstalacionesHistorico(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("POAReservacionDiariaInstalacionesHistorico", TableType.Table, DataService.GetInstance("pvdSTPTerrestre"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarIdPOAReservacionDiariaInstalacionHistorico = new TableSchema.TableColumn(schema);
				colvarIdPOAReservacionDiariaInstalacionHistorico.ColumnName = "IdPOAReservacionDiariaInstalacionHistorico";
				colvarIdPOAReservacionDiariaInstalacionHistorico.DataType = DbType.Int32;
				colvarIdPOAReservacionDiariaInstalacionHistorico.MaxLength = 0;
				colvarIdPOAReservacionDiariaInstalacionHistorico.AutoIncrement = true;
				colvarIdPOAReservacionDiariaInstalacionHistorico.IsNullable = false;
				colvarIdPOAReservacionDiariaInstalacionHistorico.IsPrimaryKey = true;
				colvarIdPOAReservacionDiariaInstalacionHistorico.IsForeignKey = false;
				colvarIdPOAReservacionDiariaInstalacionHistorico.IsReadOnly = false;
				colvarIdPOAReservacionDiariaInstalacionHistorico.DefaultSetting = @"";
				colvarIdPOAReservacionDiariaInstalacionHistorico.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdPOAReservacionDiariaInstalacionHistorico);
				
				TableSchema.TableColumn colvarFechaCargaPOA = new TableSchema.TableColumn(schema);
				colvarFechaCargaPOA.ColumnName = "FechaCargaPOA";
				colvarFechaCargaPOA.DataType = DbType.DateTime;
				colvarFechaCargaPOA.MaxLength = 0;
				colvarFechaCargaPOA.AutoIncrement = false;
				colvarFechaCargaPOA.IsNullable = false;
				colvarFechaCargaPOA.IsPrimaryKey = false;
				colvarFechaCargaPOA.IsForeignKey = false;
				colvarFechaCargaPOA.IsReadOnly = false;
				colvarFechaCargaPOA.DefaultSetting = @"";
				colvarFechaCargaPOA.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFechaCargaPOA);
				
				TableSchema.TableColumn colvarReservacionAH = new TableSchema.TableColumn(schema);
				colvarReservacionAH.ColumnName = "ReservacionAH";
				colvarReservacionAH.DataType = DbType.Int32;
				colvarReservacionAH.MaxLength = 0;
				colvarReservacionAH.AutoIncrement = false;
				colvarReservacionAH.IsNullable = false;
				colvarReservacionAH.IsPrimaryKey = false;
				colvarReservacionAH.IsForeignKey = false;
				colvarReservacionAH.IsReadOnly = false;
				colvarReservacionAH.DefaultSetting = @"";
				colvarReservacionAH.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReservacionAH);
				
				TableSchema.TableColumn colvarReservacionAereo = new TableSchema.TableColumn(schema);
				colvarReservacionAereo.ColumnName = "ReservacionAereo";
				colvarReservacionAereo.DataType = DbType.Int32;
				colvarReservacionAereo.MaxLength = 0;
				colvarReservacionAereo.AutoIncrement = false;
				colvarReservacionAereo.IsNullable = false;
				colvarReservacionAereo.IsPrimaryKey = false;
				colvarReservacionAereo.IsForeignKey = false;
				colvarReservacionAereo.IsReadOnly = false;
				colvarReservacionAereo.DefaultSetting = @"";
				colvarReservacionAereo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReservacionAereo);
				
				TableSchema.TableColumn colvarReservacionMarino = new TableSchema.TableColumn(schema);
				colvarReservacionMarino.ColumnName = "ReservacionMarino";
				colvarReservacionMarino.DataType = DbType.Int32;
				colvarReservacionMarino.MaxLength = 0;
				colvarReservacionMarino.AutoIncrement = false;
				colvarReservacionMarino.IsNullable = false;
				colvarReservacionMarino.IsPrimaryKey = false;
				colvarReservacionMarino.IsForeignKey = false;
				colvarReservacionMarino.IsReadOnly = false;
				colvarReservacionMarino.DefaultSetting = @"";
				colvarReservacionMarino.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReservacionMarino);
				
				TableSchema.TableColumn colvarIdPOAInstalacionHistorico = new TableSchema.TableColumn(schema);
				colvarIdPOAInstalacionHistorico.ColumnName = "IdPOAInstalacionHistorico";
				colvarIdPOAInstalacionHistorico.DataType = DbType.Int32;
				colvarIdPOAInstalacionHistorico.MaxLength = 0;
				colvarIdPOAInstalacionHistorico.AutoIncrement = false;
				colvarIdPOAInstalacionHistorico.IsNullable = false;
				colvarIdPOAInstalacionHistorico.IsPrimaryKey = false;
				colvarIdPOAInstalacionHistorico.IsForeignKey = true;
				colvarIdPOAInstalacionHistorico.IsReadOnly = false;
				colvarIdPOAInstalacionHistorico.DefaultSetting = @"";
				
					colvarIdPOAInstalacionHistorico.ForeignKeyTableName = "POAInstalacionesHistorico";
				schema.Columns.Add(colvarIdPOAInstalacionHistorico);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["pvdSTPTerrestre"].AddSchema("POAReservacionDiariaInstalacionesHistorico",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("IdPOAReservacionDiariaInstalacionHistorico")]
		[Bindable(true)]
		public int IdPOAReservacionDiariaInstalacionHistorico 
		{
			get { return GetColumnValue<int>(Columns.IdPOAReservacionDiariaInstalacionHistorico); }
			set { SetColumnValue(Columns.IdPOAReservacionDiariaInstalacionHistorico, value); }
		}
		  
		[XmlAttribute("FechaCargaPOA")]
		[Bindable(true)]
		public DateTime FechaCargaPOA 
		{
			get { return GetColumnValue<DateTime>(Columns.FechaCargaPOA); }
			set { SetColumnValue(Columns.FechaCargaPOA, value); }
		}
		  
		[XmlAttribute("ReservacionAH")]
		[Bindable(true)]
		public int ReservacionAH 
		{
			get { return GetColumnValue<int>(Columns.ReservacionAH); }
			set { SetColumnValue(Columns.ReservacionAH, value); }
		}
		  
		[XmlAttribute("ReservacionAereo")]
		[Bindable(true)]
		public int ReservacionAereo 
		{
			get { return GetColumnValue<int>(Columns.ReservacionAereo); }
			set { SetColumnValue(Columns.ReservacionAereo, value); }
		}
		  
		[XmlAttribute("ReservacionMarino")]
		[Bindable(true)]
		public int ReservacionMarino 
		{
			get { return GetColumnValue<int>(Columns.ReservacionMarino); }
			set { SetColumnValue(Columns.ReservacionMarino, value); }
		}
		  
		[XmlAttribute("IdPOAInstalacionHistorico")]
		[Bindable(true)]
		public int IdPOAInstalacionHistorico 
		{
			get { return GetColumnValue<int>(Columns.IdPOAInstalacionHistorico); }
			set { SetColumnValue(Columns.IdPOAInstalacionHistorico, value); }
		}
		
		#endregion
		
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a POAInstalacionesHistorico ActiveRecord object related to this POAReservacionDiariaInstalacionesHistorico
		/// 
		/// </summary>
		public TransporteTerrestre.Datos.POAInstalacionesHistorico POAInstalacionesHistorico
		{
			get { return TransporteTerrestre.Datos.POAInstalacionesHistorico.FetchByID(this.IdPOAInstalacionHistorico); }
			set { SetColumnValue("IdPOAInstalacionHistorico", value.IdPOAInstalacionHistorico); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(DateTime varFechaCargaPOA,int varReservacionAH,int varReservacionAereo,int varReservacionMarino,int varIdPOAInstalacionHistorico)
		{
			POAReservacionDiariaInstalacionesHistorico item = new POAReservacionDiariaInstalacionesHistorico();
			
			item.FechaCargaPOA = varFechaCargaPOA;
			
			item.ReservacionAH = varReservacionAH;
			
			item.ReservacionAereo = varReservacionAereo;
			
			item.ReservacionMarino = varReservacionMarino;
			
			item.IdPOAInstalacionHistorico = varIdPOAInstalacionHistorico;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varIdPOAReservacionDiariaInstalacionHistorico,DateTime varFechaCargaPOA,int varReservacionAH,int varReservacionAereo,int varReservacionMarino,int varIdPOAInstalacionHistorico)
		{
			POAReservacionDiariaInstalacionesHistorico item = new POAReservacionDiariaInstalacionesHistorico();
			
				item.IdPOAReservacionDiariaInstalacionHistorico = varIdPOAReservacionDiariaInstalacionHistorico;
			
				item.FechaCargaPOA = varFechaCargaPOA;
			
				item.ReservacionAH = varReservacionAH;
			
				item.ReservacionAereo = varReservacionAereo;
			
				item.ReservacionMarino = varReservacionMarino;
			
				item.IdPOAInstalacionHistorico = varIdPOAInstalacionHistorico;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdPOAReservacionDiariaInstalacionHistoricoColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn FechaCargaPOAColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ReservacionAHColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ReservacionAereoColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ReservacionMarinoColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn IdPOAInstalacionHistoricoColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string IdPOAReservacionDiariaInstalacionHistorico = @"IdPOAReservacionDiariaInstalacionHistorico";
			 public static string FechaCargaPOA = @"FechaCargaPOA";
			 public static string ReservacionAH = @"ReservacionAH";
			 public static string ReservacionAereo = @"ReservacionAereo";
			 public static string ReservacionMarino = @"ReservacionMarino";
			 public static string IdPOAInstalacionHistorico = @"IdPOAInstalacionHistorico";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}
