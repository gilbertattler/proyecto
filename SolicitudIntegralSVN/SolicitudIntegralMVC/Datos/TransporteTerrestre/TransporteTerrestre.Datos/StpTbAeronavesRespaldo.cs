using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace TransporteTerrestre.Datos
{
	/// <summary>
	/// Strongly-typed collection for the StpTbAeronavesRespaldo class.
	/// </summary>
    [Serializable]
	public partial class StpTbAeronavesRespaldoCollection : ActiveList<StpTbAeronavesRespaldo, StpTbAeronavesRespaldoCollection>
	{	   
		public StpTbAeronavesRespaldoCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>StpTbAeronavesRespaldoCollection</returns>
		public StpTbAeronavesRespaldoCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                StpTbAeronavesRespaldo o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the stp_tb_aeronaves_respaldo table.
	/// </summary>
	[Serializable]
	public partial class StpTbAeronavesRespaldo : ActiveRecord<StpTbAeronavesRespaldo>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public StpTbAeronavesRespaldo()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public StpTbAeronavesRespaldo(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public StpTbAeronavesRespaldo(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public StpTbAeronavesRespaldo(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("stp_tb_aeronaves_respaldo", TableType.Table, DataService.GetInstance("pvdSTPTerrestre"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarIdAeronaveRespaldo = new TableSchema.TableColumn(schema);
				colvarIdAeronaveRespaldo.ColumnName = "id_aeronave_respaldo";
				colvarIdAeronaveRespaldo.DataType = DbType.Int32;
				colvarIdAeronaveRespaldo.MaxLength = 0;
				colvarIdAeronaveRespaldo.AutoIncrement = true;
				colvarIdAeronaveRespaldo.IsNullable = false;
				colvarIdAeronaveRespaldo.IsPrimaryKey = true;
				colvarIdAeronaveRespaldo.IsForeignKey = false;
				colvarIdAeronaveRespaldo.IsReadOnly = false;
				colvarIdAeronaveRespaldo.DefaultSetting = @"";
				colvarIdAeronaveRespaldo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdAeronaveRespaldo);
				
				TableSchema.TableColumn colvarFkAeronave = new TableSchema.TableColumn(schema);
				colvarFkAeronave.ColumnName = "fk_aeronave";
				colvarFkAeronave.DataType = DbType.Int32;
				colvarFkAeronave.MaxLength = 0;
				colvarFkAeronave.AutoIncrement = false;
				colvarFkAeronave.IsNullable = false;
				colvarFkAeronave.IsPrimaryKey = false;
				colvarFkAeronave.IsForeignKey = false;
				colvarFkAeronave.IsReadOnly = false;
				colvarFkAeronave.DefaultSetting = @"";
				colvarFkAeronave.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFkAeronave);
				
				TableSchema.TableColumn colvarFkVuelo = new TableSchema.TableColumn(schema);
				colvarFkVuelo.ColumnName = "fk_vuelo";
				colvarFkVuelo.DataType = DbType.Int32;
				colvarFkVuelo.MaxLength = 0;
				colvarFkVuelo.AutoIncrement = false;
				colvarFkVuelo.IsNullable = false;
				colvarFkVuelo.IsPrimaryKey = false;
				colvarFkVuelo.IsForeignKey = true;
				colvarFkVuelo.IsReadOnly = false;
				colvarFkVuelo.DefaultSetting = @"";
				
					colvarFkVuelo.ForeignKeyTableName = "stp_tb_vuelos";
				schema.Columns.Add(colvarFkVuelo);
				
				TableSchema.TableColumn colvarFkContrato = new TableSchema.TableColumn(schema);
				colvarFkContrato.ColumnName = "fk_contrato";
				colvarFkContrato.DataType = DbType.Int32;
				colvarFkContrato.MaxLength = 0;
				colvarFkContrato.AutoIncrement = false;
				colvarFkContrato.IsNullable = false;
				colvarFkContrato.IsPrimaryKey = false;
				colvarFkContrato.IsForeignKey = false;
				colvarFkContrato.IsReadOnly = false;
				colvarFkContrato.DefaultSetting = @"";
				colvarFkContrato.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFkContrato);
				
				TableSchema.TableColumn colvarFicActua = new TableSchema.TableColumn(schema);
				colvarFicActua.ColumnName = "fic_actua";
				colvarFicActua.DataType = DbType.AnsiString;
				colvarFicActua.MaxLength = 14;
				colvarFicActua.AutoIncrement = false;
				colvarFicActua.IsNullable = false;
				colvarFicActua.IsPrimaryKey = false;
				colvarFicActua.IsForeignKey = false;
				colvarFicActua.IsReadOnly = false;
				colvarFicActua.DefaultSetting = @"";
				colvarFicActua.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFicActua);
				
				TableSchema.TableColumn colvarFecActua = new TableSchema.TableColumn(schema);
				colvarFecActua.ColumnName = "fec_actua";
				colvarFecActua.DataType = DbType.DateTime;
				colvarFecActua.MaxLength = 0;
				colvarFecActua.AutoIncrement = false;
				colvarFecActua.IsNullable = false;
				colvarFecActua.IsPrimaryKey = false;
				colvarFecActua.IsForeignKey = false;
				colvarFecActua.IsReadOnly = false;
				colvarFecActua.DefaultSetting = @"";
				colvarFecActua.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFecActua);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["pvdSTPTerrestre"].AddSchema("stp_tb_aeronaves_respaldo",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("IdAeronaveRespaldo")]
		[Bindable(true)]
		public int IdAeronaveRespaldo 
		{
			get { return GetColumnValue<int>(Columns.IdAeronaveRespaldo); }
			set { SetColumnValue(Columns.IdAeronaveRespaldo, value); }
		}
		  
		[XmlAttribute("FkAeronave")]
		[Bindable(true)]
		public int FkAeronave 
		{
			get { return GetColumnValue<int>(Columns.FkAeronave); }
			set { SetColumnValue(Columns.FkAeronave, value); }
		}
		  
		[XmlAttribute("FkVuelo")]
		[Bindable(true)]
		public int FkVuelo 
		{
			get { return GetColumnValue<int>(Columns.FkVuelo); }
			set { SetColumnValue(Columns.FkVuelo, value); }
		}
		  
		[XmlAttribute("FkContrato")]
		[Bindable(true)]
		public int FkContrato 
		{
			get { return GetColumnValue<int>(Columns.FkContrato); }
			set { SetColumnValue(Columns.FkContrato, value); }
		}
		  
		[XmlAttribute("FicActua")]
		[Bindable(true)]
		public string FicActua 
		{
			get { return GetColumnValue<string>(Columns.FicActua); }
			set { SetColumnValue(Columns.FicActua, value); }
		}
		  
		[XmlAttribute("FecActua")]
		[Bindable(true)]
		public DateTime FecActua 
		{
			get { return GetColumnValue<DateTime>(Columns.FecActua); }
			set { SetColumnValue(Columns.FecActua, value); }
		}
		
		#endregion
		
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a StpTbVuelos ActiveRecord object related to this StpTbAeronavesRespaldo
		/// 
		/// </summary>
		public TransporteTerrestre.Datos.StpTbVuelos StpTbVuelos
		{
			get { return TransporteTerrestre.Datos.StpTbVuelos.FetchByID(this.FkVuelo); }
			set { SetColumnValue("fk_vuelo", value.IdVuelo); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(int varFkAeronave,int varFkVuelo,int varFkContrato,string varFicActua,DateTime varFecActua)
		{
			StpTbAeronavesRespaldo item = new StpTbAeronavesRespaldo();
			
			item.FkAeronave = varFkAeronave;
			
			item.FkVuelo = varFkVuelo;
			
			item.FkContrato = varFkContrato;
			
			item.FicActua = varFicActua;
			
			item.FecActua = varFecActua;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varIdAeronaveRespaldo,int varFkAeronave,int varFkVuelo,int varFkContrato,string varFicActua,DateTime varFecActua)
		{
			StpTbAeronavesRespaldo item = new StpTbAeronavesRespaldo();
			
				item.IdAeronaveRespaldo = varIdAeronaveRespaldo;
			
				item.FkAeronave = varFkAeronave;
			
				item.FkVuelo = varFkVuelo;
			
				item.FkContrato = varFkContrato;
			
				item.FicActua = varFicActua;
			
				item.FecActua = varFecActua;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdAeronaveRespaldoColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn FkAeronaveColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn FkVueloColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn FkContratoColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn FicActuaColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn FecActuaColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string IdAeronaveRespaldo = @"id_aeronave_respaldo";
			 public static string FkAeronave = @"fk_aeronave";
			 public static string FkVuelo = @"fk_vuelo";
			 public static string FkContrato = @"fk_contrato";
			 public static string FicActua = @"fic_actua";
			 public static string FecActua = @"fec_actua";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}
