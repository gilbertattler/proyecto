using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace TransporteTerrestre.Datos
{
	/// <summary>
	/// Strongly-typed collection for the CatContratos class.
	/// </summary>
    [Serializable]
	public partial class CatContratosCollection : ActiveList<CatContratos, CatContratosCollection>
	{	   
		public CatContratosCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>CatContratosCollection</returns>
		public CatContratosCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                CatContratos o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the CatContratos table.
	/// </summary>
	[Serializable]
	public partial class CatContratos : ActiveRecord<CatContratos>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public CatContratos()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public CatContratos(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public CatContratos(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public CatContratos(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("CatContratos", TableType.Table, DataService.GetInstance("pvdSTPTerrestre"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarIdContrato = new TableSchema.TableColumn(schema);
				colvarIdContrato.ColumnName = "IdContrato";
				colvarIdContrato.DataType = DbType.Int32;
				colvarIdContrato.MaxLength = 0;
				colvarIdContrato.AutoIncrement = true;
				colvarIdContrato.IsNullable = false;
				colvarIdContrato.IsPrimaryKey = true;
				colvarIdContrato.IsForeignKey = false;
				colvarIdContrato.IsReadOnly = false;
				colvarIdContrato.DefaultSetting = @"";
				colvarIdContrato.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdContrato);
				
				TableSchema.TableColumn colvarContrato = new TableSchema.TableColumn(schema);
				colvarContrato.ColumnName = "Contrato";
				colvarContrato.DataType = DbType.Int32;
				colvarContrato.MaxLength = 0;
				colvarContrato.AutoIncrement = false;
				colvarContrato.IsNullable = true;
				colvarContrato.IsPrimaryKey = false;
				colvarContrato.IsForeignKey = false;
				colvarContrato.IsReadOnly = false;
				colvarContrato.DefaultSetting = @"";
				colvarContrato.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContrato);
				
				TableSchema.TableColumn colvarFechaInicio = new TableSchema.TableColumn(schema);
				colvarFechaInicio.ColumnName = "FechaInicio";
				colvarFechaInicio.DataType = DbType.DateTime;
				colvarFechaInicio.MaxLength = 0;
				colvarFechaInicio.AutoIncrement = false;
				colvarFechaInicio.IsNullable = true;
				colvarFechaInicio.IsPrimaryKey = false;
				colvarFechaInicio.IsForeignKey = false;
				colvarFechaInicio.IsReadOnly = false;
				colvarFechaInicio.DefaultSetting = @"";
				colvarFechaInicio.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFechaInicio);
				
				TableSchema.TableColumn colvarFechaFin = new TableSchema.TableColumn(schema);
				colvarFechaFin.ColumnName = "FechaFin";
				colvarFechaFin.DataType = DbType.DateTime;
				colvarFechaFin.MaxLength = 0;
				colvarFechaFin.AutoIncrement = false;
				colvarFechaFin.IsNullable = true;
				colvarFechaFin.IsPrimaryKey = false;
				colvarFechaFin.IsForeignKey = false;
				colvarFechaFin.IsReadOnly = false;
				colvarFechaFin.DefaultSetting = @"";
				colvarFechaFin.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFechaFin);
				
				TableSchema.TableColumn colvarDescripcion = new TableSchema.TableColumn(schema);
				colvarDescripcion.ColumnName = "Descripcion";
				colvarDescripcion.DataType = DbType.AnsiString;
				colvarDescripcion.MaxLength = 100;
				colvarDescripcion.AutoIncrement = false;
				colvarDescripcion.IsNullable = true;
				colvarDescripcion.IsPrimaryKey = false;
				colvarDescripcion.IsForeignKey = false;
				colvarDescripcion.IsReadOnly = false;
				colvarDescripcion.DefaultSetting = @"";
				colvarDescripcion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDescripcion);
				
				TableSchema.TableColumn colvarBorradoLogico = new TableSchema.TableColumn(schema);
				colvarBorradoLogico.ColumnName = "BorradoLogico";
				colvarBorradoLogico.DataType = DbType.Boolean;
				colvarBorradoLogico.MaxLength = 0;
				colvarBorradoLogico.AutoIncrement = false;
				colvarBorradoLogico.IsNullable = true;
				colvarBorradoLogico.IsPrimaryKey = false;
				colvarBorradoLogico.IsForeignKey = false;
				colvarBorradoLogico.IsReadOnly = false;
				colvarBorradoLogico.DefaultSetting = @"";
				colvarBorradoLogico.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBorradoLogico);
				
				TableSchema.TableColumn colvarNodo = new TableSchema.TableColumn(schema);
				colvarNodo.ColumnName = "Nodo";
				colvarNodo.DataType = DbType.String;
				colvarNodo.MaxLength = 50;
				colvarNodo.AutoIncrement = false;
				colvarNodo.IsNullable = true;
				colvarNodo.IsPrimaryKey = false;
				colvarNodo.IsForeignKey = false;
				colvarNodo.IsReadOnly = false;
				colvarNodo.DefaultSetting = @"";
				colvarNodo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNodo);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["pvdSTPTerrestre"].AddSchema("CatContratos",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("IdContrato")]
		[Bindable(true)]
		public int IdContrato 
		{
			get { return GetColumnValue<int>(Columns.IdContrato); }
			set { SetColumnValue(Columns.IdContrato, value); }
		}
		  
		[XmlAttribute("Contrato")]
		[Bindable(true)]
		public int? Contrato 
		{
			get { return GetColumnValue<int?>(Columns.Contrato); }
			set { SetColumnValue(Columns.Contrato, value); }
		}
		  
		[XmlAttribute("FechaInicio")]
		[Bindable(true)]
		public DateTime? FechaInicio 
		{
			get { return GetColumnValue<DateTime?>(Columns.FechaInicio); }
			set { SetColumnValue(Columns.FechaInicio, value); }
		}
		  
		[XmlAttribute("FechaFin")]
		[Bindable(true)]
		public DateTime? FechaFin 
		{
			get { return GetColumnValue<DateTime?>(Columns.FechaFin); }
			set { SetColumnValue(Columns.FechaFin, value); }
		}
		  
		[XmlAttribute("Descripcion")]
		[Bindable(true)]
		public string Descripcion 
		{
			get { return GetColumnValue<string>(Columns.Descripcion); }
			set { SetColumnValue(Columns.Descripcion, value); }
		}
		  
		[XmlAttribute("BorradoLogico")]
		[Bindable(true)]
		public bool? BorradoLogico 
		{
			get { return GetColumnValue<bool?>(Columns.BorradoLogico); }
			set { SetColumnValue(Columns.BorradoLogico, value); }
		}
		  
		[XmlAttribute("Nodo")]
		[Bindable(true)]
		public string Nodo 
		{
			get { return GetColumnValue<string>(Columns.Nodo); }
			set { SetColumnValue(Columns.Nodo, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(int? varContrato,DateTime? varFechaInicio,DateTime? varFechaFin,string varDescripcion,bool? varBorradoLogico,string varNodo)
		{
			CatContratos item = new CatContratos();
			
			item.Contrato = varContrato;
			
			item.FechaInicio = varFechaInicio;
			
			item.FechaFin = varFechaFin;
			
			item.Descripcion = varDescripcion;
			
			item.BorradoLogico = varBorradoLogico;
			
			item.Nodo = varNodo;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varIdContrato,int? varContrato,DateTime? varFechaInicio,DateTime? varFechaFin,string varDescripcion,bool? varBorradoLogico,string varNodo)
		{
			CatContratos item = new CatContratos();
			
				item.IdContrato = varIdContrato;
			
				item.Contrato = varContrato;
			
				item.FechaInicio = varFechaInicio;
			
				item.FechaFin = varFechaFin;
			
				item.Descripcion = varDescripcion;
			
				item.BorradoLogico = varBorradoLogico;
			
				item.Nodo = varNodo;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdContratoColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn ContratoColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn FechaInicioColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn FechaFinColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn DescripcionColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn BorradoLogicoColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn NodoColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string IdContrato = @"IdContrato";
			 public static string Contrato = @"Contrato";
			 public static string FechaInicio = @"FechaInicio";
			 public static string FechaFin = @"FechaFin";
			 public static string Descripcion = @"Descripcion";
			 public static string BorradoLogico = @"BorradoLogico";
			 public static string Nodo = @"Nodo";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}
