using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace TransporteTerrestre.Datos
{
    /// <summary>
    /// Controller class for TabActividadesPC
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class TabActividadesPCController
    {
        // Preload our schema..
        TabActividadesPC thisSchemaLoad = new TabActividadesPC();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public TabActividadesPCCollection FetchAll()
        {
            TabActividadesPCCollection coll = new TabActividadesPCCollection();
            Query qry = new Query(TabActividadesPC.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public TabActividadesPCCollection FetchByID(object IdActividadPC)
        {
            TabActividadesPCCollection coll = new TabActividadesPCCollection().Where("IdActividadPC", IdActividadPC).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public TabActividadesPCCollection FetchByQuery(Query qry)
        {
            TabActividadesPCCollection coll = new TabActividadesPCCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdActividadPC)
        {
            return (TabActividadesPC.Delete(IdActividadPC) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdActividadPC)
        {
            return (TabActividadesPC.Destroy(IdActividadPC) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(int FkIdPuntoControl,string NombreActividad,DateTime? FechaHoraInicio,DateTime? FechaHoraTermino,string Observaciones,int? PaxCarga,int? PaxDescarga,int IdActividadPCAnterior,int IdActividadPCSiguiente,bool BorradoLogico,string Nodo)
	    {
		    TabActividadesPC item = new TabActividadesPC();
		    
            item.FkIdPuntoControl = FkIdPuntoControl;
            
            item.NombreActividad = NombreActividad;
            
            item.FechaHoraInicio = FechaHoraInicio;
            
            item.FechaHoraTermino = FechaHoraTermino;
            
            item.Observaciones = Observaciones;
            
            item.PaxCarga = PaxCarga;
            
            item.PaxDescarga = PaxDescarga;
            
            item.IdActividadPCAnterior = IdActividadPCAnterior;
            
            item.IdActividadPCSiguiente = IdActividadPCSiguiente;
            
            item.BorradoLogico = BorradoLogico;
            
            item.Nodo = Nodo;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdActividadPC,int FkIdPuntoControl,string NombreActividad,DateTime? FechaHoraInicio,DateTime? FechaHoraTermino,string Observaciones,int? PaxCarga,int? PaxDescarga,int IdActividadPCAnterior,int IdActividadPCSiguiente,bool BorradoLogico,string Nodo)
	    {
		    TabActividadesPC item = new TabActividadesPC();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdActividadPC = IdActividadPC;
				
			item.FkIdPuntoControl = FkIdPuntoControl;
				
			item.NombreActividad = NombreActividad;
				
			item.FechaHoraInicio = FechaHoraInicio;
				
			item.FechaHoraTermino = FechaHoraTermino;
				
			item.Observaciones = Observaciones;
				
			item.PaxCarga = PaxCarga;
				
			item.PaxDescarga = PaxDescarga;
				
			item.IdActividadPCAnterior = IdActividadPCAnterior;
				
			item.IdActividadPCSiguiente = IdActividadPCSiguiente;
				
			item.BorradoLogico = BorradoLogico;
				
			item.Nodo = Nodo;
				
	        item.Save(UserName);
	    }
    }
}
