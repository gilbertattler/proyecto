using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace TransporteTerrestre.Datos
{
    /// <summary>
    /// Controller class for POAGerenciaHistorico
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class POAGerenciaHistoricoController
    {
        // Preload our schema..
        POAGerenciaHistorico thisSchemaLoad = new POAGerenciaHistorico();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public POAGerenciaHistoricoCollection FetchAll()
        {
            POAGerenciaHistoricoCollection coll = new POAGerenciaHistoricoCollection();
            Query qry = new Query(POAGerenciaHistorico.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public POAGerenciaHistoricoCollection FetchByID(object IdPOAGerenciaHistorico)
        {
            POAGerenciaHistoricoCollection coll = new POAGerenciaHistoricoCollection().Where("IdPOAGerenciaHistorico", IdPOAGerenciaHistorico).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public POAGerenciaHistoricoCollection FetchByQuery(Query qry)
        {
            POAGerenciaHistoricoCollection coll = new POAGerenciaHistoricoCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdPOAGerenciaHistorico)
        {
            return (POAGerenciaHistorico.Delete(IdPOAGerenciaHistorico) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdPOAGerenciaHistorico)
        {
            return (POAGerenciaHistorico.Destroy(IdPOAGerenciaHistorico) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(int FkIdGerencia,string NombreGerencia,int ReservacionAHEnero,int ReservacionAHFebrero,int ReservacionAHMarzo,int ReservacionAHAbril,int ReservacionAHMayo,int ReservacionAHJunio,int ReservacionAHJulio,int ReservacionAHAgosto,int ReservacionAHSeptiembre,int ReservacionAHOctubre,int ReservacionAHNoviembre,int ReservacionAHDiciembre,int IdPOAActivoHistorico)
	    {
		    POAGerenciaHistorico item = new POAGerenciaHistorico();
		    
            item.FkIdGerencia = FkIdGerencia;
            
            item.NombreGerencia = NombreGerencia;
            
            item.ReservacionAHEnero = ReservacionAHEnero;
            
            item.ReservacionAHFebrero = ReservacionAHFebrero;
            
            item.ReservacionAHMarzo = ReservacionAHMarzo;
            
            item.ReservacionAHAbril = ReservacionAHAbril;
            
            item.ReservacionAHMayo = ReservacionAHMayo;
            
            item.ReservacionAHJunio = ReservacionAHJunio;
            
            item.ReservacionAHJulio = ReservacionAHJulio;
            
            item.ReservacionAHAgosto = ReservacionAHAgosto;
            
            item.ReservacionAHSeptiembre = ReservacionAHSeptiembre;
            
            item.ReservacionAHOctubre = ReservacionAHOctubre;
            
            item.ReservacionAHNoviembre = ReservacionAHNoviembre;
            
            item.ReservacionAHDiciembre = ReservacionAHDiciembre;
            
            item.IdPOAActivoHistorico = IdPOAActivoHistorico;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdPOAGerenciaHistorico,int FkIdGerencia,string NombreGerencia,int ReservacionAHEnero,int ReservacionAHFebrero,int ReservacionAHMarzo,int ReservacionAHAbril,int ReservacionAHMayo,int ReservacionAHJunio,int ReservacionAHJulio,int ReservacionAHAgosto,int ReservacionAHSeptiembre,int ReservacionAHOctubre,int ReservacionAHNoviembre,int ReservacionAHDiciembre,int IdPOAActivoHistorico)
	    {
		    POAGerenciaHistorico item = new POAGerenciaHistorico();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdPOAGerenciaHistorico = IdPOAGerenciaHistorico;
				
			item.FkIdGerencia = FkIdGerencia;
				
			item.NombreGerencia = NombreGerencia;
				
			item.ReservacionAHEnero = ReservacionAHEnero;
				
			item.ReservacionAHFebrero = ReservacionAHFebrero;
				
			item.ReservacionAHMarzo = ReservacionAHMarzo;
				
			item.ReservacionAHAbril = ReservacionAHAbril;
				
			item.ReservacionAHMayo = ReservacionAHMayo;
				
			item.ReservacionAHJunio = ReservacionAHJunio;
				
			item.ReservacionAHJulio = ReservacionAHJulio;
				
			item.ReservacionAHAgosto = ReservacionAHAgosto;
				
			item.ReservacionAHSeptiembre = ReservacionAHSeptiembre;
				
			item.ReservacionAHOctubre = ReservacionAHOctubre;
				
			item.ReservacionAHNoviembre = ReservacionAHNoviembre;
				
			item.ReservacionAHDiciembre = ReservacionAHDiciembre;
				
			item.IdPOAActivoHistorico = IdPOAActivoHistorico;
				
	        item.Save(UserName);
	    }
    }
}
