using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace TransporteTerrestre.Datos
{
    /// <summary>
    /// Controller class for stp_tb_pasajeros_vias
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class StpTbPasajerosViasController
    {
        // Preload our schema..
        StpTbPasajerosVias thisSchemaLoad = new StpTbPasajerosVias();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public StpTbPasajerosViasCollection FetchAll()
        {
            StpTbPasajerosViasCollection coll = new StpTbPasajerosViasCollection();
            Query qry = new Query(StpTbPasajerosVias.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public StpTbPasajerosViasCollection FetchByID(object IdPaxVia)
        {
            StpTbPasajerosViasCollection coll = new StpTbPasajerosViasCollection().Where("id_pax_via", IdPaxVia).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public StpTbPasajerosViasCollection FetchByQuery(Query qry)
        {
            StpTbPasajerosViasCollection coll = new StpTbPasajerosViasCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdPaxVia)
        {
            return (StpTbPasajerosVias.Delete(IdPaxVia) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdPaxVia)
        {
            return (StpTbPasajerosVias.Destroy(IdPaxVia) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(int? FkDetalleSolicitudIntegral,int? FkInstalacionVia,int FkProgSecuencia,string FicActua,DateTime FecActua)
	    {
		    StpTbPasajerosVias item = new StpTbPasajerosVias();
		    
            item.FkDetalleSolicitudIntegral = FkDetalleSolicitudIntegral;
            
            item.FkInstalacionVia = FkInstalacionVia;
            
            item.FkProgSecuencia = FkProgSecuencia;
            
            item.FicActua = FicActua;
            
            item.FecActua = FecActua;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdPaxVia,int? FkDetalleSolicitudIntegral,int? FkInstalacionVia,int FkProgSecuencia,string FicActua,DateTime FecActua)
	    {
		    StpTbPasajerosVias item = new StpTbPasajerosVias();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdPaxVia = IdPaxVia;
				
			item.FkDetalleSolicitudIntegral = FkDetalleSolicitudIntegral;
				
			item.FkInstalacionVia = FkInstalacionVia;
				
			item.FkProgSecuencia = FkProgSecuencia;
				
			item.FicActua = FicActua;
				
			item.FecActua = FecActua;
				
	        item.Save(UserName);
	    }
    }
}
