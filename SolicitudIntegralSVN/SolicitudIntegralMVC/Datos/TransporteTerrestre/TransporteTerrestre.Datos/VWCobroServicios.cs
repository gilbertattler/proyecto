using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace TransporteTerrestre.Datos{
    /// <summary>
    /// Strongly-typed collection for the VWCobroServicios class.
    /// </summary>
    [Serializable]
    public partial class VWCobroServiciosCollection : ReadOnlyList<VWCobroServicios, VWCobroServiciosCollection>
    {        
        public VWCobroServiciosCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the VWCobroServicios view.
    /// </summary>
    [Serializable]
    public partial class VWCobroServicios : ReadOnlyRecord<VWCobroServicios>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("VWCobroServicios", TableType.View, DataService.GetInstance("pvdSTPTerrestre"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarIdSolicitudTerrestre = new TableSchema.TableColumn(schema);
                colvarIdSolicitudTerrestre.ColumnName = "IdSolicitudTerrestre";
                colvarIdSolicitudTerrestre.DataType = DbType.Int32;
                colvarIdSolicitudTerrestre.MaxLength = 0;
                colvarIdSolicitudTerrestre.AutoIncrement = false;
                colvarIdSolicitudTerrestre.IsNullable = false;
                colvarIdSolicitudTerrestre.IsPrimaryKey = false;
                colvarIdSolicitudTerrestre.IsForeignKey = false;
                colvarIdSolicitudTerrestre.IsReadOnly = false;
                
                schema.Columns.Add(colvarIdSolicitudTerrestre);
                
                TableSchema.TableColumn colvarFkIdViaje = new TableSchema.TableColumn(schema);
                colvarFkIdViaje.ColumnName = "FkIdViaje";
                colvarFkIdViaje.DataType = DbType.Int32;
                colvarFkIdViaje.MaxLength = 0;
                colvarFkIdViaje.AutoIncrement = false;
                colvarFkIdViaje.IsNullable = true;
                colvarFkIdViaje.IsPrimaryKey = false;
                colvarFkIdViaje.IsForeignKey = false;
                colvarFkIdViaje.IsReadOnly = false;
                
                schema.Columns.Add(colvarFkIdViaje);
                
                TableSchema.TableColumn colvarFkIdServicio = new TableSchema.TableColumn(schema);
                colvarFkIdServicio.ColumnName = "FkIdServicio";
                colvarFkIdServicio.DataType = DbType.Int32;
                colvarFkIdServicio.MaxLength = 0;
                colvarFkIdServicio.AutoIncrement = false;
                colvarFkIdServicio.IsNullable = false;
                colvarFkIdServicio.IsPrimaryKey = false;
                colvarFkIdServicio.IsForeignKey = false;
                colvarFkIdServicio.IsReadOnly = false;
                
                schema.Columns.Add(colvarFkIdServicio);
                
                TableSchema.TableColumn colvarCveServicio = new TableSchema.TableColumn(schema);
                colvarCveServicio.ColumnName = "CveServicio";
                colvarCveServicio.DataType = DbType.AnsiString;
                colvarCveServicio.MaxLength = 20;
                colvarCveServicio.AutoIncrement = false;
                colvarCveServicio.IsNullable = false;
                colvarCveServicio.IsPrimaryKey = false;
                colvarCveServicio.IsForeignKey = false;
                colvarCveServicio.IsReadOnly = false;
                
                schema.Columns.Add(colvarCveServicio);
                
                TableSchema.TableColumn colvarCobro = new TableSchema.TableColumn(schema);
                colvarCobro.ColumnName = "Cobro";
                colvarCobro.DataType = DbType.Boolean;
                colvarCobro.MaxLength = 0;
                colvarCobro.AutoIncrement = false;
                colvarCobro.IsNullable = false;
                colvarCobro.IsPrimaryKey = false;
                colvarCobro.IsForeignKey = false;
                colvarCobro.IsReadOnly = false;
                
                schema.Columns.Add(colvarCobro);
                
                TableSchema.TableColumn colvarNombreActivo = new TableSchema.TableColumn(schema);
                colvarNombreActivo.ColumnName = "NOMBRE_ACTIVO";
                colvarNombreActivo.DataType = DbType.AnsiString;
                colvarNombreActivo.MaxLength = 150;
                colvarNombreActivo.AutoIncrement = false;
                colvarNombreActivo.IsNullable = true;
                colvarNombreActivo.IsPrimaryKey = false;
                colvarNombreActivo.IsForeignKey = false;
                colvarNombreActivo.IsReadOnly = false;
                
                schema.Columns.Add(colvarNombreActivo);
                
                TableSchema.TableColumn colvarSiglasActivo = new TableSchema.TableColumn(schema);
                colvarSiglasActivo.ColumnName = "SIGLAS_ACTIVO";
                colvarSiglasActivo.DataType = DbType.AnsiString;
                colvarSiglasActivo.MaxLength = 50;
                colvarSiglasActivo.AutoIncrement = false;
                colvarSiglasActivo.IsNullable = true;
                colvarSiglasActivo.IsPrimaryKey = false;
                colvarSiglasActivo.IsForeignKey = false;
                colvarSiglasActivo.IsReadOnly = false;
                
                schema.Columns.Add(colvarSiglasActivo);
                
                TableSchema.TableColumn colvarNodo = new TableSchema.TableColumn(schema);
                colvarNodo.ColumnName = "Nodo";
                colvarNodo.DataType = DbType.String;
                colvarNodo.MaxLength = 50;
                colvarNodo.AutoIncrement = false;
                colvarNodo.IsNullable = true;
                colvarNodo.IsPrimaryKey = false;
                colvarNodo.IsForeignKey = false;
                colvarNodo.IsReadOnly = false;
                
                schema.Columns.Add(colvarNodo);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["pvdSTPTerrestre"].AddSchema("VWCobroServicios",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public VWCobroServicios()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public VWCobroServicios(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public VWCobroServicios(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public VWCobroServicios(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("IdSolicitudTerrestre")]
        [Bindable(true)]
        public int IdSolicitudTerrestre 
	    {
		    get
		    {
			    return GetColumnValue<int>("IdSolicitudTerrestre");
		    }
            set 
		    {
			    SetColumnValue("IdSolicitudTerrestre", value);
            }
        }
	      
        [XmlAttribute("FkIdViaje")]
        [Bindable(true)]
        public int? FkIdViaje 
	    {
		    get
		    {
			    return GetColumnValue<int?>("FkIdViaje");
		    }
            set 
		    {
			    SetColumnValue("FkIdViaje", value);
            }
        }
	      
        [XmlAttribute("FkIdServicio")]
        [Bindable(true)]
        public int FkIdServicio 
	    {
		    get
		    {
			    return GetColumnValue<int>("FkIdServicio");
		    }
            set 
		    {
			    SetColumnValue("FkIdServicio", value);
            }
        }
	      
        [XmlAttribute("CveServicio")]
        [Bindable(true)]
        public string CveServicio 
	    {
		    get
		    {
			    return GetColumnValue<string>("CveServicio");
		    }
            set 
		    {
			    SetColumnValue("CveServicio", value);
            }
        }
	      
        [XmlAttribute("Cobro")]
        [Bindable(true)]
        public bool Cobro 
	    {
		    get
		    {
			    return GetColumnValue<bool>("Cobro");
		    }
            set 
		    {
			    SetColumnValue("Cobro", value);
            }
        }
	      
        [XmlAttribute("NombreActivo")]
        [Bindable(true)]
        public string NombreActivo 
	    {
		    get
		    {
			    return GetColumnValue<string>("NOMBRE_ACTIVO");
		    }
            set 
		    {
			    SetColumnValue("NOMBRE_ACTIVO", value);
            }
        }
	      
        [XmlAttribute("SiglasActivo")]
        [Bindable(true)]
        public string SiglasActivo 
	    {
		    get
		    {
			    return GetColumnValue<string>("SIGLAS_ACTIVO");
		    }
            set 
		    {
			    SetColumnValue("SIGLAS_ACTIVO", value);
            }
        }
	      
        [XmlAttribute("Nodo")]
        [Bindable(true)]
        public string Nodo 
	    {
		    get
		    {
			    return GetColumnValue<string>("Nodo");
		    }
            set 
		    {
			    SetColumnValue("Nodo", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string IdSolicitudTerrestre = @"IdSolicitudTerrestre";
            
            public static string FkIdViaje = @"FkIdViaje";
            
            public static string FkIdServicio = @"FkIdServicio";
            
            public static string CveServicio = @"CveServicio";
            
            public static string Cobro = @"Cobro";
            
            public static string NombreActivo = @"NOMBRE_ACTIVO";
            
            public static string SiglasActivo = @"SIGLAS_ACTIVO";
            
            public static string Nodo = @"Nodo";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}
