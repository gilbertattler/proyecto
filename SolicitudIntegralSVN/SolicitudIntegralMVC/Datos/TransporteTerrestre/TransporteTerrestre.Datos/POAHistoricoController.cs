using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace TransporteTerrestre.Datos
{
    /// <summary>
    /// Controller class for POAHistorico
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class POAHistoricoController
    {
        // Preload our schema..
        POAHistorico thisSchemaLoad = new POAHistorico();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public POAHistoricoCollection FetchAll()
        {
            POAHistoricoCollection coll = new POAHistoricoCollection();
            Query qry = new Query(POAHistorico.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public POAHistoricoCollection FetchByID(object IdPOAHistorico)
        {
            POAHistoricoCollection coll = new POAHistoricoCollection().Where("IdPOAHistorico", IdPOAHistorico).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public POAHistoricoCollection FetchByQuery(Query qry)
        {
            POAHistoricoCollection coll = new POAHistoricoCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdPOAHistorico)
        {
            return (POAHistorico.Delete(IdPOAHistorico) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdPOAHistorico)
        {
            return (POAHistorico.Destroy(IdPOAHistorico) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string AnioPOA,DateTime FechaCarga,DateTime FechaUltimaActualizacion,string FichaUsuarioCargaPOA,string NombreUsuarioCargaPOA,int UltimaVersionPOA,string ObservacionesVersion,int IdPOA)
	    {
		    POAHistorico item = new POAHistorico();
		    
            item.AnioPOA = AnioPOA;
            
            item.FechaCarga = FechaCarga;
            
            item.FechaUltimaActualizacion = FechaUltimaActualizacion;
            
            item.FichaUsuarioCargaPOA = FichaUsuarioCargaPOA;
            
            item.NombreUsuarioCargaPOA = NombreUsuarioCargaPOA;
            
            item.UltimaVersionPOA = UltimaVersionPOA;
            
            item.ObservacionesVersion = ObservacionesVersion;
            
            item.IdPOA = IdPOA;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdPOAHistorico,string AnioPOA,DateTime FechaCarga,DateTime FechaUltimaActualizacion,string FichaUsuarioCargaPOA,string NombreUsuarioCargaPOA,int UltimaVersionPOA,string ObservacionesVersion,int IdPOA)
	    {
		    POAHistorico item = new POAHistorico();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdPOAHistorico = IdPOAHistorico;
				
			item.AnioPOA = AnioPOA;
				
			item.FechaCarga = FechaCarga;
				
			item.FechaUltimaActualizacion = FechaUltimaActualizacion;
				
			item.FichaUsuarioCargaPOA = FichaUsuarioCargaPOA;
				
			item.NombreUsuarioCargaPOA = NombreUsuarioCargaPOA;
				
			item.UltimaVersionPOA = UltimaVersionPOA;
				
			item.ObservacionesVersion = ObservacionesVersion;
				
			item.IdPOA = IdPOA;
				
	        item.Save(UserName);
	    }
    }
}
