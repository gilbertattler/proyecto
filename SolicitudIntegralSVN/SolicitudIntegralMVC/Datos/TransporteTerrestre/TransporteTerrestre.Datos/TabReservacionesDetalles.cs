using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace TransporteTerrestre.Datos
{
	/// <summary>
	/// Strongly-typed collection for the TabReservacionesDetalles class.
	/// </summary>
    [Serializable]
	public partial class TabReservacionesDetallesCollection : ActiveList<TabReservacionesDetalles, TabReservacionesDetallesCollection>
	{	   
		public TabReservacionesDetallesCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>TabReservacionesDetallesCollection</returns>
		public TabReservacionesDetallesCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                TabReservacionesDetalles o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the Tab_Reservaciones_Detalles table.
	/// </summary>
	[Serializable]
	public partial class TabReservacionesDetalles : ActiveRecord<TabReservacionesDetalles>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public TabReservacionesDetalles()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public TabReservacionesDetalles(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public TabReservacionesDetalles(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public TabReservacionesDetalles(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("Tab_Reservaciones_Detalles", TableType.Table, DataService.GetInstance("pvdSTPTerrestre"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarIdDetalleReservacion = new TableSchema.TableColumn(schema);
				colvarIdDetalleReservacion.ColumnName = "IdDetalleReservacion";
				colvarIdDetalleReservacion.DataType = DbType.Int32;
				colvarIdDetalleReservacion.MaxLength = 0;
				colvarIdDetalleReservacion.AutoIncrement = true;
				colvarIdDetalleReservacion.IsNullable = false;
				colvarIdDetalleReservacion.IsPrimaryKey = true;
				colvarIdDetalleReservacion.IsForeignKey = false;
				colvarIdDetalleReservacion.IsReadOnly = false;
				colvarIdDetalleReservacion.DefaultSetting = @"";
				colvarIdDetalleReservacion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdDetalleReservacion);
				
				TableSchema.TableColumn colvarIdReservacionInstalacion = new TableSchema.TableColumn(schema);
				colvarIdReservacionInstalacion.ColumnName = "IdReservacionInstalacion";
				colvarIdReservacionInstalacion.DataType = DbType.Int32;
				colvarIdReservacionInstalacion.MaxLength = 0;
				colvarIdReservacionInstalacion.AutoIncrement = false;
				colvarIdReservacionInstalacion.IsNullable = false;
				colvarIdReservacionInstalacion.IsPrimaryKey = false;
				colvarIdReservacionInstalacion.IsForeignKey = true;
				colvarIdReservacionInstalacion.IsReadOnly = false;
				colvarIdReservacionInstalacion.DefaultSetting = @"";
				
					colvarIdReservacionInstalacion.ForeignKeyTableName = "Tab_Reservaciones_Instalaciones";
				schema.Columns.Add(colvarIdReservacionInstalacion);
				
				TableSchema.TableColumn colvarIdSolicitudReservacion = new TableSchema.TableColumn(schema);
				colvarIdSolicitudReservacion.ColumnName = "IdSolicitudReservacion";
				colvarIdSolicitudReservacion.DataType = DbType.Int32;
				colvarIdSolicitudReservacion.MaxLength = 0;
				colvarIdSolicitudReservacion.AutoIncrement = false;
				colvarIdSolicitudReservacion.IsNullable = true;
				colvarIdSolicitudReservacion.IsPrimaryKey = false;
				colvarIdSolicitudReservacion.IsForeignKey = true;
				colvarIdSolicitudReservacion.IsReadOnly = false;
				colvarIdSolicitudReservacion.DefaultSetting = @"";
				
					colvarIdSolicitudReservacion.ForeignKeyTableName = "Tab_Reservaciones_Solicitudes";
				schema.Columns.Add(colvarIdSolicitudReservacion);
				
				TableSchema.TableColumn colvarSubdivision = new TableSchema.TableColumn(schema);
				colvarSubdivision.ColumnName = "Subdivision";
				colvarSubdivision.DataType = DbType.AnsiString;
				colvarSubdivision.MaxLength = 30;
				colvarSubdivision.AutoIncrement = false;
				colvarSubdivision.IsNullable = true;
				colvarSubdivision.IsPrimaryKey = false;
				colvarSubdivision.IsForeignKey = false;
				colvarSubdivision.IsReadOnly = false;
				colvarSubdivision.DefaultSetting = @"";
				colvarSubdivision.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSubdivision);
				
				TableSchema.TableColumn colvarContrato = new TableSchema.TableColumn(schema);
				colvarContrato.ColumnName = "Contrato";
				colvarContrato.DataType = DbType.AnsiString;
				colvarContrato.MaxLength = 12;
				colvarContrato.AutoIncrement = false;
				colvarContrato.IsNullable = true;
				colvarContrato.IsPrimaryKey = false;
				colvarContrato.IsForeignKey = false;
				colvarContrato.IsReadOnly = false;
				colvarContrato.DefaultSetting = @"";
				colvarContrato.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContrato);
				
				TableSchema.TableColumn colvarFolioEspecial = new TableSchema.TableColumn(schema);
				colvarFolioEspecial.ColumnName = "FolioEspecial";
				colvarFolioEspecial.DataType = DbType.AnsiString;
				colvarFolioEspecial.MaxLength = 20;
				colvarFolioEspecial.AutoIncrement = false;
				colvarFolioEspecial.IsNullable = true;
				colvarFolioEspecial.IsPrimaryKey = false;
				colvarFolioEspecial.IsForeignKey = false;
				colvarFolioEspecial.IsReadOnly = false;
				colvarFolioEspecial.DefaultSetting = @"";
				colvarFolioEspecial.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFolioEspecial);
				
				TableSchema.TableColumn colvarEspaciosLibres = new TableSchema.TableColumn(schema);
				colvarEspaciosLibres.ColumnName = "EspaciosLibres";
				colvarEspaciosLibres.DataType = DbType.Boolean;
				colvarEspaciosLibres.MaxLength = 0;
				colvarEspaciosLibres.AutoIncrement = false;
				colvarEspaciosLibres.IsNullable = true;
				colvarEspaciosLibres.IsPrimaryKey = false;
				colvarEspaciosLibres.IsForeignKey = false;
				colvarEspaciosLibres.IsReadOnly = false;
				colvarEspaciosLibres.DefaultSetting = @"";
				colvarEspaciosLibres.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEspaciosLibres);
				
				TableSchema.TableColumn colvarCantidadReservada = new TableSchema.TableColumn(schema);
				colvarCantidadReservada.ColumnName = "CantidadReservada";
				colvarCantidadReservada.DataType = DbType.Int32;
				colvarCantidadReservada.MaxLength = 0;
				colvarCantidadReservada.AutoIncrement = false;
				colvarCantidadReservada.IsNullable = false;
				colvarCantidadReservada.IsPrimaryKey = false;
				colvarCantidadReservada.IsForeignKey = false;
				colvarCantidadReservada.IsReadOnly = false;
				colvarCantidadReservada.DefaultSetting = @"";
				colvarCantidadReservada.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCantidadReservada);
				
				TableSchema.TableColumn colvarCantidadOcupada = new TableSchema.TableColumn(schema);
				colvarCantidadOcupada.ColumnName = "CantidadOcupada";
				colvarCantidadOcupada.DataType = DbType.Int32;
				colvarCantidadOcupada.MaxLength = 0;
				colvarCantidadOcupada.AutoIncrement = false;
				colvarCantidadOcupada.IsNullable = false;
				colvarCantidadOcupada.IsPrimaryKey = false;
				colvarCantidadOcupada.IsForeignKey = false;
				colvarCantidadOcupada.IsReadOnly = false;
				colvarCantidadOcupada.DefaultSetting = @"";
				colvarCantidadOcupada.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCantidadOcupada);
				
				TableSchema.TableColumn colvarCantidadDisponible = new TableSchema.TableColumn(schema);
				colvarCantidadDisponible.ColumnName = "CantidadDisponible";
				colvarCantidadDisponible.DataType = DbType.Int32;
				colvarCantidadDisponible.MaxLength = 0;
				colvarCantidadDisponible.AutoIncrement = false;
				colvarCantidadDisponible.IsNullable = false;
				colvarCantidadDisponible.IsPrimaryKey = false;
				colvarCantidadDisponible.IsForeignKey = false;
				colvarCantidadDisponible.IsReadOnly = false;
				colvarCantidadDisponible.DefaultSetting = @"";
				colvarCantidadDisponible.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCantidadDisponible);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["pvdSTPTerrestre"].AddSchema("Tab_Reservaciones_Detalles",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("IdDetalleReservacion")]
		[Bindable(true)]
		public int IdDetalleReservacion 
		{
			get { return GetColumnValue<int>(Columns.IdDetalleReservacion); }
			set { SetColumnValue(Columns.IdDetalleReservacion, value); }
		}
		  
		[XmlAttribute("IdReservacionInstalacion")]
		[Bindable(true)]
		public int IdReservacionInstalacion 
		{
			get { return GetColumnValue<int>(Columns.IdReservacionInstalacion); }
			set { SetColumnValue(Columns.IdReservacionInstalacion, value); }
		}
		  
		[XmlAttribute("IdSolicitudReservacion")]
		[Bindable(true)]
		public int? IdSolicitudReservacion 
		{
			get { return GetColumnValue<int?>(Columns.IdSolicitudReservacion); }
			set { SetColumnValue(Columns.IdSolicitudReservacion, value); }
		}
		  
		[XmlAttribute("Subdivision")]
		[Bindable(true)]
		public string Subdivision 
		{
			get { return GetColumnValue<string>(Columns.Subdivision); }
			set { SetColumnValue(Columns.Subdivision, value); }
		}
		  
		[XmlAttribute("Contrato")]
		[Bindable(true)]
		public string Contrato 
		{
			get { return GetColumnValue<string>(Columns.Contrato); }
			set { SetColumnValue(Columns.Contrato, value); }
		}
		  
		[XmlAttribute("FolioEspecial")]
		[Bindable(true)]
		public string FolioEspecial 
		{
			get { return GetColumnValue<string>(Columns.FolioEspecial); }
			set { SetColumnValue(Columns.FolioEspecial, value); }
		}
		  
		[XmlAttribute("EspaciosLibres")]
		[Bindable(true)]
		public bool? EspaciosLibres 
		{
			get { return GetColumnValue<bool?>(Columns.EspaciosLibres); }
			set { SetColumnValue(Columns.EspaciosLibres, value); }
		}
		  
		[XmlAttribute("CantidadReservada")]
		[Bindable(true)]
		public int CantidadReservada 
		{
			get { return GetColumnValue<int>(Columns.CantidadReservada); }
			set { SetColumnValue(Columns.CantidadReservada, value); }
		}
		  
		[XmlAttribute("CantidadOcupada")]
		[Bindable(true)]
		public int CantidadOcupada 
		{
			get { return GetColumnValue<int>(Columns.CantidadOcupada); }
			set { SetColumnValue(Columns.CantidadOcupada, value); }
		}
		  
		[XmlAttribute("CantidadDisponible")]
		[Bindable(true)]
		public int CantidadDisponible 
		{
			get { return GetColumnValue<int>(Columns.CantidadDisponible); }
			set { SetColumnValue(Columns.CantidadDisponible, value); }
		}
		
		#endregion
		
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a TabReservacionesInstalaciones ActiveRecord object related to this TabReservacionesDetalles
		/// 
		/// </summary>
		public TransporteTerrestre.Datos.TabReservacionesInstalaciones TabReservacionesInstalaciones
		{
			get { return TransporteTerrestre.Datos.TabReservacionesInstalaciones.FetchByID(this.IdReservacionInstalacion); }
			set { SetColumnValue("IdReservacionInstalacion", value.IdReservacionInstalacion); }
		}
		
		
		/// <summary>
		/// Returns a TabReservacionesSolicitudes ActiveRecord object related to this TabReservacionesDetalles
		/// 
		/// </summary>
		public TransporteTerrestre.Datos.TabReservacionesSolicitudes TabReservacionesSolicitudes
		{
			get { return TransporteTerrestre.Datos.TabReservacionesSolicitudes.FetchByID(this.IdSolicitudReservacion); }
			set { SetColumnValue("IdSolicitudReservacion", value.IdSolicitudReservacion); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(int varIdReservacionInstalacion,int? varIdSolicitudReservacion,string varSubdivision,string varContrato,string varFolioEspecial,bool? varEspaciosLibres,int varCantidadReservada,int varCantidadOcupada,int varCantidadDisponible)
		{
			TabReservacionesDetalles item = new TabReservacionesDetalles();
			
			item.IdReservacionInstalacion = varIdReservacionInstalacion;
			
			item.IdSolicitudReservacion = varIdSolicitudReservacion;
			
			item.Subdivision = varSubdivision;
			
			item.Contrato = varContrato;
			
			item.FolioEspecial = varFolioEspecial;
			
			item.EspaciosLibres = varEspaciosLibres;
			
			item.CantidadReservada = varCantidadReservada;
			
			item.CantidadOcupada = varCantidadOcupada;
			
			item.CantidadDisponible = varCantidadDisponible;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varIdDetalleReservacion,int varIdReservacionInstalacion,int? varIdSolicitudReservacion,string varSubdivision,string varContrato,string varFolioEspecial,bool? varEspaciosLibres,int varCantidadReservada,int varCantidadOcupada,int varCantidadDisponible)
		{
			TabReservacionesDetalles item = new TabReservacionesDetalles();
			
				item.IdDetalleReservacion = varIdDetalleReservacion;
			
				item.IdReservacionInstalacion = varIdReservacionInstalacion;
			
				item.IdSolicitudReservacion = varIdSolicitudReservacion;
			
				item.Subdivision = varSubdivision;
			
				item.Contrato = varContrato;
			
				item.FolioEspecial = varFolioEspecial;
			
				item.EspaciosLibres = varEspaciosLibres;
			
				item.CantidadReservada = varCantidadReservada;
			
				item.CantidadOcupada = varCantidadOcupada;
			
				item.CantidadDisponible = varCantidadDisponible;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdDetalleReservacionColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn IdReservacionInstalacionColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn IdSolicitudReservacionColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn SubdivisionColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ContratoColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn FolioEspecialColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn EspaciosLibresColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CantidadReservadaColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn CantidadOcupadaColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn CantidadDisponibleColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string IdDetalleReservacion = @"IdDetalleReservacion";
			 public static string IdReservacionInstalacion = @"IdReservacionInstalacion";
			 public static string IdSolicitudReservacion = @"IdSolicitudReservacion";
			 public static string Subdivision = @"Subdivision";
			 public static string Contrato = @"Contrato";
			 public static string FolioEspecial = @"FolioEspecial";
			 public static string EspaciosLibres = @"EspaciosLibres";
			 public static string CantidadReservada = @"CantidadReservada";
			 public static string CantidadOcupada = @"CantidadOcupada";
			 public static string CantidadDisponible = @"CantidadDisponible";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}
