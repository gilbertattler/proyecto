using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace TransporteTerrestre.Datos{
    /// <summary>
    /// Strongly-typed collection for the VWGestionGLCAcreedores class.
    /// </summary>
    [Serializable]
    public partial class VWGestionGLCAcreedoresCollection : ReadOnlyList<VWGestionGLCAcreedores, VWGestionGLCAcreedoresCollection>
    {        
        public VWGestionGLCAcreedoresCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the VWGestionGLC_ACREEDORES view.
    /// </summary>
    [Serializable]
    public partial class VWGestionGLCAcreedores : ReadOnlyRecord<VWGestionGLCAcreedores>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("VWGestionGLC_ACREEDORES", TableType.View, DataService.GetInstance("pvdSTPTerrestre"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarIdCAcreedor = new TableSchema.TableColumn(schema);
                colvarIdCAcreedor.ColumnName = "ID_C_ACREEDOR";
                colvarIdCAcreedor.DataType = DbType.Int32;
                colvarIdCAcreedor.MaxLength = 0;
                colvarIdCAcreedor.AutoIncrement = false;
                colvarIdCAcreedor.IsNullable = false;
                colvarIdCAcreedor.IsPrimaryKey = false;
                colvarIdCAcreedor.IsForeignKey = false;
                colvarIdCAcreedor.IsReadOnly = false;
                
                schema.Columns.Add(colvarIdCAcreedor);
                
                TableSchema.TableColumn colvarCveAcreedor = new TableSchema.TableColumn(schema);
                colvarCveAcreedor.ColumnName = "CVE_ACREEDOR";
                colvarCveAcreedor.DataType = DbType.AnsiString;
                colvarCveAcreedor.MaxLength = 50;
                colvarCveAcreedor.AutoIncrement = false;
                colvarCveAcreedor.IsNullable = false;
                colvarCveAcreedor.IsPrimaryKey = false;
                colvarCveAcreedor.IsForeignKey = false;
                colvarCveAcreedor.IsReadOnly = false;
                
                schema.Columns.Add(colvarCveAcreedor);
                
                TableSchema.TableColumn colvarDescripcionAcreedor = new TableSchema.TableColumn(schema);
                colvarDescripcionAcreedor.ColumnName = "DESCRIPCION_ACREEDOR";
                colvarDescripcionAcreedor.DataType = DbType.AnsiString;
                colvarDescripcionAcreedor.MaxLength = 250;
                colvarDescripcionAcreedor.AutoIncrement = false;
                colvarDescripcionAcreedor.IsNullable = false;
                colvarDescripcionAcreedor.IsPrimaryKey = false;
                colvarDescripcionAcreedor.IsForeignKey = false;
                colvarDescripcionAcreedor.IsReadOnly = false;
                
                schema.Columns.Add(colvarDescripcionAcreedor);
                
                TableSchema.TableColumn colvarRfcAcreedor = new TableSchema.TableColumn(schema);
                colvarRfcAcreedor.ColumnName = "RFC_ACREEDOR";
                colvarRfcAcreedor.DataType = DbType.AnsiString;
                colvarRfcAcreedor.MaxLength = 50;
                colvarRfcAcreedor.AutoIncrement = false;
                colvarRfcAcreedor.IsNullable = false;
                colvarRfcAcreedor.IsPrimaryKey = false;
                colvarRfcAcreedor.IsForeignKey = false;
                colvarRfcAcreedor.IsReadOnly = false;
                
                schema.Columns.Add(colvarRfcAcreedor);
                
                TableSchema.TableColumn colvarRepresentanteLegal = new TableSchema.TableColumn(schema);
                colvarRepresentanteLegal.ColumnName = "REPRESENTANTE_LEGAL";
                colvarRepresentanteLegal.DataType = DbType.AnsiString;
                colvarRepresentanteLegal.MaxLength = 60;
                colvarRepresentanteLegal.AutoIncrement = false;
                colvarRepresentanteLegal.IsNullable = true;
                colvarRepresentanteLegal.IsPrimaryKey = false;
                colvarRepresentanteLegal.IsForeignKey = false;
                colvarRepresentanteLegal.IsReadOnly = false;
                
                schema.Columns.Add(colvarRepresentanteLegal);
                
                TableSchema.TableColumn colvarCorreo = new TableSchema.TableColumn(schema);
                colvarCorreo.ColumnName = "CORREO";
                colvarCorreo.DataType = DbType.AnsiString;
                colvarCorreo.MaxLength = 70;
                colvarCorreo.AutoIncrement = false;
                colvarCorreo.IsNullable = true;
                colvarCorreo.IsPrimaryKey = false;
                colvarCorreo.IsForeignKey = false;
                colvarCorreo.IsReadOnly = false;
                
                schema.Columns.Add(colvarCorreo);
                
                TableSchema.TableColumn colvarExtTelefono = new TableSchema.TableColumn(schema);
                colvarExtTelefono.ColumnName = "EXT_TELEFONO";
                colvarExtTelefono.DataType = DbType.AnsiString;
                colvarExtTelefono.MaxLength = 20;
                colvarExtTelefono.AutoIncrement = false;
                colvarExtTelefono.IsNullable = true;
                colvarExtTelefono.IsPrimaryKey = false;
                colvarExtTelefono.IsForeignKey = false;
                colvarExtTelefono.IsReadOnly = false;
                
                schema.Columns.Add(colvarExtTelefono);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["pvdSTPTerrestre"].AddSchema("VWGestionGLC_ACREEDORES",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public VWGestionGLCAcreedores()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public VWGestionGLCAcreedores(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public VWGestionGLCAcreedores(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public VWGestionGLCAcreedores(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("IdCAcreedor")]
        [Bindable(true)]
        public int IdCAcreedor 
	    {
		    get
		    {
			    return GetColumnValue<int>("ID_C_ACREEDOR");
		    }
            set 
		    {
			    SetColumnValue("ID_C_ACREEDOR", value);
            }
        }
	      
        [XmlAttribute("CveAcreedor")]
        [Bindable(true)]
        public string CveAcreedor 
	    {
		    get
		    {
			    return GetColumnValue<string>("CVE_ACREEDOR");
		    }
            set 
		    {
			    SetColumnValue("CVE_ACREEDOR", value);
            }
        }
	      
        [XmlAttribute("DescripcionAcreedor")]
        [Bindable(true)]
        public string DescripcionAcreedor 
	    {
		    get
		    {
			    return GetColumnValue<string>("DESCRIPCION_ACREEDOR");
		    }
            set 
		    {
			    SetColumnValue("DESCRIPCION_ACREEDOR", value);
            }
        }
	      
        [XmlAttribute("RfcAcreedor")]
        [Bindable(true)]
        public string RfcAcreedor 
	    {
		    get
		    {
			    return GetColumnValue<string>("RFC_ACREEDOR");
		    }
            set 
		    {
			    SetColumnValue("RFC_ACREEDOR", value);
            }
        }
	      
        [XmlAttribute("RepresentanteLegal")]
        [Bindable(true)]
        public string RepresentanteLegal 
	    {
		    get
		    {
			    return GetColumnValue<string>("REPRESENTANTE_LEGAL");
		    }
            set 
		    {
			    SetColumnValue("REPRESENTANTE_LEGAL", value);
            }
        }
	      
        [XmlAttribute("Correo")]
        [Bindable(true)]
        public string Correo 
	    {
		    get
		    {
			    return GetColumnValue<string>("CORREO");
		    }
            set 
		    {
			    SetColumnValue("CORREO", value);
            }
        }
	      
        [XmlAttribute("ExtTelefono")]
        [Bindable(true)]
        public string ExtTelefono 
	    {
		    get
		    {
			    return GetColumnValue<string>("EXT_TELEFONO");
		    }
            set 
		    {
			    SetColumnValue("EXT_TELEFONO", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string IdCAcreedor = @"ID_C_ACREEDOR";
            
            public static string CveAcreedor = @"CVE_ACREEDOR";
            
            public static string DescripcionAcreedor = @"DESCRIPCION_ACREEDOR";
            
            public static string RfcAcreedor = @"RFC_ACREEDOR";
            
            public static string RepresentanteLegal = @"REPRESENTANTE_LEGAL";
            
            public static string Correo = @"CORREO";
            
            public static string ExtTelefono = @"EXT_TELEFONO";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}
