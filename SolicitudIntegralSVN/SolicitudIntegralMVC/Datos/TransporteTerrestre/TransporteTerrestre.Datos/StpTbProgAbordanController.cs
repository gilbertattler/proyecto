using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace TransporteTerrestre.Datos
{
    /// <summary>
    /// Controller class for stp_tb_prog_abordan
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class StpTbProgAbordanController
    {
        // Preload our schema..
        StpTbProgAbordan thisSchemaLoad = new StpTbProgAbordan();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public StpTbProgAbordanCollection FetchAll()
        {
            StpTbProgAbordanCollection coll = new StpTbProgAbordanCollection();
            Query qry = new Query(StpTbProgAbordan.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public StpTbProgAbordanCollection FetchByID(object IdProgAbordan)
        {
            StpTbProgAbordanCollection coll = new StpTbProgAbordanCollection().Where("id_prog_abordan", IdProgAbordan).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public StpTbProgAbordanCollection FetchByQuery(Query qry)
        {
            StpTbProgAbordanCollection coll = new StpTbProgAbordanCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdProgAbordan)
        {
            return (StpTbProgAbordan.Delete(IdProgAbordan) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdProgAbordan)
        {
            return (StpTbProgAbordan.Destroy(IdProgAbordan) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(int? FkProgSecuencia,int FkInstalacionDestino,int? FkInstalacionVia,int PaxAbordan,string FicActua,DateTime FecActua)
	    {
		    StpTbProgAbordan item = new StpTbProgAbordan();
		    
            item.FkProgSecuencia = FkProgSecuencia;
            
            item.FkInstalacionDestino = FkInstalacionDestino;
            
            item.FkInstalacionVia = FkInstalacionVia;
            
            item.PaxAbordan = PaxAbordan;
            
            item.FicActua = FicActua;
            
            item.FecActua = FecActua;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdProgAbordan,int? FkProgSecuencia,int FkInstalacionDestino,int? FkInstalacionVia,int PaxAbordan,string FicActua,DateTime FecActua)
	    {
		    StpTbProgAbordan item = new StpTbProgAbordan();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdProgAbordan = IdProgAbordan;
				
			item.FkProgSecuencia = FkProgSecuencia;
				
			item.FkInstalacionDestino = FkInstalacionDestino;
				
			item.FkInstalacionVia = FkInstalacionVia;
				
			item.PaxAbordan = PaxAbordan;
				
			item.FicActua = FicActua;
				
			item.FecActua = FecActua;
				
	        item.Save(UserName);
	    }
    }
}
