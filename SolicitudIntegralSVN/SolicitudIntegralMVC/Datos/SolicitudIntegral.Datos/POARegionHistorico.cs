//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SolicitudIntegral.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class POARegionHistorico
    {
        public POARegionHistorico()
        {
            this.POAActivoHistorico = new HashSet<POAActivoHistorico>();
        }
    
        public int IdPOARegionHistorico { get; set; }
        public int FkIdRegion { get; set; }
        public string NombreRegion { get; set; }
        public int TotalCargaRegion { get; set; }
        public int IdPOAHistorico { get; set; }
    
        public virtual ICollection<POAActivoHistorico> POAActivoHistorico { get; set; }
        public virtual POAHistorico POAHistorico { get; set; }
    }
}
