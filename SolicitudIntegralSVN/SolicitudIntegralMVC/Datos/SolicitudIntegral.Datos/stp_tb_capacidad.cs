//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SolicitudIntegral.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class stp_tb_capacidad
    {
        public stp_tb_capacidad()
        {
            this.stp_tb_capacidad_contrato = new HashSet<stp_tb_capacidad_contrato>();
        }
    
        public int id_capacidad { get; set; }
        public int fk_itinerario { get; set; }
        public System.DateTime fecha_capacidad { get; set; }
        public int capacidad { get; set; }
        public int disponible { get; set; }
        public string fic_actua { get; set; }
        public System.DateTime fec_actua { get; set; }
        public bool borrado_logico { get; set; }
    
        public virtual ICollection<stp_tb_capacidad_contrato> stp_tb_capacidad_contrato { get; set; }
    }
}
