//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SolicitudIntegral.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class ORDENES_SERVICIO
    {
        public int ID_ORDEN_SERVICIO { get; set; }
        public int PK_ID_INSTALACION { get; set; }
        public string CONSECUTIVO { get; set; }
        public System.DateTime FECHA { get; set; }
        public int AI_PERNOCTAS { get; set; }
        public int AI_DESAYUNOS { get; set; }
        public int AI_COMIDAS { get; set; }
        public int AI_CENAS { get; set; }
        public int AI_CENAS_MEDIA_NOCHE { get; set; }
        public int AII_PERNOCTAS { get; set; }
        public int AII_DESAYUNOS { get; set; }
        public int AII_COMIDAS { get; set; }
        public int AII_CENAS { get; set; }
        public int AII_CENAS_MEDIA_NOCHE { get; set; }
        public int AIII_PERNOCTAS { get; set; }
        public int AIII_DESAYUNOS { get; set; }
        public int AIII_COMIDAS { get; set; }
        public int AIII_CENAS { get; set; }
        public int AIII_CENAS_MEDIA_NOCHE { get; set; }
    
        public virtual PREH_INSTALACIONES_MARINAS PREH_INSTALACIONES_MARINAS { get; set; }
    }
}
