//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SolicitudIntegral.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class SOLHOSP_V_M_Guardias
    {
        public decimal PK_M_GUARDIA { get; set; }
        public string FK_CVE_PLATAFORMA { get; set; }
        public string ROL { get; set; }
        public string FOLIOSOLICITUD { get; set; }
        public string FICHAUSUARIOCAPTURA { get; set; }
        public string Guardia { get; set; }
        public string TIPO_EMPLEADO { get; set; }
        public int FK_C_M_INSTALACION { get; set; }
    }
}
