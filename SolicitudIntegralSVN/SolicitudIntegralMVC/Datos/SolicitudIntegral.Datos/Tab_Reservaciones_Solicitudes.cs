//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SolicitudIntegral.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tab_Reservaciones_Solicitudes
    {
        public Tab_Reservaciones_Solicitudes()
        {
            this.Tab_Reservaciones_Detalles = new HashSet<Tab_Reservaciones_Detalles>();
        }
    
        public int IdSolicitudReservacion { get; set; }
        public int FkIdInstalacion { get; set; }
        public string SubdivisionCentroGestor { get; set; }
        public string Contrato { get; set; }
        public string FolioEspecial { get; set; }
        public int EspacioSolicitado { get; set; }
        public System.DateTime FechaInicio { get; set; }
        public System.DateTime FechaFin { get; set; }
        public string Estado { get; set; }
    
        public virtual ICollection<Tab_Reservaciones_Detalles> Tab_Reservaciones_Detalles { get; set; }
    }
}
