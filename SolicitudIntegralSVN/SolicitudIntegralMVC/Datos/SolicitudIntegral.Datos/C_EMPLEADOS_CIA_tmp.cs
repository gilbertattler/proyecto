//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SolicitudIntegral.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class C_EMPLEADOS_CIA_tmp
    {
        public int ID_C_EMPLEADO_CIA { get; set; }
        public Nullable<int> ID_C_CONTRATO { get; set; }
        public string RFC_FM3 { get; set; }
        public string NOMBRES { get; set; }
        public string APELLIDOS { get; set; }
        public string CATEGORIA { get; set; }
        public string GENERO { get; set; }
        public string NACIONALIDAD { get; set; }
        public string CURP { get; set; }
        public string FOLIO_LIBRETA_MAR { get; set; }
        public Nullable<System.DateTime> VIGENCIA_LIBRETA_MAR { get; set; }
        public bool BORRADO_LOGICO { get; set; }
    }
}
