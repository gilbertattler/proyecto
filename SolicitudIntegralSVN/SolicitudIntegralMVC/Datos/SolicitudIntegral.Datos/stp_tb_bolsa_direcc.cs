//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SolicitudIntegral.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class stp_tb_bolsa_direcc
    {
        public int id_bolsa_direcc { get; set; }
        public decimal bolsa_presupuesto { get; set; }
        public int fk_cto_gestor { get; set; }
        public int fk_posicion_presupuestaria { get; set; }
        public int fk_fondo { get; set; }
        public int fk_elemento_pep { get; set; }
        public int fk_programa_presupuestal { get; set; }
        public string fic_actua { get; set; }
        public System.DateTime fec_actua { get; set; }
        public bool borrado_logico { get; set; }
    }
}
