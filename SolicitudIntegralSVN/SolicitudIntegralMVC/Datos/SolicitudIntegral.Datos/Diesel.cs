//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SolicitudIntegral.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class Diesel
    {
        public int Id_Diesel { get; set; }
        public string vc_SIGLAS { get; set; }
        public string vc_UBICACION { get; set; }
        public Nullable<double> f_DIAMETRO { get; set; }
        public Nullable<double> f_CAPACIDAD { get; set; }
        public Nullable<double> f_EXISTENCIA { get; set; }
        public Nullable<double> f_CONSUMO { get; set; }
        public string vc_TIPOCONEXION { get; set; }
        public string vc_VALVULA { get; set; }
        public string vc_OBSERVACION { get; set; }
        public Nullable<int> APot_Capacidad { get; set; }
        public Nullable<int> APot_DiamTomas { get; set; }
        public Nullable<int> Cemen_Capacidad { get; set; }
        public Nullable<int> Cemen_DiamTomas { get; set; }
        public Nullable<int> Bar_Capacidad { get; set; }
        public Nullable<int> Bar_DiamTomas { get; set; }
        public Nullable<int> Lodo_Capacidad { get; set; }
        public Nullable<int> Lodo_DiamTomas { get; set; }
        public Nullable<int> APer_Capacidad { get; set; }
        public Nullable<int> APer_DiamTomas { get; set; }
        public Nullable<int> ID_M_INSTALACION { get; set; }
    }
}
