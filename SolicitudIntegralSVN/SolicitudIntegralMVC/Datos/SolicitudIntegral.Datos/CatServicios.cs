//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SolicitudIntegral.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class CatServicios
    {
        public int IdServicio { get; set; }
        public int FkIdOrigen { get; set; }
        public int FkIdDestino { get; set; }
        public string CveOrigen { get; set; }
        public string CveDestino { get; set; }
        public string CveServicio { get; set; }
        public string DescripcionServicio { get; set; }
        public string Horario { get; set; }
        public string Tipo { get; set; }
        public string InicioHorarioAtencion { get; set; }
        public string FinHorarioAtencion { get; set; }
        public bool Curso { get; set; }
        public bool BorradoLogico { get; set; }
        public int DiasCandado { get; set; }
        public string Nodo { get; set; }
    }
}
