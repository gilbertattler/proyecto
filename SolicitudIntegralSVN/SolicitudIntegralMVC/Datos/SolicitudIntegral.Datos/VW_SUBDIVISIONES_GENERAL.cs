//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SolicitudIntegral.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class VW_SUBDIVISIONES_GENERAL
    {
        public int ID_C_REGION { get; set; }
        public int ID_C_ACTIVO { get; set; }
        public int ID_C_SUBDIVISION { get; set; }
        public int PK_ID_CTO_GESTOR { get; set; }
        public string CVE_REGION { get; set; }
        public string CVE_ACTIVO { get; set; }
        public string CVE_SUBDIVISION { get; set; }
        public string SUBDIVISION { get; set; }
        public string CVE_CTO_GESTOR { get; set; }
        public string SIGLAS_REGION { get; set; }
        public string NOMBRE_REGION { get; set; }
        public string SIGLAS_ACTIVO { get; set; }
        public string NOMBRE_ACTIVO { get; set; }
        public string DESCRIPCION_CTO_GESTOR { get; set; }
        public string DESCRIPCION_SUBDIVISION { get; set; }
    }
}
