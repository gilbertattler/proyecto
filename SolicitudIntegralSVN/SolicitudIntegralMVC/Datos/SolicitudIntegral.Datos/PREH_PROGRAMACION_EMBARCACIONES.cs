//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SolicitudIntegral.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class PREH_PROGRAMACION_EMBARCACIONES
    {
        public PREH_PROGRAMACION_EMBARCACIONES()
        {
            this.PREH_RUTAS_PROGRAMACION = new HashSet<PREH_RUTAS_PROGRAMACION>();
        }
    
        public int PK_ID_PROGRAMACION_EMBARCACION { get; set; }
        public int PK_ID_SEGUIMIENTO_CONTINGENCIA { get; set; }
        public int PK_ID_LANCHA { get; set; }
        public int FK_ID_PUERTO_ORIGEN { get; set; }
        public int FK_ID_PUERTO_DESTINO { get; set; }
        public int FK_C_M_INSTALACION_ORIGEN { get; set; }
        public int FK_C_M_INSTALACION_DESTINO { get; set; }
        public string FOLIO { get; set; }
        public System.DateTime FECHA_VIAJE { get; set; }
        public System.DateTime FECHA_ACTUALIZACION { get; set; }
        public string FICHA_ACTUALIZACION { get; set; }
        public string STATUS { get; set; }
        public int CAPACIDAD_CABINAS { get; set; }
        public int CAPACIDAD_CUBIERTA { get; set; }
    
        public virtual PREH_INSTALACIONES_MARINAS PREH_INSTALACIONES_MARINAS { get; set; }
        public virtual PREH_INSTALACIONES_MARINAS PREH_INSTALACIONES_MARINAS1 { get; set; }
        public virtual PREH_LANCHAS PREH_LANCHAS { get; set; }
        public virtual ICollection<PREH_RUTAS_PROGRAMACION> PREH_RUTAS_PROGRAMACION { get; set; }
        public virtual PREH_SEGUIMIENTO_CONTINGENCIAS PREH_SEGUIMIENTO_CONTINGENCIAS { get; set; }
    }
}
