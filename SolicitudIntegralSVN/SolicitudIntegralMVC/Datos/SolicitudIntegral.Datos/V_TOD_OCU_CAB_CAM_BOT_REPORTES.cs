//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SolicitudIntegral.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class V_TOD_OCU_CAB_CAM_BOT_REPORTES
    {
        public int PK_ID_OCUPANTE { get; set; }
        public string NOM_CIA { get; set; }
        public string CVE_CIA { get; set; }
        public string TIP_EMPLEADO { get; set; }
        public int FK_ID_C_M_INSTALACION { get; set; }
        public string CVE_PLATAFORMA { get; set; }
        public string FOLIO_SOLICITUD { get; set; }
        public string FOLIO_LOGISTICA { get; set; }
        public string NOM_USUARIO { get; set; }
        public string FIC_RFC_USUARIO { get; set; }
        public System.DateTime FEC_INI { get; set; }
        public System.DateTime FEC_TER { get; set; }
        public string TIP_TRANPORTE { get; set; }
        public string CVE_CENTRO { get; set; }
        public string CVE_DEPTO { get; set; }
        public string NOM_CATEGORIA { get; set; }
        public bool STATUS_ABORDO { get; set; }
        public string CLASIF_TRABAJADOR { get; set; }
        public string TIPO_PERNOCTA { get; set; }
        public bool BORRADO_LOGICO { get; set; }
        public bool ESTIMA { get; set; }
        public string TURNO { get; set; }
        public string SEXO { get; set; }
        public Nullable<int> PK_ID_CAMA { get; set; }
        public string CVE_CAMA { get; set; }
        public Nullable<short> NUM_OCUPANTES { get; set; }
        public string ESTADO { get; set; }
        public Nullable<int> PK_ID_CABINA { get; set; }
        public string CVE_CABINA { get; set; }
        public string CVE_PLATAFORMA_CABINA { get; set; }
        public string TIP_CABINA { get; set; }
        public Nullable<int> NIV_CABINA { get; set; }
        public Nullable<int> PK_ID_BOTE_OCUPANTE { get; set; }
        public string CVE_BOTE_OCUPANTE { get; set; }
        public string UBICA_BOTE_OCUPANTE { get; set; }
        public string TIPO_BOTE_OCUPANTE { get; set; }
        public string FASE_CONTINGENCIA { get; set; }
        public string ESTADO_CABINA { get; set; }
        public string ROL_OCUPANTE { get; set; }
        public Nullable<int> PK_ID_PUNTOREUNION_OCUPANTE { get; set; }
        public string CVE_PUNTOREUNION_OCUPANTE { get; set; }
        public string UBICACION_PUNTOREUNION_OCUPANTE { get; set; }
        public string ASIGNADO_CAMA { get; set; }
        public Nullable<int> PK_ID_BOTE_CAMA { get; set; }
        public string CVE_BOTE_CAMA { get; set; }
        public string UBICA_BOTE_CAMA { get; set; }
        public string TIPO_BOTE_CAMA { get; set; }
        public Nullable<int> PK_PUNTOREUNION_BOTE_CAMA { get; set; }
        public string CVE_PUNTOREUNION_BOTE_CAMA { get; set; }
        public string UBICACION_PUNTOREUNION_BOTE_CAMA { get; set; }
        public int EMPLEADO_PEMEX { get; set; }
        public int EMPLEADO_COMPAÑIA { get; set; }
        public int EMPLEADO_COMODATARIO { get; set; }
        public int PEMEX_FASE_PARCIAL { get; set; }
        public int PEMEX_FASE_TOTAL { get; set; }
        public int PEMEX_FASE_TRIPULACION { get; set; }
        public int COMPANIA_FASE_PARCIAL { get; set; }
        public int COMPANIA_FASE_TOTAL { get; set; }
        public int COMPANIA_FASE_TRIPULACION { get; set; }
        public int COMODATARIO_FASE_PARCIAL { get; set; }
        public int COMODATARIO_FASE_TOTAL { get; set; }
        public int COMODATARIO_FASE_TRIPULACION { get; set; }
        public int FASE_PARCIAL { get; set; }
        public int FASE_TOTAL { get; set; }
        public int FASE_TRIPULACION { get; set; }
        public int CLASIF_HUESPED { get; set; }
        public int CLASIF_VISITA { get; set; }
        public int CLASIF_FORANEO { get; set; }
        public int PEMEX_LANCHA { get; set; }
        public int PEMEX_HELICOPTERO { get; set; }
        public int PEMEX_PERMANECE_A_BORDO { get; set; }
        public int COMPANIA_LANCHA { get; set; }
        public int COMPANIA_HELICOPTERO { get; set; }
        public int COMPANIA_PERMANECE_A_BORDO { get; set; }
        public int COMODATARIO_LANCHA { get; set; }
        public int COMODATARIO_HELICOPTERO { get; set; }
        public int COMODATARIO_PERMANECE_A_BORDO { get; set; }
    }
}
