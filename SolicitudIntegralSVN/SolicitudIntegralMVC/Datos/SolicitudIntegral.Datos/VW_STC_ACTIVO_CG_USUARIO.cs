//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SolicitudIntegral.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class VW_STC_ACTIVO_CG_USUARIO
    {
        public string SUBDIVISION { get; set; }
        public Nullable<System.DateTime> FEC_FIN { get; set; }
        public Nullable<System.DateTime> FEC_INICIO { get; set; }
        public int ID_D_USUARIO { get; set; }
        public string FICHA { get; set; }
        public int ID_AREA_SERVICIO { get; set; }
    }
}
