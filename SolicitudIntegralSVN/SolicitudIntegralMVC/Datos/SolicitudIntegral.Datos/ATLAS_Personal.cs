//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SolicitudIntegral.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class ATLAS_Personal
    {
        public int i_IDPersonal { get; set; }
        public int i_IDCaracteristica { get; set; }
        public int i_Cantidad { get; set; }
        public int i_IDPuesto { get; set; }
        public string vc_Horario { get; set; }
        public string vc_Observacion { get; set; }
    }
}
