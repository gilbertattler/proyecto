//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SolicitudIntegral.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class PREH_AUDITORIA_DIARIA
    {
        public int PK_ID_AUDITORIA_DIARIA { get; set; }
        public int PK_ID_INSTALACION { get; set; }
        public int FK_C_M_INSTALACION { get; set; }
        public System.DateTime FECHA_AUDITORIA { get; set; }
        public Nullable<System.DateTime> FECHA_ULTIMO_CORTE { get; set; }
        public Nullable<int> TOTAL_ETAPA_PARCIAL { get; set; }
        public Nullable<int> TOTAL_ETAPA_TOTAL { get; set; }
        public Nullable<int> TOTAL_PERSONAS { get; set; }
        public Nullable<int> PARCIAL_MARITIMO_PEMEX { get; set; }
        public Nullable<int> PARCIAL_MARITIMO_COMPANIA { get; set; }
        public Nullable<int> PARCIAL_AEREO_PEMEX { get; set; }
        public Nullable<int> PARCIAL_AEREO_COMPANIA { get; set; }
        public Nullable<int> TOTAL_AEREO_PEMEX { get; set; }
        public Nullable<int> TOTAL_AEREO_COMPANIA { get; set; }
        public Nullable<int> TOTAL_COMPANIA_EMBARCACIONES { get; set; }
        public Nullable<int> TOTAL_PEMEX_EMBARCACIONES { get; set; }
        public Nullable<int> TOTAL_TRIPULACION_EMBARCACIONES { get; set; }
        public string TCPARCIAL_PEP { get; set; }
        public string TCPARCIAL_CIA { get; set; }
        public string TCTRIPULACION { get; set; }
        public int ALTAS { get; set; }
        public int BAJAS { get; set; }
        public bool ACTUALIZO { get; set; }
        public string UBICACION { get; set; }
        public Nullable<int> INDICE_UBICACION_REPORTE_PREH_PLATAFORMAS { get; set; }
        public Nullable<int> INDICE_UBICACION_REPORTE_PREH_EMBARCACIONES { get; set; }
        public Nullable<int> INDICE_UBICACION_REPORTE_PREH_AUDITORIA { get; set; }
    }
}
