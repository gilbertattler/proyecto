//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SolicitudIntegral.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class POAInstalacionesHistorico
    {
        public POAInstalacionesHistorico()
        {
            this.POAReservacionDiariaInstalacionesHistorico = new HashSet<POAReservacionDiariaInstalacionesHistorico>();
        }
    
        public int IdPOAInstalacionHistorico { get; set; }
        public int FkIdInstalacion { get; set; }
        public string NombreInstalacion { get; set; }
        public int IdPOAGerenciaHistorico { get; set; }
    
        public virtual POAGerenciaHistorico POAGerenciaHistorico { get; set; }
        public virtual ICollection<POAReservacionDiariaInstalacionesHistorico> POAReservacionDiariaInstalacionesHistorico { get; set; }
    }
}
