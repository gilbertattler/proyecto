//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SolicitudIntegral.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class GESTION_GL_V_PREH_INSTALACIONES_MARINAS
    {
        public int PK_ID_INSTALACION { get; set; }
        public string NOMBRE { get; set; }
        public string NOMBRE_COMPLETO_INSTALACION { get; set; }
        public string REGION { get; set; }
        public string AREA { get; set; }
        public string DESEMBARCO { get; set; }
        public string UBICACION { get; set; }
        public string COMPANIA { get; set; }
        public Nullable<System.DateTime> FECHA_ULTIMA_ACTUALIZACION { get; set; }
        public string PUERTO_ABRIGO { get; set; }
        public string SISTEMA { get; set; }
        public Nullable<bool> BORRADO_LOGICO { get; set; }
        public string NUMERO_EQUIPO { get; set; }
        public int INDICE_UBICACION_REPORTE_PREH { get; set; }
        public string NOMBRE_ENCARGADO { get; set; }
        public string FICHA_RFC_ENCARGADO { get; set; }
        public string TELEFONO_ENCARGADO { get; set; }
        public string EMAIL_ENCARGADO { get; set; }
        public string ETA { get; set; }
        public Nullable<int> INDICE_UBICACION_REPORTE_PREH_PLATAFORMAS { get; set; }
        public Nullable<int> INDICE_UBICACION_REPORTE_PREH_EMBARCACIONES { get; set; }
        public Nullable<int> INDICE_UBICACION_REPORTE_PREH_AUDITORIA { get; set; }
        public string NOMBRE_CLASIFICACION_PROCESO { get; set; }
        public string NOMBRE_CLASIFICACION_SERVICIO { get; set; }
        public string UNIDAD_OPERATIVA_PREH { get; set; }
        public string PREHCLASIFICACION { get; set; }
        public string PREHTIPO { get; set; }
        public string PREHREGION { get; set; }
        public string PREHOTRO1 { get; set; }
        public string PREHOTRO2 { get; set; }
    }
}
