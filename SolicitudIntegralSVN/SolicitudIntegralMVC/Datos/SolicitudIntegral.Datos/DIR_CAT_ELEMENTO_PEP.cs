//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SolicitudIntegral.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class DIR_CAT_ELEMENTO_PEP
    {
        public DIR_CAT_ELEMENTO_PEP()
        {
            this.DIR_RELACION_PROGRAMA_ELEMENTO = new HashSet<DIR_RELACION_PROGRAMA_ELEMENTO>();
        }
    
        public int PK_ID_ELEMENTO_PEP { get; set; }
        public string CVE_ELEMENTO_PEP { get; set; }
        public string DESCRIPCION_ELEMENTO_PEP { get; set; }
        public bool BORRADO_LOGICO { get; set; }
    
        public virtual ICollection<DIR_RELACION_PROGRAMA_ELEMENTO> DIR_RELACION_PROGRAMA_ELEMENTO { get; set; }
    }
}
