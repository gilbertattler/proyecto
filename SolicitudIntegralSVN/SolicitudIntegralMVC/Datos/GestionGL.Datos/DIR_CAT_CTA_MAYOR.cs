//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GestionGL.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class DIR_CAT_CTA_MAYOR
    {
        public DIR_CAT_CTA_MAYOR()
        {
            this.DIR_REL_POSICION_AREASERVICIO = new HashSet<DIR_REL_POSICION_AREASERVICIO>();
        }
    
        public int PK_ID_CTA_MAYOR { get; set; }
        public string CVE_CTA_MAYOR { get; set; }
        public string DESCRIPCION_CTA_MAYOR { get; set; }
        public bool BORRADO_LOGICO { get; set; }
    
        public virtual ICollection<DIR_REL_POSICION_AREASERVICIO> DIR_REL_POSICION_AREASERVICIO { get; set; }
    }
}
