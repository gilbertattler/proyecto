//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GestionGL.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class VW_DIR_USUARIOS
    {
        public int ID_D_USUARIO { get; set; }
        public int ID_M_USUARIO { get; set; }
        public int ID_AREA_SERVICIO { get; set; }
        public Nullable<int> ID_C_CONTRATO { get; set; }
        public Nullable<int> PK_ID_CTO_GESTOR { get; set; }
        public Nullable<int> ID_C_SUBDIVISION { get; set; }
        public Nullable<int> ID_C_ACTIVO { get; set; }
        public Nullable<int> ID_C_REGION { get; set; }
        public Nullable<System.DateTime> FEC_INICIO { get; set; }
        public Nullable<System.DateTime> FEC_FIN { get; set; }
        public string NODO { get; set; }
        public string CVE_BAJAL { get; set; }
        public Nullable<System.DateTime> FEC_ACTUA { get; set; }
        public string FIC_ACTUA { get; set; }
        public string CVE_AREA_SERVICIO { get; set; }
        public string DESCRIPCION_CORTA_AREA_SERVICIO { get; set; }
        public string DESCRIPCION_LARGA_AREA_SERVICIO { get; set; }
        public Nullable<int> ID_C_SUBDIVISION_CONTRATO_CENTRO_GESTOR { get; set; }
        public string CVE_SUBDIVISION { get; set; }
        public string SUBDIVISION { get; set; }
        public string CVE_CTO_GESTOR { get; set; }
        public string DESCRIPCION_CTO_GESTOR { get; set; }
        public string CONTRATO { get; set; }
    }
}
