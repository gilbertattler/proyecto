//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GestionGL.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class COSTEO_VW_EMBARCACION_CONTRATO
    {
        public int Id_Embar { get; set; }
        public int Fk_Id_Embar { get; set; }
        public string Cve_Embarcacion { get; set; }
        public string Nom_Embarcacion { get; set; }
        public string Contrato { get; set; }
    }
}
