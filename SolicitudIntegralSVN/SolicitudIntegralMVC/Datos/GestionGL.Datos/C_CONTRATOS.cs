//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GestionGL.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class C_CONTRATOS
    {
        public C_CONTRATOS()
        {
            this.C_EMPLEADOS_CIA = new HashSet<C_EMPLEADOS_CIA>();
            this.C_EMPLEADOS_PEMEX = new HashSet<C_EMPLEADOS_PEMEX>();
            this.C_SUBDIVISIONES_CONTRATOS_CENTROS_GESTORES = new HashSet<C_SUBDIVISIONES_CONTRATOS_CENTROS_GESTORES>();
        }
    
        public int ID_C_CONTRATO { get; set; }
        public Nullable<int> ID_C_ACREEDOR { get; set; }
        public string CONTRATO { get; set; }
        public string MONEDA { get; set; }
        public string REGION { get; set; }
        public Nullable<System.DateTime> FECHA_INICIO { get; set; }
        public Nullable<System.DateTime> FECHA_FINAL { get; set; }
        public Nullable<System.DateTime> FEC_ACTUA { get; set; }
        public string FIC_ACTUA { get; set; }
        public string DESCRIPCION { get; set; }
        public bool BORRADO_LOGICO { get; set; }
        public decimal PESOS { get; set; }
        public decimal DOLARES { get; set; }
    
        public virtual C_ACREEDORES C_ACREEDORES { get; set; }
        public virtual ICollection<C_EMPLEADOS_CIA> C_EMPLEADOS_CIA { get; set; }
        public virtual ICollection<C_EMPLEADOS_PEMEX> C_EMPLEADOS_PEMEX { get; set; }
        public virtual ICollection<C_SUBDIVISIONES_CONTRATOS_CENTROS_GESTORES> C_SUBDIVISIONES_CONTRATOS_CENTROS_GESTORES { get; set; }
    }
}
