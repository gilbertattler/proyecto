//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GestionGL.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class DIR_V_DIRECIONAMIENTOS_RELACIONADOS
    {
        public int PK_ID_DIRECCIONAMIENTO_RELACIONADO { get; set; }
        public string ORIGEN_RELACION { get; set; }
        public string ESTADO { get; set; }
        public int PK_ID_PREDIRECCIONAMIENTO { get; set; }
        public int PK_ID_CTO_GESTOR { get; set; }
        public string CVE_CTO_GESTOR { get; set; }
        public string DESCRIPCION_CTO_GESTOR { get; set; }
        public bool BORRADO_LOGICO_GESTOR { get; set; }
        public int PK_ID_FONDO { get; set; }
        public string CVE_FONDO { get; set; }
        public string DESCRIPCION_FONDO { get; set; }
        public bool BORRADO_LOGICO_FONDO { get; set; }
        public int PK_ID_ELEMENTO_PEP { get; set; }
        public string CVE_ELEMENTO_PEP { get; set; }
        public string DESCRIPCION_ELEMENTO_PEP { get; set; }
        public bool BORRADO_LOGICO_PEP { get; set; }
        public int PK_ID_PROGRAMA_PRESUPUESTAL { get; set; }
        public string CVE_PROGRAMA_PRESUPUESTAL { get; set; }
        public string DESCRIPCION_PROGRAMA_PRESUPUESTAL { get; set; }
        public string ORIGEN_PROGRAMA { get; set; }
        public bool BORRADO_LOGICO_PROGRAMA { get; set; }
        public bool BORRADO_LOGICO_PREDIRECCIONAMIENTO { get; set; }
        public string GRUPO { get; set; }
        public int PK_ID_POSICION_AREASERVICIO { get; set; }
        public int PK_ID_AREAS_SERVICIO { get; set; }
        public string CVE_AREA_SERVICIO { get; set; }
        public string DESCRIPCION_AREA_SERVICIO { get; set; }
        public bool BORRADO_LOGICO_AREA_SERVICIO { get; set; }
        public bool BORRADO_LOGICO_RELACION { get; set; }
        public int PK_ID_POSICION_PRESUPUESTARIA { get; set; }
        public string CVE_POSICION_PRESUPUESTARIA { get; set; }
        public string DESCRIPCION_PRESUPUESTARIA { get; set; }
        public bool BORRADO_LOGICO_POSICION { get; set; }
        public int PK_ID_CTA_MAYOR { get; set; }
        public string CVE_CTA_MAYOR { get; set; }
        public string DESCRIPCION_CTA_MAYOR { get; set; }
        public bool BORRADO_LOGICO_CUENTA { get; set; }
        public int PK_ID_RELACION_PROGRAMA_ELEMENTO { get; set; }
    }
}
