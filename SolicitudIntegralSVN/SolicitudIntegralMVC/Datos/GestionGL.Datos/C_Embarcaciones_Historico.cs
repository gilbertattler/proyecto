//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GestionGL.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class C_Embarcaciones_Historico
    {
        public int Id_Embar_hist { get; set; }
        public int Id_Embar { get; set; }
        public int Fk_Id_Tipo_Servicio { get; set; }
        public int FK_Id_Tipo_Embar { get; set; }
        public int Fk_Id_C_Sector { get; set; }
        public int Fk_Id_C_Region { get; set; }
        public string Cve_Embarcacion { get; set; }
        public string Nom_Embarcacion { get; set; }
        public int Capacidad_Pax { get; set; }
        public int Capacidad_Tarima { get; set; }
        public decimal Capacidad_Barita { get; set; }
        public decimal Capacidad_Cemento { get; set; }
        public decimal Capacidad_Agua_Pot { get; set; }
        public decimal Capacidad_Agua_Per { get; set; }
        public decimal Capacidad_Lodo { get; set; }
        public decimal Capacidad_Aceite { get; set; }
        public decimal Capacidad_Cubierta { get; set; }
        public decimal Min_Diesel { get; set; }
        public decimal Max_Diesel { get; set; }
        public decimal Area_Cubierta { get; set; }
        public string Estado_Embaracion { get; set; }
        public string Bandera { get; set; }
        public int Anio_Construccion { get; set; }
        public string Clasificacion { get; set; }
        public decimal Eslora { get; set; }
        public decimal Calado { get; set; }
        public decimal Velocidad__Max { get; set; }
        public decimal Cap_Carga { get; set; }
        public decimal Potencia { get; set; }
        public decimal BollarDPull { get; set; }
        public decimal BBEO_Hora { get; set; }
        public decimal Consumo_Diario { get; set; }
        public string Monitores { get; set; }
        public decimal Cable_trabajo { get; set; }
        public bool Borrado_logico { get; set; }
        public System.DateTime Fecha_Registro { get; set; }
        public System.DateTime Fecha_Actua { get; set; }
        public string Fic_captura { get; set; }
        public Nullable<decimal> Costo_Diario { get; set; }
        public Nullable<int> Fk_Cia { get; set; }
        public Nullable<decimal> TRB { get; set; }
        public string Tipo_Moneda { get; set; }
        public int Pax_cabina { get; set; }
        public int Pax_cubierta { get; set; }
        public string mmsi { get; set; }
        public bool Contrato_pemex { get; set; }
        public int Canal_radio_1 { get; set; }
        public int Canal_radio_2 { get; set; }
    }
}
