//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GestionGL.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class DIR_VW_INSTALACIONES_AREA_SERVICIO
    {
        public Nullable<int> ID_C_M_INSTALACION_RELACION { get; set; }
        public Nullable<int> FK_ID_C_M_INSTALACION_UBICACION_ACTUAL { get; set; }
        public int ID_C_M_INSTALACION { get; set; }
        public string NOMBRE_INSTALACION { get; set; }
        public string AREA_SERVICIO_AH { get; set; }
        public string AREA_SERVICIO_STM { get; set; }
        public string AREA_SERVICIO_STPVM { get; set; }
        public string AREA_SERVICIO_STA { get; set; }
        public string AREA_SERVICIO_SE { get; set; }
        public string AREA_SERVICIO_STMTR { get; set; }
        public string AREA_SERVICIO_COM { get; set; }
        public string AREA_SERVICIO_CHCKL { get; set; }
        public string AREA_SERVICIO_STT { get; set; }
    }
}
