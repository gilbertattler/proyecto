﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace STPIntegral.Negocios
{
    public class Utilerias
    {
        public static string ObtenerTipoTrabajador(string fichaRFC)
        {
            string regexRFC = "^(?<RFCC>[a-zA-Z]{4})\\W*(?<RFCN>\\d{4,6})(?<RFC>\\w{0,4})$";
            string regexFicha = "^(?<N>[0]*)(?<FICHA>\\d{5,8})$";
            string tipoTrabajador = "";

            System.Text.RegularExpressions.RegexOptions options = ((System.Text.RegularExpressions.RegexOptions.IgnorePatternWhitespace | System.Text.RegularExpressions.RegexOptions.Multiline) | System.Text.RegularExpressions.RegexOptions.IgnoreCase);


            if (System.Text.RegularExpressions.Regex.IsMatch(fichaRFC, regexFicha, options))
            {
                tipoTrabajador = "PEMEX";
            }
            else
            {
                if (System.Text.RegularExpressions.Regex.IsMatch(fichaRFC, regexRFC, options))
                {
                    tipoTrabajador = "COMPAÑIA";
                }
                else
                {
                    //No coincido con la validacion del RFC sin embargo se va a considerar como de compañia
                    tipoTrabajador = "COMPAÑIA";
                }
            }
            return tipoTrabajador;
        }

        public static string FormatearFichaRfc(string ficha)
        {
            ficha = ficha.Trim();
            string resultado = "";
            string tipo_trabajador = ObtenerTipoTrabajador(ficha);

            if (tipo_trabajador == "PEMEX")
                resultado = String.Format("{0:00000000}", Convert.ToInt32(ficha));
            else
            {
                if (tipo_trabajador == "COMPAÑIA")
                {
                    resultado = FormatearRFCCompania(ficha);
                }
            }
            return resultado;
        }

        public static string FormatearRFCCompania(string RFC)
        {
            return RFC.PadRight(14, '0');
        }

        public static bool ValidarRFC(string rfc)
        {
            bool foundMatch = false;
            try
            {
                foundMatch = Regex.IsMatch(rfc, @"\A[^a-zA-Z0-9]\Z", RegexOptions.Singleline);
            }
            catch (ArgumentException ex)
            {
                // Syntax error in the regular expression
            }
            return !foundMatch;
        }


        public static bool ValidarFicha(string ficha)
        {
            bool foundMatch = false;
            try
            {
                foundMatch = Regex.IsMatch(ficha, @"\A[^0-9]\Z", RegexOptions.Singleline);
            }
            catch (ArgumentException ex)
            {
                // Syntax error in the regular expression
            }
            return !foundMatch;
        }

    }
}
