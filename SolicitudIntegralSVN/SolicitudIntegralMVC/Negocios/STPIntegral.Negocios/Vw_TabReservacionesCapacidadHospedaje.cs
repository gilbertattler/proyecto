﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace STPIntegral.Negocios
{
    public class Vw_TabReservacionesCapacidadHospedaje
    {
        public static List<STPIntegral.Datos.VW_TabReservacionesCapacidadHospedaje_NOLOCK> ObtenerDetallesCapacidad(int fkIdInstalacion, DateTime fechaInicio, DateTime fechaFin, string subdivision, string contrato, string folio_especial)
        {
            List<STPIntegral.Datos.VW_TabReservacionesCapacidadHospedaje_NOLOCK> resultado = null;

            using (STPIntegral.Datos.STPIntegralModel stpIntegralModel = new Datos.STPIntegralModel())
            {
                resultado = stpIntegralModel.VW_TabReservacionesCapacidadHospedaje_NOLOCK.Where(x => x.FkIdInstalacion == fkIdInstalacion && (x.FechaCapacidad >= fechaInicio && x.FechaCapacidad <= fechaFin) 
                    && 
                        (
                            x.TipoReservacion == "FONDO COMUN" 
                            || (x.TipoReservacion == "SUBDIVISION" && x.SUBDIVISION == subdivision)
                            || (x.TipoReservacion == "CONTRATO" && x.CONTRATO == contrato)
                            || (x.TipoReservacion == "FOLIO ESPECIAL" && x.FolioEspecial == folio_especial)
                        )
                    ).ToList();
            }

            return resultado;
        }
    }
}
