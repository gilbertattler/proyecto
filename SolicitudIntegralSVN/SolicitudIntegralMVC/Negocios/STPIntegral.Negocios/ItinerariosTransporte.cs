﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace STPIntegral.Negocios
{
    public class ItinerariosTransporte
    {

        public static IEnumerable<STPIntegral.Datos.Vw_SoltIntItinerariosCapacidadDia_NOLOCK> ConsultarItinerarios(string tipo_solicitud_integral, string tipo_transporte, string tipo_servicio, DateTime fecha_servicio, string nodo_atencion, string folio_especial_ida, string folio_especial_regreso, string contrato, string subdivision, bool validar_fecha_limite_creacion)
        {
            List<STPIntegral.Datos.Vw_SoltIntItinerariosCapacidadDia_NOLOCK> respuesta = new List<Datos.Vw_SoltIntItinerariosCapacidadDia_NOLOCK>();
            List<STPIntegral.Datos.Vw_SoltIntItinerariosCapacidadDia_NOLOCK> itinerarios_disponibles = null;

            int id_area_servicio = (tipo_transporte == "AEREO" ? 2 : 3); //2 -> AEREO, 3 -> MARITIMO

            using (STPIntegral.Datos.STPIntegralModel context = new STPIntegral.Datos.STPIntegralModel())
            {
                DateTime fecha_capacidad_inicio = fecha_servicio.Date;
                DateTime fecha_capacidad_final = fecha_servicio.Date.AddDays(1);

                //TODO: Faltan aplicar validaciones para ver si es un solictador valido

                if (validar_fecha_limite_creacion)
                {
                    itinerarios_disponibles = (from x in context.Vw_SoltIntItinerariosCapacidadDia_NOLOCK
                                               where (x.FechaLimiteCreacionSolicitud > System.DateTime.Now && x.cve_nodo_atencion == nodo_atencion && x.fecha_capacidad >= fecha_capacidad_inicio && x.fecha_capacidad < fecha_capacidad_final) && x.fk_area_servicio == id_area_servicio && x.des_tipo_servicio == tipo_servicio
                                               select x).ToList();
                }
                else
                {
                    itinerarios_disponibles = (from x in context.Vw_SoltIntItinerariosCapacidadDia_NOLOCK
                                               where (x.fecha_capacidad >= fecha_capacidad_inicio && x.cve_nodo_atencion == nodo_atencion && x.fecha_capacidad < fecha_capacidad_final) && x.fk_area_servicio == id_area_servicio && x.des_tipo_servicio == tipo_servicio
                                               select x).ToList();
                }
            }

            //Obtener los itinerarios distintos
            List<int> ids_itinerarios_distintos = (from x in itinerarios_disponibles select x.id_itinerario).Distinct().ToList();

            STPIntegral.Datos.Vw_SoltIntItinerariosCapacidadDia_NOLOCK itinerario_disponible = null;

            foreach (int id_itinerario_busqueda in ids_itinerarios_distintos)
            {
                //Agregando el primer itinerario a la coleccion
                List<STPIntegral.Datos.Vw_SoltIntItinerariosCapacidadDia_NOLOCK> itinerarios_busqueda = itinerarios_disponibles.Where(x => x.id_itinerario == id_itinerario_busqueda).ToList();

                if (itinerarios_busqueda != null && itinerarios_busqueda.Count > 0)
                {
                    STPIntegral.Datos.Vw_SoltIntItinerariosCapacidadDia_NOLOCK primer_itinerario_coleccion = itinerarios_busqueda.FirstOrDefault();

                    itinerario_disponible = new Datos.Vw_SoltIntItinerariosCapacidadDia_NOLOCK()
                    {
                        capacidad_disponible = 0,
                        capacidad_total = 0,
                        CONTRATO = "",
                        cve_itinerario = primer_itinerario_coleccion.cve_itinerario,
                        cve_nodo_atencion = primer_itinerario_coleccion.cve_nodo_atencion,
                        cve_nodo_destino = primer_itinerario_coleccion.cve_nodo_destino,
                        cve_nodo_origen = primer_itinerario_coleccion.cve_nodo_origen,
                        cve_tipo_servicio = primer_itinerario_coleccion.cve_tipo_servicio,
                        des_itinerario = primer_itinerario_coleccion.des_itinerario,
                        des_nodo_atencion = primer_itinerario_coleccion.des_nodo_atencion,
                        des_nodo_destino = primer_itinerario_coleccion.des_nodo_destino,
                        des_nodo_origen = primer_itinerario_coleccion.des_nodo_origen,
                        des_tipo_servicio = primer_itinerario_coleccion.des_tipo_servicio,
                        dias_recepcion = primer_itinerario_coleccion.dias_recepcion,
                        fecha_capacidad = primer_itinerario_coleccion.fecha_capacidad,
                        FechaLimiteCreacionSolicitud = primer_itinerario_coleccion.FechaLimiteCreacionSolicitud,
                        fk_area_servicio = primer_itinerario_coleccion.fk_area_servicio,
                        fk_nodo_atencion = primer_itinerario_coleccion.fk_nodo_atencion,
                        fk_nodo_destino = primer_itinerario_coleccion.fk_nodo_destino,
                        fk_nodo_origen = primer_itinerario_coleccion.fk_nodo_origen,
                        folio_especial = primer_itinerario_coleccion.folio_especial,
                        horario_itinerario = primer_itinerario_coleccion.horario_itinerario,
                        horario_recepcion = primer_itinerario_coleccion.horario_recepcion,
                        horas_candado = primer_itinerario_coleccion.horas_candado,
                        ID_C_CONTRATO = null,
                        ID_C_SUBDIVISION = null,
                        id_capacidad = primer_itinerario_coleccion.id_capacidad,
                        id_itinerario = primer_itinerario_coleccion.id_itinerario,
                        id_tipo_servicio = primer_itinerario_coleccion.id_tipo_servicio,
                        observaciones_dist = "",
                        SIGLAS_ACTIVO = "",
                        SIGLAS_REGION = "",
                        SUBDIVISION = ""
                    };


                    //Buscando la capacidad disponible de los itinerarios
                    foreach (STPIntegral.Datos.Vw_SoltIntItinerariosCapacidadDia_NOLOCK itinerario_actual in itinerarios_busqueda)
                    {

                        if (itinerario_actual.ID_C_CONTRATO.HasValue == false && itinerario_actual.ID_C_SUBDIVISION.HasValue == false && String.IsNullOrEmpty(itinerario_actual.folio_especial))
                        {
                            itinerario_disponible.capacidad_total += itinerario_actual.capacidad_total;
                            itinerario_disponible.capacidad_disponible += itinerario_actual.capacidad_disponible;
                            continue;
                        }
                        
                        if (!String.IsNullOrEmpty(contrato))
                        {
                            if (itinerario_actual.CONTRATO == contrato)
                            {
                                itinerario_disponible.capacidad_total += itinerario_actual.capacidad_total;
                                itinerario_disponible.capacidad_disponible += itinerario_actual.capacidad_disponible;
                                continue;
                            }
                        }

                        if (!String.IsNullOrEmpty(subdivision))
                        {
                            if (itinerario_actual.SUBDIVISION == subdivision)
                            {
                                itinerario_disponible.capacidad_total += itinerario_actual.capacidad_total;
                                itinerario_disponible.capacidad_disponible += itinerario_actual.capacidad_disponible;
                                continue;
                            }
                        }

                        if (!String.IsNullOrEmpty(folio_especial_ida))
                        {
                            if (itinerario_actual.folio_especial == folio_especial_ida)
                            {
                                itinerario_disponible.capacidad_total += itinerario_actual.capacidad_total;
                                itinerario_disponible.capacidad_disponible += itinerario_actual.capacidad_disponible;
                                continue;
                            }
                        }

                        if (!String.IsNullOrEmpty(folio_especial_regreso))
                        {
                            if (itinerario_actual.folio_especial == folio_especial_regreso)
                            {
                                itinerario_disponible.capacidad_total += itinerario_actual.capacidad_total;
                                itinerario_disponible.capacidad_disponible += itinerario_actual.capacidad_disponible;
                                continue;
                            }
                        }
                    }

                    respuesta.Add(itinerario_disponible);
                }
            }
            
            return respuesta;
        }

        public static IEnumerable<STPIntegral.Datos.Vw_SoltIntItinerariosCapacidadDia_NOLOCK> ConsultaCapacidadDetallesItinerarioPorDia(string tipo_transporte, string nodo_atencion, int id_itinerario, DateTime fecha_servicio, string subdivision_autorizador, string contrato, string folio_especial, out int capacidad_fondo_comun)
        {
            List<STPIntegral.Datos.Vw_SoltIntItinerariosCapacidadDia_NOLOCK> resultado = new List<Datos.Vw_SoltIntItinerariosCapacidadDia_NOLOCK>();
            IEnumerable<STPIntegral.Datos.Vw_SoltIntItinerariosCapacidadDia_NOLOCK> busqueda_itinerarios_detalles = null;

            using (STPIntegral.Datos.STPIntegralModel context = new STPIntegral.Datos.STPIntegralModel())
            {
                int id_area_servicio = tipo_transporte == "AEREO" ? 2 : 3; //2: STA, 3: STPVM

                DateTime fecha_capacidad_inicio = fecha_servicio.Date;
                DateTime fecha_capacidad_final = fecha_servicio.Date.AddDays(1);

                busqueda_itinerarios_detalles = (from x in context.Vw_SoltIntItinerariosCapacidadDia_NOLOCK
                                                 where x.id_itinerario == id_itinerario && (x.fecha_capacidad >= fecha_capacidad_inicio && x.fecha_capacidad < fecha_capacidad_final) && x.cve_nodo_atencion == nodo_atencion && x.fk_area_servicio == id_area_servicio
                                                 select x).ToList();

            }

            if (busqueda_itinerarios_detalles.Count() > 0)
            {
                STPIntegral.Datos.Vw_SoltIntItinerariosCapacidadDia_NOLOCK fondo_comun = (from x in busqueda_itinerarios_detalles where x.ID_C_CONTRATO.HasValue == false && x.ID_C_SUBDIVISION.HasValue == false && String.IsNullOrEmpty(x.folio_especial) == true select x).FirstOrDefault();

                if(fondo_comun != null)
                {
                    capacidad_fondo_comun = fondo_comun.capacidad_disponible.Value;
                }
                else
                {
                    capacidad_fondo_comun = 0;
                }
            }
            else
            {
                capacidad_fondo_comun = 0;
            }


            bool itinerario_adecuado = false;

            //Validando si los detalles del itinerario coinciden con los criterios de consulta
            foreach (STPIntegral.Datos.Vw_SoltIntItinerariosCapacidadDia_NOLOCK itinerario_capacidad_detalle in busqueda_itinerarios_detalles)
            {
                itinerario_adecuado = false;

                if (!String.IsNullOrEmpty(contrato) && itinerario_adecuado == false)
                {
                    if (itinerario_capacidad_detalle.CONTRATO == contrato)
                    {
                        itinerario_adecuado = true;
                    }
                }

                if (!String.IsNullOrEmpty(subdivision_autorizador) && itinerario_adecuado == false)
                {
                    if (itinerario_capacidad_detalle.SUBDIVISION == subdivision_autorizador)
                    {
                        itinerario_adecuado = true;
                    }
                }

                if (!String.IsNullOrEmpty(folio_especial) && itinerario_adecuado == false)
                {
                    if (itinerario_capacidad_detalle.folio_especial == folio_especial)
                    {
                        itinerario_adecuado = true;
                    }
                }

                if (itinerario_adecuado == true)
                {
                    resultado.Add(itinerario_capacidad_detalle);
                }
            }

            return resultado.AsEnumerable();
        }

        public static int ObtenerCapacidadTransportePorItem(int id_itinerario, DateTime fecha_subida, string folio_especial_transporte, string no_contrato, string subdivision)
        {
            int resultado = 0;

            using (STPIntegral.Datos.STPIntegralModel stpIntegralContext = new Datos.STPIntegralModel())
            {
                resultado = ObtenerCapacidadTransportePorItem(id_itinerario, fecha_subida, folio_especial_transporte, no_contrato, subdivision, stpIntegralContext);
            }

            return resultado;
        }

        private static int ObtenerCapacidadTransportePorItem(int? id_itinerario, DateTime fecha_subida, string folio_especial_transporte, string contrato_gestion_gl, string subdivision_gestion_gl, STPIntegral.Datos.STPIntegralModel context_stpIntegral)
        {
            int resultado = 0;
            //bool hay_capacidad_transporte = false;

            DateTime fecha_inicio_capacidad = fecha_subida.Date;
            DateTime fecha_fin_capacidad = fecha_subida.Date.AddDays(1);

            //Si hubiera un numero de contrato entonces primero se valida la capacidad en stp_tb_capacidad_contrato
            var listado_capacidades_item = context_stpIntegral.Vw_SoltIntItinerariosCapacidadDia_NOLOCK.Where(x => x.id_itinerario == id_itinerario.Value && (x.fecha_capacidad >= fecha_inicio_capacidad && x.fecha_capacidad < fecha_fin_capacidad)).ToList();

            //DE AQUI EN ADELANTE SE APLICAN LAS MISMAS VALIDACIONES QUE EL OTRO MODULO, ESTO PUEDE QUEDAR EN UNA FUNCION PARA MAS FACILIDAD.
            if (listado_capacidades_item.Count() > 0)
            {
                //Primero se valida que haya disponibilidad en el folio_especial
                if (folio_especial_transporte != "")
                {
                    var listado_capacidades_por_folio_especial = listado_capacidades_item.Where(x => x.folio_especial == folio_especial_transporte).ToList();

                    if (listado_capacidades_por_folio_especial.Count() > 0)
                    {
                        resultado += listado_capacidades_por_folio_especial.Sum(x => x.capacidad_disponible.Value);
                    }
                }

                //Luego se valida si hay disponibilidad en capacidad contato en caso de que proporcionen un contrato.
                //if (contrato_gestion_gl != null && String.IsNullOrEmpty(contrato_gestion_gl.CONTRATO.Trim()) && hay_capacidad_transporte == false)
                if (!String.IsNullOrEmpty(contrato_gestion_gl))
                {
                    //Se consulta a la tabla stp_tb_capacida_detalle
                    var listado_capacidades_por_contrato_item = listado_capacidades_item.Where(x => x.CONTRATO == contrato_gestion_gl).ToList();

                    if (listado_capacidades_por_contrato_item.Count() > 0)
                    {
                        resultado += listado_capacidades_por_contrato_item.Sum(x => x.capacidad_disponible.Value);
                    }
                }

                //Luego se valida si hay disponibilidad en la subdivision en caso de que proporcionen una.
                if (!String.IsNullOrEmpty(subdivision_gestion_gl))
                {
                    //Se consulta a la tabla stp_tb_capacida_detalle
                    var listado_capacidades_por_subdivision_item = listado_capacidades_item.Where(x => x.SUBDIVISION == subdivision_gestion_gl).ToList();

                    if (listado_capacidades_por_subdivision_item.Count() > 0)
                    {
                        resultado += listado_capacidades_por_subdivision_item.Sum(x => x.capacidad_disponible.Value);
                    }
                }

                //Si no proporcionaron un folio especial u contrato o bien si no hubo disponibilidad en la tabla de capacidad detalles entonces se valida la diponibilidad general
                var listado_capacidades_fondo_comun_item = listado_capacidades_item.Where(x => x.ID_C_CONTRATO.HasValue == false && x.ID_C_SUBDIVISION.HasValue == false && String.IsNullOrEmpty(x.folio_especial) == true).ToList();
                
                if(listado_capacidades_fondo_comun_item != null && listado_capacidades_fondo_comun_item.Count() > 0)
                {
                    resultado += listado_capacidades_fondo_comun_item.Sum(x => x.capacidad_disponible.Value);
                }
            }

            return resultado;
        }

        public static int ObtenerCapacidadFolioEspecialPorItem(int? id_itinerario, DateTime fecha_subida, string folio_especial_transporte)
        {
            int resultado = 0;
            //bool hay_capacidad_transporte = false;

            DateTime fecha_inicio_capacidad = fecha_subida.Date;
            DateTime fecha_fin_capacidad = fecha_subida.Date.AddDays(1);

            using (STPIntegral.Datos.STPIntegralModel context_stpIntegral = new STPIntegral.Datos.STPIntegralModel())
            {
                //Si hubiera un numero de contrato entonces primero se valida la capacidad en stp_tb_capacidad_contrato
                var listado_capacidades_item = context_stpIntegral.Vw_SoltIntItinerariosCapacidadDia_NOLOCK.Where(x => x.id_itinerario == id_itinerario.Value && (x.fecha_capacidad >= fecha_inicio_capacidad && x.fecha_capacidad < fecha_fin_capacidad)).ToList();

                //DE AQUI EN ADELANTE SE APLICAN LAS MISMAS VALIDACIONES QUE EL OTRO MODULO, ESTO PUEDE QUEDAR EN UNA FUNCION PARA MAS FACILIDAD.
                if (listado_capacidades_item.Count() > 0)
                {
                    //Primero se valida que haya disponibilidad en el folio_especial
                    if (folio_especial_transporte != "")
                    {
                        var listado_capacidades_por_folio_especial = listado_capacidades_item.Where(x => x.folio_especial == folio_especial_transporte).ToList();

                        if (listado_capacidades_por_folio_especial.Count() > 0)
                        {
                            resultado += listado_capacidades_por_folio_especial.Sum(x => x.capacidad_disponible.Value);
                        }
                    }
                }
            }
            return resultado;
        }

    }
}
