﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using STPIntegral.Negocios.Models;
using System.Web.Script.Serialization;


namespace STPIntegral.Negocios
{

    public enum RolesUsuarioSolicitudIntegral
    {
        USUARIO,
        VENTANILLA
    }
    
    public enum EstadosSolicitudIntegral
    {
        SOLICITADO,
        CONFIRMADO,
        CANCELADO
    }

    public enum EstadosDetallesSolicitudIntegral
    {
        SOLICITADO,
        CONFIRMADO,
        CANCELADO
    }

    public enum EstadosDetallesSolicitudIntegralAH
    {
        SOLICITADO,
        CONFIRMADO,
        CANCELADO,
        REGISTRADO_A_BORDO,
        BAJA_A_BORDO
    }

    public enum EstadosDetallesSolicitudIntegralSTP
    {
        SOLICITADO,
        ABORDO,
        CONFIRMADO,
        CANCELADO
    }

    public class SolicitudIntegralController
    {
        
        public SolicitudIntegralController()
        {

        }

        //Funciones Estaticas
        
        public static IEnumerable<STPIntegral.Datos.SI_Listado_Solicitudes_Result> ConsultaSolicitudesIntegrales(int page, int pageSize, string searchString, string ficha, DateTime? fecha, string nodo, out int total_registros, int id_permiso_ua)
        {
            List<STPIntegral.Datos.SI_Listado_Solicitudes_Result> respuesta = null;

            GestionGL.Datos.VW_PERMISOS_UA permiso_ua_usuario = null;

            using (GestionGL.Datos.GestionGLModel gestiongl_model = new GestionGL.Datos.GestionGLModel())
            {
                permiso_ua_usuario = gestiongl_model.VW_PERMISOS_UA.SingleOrDefault(x => x.ID_PERMISOS_UA == id_permiso_ua);
            }
            
            using (STPIntegral.Datos.STPIntegralModel context = new STPIntegral.Datos.STPIntegralModel())
            {
                //Faltan aplicar validaciones para ver si es un solictador valido
                context.Configuration.LazyLoadingEnabled = false;
                context.Configuration.ProxyCreationEnabled = false;

                total_registros = 0;

                switch (permiso_ua_usuario.ROL)
                {
                    case "VENTANILLA":
                    {
                      

                        System.Data.Objects.ObjectParameter p = new System.Data.Objects.ObjectParameter("totReg", typeof(int));
                        respuesta = context.SI_Listado_Solicitudes(searchString, ficha, fecha, nodo,pageSize,page, p,null).ToList();
                        total_registros =Convert.ToInt32(p.Value);
                        
                        //if (!String.IsNullOrEmpty(searchString))
                        //{
                            
                        //    //respuesta = respuesta.OrderByDescending(x => x.FechaCreacion).Skip(pageSize * (page - 1)).Take(pageSize).ToList();
                        //}
                        //else
                        //{
                        //    respuesta = context.VwTabSolicitudesIntegrales.Where(x => x.IdSolicitudIntegral > 0 && x.NodoAtencion.Contains(nodo)).OrderByDescending(x => x.FechaCreacion).Skip(pageSize * (page - 1)).Take(pageSize).ToList();
                        //    total_registros = context.VwTabSolicitudesIntegrales.Where(x => x.IdSolicitudIntegral > 0 && x.NodoAtencion.Contains(nodo)).Count();
                        //}
                        
                        break;
                    }
                    case "USUARIO":
                    {
                        
                        System.Data.Objects.ObjectParameter p = new System.Data.Objects.ObjectParameter("totReg", typeof(int));
                        respuesta = context.SI_Listado_Solicitudes(searchString, ficha, fecha, nodo, pageSize, page, p, permiso_ua_usuario.FICHA).ToList();
                        total_registros = Convert.ToInt32(p.Value);
                        //if (!String.IsNullOrEmpty(searchString))
                        //{
                        //    respuesta = context.VwTabSolicitudesIntegrales.Where(x => x.IdSolicitudIntegral > 0 && x.FichaCaptura == permiso_ua_usuario.FICHA && x.FolioSolicitudIntegral.Contains(searchString) && x.NodoAtencion.Contains(nodo)).OrderByDescending(x => x.FechaCreacion).Skip(pageSize * (page - 1)).Take(pageSize).ToList();
                        //    total_registros = context.VwTabSolicitudesIntegrales.Where(x => x.IdSolicitudIntegral > 0 && x.FichaCaptura == permiso_ua_usuario.FICHA && x.FolioSolicitudIntegral.Contains(searchString) && x.NodoAtencion.Contains(nodo)).Count();
                        //}
                        //else
                        //{
                        //    respuesta = context.VwTabSolicitudesIntegrales.Where(x => x.IdSolicitudIntegral > 0 && x.FichaCaptura == permiso_ua_usuario.FICHA && x.NodoAtencion.Contains(nodo)).OrderByDescending(x => x.FechaCreacion).Skip(pageSize * (page - 1)).Take(pageSize).ToList();
                        //    total_registros = context.VwTabSolicitudesIntegrales.Where(x => x.IdSolicitudIntegral > 0 && x.FichaCaptura == permiso_ua_usuario.FICHA && x.NodoAtencion.Contains(nodo)).Count();
                        //}
                       
                        break;
                    }
                }
            }
            
            return respuesta;
        }

        public static GestionGL.Datos.VW_D_USUARIOS_GENERAL ValidarSolicitante(string subdivision_consulta, string ficha_solicitante, string tipo_solicitud, string tipo_transporte)
        {
            GestionGL.Datos.VW_D_USUARIOS_GENERAL d_usuario_solicitante = null;

            if (ficha_solicitante.Trim() != "")
            {
                
                ficha_solicitante = Utilerias.FormatearFichaRfc(ficha_solicitante);
                
                using (GestionGL.Datos.GestionGLModel context = new GestionGL.Datos.GestionGLModel())
                {
                    //Faltan aplicar validaciones para ver si es un solictador valido
                    d_usuario_solicitante = context.VW_D_USUARIOS_GENERAL.Where(x => (x.SUBDIVISION.Substring(0, 3) == subdivision_consulta.Substring(0, 3)) && x.FICHA == ficha_solicitante && x.CVE_AREA_SERVICIO == "SOLINT" && x.FEC_FIN_D_USUARIO >= System.DateTime.Now && x.CVE_BAJAL != "B").FirstOrDefault();
                    
                    //var respuesta = context.M_USUARIOS.Where(x => x.FICHA.Contains(ficha_solicitante));
                    //usuario_solitante = respuesta != null && respuesta.Count() > 0 ? respuesta.ToArray()[0] : new GestionGL.Datos.M_USUARIOS();
                }
            }

            return d_usuario_solicitante;
        }

        public static GestionGL.Datos.VW_C_D_AUTORIZADORES ValidarAutorizador(string subdivision_usuario, string ficha_autorizador, DateTime fecha_subida, DateTime fecha_bajada, string tipo_solicitud, string tipo_transporte)
        {
            GestionGL.Datos.VW_C_D_AUTORIZADORES usuario_autorizador = null;

            if (ficha_autorizador.Trim() != "")
            {
                string[] areas_servicio = null;

                switch (tipo_solicitud)
                {
                    case "SOLO HOSPEDAJE":
                    {
                        areas_servicio = new string[] { "AH" };
                        break;
                    }
                    case "SOLO TRANSPORTE":
                    {
                        if (tipo_transporte == "AEREO")
                        {
                            areas_servicio = new string[] { "STA" };
                        }
                        else
                        {
                            areas_servicio = new string[] { "STPVM" };
                        }
                        break;
                    }
                    default:
                    {
                        if (tipo_transporte == "AEREO")
                        {
                            areas_servicio = new string[] { "AH", "STA" };
                        }
                        else
                        {
                            areas_servicio = new string[] { "AH", "STPVM" };
                        }
                        break;
                    }

                }
                
                ficha_autorizador = Utilerias.FormatearFichaRfc(ficha_autorizador);

                using (GestionGL.Datos.GestionGLModel context = new GestionGL.Datos.GestionGLModel())
                {
                    //Faltan aplicar validaciones para ver si es un solictador valido
                    var respuesta = from x in context.VW_C_D_AUTORIZADORES
                                    where x.FICHA == ficha_autorizador && areas_servicio.Contains(x.CVE_AREA_SERVICIO) && (x.FECHA_INICIO < fecha_subida && x.FECHA_FINAL > fecha_bajada && x.SUBDIVISION.Substring(0, 3) == subdivision_usuario.Substring(0,3))
                                    select x;
                    usuario_autorizador = respuesta != null && respuesta.Count() > 0 ? respuesta.ToArray()[0] : new GestionGL.Datos.VW_C_D_AUTORIZADORES();
                }
            }

            return usuario_autorizador;
        }
      
        public static GestionGL.Datos.VW_C_CONTRATOS ValidarContrato(string numero_contrato, DateTime fecha_subida, DateTime fecha_bajada)
        {
            GestionGL.Datos.VW_C_CONTRATOS contrato = null;

            if (numero_contrato.Trim() != "")
            {
                fecha_subida = fecha_subida.Date;
                fecha_bajada = fecha_bajada.Date;

                using (GestionGL.Datos.GestionGLModel context = new GestionGL.Datos.GestionGLModel())
                {
                    //Faltan aplicar validaciones para ver si es un solictador valido
                    var contratos = (from x in context.VW_C_CONTRATOS
                                     where x.CONTRATO == numero_contrato && (fecha_subida >= x.FECHA_INICIO && fecha_bajada <= x.FECHA_FINAL) && x.BORRADO_LOGICO == false
                                     select x).ToList();

                    if (contratos != null && contratos.Count() > 0)
                    {
                        contrato = contratos[0];
                    }
                }
                using (STPIntegral.Datos.STPIntegralModel STPModel = new STPIntegral.Datos.STPIntegralModel())
                {
                    //Validaciones para el numero de contrato
                    var folioVisita = (from x in STPModel.TabFolioVisitas
                                       where x.FolioVisita == numero_contrato && (fecha_subida >= x.FechaInicio && fecha_bajada <= x.FechaFin) && x.BorradoLogico == false
                                       select x).ToList();

                    if (folioVisita != null && folioVisita.Count() > 0)
                    {
                        contrato.CONTRATO = folioVisita[0].FolioVisita;
                        contrato.DESCRIPCION = folioVisita[0].Motivo;
                        contrato.FECHA_INICIO = folioVisita[0].FechaInicio;
                        contrato.FECHA_FINAL = folioVisita[0].FechaFin;
                        contrato.FIC_ACTUA = folioVisita[0].FichaCaptura;
                        contrato.FEC_ACTUA = folioVisita[0].FechaCaptura;
                        contrato.RFC_ACREEDOR = folioVisita[0].RfcCompania;
                        contrato.CVE_ACREEDOR = folioVisita[0].NoAcreedorSap;
                        contrato.DESCRIPCION_ACREEDOR = folioVisita[0].NombreCompania;
                    }
                }
            }
            
            return contrato;
        }

        public static Models.EmpleadoPemexCia ValidarEmpleadoPemexCia(string ficha_rfc, string tipo_empleado)
        {
            EmpleadoPemexCia resultado = null;

            if (ficha_rfc.Trim() != "")
            {

                ficha_rfc = Utilerias.FormatearFichaRfc(ficha_rfc);

                using (GestionGL.Datos.GestionGLModel context = new GestionGL.Datos.GestionGLModel())
                {
                    if (tipo_empleado == "PEMEX")
                    {
                        GestionGL.Datos.C_EMPLEADOS_PEMEX emple_pemex = context.C_EMPLEADOS_PEMEX.Where(x => x.FICHA == ficha_rfc).FirstOrDefault();

                        if (emple_pemex != null)
                        {
                            resultado = new EmpleadoPemexCia()
                            {
                                CURP = emple_pemex.CURP,
                                FichaRfc = emple_pemex.FICHA,
                                FolioLibretaMar = "",
                                IdEmpleadoPemexCia = emple_pemex.ID_C_EMPLEADO_PEMEX,
                                NombreCompleto = (emple_pemex.NOMBRES + " " + emple_pemex.APELLIDOS).Trim(),
                                Nombres = emple_pemex.NOMBRES,
                                Apellidos = emple_pemex.APELLIDOS,
                                Categoria = emple_pemex.CATEGORIA,
                                Genero = emple_pemex.GENERO,
                                Nacionalidad = "MEXICANO",
                                RFC = emple_pemex.RFC,
                                TipoEmpleado = "PEMEX",
                                VigenciaLibretaMar = null
                            };
                        }
                    }
                    else
                    {

                        GestionGL.Datos.C_EMPLEADOS_CIA emple_cia = context.C_EMPLEADOS_CIA.Where(x => x.RFC_FM3 == ficha_rfc).FirstOrDefault();

                        if (emple_cia != null)
                        {
                            resultado = new EmpleadoPemexCia()
                            {
                                CURP = emple_cia.CURP,
                                FichaRfc = emple_cia.RFC_FM3,
                                FolioLibretaMar = emple_cia.FOLIO_LIBRETA_MAR,
                                IdEmpleadoPemexCia = emple_cia.ID_C_EMPLEADO_CIA,
                                NombreCompleto = (emple_cia.NOMBRES + " " + emple_cia.APELLIDOS).Trim(),
                                Nombres = emple_cia.NOMBRES,
                                Apellidos = emple_cia.APELLIDOS,
                                Categoria = emple_cia.CATEGORIA,
                                Genero = emple_cia.GENERO,
                                Nacionalidad = emple_cia.NACIONALIDAD,
                                RFC = emple_cia.RFC_FM3,
                                TipoEmpleado = "COMPAÑIA",
                                VigenciaLibretaMar = emple_cia.VIGENCIA_LIBRETA_MAR
                            };
                        }
                    }
                }
            }

            return resultado;
        }

        public static InstalacionesDestinoDireccionamientos ConsultarOrigenes(string tipo_solicitud, string tipo_personal, string tipo_transporte, string tipo_servicio, int? id_itinerario, string ficha_autorizador, DateTime fecha_subida, DateTime fecha_bajada)
        {
            //tipo_solicitud SOLO HOSPEDAJE, SOLO TRANSPORTE, HOSPEDAJE Y TRANSPORTE
            //tipo_personal: PEMEX, COMPAÑIA, COMODATARIO
            //tipo_transporte: AEREO, MARITIMO
            //tipo_servicio: CAMBIO DE GUARDIA, REGULARES, LATERALES, DISPOSICION
            InstalacionesDestinoDireccionamientos resultado = new InstalacionesDestinoDireccionamientos();

            IEnumerable<GestionGL.Datos.C_M_INSTALACIONES> listado_instalaciones_origen = null;

            STPIntegral.Datos.STPIntegralModel stp_integral_contex = new Datos.STPIntegralModel();
            GestionGL.Datos.GestionGLModel gestion_gl_contex = new GestionGL.Datos.GestionGLModel();

            STPIntegral.Datos.VW_stp_c_itinerario_base itinerario = stp_integral_contex.VW_stp_c_itinerario_base.SingleOrDefault(x => x.id_itinerario == id_itinerario);

            string[] areas_sevicios = null;
            
            switch (tipo_solicitud)
            {
                case "SOLO HOSPEDAJE":
                {
                    areas_sevicios = new string[] { "AH" };
                    break;
                }
                case "SOLO TRANSPORTE":
                {
                    switch (tipo_transporte)
                    {
                        case "AEREO":
                        {
                            areas_sevicios = new string[] { "STA" };

                            if (tipo_personal != "PEMEX" && tipo_servicio == "CAMBIO DE GUARDIA")
                            {
                                throw new ShowCaseException("ERR-SI-0021: No se puede hacer una solicitud del tipo 'CAMBIO DE GUARDIA' con personal de COMPAÑIA o COMODATARIO para el servicio AEREO", TypeMessage.error,"ERR-SI-0021");
                            }

                            switch (itinerario.cve_nodo_origen)
                            {
                                case "CME":
                                case "BCS":
                                    {
                                        listado_instalaciones_origen = gestion_gl_contex.C_M_INSTALACIONES.Where(x => x.SIGLAS_INSTALACION == itinerario.cve_nodo_origen);
                                        break;
                                    }
                                case "IXT":
                                    {
                                        listado_instalaciones_origen = from x in gestion_gl_contex.C_M_INSTALACIONES
                                                                       where (from y in gestion_gl_contex.VW_C_D_INSTALACIONES where y.CVE_AREA_SERVICIO == "STA" select y.ID_C_M_INSTALACION).Distinct().Contains(x.ID_C_M_INSTALACION)
                                                                       select x;
                                        break;
                                    }
                                default: //OTROS NODOS (LA NORTE)
                                    {

                                        listado_instalaciones_origen = gestion_gl_contex.C_M_INSTALACIONES.Where(x => x.SIGLAS_INSTALACION == itinerario.cve_nodo_origen);
                                        break;
                                    }

                            }
                            break;
                        }
                        case "MARITIMO":
                        {
                            areas_sevicios = new string[] { "STPVM" };

                            switch (itinerario.cve_nodo_origen)
                            {
                                case "CME":
                                case "BCS": 
                                {
                                    listado_instalaciones_origen = gestion_gl_contex.C_M_INSTALACIONES.Where(x => x.SIGLAS_INSTALACION == itinerario.cve_nodo_origen);
                                    break; 
                                }
                                case "IXT":
                                {
                                    listado_instalaciones_origen = from x in gestion_gl_contex.C_M_INSTALACIONES
                                                                   where (from y in gestion_gl_contex.VW_C_D_INSTALACIONES where y.CVE_AREA_SERVICIO == "STPVM" select y.ID_C_M_INSTALACION).Distinct().Contains(x.ID_C_M_INSTALACION)
                                                                   select x;
                                    break; 
                                }
                                default: //OTROS NODOS (LA NORTE)
                                {

                                    listado_instalaciones_origen = gestion_gl_contex.C_M_INSTALACIONES.Where(x => x.SIGLAS_INSTALACION == itinerario.cve_nodo_origen);
                                    break;
                                }
                            }
                            break;
                        }
                    }
                    break;
                }
                case "HOSPEDAJE Y TRANSPORTE":
                {
                    switch (tipo_transporte)
                    {
                        case "AEREO":
                        {
                            areas_sevicios = new string[] { "AH", "STA" };

                            if (tipo_personal != "PEMEX" && tipo_servicio == "CAMBIO DE GUARDIA")
                            {
                                throw new ShowCaseException("ERR-SI-0022: No se puede hacer una solicitud del tipo 'CAMBIO DE GUARDIA' con personal de COMPAÑIA o COMODATARIO para el servicio AEREO", TypeMessage.error,"ERR-SI-0022");
                            }

                            switch (itinerario.cve_nodo_origen)
                            {
                                case "CME":
                                case "BCS":
                                {
                                    listado_instalaciones_origen = gestion_gl_contex.C_M_INSTALACIONES.Where(x => x.SIGLAS_INSTALACION == itinerario.cve_nodo_origen);
                                    break;
                                }
                                case "IXT":
                                {
                                    listado_instalaciones_origen = from x in gestion_gl_contex.C_M_INSTALACIONES
                                                                    where 
                                                                    (
                                                                        from y in gestion_gl_contex.VW_C_D_INSTALACIONES 
                                                                        where y.CVE_AREA_SERVICIO == "STA" 
                                                                        && (from z in gestion_gl_contex.VW_PREH_INSTALACIONES_MARINAS where z.SISTEMA == "SCHAP" && z.BORRADO_LOGICO == false && z.INDICE_UBICACION_REPORTE_PREH_PLATAFORMAS != null
                                                                            select z.PK_ID_INSTALACION).Distinct().Contains(y.ID_C_M_INSTALACION)
                                                                        select y.ID_C_M_INSTALACION).Distinct().Contains(x.ID_C_M_INSTALACION)
                                                                    select x;
                                    break;
                                }
                                default: //OTROS NODOS (LA NORTE)
                                {
                                    listado_instalaciones_origen = gestion_gl_contex.C_M_INSTALACIONES.Where(x => x.SIGLAS_INSTALACION == itinerario.cve_nodo_origen);
                                    break;
                                }
                            }
                            break;
                        }
                        case "MARITIMO":
                        {
                            areas_sevicios = new string[] { "AH", "STPVM" };

                            switch (itinerario.cve_nodo_origen)
                            {
                                case "CME":
                                case "BCS":
                                {
                                    listado_instalaciones_origen = gestion_gl_contex.C_M_INSTALACIONES.Where(x => x.SIGLAS_INSTALACION == itinerario.cve_nodo_origen);
                                    break;
                                }
                                case "IXT":
                                {
                                    listado_instalaciones_origen = from x in gestion_gl_contex.C_M_INSTALACIONES
                                                                    where
                                                                    (
                                                                        from y in gestion_gl_contex.VW_C_D_INSTALACIONES
                                                                        where y.CVE_AREA_SERVICIO == "STPVM"
                                                                        && (from z in gestion_gl_contex.VW_PREH_INSTALACIONES_MARINAS
                                                                            where z.SISTEMA == "SCHAP" && z.BORRADO_LOGICO == false && z.INDICE_UBICACION_REPORTE_PREH_PLATAFORMAS != null
                                                                            select z.PK_ID_INSTALACION).Distinct().Contains(y.ID_C_M_INSTALACION)
                                                                        select y.ID_C_M_INSTALACION).Distinct().Contains(x.ID_C_M_INSTALACION)
                                                                    select x;
                                    break;
                                }
                                default: //OTROS NODOS (LA NORTE)
                                {
                                    listado_instalaciones_origen = gestion_gl_contex.C_M_INSTALACIONES.Where(x => x.SIGLAS_INSTALACION == itinerario.cve_nodo_origen);
                                    break;
                                }
                            }

                            break;
                        }
                    }

                    break;
                }
            }

            var listado_ids_instalaciones_origen = (from x in listado_instalaciones_origen select x.ID_C_M_INSTALACION).ToList();

            var direccionamientos_instalaciones_origen = (from x in gestion_gl_contex.VW_PERMISOS_DIRECCIONAMIENTOS_GENERAL
                                                          where (listado_ids_instalaciones_origen).Contains(x.ID_C_M_INSTALACION) && (x.INICIO <= fecha_subida && x.FIN >= fecha_subida) && x.FICHA_AUTORIZADOR == ficha_autorizador && areas_sevicios.Contains(x.AREA_SERVICIO_AUTORIZADOR)
                                                          select x).ToList();


            if (tipo_transporte == "AEREO" && (tipo_servicio == "LATERALES" || (tipo_servicio == "REGULARES" && itinerario.cve_nodo_origen == "IXT")) )
            {
                //Si el tipo de servicio es un movimiento lateral entonces se filtran las instalaciones origenes a las que el autorizador tenga direccionamientos.
                var listado_ids_instalaciones_direccionamientos = (from x in direccionamientos_instalaciones_origen select x.ID_C_M_INSTALACION).Distinct();
                listado_instalaciones_origen = (from x in listado_instalaciones_origen where listado_ids_instalaciones_direccionamientos.Contains(x.ID_C_M_INSTALACION) select x).ToList();
            }


            if (tipo_solicitud != "SOLO HOSPEDAJE")
            {
                listado_instalaciones_origen = (from x in listado_instalaciones_origen
                                                orderby x.NOMBRE_INSTALACION ascending
                                                select new GestionGL.Datos.C_M_INSTALACIONES
                                                    {
                                                        ID_C_M_INSTALACION = x.ID_C_M_INSTALACION,
                                                        SIGLAS_INSTALACION = x.SIGLAS_INSTALACION,
                                                        NOMBRE_INSTALACION = x.NOMBRE_INSTALACION
                                                    }).ToList();
            }

            //tipo_solicitud SOLO HOSPEDAJE, SOLO TRANSPORTE, HOSPEDAJE Y TRANSPORTE
            //tipo_personal: PEMEX, COMPAÑIA, COMODATARIO
            //tipo_transporte: AEREO, MARITIMO

            resultado.DireccionamientosInstalaciones = direccionamientos_instalaciones_origen;
            resultado.InstalacionesDestino = listado_instalaciones_origen;

            gestion_gl_contex.Dispose();
            stp_integral_contex.Dispose();

            return resultado;
        }

        public static InstalacionesDestinoDireccionamientos ConsultarDestinos(string tipo_solicitud, string tipo_personal, string tipo_transporte, string tipo_servicio, int? id_itinerario, string ficha_autorizador, DateTime fecha_subida, DateTime fecha_bajada, int? id_instalacion_origen)
        {
            //tipo_solicitud SOLO HOSPEDAJE, SOLO TRANSPORTE, HOSPEDAJE Y TRANSPORTE
            //tipo_personal: PEMEX, COMPAÑIA, COMODATARIO
            //tipo_transporte: AEREO, MARITIMO
            //tipo_servicio: CAMBIO DE GUARDIA, REGULARES, LATERALES, DISPOSICION

            InstalacionesDestinoDireccionamientos resultado = new InstalacionesDestinoDireccionamientos();

            IEnumerable<GestionGL.Datos.C_M_INSTALACIONES> listado_instalaciones_destino = null;

            STPIntegral.Datos.STPIntegralModel stp_integral_contex = new Datos.STPIntegralModel();
            GestionGL.Datos.GestionGLModel gestion_gl_contex = new GestionGL.Datos.GestionGLModel();

            STPIntegral.Datos.VW_stp_c_itinerario_base itinerario = stp_integral_contex.VW_stp_c_itinerario_base.SingleOrDefault(x => x.id_itinerario == id_itinerario);

            if ((tipo_solicitud != "SOLO HOSPEDAJE" && itinerario != null) || tipo_solicitud == "SOLO HOSPEDAJE")
            {
                //throw new ShowCaseException(String.Format("ERR-SI-0060: No se pueden consultar los destinos pues no se especifico o no se encontro un itinerario de transporte {0}: {1}", tipo_transporte, id_itinerario), TypeMessage.error, "ERR-SI-0060");

                string[] areas_sevicios = null;

                switch (tipo_solicitud)
                {
                    case "SOLO HOSPEDAJE":
                        {
                            areas_sevicios = new string[] { "AH" };

                            var listado_instalaciones_hospedaje = (from x in gestion_gl_contex.VW_PERMISOS_DIRECCIONAMIENTOS_GENERAL
                                                                   where (x.INICIO <= fecha_subida && x.FIN >= fecha_subida) && x.FICHA_AUTORIZADOR == ficha_autorizador && x.AREA_SERVICIO_AUTORIZADOR == "AH"
                                                                   && (from y in gestion_gl_contex.VW_PREH_INSTALACIONES_MARINAS where y.BORRADO_LOGICO == false && y.SISTEMA == "SCHAP" && y.INDICE_UBICACION_REPORTE_PREH_PLATAFORMAS != null select y.PK_ID_INSTALACION).Contains(x.ID_C_M_INSTALACION)
                                                                   select x.ID_C_M_INSTALACION).Distinct();

                            listado_instalaciones_destino = (from x in gestion_gl_contex.C_M_INSTALACIONES
                                                             where (listado_instalaciones_hospedaje).Contains(x.ID_C_M_INSTALACION)
                                                             select x).ToArray();

                            break;
                        }
                    case "SOLO TRANSPORTE":
                        {
                            switch (tipo_transporte)
                            {
                                case "AEREO":
                                    {
                                        areas_sevicios = new string[] { "STA" };

                                        if (tipo_personal != "PEMEX" && tipo_servicio == "CAMBIO DE GUARDIA")
                                        {
                                            throw new ShowCaseException("ERR-SI-0023: No se puede hacer una solicitud del tipo 'CAMBIO DE GUARDIA' con personal de COMPAÑIA o COMODATARIO para el servicio AEREO", TypeMessage.error, "ERR-SI-0023");
                                        }

                                        switch (itinerario.cve_nodo_destino)
                                        {
                                            case "CME":
                                            case "BCS":
                                                {
                                                    listado_instalaciones_destino = gestion_gl_contex.C_M_INSTALACIONES.Where(x => x.SIGLAS_INSTALACION == itinerario.cve_nodo_destino);
                                                    break;
                                                }
                                            case "IXT":
                                                {
                                                    var listado_instalaciones_transporte = (from x in gestion_gl_contex.VW_PERMISOS_DIRECCIONAMIENTOS_GENERAL
                                                                                            where (x.INICIO <= fecha_subida && x.FIN >= fecha_subida) && x.FICHA_AUTORIZADOR == ficha_autorizador && x.AREA_SERVICIO_AUTORIZADOR == "STA"
                                                                                            select x.ID_C_M_INSTALACION).Distinct();

                                                    //VALIDACION DE QUE CUANDO LA SOLICITUD ES DE SOLO TRANSPORTE NO MUESTRE INSTALACIONES GESTIONADAS POR EL SCHAP
                                                    if (tipo_servicio != "DISPOSICION" && tipo_servicio != "LATERALES")
                                                    {
                                                        var listado_ids_instalaciones_schap = (from x in gestion_gl_contex.VW_PREH_INSTALACIONES_MARINAS select x.PK_ID_INSTALACION).ToList();
                                                        listado_instalaciones_transporte = (from x in listado_instalaciones_transporte where !listado_ids_instalaciones_schap.Contains(x) select x);
                                                    }


                                                    listado_instalaciones_destino = (from x in gestion_gl_contex.C_M_INSTALACIONES
                                                                                     where listado_instalaciones_transporte.Contains(x.ID_C_M_INSTALACION)
                                                                                     select x).ToArray();

                                                    break;
                                                }
                                            default: //OTROS NODOS (LA NORTE)
                                                {
                                                    var listado_instalaciones_transporte = (from x in gestion_gl_contex.VW_PERMISOS_DIRECCIONAMIENTOS_GENERAL
                                                                                            where (x.INICIO <= fecha_subida && x.FIN >= fecha_subida) && x.FICHA_AUTORIZADOR == ficha_autorizador && x.AREA_SERVICIO_AUTORIZADOR == "STA"
                                                                                            select x.ID_C_M_INSTALACION).Distinct();

                                                    listado_instalaciones_destino = (from x in gestion_gl_contex.C_M_INSTALACIONES
                                                                                     where listado_instalaciones_transporte.Contains(x.ID_C_M_INSTALACION) && x.SIGLAS_INSTALACION == itinerario.cve_nodo_destino
                                                                                     select x).ToArray();
                                                    break;
                                                }
                                        }
                                        break;
                                    }
                                case "MARITIMO":
                                    {

                                        areas_sevicios = new string[] { "STPVM" };

                                        switch (itinerario.cve_nodo_destino)
                                        {
                                            case "CME":
                                            case "BCS":
                                                {
                                                    listado_instalaciones_destino = gestion_gl_contex.C_M_INSTALACIONES.Where(x => x.SIGLAS_INSTALACION == itinerario.cve_nodo_destino);
                                                    break;
                                                }
                                            case "IXT":
                                                {
                                                    IEnumerable<int> listado_ids_instalaciones_transporte = null;

                                                    string [] SiglasInstalacionesComodin = new string [] { "RIX", "RAK", "TDS", "RKU", "RRS" };
                                                    IEnumerable<int> listado_ids_instalaciones_comodin = (from x in gestion_gl_contex.C_M_INSTALACIONES.Where(x => SiglasInstalacionesComodin.Contains(x.SIGLAS_INSTALACION)) select x.ID_C_M_INSTALACION).Distinct();

                                                    if (tipo_servicio == "LATERALES")
                                                    {
                                                        listado_ids_instalaciones_transporte = stp_integral_contex.stp_c_itinerario_destino.Where(x => x.id_itinerario == id_itinerario).Select(x => x.fk_instalacion).ToList(); 

                                                        //Comprobando si tiene instalaciones comodin
                                                        if ((from x in listado_ids_instalaciones_comodin where listado_ids_instalaciones_transporte.Contains(x) select x).Count() > 0)
                                                        {
                                                            //Tiene instalaciones comodin
                                                            listado_ids_instalaciones_transporte = (from x in gestion_gl_contex.VW_C_D_INSTALACIONES
                                                                                                    where x.CVE_AREA_SERVICIO == "STPVM"
                                                                                                    select x.ID_C_M_INSTALACION).Distinct();
                                                        }
                                                    }
                                                    else
                                                    {
                                                        listado_ids_instalaciones_transporte = (from x in gestion_gl_contex.VW_PERMISOS_DIRECCIONAMIENTOS_GENERAL
                                                         where (x.INICIO <= fecha_subida && x.FIN >= fecha_subida) && x.FICHA_AUTORIZADOR == ficha_autorizador && x.AREA_SERVICIO_AUTORIZADOR == "STPVM"
                                                         select x.ID_C_M_INSTALACION).Distinct();
                                                    }

                                                    if (tipo_servicio == "CAMBIO DE GUARDIA" || tipo_servicio == "REGULARES")
                                                    {
                                                        var listado_ids_instalaciones_destino_ruta_maritimo = stp_integral_contex.stp_c_itinerario_destino.Where(x => x.id_itinerario == id_itinerario).Select(x => x.fk_instalacion).ToList();

                                                        if ((from x in listado_ids_instalaciones_comodin where listado_ids_instalaciones_destino_ruta_maritimo.Contains(x) select x).Count() <= 0)
                                                        {
                                                            //No contiene instalaciones comodin
                                                            
                                                            //Filtrando las instalaciones a las que tiene permiso el autorizador junto con las que vienen en la ruta del itinerario
                                                            listado_ids_instalaciones_transporte = listado_ids_instalaciones_transporte.Where(x => listado_ids_instalaciones_destino_ruta_maritimo.Contains(x));
                                                        }

                                                        //VALIDACION DE QUE CUANDO LA SOLICITUD ES DE SOLO TRANSPORTE NO MUESTRE INSTALACIONES GESTIONADAS POR EL SCHAP
                                                        var listado_ids_instalaciones_schap = (from x in gestion_gl_contex.VW_PREH_INSTALACIONES_MARINAS select x.PK_ID_INSTALACION).ToList();
                                                        listado_ids_instalaciones_transporte = (from x in listado_ids_instalaciones_transporte where !listado_ids_instalaciones_schap.Contains(x) select x);
                                                    }

                                                   
                                                    listado_instalaciones_destino = (from x in gestion_gl_contex.C_M_INSTALACIONES
                                                                                        where listado_ids_instalaciones_transporte.Contains(x.ID_C_M_INSTALACION)
                                                                                        select x).ToArray();
                                                    
                                                    break;
                                                }
                                            default: //OTROS NODOS (LA NORTE)
                                                {
                                                    var listado_ids_instalaciones_transporte = (from x in gestion_gl_contex.VW_PERMISOS_DIRECCIONAMIENTOS_GENERAL
                                                                                            where (x.INICIO <= fecha_subida && x.FIN >= fecha_subida) && x.FICHA_AUTORIZADOR == ficha_autorizador && x.AREA_SERVICIO_AUTORIZADOR == "STPVM"
                                                                                            select x.ID_C_M_INSTALACION).Distinct();

                                                    if (tipo_servicio != "DISPOSICION")
                                                    {
                                                        var listado_ids_instalaciones_destino_ruta_maritimo = stp_integral_contex.stp_c_itinerario_destino.Where(x => x.id_itinerario == id_itinerario).Select(x => x.fk_instalacion).ToList();

                                                        //Filtrando las instalaciones a las que tiene permiso el autorizador junto con las que vienen en la ruta del itinerario
                                                        listado_ids_instalaciones_transporte = listado_ids_instalaciones_transporte.Where(x => listado_ids_instalaciones_destino_ruta_maritimo.Contains(x));
                                                    }

                                                    listado_instalaciones_destino = (from x in gestion_gl_contex.C_M_INSTALACIONES
                                                                                     where listado_ids_instalaciones_transporte.Contains(x.ID_C_M_INSTALACION) && x.SIGLAS_INSTALACION == itinerario.cve_nodo_destino
                                                                                     select x).ToArray();
                                                    break;
                                                }
                                        }
                                        break;
                                    }
                            }
                            break;
                        }
                    case "HOSPEDAJE Y TRANSPORTE":
                        {
                            switch (tipo_transporte)
                            {
                                case "AEREO":
                                    {
                                        areas_sevicios = new string[] { "AH", "STA" };

                                        if (tipo_personal != "PEMEX" && tipo_servicio == "CAMBIO DE GUARDIA")
                                        {
                                            throw new ShowCaseException("ERR-SI-0024: No se puede hacer una solicitud del tipo 'CAMBIO DE GUARDIA' con personal de COMPAÑIA o COMODATARIO para el servicio AEREO", TypeMessage.error, "ERR-SI-0024");
                                        }

                                        switch (itinerario.cve_nodo_destino)
                                        {
                                            case "CME":
                                            case "BCS":
                                                {
                                                    listado_instalaciones_destino = gestion_gl_contex.C_M_INSTALACIONES.Where(x => x.SIGLAS_INSTALACION == itinerario.cve_nodo_destino);
                                                    break;
                                                }
                                            case "IXT":
                                                {
                                                    var listado_instalaciones_hospedaje = (from x in gestion_gl_contex.VW_PERMISOS_DIRECCIONAMIENTOS_GENERAL
                                                                                           where (x.INICIO <= fecha_subida && x.FIN >= fecha_subida) && x.FICHA_AUTORIZADOR == ficha_autorizador && x.AREA_SERVICIO_AUTORIZADOR == "AH"
                                                                                           && (from y in gestion_gl_contex.VW_PREH_INSTALACIONES_MARINAS where y.BORRADO_LOGICO == false && y.SISTEMA == "SCHAP" && y.INDICE_UBICACION_REPORTE_PREH_PLATAFORMAS != null select y.PK_ID_INSTALACION).Contains(x.ID_C_M_INSTALACION)
                                                                                           select x.ID_C_M_INSTALACION).Distinct();

                                                    var listado_instalaciones_transporte = (from x in gestion_gl_contex.VW_PERMISOS_DIRECCIONAMIENTOS_GENERAL
                                                                                            where (x.INICIO <= fecha_subida && x.FIN >= fecha_subida) && x.FICHA_AUTORIZADOR == ficha_autorizador && x.AREA_SERVICIO_AUTORIZADOR == "STA"
                                                                                            select x.ID_C_M_INSTALACION).Distinct();

                                                    //TODO: Aqui tambien ocurre alguna excepcion provocada desde la pantalla de Reprogramacion [Tiempo de espera Agotado]
                                                    listado_instalaciones_destino = (from x in gestion_gl_contex.C_M_INSTALACIONES
                                                                                     where (from y in listado_instalaciones_hospedaje
                                                                                            where listado_instalaciones_transporte.Contains(y)
                                                                                            select y).Contains(x.ID_C_M_INSTALACION)
                                                                                     select x).ToArray();
                                                    break;
                                                }
                                        }
                                        break;
                                    }
                                case "MARITIMO":
                                    {
                                        areas_sevicios = new string[] { "AH", "STPVM" };

                                        switch (itinerario.cve_nodo_destino)
                                        {
                                            case "CME":
                                            case "BCS":
                                                {
                                                    listado_instalaciones_destino = gestion_gl_contex.C_M_INSTALACIONES.Where(x => x.SIGLAS_INSTALACION == itinerario.cve_nodo_destino);
                                                    break;
                                                }
                                            case "IXT":
                                                {
                                                    var listado_ids_instalaciones_hospedaje = (from x in gestion_gl_contex.VW_PERMISOS_DIRECCIONAMIENTOS_GENERAL
                                                                                           where (x.INICIO <= fecha_subida && x.FIN >= fecha_subida) && x.FICHA_AUTORIZADOR == ficha_autorizador && x.AREA_SERVICIO_AUTORIZADOR == "AH"
                                                                                           && (from y in gestion_gl_contex.VW_PREH_INSTALACIONES_MARINAS where y.BORRADO_LOGICO == false && y.SISTEMA == "SCHAP" && y.INDICE_UBICACION_REPORTE_PREH_PLATAFORMAS != null select y.PK_ID_INSTALACION).Contains(x.ID_C_M_INSTALACION)
                                                                                           select x.ID_C_M_INSTALACION).Distinct();

                                                    var listado_ids_instalaciones_transporte = (from x in gestion_gl_contex.VW_PERMISOS_DIRECCIONAMIENTOS_GENERAL
                                                                                            where (x.INICIO <= fecha_subida && x.FIN >= fecha_subida) && x.FICHA_AUTORIZADOR == ficha_autorizador && x.AREA_SERVICIO_AUTORIZADOR == "STPVM"
                                                                                            select x.ID_C_M_INSTALACION).Distinct();

                                                    //Filtrando las instalaciones a las que se tiene permiso en hospedaje y en transporte
                                                    var listado_ids_instalaciones_transporte_hospedaje = listado_ids_instalaciones_transporte.Where(x => listado_ids_instalaciones_hospedaje.Contains(x));

                                                    if (tipo_servicio != "DISPOSICION")
                                                    {
                                                        var listado_ids_instalaciones_destino_ruta_maritimo = stp_integral_contex.stp_c_itinerario_destino.Where(x => x.id_itinerario == id_itinerario).Select(x => x.fk_instalacion).ToList();

                                                        //Filtrando las instalaciones a las que tiene permiso el autorizador junto con las que vienen en la ruta del itinerario
                                                        listado_ids_instalaciones_transporte_hospedaje = listado_ids_instalaciones_transporte_hospedaje.Where(x => listado_ids_instalaciones_destino_ruta_maritimo.Contains(x));
                                                    }

                                                    //TODO: Esto marco un error de tiempo de espera en una prueba, quiza haga falta un try catch.
                                                    listado_instalaciones_destino = (from x in gestion_gl_contex.C_M_INSTALACIONES
                                                                                     where listado_ids_instalaciones_transporte_hospedaje.Contains(x.ID_C_M_INSTALACION)
                                                                                     select x).ToArray();
                                                    break;
                                                }
                                        }
                                        break;
                                    }
                            }
                            break;
                        }
                }

                var listado_ids_instalaciones_destino = (from x in listado_instalaciones_destino select x.ID_C_M_INSTALACION).ToList();

                var direccionamientos_instalaciones_destino = (from x in gestion_gl_contex.VW_PERMISOS_DIRECCIONAMIENTOS_GENERAL
                                                               where (listado_ids_instalaciones_destino).Contains(x.ID_C_M_INSTALACION) && (x.INICIO <= fecha_subida && x.FIN >= fecha_subida) && x.FICHA_AUTORIZADOR == ficha_autorizador && areas_sevicios.Contains(x.AREA_SERVICIO_AUTORIZADOR)
                                                               select x).ToList();


                listado_instalaciones_destino = (from x in listado_instalaciones_destino
                                                 orderby x.NOMBRE_INSTALACION ascending
                                                 select new GestionGL.Datos.C_M_INSTALACIONES
                                                 {
                                                     ID_C_M_INSTALACION = x.ID_C_M_INSTALACION,
                                                     SIGLAS_INSTALACION = x.SIGLAS_INSTALACION,
                                                     NOMBRE_INSTALACION = x.NOMBRE_INSTALACION
                                                 }).ToList();

                //tipo_solicitud SOLO HOSPEDAJE, SOLO TRANSPORTE, HOSPEDAJE Y TRANSPORTE
                //tipo_personal: PEMEX, COMPAÑIA, COMODATARIO
                //tipo_transporte: AEREO, MARITIMO

                resultado.DireccionamientosInstalaciones = direccionamientos_instalaciones_destino;
                resultado.InstalacionesDestino = listado_instalaciones_destino;
            }

            gestion_gl_contex.Dispose();
            stp_integral_contex.Dispose();

            return resultado;
        }

        public static STPIntegral.Datos.TabDetallesSolicitudIntegral ValidarEmpleadoDetalleSolicitud(string tipo_solicitud, string tipo_personal, string tipo_transporte, string tipo_servicio, int id_itinerario, string ficha_autorizador, DateTime fecha_subida, DateTime fecha_bajada, int id_instalacion_origen, int id_instalacion_destino, string ficha_rfc, string tipo_movimiento_personal, string folio_especial_transporte, string folio_especial_hospedaje, string numero_contrato, string subdivision_autorizador)
        {
            STPIntegral.Datos.TabDetallesSolicitudIntegral detalle = new Datos.TabDetallesSolicitudIntegral();

            STPIntegral.Datos.STPIntegralModel context_stpIntegral = new Datos.STPIntegralModel();
            GestionGL.Datos.GestionGLModel context_gestionGL = new GestionGL.Datos.GestionGLModel();
            SCHAP.Datos.SCHAPModel contex_schap = new SCHAP.Datos.SCHAPModel();

            detalle = ValidarEmpleadoDetalleSolicitud(tipo_solicitud, tipo_personal, tipo_transporte, tipo_servicio, id_itinerario, ficha_autorizador, fecha_subida, fecha_bajada, id_instalacion_origen, id_instalacion_destino, ficha_rfc, tipo_movimiento_personal, folio_especial_transporte, folio_especial_hospedaje, numero_contrato, subdivision_autorizador, true, context_stpIntegral, context_gestionGL, contex_schap);

            context_stpIntegral.Dispose();
            context_gestionGL.Dispose();
            contex_schap.Dispose();

            return detalle;
        }

        private static STPIntegral.Datos.TabDetallesSolicitudIntegral ValidarEmpleadoDetalleSolicitud(string tipo_solicitud, string tipo_personal, string tipo_transporte, string tipo_servicio, int? id_itinerario, string ficha_autorizador, DateTime fecha_subida, DateTime fecha_bajada, int id_instalacion_origen, int id_instalacion_destino, string ficha_rfc, string tipo_movimiento_personal, string folio_especial_transporte, string folio_especial_hospedaje, string numero_contrato, string subdivision_autorizador, bool validar_disponiblidad_hospedaje_transporte, STPIntegral.Datos.STPIntegralModel context_stpIntegral, GestionGL.Datos.GestionGLModel context_gestionGL, SCHAP.Datos.SCHAPModel context_schap)
        {
            STPIntegral.Datos.TabDetallesSolicitudIntegral detalle = new Datos.TabDetallesSolicitudIntegral();
            detalle.FechaSubida = fecha_subida;
            detalle.FechaBajada = fecha_bajada;

            //Quitandole la hora a las fechas de subida y bajada
            fecha_bajada = fecha_bajada.Date;
            fecha_subida = fecha_subida.Date;

            //Validando que exista la ficha/RFC
            if (tipo_personal == "PEMEX")
            {
                
                GestionGL.Datos.C_EMPLEADOS_PEMEX empleado = context_gestionGL.C_EMPLEADOS_PEMEX.Where(x => x.FICHA == ficha_rfc).SingleOrDefault();
                if (empleado != null)
                {
                    detalle.FichaRFCEmpleado = empleado.FICHA;
                    detalle.NombreEmpleado = (empleado.NOMBRES + " " + empleado.APELLIDOS).Trim();
                    detalle.IdPasajeroCiaPEMEX = empleado.ID_C_EMPLEADO_PEMEX;
                }
                else
                {
                    throw new ShowCaseException(string.Format("ERR-SI-0025: No se encontro al empleado de PEMEX con la ficha: '{0}'",ficha_rfc),TypeMessage.warning,"ERR-SI-0025");
                }
            }
            else
            {
                GestionGL.Datos.C_EMPLEADOS_CIA empleado = context_gestionGL.C_EMPLEADOS_CIA.Where(x => x.RFC_FM3 == ficha_rfc).SingleOrDefault();
                if (empleado != null)
                {
                    detalle.FichaRFCEmpleado = empleado.RFC_FM3;
                    detalle.NombreEmpleado = (empleado.NOMBRES + " " + empleado.APELLIDOS).Trim();
                    detalle.VigenciaLibretaMar = empleado.VIGENCIA_LIBRETA_MAR;
                    detalle.FolioLibretaMar = empleado.FOLIO_LIBRETA_MAR;
                    
                    detalle.IdPasajeroCiaPEMEX = empleado.ID_C_EMPLEADO_CIA;
                }
                else
                {
                    throw new ShowCaseException(string.Format("ERR-SI-0026: No se encontro al empleado de COMPAÑIA con el RFC: '{0}'", ficha_rfc), TypeMessage.warning, "ERR-SI-0026");
                }
            }

            //Si el usuario existe entonces se valida la capacidad de los servicios solicitados
            //tipo_solicitud SOLO HOSPEDAJE, SOLO TRANSPORTE, HOSPEDAJE Y TRANSPORTE

            switch (tipo_solicitud)
            {
                case "SOLO HOSPEDAJE":
                case "HOSPEDAJE Y TRANSPORTE":
                { 
                    //Dependiendo del tipo de movimiento hay que validar si el empleado se encuentra a bordo de alguna otra instalacion o si esta en otra solicitud
                    if (tipo_movimiento_personal == "SUBIDA")
                    {
                        //MOVIMIENTO DE SUBIDA DE PERSONAL: Hay que validar que no este en otra solicitud de hospedaje y que tampoco este dado de alta en alguna otra plataforma

                        //Validaciones de SCHAP
                            //Validando que el trabajador no este en otra solicitud de AH
                            //--'A' - CONFIRMADO
                            //--'B' - BAJA A BORDO
                            //--'C' - CANCELADO
                            //--'H' - REGISTRADO A BORDO
                            //--'N' - SOLICITADO
                        var listado_huespedes = context_schap.SOLHOSP_HUESPEDES.Where(x => x.FICHA == detalle.FichaRFCEmpleado && (x.STATUS == "A" || x.STATUS == "N" || x.STATUS == "H") && (x.FECHA_LLEGADA < fecha_bajada && x.FECHA_SALIDA > fecha_subida)).ToList();
                            
                            if(listado_huespedes.Count() > 0)
                            {
                                throw new ShowCaseException(String.Format("ERR-SI-0027: No se puede registrar al trabajador {0} [{1}] pues ya se encuentra registrado en una solicitud de hospedaje con folio '{2}' para el rango de fechas '{3:dd/MM/yyyy}' y '{4:dd/MM/yyyy}'", detalle.NombreEmpleado, detalle.FichaRFCEmpleado, listado_huespedes[0].FOLIO, listado_huespedes[0].FECHA_LLEGADA, listado_huespedes[0].FECHA_SALIDA), TypeMessage.warning, "ERR-SI-0027");
                            }

                            //Si cumple esta validacion ahora verificamos que no este en ninguna instalacion del SCHAP
                            var listado_ocupantes_schap = context_schap.TAB_OCUPANTES.Where(x => x.BORRADO_LOGICO == false && x.FIC_RFC_USUARIO == detalle.FichaRFCEmpleado && (x.FEC_INI < fecha_bajada && x.FEC_TER > fecha_subida)).ToList();

                            if (listado_ocupantes_schap.Count() > 0)
                            {
                                int fk_id_instalacion = listado_ocupantes_schap[0].FK_ID_C_M_INSTALACION;
                                GestionGL.Datos.C_M_INSTALACIONES instalacion = context_gestionGL.C_M_INSTALACIONES.Where(x => x.ID_C_M_INSTALACION == fk_id_instalacion).SingleOrDefault();
                                throw new ShowCaseException(String.Format("ERR-SI-0028: No se puede registrar al trabajador {0} [{1}] pues actualmente se encuentra a bordo de la instalación '{2}'con solicitud folio '{3}' para el rango de fechas '{4:dd/MM/yyyy}' y '{5:dd/MM/yyyy}'", detalle.NombreEmpleado, detalle.FichaRFCEmpleado, instalacion != null ? instalacion.NOMBRE_INSTALACION : listado_ocupantes_schap[0].FK_ID_C_M_INSTALACION.ToString(), listado_ocupantes_schap[0].FOLIO_SOLICITUD, listado_ocupantes_schap[0].FEC_INI, listado_ocupantes_schap[0].FEC_TER), TypeMessage.warning, "ERR-SI-0028");
                            }
                        //Validaciones de la Solicitud Integral
                        //Que el trabajador no se encuentre a en otra solicitud integral para ese mismo rango de fechas
                            var listado_ocupantes_en_solicitud_integral = context_stpIntegral.TabDetallesSolicitudIntegral.Where(x => x.FichaRFCEmpleado == detalle.FichaRFCEmpleado && (x.EstadoHuespedAH == "SOLICITADO" || x.EstadoHuespedAH == "CONFIRMADO") && (x.FechaSubida <= detalle.FechaBajada && x.FechaBajada >= detalle.FechaSubida)).ToList();

                        if (listado_ocupantes_en_solicitud_integral.Count() > 0)
                        {
                            int id_solicitud_integral = listado_ocupantes_en_solicitud_integral[0].IdSolicitudIntegral;
                            var solicitud_integral = context_stpIntegral.TabSolicitudIntegral.Where(x => x.IdSolicitudIntegral == id_solicitud_integral).SingleOrDefault();
                            throw new ShowCaseException(String.Format("ERR-SI-0029: No se puede registrar al trabajador {0} [{1}] pues actualmente ya se encuentra en una solicitud integral con estado '{2}' y folio '{3}' para el rango de fechas '{4:dd/MM/yyyy}' y '{5:dd/MM/yyyy}'", detalle.NombreEmpleado, detalle.FichaRFCEmpleado, listado_ocupantes_en_solicitud_integral[0].EstadoOcupanteSolicitudIntegral, solicitud_integral.FolioSolicitudIntegral, listado_ocupantes_en_solicitud_integral[0].FechaSubida, listado_ocupantes_en_solicitud_integral[0].FechaBajada), TypeMessage.warning,"ERR-SI-0029");
                        }
                    }
                    else
                    {
                        //COMO no se requiere solicitud de hospedaje, solo se valida que no este en otra solicitud integral para la misma fecha.
                        //Queda pendiente validar que el trabajador se encuentre a bordo para poder bajar. (Anque solo se aplique la validacion para el cambio de guardia.

                        if (tipo_servicio == "CAMBIO DE GUARDIA")
                        {
                            //VALIDAR QUE EL TRABAJADOR DE BAJADA SE ENCUENTRE A BORDO DE LA INSTALACION
                            var listado_ocupantes = context_schap.TAB_OCUPANTES.Where(x => x.BORRADO_LOGICO == false && x.FIC_RFC_USUARIO == detalle.FichaRFCEmpleado && x.FK_ID_C_M_INSTALACION == id_instalacion_origen).ToList();
                                
                            if (listado_ocupantes.Count() <= 0)
                            {
                                GestionGL.Datos.C_M_INSTALACIONES instalacion = context_gestionGL.C_M_INSTALACIONES.Where(x => x.ID_C_M_INSTALACION == id_instalacion_origen).SingleOrDefault();
                                throw new ShowCaseException(String.Format("ERR-SI-0030: No se puede registrar al trabajador {0} [{1}] pues no se encuentra a bordo de la instalación '{2}'.", detalle.NombreEmpleado, detalle.FichaRFCEmpleado, instalacion.NOMBRE_INSTALACION), TypeMessage.warning, "ERR-SI-0030");
                            }
                        }

                        //Validando que no se encuentre en otra solicitud integral
                        var listado_ocupantes_en_solicitud_integral = context_stpIntegral.TabDetallesSolicitudIntegral.Where(x => x.FichaRFCEmpleado == detalle.FichaRFCEmpleado && (x.EstadoOcupanteSolicitudIntegral == "SOLICITADO" || x.EstadoOcupanteSolicitudIntegral == "CONFIRMADO") && (x.FechaSubida <= detalle.FechaBajada && x.FechaBajada >= detalle.FechaSubida)).ToList();

                        if (listado_ocupantes_en_solicitud_integral.Count() > 0)
                        {
                            int IdSolicitudIntegral = listado_ocupantes_en_solicitud_integral[0].IdSolicitudIntegral;
                            var solicitud_integral = context_stpIntegral.TabSolicitudIntegral.Where(x => x.IdSolicitudIntegral == IdSolicitudIntegral).SingleOrDefault();
                            throw new ShowCaseException(String.Format("ERR-SI-0031: No se puede registrar al trabajador {0} [{1}] pues actualmente ya se encuentra en una solicitud integral con estado '{2}' y folio '{3}' para el rango de fechas '{4:dd/MM/yyyy}' y '{5:dd/MM/yyyy}'", detalle.NombreEmpleado, detalle.FichaRFCEmpleado, listado_ocupantes_en_solicitud_integral[0].EstadoOcupanteSolicitudIntegral, solicitud_integral.FolioSolicitudIntegral, listado_ocupantes_en_solicitud_integral[0].FechaSubida, listado_ocupantes_en_solicitud_integral[0].FechaBajada), TypeMessage.warning,"ERR-SI-0031");
                        }
                    }

                    break;
                }
                case "SOLO TRANSPORTE":
                {
                    //DE MOMENTO NO SE VALIDA NADA
                    break;
                }
            }

            //En este punto ya pasamos las validaciones de que el trabajor este o no este a bordo, tambien que no este en otra solicitud integral para el mismo rango de fechas.
            //AHORA HAY QUE VALIDAR LA CAPACIDAD DE TRANSPORTE Y HOSPEDAJE

            if (validar_disponiblidad_hospedaje_transporte == true)
            {
                //Variables de utileria para no tener que depender de gestion Gl en las funciones que validan disponiblidad de hospedaje y transporte, ahora solo se depedne solo de una coneccion a StpIntegral (esto facilita el manejo de transacciones)
                GestionGL.Datos.C_M_INSTALACIONES instalacion_destino = null;
                GestionGL.Datos.C_CONTRATOS contrato_gestion_gl = null;

                if (!String.IsNullOrEmpty(numero_contrato.Trim()))
                {
                    contrato_gestion_gl = context_gestionGL.C_CONTRATOS.Where(x => x.CONTRATO == numero_contrato).SingleOrDefault();
                }

                if (tipo_solicitud != "SOLO TRANSPORTE" && tipo_movimiento_personal == "SUBIDA")
                {
                    instalacion_destino = context_gestionGL.C_M_INSTALACIONES.Where(x => x.ID_C_M_INSTALACION == id_instalacion_destino).SingleOrDefault();
                }

                int capacidad_disponible_solictada_transporte_hospedaje = 1;

                ValidarCapacidadHospedajeTransporte(instalacion_destino, contrato_gestion_gl, tipo_solicitud, tipo_servicio, tipo_movimiento_personal, fecha_subida, fecha_bajada, id_itinerario, folio_especial_transporte, folio_especial_hospedaje, subdivision_autorizador, capacidad_disponible_solictada_transporte_hospedaje, context_stpIntegral);
            }
            //En este punto el usuario ya paso todas las validaciones entonces si puede viajar

            return detalle;
        }

        private static bool ValidarCapacidadHospedajeTransporte(GestionGL.Datos.C_M_INSTALACIONES instalacion_destino, GestionGL.Datos.C_CONTRATOS contrato_gestion_gl, string tipo_solicitud, string tipo_servicio, string tipo_movimiento_personal, DateTime fecha_subida, DateTime fecha_bajada, int? id_itinerario, string folio_especial_transporte, string folio_especial_hospedaje, string subdivision_autorizador, int capacidad_disponible_solictada_transporte_hospedaje, STPIntegral.Datos.STPIntegralModel context_stpIntegral)
        {
            //Validando que exista capacidad en transporte (SI APLICA)            

            bool hay_capacidad_transporte = true;

            if (tipo_solicitud != "SOLO HOSPEDAJE")
            {
                hay_capacidad_transporte = ValidarCapacidadTransporte(tipo_solicitud, tipo_servicio, tipo_movimiento_personal, id_itinerario, fecha_subida, fecha_bajada, folio_especial_transporte, contrato_gestion_gl, subdivision_autorizador, capacidad_disponible_solictada_transporte_hospedaje, context_stpIntegral);
            }


            //Validando espacio en Hospedaje
            bool hay_disponibilidad_hospedaje = true;

            if (tipo_solicitud != "SOLO TRANSPORTE" && tipo_movimiento_personal == "SUBIDA")
            {
                hay_disponibilidad_hospedaje = ValidarCapacidadHospedaje(instalacion_destino, fecha_subida, fecha_bajada, contrato_gestion_gl, folio_especial_hospedaje, subdivision_autorizador, capacidad_disponible_solictada_transporte_hospedaje, context_stpIntegral);
            }

            if (hay_disponibilidad_hospedaje == false)
            {
                throw new ShowCaseException(String.Format("ERR-SI-0032: No hay disponibilidad de hospedaje para la instalación: '{1}'", instalacion_destino.NOMBRE_INSTALACION), TypeMessage.alert, "ERR-SI-0032");
            }

            return hay_disponibilidad_hospedaje && hay_capacidad_transporte;
        }

        private static bool ValidarCapacidadHospedaje(GestionGL.Datos.C_M_INSTALACIONES instalacion_destino, DateTime fecha_subida, DateTime fecha_bajada, GestionGL.Datos.C_CONTRATOS contrato_gestion_gl, string folio_especial, string subdivision_autorizador, int cantidad_requerida_espacio_hospedaje, STPIntegral.Datos.STPIntegralModel context_stpIntegral)
        {
            var hay_disponibilidad_hospedaje = false;

            //obtener las capacidades para todo el rango de fechas en la instalacion destino.
            var listado_reservaciones_hospedaje = context_stpIntegral.VW_TabReservacionesCapacidadHospedaje_NOLOCK.Where(x => x.FkIdInstalacion == instalacion_destino.ID_C_M_INSTALACION && (x.FechaCapacidad >= fecha_subida && x.FechaCapacidad <= fecha_bajada)).ToList();

            //ahora hay que verificar la disponibilidad para cada uno de los dias en que esta programada la estancia.
            DateTime dia_verificacion = fecha_subida.Date;

            List<STPIntegral.Datos.VW_TabReservacionesCapacidadHospedaje_NOLOCK> listado_disponibilidad_por_dia = null;
            List<STPIntegral.Datos.VW_TabReservacionesCapacidadHospedaje_NOLOCK> consulta_disponibilidad = null;

            int cantidad_disponible = 0;

            if (fecha_subida.Date != fecha_bajada.Date)
            {
                do
                {
                    //Se inicializa a false la disponibilidad del dia
                    hay_disponibilidad_hospedaje = false;

                    //Verificar disponibilidad para cada dia
                    //Primero se verifica si la cosulta regreso alguna reservacion para ese dia y esa instalacion, en caso contrario se decreta que no hay disponibilidad
                    listado_disponibilidad_por_dia = (from x in listado_reservaciones_hospedaje where x.FechaCapacidad == dia_verificacion && x.FkIdInstalacion == instalacion_destino.ID_C_M_INSTALACION select x).ToList(); //esta funcion regresa nulo si no encuentra nada para ese dia.

                    if (listado_disponibilidad_por_dia == null || listado_disponibilidad_por_dia.Count == 0)
                    {
                        //No hay reservacion para este dia.
                        throw new ShowCaseException(String.Format("ERR-SI-0033: No se encontro información de reservacion de hospedaje en la fecha: '{0:dd/MM/yyyy}' de la instalación: '{1}'", dia_verificacion, instalacion_destino.NOMBRE_INSTALACION), TypeMessage.alert, "ERR-SI-0033");
                    }

                    //Si la consulta regreso algun resultado entonces primero se verifica que haya disponibilidad para el folio especial (si es que lo proprocionaron)
                    if (!String.IsNullOrEmpty(folio_especial))
                    {
                        consulta_disponibilidad = (from x in listado_disponibilidad_por_dia where x.TipoReservacion == "FOLIO ESPECIAL" && x.FolioEspecial == folio_especial select x).ToList();

                        cantidad_disponible = (from x in consulta_disponibilidad select x.TotalDisponibles.Value).Sum();

                        if (cantidad_disponible >= cantidad_requerida_espacio_hospedaje)
                        {
                            hay_disponibilidad_hospedaje = true;
                        }
                    }

                    //luego se verifica que haya disponibilidad para el contrato (si es que lo proprocionaron)
                    if (contrato_gestion_gl != null && !String.IsNullOrEmpty(contrato_gestion_gl.CONTRATO) && hay_disponibilidad_hospedaje == false)
                    {
                        consulta_disponibilidad = (from x in listado_disponibilidad_por_dia where x.TipoReservacion == "CONTRATO" && x.CONTRATO == contrato_gestion_gl.CONTRATO select x).ToList();

                        cantidad_disponible = (from x in consulta_disponibilidad select x.TotalDisponibles.Value).Sum();

                        if (cantidad_disponible >= cantidad_requerida_espacio_hospedaje)
                        {
                            hay_disponibilidad_hospedaje = true;
                        }
                    }

                    //Luego validamos que exista disponibilidad a nivel Subdivision en caso de que con la validacion anterior no haya habido
                    if (!String.IsNullOrEmpty(subdivision_autorizador) && hay_disponibilidad_hospedaje == false)
                    {
                        consulta_disponibilidad = (from x in listado_disponibilidad_por_dia where x.TipoReservacion == "SUBDIVISION" && x.SUBDIVISION.StartsWith(subdivision_autorizador.Substring(0, 3)) select x).ToList();

                        cantidad_disponible = (from x in consulta_disponibilidad select x.TotalDisponibles.Value).Sum();

                        if (cantidad_disponible >= cantidad_requerida_espacio_hospedaje)
                        {
                            hay_disponibilidad_hospedaje = true;
                        }
                    }

                    //por ultimo validamos que exista disponibilidad en el fondo comun
                    if (hay_disponibilidad_hospedaje == false)
                    {
                        consulta_disponibilidad = (from x in listado_disponibilidad_por_dia where x.TipoReservacion == "FONDO COMUN" select x).ToList();

                        cantidad_disponible = (from x in consulta_disponibilidad select x.TotalDisponibles.Value).Sum();

                        if (cantidad_disponible >= cantidad_requerida_espacio_hospedaje)
                        {
                            hay_disponibilidad_hospedaje = true;
                        }
                    }

                    //Si no hay disponibilidad se muestra un mensaje de error.
                    if (!hay_disponibilidad_hospedaje)
                    {
                        throw new ShowCaseException(String.Format("ERR-SI-0034: No hay disponibilidad de hospedaje en la fecha: '{0:dd/MM/yyyy}' para la instalación: '{1}'", dia_verificacion, instalacion_destino.NOMBRE_INSTALACION), TypeMessage.alert, "ERR-SI-0034");
                    }
                    else
                    {
                        dia_verificacion = dia_verificacion.AddDays(1);
                    }
                }
                while (dia_verificacion <= fecha_bajada && hay_disponibilidad_hospedaje == true);
            }
            else
            {
                //Si hay hospedaje dado que sube y baja el mismo dia.
                hay_disponibilidad_hospedaje = true;
            }
            
            return hay_disponibilidad_hospedaje;
        }

        private static bool ValidarCapacidadTransporte(string tipo_solicitud, string tipo_servicio, string tipo_movimiento_personal, int? id_itinerario, DateTime fecha_subida, DateTime fecha_bajada, string folio_especial_transporte, GestionGL.Datos.C_CONTRATOS contrato_gestion_gl, string subdivision, int capacidad_disponible_solicitada, STPIntegral.Datos.STPIntegralModel context_stpIntegral)
        {
            bool hay_capacidad_transporte = false;
            //Si pasa hasta aqui es por que es necesario validar la capacidad en transporte.

            //Aqui se valida la capacidad de transporte de acuerdo al ITEM que trae el detalle
            //la unica exepcion es cuando el movimiento es cambio de guardia, pues hay un movimiento de subida y otro de bajada
            //de tal manera que hay dos items distintos. uno con nodos origen - destino [CME - IXT] para el movimiento de subida y el correspondiente a [IXT - CME] para el de bajada.

            int? itinerario_validado = id_itinerario;

            if (tipo_servicio != "CAMBIO DE GUARDIA" || (tipo_servicio == "CAMBIO DE GUARDIA" && tipo_movimiento_personal == "SUBIDA"))
            {
                //Los que no son de cambio de guardia solo se validan con el id del item que trae el detalle
                hay_capacidad_transporte = hay_capacidad_transporte = ValidarCapacidadTransportePorItem(id_itinerario, fecha_subida, folio_especial_transporte, contrato_gestion_gl, subdivision, capacidad_disponible_solicitada, context_stpIntegral);
            }
            else
            {
                //Si el movimiento es de cambio de guardia y el personal es el de bajada entonces se valida la capacidad contra el item de cambio de guardia que se del tipo IXT - CME

                //Los que no son de cambio de guardia solo se validan con el id del item que trae el detalle
                
                //Verificar que el itinerario sea de IXT a CME en caso contrario seleccionar el correcto
                //Primero nos traemos todos los itinerarios de cambio de guardia.
                var itinerarios_cambio_guardia = context_stpIntegral.VW_stp_c_itinerario_base.Where(x => x.des_tipo_servicio == "CAMBIO DE GUARDIA").ToList();
                //luego buscamos que tipo de nodo es el itinerario que nos proporcionaron
                STPIntegral.Datos.VW_stp_c_itinerario_base itinerario_proporcionado = (from x in itinerarios_cambio_guardia where x.id_itinerario == id_itinerario select x).SingleOrDefault();

                int id_itinerario_bajada_ixt_cme = 0;

                //if ((itinerario_proporcionado.cve_nodo_origen == "CME" || itinerario_proporcionado.cve_nodo_origen == "BCS") && (itinerario_proporcionado.cve_nodo_destino == "IXT"))
                if ((itinerario_proporcionado.cve_nodo_origen != "IXT") && (itinerario_proporcionado.cve_nodo_destino == "IXT"))
                {
                    //Se trata de un itinerario de subida, hay que seleccionar el itinerario de bajada, en este caso temporalmente seleccionamos el primero que entontremos
                    id_itinerario_bajada_ixt_cme = (from x in itinerarios_cambio_guardia where x.des_tipo_servicio == tipo_servicio && (x.cve_nodo_origen == itinerario_proporcionado.cve_nodo_destino && x.cve_nodo_destino == itinerario_proporcionado.cve_nodo_origen) select x.id_itinerario).First();
                }
                else
                {
                    //if ((itinerario_proporcionado.cve_nodo_destino == "CME" || itinerario_proporcionado.cve_nodo_destino == "BCS") && (itinerario_proporcionado.cve_nodo_origen == "IXT"))
                    if ((itinerario_proporcionado.cve_nodo_destino != "IXT") && (itinerario_proporcionado.cve_nodo_origen == "IXT"))
                    {
                        id_itinerario_bajada_ixt_cme = itinerario_proporcionado.id_itinerario;
                    }
                    else
                    {
                        //En caso de que proporcionen otra cosa entonces seleccionamos el primero que encontremos
                        //if (itinerario_proporcionado.cve_nodo_destino == "CME" || itinerario_proporcionado.cve_nodo_destino == "BCS")
                        if (itinerario_proporcionado.cve_nodo_destino != "IXT")
                        {
                            id_itinerario_bajada_ixt_cme = (from x in itinerarios_cambio_guardia where x.des_tipo_servicio == tipo_servicio && (x.cve_nodo_origen == "IXT" && x.cve_nodo_destino == itinerario_proporcionado.cve_nodo_origen) select x.id_itinerario).First();
                        }
                        else
                        {
                            //if (itinerario_proporcionado.cve_nodo_origen == "CME" || itinerario_proporcionado.cve_nodo_origen == "BCS")
                            if (itinerario_proporcionado.cve_nodo_origen != "IXT")
                            {
                                id_itinerario_bajada_ixt_cme = (from x in itinerarios_cambio_guardia where x.des_tipo_servicio == tipo_servicio && (x.cve_nodo_origen == "IXT" && x.cve_nodo_destino == itinerario_proporcionado.cve_nodo_origen) select x.id_itinerario).First();
                            }
                            else
                            {
                                //en ultima instalacion se selecciona el primer nodo IXT - CME
                                //id_itinerario_bajada_ixt_cme = (from x in itinerarios_cambio_guardia where x.des_tipo_servicio == tipo_servicio && (x.cve_nodo_origen == "IXT" && x.cve_nodo_destino == "CME") select x.id_itinerario).First();
                                id_itinerario_bajada_ixt_cme = (from x in itinerarios_cambio_guardia where x.des_tipo_servicio == tipo_servicio && (x.cve_nodo_origen == "IXT" && x.cve_nodo_destino != "IXT") select x.id_itinerario).First();
                            }
                        }
                    }
                }

                itinerario_validado = id_itinerario_bajada_ixt_cme;
                hay_capacidad_transporte = ValidarCapacidadTransportePorItem(id_itinerario_bajada_ixt_cme, fecha_subida, folio_especial_transporte, contrato_gestion_gl, subdivision, 1, context_stpIntegral);
            }

            if (tipo_solicitud != "SOLO HOSPEDAJE" && hay_capacidad_transporte == false)
            {
                throw new ShowCaseException(String.Format("ERR-SI-0035: No hay disponibilidad de transporte para el item seleccionado (id = {0}) y la fecha: '{1:dd/MM/yyyy}'", itinerario_validado.HasValue ? itinerario_validado.Value : 0, fecha_subida), TypeMessage.alert, "ERR-SI-0035");
            }

            return hay_capacidad_transporte;
        }

        private static bool ValidarCapacidadTransportePorItem(int? id_itinerario, DateTime fecha_subida, string folio_especial_transporte, GestionGL.Datos.C_CONTRATOS contrato_gestion_gl, string subdivision, int capacidad_solicitada, STPIntegral.Datos.STPIntegralModel context_stpIntegral)
        {
            bool hay_capacidad_transporte = false;

            //Si hubiera un numero de contrato entonces primero se valida la capacidad en stp_tb_capacidad_contrato
            DateTime fecha_inicio_consulta = fecha_subida.Date;
            DateTime fecha_fin_consulta = fecha_subida.Date.AddDays(1);

            var listado_capacidades_item = context_stpIntegral.Vw_SoltIntItinerariosCapacidadDia_NOLOCK.Where(x => x.id_itinerario == id_itinerario.Value && (x.fecha_capacidad >= fecha_inicio_consulta && x.fecha_capacidad < fecha_fin_consulta)).ToList();

            //DE AQUI EN ADELANTE SE APLICAN LAS MISMAS VALIDACIONES QUE EL OTRO MODULO, ESTO PUEDE QUEDAR EN UNA FUNCION PARA MAS FACILIDAD.
            if (listado_capacidades_item != null && listado_capacidades_item.Count() > 0)
            {
                //Primero se valida que haya disponibilidad en el folio_especial
                if (folio_especial_transporte != "")
                {
                    //Se consulta a la tabla stp_tb_capacida_detalle
                    var listado_capacidades_por_contrato_item = listado_capacidades_item.Where(x => x.folio_especial == folio_especial_transporte).FirstOrDefault();

                    if (listado_capacidades_por_contrato_item != null)
                    {
                        if (listado_capacidades_por_contrato_item.capacidad_disponible >= capacidad_solicitada)
                        {
                            //Si hay espacios disponible.
                            hay_capacidad_transporte = true;
                        }
                    }
                }
                
                //Luego se valida si hay disponibilidad en capacidad contato en caso de que proporcionen un contrato.
                if (contrato_gestion_gl != null && String.IsNullOrEmpty(contrato_gestion_gl.CONTRATO.Trim()) && hay_capacidad_transporte == false)
                {
                    //Se consulta a la tabla stp_tb_capacida_detalle
                    var listado_capacidades_por_contrato_item = listado_capacidades_item.Where(x => x.CONTRATO == contrato_gestion_gl.CONTRATO).FirstOrDefault();

                    if (listado_capacidades_por_contrato_item != null)
                    {
                        if (listado_capacidades_por_contrato_item.capacidad_disponible >= capacidad_solicitada)
                        {
                            //Si hay espacios disponible.
                            hay_capacidad_transporte = true;
                        }
                    }
                }

                //Luego se valida si hay disponibilidad en capacidad contato en caso de que proporcionen un contrato.
                if (String.IsNullOrEmpty(subdivision) && hay_capacidad_transporte == false)
                {
                    //Se consulta a la tabla stp_tb_capacida_detalle
                    var listado_capacidades_por_contrato_item = listado_capacidades_item.Where(x => x.SUBDIVISION == subdivision).FirstOrDefault();

                    if (listado_capacidades_por_contrato_item != null)
                    {
                        if (listado_capacidades_por_contrato_item.capacidad_disponible >= capacidad_solicitada)
                        {
                            //Si hay espacios disponible.
                            hay_capacidad_transporte = true;
                        }
                    }
                }

                //Si no proporcionaron un folio especial u contrato o bien si no hubo disponibilidad en la tabla de capacidad detalles entonces se valida la diponibilidad general
                if (hay_capacidad_transporte == false)
                {
                    var listado_capacidades_por_contrato_item = listado_capacidades_item.Where(x => x.ID_C_CONTRATO.HasValue == false && x.ID_C_SUBDIVISION.HasValue == false && String.IsNullOrEmpty(x.folio_especial)).FirstOrDefault();

                    if (listado_capacidades_por_contrato_item != null && listado_capacidades_por_contrato_item.capacidad_disponible >= capacidad_solicitada)
                    {
                        hay_capacidad_transporte = true;
                    }
                }
            }
            return hay_capacidad_transporte;
        }

        public static int ObtenerCapacidadHospedaje(int id_instalacion_destino, DateTime fecha_subida, DateTime fecha_bajada, string no_contrato, string folio_especial, string subdivision_autorizador)
        {
            int resultado = 0;

            if (id_instalacion_destino > 0)
            {
                GestionGL.Datos.C_CONTRATOS contrato_gestion_gl = null;
                GestionGL.Datos.C_M_INSTALACIONES instalacion_destino = null;

                
                using (GestionGL.Datos.GestionGLModel gestionGlContext = new GestionGL.Datos.GestionGLModel())
                {
                    if (!String.IsNullOrEmpty(no_contrato))
                    {
                        contrato_gestion_gl = gestionGlContext.C_CONTRATOS.Where(x => x.CONTRATO == no_contrato).FirstOrDefault();
                    }
                    
                    instalacion_destino = gestionGlContext.C_M_INSTALACIONES.Where(x => x.ID_C_M_INSTALACION == id_instalacion_destino).SingleOrDefault();
                }
                

                if (instalacion_destino != null)
                {
                    using (STPIntegral.Datos.STPIntegralModel stpIntegralContext = new Datos.STPIntegralModel())
                    {
                        resultado = ObtenerCapacidadHospedaje(instalacion_destino, fecha_subida, fecha_bajada, contrato_gestion_gl, folio_especial, subdivision_autorizador, stpIntegralContext);
                    }
                }
            }

            return resultado;
        }

        private static int ObtenerCapacidadHospedaje(GestionGL.Datos.C_M_INSTALACIONES instalacion_destino, DateTime fecha_subida, DateTime fecha_bajada, GestionGL.Datos.C_CONTRATOS contrato_gestion_gl, string folio_especial, string subdivision_autorizador, STPIntegral.Datos.STPIntegralModel context_stpIntegral)
        {
            int resultado = 0;

            bool hay_disponibilidad_hospedaje = false;

            //obtener las capacidades para todo el rango de fechas en la instalacion destino.
            var listado_reservaciones_hospedaje = context_stpIntegral.VW_TabReservacionesCapacidadHospedaje_NOLOCK.Where(x => x.FkIdInstalacion == instalacion_destino.ID_C_M_INSTALACION && (x.FechaCapacidad >= fecha_subida && x.FechaCapacidad <= fecha_bajada)).ToList();

            //ahora hay que verificar la disponibilidad para cada uno de los dias en que esta programada la estancia.
            DateTime dia_verificacion = fecha_subida.Date;

            List<STPIntegral.Datos.VW_TabReservacionesCapacidadHospedaje_NOLOCK> listado_disponibilidad_por_dia = null;
            List<STPIntegral.Datos.VW_TabReservacionesCapacidadHospedaje_NOLOCK> consulta_disponibilidad = null;

            int CapacidadDisponiblePorDia = 0;
            int cantidad_disponible = 0;

            do
            {
                //Se inicializa a false la disponibilidad del dia
                hay_disponibilidad_hospedaje = false;

                CapacidadDisponiblePorDia = 0;

                //Verificar disponibilidad para cada dia
                //Primero se verifica si la cosulta regreso alguna reservacion para ese dia y esa instalacion, en caso contrario se decreta que no hay disponibilidad
                listado_disponibilidad_por_dia = (from x in listado_reservaciones_hospedaje where x.FechaCapacidad == dia_verificacion && x.FkIdInstalacion == instalacion_destino.ID_C_M_INSTALACION select x).ToList(); //esta funcion regresa nulo si no encuentra nada para ese dia.

                if (listado_disponibilidad_por_dia == null || listado_disponibilidad_por_dia.Count == 0)
                {
                    //No hay reservacion para este dia.
                    resultado = 0;
                    break;
                }

                //Si la consulta regreso algun resultado entonces primero se verifica que haya disponibilidad para el folio especial (si es que lo proprocionaron)
                if (folio_especial != "")
                {
                    consulta_disponibilidad = (from x in listado_disponibilidad_por_dia where x.TipoReservacion == "FOLIO ESPECIAL" && x.FolioEspecial == folio_especial select x).ToList();

                    cantidad_disponible = (from x in consulta_disponibilidad select x.TotalDisponibles.Value).Sum();
                    CapacidadDisponiblePorDia += cantidad_disponible;
                    hay_disponibilidad_hospedaje = true;
                }

                //luego se verifica que haya disponibilidad para el contrato (si es que lo proprocionaron)
                if (contrato_gestion_gl != null && contrato_gestion_gl.CONTRATO != "" /*&& hay_disponibilidad_hospedaje == false*/)
                {
                    consulta_disponibilidad = (from x in listado_disponibilidad_por_dia where x.TipoReservacion == "CONTRATO" && x.CONTRATO == contrato_gestion_gl.CONTRATO select x).ToList();

                    cantidad_disponible = (from x in consulta_disponibilidad select x.TotalDisponibles.Value).Sum();
                    CapacidadDisponiblePorDia += cantidad_disponible;
                    hay_disponibilidad_hospedaje = true;
                }

                //Luego validamos que exista disponibilidad a nivel Subdivision en caso de que con la validacion anterior no haya habido
                if (!String.IsNullOrEmpty(subdivision_autorizador))
                {
                    consulta_disponibilidad = (from x in listado_disponibilidad_por_dia where x.TipoReservacion == "SUBDIVISION" && x.SUBDIVISION.StartsWith(subdivision_autorizador.Substring(0,3)) select x).ToList();

                    cantidad_disponible = (from x in consulta_disponibilidad select x.TotalDisponibles.Value).Sum();
                    CapacidadDisponiblePorDia += cantidad_disponible;
                    hay_disponibilidad_hospedaje = true;
                }

                //Por ultimo validamos que exista disponibilidad a nivel FONDO COMUN en caso de que con la validacion anterior no haya habido
                
                consulta_disponibilidad = (from x in listado_disponibilidad_por_dia where x.TipoReservacion == "FONDO COMUN" select x).ToList();

                cantidad_disponible = (from x in consulta_disponibilidad select x.TotalDisponibles.Value).Sum();
                CapacidadDisponiblePorDia += cantidad_disponible;
                hay_disponibilidad_hospedaje = true;

                if ((CapacidadDisponiblePorDia < resultado) || resultado == 0)
                {
                    resultado = CapacidadDisponiblePorDia;
                }

                //Si no hay disponibilidad se muestra un mensaje de error.
                if (!hay_disponibilidad_hospedaje)
                {
                    resultado = 0;
                    break;
                }
                else
                {
                    dia_verificacion = dia_verificacion.AddDays(1);
                }
            }
            while (dia_verificacion <= fecha_bajada && hay_disponibilidad_hospedaje == true);

            return resultado;
        }

        public static int ObtenerCapacidadFolioEspecialHospedaje(int id_instalacion_destino, DateTime fecha_subida, DateTime fecha_bajada, string folio_especial)
        {
            int resultado = 0;

            if (id_instalacion_destino > 0)
            {
                GestionGL.Datos.C_M_INSTALACIONES instalacion_destino = null;


                using (GestionGL.Datos.GestionGLModel gestionGlContext = new GestionGL.Datos.GestionGLModel())
                {
                    instalacion_destino = gestionGlContext.C_M_INSTALACIONES.Where(x => x.ID_C_M_INSTALACION == id_instalacion_destino).SingleOrDefault();
                }


                if (instalacion_destino != null)
                {
                    using (STPIntegral.Datos.STPIntegralModel stpIntegralContext = new Datos.STPIntegralModel())
                    {
                        resultado = ObtenerCapacidadFolioEspecialHospedaje(instalacion_destino, fecha_subida, fecha_bajada, folio_especial, stpIntegralContext);
                    }
                }
            }

            return resultado;
        }

        private static int ObtenerCapacidadFolioEspecialHospedaje(GestionGL.Datos.C_M_INSTALACIONES instalacion_destino, DateTime fecha_subida, DateTime fecha_bajada, string folio_especial, STPIntegral.Datos.STPIntegralModel context_stpIntegral)
        {
            int resultado = 0;

            bool hay_disponibilidad_hospedaje = false;

            //obtener las capacidades para todo el rango de fechas en la instalacion destino.
            var listado_reservaciones_hospedaje = context_stpIntegral.VW_TabReservacionesCapacidadHospedaje_NOLOCK.Where(x => x.FkIdInstalacion == instalacion_destino.ID_C_M_INSTALACION && (x.FechaCapacidad >= fecha_subida && x.FechaCapacidad <= fecha_bajada)).ToList();

            //ahora hay que verificar la disponibilidad para cada uno de los dias en que esta programada la estancia.
            DateTime dia_verificacion = fecha_subida.Date;

            List<STPIntegral.Datos.VW_TabReservacionesCapacidadHospedaje_NOLOCK> listado_disponibilidad_por_dia = null;
            List<STPIntegral.Datos.VW_TabReservacionesCapacidadHospedaje_NOLOCK> consulta_disponibilidad = null;

            int CapacidadDisponiblePorDia = 0;
            int cantidad_disponible = 0;

            do
            {
                //Se inicializa a false la disponibilidad del dia
                hay_disponibilidad_hospedaje = false;

                CapacidadDisponiblePorDia = 0;

                //Verificar disponibilidad para cada dia
                //Primero se verifica si la cosulta regreso alguna reservacion para ese dia y esa instalacion, en caso contrario se decreta que no hay disponibilidad
                listado_disponibilidad_por_dia = (from x in listado_reservaciones_hospedaje where x.FechaCapacidad == dia_verificacion && x.FkIdInstalacion == instalacion_destino.ID_C_M_INSTALACION select x).ToList(); //esta funcion regresa nulo si no encuentra nada para ese dia.

                if (listado_disponibilidad_por_dia == null || listado_disponibilidad_por_dia.Count == 0)
                {
                    //No hay reservacion para este dia.
                    resultado = 0;
                    break;
                }

                //Si la consulta regreso algun resultado entonces primero se verifica que haya disponibilidad para el folio especial (si es que lo proprocionaron)
                if (folio_especial != "")
                {
                    consulta_disponibilidad = (from x in listado_disponibilidad_por_dia where x.TipoReservacion == "FOLIO ESPECIAL" && x.FolioEspecial == folio_especial select x).ToList();

                    cantidad_disponible = (from x in consulta_disponibilidad select x.TotalDisponibles.Value).Sum();
                    CapacidadDisponiblePorDia += cantidad_disponible;
                    hay_disponibilidad_hospedaje = true;
                }

                hay_disponibilidad_hospedaje = true;

                if ((CapacidadDisponiblePorDia < resultado) || resultado == 0)
                {
                    resultado = CapacidadDisponiblePorDia;
                }

                //Si no hay disponibilidad se muestra un mensaje de error.
                if (!hay_disponibilidad_hospedaje)
                {
                    resultado = 0;
                    break;
                }
                else
                {
                    dia_verificacion = dia_verificacion.AddDays(1);
                }
            }
            while (dia_verificacion <= fecha_bajada && hay_disponibilidad_hospedaje == true);

            return resultado;
        }

        //AUDITAR
        public static SolicitudIntegralModel AgregarDetalleSolicitudIntegral(STPIntegral.Datos.TabSolicitudIntegral solicitud_integral, IEnumerable<STPIntegral.Datos.TabDetallesSolicitudIntegral> _detalles_solicitud_integral, STPIntegral.Datos.TabAuditoriaSolicitudIntegral auditoria, string folio_especial_transporte_ida = "", string folio_especial_transporte_retorno = "", string folio_especial_hospedaje = "")
        {
            //tipo_solicitud SOLO HOSPEDAJE, SOLO TRANSPORTE, HOSPEDAJE Y TRANSPORTE
            //tipo_personal: PEMEX, COMPAÑIA, COMODATARIO
            //tipo_transporte: AEREO, MARITIMO
            //tipo_servicio: CAMBIO DE GUARDIA, REGULARES, LATERALES, DISPOSICION

            SolicitudIntegralModel respuesta = null;

            //Contextos para obtener acceso a la base de datos.
            GestionGL.Datos.GestionGLModel gestionGL_context = new GestionGL.Datos.GestionGLModel();
            STPIntegral.Datos.STPIntegralModel stpIntegral_contex = new Datos.STPIntegralModel();
            SCHAP.Datos.SCHAPModel schap_context = new SCHAP.Datos.SCHAPModel();

            gestionGL_context.Configuration.LazyLoadingEnabled = false;
            stpIntegral_contex.Configuration.LazyLoadingEnabled = false;
            schap_context.Configuration.LazyLoadingEnabled = false;

            gestionGL_context.Configuration.ProxyCreationEnabled = false;
            stpIntegral_contex.Configuration.ProxyCreationEnabled = false;
            schap_context.Configuration.ProxyCreationEnabled = false;

            //SI la solicitud requiere retorno entonces hay que agregar a los pasajeros como retorno tambien.

            List<STPIntegral.Datos.TabDetallesSolicitudIntegral> detalles_solicitud_integral = new List<Datos.TabDetallesSolicitudIntegral>(_detalles_solicitud_integral);

            if (solicitud_integral.RequiereRetorno == true)
            {
                List<STPIntegral.Datos.TabDetallesSolicitudIntegral> detalles_retorno = new List<Datos.TabDetallesSolicitudIntegral>();

                STPIntegral.Datos.TabDetallesSolicitudIntegral detalle_retorno = null;

                foreach (STPIntegral.Datos.TabDetallesSolicitudIntegral detalle_solicitud in detalles_solicitud_integral)
                {
                    detalle_retorno = new Datos.TabDetallesSolicitudIntegral();

                    detalle_retorno.CentroGestorDireccionamientoAH = detalle_solicitud.CentroGestorDireccionamientoAH;
                    detalle_retorno.CentroGestorDireccionamientoSTP = detalle_solicitud.CentroGestorDireccionamientoSTP;
                    detalle_retorno.ClaveInstalacionDestino = detalle_solicitud.ClaveInstalacionOrigen;
                    detalle_retorno.ClaveInstalacionOrigen = detalle_solicitud.ClaveInstalacionDestino; 
                    detalle_retorno.CuentaMayorDireccionamientoAH = detalle_solicitud.CuentaMayorDireccionamientoAH;
                    detalle_retorno.CuentaMayorDireccionamientoSTP = detalle_solicitud.CuentaMayorDireccionamientoSTP;
                    detalle_retorno.ElementoPEPDireccionamiento = detalle_solicitud.ElementoPEPDireccionamiento;
                    detalle_retorno.ElementoPEPDireccionamientoSTP = detalle_solicitud.ElementoPEPDireccionamientoSTP;
                    detalle_retorno.EstadoConfirmaSTP = detalle_solicitud.EstadoConfirmaSTP;
                    detalle_retorno.EstadoHuespedAH= detalle_solicitud.EstadoHuespedAH;
                    detalle_retorno.EstadoOcupanteSolicitudIntegral = detalle_solicitud.EstadoOcupanteSolicitudIntegral;
                    detalle_retorno.EstadoPasajeroSTP = detalle_solicitud.EstadoPasajeroSTP;
                    detalle_retorno.FechaBajada= detalle_solicitud.FechaBajada;
                    detalle_retorno.FechaSubida = detalle_solicitud.FechaSubida;
                    detalle_retorno.FichaRFCEmpleado = detalle_solicitud.FichaRFCEmpleado;
                    detalle_retorno.fk_contrato = detalle_solicitud.fk_contrato;
                    detalle_retorno.fk_itinerario = solicitud_integral.FkIdItinerarioTransporteRetorno;
                    detalle_retorno.FkIdTransporteStp = detalle_solicitud.FkIdTransporteStp;
                    detalle_retorno.FolioLibretaMar = detalle_solicitud.FolioLibretaMar;
                    detalle_retorno.FondoDireccionamientoAH = detalle_solicitud.FondoDireccionamientoAH;
                    detalle_retorno.FondoDireccionamientoSTP = detalle_solicitud.FondoDireccionamientoSTP;
                    detalle_retorno.IdInstalacionDestino = detalle_solicitud.IdInstalacionOrigen;
                    detalle_retorno.IdInstalacionOrigen = detalle_solicitud.IdInstalacionDestino; 
                    detalle_retorno.IdPasajeroCiaPEMEX = detalle_solicitud.IdPasajeroCiaPEMEX;
                    detalle_retorno.LLevaMaterial = detalle_solicitud.LLevaMaterial;
                    detalle_retorno.NombreEmpleado = detalle_solicitud.NombreEmpleado;
                    detalle_retorno.NombreInstalacionDestino = detalle_solicitud.NombreInstalacionOrigen;
                    detalle_retorno.NombreInstalacionOrigen = detalle_solicitud.NombreInstalacionDestino;
                    detalle_retorno.NombreTransporteSTP = detalle_solicitud.NombreTransporteSTP;
                    detalle_retorno.NumeroViajeSTP = detalle_solicitud.NumeroViajeSTP;
                    detalle_retorno.ObservacionesDetalle = detalle_solicitud.ObservacionesDetalle;
                    detalle_retorno.PosicionPresupuestalDireccionamientoAH = detalle_solicitud.PosicionPresupuestalDireccionamientoAH;
                    detalle_retorno.PosicionPresupuestalDireccionamientoSTP = detalle_solicitud.PosicionPresupuestalDireccionamientoSTP;
                    detalle_retorno.ProgramaPresupuestalDireccionamientoAH = detalle_solicitud.ProgramaPresupuestalDireccionamientoAH;
                    detalle_retorno.ProgramaPresupuestalDireccionamientoSTP = detalle_solicitud.ProgramaPresupuestalDireccionamientoSTP;
                    detalle_retorno.StatusPaxSTP = detalle_solicitud.StatusPaxSTP;
                    detalle_retorno.TipoMovimientoPersonal = "RETORNO";
                    detalle_retorno.TipoPersonal = detalle_solicitud.TipoPersonal;
                    detalle_retorno.VigenciaLibretaMar = detalle_solicitud.VigenciaLibretaMar;

                    detalles_retorno.Add(detalle_retorno);
                }

                foreach(STPIntegral.Datos.TabDetallesSolicitudIntegral detalle_solicitud in detalles_retorno)
                {
                    detalles_solicitud_integral.Add(detalle_solicitud);
                }
            }


            AplicarValidacionesSolicitudIntegral(solicitud_integral, detalles_solicitud_integral, gestionGL_context, stpIntegral_contex, schap_context, true);

            //Obtenemos variables de utileria para las validaciones posteriores

            GestionGL.Datos.C_CONTRATOS contrato_gestion_gl = null;
            GestionGL.Datos.C_ACREEDORES acreedor_contrato_gestion_gl = null;

            if (solicitud_integral.TipoPersonal != "PEMEX")
            {
                if (!String.IsNullOrEmpty(solicitud_integral.NumeroContrato.Trim()))
                {
                    contrato_gestion_gl = gestionGL_context.C_CONTRATOS.Where(x => x.CONTRATO == solicitud_integral.NumeroContrato).SingleOrDefault();

                    if (contrato_gestion_gl != null)
                    {
                        acreedor_contrato_gestion_gl = gestionGL_context.C_ACREEDORES.Where(x => x.ID_C_ACREEDOR == contrato_gestion_gl.ID_C_ACREEDOR).SingleOrDefault();
                    }
                }
            }
            else
            {
                solicitud_integral.NumeroContrato = "";
                solicitud_integral.ContratoVisita = false;
                solicitud_integral.NombreCompania = "";
            }

            //Obtenemos el listado de las distintas instalaciones origen destino

            List<int> listado_ids_instalaciones = (from x in detalles_solicitud_integral select x.IdInstalacionDestino).Distinct().ToList();
            listado_ids_instalaciones.AddRange((from x in detalles_solicitud_integral select x.IdInstalacionOrigen).Distinct().ToList());
            listado_ids_instalaciones = listado_ids_instalaciones.Distinct().ToList();


            List<GestionGL.Datos.C_M_INSTALACIONES> instalaciones_origenes_destinos = (from x in gestionGL_context.C_M_INSTALACIONES
                                                                                       where (listado_ids_instalaciones).Contains(x.ID_C_M_INSTALACION)
                                                                                       select x).ToList();

            GestionGL.Datos.C_M_INSTALACIONES instalacion_destino = null;

            //En este punto ya estan todas las validaciones que no requieren una transaccion, entonces hacemos dispose a todas las conecciones.
            stpIntegral_contex.Dispose();
            gestionGL_context.Dispose();
            schap_context.Dispose();

            int capacidad_disponible_solictada_transporte_hospedaje = 1;

            DateTimeJavaScriptConverter dtjs = new DateTimeJavaScriptConverter();
            JavaScriptSerializer js = new JavaScriptSerializer(); 


            //INICIAN VALIDACIONES TRANSACCIONADAS
            using (TransactionScope transaction_scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = IsolationLevel.Serializable }))
            {
                try
                {
                    //Como toda la parte transaccional quedo solo en STPIntegral, entonces se crea un nuevo contexto para que todo esto se transaccione.
                    using (stpIntegral_contex = new Datos.STPIntegralModel())
                    {

                        DateTime fecha_hora_salida_transporte = System.DateTime.Now;

                        //Aplicando validacion para el horario de salida en caso de que la solicitud sea de disposicion
                        if (solicitud_integral.TipoSolicitudIntegral != "SOLO HOSPEDAJE")
                        {
                            if (solicitud_integral.TipoServicio == "DISPOSICION")
                            {
                                if (!solicitud_integral.HoraSalidaItinerarioTransporte.HasValue)
                                {
                                    throw new ShowCaseException(String.Format("ERR-SI-0067: Cuando una solicitud integral incluye transporte y ademas es un servicio a disposicion es necesario indicar un horario de salida"), null, TypeMessage.error, "ERR-SI-0067");
                                }

                                fecha_hora_salida_transporte = new DateTime(solicitud_integral.FechaSubida.Year, solicitud_integral.FechaSubida.Month, solicitud_integral.FechaSubida.Day, solicitud_integral.HoraSalidaItinerarioTransporte.Value.Hour, solicitud_integral.HoraSalidaItinerarioTransporte.Value.Minute, 0);
                            }
                            else
                            {
                                fecha_hora_salida_transporte = solicitud_integral.FechaSubida;
                            }
                        }
                        
                        
                        if (solicitud_integral.IdSolicitudIntegral <= 0)
                        {
                            //si se confirma la capacidad entonces se procede a insertar la solicitud integral nueva si hace falta. (previa generacion transaccionada del folio de solicitud).
                            solicitud_integral.FolioSolicitudIntegral = ObtenerNuevoFolioSolicitudIntegral(stpIntegral_contex, System.DateTime.Now.Date);

                            //Agregando la solicitud integral nueva.
                            solicitud_integral.ContadorAnexo = 0;
                            solicitud_integral.ContratoVisita = false;
                            solicitud_integral.FKidOrigenSolicitud = detalles_solicitud_integral.First().IdInstalacionOrigen;
                            solicitud_integral.FKidDestinoSolicitud = detalles_solicitud_integral.First().IdInstalacionDestino;
                            solicitud_integral.EstadoSolicitudIntegral = "SOLICITADO";
                            solicitud_integral.EstadoVistoBuenoAH = "PENDIENTE";
                            solicitud_integral.FechaUltimaActualizacion = System.DateTime.Now;
                            solicitud_integral.HabilitarCapturaAnexos = false;

                            solicitud_integral.LetraUltimoAnexo = " ";
                            solicitud_integral.NodoTransporte = " ";
                            
                            solicitud_integral.ObservacionesLogistica = " ";
                            solicitud_integral.ObservacionesSolicitud = " ";
                            solicitud_integral.ObservacionesVentanilla = " ";
                            
                            solicitud_integral.SolicitudResguardada = false;

                            solicitud_integral.HoraSalidaItinerarioTransporte = fecha_hora_salida_transporte;
                            
                            solicitud_integral.FechaCreacion = System.DateTime.Now;
                            solicitud_integral.FechaConfirmacion = System.DateTime.Now;
                            solicitud_integral.FechaViaje = solicitud_integral.FechaSubida;

                            solicitud_integral.NombreCompania = solicitud_integral.TipoPersonal == "PEMEX" ? "PEMEX" : acreedor_contrato_gestion_gl.DESCRIPCION_ACREEDOR;

                            stpIntegral_contex.TabSolicitudIntegral.Add(solicitud_integral);
                            stpIntegral_contex.SaveChanges();

                            //AUDITAR
                            auditoria.FkIdSolicitudIntegral = solicitud_integral.IdSolicitudIntegral;
                            auditoria.TipoMovimientoAuditoria = "INSERTA SOLICITUD";
                            auditoria.DescripcionAuditoria = "GUARDA UNA NUEVA SOLICITUD CON FOLIO: " + solicitud_integral.FolioSolicitudIntegral.ToString();
                            auditoria.ObjetoAntesModificacion = "";
                            auditoria.ObjetoDespuesModificacion = "";//js.Serialize(solicitud_integral);
                            AgregarAuditoria(stpIntegral_contex,auditoria);
                        }


                        //Agregar DEtalles Pasajeros.-
                        foreach (STPIntegral.Datos.TabDetallesSolicitudIntegral detalle_solicitud_parametro in detalles_solicitud_integral)
                        {
                            //primero se verifica que haya disponibilidad de capacidad tanto en transporte como en hospedaje (lo que aplique).
                            if (solicitud_integral.TipoServicio == "CAMBIO DE GUARDIA")
                            { 
                                detalle_solicitud_parametro.fk_itinerario = detalle_solicitud_parametro.TipoMovimientoPersonal == "SUBIDA" ? solicitud_integral.FkIdItinerarioTransporteIda : solicitud_integral.FkIdItinerarioTransporteRetorno;
                            }

                            instalacion_destino = instalaciones_origenes_destinos.Where(x => x.ID_C_M_INSTALACION == detalle_solicitud_parametro.IdInstalacionDestino).FirstOrDefault();

                            //Si es una emergencia, no valida los lugares disponibles.
                            if (!(solicitud_integral.TipoSolicitudIntegral == "SOLO TRANSPORTE" && solicitud_integral.TipoServicio == "EMERGENCIA"))
                                ValidarCapacidadHospedajeTransporte(instalacion_destino, contrato_gestion_gl, solicitud_integral.TipoSolicitudIntegral, solicitud_integral.TipoServicio, detalle_solicitud_parametro.TipoMovimientoPersonal, detalle_solicitud_parametro.FechaSubida, detalle_solicitud_parametro.FechaBajada, detalle_solicitud_parametro.fk_itinerario, detalle_solicitud_parametro.TipoMovimientoPersonal == "SUBIDA" ? folio_especial_transporte_ida : folio_especial_transporte_retorno, folio_especial_hospedaje, solicitud_integral.SubdivisionAutorizador, capacidad_disponible_solictada_transporte_hospedaje, stpIntegral_contex);

                            detalle_solicitud_parametro.IdSolicitudIntegral = solicitud_integral.IdSolicitudIntegral;

                            detalle_solicitud_parametro.EstadoHuespedAH = "SOLICITADO";
                            detalle_solicitud_parametro.EstadoOcupanteSolicitudIntegral = "SOLICITADO";
                            detalle_solicitud_parametro.EstadoPasajeroSTP = "SOLICITADO";
                            detalle_solicitud_parametro.FolioLibretaMar = "";
                            detalle_solicitud_parametro.VigenciaLibretaMar = null;
                            detalle_solicitud_parametro.ObservacionesDetalle = "";

                            detalle_solicitud_parametro.fk_contrato = (solicitud_integral.TipoPersonal == "PEMEX") || (solicitud_integral.TipoPersonal != "PEMEX" && contrato_gestion_gl == null)  ? (int?)null : contrato_gestion_gl.ID_C_CONTRATO;

                            //Aqui falta validar si el itinerario es de subida o bajada para cambiar la hora segun corresponda.
                            if (solicitud_integral.TipoServicio == "DISPOSICION")
                            {
                                detalle_solicitud_parametro.HoraViajeItemParajeroSTA = fecha_hora_salida_transporte;
                            }
                            else
                            {
                                if (detalle_solicitud_parametro.fk_itinerario.HasValue)
                                {
                                    detalle_solicitud_parametro.HoraViajeItemParajeroSTA = ObtenerHorarioSalidaItinerario(detalle_solicitud_parametro.fk_itinerario.Value, solicitud_integral.FechaSubida, detalle_solicitud_parametro.IdInstalacionDestino, stpIntegral_contex);
                                }
                                else
                                {
                                    detalle_solicitud_parametro.HoraViajeItemParajeroSTA = solicitud_integral.HoraSalidaItinerarioTransporte;
                                }
                            }

                            // Si no es una emergencia, busca los direccionamientos de subida. En caso contrario, guarda los especificados.
                            if (!(solicitud_integral.TipoSolicitudIntegral == "SOLO TRANSPORTE" && solicitud_integral.TipoServicio == "EMERGENCIA"))
                            {
                                //AQUI VA LA VALIDACION DE LOS 20 DIAS para ATRAS
                                if (solicitud_integral.TipoServicio != "SOLO HOSPEDAJE" && (solicitud_integral.TipoServicio == "LATERALES" || solicitud_integral.TipoServicio == "REGULARES"))
                                {
                                    var db = stpIntegral_contex;

                                    var dias = fecha_hora_salida_transporte.AddDays(-20);

                                    // Se busca el detalle de subida del pasajero con un máximo de 20 días de antigüedad.
                                    var detalle_direccionamientos = (from x in db.TabDetallesSolicitudIntegral
                                                                     join y in db.TabSolicitudIntegral on x.IdSolicitudIntegral equals y.IdSolicitudIntegral
                                                                     join z in db.stp_c_itinerario_base on x.fk_itinerario equals z.id_itinerario
                                                                     where ((solicitud_integral.TipoServicio == "LATERALES" && z.fk_nodo_origen != 3 && z.fk_nodo_destino == 3) ||
                                                                                 (solicitud_integral.TipoServicio == "REGULARES" && z.fk_nodo_origen == 3))
                                                                             && x.FichaRFCEmpleado == detalle_solicitud_parametro.FichaRFCEmpleado
                                                                             && y.FechaSubida >= dias && x.EstadoPasajeroSTP == "ABORDO"
                                                                     orderby y.HoraSalidaItinerarioTransporte descending
                                                                     select new
                                                                     {
                                                                         y.FolioSolicitudIntegral,
                                                                         x.CentroGestorDireccionamientoSTP,
                                                                         x.CuentaMayorDireccionamientoSTP,
                                                                         x.ElementoPEPDireccionamientoSTP,
                                                                         x.FondoDireccionamientoSTP,
                                                                         x.PosicionPresupuestalDireccionamientoSTP,
                                                                         x.ProgramaPresupuestalDireccionamientoSTP
                                                                     }).FirstOrDefault();

                                    if (detalle_direccionamientos == null)
                                    {
                                        // Si no se encuentra el detalle y actualmente no se tienen direccionamientos por asignar, se lanza una excepción.
                                        if (string.IsNullOrEmpty(detalle_solicitud_parametro.ElementoPEPDireccionamientoSTP))
                                            throw new ShowCaseException(String.Format("ERR-SI-0079: Ocurrio el siguiente error al intentar agregar un detalle a la Solicitud Integral [{0}]: '{1}'", solicitud_integral.FolioSolicitudIntegral, "Este pasajero no cuenta con direccionamientos de origen."), null, TypeMessage.error, "ERR-SI-0079");
                                    }
                                    else
                                    {
                                        // Si se encuentran los direccionamientos, se reemplazan los actuales.
                                        detalle_solicitud_parametro.CentroGestorDireccionamientoSTP = detalle_direccionamientos.CentroGestorDireccionamientoSTP;
                                        detalle_solicitud_parametro.CuentaMayorDireccionamientoSTP = detalle_direccionamientos.CuentaMayorDireccionamientoSTP;
                                        detalle_solicitud_parametro.ElementoPEPDireccionamientoSTP = detalle_direccionamientos.ElementoPEPDireccionamientoSTP;
                                        detalle_solicitud_parametro.FondoDireccionamientoSTP = detalle_direccionamientos.FondoDireccionamientoSTP;
                                        detalle_solicitud_parametro.PosicionPresupuestalDireccionamientoSTP = detalle_direccionamientos.PosicionPresupuestalDireccionamientoSTP;
                                        detalle_solicitud_parametro.ProgramaPresupuestalDireccionamientoSTP = detalle_direccionamientos.ProgramaPresupuestalDireccionamientoSTP;
                                    }
                                }
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(detalle_solicitud_parametro.ElementoPEPDireccionamientoSTP))
                                    throw new ShowCaseException(String.Format("ERR-SI-0080: Ocurrio el siguiente error al intentar agregar un detalle a la Solicitud Integral [{0}]: '{1}'", solicitud_integral.FolioSolicitudIntegral, "No fueron especificados direccionamientos para este pasajero."), null, TypeMessage.error, "ERR-SI-0080");
                            }

                            //Agregando el detalle a la solicitud integral.
                            stpIntegral_contex.TabDetallesSolicitudIntegral.Add(detalle_solicitud_parametro);
                            
                            stpIntegral_contex.SaveChanges();
                            //AUDITAR
                            auditoria.TipoMovimientoAuditoria = "INSERTA DETALLE";
                            auditoria.DescripcionAuditoria = "GUARDA EL PASAJERO/HUESPED CON FICHA/RFC: " + detalle_solicitud_parametro.NombreEmpleado +  " EN EL DETALLE DEL FOLIO: " + solicitud_integral.FolioSolicitudIntegral.ToString();
                            auditoria.FkIdSolicitudIntegral = solicitud_integral.IdSolicitudIntegral;
                            auditoria.ObjetoAntesModificacion = "";
                            //auditoria.ObjetoDespuesModificacion = "hola";
                            auditoria.ObjetoDespuesModificacion = "";//js.Serialize(detalle_solicitud_parametro);
                            AgregarAuditoria(stpIntegral_contex, auditoria);


                            //Como ya se agrego un detalle entonces hay que quitar un espacio en la reservacion de transporte y/o hospedaje si aplica.

                            //Si es una emergencia, se omite esta validación.
                            if (!(solicitud_integral.TipoSolicitudIntegral == "SOLO TRANSPORTE" && solicitud_integral.TipoServicio == "EMERGENCIA"))
                            {
                                //Empezamos con transporte, el orden de prioridad es el siguiente, Primero se validan los folios especiales, luego los contratos, seguido de los centros gestores, por ultimo los espacios libres totales.
                                AgregarEliminarEspacioReservacionSTPAH(stpIntegral_contex, solicitud_integral, detalle_solicitud_parametro, contrato_gestion_gl, folio_especial_transporte_ida, folio_especial_transporte_retorno, folio_especial_hospedaje, instalacion_destino, 1, false, auditoria);
                            }

                            

                        }//fin foreach(STPIntegral.Datos.TabDetallesSolicitudIntegral detalle_solicitud_parametro in detalles_solicitud_integral)
                    }
                    
                    ////stpIntegral_contex.SaveChanges();

                    //   // si todo sale bien se cierra la transaccion, en caso contrario se aborta toda la operacion.
                    transaction_scope.Complete();
                }
                catch (ShowCaseException ex)
                {
                    throw ex;
                }
                catch (System.Exception ex)
                {
                    throw new ShowCaseException(String.Format("ERR-SI-0037: Ocurrio el siguiente error al intentar agregar un detalle a la Solicitud Integral [{0}]: '{1}'", solicitud_integral.FolioSolicitudIntegral, ex.Message), ex, TypeMessage.error, "ERR-SI-0037");
                }
            }

            //Cuando todo esto esta listo entonces se procede a abrir una transaccion.
            //{
            //   //primero se verifica que haya disponibilidad de capacidad tanto en transporte como en hospedaje (lo que aplique).
            //   //si se confirma la capacidad entonces se procede a insertar la solicitud integral nueva si hace falta. (previa generacion transaccionada del folio de solicitud).
            //   //luego se inserta el detalle de la solicitud integral con el nuevo id generado.
            //   //por ultimo se actualizan las capacidades de reservacion en transporte y hospedaje segun aplique.
            //   // si todo sale bien se cierra la transaccion, en caso contrario se aborta toda la operacion.
            //}

            respuesta = new SolicitudIntegralModel();

            respuesta.SolicitudIntegral = solicitud_integral;

            using (STPIntegral.Datos.STPIntegralModel stpIntegralContext = new Datos.STPIntegralModel())
            {
                stpIntegralContext.Configuration.LazyLoadingEnabled = false;
                stpIntegralContext.Configuration.ProxyCreationEnabled = false;

                respuesta.DetallesSolicitudIntegral = stpIntegralContext.TabDetallesSolicitudIntegral.Where(x => x.IdSolicitudIntegral == solicitud_integral.IdSolicitudIntegral).ToList();
            }

            respuesta.ContratoSap = null;
            respuesta.InstalacionesDestino = null;
            respuesta.InstalacionesOrigen = null;
            respuesta.ItinerariosTransporte = null;
            respuesta.ListadoDireccionamientosDestinos = null;

            return respuesta;
        }

        //AUDITAR
        public static List<STPIntegral.Datos.TabDetallesSolicitudIntegral> ReemplazarPasajeroSolicitudIntegral(int id_detalle_solicitud_integral, string nueva_ficha_rfc, STPIntegral.Datos.TabAuditoriaSolicitudIntegral auditoria)
        {
            //Contextos para obtener acceso a la base de datos.
            List<STPIntegral.Datos.TabDetallesSolicitudIntegral> resultado = new List<Datos.TabDetallesSolicitudIntegral>();

            GestionGL.Datos.GestionGLModel gestionGL_context = new GestionGL.Datos.GestionGLModel();
            STPIntegral.Datos.STPIntegralModel stpIntegral_contex = new Datos.STPIntegralModel();
            SCHAP.Datos.SCHAPModel schap_context = new SCHAP.Datos.SCHAPModel();

            gestionGL_context.Configuration.LazyLoadingEnabled = false;
            stpIntegral_contex.Configuration.LazyLoadingEnabled = false;
            schap_context.Configuration.LazyLoadingEnabled = false;

            gestionGL_context.Configuration.ProxyCreationEnabled = false;
            stpIntegral_contex.Configuration.ProxyCreationEnabled = false;
            schap_context.Configuration.ProxyCreationEnabled = false;

           //PARA LA AUDITORIA
            JavaScriptSerializer js = new JavaScriptSerializer();
            
            string ficha_ant;
            string nombre_ant;
            
            STPIntegral.Datos.TabDetallesSolicitudIntegral detalle_solicitud_integral = stpIntegral_contex.TabDetallesSolicitudIntegral.SingleOrDefault(x => x.IdDetalleSolicitudIntegral == id_detalle_solicitud_integral);
            
            ficha_ant = detalle_solicitud_integral.FichaRFCEmpleado;
            nombre_ant = detalle_solicitud_integral.NombreEmpleado;


            if (detalle_solicitud_integral == null)
            {
                throw new ShowCaseException(String.Format("ERR-SI-0069: No se encontro el detalle con id: '{0}' de la solicitud integral para el cambio de pasajero.", id_detalle_solicitud_integral), null, TypeMessage.error, "ERR-SI-0069");
            }

            //Validando el estado del pasajero
            if (detalle_solicitud_integral.EstadoOcupanteSolicitudIntegral != EstadosSolicitudIntegral.SOLICITADO.ToString())
            {
                throw new ShowCaseException(String.Format("ERR-SI-0070: No se puede cambiar a un pasajero cuando cuando su estado es diferente a 'SOLICITADO'", id_detalle_solicitud_integral), null, TypeMessage.error, "ERR-SI-0070");
            }
            
            //Validando la ficha RFC del personal
            string nuevo_nombre = "";

            if (detalle_solicitud_integral.TipoPersonal == "PEMEX")
            {
                GestionGL.Datos.C_EMPLEADOS_PEMEX empleado_pemex = gestionGL_context.C_EMPLEADOS_PEMEX.FirstOrDefault(x => x.FICHA == nueva_ficha_rfc);

                if (empleado_pemex == null)
                {
                    throw new ShowCaseException(String.Format("ERR-SI-0071: No se encontro al trabajador de pemex con ficha: '{0}'", nueva_ficha_rfc), null, TypeMessage.error, "ERR-SI-0071");
                }
                else
                {
                    nuevo_nombre = (empleado_pemex.NOMBRES + " " + empleado_pemex.APELLIDOS).Trim();
                }
            }
            else
            {
                GestionGL.Datos.C_EMPLEADOS_CIA empleado_cia = gestionGL_context.C_EMPLEADOS_CIA.FirstOrDefault(x => x.RFC_FM3 == nueva_ficha_rfc);

                if (empleado_cia == null)
                {
                    throw new ShowCaseException(String.Format("ERR-SI-0072: No se encontro al trabajador de compañia con RFC: '{0}'", nueva_ficha_rfc), null, TypeMessage.error, "ERR-SI-0071");
                }
                else
                {
                    nuevo_nombre = (empleado_cia.NOMBRES + " " + empleado_cia.APELLIDOS).Trim();
                }
            }
            
            STPIntegral.Datos.TabSolicitudIntegral solicitud_integral = stpIntegral_contex.TabSolicitudIntegral.SingleOrDefault(x => x.IdSolicitudIntegral == detalle_solicitud_integral.IdSolicitudIntegral);

            //Si el personal es de bajada y ademas va con hospedaje hay que validar que el nuevo trabajador este a bordo
            if (solicitud_integral.TipoSolicitudIntegral == "HOSPEDAJE Y TRANSPORTE" && detalle_solicitud_integral.TipoMovimientoPersonal == "BAJADA")
            {
                SCHAP.Datos.TAB_OCUPANTES ocupante_schap = schap_context.TAB_OCUPANTES.FirstOrDefault(x => x.BORRADO_LOGICO == false && x.FK_ID_C_M_INSTALACION == detalle_solicitud_integral.IdInstalacionDestino && x.FIC_RFC_USUARIO == nueva_ficha_rfc);

                if (ocupante_schap == null)
                {
                    throw new ShowCaseException(String.Format("ERR-SI-0073: El nuevo trabajador de reemplazo se debe encontrar a bordo de la instalación destino", nueva_ficha_rfc), null, TypeMessage.error, "ERR-SI-0073");
                }
            }

            STPIntegral.Datos.TabDetallesSolicitudIntegral pasajero_detalle_retorno = null;

            if ((detalle_solicitud_integral.TipoMovimientoPersonal == "SUBIDA" && solicitud_integral.TipoSolicitudIntegral == "SOLO TRANSPORTE") || detalle_solicitud_integral.TipoMovimientoPersonal == "RETORNO")
            {
                if(detalle_solicitud_integral.TipoMovimientoPersonal == "RETORNO")
                {
                    pasajero_detalle_retorno = stpIntegral_contex.TabDetallesSolicitudIntegral.FirstOrDefault(x => x.TipoMovimientoPersonal == "SUBIDA" && x.FichaRFCEmpleado == detalle_solicitud_integral.FichaRFCEmpleado);
                }
                else
                {
                    pasajero_detalle_retorno = stpIntegral_contex.TabDetallesSolicitudIntegral.FirstOrDefault(x => x.TipoMovimientoPersonal == "RETORNO" && x.FichaRFCEmpleado == detalle_solicitud_integral.FichaRFCEmpleado);
                }
            }

            //Si pasa las validaciones entonces se hace la modificacion.
            detalle_solicitud_integral.FichaRFCEmpleado = nueva_ficha_rfc;
            detalle_solicitud_integral.NombreEmpleado = nuevo_nombre;
            resultado.Add(detalle_solicitud_integral);

            if (pasajero_detalle_retorno != null)
            {
                pasajero_detalle_retorno.FichaRFCEmpleado = nueva_ficha_rfc;
                pasajero_detalle_retorno.NombreEmpleado = nuevo_nombre;
                resultado.Add(pasajero_detalle_retorno);
            }

            //Se guardan los cambios.
            stpIntegral_contex.SaveChanges();
            //SE INSERTA LA AUDITORIA
            auditoria.TipoMovimientoAuditoria = "REEEMPLAZO PASAJERO";
            auditoria.DescripcionAuditoria = "SE REEMPLAZA AL PASAJERO/HUESPED CON FICHA/RFC: " + ficha_ant + " (" + nombre_ant + ") POR: " + detalle_solicitud_integral.FichaRFCEmpleado + " (" + detalle_solicitud_integral.NombreEmpleado + ") " + " EN EL DETALLE DEL FOLIO: " + solicitud_integral.FolioSolicitudIntegral.ToString() + " Y TIPO DE MOVIMIENTO: " + detalle_solicitud_integral.TipoMovimientoPersonal;
            auditoria.FkIdSolicitudIntegral = solicitud_integral.IdSolicitudIntegral;
            auditoria.ObjetoAntesModificacion = "";//js.Serialize(detalle_solicitud_integral_ANTERIOR);
            auditoria.ObjetoDespuesModificacion = "";//js.Serialize(detalle_solicitud_integral);
            AgregarAuditoria(stpIntegral_contex, auditoria);

            if (pasajero_detalle_retorno != null)
            {
                auditoria.TipoMovimientoAuditoria = "REEEMPLAZO PASAJERO";
                auditoria.DescripcionAuditoria = "SE REEMPLAZA AL PASAJERO/HUESPED CON FICHA/RFC: " + ficha_ant + " (" + nombre_ant + ") POR: " + pasajero_detalle_retorno.FichaRFCEmpleado + " (" + pasajero_detalle_retorno.NombreEmpleado + ") " + " EN EL DETALLE DEL FOLIO: " + solicitud_integral.FolioSolicitudIntegral.ToString() + " Y TIPO DE MOVIMIENTO: " + pasajero_detalle_retorno.TipoMovimientoPersonal;
                auditoria.FkIdSolicitudIntegral = solicitud_integral.IdSolicitudIntegral;
                auditoria.ObjetoAntesModificacion = "";//js.Serialize(detalle_solicitud_integral_ANTERIOR);
                auditoria.ObjetoDespuesModificacion = "";//js.Serialize(detalle_solicitud_integral);
                AgregarAuditoria(stpIntegral_contex, auditoria);
            }


            stpIntegral_contex.Dispose();
            gestionGL_context.Dispose();
            schap_context.Dispose();

            foreach (STPIntegral.Datos.TabDetallesSolicitudIntegral detalle in resultado)
            {
                detalle.TabSolicitudIntegral = null;
                detalle.ah_tb_capacidad_detalle = null;
            }

            return resultado;
        }

        //AUDITAR
        public static bool GuardarDetalleObservaciones(int id_detalle_solicitud_integral, string ficha_pasajero, string observaciones, bool lleva_material,STPIntegral.Datos.TabAuditoriaSolicitudIntegral auditoria)
        {
            using (var db = new Datos.STPIntegralModel())
            {
                db.Configuration.LazyLoadingEnabled = false;
                db.Configuration.ProxyCreationEnabled = false;

                var ocupante = db.TabDetallesSolicitudIntegral.SingleOrDefault(x => x.IdDetalleSolicitudIntegral == id_detalle_solicitud_integral);

                if (ocupante == null) throw new ShowCaseException(String.Format("ERR-SI-0081: No existe la información requerida del pasajero [{0}]", ficha_pasajero), null, TypeMessage.error, "ERR-SI-0081");

                if (ocupante.EstadoOcupanteSolicitudIntegral != "SOLICITADO") throw new ShowCaseException(String.Format("ERR-SI-0082: No se pueden guardar los datos debido a que el status del pasajero [{0}] debe ser 'SOLICITADO'", ocupante.FichaRFCEmpleado), null, TypeMessage.error, "ERR-SI-0082");

                ocupante.ObservacionesDetalle = observaciones;
                ocupante.LLevaMaterial = lleva_material;

                db.SaveChanges();

                //INSERTAMOS LA AUDITORIA
                auditoria.TipoMovimientoAuditoria = "INSERTA OBSERVACIONES";
                auditoria.DescripcionAuditoria = "SE INSERTAN LAS OBSERVACIONES DEL PASAJERO: " + ocupante.FichaRFCEmpleado + " (" + ocupante.NombreEmpleado + ") LAS CUALES FUERON: " + observaciones.ToUpper();
                auditoria.FkIdSolicitudIntegral = ocupante.IdSolicitudIntegral;
                auditoria.ObjetoAntesModificacion = "";//js.Serialize(detalle_solicitud_integral_ANTERIOR);
                auditoria.ObjetoDespuesModificacion = "";//js.Serialize(detalle_solicitud_integral);
                db.TabAuditoriaSolicitudIntegral.Add(auditoria);
                db.SaveChanges();
                //AgregarAuditoria(stpIntegral_contexT, auditoria);
            }

            return true;
        }
            
        private static DateTime ObtenerHorarioSalidaItinerario(int id_itinerario, DateTime fecha_salida, int id_instalacion_destino, STPIntegral.Datos.STPIntegralModel stpIntegral_contex)
        {
            DateTime resultado = System.DateTime.Now;

            fecha_salida = fecha_salida.Date;
            
            STPIntegral.Datos.stp_c_itinerario_destino itinerario_instalacion_destino = stpIntegral_contex.stp_c_itinerario_destino.SingleOrDefault(x => x.id_itinerario == id_itinerario && x.fk_instalacion == id_instalacion_destino);

            if (itinerario_instalacion_destino != null && itinerario_instalacion_destino.hora_salida.HasValue)
            {
                resultado = fecha_salida.AddHours(itinerario_instalacion_destino.hora_salida.Value.Hour).AddMinutes(itinerario_instalacion_destino.hora_salida.Value.Minute);
            }
            else 
            {
                STPIntegral.Datos.stp_tb_itinerario_dia itinerario_dia = stpIntegral_contex.stp_tb_itinerario_dia.SingleOrDefault(x => x.id_itinerario == id_itinerario && x.fecha_itinerario_dia == fecha_salida);

                if (itinerario_dia != null && itinerario_dia.horario_itinerario.HasValue)
                {
                    resultado = fecha_salida.AddHours(itinerario_dia.horario_itinerario.Value.Hour).AddMinutes(itinerario_dia.horario_itinerario.Value.Minute);
                }
                else
                {
                    STPIntegral.Datos.stp_c_itinerario_base itinerario_base = stpIntegral_contex.stp_c_itinerario_base.SingleOrDefault(x => x.id_itinerario == id_itinerario);

                    if (itinerario_base != null && itinerario_base.horario_itinerario.HasValue)
                    {
                        resultado = fecha_salida.AddHours(itinerario_base.horario_itinerario.Value.Hour).AddMinutes(itinerario_base.horario_itinerario.Value.Minute);
                    }
                    else
                    {
                        resultado = fecha_salida;
                    }
                }
            }

            return resultado;
        }

        private static bool AplicarValidacionesSolicitudIntegral(STPIntegral.Datos.TabSolicitudIntegral solicitud_integral, IEnumerable<STPIntegral.Datos.TabDetallesSolicitudIntegral> detalles_solicitud_integral, GestionGL.Datos.GestionGLModel gestionGL_context, STPIntegral.Datos.STPIntegralModel stpIntegral_contex, SCHAP.Datos.SCHAPModel schap_context, bool validarDetalles)
        {
            bool resultado = true;
            //1 Algoritmo - Verificar si la solicitud existe: 
            //2 Si la solicitud no existe, entonces se tiene que crear desde 0:
            //3 Se tiene que hacer las validaciones que no requieran una transaccion.
            //4 Fechas de servicio correctas,  Solicitante valido, Autorizador Valido, Itinerario valido, que las instalaciones origen y destino realmente existan y que las areas de servicio de esas instalaciones correspondan,
            //que las fichas-RFC del personal existan, que las fichas del personal no se repitan, que el personal no se encuentre en otras instalaciones al momento de hacer la solicitud o que no este en otra solicitud integral.
            //que el direccionamiento elegido no este expirado y que sea correcto (que corresponda al autorizador).
            //cuando todas estas validaciones sean correctas entonces se prepara un objeto del tipo SolicitudIntegral con todos los datos validos listos para su alta con IdSolicitudIntegral = 0 y se pasa a la siguiente fase.
            //si la solicitud ya existia:
            //Se hace una consulta a la base de datos y se valida que el objeto id_solicitud_integral exista.
            //Se aplican de nuevo validaciones para ver si no estan alteradas las fechas de subida, bajada, que los direccionamientos sean validos, que las instalaciones existan y correspondan a las areas de servicio.

            //Cuando todo esto esta listo entonces se procede a abrir una transaccion.
            //{
            //   //primero se verifica que haya disponibilidad de capacidad tanto en transporte como en hospedaje (lo que aplique).
            //   //si se confirma la capacidad entonces se procede a insertar la solicitud integral nueva si hace falta. (previa generacion transaccionada del folio de solicitud).
            //   //luego se inserta el detalle de la solicitud integral con el nuevo id generado.
            //   //por ultimo se actualizan las capacidades de reservacion en transporte y hospedaje segun aplique.
            //   // si todo sale bien se cierra la transaccion, en caso contrario se aborta toda la operacion.
            //}

            //1 Algoritmo - Verificar si la solicitud existe: 
            if (solicitud_integral.IdSolicitudIntegral <= 0)
            {
                //SOLICITUD INTEGRAL NUEVA
                //2 Si la solicitud no existe, entonces se tiene que crear desde 0:
                //3 Aqui se tienen que hacer TODAS las validaciones que no requieran una transaccion (para efectos de comprobar si los datos de la solictud son correctos).

                //3.0 Validando que el tipo de solicitud, el tipo de personal, transporte y servicio sean validos.
                //Validando el tipo de Solicitud
                if (String.IsNullOrEmpty(solicitud_integral.TipoSolicitudIntegral.Trim()))
                {
                    throw new ShowCaseException(String.Format("ERR-SI-0010: El tipo de solicitud no puede estar vacio, debe ser alguno de las siguientes opciones: ['SOLO HOSPEDAJE', 'SOLO TRANSPORTE', 'HOSPEDAJE Y TRANSPORTE']"), TypeMessage.error, "ERR-SI-0010");
                }
                else
                {
                    if (!(solicitud_integral.TipoSolicitudIntegral == "SOLO HOSPEDAJE" || solicitud_integral.TipoSolicitudIntegral == "SOLO TRANSPORTE" || solicitud_integral.TipoSolicitudIntegral == "HOSPEDAJE Y TRANSPORTE"))
                    {
                        throw new ShowCaseException(String.Format("ERR-SI-0011: El tipo de solicitud proporcionado no es un dato valido: '{0}', debe ser alguno de las siguientes opciones: ['SOLO HOSPEDAJE', 'SOLO TRANSPORTE', 'HOSPEDAJE Y TRANSPORTE']", solicitud_integral.TipoSolicitudIntegral), TypeMessage.error, "ERR-SI-0011");
                    }
                }

                //Validando el tipo de Personal
                if (String.IsNullOrEmpty(solicitud_integral.TipoPersonal.Trim()))
                {
                    throw new ShowCaseException(String.Format("ERR-SI-0012: El tipo de personal de la solicitud no puede estar vacio, debe ser alguno de las siguientes opciones: ['PEMEX', 'COMPAÑIA', 'COMODATARIO']"), TypeMessage.error, "ERR-SI-0012");
                }
                else
                {
                    if (!(solicitud_integral.TipoPersonal == "PEMEX" || solicitud_integral.TipoPersonal == "COMPAÑIA" || solicitud_integral.TipoPersonal == "COMODATARIO"))
                    {
                        throw new ShowCaseException(String.Format("ERR-SI-0013: El tipo de empleado proporcionado no es un dato valido: '{0}', debe ser alguno de las siguientes opciones: ['PEMEX', 'COMPAÑIA', 'COMODATARIO']", solicitud_integral.TipoPersonal), TypeMessage.error, "ERR-SI-0013");
                    }
                }

                //Validando que exista un numero de contrato cuando la solicitud es de compañia
                if (solicitud_integral.TipoPersonal != "PEMEX")
                {
                    if (String.IsNullOrEmpty(solicitud_integral.NumeroContrato))
                    {
                        throw new ShowCaseException(String.Format("ERR-SI-0068: Si la solicitud integral es de personal de COMPAÑIA o COMODATARIO se debe proporcionar un número de contrato"), TypeMessage.error, "ERR-SI-0068");
                    }
                }


                //Si la solicitud es de solo hospedaje no hace falta validar el tipo de transporte y el tipo de servicio
                if (solicitud_integral.TipoSolicitudIntegral != "SOLO HOSPEDAJE")
                {
                    //Validando el tipo de Transporte
                    if (String.IsNullOrEmpty(solicitud_integral.TipoTransporte.Trim()))
                    {
                        throw new ShowCaseException(String.Format("ERR-SI-0014: El tipo de trasporte de la solicitud no puede estar vacio, debe ser alguno de las siguientes opciones: ['AEREO', 'MARITIMO']"), TypeMessage.error, "ERR-SI-0014");
                    }
                    else
                    {
                        if (!(solicitud_integral.TipoTransporte == "AEREO" || solicitud_integral.TipoTransporte == "MARITIMO"))
                        {
                            throw new ShowCaseException(String.Format("ERR-SI-0015: El tipo de transporte proporcionado no es un dato valido: '{0}', debe ser alguno de las siguientes opciones: ['AEREO', 'MARITIMO']", solicitud_integral.TipoTransporte), TypeMessage.error, "ERR-SI-0015");
                        }
                    }

                    //Validando el tipo de servicio
                    if (String.IsNullOrEmpty(solicitud_integral.TipoServicio.Trim()))
                    {
                        throw new ShowCaseException(String.Format("ERR-SI-0016: El tipo de servicio de la solicitud no puede estar vacio, debe ser alguno de las siguientes opciones: ['CAMBIO DE GUARDIA', 'REGULARES', 'LATERALES', 'DISPOSICION', 'EMERGENCIA']"), TypeMessage.error, "ERR-SI-0016");
                    }
                    else
                    {
                        if (!(solicitud_integral.TipoServicio == "CAMBIO DE GUARDIA" || solicitud_integral.TipoServicio == "REGULARES" || solicitud_integral.TipoServicio == "LATERALES" || solicitud_integral.TipoServicio == "DISPOSICION" || solicitud_integral.TipoServicio == "EMERGENCIA"))
                        {
                            throw new ShowCaseException(String.Format("ERR-SI-0017: El tipo de transporte proporcionado no es un dato valido: '{0}', debe ser alguno de las siguientes opciones: ['CAMBIO DE GUARDIA', 'REGULARES', 'LATERALES', 'DISPOSICION']", solicitud_integral.TipoServicio), TypeMessage.error, "ERR-SI-0017");
                        }

                        if (solicitud_integral.TipoServicio == "EMERGENCIA" && solicitud_integral.TipoSolicitudIntegral != "SOLO TRANSPORTE")
                        {
                            throw new ShowCaseException(String.Format("ERR-SI-0083: El tipo de solicitud proporcionado no es un dato valido: '{0}', debe ser 'SOLO TRANSPORTE'.", solicitud_integral.TipoSolicitudIntegral), TypeMessage.error, "ERR-SI-0083");
                        }
                    }

                    //AHORA TENEMOS QUE APLICAR UNA VALIDACION QUE ES PARTE DE LAS REGLAS DEL NEGOCIO DE TRANSPORTE AEREO, "NO SE PUEDE HACER UNA SOLICITUD DEL TIPO 'CAMBIO DE GUARDIA' PARA PERSONAL QUE NO ES DE PEMEX
                    if (solicitud_integral.TipoPersonal != "PEMEX" && solicitud_integral.TipoTransporte == "AEREO" && solicitud_integral.TipoServicio == "CAMBIO DE GUARDIA")
                    {
                        throw new ShowCaseException(String.Format("ERR-SI-0018: No se puede crear una solicitud que incluya trasporte AEREO del tipo CAMBIO DE GUARDIA para personal de COMPAÑIA o COMODATARIO", solicitud_integral.TipoServicio), TypeMessage.error, "ERR-SI-0018");
                    }

                    //Igual hace falta preguntar si el tipo de servicio "CAMBIO DE GUARDIA esta disponible para solicitudes del tipo "SOLO TRANSPORTE", aqui tengo dudas, esta validacion queda pendiente.
                    //PENDIENTE
                }
                else
                {
                    //En que caso de que la solicitud sea de solo HOSPEDAJE entonces reseteo algunos campos que no se usan.
                    
                    solicitud_integral.FechaViaje = solicitud_integral.FechaSubida;
                    solicitud_integral.FkIdItinerarioTransporteIda = null;
                    solicitud_integral.FkIdItinerarioTransporteRetorno = null;
                    solicitud_integral.HoraSalidaItinerarioTransporte = null;
                    
                    solicitud_integral.TipoServicio = "";
                    solicitud_integral.TipoTransporte = "";
                    
                    solicitud_integral.NodoTransporte = "";
                    
                }

                //Si la solicitud es de SOLO TRANSPORTE igual hay que resear algunos datos que no solo se usan para hospedaje.
                if (solicitud_integral.TipoSolicitudIntegral == "SOLO TRANSPORTE")
                {
                    solicitud_integral.FechaBajada = solicitud_integral.FechaSubida;
                }

                //3.1 Empezamos con validar las fechas de entrada/salida segun aplique.
                //3.1.1 Validando que no se haga una solicitud  con fecha de subida o servicio anterior al dia actual. (en este caso se valida solo la fecha del detalle de la solicitud)
                if (solicitud_integral.FechaBajada.Date < solicitud_integral.FechaSubida.Date)
                {
                    throw new ShowCaseException(String.Format("ERR-SI-0001: No se puede crear una solictud integral con fecha de bajada menor a la fecha de subida: '[ {0:dd/MM/yyyy} < {1:dd/MM/yyyy} ]'", solicitud_integral.FechaBajada.Date, solicitud_integral.FechaSubida.Date), TypeMessage.error, "ERR-SI-0002");
                }

                //3.1.2 Validar que no haya una solicitud con fecha de bajada < a la fecha de subida
                if (solicitud_integral.FechaSubida.Date < System.DateTime.Now.Date)
                {
                    throw new ShowCaseException(String.Format("ERR-SI-0002: No se puede crear una solictud integral con fecha de subida/servicio menor a la fecha actual: '[ {0:dd/MM/yyyy} < {1:dd/MM/yyyy} ]'", solicitud_integral.FechaSubida.Date, System.DateTime.Now.Date), TypeMessage.error, "ERR-SI-0002");
                }

                //3.2 Obteniendo los datos del usuario que captura
                if (String.IsNullOrEmpty(solicitud_integral.FichaCaptura))
                {
                    throw new ShowCaseException(String.Format("ERR-SI-0007: Se debe proporcionar la ficha de un usuario de captura para la solicitud integral.", solicitud_integral.FichaCaptura), TypeMessage.error, "ERR-SI-0007");
                }

                GestionGL.Datos.M_USUARIOS usuario_captura = gestionGL_context.M_USUARIOS.Where(x => x.FICHA == solicitud_integral.FichaCaptura).FirstOrDefault();

                if (usuario_captura != null)
                {
                    solicitud_integral.NombreCaptura = usuario_captura.NOMBRE;
                }
                else
                {
                    throw new ShowCaseException(String.Format("ERR-SI-0003: La ficha/RFC del usuario que captura la solicitud no se encontro en la base de datos de usuarios: '{0}'", solicitud_integral.FichaCaptura), TypeMessage.error, "ERR-SI-0003");
                }

                //3.3 Validando que la ficha del solicitante y del autorizador sean validas.

                //3.3.1 - Validando la ficha del Solicitante.
                if (String.IsNullOrEmpty(solicitud_integral.FichaSolicitante))
                {
                    throw new ShowCaseException(String.Format("ERR-SI-0008: Se debe proporcionar la ficha de un usuario solicitante para la solicitud integral."), TypeMessage.error, "ERR-SI-0008");
                }

                GestionGL.Datos.M_USUARIOS usuario_solicitante = gestionGL_context.M_USUARIOS.Where(x => x.FICHA == solicitud_integral.FichaSolicitante).FirstOrDefault();

                if (usuario_solicitante != null)
                {
                    solicitud_integral.NombreSolicitante = usuario_solicitante.NOMBRE.Trim();
                    //Aqui falta obtener la subdivision, pero en todo caso se debe validar la que tenga el usuario.
                    solicitud_integral.SubdivisionSolicitante = solicitud_integral.SubdivisionCaptura;
                }
                else
                {
                    throw new ShowCaseException(String.Format("ERR-SI-0004: La ficha del usuario solicitante de la solicitud no se encontro en la base de datos de usuarios: '{0}'", solicitud_integral.FichaCaptura), TypeMessage.error, "ERR-SI-0004");
                }

                //3.3.2 - Validando la ficha del Autorizador.
                if (String.IsNullOrEmpty(solicitud_integral.FichaAutorizador))
                {
                    throw new ShowCaseException(String.Format("ERR-SI-0009: Se debe proporcionar la ficha de un autorizador para la solicitud integral."), TypeMessage.error, "ERR-SI-0009");
                }

                GestionGL.Datos.Vw_C_M_AUTORIZADORES usuario_autorizador = gestionGL_context.Vw_C_M_AUTORIZADORES.Where(x => x.FICHA == solicitud_integral.FichaAutorizador).FirstOrDefault();

                if (usuario_autorizador != null)
                {
                    solicitud_integral.NombreAutorizador = (usuario_autorizador.NOMBRES + " " + usuario_autorizador.APELLIDOS).Trim();

                    //Aqui falta obtener la subdivision, pero en todo caso se debe validar la que tenga el usuario.
                    //solicitud_integral.SubdivisionAutorizador;// La subdivision del autorizador es la que viene de la pantalla de captura.
                    if (solicitud_integral.SubdivisionCaptura.Substring(0, 3) != solicitud_integral.SubdivisionAutorizador.Substring(0, 3))
                    {
                        throw new ShowCaseException(String.Format("ERR-SI-0061: El solicitante y el autorizador deben de pertenecer a la misma región y activo (los primeros dos digitos de la subdivision deben ser iguales): Subdivision Solictante: '{0}', Subdivision Autorizador: '{1}'.", solicitud_integral.SubdivisionSolicitante,solicitud_integral.SubdivisionAutorizador), TypeMessage.error, "ERR-SI-0061");
                    }
                }
                else
                {
                    throw new ShowCaseException(String.Format("ERR-SI-0005: La ficha del usuario autorizador de la solicitud no se encontro en la base de datos de usuarios: '{0}'", solicitud_integral.FichaAutorizador), TypeMessage.error, "ERR-SI-0005");
                }

                //3.4 Validando que el Itinerario elegido sea el correcto.
                if (solicitud_integral.TipoSolicitudIntegral != "SOLO HOSPEDAJE")
                {
                    //Como se trata de una solicitud que incluye transporte entonces hay que validar que el itinerario seleccionado exista.
                    STPIntegral.Datos.VW_stp_c_itinerario_base itinerario_viaje_ida = stpIntegral_contex.VW_stp_c_itinerario_base.Where(x => x.id_itinerario == solicitud_integral.FkIdItinerarioTransporteIda).SingleOrDefault();

                    if (itinerario_viaje_ida == null)
                    {
                        throw new ShowCaseException(String.Format("ERR-SI-0006: No se encontro el itinerario seleccionado en la base de datos."), TypeMessage.error, "ERR-SI-0006");
                    }
                    else
                    {
                        //Si el itinerario de ida existe entonces hay que preguntar si la solictud es de cambio de guardia para validar si hay item de retorno.
                        if (solicitud_integral.TipoServicio == "CAMBIO DE GUARDIA")
                        {
                            //validando que exista una solicitud de retorno, en caso contrario hay que seleccionar una
                            if (solicitud_integral.FkIdItinerarioTransporteRetorno == null || solicitud_integral.FkIdItinerarioTransporteRetorno <= 0)
                            {
                                //Se selecciona la primera que se encuentre (esto hay que revisarlo todavia) que tenga los nodos de origen y destino invertidos.
                                //Area Servicios Aereo: 2
                                //Area Servicios Maritimo: 3
                                IEnumerable<STPIntegral.Datos.Vw_SoltIntItinerariosCapacidadDia_NOLOCK> listado_itinerarios_disponibles = ItinerariosTransporte.ConsultarItinerarios(solicitud_integral.TipoSolicitudIntegral,solicitud_integral.TipoTransporte, solicitud_integral.TipoServicio, solicitud_integral.FechaSubida, solicitud_integral.NodoAtencion, "", "", solicitud_integral.NumeroContrato, solicitud_integral.SubdivisionAutorizador, true);

                                STPIntegral.Datos.Vw_SoltIntItinerariosCapacidadDia_NOLOCK itinerario_retorno = (from x in listado_itinerarios_disponibles where x.cve_nodo_origen == itinerario_viaje_ida.cve_nodo_destino && x.cve_nodo_destino == itinerario_viaje_ida.cve_nodo_origen select x).FirstOrDefault();

                                if (itinerario_retorno != null)
                                {
                                    solicitud_integral.FkIdItinerarioTransporteRetorno = itinerario_retorno.id_itinerario;
                                }
                                else
                                {
                                    throw new ShowCaseException(String.Format("ERR-SI-0036: No se encontro el itinerario un itinerario de retorno."), TypeMessage.error, "ERR-SI-0036");
                                }
                            }
                        }//Fin if (solicitud_integral.TipoServicio == "CAMBIO DE GUARDIA")
                    }
                }//Fin if (solicitud_integral.TipoSolicitudIntegral != "SOLO HOSPEDAJE")

                //Creo que hasta aqui son todas las validaciones que de momento se pueden hacer a la solicitud integral sin requerir entrar al ambito de una transaccion.
            }
            else
            {
                //SOLICITUD INTEGRAL EXISTENTE

                //hay que traer los datos de la solicitud integral existente.
                STPIntegral.Datos.TabSolicitudIntegral _solicitud_integral_existente = stpIntegral_contex.TabSolicitudIntegral.Where(x => x.IdSolicitudIntegral == solicitud_integral.IdSolicitudIntegral).Single();

                if (_solicitud_integral_existente == null)
                {
                    throw new ShowCaseException(String.Format("ERR-SI-0019: No se encontro ningun registro del identificador de la solicitud integral existente proporcionada: [Id: {0} - Folio: '{1}']", solicitud_integral.IdSolicitudIntegral, solicitud_integral.FolioSolicitudIntegral), TypeMessage.error, "ERR-SI-0019");
                }
                else
                {
                    solicitud_integral = _solicitud_integral_existente;
                }

                //Como se va a agregar un nuevo detalle hay que validar que la solicitud aun se encuentre en estado "SOLICITADA"
                if (solicitud_integral.EstadoSolicitudIntegral != "SOLICITADO")
                {
                    throw new ShowCaseException(String.Format("ERR-SI-0020: No se puede agregar a otro empleado a la solictud integral pues esta ya no se encuentra en estado solicitado: [Id: {0} - Folio: '{1}' - Estado: '{2}']", solicitud_integral.IdSolicitudIntegral, solicitud_integral.FolioSolicitudIntegral, solicitud_integral.EstadoSolicitudIntegral), TypeMessage.error, "ERR-SI-0020");
                }
            }


            if (solicitud_integral.TipoSolicitudIntegral == "SOLO HOSPEDAJE")
            {
                GestionGL.Datos.C_M_INSTALACIONES instalacion_carmen = gestionGL_context.C_M_INSTALACIONES.Single(x => x.SIGLAS_INSTALACION == "CME");

                foreach (STPIntegral.Datos.TabDetallesSolicitudIntegral detalle_solicitud in detalles_solicitud_integral)
                {
                    detalle_solicitud.IdInstalacionOrigen = instalacion_carmen.ID_C_M_INSTALACION;
                    detalle_solicitud.ClaveInstalacionOrigen = instalacion_carmen.SIGLAS_INSTALACION;
                    detalle_solicitud.NombreInstalacionOrigen = instalacion_carmen.NOMBRE_INSTALACION;
                }
            }

            //Aplicando validaciones para ver si es un trabajador de bajada en caso de que su instalacion destino sea PUERTO (SOLO APLICA PARA MOVIMIENTOS "HOSPEDAJE Y TRANSPORTE")
            if (solicitud_integral.TipoSolicitudIntegral == "HOSPEDAJE Y TRANSPORTE")
            {
                List<string> listado_nodos_no_ixt = (from x in stpIntegral_contex.stp_c_nodos.Where(x => x.cve_nodo != "IXT") select x.cve_nodo).Distinct().ToList();
                List<int> ids_instalaciones_puerto = (from x in gestionGL_context.C_M_INSTALACIONES.Where(x => listado_nodos_no_ixt.Contains(x.SIGLAS_INSTALACION)) select x.ID_C_M_INSTALACION).Distinct().ToList();

                foreach (STPIntegral.Datos.TabDetallesSolicitudIntegral detalle_solicitud in detalles_solicitud_integral)
                {
                    if (ids_instalaciones_puerto.Contains(detalle_solicitud.IdInstalacionDestino))
                    {
                        detalle_solicitud.TipoMovimientoPersonal = "BAJADA";

                        //TODO: SI es de bajada hay que validar que el trabajador se encuentre a bordo.
                    }
                }
            }
            
            //En este punto ya tenemos al objeto solicitud integral (nuevo o existente) ahora tenemos que validar los detalles de la solicitud.
            //Se llama a la funcion ValidarEmpleadoDetalleSolicitud para verificar si no esta en otra solicitud de hospedaje u en alguna otra plataforma, sin embargo no se valida disponiblidad de hospedaje ni de transporte, eso se hace mas adelante dentro del ambito de la transaccion.
            if (validarDetalles == true)
            {
                foreach (STPIntegral.Datos.TabDetallesSolicitudIntegral detalle in detalles_solicitud_integral)
                {
                    STPIntegral.Datos.TabDetallesSolicitudIntegral detalle_empleado_validado = ValidarEmpleadoDetalleSolicitud(solicitud_integral.TipoSolicitudIntegral, solicitud_integral.TipoPersonal, solicitud_integral.TipoTransporte, solicitud_integral.TipoServicio, detalle.fk_itinerario, solicitud_integral.FichaAutorizador, detalle.FechaSubida, detalle.FechaBajada, detalle.IdInstalacionOrigen, detalle.IdInstalacionDestino, detalle.FichaRFCEmpleado, detalle.TipoMovimientoPersonal, "", "", solicitud_integral.NumeroContrato, solicitud_integral.SubdivisionAutorizador, false, stpIntegral_contex, gestionGL_context, schap_context);

                    detalle.NombreEmpleado = detalle_empleado_validado.NombreEmpleado;
                    detalle.FichaRFCEmpleado = detalle_empleado_validado.FichaRFCEmpleado;
                    detalle.IdPasajeroCiaPEMEX = detalle_empleado_validado.IdPasajeroCiaPEMEX;
                }
            }

            return resultado;
        }

        private static string ObtenerNuevoFolioSolicitudIntegral(STPIntegral.Datos.STPIntegralModel stpIntegral_context, DateTime fecha_folio)
        {
            string resultado = "";
            int folio = 0;

            fecha_folio = fecha_folio.Date;
            DateTime fecha_folio_mas_un_dia = fecha_folio.AddDays(1);

            int conteo_solicitudes = stpIntegral_context.TabSolicitudIntegral.Count(x => (x.FechaCreacion >= fecha_folio && x.FechaCreacion < fecha_folio_mas_un_dia));

            conteo_solicitudes = conteo_solicitudes + 1;

            resultado = String.Format("SI-{0:ddMMyyyy}-{1:0000}", fecha_folio, conteo_solicitudes);

            return resultado;
        }

        public static SolicitudIntegralModel ObtenerDatosEdicionSolicitudIntegral(string folio_solicitud_integral, int id_permiso_ua)
        {
            //tipo_solicitud SOLO HOSPEDAJE, SOLO TRANSPORTE, HOSPEDAJE Y TRANSPORTE
            //tipo_personal: PEMEX, COMPAÑIA, COMODATARIO
            //tipo_transporte: AEREO, MARITIMO
            //tipo_servicio: CAMBIO DE GUARDIA, REGULARES, LATERALES, DISPOSICION

            SolicitudIntegralModel respuesta = null;

            //Contextos para obtener acceso a la base de datos.
            GestionGL.Datos.GestionGLModel gestionGL_context = new GestionGL.Datos.GestionGLModel();
            STPIntegral.Datos.STPIntegralModel stpIntegral_contex = new Datos.STPIntegralModel();
            SCHAP.Datos.SCHAPModel schap_context = new SCHAP.Datos.SCHAPModel();

            gestionGL_context.Configuration.LazyLoadingEnabled = false;
            stpIntegral_contex.Configuration.LazyLoadingEnabled = false;
            schap_context.Configuration.LazyLoadingEnabled = false;

            gestionGL_context.Configuration.ProxyCreationEnabled = false;
            stpIntegral_contex.Configuration.ProxyCreationEnabled = false;
            schap_context.Configuration.ProxyCreationEnabled = false;

            respuesta = new SolicitudIntegralModel();

            GestionGL.Datos.VW_PERMISOS_UA permiso_ua_usuario = null;

            using (GestionGL.Datos.GestionGLModel gestiongl_model = new GestionGL.Datos.GestionGLModel())
            {
                permiso_ua_usuario = gestiongl_model.VW_PERMISOS_UA.SingleOrDefault(x => x.ID_PERMISOS_UA == id_permiso_ua);
            }

            using (STPIntegral.Datos.STPIntegralModel stpIntegralContext = new Datos.STPIntegralModel())
            {
                stpIntegralContext.Configuration.LazyLoadingEnabled = false;
                stpIntegralContext.Configuration.ProxyCreationEnabled = false;

                switch (permiso_ua_usuario.ROL)
                {
                    case "VENTANILLA":
                    {
                        respuesta.SolicitudIntegral = stpIntegralContext.TabSolicitudIntegral.Where(x => x.FolioSolicitudIntegral == folio_solicitud_integral).FirstOrDefault();
                        break;
                    }
                    case "USUARIO":
                    {
                        respuesta.SolicitudIntegral = stpIntegralContext.TabSolicitudIntegral.Where(x => x.FolioSolicitudIntegral == folio_solicitud_integral && x.FichaCaptura == permiso_ua_usuario.FICHA).FirstOrDefault();
                        break;
                    }
                }

                if (respuesta.SolicitudIntegral != null)
                {
                    respuesta.DetallesSolicitudIntegral = stpIntegralContext.TabDetallesSolicitudIntegral.Where(x => x.IdSolicitudIntegral == respuesta.SolicitudIntegral.IdSolicitudIntegral).ToList();
                }
            }

            if (respuesta.SolicitudIntegral != null)
            {
                if (respuesta.SolicitudIntegral.TipoPersonal != "PEMEX")
                {
                    respuesta.ContratoSap = ValidarContrato(respuesta.SolicitudIntegral.NumeroContrato, respuesta.SolicitudIntegral.FechaSubida, respuesta.SolicitudIntegral.FechaBajada);
                }
                else
                {
                    respuesta.ContratoSap = null;
                }

                if (respuesta.SolicitudIntegral.TipoSolicitudIntegral != "SOLO HOSPEDAJE")
                {
                    respuesta.ItinerariosTransporte = ItinerariosTransporte.ConsultarItinerarios(respuesta.SolicitudIntegral.TipoSolicitudIntegral, respuesta.SolicitudIntegral.TipoTransporte, respuesta.SolicitudIntegral.TipoServicio, respuesta.SolicitudIntegral.FechaSubida, respuesta.SolicitudIntegral.NodoAtencion, "", "", respuesta.SolicitudIntegral.NumeroContrato, respuesta.SolicitudIntegral.SubdivisionAutorizador, false);
                }
                else
                {
                    respuesta.ItinerariosTransporte = null;
                }

                InstalacionesDestinoDireccionamientos instalaciones_origen = ConsultarOrigenes(
                                                                                                respuesta.SolicitudIntegral.TipoSolicitudIntegral, 
                                                                                                respuesta.SolicitudIntegral.TipoPersonal, 
                                                                                                respuesta.SolicitudIntegral.TipoTransporte, 
                                                                                                respuesta.SolicitudIntegral.TipoServicio, 
                                                                                                respuesta.SolicitudIntegral.FkIdItinerarioTransporteIda, 
                                                                                                respuesta.SolicitudIntegral.FichaAutorizador, 
                                                                                                respuesta.SolicitudIntegral.FechaSubida, 
                                                                                                respuesta.SolicitudIntegral.FechaBajada
                                                                                               );

                InstalacionesDestinoDireccionamientos instalaciones_destinos = ConsultarDestinos(respuesta.SolicitudIntegral.TipoSolicitudIntegral, respuesta.SolicitudIntegral.TipoPersonal, respuesta.SolicitudIntegral.TipoTransporte, respuesta.SolicitudIntegral.TipoServicio, respuesta.SolicitudIntegral.FkIdItinerarioTransporteIda, respuesta.SolicitudIntegral.FichaAutorizador, respuesta.SolicitudIntegral.FechaSubida, respuesta.SolicitudIntegral.FechaBajada, respuesta.SolicitudIntegral.FKidOrigenSolicitud.HasValue ? respuesta.SolicitudIntegral.FKidOrigenSolicitud.Value : 0);

                if (instalaciones_origen.InstalacionesDestino != null)
                {
                    foreach (GestionGL.Datos.C_M_INSTALACIONES instalacion_origen in instalaciones_origen.InstalacionesDestino)
                    {
                        instalacion_origen.C_CLASIFICACION_PROCESO = null;
                        instalacion_origen.C_CLASIFICACIONES_SERVICIO = null;
                        instalacion_origen.C_COMPLEJOS = null;
                        instalacion_origen.C_D_INSTALACIONES = null;
                        instalacion_origen.C_M_INSTALACIONES1 = null;
                        instalacion_origen.C_M_INSTALACIONES11 = null;
                        instalacion_origen.C_M_INSTALACIONES2 = null;
                        instalacion_origen.C_M_INSTALACIONES3 = null;
                        instalacion_origen.C_M_PLATAFORMAS_PREH = null;
                        instalacion_origen.C_PERMISOS_PREH_INSTALACIONES_MARINAS_SCHAP = null;
                        instalacion_origen.C_PERMISOS_UA_SCHAP = null;
                        instalacion_origen.C_POZOS = null;
                        instalacion_origen.C_TIPO_ESTRUCTURA = null;
                    }
                }

                if (respuesta.InstalacionesDestino != null)
                {
                    foreach (GestionGL.Datos.C_M_INSTALACIONES instalacion_destino in respuesta.InstalacionesDestino)
                    {
                        instalacion_destino.C_CLASIFICACION_PROCESO = null;
                        instalacion_destino.C_CLASIFICACIONES_SERVICIO = null;
                        instalacion_destino.C_COMPLEJOS = null;
                        instalacion_destino.C_D_INSTALACIONES = null;
                        instalacion_destino.C_M_INSTALACIONES1 = null;
                        instalacion_destino.C_M_INSTALACIONES11 = null;
                        instalacion_destino.C_M_INSTALACIONES2 = null;
                        instalacion_destino.C_M_INSTALACIONES3 = null;
                        instalacion_destino.C_M_PLATAFORMAS_PREH = null;
                        instalacion_destino.C_PERMISOS_PREH_INSTALACIONES_MARINAS_SCHAP = null;
                        instalacion_destino.C_PERMISOS_UA_SCHAP = null;
                        instalacion_destino.C_POZOS = null;
                        instalacion_destino.C_TIPO_ESTRUCTURA = null;
                    }
                }

                respuesta.InstalacionesOrigen = instalaciones_origen.InstalacionesDestino;
                respuesta.ListadoDireccionamientosOrigenes = instalaciones_origen.DireccionamientosInstalaciones;

                respuesta.InstalacionesDestino = instalaciones_destinos.InstalacionesDestino;
                respuesta.ListadoDireccionamientosDestinos = instalaciones_destinos.DireccionamientosInstalaciones;
            }
            else
            {
                respuesta = null;
            }

            return respuesta;
        }

        //AUDITAR
        public static List<STPIntegral.Datos.TabDetallesSolicitudIntegral> EliminarDetalleSolicitudIntegral(int IdDetalleSolicitudIntegral, int id_permiso_ua,STPIntegral.Datos.TabAuditoriaSolicitudIntegral auditoria, out string operacion)
        {
            //Primero se valida que el estado de la solicitud sea SOLICITADO
            //Luego se valida que el estado del pasajero sea SOLICITADO
            List<STPIntegral.Datos.TabDetallesSolicitudIntegral> resultado = new List<Datos.TabDetallesSolicitudIntegral>();
            STPIntegral.Datos.TabSolicitudIntegral solicitudIntegral = null;

            GestionGL.Datos.VW_PERMISOS_UA permiso_ua_usuario = null;

            using (GestionGL.Datos.GestionGLModel gestiongl_model = new GestionGL.Datos.GestionGLModel())
            {
                permiso_ua_usuario = gestiongl_model.VW_PERMISOS_UA.SingleOrDefault(x => x.ID_PERMISOS_UA == id_permiso_ua);
            }

            STPIntegral.Datos.TabDetallesSolicitudIntegral detalle_eliminar = null;

            using (STPIntegral.Datos.STPIntegralModel stpIntegral_contex = new Datos.STPIntegralModel())
            {
                stpIntegral_contex.Configuration.LazyLoadingEnabled = false;
                stpIntegral_contex.Configuration.ProxyCreationEnabled = false;

                detalle_eliminar = stpIntegral_contex.TabDetallesSolicitudIntegral.Where(x => x.IdDetalleSolicitudIntegral == IdDetalleSolicitudIntegral).SingleOrDefault();

                if (resultado == null)
                {
                    throw new ShowCaseException(String.Format("ERR-SI-0038: No se encontro el detalle de la solicitud integral eliminación con el id proporcionado: '{0}'", IdDetalleSolicitudIntegral), TypeMessage.error, "ERR-SI-0038");
                }
                else

                    solicitudIntegral = stpIntegral_contex.TabSolicitudIntegral.Where(x => x.IdSolicitudIntegral == detalle_eliminar.IdSolicitudIntegral).SingleOrDefault();

                if (solicitudIntegral == null)
                {
                    throw new ShowCaseException(String.Format("ERR-SI-0039: No se encontro el id de la solicitud integral: '{0}'", detalle_eliminar.IdSolicitudIntegral), TypeMessage.error, "ERR-SI-0039");
                }

                else
                {
                    if (permiso_ua_usuario.ROL == RolesUsuarioSolicitudIntegral.USUARIO.ToString() && solicitudIntegral.FichaCaptura != permiso_ua_usuario.FICHA)
                    {
                        throw new ShowCaseException(String.Format("ERR-SI-0062: Solo el usuario que capturo la solicitud integral esta autorizado para eliminar un detalle: '{0}'", solicitudIntegral.NombreCaptura), TypeMessage.error, "ERR-SI-0062");
                    }
                }

                if (permiso_ua_usuario.ROL == RolesUsuarioSolicitudIntegral.USUARIO.ToString() && solicitudIntegral.EstadoSolicitudIntegral != EstadosSolicitudIntegral.SOLICITADO.ToString())
                {
                    throw new ShowCaseException(String.Format("ERR-SI-0040: No se puede eliminar el detalle pues el estado de la solicitud integral es diferente a 'SOLICITADO': {0} [{1}]'", solicitudIntegral.FolioSolicitudIntegral, solicitudIntegral.EstadoSolicitudIntegral), TypeMessage.error, "ERR-SI-0040");
                }

                if (permiso_ua_usuario.ROL == RolesUsuarioSolicitudIntegral.USUARIO.ToString() && detalle_eliminar.EstadoOcupanteSolicitudIntegral != EstadosDetallesSolicitudIntegral.SOLICITADO.ToString())
                {
                    throw new ShowCaseException(String.Format("ERR-SI-0041: No se puede eliminar el detalle pues su estado en la solicitud integral es diferente de 'SOLICITADO': {0} {1} -[{2}]'", detalle_eliminar.FichaRFCEmpleado, detalle_eliminar.NombreEmpleado, detalle_eliminar.EstadoOcupanteSolicitudIntegral), TypeMessage.error, "ERR-SI-0041");
                }

                List<STPIntegral.Datos.TabDetallesSolicitudIntegral> listado_detalles_solicitud_integral = stpIntegral_contex.TabDetallesSolicitudIntegral.Where(x => x.IdSolicitudIntegral == solicitudIntegral.IdSolicitudIntegral).ToList();

                //if (listado_detalles_solicitud_integral.Count == 1)
                //{
                //    throw new ShowCaseException(String.Format("ERR-SI-0045: No se puede eliminar el detalle de la solicitud integral pues se quedaria sin ningun registro, en este caso ese debe Cancelar la Solicitud Integral", resultado.FichaRFCEmpleado, resultado.NombreEmpleado, resultado.EstadoOcupanteSolicitudIntegral), TypeMessage.error, "ERR-SI-0045");
                //}
            }

            GestionGL.Datos.C_CONTRATOS contrato_gestion_gl = null;
            GestionGL.Datos.C_M_INSTALACIONES instalacion_destino = null;
            GestionGL.Datos.C_M_INSTALACIONES instalacion_destino_retorno = null;

            using (GestionGL.Datos.GestionGLModel gestionGl_context = new GestionGL.Datos.GestionGLModel())
            {
                gestionGl_context.Configuration.LazyLoadingEnabled = false;
                gestionGl_context.Configuration.ProxyCreationEnabled = false;

                if (!String.IsNullOrEmpty(solicitudIntegral.NumeroContrato.Trim()))
                {
                    contrato_gestion_gl = gestionGl_context.C_CONTRATOS.Where(x => x.CONTRATO == solicitudIntegral.NumeroContrato).SingleOrDefault();
                }

                instalacion_destino = gestionGl_context.C_M_INSTALACIONES.Single(x => x.ID_C_M_INSTALACION == detalle_eliminar.IdInstalacionDestino);
                instalacion_destino_retorno = gestionGl_context.C_M_INSTALACIONES.Single(x => x.ID_C_M_INSTALACION == detalle_eliminar.IdInstalacionOrigen);
            }

            bool se_disminuyo_espacio_en_hospedaje = false;

            using (TransactionScope transaction_scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
            {
                try
                {
                    //Como toda la parte transaccional quedo solo en STPIntegral, entonces se crea un nuevo contexto para que todo esto se transaccione.
                    using (Datos.STPIntegralModel stp_integral_context = new Datos.STPIntegralModel())
                    {
                        Datos.TabDetallesSolicitudIntegral detalle_solicitud_eliminar = stp_integral_context.TabDetallesSolicitudIntegral.Single(x => x.IdDetalleSolicitudIntegral == IdDetalleSolicitudIntegral);

                        Datos.TabDetallesSolicitudIntegral detalle_solicitud_retorno_eliminar = stp_integral_context.TabDetallesSolicitudIntegral.SingleOrDefault(x => x.IdSolicitudIntegral == detalle_solicitud_eliminar.IdSolicitudIntegral && x.FichaRFCEmpleado == detalle_solicitud_eliminar.FichaRFCEmpleado && x.TipoMovimientoPersonal == "RETORNO" && x.EstadoOcupanteSolicitudIntegral == detalle_solicitud_eliminar.EstadoOcupanteSolicitudIntegral);

                        resultado.Add(detalle_solicitud_eliminar);

                        if (detalle_solicitud_retorno_eliminar != null)
                        {
                            resultado.Add(detalle_solicitud_retorno_eliminar);
                        }

                        Datos.TabSolicitudIntegral solicitud_integral = stp_integral_context.TabSolicitudIntegral.SingleOrDefault(x => x.IdSolicitudIntegral == detalle_solicitud_eliminar.IdSolicitudIntegral);

                        switch (detalle_solicitud_eliminar.EstadoOcupanteSolicitudIntegral)
                        {
                            case "CONFIRMADO":
                                {
                                    //Aplicando reglas de cancelacion de hospedaje
                                    if (solicitud_integral.TipoSolicitudIntegral != "SOLO TRANSPORTE")
                                    {
                                        if (detalle_solicitud_eliminar.EstadoOcupanteSolicitudIntegral == EstadosDetallesSolicitudIntegral.CONFIRMADO.ToString() && detalle_solicitud_eliminar.EstadoHuespedAH == EstadosDetallesSolicitudIntegralAH.CONFIRMADO.ToString())
                                        {
                                            //Si el ocupante esta confirmado entonces aun se puede Cancelar

                                            detalle_solicitud_eliminar.EstadoOcupanteSolicitudIntegral = EstadosDetallesSolicitudIntegral.CANCELADO.ToString();
                                            detalle_solicitud_eliminar.EstadoHuespedAH = EstadosDetallesSolicitudIntegralAH.CANCELADO.ToString();
                                            detalle_solicitud_eliminar.EstadoPasajeroSTP = EstadosDetallesSolicitudIntegralAH.CANCELADO.ToString();

                                            if (detalle_solicitud_retorno_eliminar != null)
                                            {
                                                detalle_solicitud_retorno_eliminar.EstadoOcupanteSolicitudIntegral = EstadosDetallesSolicitudIntegral.CANCELADO.ToString();
                                                detalle_solicitud_retorno_eliminar.EstadoHuespedAH = EstadosDetallesSolicitudIntegralAH.CANCELADO.ToString();
                                                detalle_solicitud_retorno_eliminar.EstadoPasajeroSTP = EstadosDetallesSolicitudIntegralAH.CANCELADO.ToString();
                                            }
                                        }
                                        else
                                        {
                                            throw new ShowCaseException(String.Format("ERR-SI-0075: No se puede Eliminar/Cancelar al pasajero '[{0}] - {1}' pues su estado en el area de servicio Alimentación y Hospedaje es diferente de Cancelado: {2}", detalle_solicitud_eliminar.FichaRFCEmpleado, detalle_solicitud_eliminar.NombreEmpleado, detalle_solicitud_eliminar.EstadoHuespedAH), null, TypeMessage.error, "ERR-SI-0075");
                                        }
                                    }

                                    //Aplicando reglas de Cancelacion de Trasnporte
                                    if (solicitud_integral.TipoSolicitudIntegral != "SOLO HOSPEDAJE")
                                    {
                                        switch (solicitud_integral.TipoTransporte)
                                        {
                                            case "AEREO":
                                                {
                                                    //Validando si el pasajero no esta programado en STA
                                                    STPIntegral.Datos.stp_tb_pasajeros_operacion pasajero_operacion = stp_integral_context.stp_tb_pasajeros_operacion.FirstOrDefault(x => x.fk_detalle_solicitud_integral == detalle_solicitud_eliminar.IdDetalleSolicitudIntegral);

                                                    if (pasajero_operacion == null)
                                                    {
                                                        //Si no esta programado
                                                        detalle_solicitud_eliminar.EstadoOcupanteSolicitudIntegral = EstadosDetallesSolicitudIntegral.CANCELADO.ToString();
                                                        detalle_solicitud_eliminar.EstadoHuespedAH = EstadosDetallesSolicitudIntegralAH.CANCELADO.ToString();
                                                        detalle_solicitud_eliminar.EstadoPasajeroSTP = EstadosDetallesSolicitudIntegralAH.CANCELADO.ToString();
                                                    }
                                                    else
                                                    {
                                                        throw new ShowCaseException(String.Format("ERR-SI-0076: No se puede Eliminar/Cancelar al pasajero '[{0}] - {1}' pues ya se encuentra en una o mas programaciones de transporte Aereo", detalle_solicitud_eliminar.FichaRFCEmpleado, detalle_solicitud_eliminar.NombreEmpleado), null, TypeMessage.error, "ERR-SI-0076");
                                                    }

                                                    if (detalle_solicitud_retorno_eliminar != null)
                                                    {
                                                        STPIntegral.Datos.stp_tb_pasajeros_operacion pasajero_operacion_retorno = stp_integral_context.stp_tb_pasajeros_operacion.FirstOrDefault(x => x.fk_detalle_solicitud_integral == detalle_solicitud_retorno_eliminar.IdDetalleSolicitudIntegral);

                                                        if (pasajero_operacion_retorno == null)
                                                        {
                                                            detalle_solicitud_retorno_eliminar.EstadoOcupanteSolicitudIntegral = EstadosDetallesSolicitudIntegral.CANCELADO.ToString();
                                                            detalle_solicitud_retorno_eliminar.EstadoHuespedAH = EstadosDetallesSolicitudIntegralAH.CANCELADO.ToString();
                                                            detalle_solicitud_retorno_eliminar.EstadoPasajeroSTP = EstadosDetallesSolicitudIntegralAH.CANCELADO.ToString();
                                                        }
                                                        else
                                                        {
                                                            throw new ShowCaseException(String.Format("ERR-SI-0084: No se puede Eliminar/Cancelar al pasajero de Retorno '[{0}] - {1}' pues ya se encuentra en una o mas programaciones de transporte Aereo", detalle_solicitud_retorno_eliminar.FichaRFCEmpleado, detalle_solicitud_retorno_eliminar.NombreEmpleado), null, TypeMessage.error, "ERR-SI-0084");
                                                        }
                                                    }

                                                    break;
                                                }
                                            case "MARITIMO":
                                                {
                                                    //En maritimo se valida que no tenga un numero de viaje asignado.
                                                    if (String.IsNullOrEmpty(detalle_solicitud_retorno_eliminar.NumeroViajeSTP))
                                                    {
                                                        detalle_solicitud_retorno_eliminar.EstadoOcupanteSolicitudIntegral = EstadosDetallesSolicitudIntegral.CANCELADO.ToString();
                                                        detalle_solicitud_retorno_eliminar.EstadoHuespedAH = EstadosDetallesSolicitudIntegralAH.CANCELADO.ToString();
                                                        detalle_solicitud_retorno_eliminar.EstadoPasajeroSTP = EstadosDetallesSolicitudIntegralAH.CANCELADO.ToString();
                                                    }
                                                    else
                                                    {
                                                        throw new ShowCaseException(String.Format("ERR-SI-0085: No se puede Eliminar/Cancelar al pasajero de Retorno '[{0}] - {1}' pues ya se encuentra en una o mas programaciones de transporte Maritimo", detalle_solicitud_eliminar.FichaRFCEmpleado, detalle_solicitud_eliminar.NombreEmpleado), null, TypeMessage.error, "ERR-SI-0077");
                                                    }

                                                    if (detalle_solicitud_retorno_eliminar != null)
                                                    {
                                                        if (String.IsNullOrEmpty(detalle_solicitud_eliminar.NumeroViajeSTP))
                                                        {
                                                            detalle_solicitud_eliminar.EstadoOcupanteSolicitudIntegral = EstadosDetallesSolicitudIntegral.CANCELADO.ToString();
                                                            detalle_solicitud_eliminar.EstadoHuespedAH = EstadosDetallesSolicitudIntegralAH.CANCELADO.ToString();
                                                            detalle_solicitud_eliminar.EstadoPasajeroSTP = EstadosDetallesSolicitudIntegralAH.CANCELADO.ToString();
                                                        }
                                                        else
                                                        {
                                                            throw new ShowCaseException(String.Format("ERR-SI-0077: No se puede Eliminar/Cancelar al pasajero '[{0}] - {1}' pues ya se encuentra en una o mas programaciones de transporte Maritimo", detalle_solicitud_retorno_eliminar.FichaRFCEmpleado, detalle_solicitud_retorno_eliminar.NombreEmpleado), null, TypeMessage.error, "ERR-SI-0077");
                                                        }
                                                    }
                                                    break;
                                                }
                                        }
                                    }

                                    // SE INSERTA AUDITORIA
                                    auditoria.TipoMovimientoAuditoria = "CANCELA PASAJERO";
                                    auditoria.DescripcionAuditoria = "CANCELA AL PASAJERO CON FICHA/RFC: " + detalle_solicitud_eliminar.FichaRFCEmpleado + " (" + detalle_solicitud_eliminar.NombreEmpleado + ") DE LA SOLICITUD: " + solicitud_integral.FolioSolicitudIntegral;
                                    auditoria.FkIdSolicitudIntegral = detalle_solicitud_eliminar.IdSolicitudIntegral;
                                    auditoria.ObjetoAntesModificacion = "";//js.Serialize(detalle_solicitud_integral_ANTERIOR);
                                    auditoria.ObjetoDespuesModificacion = "";//js.Serialize(detalle_solicitud_integral);
                                    AgregarAuditoria(stp_integral_context, auditoria);

                                    if (detalle_solicitud_retorno_eliminar != null)
                                    {
                                        // SE INSERTA AUDITORIA
                                        auditoria.TipoMovimientoAuditoria = "CANCELA PASAJERO RETORNO";
                                        auditoria.DescripcionAuditoria = "CANCELA AL PASAJERO RETORNO CON FICHA/RFC: " + detalle_solicitud_retorno_eliminar.FichaRFCEmpleado + " (" + detalle_solicitud_retorno_eliminar.NombreEmpleado + ") DE LA SOLICITUD: " + solicitud_integral.FolioSolicitudIntegral;
                                        auditoria.FkIdSolicitudIntegral = detalle_solicitud_retorno_eliminar.IdSolicitudIntegral;
                                        auditoria.ObjetoAntesModificacion = "";//js.Serialize(detalle_solicitud_integral_ANTERIOR);
                                        auditoria.ObjetoDespuesModificacion = "";//js.Serialize(detalle_solicitud_integral);
                                        AgregarAuditoria(stp_integral_context, auditoria);
                                    }

                                    operacion = "CANCELADO";

                                    bool solicitud_cancelada = CancelarSolicitudIntegralSiTodosLosDetallesEstanCancelados(solicitud_integral.IdSolicitudIntegral, stp_integral_context, auditoria);

                                    if (solicitud_cancelada == true)
                                    {
                                        operacion = "SOLICITUD CANCELADA";
                                    }

                                    break;
                                }
                            case "SOLICITADO":
                                {
                                    //Aumentar el espacio requerido.
                                    AgregarEliminarEspacioReservacionAH(stp_integral_context, solicitudIntegral, detalle_solicitud_eliminar, contrato_gestion_gl, "", "", "", instalacion_destino, 1, true, auditoria);

                                    //Se elimina el Detalle.
                                    stp_integral_context.TabDetallesSolicitudIntegral.Remove(detalle_solicitud_eliminar);

                                    operacion = "ELIMINADO";

                                    auditoria.TipoMovimientoAuditoria = "ELIMINA PASAJERO";
                                    auditoria.DescripcionAuditoria = "ELIMINA AL PASAJERO: " + detalle_solicitud_eliminar.FichaRFCEmpleado + " (" + detalle_solicitud_eliminar.NombreEmpleado + ") DE LA SOLICITUD: " + solicitudIntegral.FolioSolicitudIntegral;
                                    auditoria.FkIdSolicitudIntegral = solicitudIntegral.IdSolicitudIntegral;
                                    auditoria.ObjetoAntesModificacion = "";//js.Serialize(detalle_solicitud_integral_ANTERIOR);
                                    auditoria.ObjetoDespuesModificacion = "";//js.Serialize(detalle_solicitud_integral);
                                    //db.TabAuditoriaSolicitudIntegral.Add(auditoria);
                                    //db.SaveChanges();

                                    AgregarAuditoria(stp_integral_context, auditoria);

                                    if (detalle_solicitud_retorno_eliminar != null)
                                    {
                                        if (detalle_solicitud_retorno_eliminar != null)
                                        {
                                            AgregarEliminarEspacioReservacionAH(stp_integral_context, solicitudIntegral, detalle_solicitud_retorno_eliminar, contrato_gestion_gl, "", "", "", instalacion_destino_retorno, 1, true, auditoria);
                                        }

                                        stp_integral_context.TabDetallesSolicitudIntegral.Remove(detalle_solicitud_retorno_eliminar);

                                        operacion = "ELIMINADO";

                                        auditoria.TipoMovimientoAuditoria = "ELIMINA PASAJERO RETORNO";
                                        auditoria.DescripcionAuditoria = "ELIMINA AL PASAJERO RETORNO: " + detalle_solicitud_retorno_eliminar.FichaRFCEmpleado + " (" + detalle_solicitud_retorno_eliminar.NombreEmpleado + ") DE LA SOLICITUD: " + solicitudIntegral.FolioSolicitudIntegral;
                                        auditoria.FkIdSolicitudIntegral = solicitudIntegral.IdSolicitudIntegral;
                                        auditoria.ObjetoAntesModificacion = "";//js.Serialize(detalle_solicitud_integral_ANTERIOR);
                                        auditoria.ObjetoDespuesModificacion = "";//js.Serialize(detalle_solicitud_integral);
                                        //db.TabAuditoriaSolicitudIntegral.Add(auditoria);
                                        //db.SaveChanges();
                                        AgregarAuditoria(stp_integral_context, auditoria);
                                    }

                                    se_disminuyo_espacio_en_hospedaje = true;

                                    break;
                                }
                            default:
                                {
                                    operacion = "N/A";
                                    
                                    throw new ShowCaseException(String.Format("ERR-SI-0078: No se puede Eliminar/Cancelar al pasajero '[{0}] - {1}' pues su estado es diferente a CONFIRMADO o SOLICITADO: {2}", detalle_solicitud_eliminar.FichaRFCEmpleado, detalle_solicitud_eliminar.NombreEmpleado, detalle_solicitud_eliminar.EstadoOcupanteSolicitudIntegral), null, TypeMessage.error, "ERR-SI-0077");
                                }
                        }
                        stp_integral_context.SaveChanges();

                        if (se_disminuyo_espacio_en_hospedaje == false)
                        {
                            AgregarEliminarEspacioReservacionSTPAH(stp_integral_context, solicitudIntegral, detalle_solicitud_eliminar, contrato_gestion_gl, "", "", "", instalacion_destino, 1, true, auditoria);

                            if (detalle_solicitud_retorno_eliminar != null)
                            {
                                AgregarEliminarEspacioReservacionSTPAH(stp_integral_context, solicitudIntegral, detalle_solicitud_retorno_eliminar, contrato_gestion_gl, "", "", "", instalacion_destino_retorno, 1, true, auditoria);
                            }
                        }
                        else
                        {
                            AgregarEliminarEspacioReservacionSTP(stp_integral_context, solicitudIntegral, detalle_solicitud_eliminar, contrato_gestion_gl, "", "", "", instalacion_destino, 1, true, auditoria);

                            if (detalle_solicitud_retorno_eliminar != null)
                            {
                                AgregarEliminarEspacioReservacionSTP(stp_integral_context, solicitudIntegral, detalle_solicitud_retorno_eliminar, contrato_gestion_gl, "", "", "", instalacion_destino_retorno, 1, true, auditoria);
                            }
                        }
                        
                    }

                    transaction_scope.Complete();
                }
                catch (ShowCaseException ex)
                {
                    throw ex;
                }
                catch (System.Exception ex)
                {
                    throw new ShowCaseException(String.Format("ERR-SI-0044: Ocurrio el siguiente error al intentar eliminar un detalle en la Solicitud Integral [0]: '{1}'", solicitudIntegral.FolioSolicitudIntegral, ex.Message), ex, TypeMessage.error, "ERR-SI-0044");
                }
            }

            //En este punto ya se pasaron las validaciones, aqui se procede a eliminar al empleado y a recuperar el espacio.
            //Si esto es correcto se procede a eliminar el detalle de la solicitud
            //Dependiendo del tipo de servicio hay que recuperar el espacio reservado de AH y/o STP
            foreach (STPIntegral.Datos.TabDetallesSolicitudIntegral detalle in resultado)
            {
                detalle.TabSolicitudIntegral = null;
                detalle.ah_tb_capacidad_detalle = null;
            }

            return resultado;
        }

        private static void AgregarAuditoria(STPIntegral.Datos.STPIntegralModel stp_integral_context, STPIntegral.Datos.TabAuditoriaSolicitudIntegral auditoriasolicitudIntegral)
        {
            //GUARDAMOS LA AUDITORIA
            stp_integral_context.TabAuditoriaSolicitudIntegral.Add(auditoriasolicitudIntegral);
            stp_integral_context.SaveChanges();
        } 
        //AUDITAR??
        private static void AgregarEliminarEspacioReservacionSTPAH(STPIntegral.Datos.STPIntegralModel stp_integral_context, STPIntegral.Datos.TabSolicitudIntegral solicitudIntegral, STPIntegral.Datos.TabDetallesSolicitudIntegral detalleSolicitudIntegral, GestionGL.Datos.C_CONTRATOS contrato_gestion_gl, string folio_especial_transporte_ida, string folio_especial_transporte_retorno, string folio_especial_hospedaje, GestionGL.Datos.C_M_INSTALACIONES instalacionDestino, int capacidad_requerida, bool aumentar_capacidad,STPIntegral.Datos.TabAuditoriaSolicitudIntegral auditoria)
        {
            //EN ESTE PUNTO HAY QUE RECUPERAR EL ESPACIO UTILIZADO

            //Empezamos con transporte, el orden de prioridad es el siguiente, Primero se validan los folios especiales, luego los contratos, seguido de los centros gestores, por ultimo los espacios libres totales.
            if (solicitudIntegral.TipoSolicitudIntegral != "SOLO HOSPEDAJE")
            {
                AgregarEliminarEspacioReservacionSTP(stp_integral_context, solicitudIntegral, detalleSolicitudIntegral, contrato_gestion_gl, folio_especial_transporte_ida, folio_especial_transporte_retorno, folio_especial_hospedaje, instalacionDestino, capacidad_requerida, aumentar_capacidad, auditoria);
            }

            //Modificando las capacidades de hospedaje
            if (solicitudIntegral.TipoSolicitudIntegral != "SOLO TRANSPORTE" && detalleSolicitudIntegral.TipoMovimientoPersonal == "SUBIDA")
            {
                AgregarEliminarEspacioReservacionAH(stp_integral_context, solicitudIntegral, detalleSolicitudIntegral, contrato_gestion_gl, folio_especial_transporte_ida, folio_especial_transporte_retorno, folio_especial_hospedaje, instalacionDestino, capacidad_requerida, aumentar_capacidad, auditoria);
            }//Fin disminucion de ocupacion en hospedaje.
        }

        public static void AgregarEliminarEspacioReservacionSTP(STPIntegral.Datos.STPIntegralModel stp_integral_context, STPIntegral.Datos.TabSolicitudIntegral solicitudIntegral, STPIntegral.Datos.TabDetallesSolicitudIntegral detalleSolicitudIntegral, GestionGL.Datos.C_CONTRATOS contrato_gestion_gl, string folio_especial_transporte_ida, string folio_especial_transporte_retorno, string folio_especial_hospedaje, GestionGL.Datos.C_M_INSTALACIONES instalacionDestino, int capacidad_requerida, bool aumentar_capacidad, STPIntegral.Datos.TabAuditoriaSolicitudIntegral auditoria)
        {
            //CUANDO ES UNA SOLICITUD INTEGRAL QUE INCLUYE TRANSPORTE.
            bool se_aumento_disminuyo_disponibilidad_transporte = false;
            //La parte de la validacion del folio especial queda pendiente.

            DateTime fecha_inicio_consulta = detalleSolicitudIntegral.FechaSubida.Date;
            DateTime fecha_fin_consulta = detalleSolicitudIntegral.FechaSubida.Date.AddDays(1);

            List<STPIntegral.Datos.Vw_SoltIntItinerariosCapacidadDia_NOLOCK> listado_stp_capacidad = stp_integral_context.Vw_SoltIntItinerariosCapacidadDia_NOLOCK.Where(x => x.id_itinerario == detalleSolicitudIntegral.fk_itinerario && (x.fecha_capacidad >= fecha_inicio_consulta && x.fecha_capacidad < fecha_fin_consulta)).ToList();

            //Validar capacidad por contrato
            if ((aumentar_capacidad == false && listado_stp_capacidad != null && listado_stp_capacidad.Count > 0) || (aumentar_capacidad == true))
            {
                if (aumentar_capacidad == true)
                {
                    STPIntegral.Datos.stp_tb_itinerario_ocupacion capacidad_stp = stp_integral_context.stp_tb_itinerario_ocupacion.FirstOrDefault(x => x.fkDetalleSolicitudIntegral == detalleSolicitudIntegral.IdDetalleSolicitudIntegral);

                    if (capacidad_stp != null)
                    {
                        capacidad_stp.fkDetalleSolicitudIntegral = null;
                        capacidad_stp.disponible = true;
                        capacidad_stp.fec_actua = System.DateTime.Now;
                    }
                    else
                    {
                        throw new NotImplementedException("No hay detalles de ocupacion reservada para el detalle de la solicitud integral: " + detalleSolicitudIntegral.IdDetalleSolicitudIntegral.ToString());
                    }

                    stp_integral_context.SaveChanges();
                    se_aumento_disminuyo_disponibilidad_transporte = true;
                    // SI INSERTA AUDITORIA
                    auditoria.TipoMovimientoAuditoria = "LIBERAR ESPACIOS STP";
                    auditoria.DescripcionAuditoria = "LIBERA EL ESPACIO  DEL PASAJERO/HUESPED: " + detalleSolicitudIntegral.FichaRFCEmpleado + " (" + detalleSolicitudIntegral.NombreEmpleado + ") DE LA SOLICITUD: " + solicitudIntegral.FolioSolicitudIntegral + " CON ID_DETALLE: " + detalleSolicitudIntegral.IdDetalleSolicitudIntegral;
                    auditoria.FkIdSolicitudIntegral = solicitudIntegral.IdSolicitudIntegral;
                    auditoria.ObjetoAntesModificacion = "";//js.Serialize(detalle_solicitud_integral_ANTERIOR);
                    auditoria.ObjetoDespuesModificacion = "";//js.Serialize(detalle_solicitud_integral);
                    AgregarAuditoria(stp_integral_context, auditoria);
                }
                else
                {
                    if (se_aumento_disminuyo_disponibilidad_transporte == false && (detalleSolicitudIntegral.TipoMovimientoPersonal == "SUBIDA" && !String.IsNullOrEmpty(folio_especial_transporte_ida)) || (detalleSolicitudIntegral.TipoMovimientoPersonal != "SUBIDA" && !String.IsNullOrEmpty(folio_especial_transporte_retorno)))
                    {
                        STPIntegral.Datos.Vw_SoltIntItinerariosCapacidadDia_NOLOCK itinerario_dist_capacidad_folio_especial = (from x in listado_stp_capacidad where x.folio_especial == (detalleSolicitudIntegral.TipoMovimientoPersonal == "SUBIDA" ? folio_especial_transporte_ida : folio_especial_transporte_retorno) select x).FirstOrDefault();
                        //var capacidad_detalle = stp_integral_context.stp_tb_capacidad_detalle.Where(x => x.fk_capacidad == stp_capacidad.id_capacidad && x.fk_contrato == contrato_gestion_gl.ID_C_CONTRATO).FirstOrDefault();
                        if (itinerario_dist_capacidad_folio_especial != null && itinerario_dist_capacidad_folio_especial.capacidad_disponible > 0)
                        {
                            STPIntegral.Datos.stp_tb_itinerario_ocupacion capacidad_stp = stp_integral_context.stp_tb_itinerario_ocupacion.FirstOrDefault(x => x.id_itinerario_dist == itinerario_dist_capacidad_folio_especial.id_itinerario_dist && x.fecha == itinerario_dist_capacidad_folio_especial.fecha_capacidad && x.disponible == true);

                            if (capacidad_stp != null)
                            {
                                capacidad_stp.disponible = false;
                                capacidad_stp.fkDetalleSolicitudIntegral = detalleSolicitudIntegral.IdDetalleSolicitudIntegral;
                                capacidad_stp.fec_actua = System.DateTime.Now;
                            }
                            else
                            {
                                throw new NotImplementedException("No hay registros de capacidad por Folio Especial: " + (detalleSolicitudIntegral.TipoMovimientoPersonal == "SUBIDA" ? folio_especial_transporte_ida : folio_especial_transporte_retorno));
                            }

                            stp_integral_context.SaveChanges();
                            se_aumento_disminuyo_disponibilidad_transporte = true;
                        }
                    }

                    if (se_aumento_disminuyo_disponibilidad_transporte == false && contrato_gestion_gl != null && contrato_gestion_gl.CONTRATO != "")
                    {
                        STPIntegral.Datos.Vw_SoltIntItinerariosCapacidadDia_NOLOCK itinerario_dist_capacidad_contrato = (from x in listado_stp_capacidad where x.CONTRATO == contrato_gestion_gl.CONTRATO select x).FirstOrDefault();
                        //var capacidad_detalle = stp_integral_context.stp_tb_capacidad_detalle.Where(x => x.fk_capacidad == stp_capacidad.id_capacidad && x.fk_contrato == contrato_gestion_gl.ID_C_CONTRATO).FirstOrDefault();
                        if (itinerario_dist_capacidad_contrato != null && itinerario_dist_capacidad_contrato.capacidad_disponible > 0)
                        {
                            STPIntegral.Datos.stp_tb_itinerario_ocupacion capacidad_stp = stp_integral_context.stp_tb_itinerario_ocupacion.FirstOrDefault(x => x.id_itinerario_dist == itinerario_dist_capacidad_contrato.id_itinerario_dist && x.fecha == itinerario_dist_capacidad_contrato.fecha_capacidad && x.disponible == true);

                            if (capacidad_stp != null)
                            {
                                capacidad_stp.disponible = false;
                                capacidad_stp.fkDetalleSolicitudIntegral = detalleSolicitudIntegral.IdDetalleSolicitudIntegral;
                                capacidad_stp.fec_actua = System.DateTime.Now;
                            }
                            else
                            {
                                throw new NotImplementedException("No hay registros de capacidad por Contrato: " + contrato_gestion_gl.CONTRATO);
                            }

                            stp_integral_context.SaveChanges();
                            se_aumento_disminuyo_disponibilidad_transporte = true;
                        }
                    }

                    if (se_aumento_disminuyo_disponibilidad_transporte == false && !String.IsNullOrEmpty(solicitudIntegral.SubdivisionAutorizador))
                    {
                        STPIntegral.Datos.Vw_SoltIntItinerariosCapacidadDia_NOLOCK itinerario_dist_capacidad_subdivision = (from x in listado_stp_capacidad where x.SUBDIVISION == solicitudIntegral.SubdivisionAutorizador select x).FirstOrDefault();
                        //var capacidad_detalle = stp_integral_context.stp_tb_capacidad_detalle.Where(x => x.fk_capacidad == stp_capacidad.id_capacidad && x.fk_contrato == contrato_gestion_gl.ID_C_CONTRATO).FirstOrDefault();
                        if (itinerario_dist_capacidad_subdivision != null && itinerario_dist_capacidad_subdivision.capacidad_disponible > 0)
                        {

                            STPIntegral.Datos.stp_tb_itinerario_ocupacion capacidad_stp = stp_integral_context.stp_tb_itinerario_ocupacion.FirstOrDefault(x => x.id_itinerario_dist == itinerario_dist_capacidad_subdivision.id_itinerario_dist && x.fecha == itinerario_dist_capacidad_subdivision.fecha_capacidad && x.disponible == true);

                            if (capacidad_stp != null)
                            {
                                capacidad_stp.disponible = false;
                                capacidad_stp.fkDetalleSolicitudIntegral = detalleSolicitudIntegral.IdDetalleSolicitudIntegral;
                                capacidad_stp.fec_actua = System.DateTime.Now;
                            }
                            else
                            {
                                throw new NotImplementedException("No hay registros de reservaciones de capacidad para la subdivision:" + solicitudIntegral.SubdivisionAutorizador);
                            }

                            stp_integral_context.SaveChanges();
                            se_aumento_disminuyo_disponibilidad_transporte = true;
                        }
                    }

                    //Verificando capacidad fondo comun
                    if (se_aumento_disminuyo_disponibilidad_transporte == false)
                    {
                        STPIntegral.Datos.Vw_SoltIntItinerariosCapacidadDia_NOLOCK itinerario_dist_capacidad_fondo_comun = (from x in listado_stp_capacidad where x.ID_C_SUBDIVISION.HasValue == false && x.ID_C_CONTRATO.HasValue == false && String.IsNullOrEmpty(x.folio_especial) == true select x).FirstOrDefault();
                        //var capacidad_detalle = stp_integral_context.stp_tb_capacidad_detalle.Where(x => x.fk_capacidad == stp_capacidad.id_capacidad && x.fk_contrato == contrato_gestion_gl.ID_C_CONTRATO).FirstOrDefault();
                        if (itinerario_dist_capacidad_fondo_comun != null && itinerario_dist_capacidad_fondo_comun.capacidad_disponible > 0)
                        {
                            STPIntegral.Datos.stp_tb_itinerario_ocupacion capacidad_stp = stp_integral_context.stp_tb_itinerario_ocupacion.FirstOrDefault(x => x.id_itinerario_dist == itinerario_dist_capacidad_fondo_comun.id_itinerario_dist && x.fecha == itinerario_dist_capacidad_fondo_comun.fecha_capacidad && x.disponible == true);

                            if (capacidad_stp != null)
                            {
                                capacidad_stp.disponible = false;
                                capacidad_stp.fkDetalleSolicitudIntegral = detalleSolicitudIntegral.IdDetalleSolicitudIntegral;
                                capacidad_stp.fec_actua = System.DateTime.Now;
                            }
                            else
                            {
                                throw new NotImplementedException("No hay registros de reservaciones de capacidad para el fondo comun");
                            }

                            stp_integral_context.SaveChanges();
                            se_aumento_disminuyo_disponibilidad_transporte = true;
                        }
                    }
                    // SI INSERTA AUDITORIA
                    auditoria.TipoMovimientoAuditoria = "OCUPAR ESPACIOS STP";
                    auditoria.DescripcionAuditoria = "OCUPA EL ESPACIO EL PASAJERO/HUESPED: " + detalleSolicitudIntegral.FichaRFCEmpleado + " (" + detalleSolicitudIntegral.NombreEmpleado + ") DE LA SOLICITUD: " + solicitudIntegral.FolioSolicitudIntegral + " CON ID_DETALLE: " + detalleSolicitudIntegral.IdDetalleSolicitudIntegral;
                    auditoria.FkIdSolicitudIntegral = solicitudIntegral.IdSolicitudIntegral;
                    auditoria.ObjetoAntesModificacion = "";//js.Serialize(detalle_solicitud_integral_ANTERIOR);
                    auditoria.ObjetoDespuesModificacion = "";//js.Serialize(detalle_solicitud_integral);
                    AgregarAuditoria(stp_integral_context, auditoria);

                }
            }
            else
            {
                throw new NotImplementedException("No hay registros de capacidad por dia NOLOCK");
            }
        }

        public static void AgregarEliminarEspacioReservacionAH(STPIntegral.Datos.STPIntegralModel stp_integral_context, STPIntegral.Datos.TabSolicitudIntegral solicitudIntegral, STPIntegral.Datos.TabDetallesSolicitudIntegral detalleSolicitudIntegral, GestionGL.Datos.C_CONTRATOS contrato_gestion_gl, string folio_especial_transporte_ida, string folio_especial_transporte_retorno, string folio_especial_hospedaje, GestionGL.Datos.C_M_INSTALACIONES instalacionDestino, int capacidad_requerida, bool aumentar_capacidad,STPIntegral.Datos.TabAuditoriaSolicitudIntegral auditoria)
        {
            var se_aumento_disminuyo_la_capacidad_de_hospedaje = false;

            //obtener las capacidades para todo el rango de fechas en la instalacion destino.
            var listado_reservaciones_hospedaje = stp_integral_context.VW_TabReservacionesCapacidadHospedaje_NOLOCK.Where(x => x.FkIdInstalacion == instalacionDestino.ID_C_M_INSTALACION && (x.FechaCapacidad >= detalleSolicitudIntegral.FechaSubida && x.FechaCapacidad <= detalleSolicitudIntegral.FechaBajada)).ToList();

            //ahora hay que verificar la disponibilidad para cada uno de los dias en que esta programada la estancia.
            DateTime dia_verificacion = detalleSolicitudIntegral.FechaSubida.Date;

            List<STPIntegral.Datos.VW_TabReservacionesCapacidadHospedaje_NOLOCK> listado_disponibilidad_por_dia = null;
            List<STPIntegral.Datos.VW_TabReservacionesCapacidadHospedaje_NOLOCK> consulta_disponibilidad = null;

            if ((aumentar_capacidad == false && solicitudIntegral.FechaSubida.Date != solicitudIntegral.FechaBajada.Date) || aumentar_capacidad == true)
            {
                //Solo se ocupan espacios si la fecha de bajada es mayor a la fecha de subida.

                do
                {
                    //Se inicializa a false la disponibilidad del dia
                    se_aumento_disminuyo_la_capacidad_de_hospedaje = false;

                    //Verificar disponibilidad para cada dia
                    //Primero se verifica si la cosulta regreso alguna reservacion para ese dia y esa instalacion, en caso contrario se decreta que no hay disponibilidad
                    listado_disponibilidad_por_dia = (from x in listado_reservaciones_hospedaje where x.FechaCapacidad == dia_verificacion && x.FkIdInstalacion == instalacionDestino.ID_C_M_INSTALACION select x).ToList(); //esta funcion regresa nulo si no encuentra nada para ese dia.

                    if (listado_disponibilidad_por_dia == null)
                    {
                        //No hay reservacion para este dia.
                        if (aumentar_capacidad)
                        {
                            throw new ShowCaseException(String.Format("ERR-SI-0042: No se puede aumentar la capacidad del hospedaje pues no se encontro información de reservacion en la fecha: '{0:dd/MM/yyyy}' de la instalación: '{1}'", dia_verificacion, instalacionDestino.NOMBRE_INSTALACION), TypeMessage.alert, "ERR-SI-0042");
                        }
                        else
                        {
                            throw new ShowCaseException(String.Format("ERR-SI-0033: No se puede disminuir la capacidad de hospedaje pues no se encontro información de reservacion de hospedaje en la fecha: '{0:dd/MM/yyyy}' de la instalación: '{1}'", dia_verificacion, instalacionDestino.NOMBRE_INSTALACION), TypeMessage.alert, "ERR-SI-0033");
                        }
                    }

                    if (aumentar_capacidad)
                    {

                        STPIntegral.Datos.VW_AhTbCapacidadDetalle vw_reservacion_detalle = null;
                        STPIntegral.Datos.ah_tb_capacidad_detalle reservacion_detalle = null;

                        vw_reservacion_detalle = stp_integral_context.VW_AhTbCapacidadDetalle.Where(x => x.FkIdInstalacion == instalacionDestino.ID_C_M_INSTALACION && x.FkIdDetalleSolicitudIntegral == detalleSolicitudIntegral.IdDetalleSolicitudIntegral).FirstOrDefault();

                        if (vw_reservacion_detalle != null)
                        {
                            reservacion_detalle = stp_integral_context.ah_tb_capacidad_detalle.Where(x => x.IdAhCapacidadDetalle == vw_reservacion_detalle.IdAhCapacidadDetalle).FirstOrDefault();
                        }

                        if (reservacion_detalle != null)
                        {
                            reservacion_detalle.Disponible = true;
                            reservacion_detalle.FkIdDetalleSolicitudIntegral = null;

                            stp_integral_context.SaveChanges();

                            se_aumento_disminuyo_la_capacidad_de_hospedaje = true;

                            // SI INSERTA AUDITORIA
                            auditoria.TipoMovimientoAuditoria = "LIBERAR ESPACIOS AH";
                            auditoria.DescripcionAuditoria = "LIBERA EL ESPACIO DEL PASAJERO/HUESPED: " + detalleSolicitudIntegral.FichaRFCEmpleado + " (" + detalleSolicitudIntegral.NombreEmpleado + ") DE LA SOLICITUD: " + solicitudIntegral.FolioSolicitudIntegral + " CON ID_DETALLE: " + detalleSolicitudIntegral.IdDetalleSolicitudIntegral + " PARA EL DIA: " + dia_verificacion + " EN LA INSTALACION: " + instalacionDestino.NOMBRE_INSTALACION;
                            auditoria.FkIdSolicitudIntegral = solicitudIntegral.IdSolicitudIntegral;
                            auditoria.ObjetoAntesModificacion = "";//js.Serialize(detalle_solicitud_integral_ANTERIOR);
                            auditoria.ObjetoDespuesModificacion = "";//js.Serialize(detalle_solicitud_integral);
                            AgregarAuditoria(stp_integral_context, auditoria);
                        }
                        else
                        {
                            if (detalleSolicitudIntegral.FechaSubida.Date == detalleSolicitudIntegral.FechaBajada.Date)
                            {
                                se_aumento_disminuyo_la_capacidad_de_hospedaje = true;
                            }
                        }
                    }
                    else
                    {
                        STPIntegral.Datos.ah_tb_capacidad_detalle reservacion_detalle = null;
                        int id_ah_reservacion_capacidad = 0;

                        //Si la consulta regreso algun resultado entonces primero se verifica que haya disponibilidad para el folio especial (si es que lo proprocionaron)
                        if (!String.IsNullOrEmpty(folio_especial_hospedaje) && se_aumento_disminuyo_la_capacidad_de_hospedaje == false)
                        {
                            consulta_disponibilidad = (from x in listado_disponibilidad_por_dia where x.TipoReservacion == "FOLIO ESPECIAL" && x.FolioEspecial == folio_especial_hospedaje select x).ToList();

                            if (consulta_disponibilidad != null)
                            {
                                foreach (STPIntegral.Datos.VW_TabReservacionesCapacidadHospedaje_NOLOCK consulta_disponibilidad_detallado in consulta_disponibilidad)
                                {
                                    if (consulta_disponibilidad != null && consulta_disponibilidad.Count > 0 && (aumentar_capacidad == true || (consulta_disponibilidad_detallado.TotalDisponibles > capacidad_requerida && aumentar_capacidad == false)))
                                    {
                                        id_ah_reservacion_capacidad = consulta_disponibilidad_detallado.IdAhReservacionCapacidad;

                                        reservacion_detalle = stp_integral_context.ah_tb_capacidad_detalle.Where(x => x.FkIdAhReservacionCapacidad == id_ah_reservacion_capacidad && x.FechaCapacidad == consulta_disponibilidad_detallado.FechaCapacidad && x.Disponible == true && x.Bloqueado == false).FirstOrDefault();

                                        reservacion_detalle.Disponible = false;
                                        reservacion_detalle.FkIdDetalleSolicitudIntegral = detalleSolicitudIntegral.IdDetalleSolicitudIntegral;

                                        se_aumento_disminuyo_la_capacidad_de_hospedaje = true;
                                        break;
                                    }
                                }
                            }
                        }

                        //luego se verifica que haya disponibilidad para el contrato (si es que lo proprocionaron)
                        if (contrato_gestion_gl != null && !String.IsNullOrEmpty(contrato_gestion_gl.CONTRATO) && se_aumento_disminuyo_la_capacidad_de_hospedaje == false)
                        {
                            consulta_disponibilidad = (from x in listado_disponibilidad_por_dia where x.TipoReservacion == "CONTRATO" && x.CONTRATO == contrato_gestion_gl.CONTRATO select x).ToList();

                            if (consulta_disponibilidad != null)
                            {
                                foreach (STPIntegral.Datos.VW_TabReservacionesCapacidadHospedaje_NOLOCK consulta_disponibilidad_detallado in consulta_disponibilidad)
                                {
                                    if (consulta_disponibilidad != null && consulta_disponibilidad.Count > 0 && (aumentar_capacidad == true || (consulta_disponibilidad_detallado.TotalDisponibles > capacidad_requerida && aumentar_capacidad == false)))
                                    {
                                        id_ah_reservacion_capacidad = consulta_disponibilidad_detallado.IdAhReservacionCapacidad;

                                        reservacion_detalle = stp_integral_context.ah_tb_capacidad_detalle.Where(x => x.FkIdAhReservacionCapacidad == id_ah_reservacion_capacidad && x.FechaCapacidad == consulta_disponibilidad_detallado.FechaCapacidad && x.Disponible == true && x.Bloqueado == false).FirstOrDefault();

                                        reservacion_detalle.Disponible = false;
                                        reservacion_detalle.FkIdDetalleSolicitudIntegral = detalleSolicitudIntegral.IdDetalleSolicitudIntegral;

                                        se_aumento_disminuyo_la_capacidad_de_hospedaje = true;
                                        break;
                                    }
                                }
                            }
                        }

                        //Luego validamos que exista disponibilidad a nivel Subdivision en caso de que con la validacion anterior no haya habido
                        if (!String.IsNullOrEmpty(solicitudIntegral.SubdivisionAutorizador) && se_aumento_disminuyo_la_capacidad_de_hospedaje == false)
                        {
                            consulta_disponibilidad = (from x in listado_disponibilidad_por_dia where x.TipoReservacion == "SUBDIVISION" && x.SUBDIVISION.StartsWith(solicitudIntegral.SubdivisionAutorizador.Substring(0, 3)) select x).ToList();

                            if (consulta_disponibilidad != null)
                            {
                                foreach (STPIntegral.Datos.VW_TabReservacionesCapacidadHospedaje_NOLOCK consulta_disponibilidad_detallado in consulta_disponibilidad)
                                {

                                    if (consulta_disponibilidad != null && consulta_disponibilidad.Count > 0 && (aumentar_capacidad == true || (consulta_disponibilidad_detallado.TotalDisponibles >= capacidad_requerida && aumentar_capacidad == false)))
                                    {
                                        id_ah_reservacion_capacidad = consulta_disponibilidad_detallado.IdAhReservacionCapacidad;

                                        reservacion_detalle = stp_integral_context.ah_tb_capacidad_detalle.Where(x => x.FkIdAhReservacionCapacidad == id_ah_reservacion_capacidad && x.FechaCapacidad == consulta_disponibilidad_detallado.FechaCapacidad && x.Disponible == true && x.Bloqueado == false).FirstOrDefault();

                                        reservacion_detalle.Disponible = false;
                                        reservacion_detalle.FkIdDetalleSolicitudIntegral = detalleSolicitudIntegral.IdDetalleSolicitudIntegral;

                                        se_aumento_disminuyo_la_capacidad_de_hospedaje = true;
                                        break;
                                    }
                                }
                            }
                        }

                        //Por ultimo quitamos espacios del fondo comun en caso de que no haya habido en las validaciones anteriores
                        if (se_aumento_disminuyo_la_capacidad_de_hospedaje == false)
                        {
                            consulta_disponibilidad = (from x in listado_disponibilidad_por_dia where x.TipoReservacion == "FONDO COMUN" select x).ToList();

                            if (consulta_disponibilidad != null)
                            {
                                foreach (STPIntegral.Datos.VW_TabReservacionesCapacidadHospedaje_NOLOCK consulta_disponibilidad_detallado in consulta_disponibilidad)
                                {

                                    if (consulta_disponibilidad != null && consulta_disponibilidad.Count > 0 && (aumentar_capacidad == true || (consulta_disponibilidad_detallado.TotalDisponibles >= capacidad_requerida && aumentar_capacidad == false)))
                                    {
                                        id_ah_reservacion_capacidad = consulta_disponibilidad_detallado.IdAhReservacionCapacidad;

                                        reservacion_detalle = stp_integral_context.ah_tb_capacidad_detalle.Where(x => x.FkIdAhReservacionCapacidad == id_ah_reservacion_capacidad && x.FechaCapacidad == consulta_disponibilidad_detallado.FechaCapacidad && x.Disponible == true && x.Bloqueado == false).FirstOrDefault();

                                        reservacion_detalle.Disponible = false;
                                        reservacion_detalle.FkIdDetalleSolicitudIntegral = detalleSolicitudIntegral.IdDetalleSolicitudIntegral;

                                        se_aumento_disminuyo_la_capacidad_de_hospedaje = true;
                                        break;
                                    }
                                }
                            }
                        }

                        if (reservacion_detalle != null)
                        {
                            stp_integral_context.SaveChanges();

                        }
                        // SI INSERTA AUDITORIA
                        auditoria.TipoMovimientoAuditoria = "OCUPAR ESPACIOS AH";
                        auditoria.DescripcionAuditoria = "OCUPA EL ESPACIO DEL PASAJERO/HUESPED: " + detalleSolicitudIntegral.FichaRFCEmpleado + " (" + detalleSolicitudIntegral.NombreEmpleado + ") DE LA SOLICITUD: " + solicitudIntegral.FolioSolicitudIntegral + " CON ID_DETALLE: " + detalleSolicitudIntegral.IdDetalleSolicitudIntegral + " PARA EL DIA: " + dia_verificacion + " EN LA INSTALACION: " + instalacionDestino.NOMBRE_INSTALACION;
                        auditoria.FkIdSolicitudIntegral = solicitudIntegral.IdSolicitudIntegral;
                        auditoria.ObjetoAntesModificacion = "";//js.Serialize(detalle_solicitud_integral_ANTERIOR);
                        auditoria.ObjetoDespuesModificacion = "";//js.Serialize(detalle_solicitud_integral);
                        AgregarAuditoria(stp_integral_context, auditoria);
                    }

                    //Si no hay disponibilidad se muestra un mensaje de error.
                    if (!se_aumento_disminuyo_la_capacidad_de_hospedaje)
                    {
                        if (aumentar_capacidad)
                        {
                            throw new ShowCaseException(String.Format("ERR-SI-0043: No se pudo aumentar la capacidad de hospedaje el día : '{0:dd/MM/yyyy}' para la instalación: '{1}'", dia_verificacion, instalacionDestino.NOMBRE_INSTALACION), TypeMessage.alert, "ERR-SI-0043");
                        }
                        else
                        {
                            throw new ShowCaseException(String.Format("ERR-SI-0034: No se puede ocupar la capacidad de hospedaje pues no hay disponibilidad en la fecha: '{0:dd/MM/yyyy}' para la instalación: '{1}'", dia_verificacion, instalacionDestino.NOMBRE_INSTALACION), TypeMessage.alert, "ERR-SI-0034");
                        }

                    }
                    else
                    {
                        dia_verificacion = dia_verificacion.AddDays(1);
                    }
                }
                while (dia_verificacion <= detalleSolicitudIntegral.FechaBajada && se_aumento_disminuyo_la_capacidad_de_hospedaje == true);
            }
        }

        //AUDITAR
        public static STPIntegral.Datos.TabSolicitudIntegral CancelarSolicituIntegral(int id_solicitud_integral, int id_permiso_ua, STPIntegral.Datos.TabAuditoriaSolicitudIntegral auditoria)
        {
            //Primero se valida que el estado de la solicitud sea SOLICITADO
            //Luego se valida que el estado del pasajero sea SOLICITADO
            STPIntegral.Datos.TabSolicitudIntegral resultado = null;

            List<STPIntegral.Datos.TabDetallesSolicitudIntegral> detalles_solicitud_integral = null;

            GestionGL.Datos.VW_PERMISOS_UA permiso_ua_usuario = null;

            using (GestionGL.Datos.GestionGLModel gestiongl_model = new GestionGL.Datos.GestionGLModel())
            {
                permiso_ua_usuario = gestiongl_model.VW_PERMISOS_UA.SingleOrDefault(x => x.ID_PERMISOS_UA == id_permiso_ua);
            }

            using (STPIntegral.Datos.STPIntegralModel stpIntegral_contex = new Datos.STPIntegralModel())
            {
                stpIntegral_contex.Configuration.LazyLoadingEnabled = false;
                stpIntegral_contex.Configuration.ProxyCreationEnabled = false;

                resultado = stpIntegral_contex.TabSolicitudIntegral.Where(x => x.IdSolicitudIntegral == id_solicitud_integral).SingleOrDefault();

                if (resultado == null)
                {
                    throw new ShowCaseException(String.Format("ERR-SI-0046: No se encontro el folio de de la solicitud integral: '{0}'", resultado.FolioSolicitudIntegral), TypeMessage.error, "ERR-SI-0046");
                }
                else
                {
                    if (permiso_ua_usuario.ROL == RolesUsuarioSolicitudIntegral.USUARIO.ToString() && resultado.FichaCaptura != permiso_ua_usuario.FICHA)
                    {
                        throw new ShowCaseException(String.Format("ERR-SI-0064: Solo el usuario que capturo la solicitud integral esta autorizado para cancelar la solicitud: '{0}'", resultado.NombreCaptura), TypeMessage.error, "ERR-SI-0064");
                    }
                }

                //DE MOMENTO SE VA A QUITAR ESTA VALIDACION
                if (resultado.EstadoSolicitudIntegral != EstadosSolicitudIntegral.SOLICITADO.ToString() && permiso_ua_usuario.ROL != RolesUsuarioSolicitudIntegral.VENTANILLA.ToString())
                {
                    throw new ShowCaseException(String.Format("ERR-SI-0047: No se puede cancelar la solicitud integral pues su estado es diferente a 'SOLICITADO': {0} [{1}]'", resultado.FolioSolicitudIntegral, resultado.EstadoSolicitudIntegral), TypeMessage.error, "ERR-SI-0047");
                }

                detalles_solicitud_integral = stpIntegral_contex.TabDetallesSolicitudIntegral.Where(x => x.IdSolicitudIntegral == resultado.IdSolicitudIntegral).ToList();

                foreach (STPIntegral.Datos.TabDetallesSolicitudIntegral detalle_solicitud in detalles_solicitud_integral)
                {
                    if (resultado.EstadoSolicitudIntegral != EstadosDetallesSolicitudIntegral.SOLICITADO.ToString() && permiso_ua_usuario.ROL != RolesUsuarioSolicitudIntegral.VENTANILLA.ToString())
                    {
                        throw new ShowCaseException(String.Format("ERR-SI-0048: No se puede cancelar el detalle pues su estado en la solicitud integral es diferente de 'SOLICITADO': {0} {1} -[{2}]'", detalle_solicitud.FichaRFCEmpleado, detalle_solicitud.NombreEmpleado, detalle_solicitud.EstadoOcupanteSolicitudIntegral), TypeMessage.error, "ERR-SI-0048");
                    }
                }
            }

            GestionGL.Datos.C_CONTRATOS contrato_gestion_gl = null;

            List<GestionGL.Datos.C_M_INSTALACIONES> listado_instalaciones_destino = null;

            GestionGL.Datos.C_M_INSTALACIONES instalacionDestino = null;

            using (GestionGL.Datos.GestionGLModel gestionGl_context = new GestionGL.Datos.GestionGLModel())
            {
                gestionGl_context.Configuration.LazyLoadingEnabled = false;
                gestionGl_context.Configuration.ProxyCreationEnabled = false;

                if (!String.IsNullOrEmpty(resultado.NumeroContrato.Trim()))
                {
                    contrato_gestion_gl = gestionGl_context.C_CONTRATOS.Where(x => x.CONTRATO == resultado.NumeroContrato).SingleOrDefault();
                }

                if (detalles_solicitud_integral != null)
                {
                    List<int> ids_instalaciones_destino_personal_subida = (from y in detalles_solicitud_integral where y.TipoMovimientoPersonal == "SUBIDA" select y.IdInstalacionDestino).ToList();
                    
                    listado_instalaciones_destino = (from x in gestionGl_context.C_M_INSTALACIONES
                                                     where ids_instalaciones_destino_personal_subida.Contains(x.ID_C_M_INSTALACION)
                                                     select x).ToList();
                }
            }

            using (TransactionScope transaction_scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
            {
                try
                {
                    //Como toda la parte transaccional quedo solo en STPIntegral, entonces se crea un nuevo contexto para que todo esto se transaccione.
                    using (Datos.STPIntegralModel stp_integral_context = new Datos.STPIntegralModel())
                    {
                        stp_integral_context.Configuration.LazyLoadingEnabled = false;
                        stp_integral_context.Configuration.ProxyCreationEnabled = false;

                        //CANCELANDO LA SOLICITUD INTEGRAL
                        resultado = stp_integral_context.TabSolicitudIntegral.Where(x => x.IdSolicitudIntegral == id_solicitud_integral).SingleOrDefault();
                        resultado.EstadoSolicitudIntegral = EstadosSolicitudIntegral.CANCELADO.ToString();
                        stp_integral_context.SaveChanges();

                        // SE INSERTA AUDITORIA
                        auditoria.TipoMovimientoAuditoria = "CANCELA SOLICITUD";
                        auditoria.DescripcionAuditoria = "CANCELA SOLICITUD CON FOLIO: " + resultado.FolioSolicitudIntegral;
                        auditoria.FkIdSolicitudIntegral = resultado.IdSolicitudIntegral;
                        auditoria.ObjetoAntesModificacion = "";//js.Serialize(detalle_solicitud_integral_ANTERIOR);
                        auditoria.ObjetoDespuesModificacion = "";//js.Serialize(detalle_solicitud_integral);
                        AgregarAuditoria(stp_integral_context, auditoria);

                        //CANCELANDO LOS DETALLE DE LA SOLICITUD INTEGRAL
                        detalles_solicitud_integral = stp_integral_context.TabDetallesSolicitudIntegral.Where(x => x.IdSolicitudIntegral == resultado.IdSolicitudIntegral).ToList();

                        foreach (STPIntegral.Datos.TabDetallesSolicitudIntegral detalle in detalles_solicitud_integral)
                        {
                            detalle.EstadoHuespedAH = EstadosDetallesSolicitudIntegralAH.CANCELADO.ToString();
                            detalle.EstadoPasajeroSTP = EstadosDetallesSolicitudIntegralSTP.CANCELADO.ToString();
                            detalle.EstadoOcupanteSolicitudIntegral = EstadosDetallesSolicitudIntegralSTP.CANCELADO.ToString();

                            stp_integral_context.SaveChanges();

                            // SE INSERTA AUDITORIA
                            auditoria.TipoMovimientoAuditoria = "CANCELA PASAJERO";
                            auditoria.DescripcionAuditoria = "CANCELA AL PASAJERO CON FICHA/RFC: " + detalle.FichaRFCEmpleado + " (" + detalle.NombreEmpleado + ") DE LA SOLICITUD: " + resultado.FolioSolicitudIntegral;
                            auditoria.FkIdSolicitudIntegral = resultado.IdSolicitudIntegral;
                            auditoria.ObjetoAntesModificacion = "";//js.Serialize(detalle_solicitud_integral_ANTERIOR);
                            auditoria.ObjetoDespuesModificacion = "";//js.Serialize(detalle_solicitud_integral);
                            AgregarAuditoria(stp_integral_context, auditoria);

                            instalacionDestino = listado_instalaciones_destino.Where(x => x.ID_C_M_INSTALACION == detalle.IdInstalacionDestino).SingleOrDefault();
                            
                            //TODO: REVISAR QUE PASA AQUI CON LOS FOLIOS ESPECIALES
                            AgregarEliminarEspacioReservacionSTPAH(stp_integral_context, resultado, detalle, contrato_gestion_gl, "", "", "", instalacionDestino, 1, true, auditoria);
                        }
                    }

                    transaction_scope.Complete();
                }
                catch (ShowCaseException ex)
                {
                    throw ex;
                }
                catch (System.Exception ex)
                {
                    throw new ShowCaseException(String.Format("ERR-SI-0049: Ocurrio el siguiente error al intentar Cancelar la Solicitud Integral [{0}]: '{1}'", resultado.FolioSolicitudIntegral, ex.Message), ex, TypeMessage.error, "ERR-SI-0049");
                }
            }

            //En este punto ya se pasaron las validaciones, aqui se procede a eliminar al empleado y a recuperar el espacio.
            //Si esto es correcto se procede a eliminar el detalle de la solicitud
            //Dependiendo del tipo de servicio hay que recuperar el espacio reservado de AH y/o STP
            resultado.TabAuditoriaSolicitudIntegral = null;
            resultado.TabDetallesSolicitudIntegral = null;

            return resultado;
        }

        //AUDITAR
        public static STPIntegral.Datos.TabSolicitudIntegral ConfirmarSolicitudIntegral(int id_solicitud_integral, int id_permiso_ua,STPIntegral.Datos.TabAuditoriaSolicitudIntegral auditoria)
        {
            //Primero se valida que el estado de la solicitud sea SOLICITADO
            //Luego se valida que el estado del pasajero sea SOLICITADO
            STPIntegral.Datos.TabSolicitudIntegral resultado = null;

            List<STPIntegral.Datos.TabDetallesSolicitudIntegral> detalles_solicitud_integral = null;
            
            GestionGL.Datos.VW_PERMISOS_UA permiso_ua_usuario = null;
            
            using (GestionGL.Datos.GestionGLModel gestiongl_model = new GestionGL.Datos.GestionGLModel())
            {
                permiso_ua_usuario = gestiongl_model.VW_PERMISOS_UA.SingleOrDefault(x => x.ID_PERMISOS_UA == id_permiso_ua);
            }

            using (STPIntegral.Datos.STPIntegralModel stpIntegral_contex = new Datos.STPIntegralModel())
            {
                stpIntegral_contex.Configuration.LazyLoadingEnabled = false;
                stpIntegral_contex.Configuration.ProxyCreationEnabled = false;

                resultado = stpIntegral_contex.TabSolicitudIntegral.Where(x => x.IdSolicitudIntegral == id_solicitud_integral).SingleOrDefault();

                if (resultado == null)
                {
                    throw new ShowCaseException(String.Format("ERR-SI-0050: No se encontro el folio de de la solicitud integral: '{0}'", resultado.FolioSolicitudIntegral), TypeMessage.error, "ERR-SI-0050");
                }
                else
                {
                    if (permiso_ua_usuario.ROL != RolesUsuarioSolicitudIntegral.VENTANILLA.ToString())
                    {
                        throw new ShowCaseException(String.Format("ERR-SI-0065: Solo un usuario de ventanilla puede confirmar una solicitud integral"), TypeMessage.error, "ERR-SI-0065");
                    }
                }

                if (resultado.EstadoSolicitudIntegral != EstadosSolicitudIntegral.SOLICITADO.ToString())
                {
                    throw new ShowCaseException(String.Format("ERR-SI-0051: No se puede confirmar la solicitud integral pues su estado es diferente a 'SOLICITADO': {0} [{1}]'", resultado.FolioSolicitudIntegral, resultado.EstadoSolicitudIntegral), TypeMessage.error, "ERR-SI-0051");
                }

                detalles_solicitud_integral = stpIntegral_contex.TabDetallesSolicitudIntegral.Where(x => x.IdSolicitudIntegral == resultado.IdSolicitudIntegral).ToList();

                foreach (STPIntegral.Datos.TabDetallesSolicitudIntegral detalle_solicitud in detalles_solicitud_integral)
                {
                    if (resultado.EstadoSolicitudIntegral != EstadosDetallesSolicitudIntegral.SOLICITADO.ToString())
                    {
                        throw new ShowCaseException(String.Format("ERR-SI-0052: No se puede confirmar el detalle pues su estado en la solicitud integral es diferente de 'SOLICITADO': {0} {1} -[{2}]'", detalle_solicitud.FichaRFCEmpleado, detalle_solicitud.NombreEmpleado, detalle_solicitud.EstadoOcupanteSolicitudIntegral), TypeMessage.error, "ERR-SI-0052");
                    }
                }
            }

            using (TransactionScope transaction_scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
            {
                try
                {
                    //Como toda la parte transaccional quedo solo en STPIntegral, entonces se crea un nuevo contexto para que todo esto se transaccione.
                    using (Datos.STPIntegralModel stp_integral_context = new Datos.STPIntegralModel())
                    {
                        stp_integral_context.Configuration.LazyLoadingEnabled = false;
                        stp_integral_context.Configuration.ProxyCreationEnabled = false;

                        //CANCELANDO LA SOLICITUD INTEGRAL
                        resultado = stp_integral_context.TabSolicitudIntegral.Where(x => x.IdSolicitudIntegral == id_solicitud_integral).SingleOrDefault();
                        resultado.EstadoSolicitudIntegral = EstadosSolicitudIntegral.CONFIRMADO.ToString();
                        stp_integral_context.SaveChanges();

                        // SE INSERTA AUDITORIA
                        auditoria.TipoMovimientoAuditoria = "CONFIRMA SOLICITUD";
                        auditoria.DescripcionAuditoria = "CONFIRMA SOLICITUD CON FOLIO: " + resultado.FolioSolicitudIntegral;
                        auditoria.FkIdSolicitudIntegral = resultado.IdSolicitudIntegral;
                        auditoria.ObjetoAntesModificacion = "";//js.Serialize(detalle_solicitud_integral_ANTERIOR);
                        auditoria.ObjetoDespuesModificacion = "";//js.Serialize(detalle_solicitud_integral);
                        AgregarAuditoria(stp_integral_context, auditoria);

                        //CANCELANDO LOS DETALLE DE LA SOLICITUD INTEGRAL
                        detalles_solicitud_integral = stp_integral_context.TabDetallesSolicitudIntegral.Where(x => x.IdSolicitudIntegral == resultado.IdSolicitudIntegral).ToList();

                        foreach (STPIntegral.Datos.TabDetallesSolicitudIntegral detalle in detalles_solicitud_integral)
                        {
                            detalle.EstadoHuespedAH = EstadosDetallesSolicitudIntegralAH.CONFIRMADO.ToString();
                            detalle.EstadoPasajeroSTP = EstadosDetallesSolicitudIntegralSTP.ABORDO.ToString();
                            detalle.EstadoOcupanteSolicitudIntegral = EstadosDetallesSolicitudIntegralSTP.CONFIRMADO.ToString();

                            stp_integral_context.SaveChanges();

                            // SE INSERTA AUDITORIA
                            auditoria.TipoMovimientoAuditoria = "CONFIRMA PASAJERO";
                            auditoria.DescripcionAuditoria = "CONFIRMA AL PASAJERO CON FICHA/RFC: " + detalle.FichaRFCEmpleado + " (" + detalle.NombreEmpleado + ") DE LA SOLICITUD: " + resultado.FolioSolicitudIntegral;
                            auditoria.FkIdSolicitudIntegral = resultado.IdSolicitudIntegral;
                            auditoria.ObjetoAntesModificacion = "";//js.Serialize(detalle_solicitud_integral_ANTERIOR);
                            auditoria.ObjetoDespuesModificacion = "";//js.Serialize(detalle_solicitud_integral);
                            AgregarAuditoria(stp_integral_context, auditoria);
                        }
                    }

                    transaction_scope.Complete();
                }
                catch (ShowCaseException ex)
                {
                    throw ex;
                }
                catch (System.Exception ex)
                {
                    throw new ShowCaseException(String.Format("ERR-SI-0053: Ocurrio el siguiente error al intentar Confirmar la Solicitud Integral [{0}]: '{1}'", resultado.FolioSolicitudIntegral, ex.Message), ex, TypeMessage.error, "ERR-SI-0053");
                }
            }

            //En este punto ya se pasaron las validaciones, aqui se procede a eliminar al empleado y a recuperar el espacio.
            //Si esto es correcto se procede a eliminar el detalle de la solicitud
            //Dependiendo del tipo de servicio hay que recuperar el espacio reservado de AH y/o STP
            resultado.TabAuditoriaSolicitudIntegral = null;
            resultado.TabDetallesSolicitudIntegral = null;


            return resultado;
        }

        //AUDITAR
        private static bool CancelarSolicitudIntegralSiTodosLosDetallesEstanCancelados(int id_solicitud_integral, STPIntegral.Datos.STPIntegralModel stpIntegral_contex, STPIntegral.Datos.TabAuditoriaSolicitudIntegral auditoria)
        {
            //Regresa true si la solicitud se cancela
            bool resultado = false;
            //VERIFICANDO SI TODOS LOS DETALLES DE LA SOLICITUD ESTAN CANCELADOS
            List<STPIntegral.Datos.TabDetallesSolicitudIntegral> detalles_solicitud_anterior = stpIntegral_contex.TabDetallesSolicitudIntegral.Where(x => x.IdSolicitudIntegral == id_solicitud_integral && x.EstadoOcupanteSolicitudIntegral != "CANCELADO").ToList();

            if (detalles_solicitud_anterior == null || (detalles_solicitud_anterior != null && detalles_solicitud_anterior.Count == 0))
            {
                //EN CASO DE QUE TODOS LOS DETALLES ESTEN CANCELADOS ENTONCES SE CANCELA TAMBIEN LA SOLICITUD
                STPIntegral.Datos.TabSolicitudIntegral solicitudIntegralAnteriorACancelar = stpIntegral_contex.TabSolicitudIntegral.Single(x => x.IdSolicitudIntegral == id_solicitud_integral);
                solicitudIntegralAnteriorACancelar.EstadoSolicitudIntegral = EstadosSolicitudIntegral.CANCELADO.ToString();
                stpIntegral_contex.SaveChanges();

                // SE INSERTA AUDITORIA
                auditoria.TipoMovimientoAuditoria = "REPROGRAMAR SOLICITUD";
                auditoria.DescripcionAuditoria = "CANCELA SOLICITUD CON FOLIO: " + solicitudIntegralAnteriorACancelar.FolioSolicitudIntegral;
                auditoria.FkIdSolicitudIntegral = solicitudIntegralAnteriorACancelar.IdSolicitudIntegral;
                auditoria.ObjetoAntesModificacion = "";//js.Serialize(detalle_solicitud_integral_ANTERIOR);
                auditoria.ObjetoDespuesModificacion = "";//js.Serialize(detalle_solicitud_integral);

                AgregarAuditoria(stpIntegral_contex, auditoria);
                
                resultado = true;
            }

            return resultado;
        }

        #region REPROGRAMACION DE SOLICITUDES

        public static List<Models.CapacidadReprogramacionSolicitudIntegral> ObtenerCapacidadesReprogramacionSolicitudIntegral(int idSolicitudIntegral, DateTime nuevaFechaSubida, DateTime? nuevaFechaBajada)
        {
            STPIntegral.Datos.STPIntegralModel stp_integral_model = new STPIntegral.Datos.STPIntegralModel();
            GestionGL.Datos.GestionGLModel gestion_gl_model = new GestionGL.Datos.GestionGLModel();

            List<Models.CapacidadReprogramacionSolicitudIntegral> resultado = null;

            STPIntegral.Datos.TabSolicitudIntegral solicitud_integral = stp_integral_model.TabSolicitudIntegral.SingleOrDefault(x => x.IdSolicitudIntegral == idSolicitudIntegral);

            if (solicitud_integral == null)
            {
                throw new ShowCaseException(String.Format("ERR-SI-0054: ERR-SI-0054: No se encontro la Solicitud Integral con id: '{0}'", idSolicitudIntegral), TypeMessage.error, "ERR-SI-0054");
            }

            List<STPIntegral.Datos.TabDetallesSolicitudIntegral> detalles_solicitud_integral = stp_integral_model.TabDetallesSolicitudIntegral.Where(x => x.IdSolicitudIntegral == solicitud_integral.IdSolicitudIntegral).ToList();

            //obteniendo diferentes origenes-destinos, y capacidad requerida por origen-destino.
            resultado =
                (from t in detalles_solicitud_integral
                group t by new { t.IdInstalacionOrigen, t.IdInstalacionDestino, t.TipoMovimientoPersonal } into grp
                select new Models.CapacidadReprogramacionSolicitudIntegral()
                {
                    FkIdInstalacionOrigen =  grp.Key.IdInstalacionOrigen,
                    FkIdInstalacionDestino = grp.Key.IdInstalacionDestino,
                    CapacidadRequeridaHospedaje = (grp.Key.TipoMovimientoPersonal == "SUBIDA" && solicitud_integral.TipoSolicitudIntegral != "SOLO TRANSPORTE" ? grp.Count() : 0),
                    CapacidadRequeridaTransporte = (solicitud_integral.TipoSolicitudIntegral != "SOLO HOSPEDAJE" ? grp.Count() : 0),
                    FechaSubidaOriginal = solicitud_integral.FechaSubida,
                    FechaBajadaOriginal = solicitud_integral.FechaBajada,
                    FechaSubidaNueva = nuevaFechaSubida,
                    FechaBajadaNueva = nuevaFechaBajada,
                    FkIdItinerarioTransporte = grp.Key.TipoMovimientoPersonal == "SUBIDA" ? solicitud_integral.FkIdItinerarioTransporteIda : solicitud_integral.FkIdItinerarioTransporteRetorno
                 }).ToList();


            List<int> listado_ids_instalaciones_distintas = (from x in resultado select x.FkIdInstalacionOrigen).ToList();
            listado_ids_instalaciones_distintas.AddRange((from x in resultado select x.FkIdInstalacionDestino));

            listado_ids_instalaciones_distintas = (from x in listado_ids_instalaciones_distintas select x).Distinct().ToList();

            List<GestionGL.Datos.C_M_INSTALACIONES> listado_instalaciones = gestion_gl_model.C_M_INSTALACIONES.Where(x => listado_ids_instalaciones_distintas.Contains(x.ID_C_M_INSTALACION)).ToList();
            GestionGL.Datos.C_M_INSTALACIONES instalacion_busqueda = null;

            List<STPIntegral.Datos.stp_c_itinerario_base> listado_itinerarios = stp_integral_model.stp_c_itinerario_base.Where(x => x.id_itinerario == solicitud_integral.FkIdItinerarioTransporteIda || x.id_itinerario == solicitud_integral.FkIdItinerarioTransporteRetorno).ToList();
            STPIntegral.Datos.stp_c_itinerario_base itinerario_busqueda = null;
            
            foreach(Models.CapacidadReprogramacionSolicitudIntegral capacidad_requerida in resultado)
            {
                if(capacidad_requerida.CapacidadRequeridaHospedaje > 0)
                {
                    capacidad_requerida.CapacidadDisponibleHospedaje = ObtenerCapacidadHospedaje(capacidad_requerida.FkIdInstalacionDestino, capacidad_requerida.FechaSubidaNueva, capacidad_requerida.FechaBajadaNueva.Value, solicitud_integral.NumeroContrato, "", solicitud_integral.SubdivisionAutorizador);
                }

                if (capacidad_requerida.CapacidadRequeridaTransporte > 0)
                {
                    capacidad_requerida.CapacidadDisponibleTransporte = ItinerariosTransporte.ObtenerCapacidadTransportePorItem(capacidad_requerida.FkIdItinerarioTransporte.Value, capacidad_requerida.FechaSubidaNueva, "", solicitud_integral.NumeroContrato, solicitud_integral.SubdivisionAutorizador);
                }
                
                instalacion_busqueda = listado_instalaciones.Single(x => x.ID_C_M_INSTALACION == capacidad_requerida.FkIdInstalacionDestino);
                
                capacidad_requerida.NombreInstalacionDestino = instalacion_busqueda.NOMBRE_INSTALACION;
                capacidad_requerida.SiglasInstalacionDestino = instalacion_busqueda.SIGLAS_INSTALACION;

                instalacion_busqueda = listado_instalaciones.Single(x => x.ID_C_M_INSTALACION == capacidad_requerida.FkIdInstalacionOrigen);

                capacidad_requerida.NombreInstalacionOrigen = instalacion_busqueda.NOMBRE_INSTALACION;
                capacidad_requerida.SiglasInstalacionOrigen = instalacion_busqueda.SIGLAS_INSTALACION;

                itinerario_busqueda = listado_itinerarios.SingleOrDefault(x => x.id_itinerario == capacidad_requerida.FkIdItinerarioTransporte);

                if (itinerario_busqueda != null)
                {
                    capacidad_requerida.NombreItinerario = itinerario_busqueda.des_itinerario;
                }
            }

            return resultado;
        }

        //AUDITAR
        public static SolicitudIntegralModel ReprogramarSolicitud(STPIntegral.Datos.TabSolicitudIntegral solicitud_integral, IEnumerable<STPIntegral.Datos.TabDetallesSolicitudIntegral> detalles_solicitud_integral, DateTime NuevaFechaSubida, DateTime NuevaFechaBajada,STPIntegral.Datos.TabAuditoriaSolicitudIntegral auditoria)
        {
            //tipo_solicitud SOLO HOSPEDAJE, SOLO TRANSPORTE, HOSPEDAJE Y TRANSPORTE
            //tipo_personal: PEMEX, COMPAÑIA, COMODATARIO
            //tipo_transporte: AEREO, MARITIMO
            //tipo_servicio: CAMBIO DE GUARDIA, REGULARES, LATERALES, DISPOSICION

            SolicitudIntegralModel respuesta = null;


            //Validacion de no reprogramar una solicitud de MARITIMO a AEREO
            if (solicitud_integral.TipoTransporte == "MARITIMO")
            {
                throw new ShowCaseException("ERR-SI-0074: No se puede reprogramar una solicitud de MARITMO a AEREO", TypeMessage.error, "ERR-SI-0074");
            }

            //Contextos para obtener acceso a la base de datos.
            GestionGL.Datos.GestionGLModel gestionGL_context = new GestionGL.Datos.GestionGLModel();
            STPIntegral.Datos.STPIntegralModel stpIntegral_contex = new Datos.STPIntegralModel();
            SCHAP.Datos.SCHAPModel schap_context = new SCHAP.Datos.SCHAPModel();

            gestionGL_context.Configuration.LazyLoadingEnabled = false;
            stpIntegral_contex.Configuration.LazyLoadingEnabled = false;
            schap_context.Configuration.LazyLoadingEnabled = false;

            gestionGL_context.Configuration.ProxyCreationEnabled = false;
            stpIntegral_contex.Configuration.ProxyCreationEnabled = false;
            schap_context.Configuration.ProxyCreationEnabled = false;

            int id_solicitud_integral_anterior = solicitud_integral.IdSolicitudIntegral;
            string folio_solicitud_integral_anterior = solicitud_integral.FolioSolicitudIntegral;

            solicitud_integral.IdSolicitudIntegral = 0;
            solicitud_integral.FolioSolicitudIntegral = "";

            solicitud_integral.FechaSubida = NuevaFechaSubida;
            solicitud_integral.FechaBajada = NuevaFechaBajada;

            AplicarValidacionesSolicitudIntegral(solicitud_integral, detalles_solicitud_integral, gestionGL_context, stpIntegral_contex, schap_context, false);

            //Obtenemos variables de utileria para las validaciones posteriores

            GestionGL.Datos.C_CONTRATOS contrato_gestion_gl = null;
            GestionGL.Datos.C_ACREEDORES acreedor_contrato_gestion_gl = null;

            if (solicitud_integral.TipoPersonal != "PEMEX")
            {
                if (!String.IsNullOrEmpty(solicitud_integral.NumeroContrato.Trim()))
                {
                    contrato_gestion_gl = gestionGL_context.C_CONTRATOS.Where(x => x.CONTRATO == solicitud_integral.NumeroContrato).SingleOrDefault();

                    if (contrato_gestion_gl != null)
                    {
                        acreedor_contrato_gestion_gl = gestionGL_context.C_ACREEDORES.Where(x => x.ID_C_ACREEDOR == contrato_gestion_gl.ID_C_ACREEDOR).SingleOrDefault();
                    }
                }
            }
            else
            {
                solicitud_integral.NumeroContrato = "";
                solicitud_integral.ContratoVisita = false;
                solicitud_integral.NombreCompania = "";
            }

            //Obtenemos el listado de las distintas instalaciones origen destino
            List<int> listado_ids_instalaciones = (from x in detalles_solicitud_integral select x.IdInstalacionDestino).Distinct().ToList();
            listado_ids_instalaciones.AddRange((from x in detalles_solicitud_integral select x.IdInstalacionOrigen).Distinct().ToList());
            listado_ids_instalaciones = listado_ids_instalaciones.Distinct().ToList();


            List<GestionGL.Datos.C_M_INSTALACIONES> instalaciones_origenes_destinos = (from x in gestionGL_context.C_M_INSTALACIONES
                                                                                       where (listado_ids_instalaciones).Contains(x.ID_C_M_INSTALACION)
                                                                                       select x).ToList();

            GestionGL.Datos.C_M_INSTALACIONES instalacion_destino = null;

            //En este punto ya estan todas las validaciones que no requieren una transaccion, entonces hacemos dispose a todas las conecciones.
            stpIntegral_contex.Dispose();
            gestionGL_context.Dispose();
            schap_context.Dispose();

            int capacidad_disponible_solictada_transporte_hospedaje = 1;

            //INICIAN VALIDACIONES TRANSACCIONADAS
            using (TransactionScope transaction_scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = IsolationLevel.Serializable }))
            {
                try
                {
                    //Como toda la parte transaccional quedo solo en STPIntegral, entonces se crea un nuevo contexto para que todo esto se transaccione.
                    using (stpIntegral_contex = new Datos.STPIntegralModel())
                    {
                        if (solicitud_integral.IdSolicitudIntegral <= 0)
                        {
                            //si se confirma la capacidad entonces se procede a insertar la solicitud integral nueva si hace falta. (previa generacion transaccionada del folio de solicitud).
                            solicitud_integral.FolioSolicitudIntegral = ObtenerNuevoFolioSolicitudIntegral(stpIntegral_contex, System.DateTime.Now.Date);

                            //Agregando la solicitud integral nueva.

                            solicitud_integral.ContadorAnexo = 0;
                            solicitud_integral.ContratoVisita = false;
                            solicitud_integral.FKidOrigenSolicitud = detalles_solicitud_integral.First().IdInstalacionOrigen;
                            solicitud_integral.FKidDestinoSolicitud = detalles_solicitud_integral.First().IdInstalacionDestino;
                            solicitud_integral.EstadoSolicitudIntegral = EstadosSolicitudIntegral.SOLICITADO.ToString();
                            solicitud_integral.EstadoVistoBuenoAH = "PENDIENTE";
                            solicitud_integral.FechaUltimaActualizacion = System.DateTime.Now;
                            solicitud_integral.HabilitarCapturaAnexos = false;

                            solicitud_integral.LetraUltimoAnexo = "";

                            solicitud_integral.NodoTransporte = "";
                            solicitud_integral.ObservacionesLogistica = "";
                            solicitud_integral.ObservacionesSolicitud = "";
                            solicitud_integral.ObservacionesVentanilla = "";

                            solicitud_integral.SolicitudResguardada = false;

                            solicitud_integral.HoraSalidaItinerarioTransporte = solicitud_integral.FechaSubida;


                            solicitud_integral.FechaCreacion = System.DateTime.Now;
                            solicitud_integral.FechaConfirmacion = System.DateTime.Now;
                            solicitud_integral.FechaViaje = solicitud_integral.FechaSubida;

                            solicitud_integral.NombreCompania = solicitud_integral.TipoPersonal == "PEMEX" ? "PEMEX" : acreedor_contrato_gestion_gl.DESCRIPCION_ACREEDOR;

                            stpIntegral_contex.TabSolicitudIntegral.Add(solicitud_integral);
                            stpIntegral_contex.SaveChanges();

                            //SE INSERTA AUDITORIA
                            auditoria.FkIdSolicitudIntegral = solicitud_integral.IdSolicitudIntegral;
                            auditoria.TipoMovimientoAuditoria = "REPROGRAMAR SOLICITUD";
                            auditoria.DescripcionAuditoria = "GENERA LA NUEVA SOLICITUD CON FOLIO: " + solicitud_integral.FolioSolicitudIntegral.ToString();
                            auditoria.ObjetoAntesModificacion = "";
                            auditoria.ObjetoDespuesModificacion = "";//js.Serialize(solicitud_integral);
                            AgregarAuditoria(stpIntegral_contex, auditoria);
                           
                        }

                        STPIntegral.Datos.TabSolicitudIntegral solicitudIntegralAnterior = stpIntegral_contex.TabSolicitudIntegral.Single(x => x.IdSolicitudIntegral == id_solicitud_integral_anterior);
                        STPIntegral.Datos.TabDetallesSolicitudIntegral detalle_solicitud_anterior_a_cancelar = null;

                        foreach (STPIntegral.Datos.TabDetallesSolicitudIntegral detalle_solicitud_parametro in detalles_solicitud_integral)
                        {
                            detalle_solicitud_parametro.FechaSubida = solicitud_integral.FechaSubida;
                            detalle_solicitud_parametro.FechaBajada = solicitud_integral.FechaBajada;

                            //primero se verifica que haya disponibilidad de capacidad tanto en transporte como en hospedaje (lo que aplique).
                            detalle_solicitud_parametro.fk_itinerario = detalle_solicitud_parametro.TipoMovimientoPersonal == "SUBIDA" ? solicitud_integral.FkIdItinerarioTransporteIda : solicitud_integral.FkIdItinerarioTransporteRetorno;

                            instalacion_destino = instalaciones_origenes_destinos.Where(x => x.ID_C_M_INSTALACION == detalle_solicitud_parametro.IdInstalacionDestino).FirstOrDefault();

                            ValidarCapacidadHospedajeTransporte(instalacion_destino, contrato_gestion_gl, solicitud_integral.TipoSolicitudIntegral, solicitud_integral.TipoServicio, detalle_solicitud_parametro.TipoMovimientoPersonal, detalle_solicitud_parametro.FechaSubida, detalle_solicitud_parametro.FechaBajada, detalle_solicitud_parametro.fk_itinerario, "", "", solicitud_integral.SubdivisionAutorizador, capacidad_disponible_solictada_transporte_hospedaje, stpIntegral_contex);

                            detalle_solicitud_parametro.IdSolicitudIntegral = solicitud_integral.IdSolicitudIntegral;

                            detalle_solicitud_parametro.EstadoHuespedAH = EstadosDetallesSolicitudIntegralAH.SOLICITADO.ToString();
                            detalle_solicitud_parametro.EstadoOcupanteSolicitudIntegral = EstadosDetallesSolicitudIntegral.SOLICITADO.ToString();
                            detalle_solicitud_parametro.EstadoPasajeroSTP = EstadosDetallesSolicitudIntegralSTP.SOLICITADO.ToString();
                            detalle_solicitud_parametro.FolioLibretaMar = "";
                            detalle_solicitud_parametro.ObservacionesDetalle = "";

                            detalle_solicitud_parametro.VigenciaLibretaMar = null;

                            detalle_solicitud_parametro.fk_contrato = (solicitud_integral.TipoPersonal == "PEMEX") || (solicitud_integral.TipoPersonal != "PEMEX" && contrato_gestion_gl == null) ? (int?)null : contrato_gestion_gl.ID_C_CONTRATO;
                            //Aqui falta validar si el itinerario es de subida o bajada para cambiar la hora segun corresponda.
                            if (detalle_solicitud_parametro.fk_itinerario.HasValue)
                            {
                                detalle_solicitud_parametro.HoraViajeItemParajeroSTA = ObtenerHorarioSalidaItinerario(detalle_solicitud_parametro.fk_itinerario.Value, solicitud_integral.FechaSubida, detalle_solicitud_parametro.IdInstalacionDestino, stpIntegral_contex);
                            }
                            else
                            {
                                detalle_solicitud_parametro.HoraViajeItemParajeroSTA = solicitud_integral.HoraSalidaItinerarioTransporte;
                            }


                            //Cancelando el detalle de la solicitud anterior
                            detalle_solicitud_anterior_a_cancelar = stpIntegral_contex.TabDetallesSolicitudIntegral.SingleOrDefault(x => x.IdDetalleSolicitudIntegral == detalle_solicitud_parametro.IdDetalleSolicitudIntegral);

                            if (detalle_solicitud_anterior_a_cancelar != null)
                            {
                                detalle_solicitud_anterior_a_cancelar.EstadoHuespedAH = EstadosDetallesSolicitudIntegralAH.CANCELADO.ToString();
                                detalle_solicitud_anterior_a_cancelar.EstadoOcupanteSolicitudIntegral = EstadosDetallesSolicitudIntegral.CANCELADO.ToString();
                                detalle_solicitud_anterior_a_cancelar.EstadoPasajeroSTP = EstadosDetallesSolicitudIntegralSTP.CANCELADO.ToString();

                                if(String.IsNullOrEmpty(detalle_solicitud_anterior_a_cancelar.ObservacionesDetalle))
                                {
                                    detalle_solicitud_anterior_a_cancelar.ObservacionesDetalle = "";
                                }

                                //SE INSERTA AUDITORIA
                                auditoria.FkIdSolicitudIntegral = solicitud_integral.IdSolicitudIntegral;
                                auditoria.TipoMovimientoAuditoria = "REPROGRAMAR SOLICITUD";
                                auditoria.DescripcionAuditoria = "CANCELA AL PASAJERO CON FICHA/RFC: " + detalle_solicitud_anterior_a_cancelar.FichaRFCEmpleado + " (" + detalle_solicitud_anterior_a_cancelar.NombreEmpleado + ") DE LA SOLICITUD: " + solicitudIntegralAnterior.FolioSolicitudIntegral;
                                auditoria.FkIdSolicitudIntegral = solicitudIntegralAnterior.IdSolicitudIntegral;
                                auditoria.ObjetoAntesModificacion = "";//js.Serialize(detalle_solicitud_integral_ANTERIOR);
                                auditoria.ObjetoDespuesModificacion = "";//js.Serialize(detalle_solicitud_integral);
                                AgregarAuditoria(stpIntegral_contex, auditoria);
                            }
                            else
                            {
                                throw new ShowCaseException(String.Format("ERR-SI-0055: No se encontro del detalle de la solicitud.", solicitud_integral.FolioSolicitudIntegral), null, TypeMessage.error, "ERR-SI-0055");
                            }

                            detalle_solicitud_parametro.IdDetalleSolicitudIntegral = 0;

                            //Agregando el detalle a la solicitud integral.
                            stpIntegral_contex.TabDetallesSolicitudIntegral.Add(detalle_solicitud_parametro);
                            //AUDITAR
                            stpIntegral_contex.SaveChanges();

                            //AUDITAR
                            auditoria.TipoMovimientoAuditoria = "REPROGRAMAR SOLICITUD";
                            auditoria.DescripcionAuditoria = "GUARDA EL PASAJERO/HUESPED CON FICHA/RFC: " + detalle_solicitud_parametro.FichaRFCEmpleado + " (" + detalle_solicitud_parametro.NombreEmpleado + ")  EN EL DETALLE DEL FOLIO: " + solicitud_integral.FolioSolicitudIntegral.ToString();
                            auditoria.FkIdSolicitudIntegral = solicitud_integral.IdSolicitudIntegral;
                            auditoria.ObjetoAntesModificacion = "";
                            //auditoria.ObjetoDespuesModificacion = "hola";
                            auditoria.ObjetoDespuesModificacion = "";//js.Serialize(detalle_solicitud_parametro);
                            AgregarAuditoria(stpIntegral_contex, auditoria);

                            //Como ya se agrego un detalle entonces hay que quitar un espacio en la reservacion de transporte y/o hospedaje si aplica.

                            //Empezamos con transporte, el orden de prioridad es el siguiente, Primero se validan los folios especiales, luego los contratos, seguido de los centros gestores, por ultimo los espacios libres totales.
                            //Rerservando un nuevo espacio
                            //TODO: HACE FALTA PEDIR LOS FOLIOS ESPECIALES EN LA PANTALLA DE REPROGRAMACION
                            AgregarEliminarEspacioReservacionSTPAH(stpIntegral_contex, solicitud_integral, detalle_solicitud_parametro, contrato_gestion_gl, "", "", "", instalacion_destino, 1, false, auditoria);
                            //Elimiando el espacio anterior

                            AgregarEliminarEspacioReservacionSTPAH(stpIntegral_contex, solicitudIntegralAnterior, detalle_solicitud_anterior_a_cancelar, contrato_gestion_gl, "", "", "", instalacion_destino, 1, true, auditoria);
                        }//fin foreach(STPIntegral.Datos.TabDetallesSolicitudIntegral detalle_solicitud_parametro in detalles_solicitud_integral)


                        //VERIFICANDO SI TODOS LOS DETALLES DE LA SOLICITUD ESTAN CANCELADOS 
                        CancelarSolicitudIntegralSiTodosLosDetallesEstanCancelados(solicitudIntegralAnterior.IdSolicitudIntegral, stpIntegral_contex, auditoria);
                    }

                    ////stpIntegral_contex.SaveChanges();

                    //   // si todo sale bien se cierra la transaccion, en caso contrario se aborta toda la operacion.
                    transaction_scope.Complete();
                }
                catch (ShowCaseException ex)
                {
                    throw ex;
                }
                catch (System.Exception ex)
                {
                    StringBuilder mensaje_error = new StringBuilder();

                    mensaje_error.Append(ex.Message);

                    if (ex.InnerException != null)
                    {
                        mensaje_error.Append(String.Format(" Detalles: {0}", ex.InnerException.Message));
                    }

                    throw new ShowCaseException(String.Format("ERR-SI-0037: Ocurrio el siguiente error al intentar agregar un detalle a la Solicitud Integral [{0}]: '{1}'", solicitud_integral.FolioSolicitudIntegral, mensaje_error.ToString()), ex, TypeMessage.error, "ERR-SI-0037");
                }
            }

            respuesta = new SolicitudIntegralModel();

            respuesta.SolicitudIntegral = solicitud_integral;

            using (STPIntegral.Datos.STPIntegralModel stpIntegralContext = new Datos.STPIntegralModel())
            {
                stpIntegralContext.Configuration.LazyLoadingEnabled = false;
                stpIntegralContext.Configuration.ProxyCreationEnabled = false;

                respuesta.DetallesSolicitudIntegral = stpIntegralContext.TabDetallesSolicitudIntegral.Where(x => x.IdSolicitudIntegral == solicitud_integral.IdSolicitudIntegral).ToList();
            }

            respuesta.ContratoSap = null;
            respuesta.InstalacionesDestino = null;
            respuesta.InstalacionesOrigen = null;
            respuesta.ItinerariosTransporte = null;
            respuesta.ListadoDireccionamientosDestinos = null;

            return respuesta;
        }

        #endregion

        #region DIFERIR SOLICITUDES

        public static List<Models.CapacidadReprogramacionSolicitudIntegral> ObtenerCapacidadesDiferirSolicitudIntegral(int idSolicitudIntegral, int idItinerarioIdaDiferir, int idItinerarioRetornoDiferir, string subdivision_autorizador_diferir, DateTime nuevaFechaSubida, DateTime nuevaFechaBajada)
        {
            List<Models.CapacidadReprogramacionSolicitudIntegral> resultado = null;

            if (!String.IsNullOrEmpty(subdivision_autorizador_diferir))
            {
                STPIntegral.Datos.STPIntegralModel stp_integral_model = new STPIntegral.Datos.STPIntegralModel();
                GestionGL.Datos.GestionGLModel gestion_gl_model = new GestionGL.Datos.GestionGLModel();

                STPIntegral.Datos.TabSolicitudIntegral solicitud_integral = stp_integral_model.TabSolicitudIntegral.SingleOrDefault(x => x.IdSolicitudIntegral == idSolicitudIntegral);

                if (solicitud_integral == null)
                {
                    throw new ShowCaseException(String.Format("ERR-SI-0054: ERR-SI-0054: No se encontro la Solicitud Integral con id: '{0}'", idSolicitudIntegral), TypeMessage.error, "ERR-SI-0054");
                }

                List<STPIntegral.Datos.TabDetallesSolicitudIntegral> detalles_solicitud_integral = stp_integral_model.TabDetallesSolicitudIntegral.Where(x => x.IdSolicitudIntegral == solicitud_integral.IdSolicitudIntegral).ToList();

                //obteniendo diferentes origenes-destinos, y capacidad requerida por origen-destino.
                resultado =
                    (from t in detalles_solicitud_integral
                     group t by new { t.IdInstalacionOrigen, t.IdInstalacionDestino, t.TipoMovimientoPersonal } into grp
                     select new Models.CapacidadReprogramacionSolicitudIntegral()
                     {
                         FkIdInstalacionOrigen = grp.Key.IdInstalacionOrigen,
                         FkIdInstalacionDestino = grp.Key.IdInstalacionDestino,
                         CapacidadRequeridaHospedaje = (grp.Key.TipoMovimientoPersonal == "SUBIDA" && solicitud_integral.TipoSolicitudIntegral != "SOLO TRANSPORTE" ? grp.Count() : 0),
                         CapacidadRequeridaTransporte = (solicitud_integral.TipoSolicitudIntegral != "SOLO HOSPEDAJE" ? grp.Count() : 0),
                         FechaSubidaOriginal = solicitud_integral.FechaSubida,
                         FechaBajadaOriginal = solicitud_integral.FechaBajada,
                         FechaSubidaNueva = nuevaFechaSubida,
                         FechaBajadaNueva = nuevaFechaBajada,
                         FkIdItinerarioTransporte = grp.Key.TipoMovimientoPersonal == "SUBIDA" ? idItinerarioIdaDiferir : idItinerarioRetornoDiferir
                     }).ToList();


                List<int> listado_ids_instalaciones_distintas = (from x in resultado select x.FkIdInstalacionOrigen).ToList();
                listado_ids_instalaciones_distintas.AddRange((from x in resultado select x.FkIdInstalacionDestino));

                listado_ids_instalaciones_distintas = (from x in listado_ids_instalaciones_distintas select x).Distinct().ToList();

                List<GestionGL.Datos.C_M_INSTALACIONES> listado_instalaciones = gestion_gl_model.C_M_INSTALACIONES.Where(x => listado_ids_instalaciones_distintas.Contains(x.ID_C_M_INSTALACION)).ToList();
                GestionGL.Datos.C_M_INSTALACIONES instalacion_busqueda = null;

                List<STPIntegral.Datos.stp_c_itinerario_base> listado_itinerarios = stp_integral_model.stp_c_itinerario_base.Where(x => x.id_itinerario == idItinerarioIdaDiferir || x.id_itinerario == idItinerarioRetornoDiferir).ToList();
                STPIntegral.Datos.stp_c_itinerario_base itinerario_busqueda = null;

                foreach (Models.CapacidadReprogramacionSolicitudIntegral capacidad_requerida in resultado)
                {
                    if (capacidad_requerida.CapacidadRequeridaHospedaje > 0)
                    {
                        capacidad_requerida.CapacidadDisponibleHospedaje = ObtenerCapacidadHospedaje(capacidad_requerida.FkIdInstalacionDestino, capacidad_requerida.FechaSubidaNueva, capacidad_requerida.FechaBajadaNueva.Value, solicitud_integral.NumeroContrato, "", subdivision_autorizador_diferir);
                    }

                    if (capacidad_requerida.CapacidadRequeridaTransporte > 0)
                    {
                        capacidad_requerida.CapacidadDisponibleTransporte = ItinerariosTransporte.ObtenerCapacidadTransportePorItem(capacidad_requerida.FkIdItinerarioTransporte.Value, capacidad_requerida.FechaSubidaNueva, "", solicitud_integral.NumeroContrato, solicitud_integral.SubdivisionAutorizador);
                    }

                    instalacion_busqueda = listado_instalaciones.Single(x => x.ID_C_M_INSTALACION == capacidad_requerida.FkIdInstalacionDestino);

                    capacidad_requerida.NombreInstalacionDestino = instalacion_busqueda.NOMBRE_INSTALACION;
                    capacidad_requerida.SiglasInstalacionDestino = instalacion_busqueda.SIGLAS_INSTALACION;

                    instalacion_busqueda = listado_instalaciones.Single(x => x.ID_C_M_INSTALACION == capacidad_requerida.FkIdInstalacionOrigen);

                    capacidad_requerida.NombreInstalacionOrigen = instalacion_busqueda.NOMBRE_INSTALACION;
                    capacidad_requerida.SiglasInstalacionOrigen = instalacion_busqueda.SIGLAS_INSTALACION;

                    itinerario_busqueda = listado_itinerarios.SingleOrDefault(x => x.id_itinerario == capacidad_requerida.FkIdItinerarioTransporte);

                    if (itinerario_busqueda != null)
                    {
                        capacidad_requerida.NombreItinerario = itinerario_busqueda.des_itinerario;
                    }
                }
            }

            return resultado;
        }

        //AUDITAR
        public static SolicitudIntegralModel DiferirPasajerosSolicitud(STPIntegral.Datos.TabSolicitudIntegral nueva_solicitud_integral_diferir, IEnumerable<STPIntegral.Datos.TabDetallesSolicitudIntegral> detalles_diferir_solicitud_integral, string folioSolicitudIntegralADiferir,STPIntegral.Datos.TabAuditoriaSolicitudIntegral auditoria)
        {
            //tipo_solicitud SOLO HOSPEDAJE, SOLO TRANSPORTE, HOSPEDAJE Y TRANSPORTE
            //tipo_personal: PEMEX, COMPAÑIA, COMODATARIO
            //tipo_transporte: AEREO, MARITIMO
            //tipo_servicio: CAMBIO DE GUARDIA, REGULARES, LATERALES, DISPOSICION

            SolicitudIntegralModel respuesta = null;

            //Contextos para obtener acceso a la base de datos.
            GestionGL.Datos.GestionGLModel gestionGL_context = new GestionGL.Datos.GestionGLModel();
            STPIntegral.Datos.STPIntegralModel stpIntegral_contex = new Datos.STPIntegralModel();
            SCHAP.Datos.SCHAPModel schap_context = new SCHAP.Datos.SCHAPModel();

            gestionGL_context.Configuration.LazyLoadingEnabled = false;
            stpIntegral_contex.Configuration.LazyLoadingEnabled = false;
            schap_context.Configuration.LazyLoadingEnabled = false;

            gestionGL_context.Configuration.ProxyCreationEnabled = false;
            stpIntegral_contex.Configuration.ProxyCreationEnabled = false;
            schap_context.Configuration.ProxyCreationEnabled = false;

            STPIntegral.Datos.TabSolicitudIntegral solicitudIntegralAnterior = stpIntegral_contex.TabSolicitudIntegral.Single(x => x.FolioSolicitudIntegral == folioSolicitudIntegralADiferir);

            if (solicitudIntegralAnterior.TipoPersonal == "COMPAÑIA" && solicitudIntegralAnterior.TipoTransporte == "MARITIMO" && solicitudIntegralAnterior.TipoServicio != "LATERALES")
            {
                throw new ShowCaseException(String.Format("ERR-SI-0066: No se puede diferir una solicitud de personal de COMPAÑIA desde el transporte MARITIMO al AEREO."), null, TypeMessage.error, "ERR-SI-0066");
            }

            AplicarValidacionesSolicitudIntegral(nueva_solicitud_integral_diferir, detalles_diferir_solicitud_integral, gestionGL_context, stpIntegral_contex, schap_context, false);

            //Obtenemos variables de utileria para las validaciones posteriores

            GestionGL.Datos.C_CONTRATOS contrato_gestion_gl = null;
            GestionGL.Datos.C_ACREEDORES acreedor_contrato_gestion_gl = null;

            if (nueva_solicitud_integral_diferir.TipoPersonal != "PEMEX")
            {
                if (!String.IsNullOrEmpty(nueva_solicitud_integral_diferir.NumeroContrato.Trim()))
                {
                    contrato_gestion_gl = gestionGL_context.C_CONTRATOS.Where(x => x.CONTRATO == nueva_solicitud_integral_diferir.NumeroContrato).SingleOrDefault();

                    if (contrato_gestion_gl != null)
                    {
                        acreedor_contrato_gestion_gl = gestionGL_context.C_ACREEDORES.Where(x => x.ID_C_ACREEDOR == contrato_gestion_gl.ID_C_ACREEDOR).SingleOrDefault();
                    }
                }
            }
            else
            {
                nueva_solicitud_integral_diferir.NumeroContrato = "";
                nueva_solicitud_integral_diferir.ContratoVisita = false;
                nueva_solicitud_integral_diferir.NombreCompania = "";
            }

            //Obtenemos el listado de las distintas instalaciones origen destino
            List<int> listado_ids_instalaciones = (from x in detalles_diferir_solicitud_integral select x.IdInstalacionDestino).Distinct().ToList();
            listado_ids_instalaciones.AddRange((from x in detalles_diferir_solicitud_integral select x.IdInstalacionOrigen).Distinct().ToList());
            listado_ids_instalaciones = listado_ids_instalaciones.Distinct().ToList();

            List<GestionGL.Datos.C_M_INSTALACIONES> instalaciones_origenes_destinos = (from x in gestionGL_context.C_M_INSTALACIONES
                                                                                       where (listado_ids_instalaciones).Contains(x.ID_C_M_INSTALACION)
                                                                                       select x).ToList();

            GestionGL.Datos.C_M_INSTALACIONES instalacion_destino = null;

            //En este punto ya estan todas las validaciones que no requieren una transaccion, entonces hacemos dispose a todas las conecciones.
            stpIntegral_contex.Dispose();
            gestionGL_context.Dispose();
            schap_context.Dispose();

            int capacidad_disponible_solictada_transporte_hospedaje = 1;

            //INICIAN VALIDACIONES TRANSACCIONADAS
            using (TransactionScope transaction_scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = IsolationLevel.Serializable }))
            {
                try
                {
                    //Como toda la parte transaccional quedo solo en STPIntegral, entonces se crea un nuevo contexto para que todo esto se transaccione.
                    using (stpIntegral_contex = new Datos.STPIntegralModel())
                    {
                        if (nueva_solicitud_integral_diferir.IdSolicitudIntegral <= 0)
                        {
                            //si se confirma la capacidad entonces se procede a insertar la solicitud integral nueva si hace falta. (previa generacion transaccionada del folio de solicitud).
                            nueva_solicitud_integral_diferir.FolioSolicitudIntegral = ObtenerNuevoFolioSolicitudIntegral(stpIntegral_contex, System.DateTime.Now.Date);

                            //Agregando la solicitud integral nueva.

                            nueva_solicitud_integral_diferir.ContadorAnexo = 0;
                            nueva_solicitud_integral_diferir.ContratoVisita = false;
                            nueva_solicitud_integral_diferir.FKidOrigenSolicitud = detalles_diferir_solicitud_integral.First().IdInstalacionOrigen;
                            nueva_solicitud_integral_diferir.FKidDestinoSolicitud = detalles_diferir_solicitud_integral.First().IdInstalacionDestino;
                            nueva_solicitud_integral_diferir.EstadoSolicitudIntegral = EstadosSolicitudIntegral.SOLICITADO.ToString();
                            nueva_solicitud_integral_diferir.EstadoVistoBuenoAH = "PENDIENTE";
                            nueva_solicitud_integral_diferir.FechaUltimaActualizacion = System.DateTime.Now;
                            nueva_solicitud_integral_diferir.HabilitarCapturaAnexos = false;

                            nueva_solicitud_integral_diferir.LetraUltimoAnexo = "";

                            nueva_solicitud_integral_diferir.NodoTransporte = "";
                            nueva_solicitud_integral_diferir.ObservacionesLogistica = "";
                            nueva_solicitud_integral_diferir.ObservacionesSolicitud = "";
                            nueva_solicitud_integral_diferir.ObservacionesVentanilla = "";

                            nueva_solicitud_integral_diferir.SolicitudResguardada = false;

                            nueva_solicitud_integral_diferir.HoraSalidaItinerarioTransporte = nueva_solicitud_integral_diferir.FechaSubida;


                            nueva_solicitud_integral_diferir.FechaCreacion = System.DateTime.Now;
                            nueva_solicitud_integral_diferir.FechaConfirmacion = System.DateTime.Now;
                            nueva_solicitud_integral_diferir.FechaViaje = nueva_solicitud_integral_diferir.FechaSubida;

                            nueva_solicitud_integral_diferir.NombreCompania = nueva_solicitud_integral_diferir.TipoPersonal == "PEMEX" ? "PEMEX" : acreedor_contrato_gestion_gl.DESCRIPCION_ACREEDOR;

                            stpIntegral_contex.TabSolicitudIntegral.Add(nueva_solicitud_integral_diferir);
                            stpIntegral_contex.SaveChanges();

                            
                            //SE INSERTA AUDITORIA
                            auditoria.FkIdSolicitudIntegral = nueva_solicitud_integral_diferir.IdSolicitudIntegral;
                            auditoria.TipoMovimientoAuditoria = "DIFERIR SOLICITUD";
                            auditoria.DescripcionAuditoria = "GENERA LA NUEVA SOLICITUD CON FOLIO: " + nueva_solicitud_integral_diferir.FolioSolicitudIntegral.ToString();
                            auditoria.ObjetoAntesModificacion = "";
                            auditoria.ObjetoDespuesModificacion = "";//js.Serialize(solicitud_integral);
                            AgregarAuditoria(stpIntegral_contex, auditoria);
                        }

                        STPIntegral.Datos.TabDetallesSolicitudIntegral detalle_solicitud_anterior_a_cancelar = null;

                        foreach (STPIntegral.Datos.TabDetallesSolicitudIntegral detalle_solicitud_parametro in detalles_diferir_solicitud_integral)
                        {
                            detalle_solicitud_parametro.FechaSubida = nueva_solicitud_integral_diferir.FechaSubida;
                            detalle_solicitud_parametro.FechaBajada = nueva_solicitud_integral_diferir.FechaBajada;

                            //primero se verifica que haya disponibilidad de capacidad tanto en transporte como en hospedaje (lo que aplique).
                            detalle_solicitud_parametro.fk_itinerario = detalle_solicitud_parametro.TipoMovimientoPersonal == "SUBIDA" ? nueva_solicitud_integral_diferir.FkIdItinerarioTransporteIda : nueva_solicitud_integral_diferir.FkIdItinerarioTransporteRetorno;

                            instalacion_destino = instalaciones_origenes_destinos.Where(x => x.ID_C_M_INSTALACION == detalle_solicitud_parametro.IdInstalacionDestino).FirstOrDefault();

                            ValidarCapacidadHospedajeTransporte(instalacion_destino, contrato_gestion_gl, nueva_solicitud_integral_diferir.TipoSolicitudIntegral, nueva_solicitud_integral_diferir.TipoServicio, detalle_solicitud_parametro.TipoMovimientoPersonal, detalle_solicitud_parametro.FechaSubida, detalle_solicitud_parametro.FechaBajada, detalle_solicitud_parametro.fk_itinerario, "", "", nueva_solicitud_integral_diferir.SubdivisionAutorizador, capacidad_disponible_solictada_transporte_hospedaje, stpIntegral_contex);

                            detalle_solicitud_parametro.IdSolicitudIntegral = nueva_solicitud_integral_diferir.IdSolicitudIntegral;

                            detalle_solicitud_parametro.EstadoHuespedAH = EstadosDetallesSolicitudIntegralAH.SOLICITADO.ToString();
                            detalle_solicitud_parametro.EstadoOcupanteSolicitudIntegral = EstadosDetallesSolicitudIntegral.SOLICITADO.ToString();
                            detalle_solicitud_parametro.EstadoPasajeroSTP = EstadosDetallesSolicitudIntegralSTP.SOLICITADO.ToString();
                            detalle_solicitud_parametro.FolioLibretaMar = "";
                            detalle_solicitud_parametro.ObservacionesDetalle = "";

                            detalle_solicitud_parametro.VigenciaLibretaMar = null;

                            detalle_solicitud_parametro.fk_contrato = (nueva_solicitud_integral_diferir.TipoPersonal == "PEMEX") || (nueva_solicitud_integral_diferir.TipoPersonal != "PEMEX" && contrato_gestion_gl == null) ? (int?)null : contrato_gestion_gl.ID_C_CONTRATO;
                            //Aqui falta validar si el itinerario es de subida o bajada para cambiar la hora segun corresponda.
                            if (detalle_solicitud_parametro.fk_itinerario.HasValue)
                            {
                                detalle_solicitud_parametro.HoraViajeItemParajeroSTA = ObtenerHorarioSalidaItinerario(detalle_solicitud_parametro.fk_itinerario.Value, nueva_solicitud_integral_diferir.FechaSubida, detalle_solicitud_parametro.IdInstalacionDestino, stpIntegral_contex);
                            }
                            else
                            {
                                detalle_solicitud_parametro.HoraViajeItemParajeroSTA = nueva_solicitud_integral_diferir.HoraSalidaItinerarioTransporte;
                            }

                            //Cancelando el detalle de la solicitud anterior
                            detalle_solicitud_anterior_a_cancelar = stpIntegral_contex.TabDetallesSolicitudIntegral.SingleOrDefault(x => x.IdDetalleSolicitudIntegral == detalle_solicitud_parametro.IdDetalleSolicitudIntegral);

                            if (detalle_solicitud_anterior_a_cancelar != null)
                            {
                                detalle_solicitud_anterior_a_cancelar.EstadoHuespedAH = EstadosDetallesSolicitudIntegralAH.CANCELADO.ToString();
                                detalle_solicitud_anterior_a_cancelar.EstadoOcupanteSolicitudIntegral = EstadosDetallesSolicitudIntegral.CANCELADO.ToString();
                                detalle_solicitud_anterior_a_cancelar.EstadoPasajeroSTP = EstadosDetallesSolicitudIntegralSTP.CANCELADO.ToString();

                                if (String.IsNullOrEmpty(detalle_solicitud_anterior_a_cancelar.ObservacionesDetalle))
                                {
                                    detalle_solicitud_anterior_a_cancelar.ObservacionesDetalle = "";
                                }

                                //SE INSERTA AUDITORIA
                                auditoria.FkIdSolicitudIntegral = detalle_solicitud_anterior_a_cancelar.IdSolicitudIntegral;
                                auditoria.TipoMovimientoAuditoria = "DIFERIR SOLICITUD";
                                auditoria.DescripcionAuditoria = "CANCELA AL PASAJERO CON FICHA/RFC: " + detalle_solicitud_anterior_a_cancelar.FichaRFCEmpleado + " (" + detalle_solicitud_anterior_a_cancelar.NombreEmpleado + ") DE LA SOLICITUD: " + solicitudIntegralAnterior.FolioSolicitudIntegral;
                                auditoria.FkIdSolicitudIntegral = solicitudIntegralAnterior.IdSolicitudIntegral;
                                auditoria.ObjetoAntesModificacion = "";//js.Serialize(detalle_solicitud_integral_ANTERIOR);
                                auditoria.ObjetoDespuesModificacion = "";//js.Serialize(detalle_solicitud_integral);
                                AgregarAuditoria(stpIntegral_contex, auditoria);

                            }
                            else
                            {
                                throw new ShowCaseException(String.Format("ERR-SI-0055: No se encontro del detalle de la solicitud.", nueva_solicitud_integral_diferir.FolioSolicitudIntegral), null, TypeMessage.error, "ERR-SI-0055");
                            }

                            detalle_solicitud_parametro.IdDetalleSolicitudIntegral = 0;

                            //Agregando el detalle a la solicitud integral.
                            stpIntegral_contex.TabDetallesSolicitudIntegral.Add(detalle_solicitud_parametro);

                            //AUDITAR
                            stpIntegral_contex.SaveChanges();

                            //AUDITAR
                            auditoria.TipoMovimientoAuditoria = "DIFERIR SOLICITUD";
                            auditoria.DescripcionAuditoria = "GUARDA EL PASAJERO/HUESPED CON FICHA/RFC: " + detalle_solicitud_parametro.FichaRFCEmpleado + " (" + detalle_solicitud_parametro.NombreEmpleado + ")  EN EL DETALLE DEL FOLIO: " + nueva_solicitud_integral_diferir.FolioSolicitudIntegral.ToString();
                            auditoria.FkIdSolicitudIntegral = nueva_solicitud_integral_diferir.IdSolicitudIntegral;
                            auditoria.ObjetoAntesModificacion = "";
                            //auditoria.ObjetoDespuesModificacion = "hola";
                            auditoria.ObjetoDespuesModificacion = "";//js.Serialize(detalle_solicitud_parametro);
                            AgregarAuditoria(stpIntegral_contex, auditoria);

                            //Como ya se agrego un detalle entonces hay que quitar un espacio en la reservacion de transporte y/o hospedaje si aplica.

                            //Empezamos con transporte, el orden de prioridad es el siguiente, Primero se validan los folios especiales, luego los contratos, seguido de los centros gestores, por ultimo los espacios libres totales.
                            //Rerservando un nuevo espacio
                            //TODO: HACE FALTA PEDIR LOS FOLIOS ESPECIALES EN LA PANTALLA DE DIFERIR
                            AgregarEliminarEspacioReservacionSTPAH(stpIntegral_contex, nueva_solicitud_integral_diferir, detalle_solicitud_parametro, contrato_gestion_gl, "", "", "", instalacion_destino, 1, false, auditoria);
                            //Elimiando el espacio anterior
                            AgregarEliminarEspacioReservacionSTPAH(stpIntegral_contex, solicitudIntegralAnterior, detalle_solicitud_anterior_a_cancelar, contrato_gestion_gl, "", "", "", instalacion_destino, 1, true, auditoria);
                        }//fin foreach(STPIntegral.Datos.TabDetallesSolicitudIntegral detalle_solicitud_parametro in detalles_solicitud_integral)


                        //VERIFICANDO SI TODOS LOS DETALLES DE LA SOLICITUD ESTAN CANCELADOS 
                        CancelarSolicitudIntegralSiTodosLosDetallesEstanCancelados(solicitudIntegralAnterior.IdSolicitudIntegral, stpIntegral_contex, auditoria);
                    }

                    ////stpIntegral_contex.SaveChanges();

                    //   // si todo sale bien se cierra la transaccion, en caso contrario se aborta toda la operacion.
                    transaction_scope.Complete();
                }
                catch (ShowCaseException ex)
                {
                    throw ex;
                }
                catch (System.Exception ex)
                {
                    throw new ShowCaseException(String.Format("ERR-SI-0037: Ocurrio el siguiente error al intentar agregar un detalle a la Solicitud Integral [{0}]: '{1}'", nueva_solicitud_integral_diferir.FolioSolicitudIntegral, ex.Message), ex, TypeMessage.error, "ERR-SI-0037");
                }
            }

            respuesta = new SolicitudIntegralModel();

            respuesta.SolicitudIntegral = nueva_solicitud_integral_diferir;

            using (STPIntegral.Datos.STPIntegralModel stpIntegralContext = new Datos.STPIntegralModel())
            {
                stpIntegralContext.Configuration.LazyLoadingEnabled = false;
                stpIntegralContext.Configuration.ProxyCreationEnabled = false;

                respuesta.DetallesSolicitudIntegral = stpIntegralContext.TabDetallesSolicitudIntegral.Where(x => x.IdSolicitudIntegral == nueva_solicitud_integral_diferir.IdSolicitudIntegral).ToList();
            }

            respuesta.ContratoSap = null;
            respuesta.InstalacionesDestino = null;
            respuesta.InstalacionesOrigen = null;
            respuesta.ItinerariosTransporte = null;
            respuesta.ListadoDireccionamientosDestinos = null;

            return respuesta;
        }


        //AUDITAR
        public static List<STPIntegral.Datos.TabDetallesSolicitudIntegral> CambiarOrigenDestinoPasajero(STPIntegral.Datos.TabDetallesSolicitudIntegral _detalles_solicitud_integral, STPIntegral.Datos.TabAuditoriaSolicitudIntegral auditoria)
        {

            List<STPIntegral.Datos.TabDetallesSolicitudIntegral> respuesta = new List<Datos.TabDetallesSolicitudIntegral>();

            //Contextos para obtener acceso a la base de datos.
            GestionGL.Datos.GestionGLModel gestionGL_context = new GestionGL.Datos.GestionGLModel();
            STPIntegral.Datos.STPIntegralModel stpIntegral_contex = new Datos.STPIntegralModel();
            stpIntegral_contex.Configuration.LazyLoadingEnabled = false;
            gestionGL_context.Configuration.LazyLoadingEnabled = false;
            stpIntegral_contex.Configuration.ProxyCreationEnabled = false;
            gestionGL_context.Configuration.ProxyCreationEnabled = false;

            STPIntegral.Datos.TabSolicitudIntegral solicitud_integral = stpIntegral_contex.TabSolicitudIntegral.Single(x => x.IdSolicitudIntegral == _detalles_solicitud_integral.IdSolicitudIntegral);
            STPIntegral.Datos.TabDetallesSolicitudIntegral detalle_solicitud_a_actualizar_ins = stpIntegral_contex.TabDetallesSolicitudIntegral.Single(x => x.IdDetalleSolicitudIntegral == _detalles_solicitud_integral.IdDetalleSolicitudIntegral);
         
            GestionGL.Datos.C_M_INSTALACIONES instalacion_destino_liberar = gestionGL_context.C_M_INSTALACIONES.Single(x => x.ID_C_M_INSTALACION == detalle_solicitud_a_actualizar_ins.IdInstalacionDestino);
            GestionGL.Datos.C_M_INSTALACIONES instalacion_destino_ocupar = gestionGL_context.C_M_INSTALACIONES.Single(x => x.ID_C_M_INSTALACION == _detalles_solicitud_integral.IdInstalacionDestino);
            stpIntegral_contex.Dispose();
            gestionGL_context.Dispose();

            //INICIAN VALIDACIONES TRANSACCIONADAS
            using (TransactionScope transaction_scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = IsolationLevel.Serializable }))
            {
                try
                {
                    //Como toda la parte transaccional quedo solo en STPIntegral, entonces se crea un nuevo contexto para que todo esto se transaccione.
                    using (stpIntegral_contex = new Datos.STPIntegralModel())
                    {
                        STPIntegral.Datos.TabDetallesSolicitudIntegral detalle_solicitud_a_actualizar = stpIntegral_contex.TabDetallesSolicitudIntegral.Single(x => x.IdDetalleSolicitudIntegral == _detalles_solicitud_integral.IdDetalleSolicitudIntegral);

                        if (solicitud_integral.TipoServicio != "CAMBIO DE GUARDIA")
                        {
                            if (solicitud_integral.TipoSolicitudIntegral != "SOLO TRANSPORTE")
                            {
                                //LIBERA
                                AgregarEliminarEspacioReservacionAH(stpIntegral_contex, solicitud_integral, detalle_solicitud_a_actualizar, null, "", "", "", instalacion_destino_liberar, 1, true, auditoria);

                                //TODO:OCUPA
                                AgregarEliminarEspacioReservacionAH(stpIntegral_contex, solicitud_integral, detalle_solicitud_a_actualizar, null, "", "", "", instalacion_destino_ocupar, 1, false, auditoria);
                            }

                            STPIntegral.Datos.TabDetallesSolicitudIntegral detalle_solicitud_a_actualizar_retorno = stpIntegral_contex.TabDetallesSolicitudIntegral.SingleOrDefault(x => x.IdSolicitudIntegral == _detalles_solicitud_integral.IdSolicitudIntegral && x.FichaRFCEmpleado == _detalles_solicitud_integral.FichaRFCEmpleado && x.TipoMovimientoPersonal == "RETORNO");

                            if (detalle_solicitud_a_actualizar_retorno != null)
                            {
                                detalle_solicitud_a_actualizar_retorno.IdInstalacionOrigen = _detalles_solicitud_integral.IdInstalacionDestino;
                                detalle_solicitud_a_actualizar_retorno.IdInstalacionDestino = _detalles_solicitud_integral.IdInstalacionOrigen;
                                detalle_solicitud_a_actualizar_retorno.ClaveInstalacionOrigen = _detalles_solicitud_integral.ClaveInstalacionDestino;
                                detalle_solicitud_a_actualizar_retorno.ClaveInstalacionDestino = _detalles_solicitud_integral.ClaveInstalacionOrigen;
                                detalle_solicitud_a_actualizar_retorno.NombreInstalacionOrigen = _detalles_solicitud_integral.NombreInstalacionDestino;
                                detalle_solicitud_a_actualizar_retorno.NombreInstalacionDestino = _detalles_solicitud_integral.NombreInstalacionOrigen;
                                detalle_solicitud_a_actualizar_retorno.FondoDireccionamientoAH = _detalles_solicitud_integral.FondoDireccionamientoAH;
                                detalle_solicitud_a_actualizar_retorno.CentroGestorDireccionamientoAH = _detalles_solicitud_integral.CentroGestorDireccionamientoAH;
                                detalle_solicitud_a_actualizar_retorno.ElementoPEPDireccionamiento = _detalles_solicitud_integral.ElementoPEPDireccionamiento;
                                detalle_solicitud_a_actualizar_retorno.CuentaMayorDireccionamientoAH = _detalles_solicitud_integral.CuentaMayorDireccionamientoAH;
                                detalle_solicitud_a_actualizar_retorno.ProgramaPresupuestalDireccionamientoAH = _detalles_solicitud_integral.ProgramaPresupuestalDireccionamientoAH;
                                detalle_solicitud_a_actualizar_retorno.PosicionPresupuestalDireccionamientoAH = _detalles_solicitud_integral.PosicionPresupuestalDireccionamientoAH;

                                detalle_solicitud_a_actualizar_retorno.FondoDireccionamientoSTP = _detalles_solicitud_integral.FondoDireccionamientoSTP;
                                detalle_solicitud_a_actualizar_retorno.CentroGestorDireccionamientoSTP = _detalles_solicitud_integral.CentroGestorDireccionamientoSTP;
                                detalle_solicitud_a_actualizar_retorno.ElementoPEPDireccionamientoSTP = _detalles_solicitud_integral.ElementoPEPDireccionamientoSTP;
                                detalle_solicitud_a_actualizar_retorno.CuentaMayorDireccionamientoSTP = _detalles_solicitud_integral.CuentaMayorDireccionamientoSTP;
                                detalle_solicitud_a_actualizar_retorno.ProgramaPresupuestalDireccionamientoSTP = _detalles_solicitud_integral.ProgramaPresupuestalDireccionamientoSTP;
                                detalle_solicitud_a_actualizar_retorno.PosicionPresupuestalDireccionamientoSTP = _detalles_solicitud_integral.PosicionPresupuestalDireccionamientoSTP;

                                respuesta.Add(detalle_solicitud_a_actualizar_retorno);
                            }

                            string origen = detalle_solicitud_a_actualizar.NombreInstalacionOrigen;
                            string destino = detalle_solicitud_a_actualizar.NombreInstalacionDestino;

                            detalle_solicitud_a_actualizar.IdInstalacionOrigen = _detalles_solicitud_integral.IdInstalacionDestino;
                            detalle_solicitud_a_actualizar.IdInstalacionDestino = _detalles_solicitud_integral.IdInstalacionDestino;
                            detalle_solicitud_a_actualizar.ClaveInstalacionOrigen = _detalles_solicitud_integral.ClaveInstalacionOrigen;
                            detalle_solicitud_a_actualizar.ClaveInstalacionDestino = _detalles_solicitud_integral.ClaveInstalacionDestino;
                            detalle_solicitud_a_actualizar.NombreInstalacionOrigen = _detalles_solicitud_integral.NombreInstalacionOrigen;
                            detalle_solicitud_a_actualizar.NombreInstalacionDestino = _detalles_solicitud_integral.NombreInstalacionDestino;
                            detalle_solicitud_a_actualizar.FondoDireccionamientoAH = _detalles_solicitud_integral.FondoDireccionamientoAH;
                            detalle_solicitud_a_actualizar.CentroGestorDireccionamientoAH = _detalles_solicitud_integral.CentroGestorDireccionamientoAH;
                            detalle_solicitud_a_actualizar.ElementoPEPDireccionamiento = _detalles_solicitud_integral.ElementoPEPDireccionamiento;
                            detalle_solicitud_a_actualizar.CuentaMayorDireccionamientoAH = _detalles_solicitud_integral.CuentaMayorDireccionamientoAH;
                            detalle_solicitud_a_actualizar.ProgramaPresupuestalDireccionamientoAH = _detalles_solicitud_integral.ProgramaPresupuestalDireccionamientoAH;
                            detalle_solicitud_a_actualizar.PosicionPresupuestalDireccionamientoAH = _detalles_solicitud_integral.PosicionPresupuestalDireccionamientoAH;

                            detalle_solicitud_a_actualizar.FondoDireccionamientoSTP = _detalles_solicitud_integral.FondoDireccionamientoSTP;
                            detalle_solicitud_a_actualizar.CentroGestorDireccionamientoSTP = _detalles_solicitud_integral.CentroGestorDireccionamientoSTP;
                            detalle_solicitud_a_actualizar.ElementoPEPDireccionamientoSTP = _detalles_solicitud_integral.ElementoPEPDireccionamientoSTP;
                            detalle_solicitud_a_actualizar.CuentaMayorDireccionamientoSTP = _detalles_solicitud_integral.CuentaMayorDireccionamientoSTP;
                            detalle_solicitud_a_actualizar.ProgramaPresupuestalDireccionamientoSTP = _detalles_solicitud_integral.ProgramaPresupuestalDireccionamientoSTP;
                            detalle_solicitud_a_actualizar.PosicionPresupuestalDireccionamientoSTP = _detalles_solicitud_integral.PosicionPresupuestalDireccionamientoSTP;
                            stpIntegral_contex.SaveChanges();

                            // SE INSERTA AUDITORIA
                            auditoria.TipoMovimientoAuditoria = "CAMBIO ORIGEN DESTINO";
                            auditoria.DescripcionAuditoria = "ACTUALIZA EL ORIGEN Y/O DESTINO, TENIA : " + origen + " - " + destino + " DEL PASAJERO: " + detalle_solicitud_a_actualizar.FichaRFCEmpleado + " (" + detalle_solicitud_a_actualizar.NombreEmpleado + ") DE LA SOLICITUD: " + solicitud_integral.FolioSolicitudIntegral;
                            auditoria.FkIdSolicitudIntegral = solicitud_integral.IdSolicitudIntegral;
                            auditoria.ObjetoAntesModificacion = "";//js.Serialize(detalle_solicitud_integral_ANTERIOR);
                            auditoria.ObjetoDespuesModificacion = "";//js.Serialize(detalle_solicitud_integral);
                            AgregarAuditoria(stpIntegral_contex, auditoria);

                            respuesta.Add(detalle_solicitud_a_actualizar);
                        }
                        else 
                        {
                            throw new ShowCaseException("ERR-SI-XXXX: No se puede hacer el cambio de origen y/o destino en una solicitud del tipo 'CAMBIO DE GUARDIA'", TypeMessage.error, "ERR-SI-XXXX");
                        
                        }
                    }
                    //Si todo sale bien se cierra la transaccion, en caso contrario se aborta toda la operacion.
                    transaction_scope.Complete();
                }
                catch (ShowCaseException ex)
                {
                    throw ex;
                }
                catch (System.Exception ex)
                {
                    StringBuilder mensaje_error = new StringBuilder();

                    mensaje_error.Append(ex.Message);

                    if (ex.InnerException != null)
                    {
                        mensaje_error.Append(String.Format(" Detalles: {0}", ex.InnerException.Message));
                    }

                    throw new ShowCaseException(String.Format("ERR-SI-0037: Ocurrio el siguiente error al intentar agregar un detalle a la Solicitud Integral [{0}]: '{1}'", solicitud_integral.FolioSolicitudIntegral, mensaje_error.ToString()), ex, TypeMessage.error, "ERR-SI-0037");
                }
            }

            foreach (STPIntegral.Datos.TabDetallesSolicitudIntegral detalle in respuesta)
            {
                detalle.TabSolicitudIntegral = null;
                detalle.ah_tb_capacidad_detalle = null;
            }
            
            
            return respuesta;
        }

        #endregion
    }
}
