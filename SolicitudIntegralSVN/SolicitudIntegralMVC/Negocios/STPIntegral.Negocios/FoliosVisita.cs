﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace STPIntegral.Negocios
{
    public class FoliosVisita
    {
        public static STPIntegral.Datos.TabFolioVisitas GuardarFolio(STPIntegral.Datos.TabFolioVisitas folio_visita)
        {
            STPIntegral.Datos.TabFolioVisitas resultado = null;

            if (string.IsNullOrEmpty(folio_visita.FolioVisita))
            {
                throw new ShowCaseException("Debe especificar un folio de visita");
            }

            if (folio_visita.FechaInicio == DateTime.MinValue || folio_visita.FechaFin == DateTime.MinValue)
            {
                throw new ShowCaseException("Debe especificar la Fecha de Inicio y Fin");
            }

            if (string.IsNullOrEmpty(folio_visita.FichaAutorizador) || string.IsNullOrEmpty(folio_visita.FichaSolicitante))
            {
                throw new ShowCaseException("Debe especificar la ficha del autorizador y el solicitante");
            }

            STPIntegral.Datos.TabFolioVisitas folioVisita = null;

            using (STPIntegral.Datos.STPIntegralModel stpIntegralModel = new Datos.STPIntegralModel())
            {
                if (folio_visita.IdFolioVisita <= 0)
                {
                    //Alta del folio
                    GestionGL.Datos.C_EMPLEADOS_PEMEX empleado = null;

                    using (GestionGL.Datos.GestionGLModel gestionGlModel = new GestionGL.Datos.GestionGLModel())
                    {
                        empleado = gestionGlModel.C_EMPLEADOS_PEMEX.Where(x => x.FICHA == folio_visita.FichaAutorizador).FirstOrDefault();

                        if (empleado == null)
                        {
                            throw new ShowCaseException(String.Format("Debe especificar una ficha del autorizador valido", TypeMessage.error));
                        }

                        empleado = gestionGlModel.C_EMPLEADOS_PEMEX.Where(x => x.FICHA == folio_visita.FichaSolicitante).FirstOrDefault();

                        if (empleado == null)
                        {
                            throw new ShowCaseException(String.Format("Debe especificar una ficha del solicitante valido", TypeMessage.error));
                        }
                    }

                    folioVisita = stpIntegralModel.TabFolioVisitas.Where(x => x.FolioVisita == folio_visita.FolioVisita && x.BorradoLogico == false).FirstOrDefault();

                    if (folioVisita != null)
                    {
                        throw new ShowCaseException(String.Format("Folio Duplicado", TypeMessage.error));
                    }

                    stpIntegralModel.TabFolioVisitas.Add(folio_visita);

                    resultado = folio_visita;
                }
                else
                {
                    //Modificacion del folio
                    STPIntegral.Datos.TabFolioVisitas folio_visita_modificar = stpIntegralModel.TabFolioVisitas.SingleOrDefault(x => x.IdFolioVisita == folio_visita.IdFolioVisita);

                    if (folio_visita_modificar != null)
                    {
                        //validar si esta eliminando el folio visita
                        if (folio_visita.BorradoLogico == false)
                        {
                            folio_visita_modificar.Foliodocumento = folio_visita.Foliodocumento;
                            folio_visita_modificar.FechaInicio = folio_visita.FechaInicio;
                            folio_visita_modificar.FechaFin = folio_visita.FechaFin;
                            folio_visita_modificar.RfcCompania = folio_visita.RfcCompania;
                            folio_visita_modificar.NoAcreedorSap = folio_visita.NoAcreedorSap;
                            folio_visita_modificar.NombreCompania = folio_visita.NombreCompania;
                            folio_visita_modificar.Motivo = folio_visita.Motivo;
                        }
                        else
                        {
                            folio_visita_modificar.BorradoLogico = folio_visita.BorradoLogico;
                        }
                    }

                    resultado = folio_visita_modificar;
                }

                stpIntegralModel.SaveChanges();
            }

            return resultado;
        }


    }
}
