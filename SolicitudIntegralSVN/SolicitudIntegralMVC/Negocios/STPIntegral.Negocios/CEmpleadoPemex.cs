﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace STPIntegral.Negocios
{
    public class CEmpleadoPemex
    {

        public static GestionGL.Datos.C_EMPLEADOS_PEMEX GuardarEmpleado(GestionGL.Datos.C_EMPLEADOS_PEMEX empleado_pemex)
        {
            GestionGL.Datos.C_EMPLEADOS_PEMEX resultado = null;

            //Validar que hayan escrito el RFC.
            if (String.IsNullOrEmpty(empleado_pemex.FICHA))
            {
                throw new ShowCaseException("Debe especificar una Ficha para el empleado");
            }

            if (!Utilerias.ValidarFicha(empleado_pemex.FICHA))
            {
                throw new ShowCaseException("La Ficha del empleado solo puede contener caracteres numéricos: [0-9]");
            }
            else
            {
                empleado_pemex.FICHA = Utilerias.FormatearFichaRfc(empleado_pemex.FICHA);
            }

            empleado_pemex.CURP = "";
            empleado_pemex.EMAIL = "";
            empleado_pemex.CARGO = "";
            empleado_pemex.EXTENSION_TELEFONICA = "";
            empleado_pemex.ID_C_CONTRATO = null;
            empleado_pemex.RFC = "";

                        //Validar que hayan escrito el nombre y los apellidos.
            if (String.IsNullOrEmpty(empleado_pemex.NOMBRES))
            {
                throw new ShowCaseException("Debe especificar el nombre del empleado");
            }

            if (String.IsNullOrEmpty(empleado_pemex.APELLIDOS))
            {
                throw new ShowCaseException("Debe especificar los apellidos del empleado.");
            }

            List<GestionGL.Datos.C_EMPLEADOS_PEMEX> listado_empleados_pemex_busqueda = null;

            empleado_pemex.APELLIDOS = empleado_pemex.APELLIDOS.ToUpper();
            empleado_pemex.CATEGORIA = empleado_pemex.CATEGORIA.ToUpper();
            empleado_pemex.GENERO = empleado_pemex.GENERO.ToUpper();
            empleado_pemex.NOMBRES = empleado_pemex.NOMBRES.ToUpper();
            empleado_pemex.FICHA = empleado_pemex.FICHA.ToUpper();

            using (GestionGL.Datos.GestionGLModel gestionGlModel = new GestionGL.Datos.GestionGLModel())
            {
                if (empleado_pemex.ID_C_EMPLEADO_PEMEX <= 0)
                {
                    //Alta de personal
                    //Validar que no exista otro empleado con el mismo RFC ni con el mismo nombre.
                    listado_empleados_pemex_busqueda = gestionGlModel.C_EMPLEADOS_PEMEX.Where(x => x.FICHA == empleado_pemex.FICHA).ToList();

                    if (listado_empleados_pemex_busqueda.Count > 0)
                    {
                        throw new ShowCaseException(String.Format("No se puede guardar un empleado nuevo pues ya existe otro con la misma Ficha: [{0}] {1}.", listado_empleados_pemex_busqueda[0].FICHA, listado_empleados_pemex_busqueda[0].NOMBRES + " " + listado_empleados_pemex_busqueda[0].APELLIDOS), TypeMessage.error);
                    }

                    listado_empleados_pemex_busqueda = gestionGlModel.C_EMPLEADOS_PEMEX.Where(x => x.NOMBRES == empleado_pemex.NOMBRES && x.APELLIDOS == empleado_pemex.APELLIDOS).ToList();

                    if (listado_empleados_pemex_busqueda.Count > 0)
                    {
                        throw new ShowCaseException(String.Format("No se puede guardar un empleado nuevo pues ya existe otro con el mismo nombre y apellidos: [{0}] {1}.", listado_empleados_pemex_busqueda[0].FICHA, listado_empleados_pemex_busqueda[0].NOMBRES + " " + listado_empleados_pemex_busqueda[0].APELLIDOS), TypeMessage.error);
                    }

                    gestionGlModel.C_EMPLEADOS_PEMEX.Add(empleado_pemex);

                    resultado = empleado_pemex;
                }
                else
                {
                    //Modificacion de personal
                    //Validar que no exista otro empleado con el mismo RFC ni con el mismo nombre.
                    listado_empleados_pemex_busqueda = gestionGlModel.C_EMPLEADOS_PEMEX.Where(x => x.FICHA == empleado_pemex.FICHA && x.ID_C_EMPLEADO_PEMEX != empleado_pemex.ID_C_EMPLEADO_PEMEX).ToList();

                    if (listado_empleados_pemex_busqueda.Count > 0)
                    {
                        throw new ShowCaseException(String.Format("No se puede guardar un empleado nuevo pues ya existe otro con la misma Ficha: [{0}] {1}.", listado_empleados_pemex_busqueda[0].FICHA, listado_empleados_pemex_busqueda[0].NOMBRES + " " + listado_empleados_pemex_busqueda[0].APELLIDOS), TypeMessage.error);
                    }

                    listado_empleados_pemex_busqueda = gestionGlModel.C_EMPLEADOS_PEMEX.Where(x => x.NOMBRES == empleado_pemex.NOMBRES && x.APELLIDOS == empleado_pemex.APELLIDOS && x.ID_C_EMPLEADO_PEMEX != empleado_pemex.ID_C_EMPLEADO_PEMEX).ToList();

                    if (listado_empleados_pemex_busqueda.Count > 0)
                    {
                        throw new ShowCaseException(String.Format("No se puede guardar un empleado nuevo pues ya existe otro con el mismo nombre y apellidos: [{0}] {1}.", listado_empleados_pemex_busqueda[0].FICHA, listado_empleados_pemex_busqueda[0].NOMBRES + " " + listado_empleados_pemex_busqueda[0].APELLIDOS), TypeMessage.error);
                    }

                    GestionGL.Datos.C_EMPLEADOS_PEMEX empleado_pemex_modificar = gestionGlModel.C_EMPLEADOS_PEMEX.SingleOrDefault(x => x.ID_C_EMPLEADO_PEMEX == empleado_pemex.ID_C_EMPLEADO_PEMEX);

                    if (empleado_pemex_modificar != null)
                    {
                        //empleado_pemex_modificar.APELLIDOS = empleado_pemex.APELLIDOS;
                        empleado_pemex_modificar.CATEGORIA = empleado_pemex.CATEGORIA;
                        empleado_pemex_modificar.GENERO = empleado_pemex.GENERO;
                        //empleado_pemex_modificar.NOMBRES = empleado_pemex.NOMBRES;
                        empleado_pemex_modificar.FICHA = empleado_pemex.FICHA;
                    }

                    resultado = empleado_pemex_modificar;
                }

                gestionGlModel.SaveChanges();
            }

            return resultado;
        }

    }
}
