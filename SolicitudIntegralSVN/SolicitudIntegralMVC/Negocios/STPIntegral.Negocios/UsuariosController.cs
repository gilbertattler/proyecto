﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace STPIntegral.Negocios
{
    public class UsuariosController
    {
        public static GestionGL.Datos.M_USUARIOS ObtenerMUsuario(int idMUsuario)
        {
            GestionGL.Datos.M_USUARIOS usuario = null;

            using (GestionGL.Datos.GestionGLModel gestionGlModel = new GestionGL.Datos.GestionGLModel())
            {
                usuario = gestionGlModel.M_USUARIOS.SingleOrDefault(x => x.ID_M_USUARIO == idMUsuario);
            }

            return usuario;
        }

        public static GestionGL.Datos.M_USUARIOS CambiarContrasenia(int idMUsuario, string password_anterior, string nuevo_password)
        {
            GestionGL.Datos.M_USUARIOS usuario = null;

            using (GestionGL.Datos.GestionGLModel gestionGlModel = new GestionGL.Datos.GestionGLModel())
            {
                usuario = gestionGlModel.M_USUARIOS.SingleOrDefault(x => x.ID_M_USUARIO == idMUsuario);

                if (usuario.PASS.ToUpper() == password_anterior.ToUpper())
                {
                    usuario.PASS = nuevo_password;
                    gestionGlModel.SaveChanges();
                }
                else
                {
                    throw new ShowCaseException("La contraseña anterior no coincide, favor de verificar", TypeMessage.alert, "");
                }
            }

            return usuario;
        }

        public static GestionGL.Datos.M_USUARIOS ActualizarNombreUsuarioDominio(int idMUsuario, string password_anterior, string nombre_usuario_dominio)
        {
            GestionGL.Datos.M_USUARIOS usuario = null;

            using (GestionGL.Datos.GestionGLModel gestionGlModel = new GestionGL.Datos.GestionGLModel())
            {
                usuario = gestionGlModel.M_USUARIOS.SingleOrDefault(x => x.ID_M_USUARIO == idMUsuario);

                List<GestionGL.Datos.M_USUARIOS> listado_usuarios_dominio_repetido = gestionGlModel.M_USUARIOS.Where(x => x.ID_M_USUARIO != idMUsuario && x.DOMINIO.ToUpper() == nombre_usuario_dominio.ToUpper()).ToList();

                if (listado_usuarios_dominio_repetido != null && listado_usuarios_dominio_repetido.Count > 0)
                {
                    throw new ShowCaseException(String.Format("Otro usuario ya esta registrado con la cuenta de dominio: {0}",nombre_usuario_dominio), TypeMessage.alert, "");
                }

                if (usuario.PASS.ToUpper() == password_anterior.ToUpper())
                {
                    usuario.DOMINIO = nombre_usuario_dominio;
                    gestionGlModel.SaveChanges();
                }
                else
                {
                    throw new ShowCaseException("La contraseña no coincide, favor de verificar", TypeMessage.alert, "");
                }
            }

            return usuario;
        }
    }
}
