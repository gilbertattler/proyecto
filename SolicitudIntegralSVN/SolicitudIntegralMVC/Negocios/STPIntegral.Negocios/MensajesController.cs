﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace STPIntegral.Negocios
{
    public class MensajesController
    {
        public static IEnumerable<Datos.stp_c_mensajes> ObtenerNoticiasActualesParaSolicitudIntegral()
        {
            IEnumerable<Datos.stp_c_mensajes> listado_noticias = null;

            using (STPIntegral.Datos.STPIntegralModel stp_integral_model = new Datos.STPIntegralModel())
            {
                listado_noticias = stp_integral_model.stp_c_mensajes.Where(x => x.mostrar_si == true && x.borrado_logico == false && (x.inicio <= System.DateTime.Now && x.termino >= System.DateTime.Now)).OrderByDescending(x => x.fec_crea).ToList();
            }

            return listado_noticias;
        }
    }
}
