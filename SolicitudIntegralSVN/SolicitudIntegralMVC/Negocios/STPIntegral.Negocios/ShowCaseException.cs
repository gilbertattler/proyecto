﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Reflection;
using System.Web.Script.Serialization;

namespace STPIntegral.Negocios
{
    public enum TypeMessage
    {
        alert,
        success,
        error,
        warning,
        information,
        confirmation 
    }
    
    public class ShowCaseException : ApplicationException
    {
        private List<string> reasons = new List<string>();
        private TypeMessage message_type { get; set; }
        private string error_code { get; set; }
        public ShowCaseException(TypeMessage _message_type = TypeMessage.warning, string _error_code = "") : base() { message_type = _message_type; error_code = _error_code; }
        public ShowCaseException(string _message, TypeMessage _message_type = TypeMessage.warning, string _error_code = "") : base(_message) { message_type = _message_type; error_code = _error_code; }
        public ShowCaseException(string _message, Exception innerException, TypeMessage _message_type = TypeMessage.warning, string _error_code = "") : base(_message, innerException) { message_type = _message_type; }
        
        public List<string> Reasons { get { return reasons; } }
        public string TMessage { get { return Enum.GetName(typeof(TypeMessage), message_type); } }
        
        public static string SerializeException(Exception ex)  
        {  
            JavaScriptSerializer serializer = new JavaScriptSerializer();  
  
            StringBuilder exSerialized = new StringBuilder("{");  
            PropertyInfo[] exProperties = ex.GetType().GetProperties(BindingFlags.Public | BindingFlags.FlattenHierarchy | BindingFlags.Instance);  
            foreach (PropertyInfo property in exProperties.Where(p => p.Name != "TargetSite"))  
            {  
                //if (![DEBUG_MODE] &&  property.Name == "InnerException" || property.Name == "Source" || property.Name == "StackTrace"))  
                //    continue;  
  
                exSerialized.AppendFormat("\"{0}\":", property.Name);
                if (property.Name != "InnerException")
                {
                    serializer.Serialize(property.GetValue(ex, null), exSerialized);
                    exSerialized.Append(", ");
                }
                else
                {
                    serializer.Serialize(ex.InnerException != null ? ex.InnerException.Message : "", exSerialized);
                    exSerialized.Append(", ");
                }

            }  
  
            //if ([DEBUG_MODE])   
            //{  
            //    exSerialized.AppendFormat("\"ToString\":");  
            //    serializer.Serialize(ex.ToString(), exSerialized);  
            //    exSerialized.Append(", ");  
            //}  
  
            exSerialized.Remove(exSerialized.Length - 2, 2);  
            exSerialized.Append("}");  
            return exSerialized.ToString();  
        } 
    }
}