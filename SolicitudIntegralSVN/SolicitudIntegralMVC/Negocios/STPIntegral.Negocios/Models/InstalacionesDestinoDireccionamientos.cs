﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STPIntegral.Negocios.Models
{
    public class InstalacionesDestinoDireccionamientos
    {
        public IEnumerable<GestionGL.Datos.C_M_INSTALACIONES> InstalacionesDestino;
        public IEnumerable<GestionGL.Datos.VW_PERMISOS_DIRECCIONAMIENTOS_GENERAL> DireccionamientosInstalaciones;
    }
}
