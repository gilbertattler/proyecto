﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STPIntegral.Negocios.Models
{
    public class SolicitudIntegralModel
    {
        public STPIntegral.Datos.TabSolicitudIntegral SolicitudIntegral;
        public IEnumerable<STPIntegral.Datos.TabDetallesSolicitudIntegral> DetallesSolicitudIntegral;

        public GestionGL.Datos.VW_C_CONTRATOS ContratoSap;
        public IEnumerable<STPIntegral.Datos.Vw_SoltIntItinerariosCapacidadDia_NOLOCK> ItinerariosTransporte;

        public IEnumerable<GestionGL.Datos.C_M_INSTALACIONES> InstalacionesOrigen;
        public IEnumerable<GestionGL.Datos.C_M_INSTALACIONES> InstalacionesDestino;

        public IEnumerable<GestionGL.Datos.VW_PERMISOS_DIRECCIONAMIENTOS_GENERAL> ListadoDireccionamientosDestinos;
        public IEnumerable<GestionGL.Datos.VW_PERMISOS_DIRECCIONAMIENTOS_GENERAL> ListadoDireccionamientosOrigenes;
    }
}
