﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace STPIntegral.Negocios.Models
{
    public class CapacidadReprogramacionSolicitudIntegral
    {
        public int FkIdInstalacionOrigen { get; set; }
        public int FkIdInstalacionDestino { get; set; }
        public string NombreInstalacionOrigen { get; set; }
        public string NombreInstalacionDestino { get; set; }
        public string SiglasInstalacionOrigen { get; set; }
        public string SiglasInstalacionDestino { get; set; }
        public DateTime FechaSubidaOriginal { get; set; }
        public DateTime? FechaBajadaOriginal { get; set; }
        public DateTime FechaSubidaNueva { get; set; }
        public DateTime? FechaBajadaNueva { get; set; }
        public int CapacidadRequeridaTransporte { get; set; }
        public int CapacidadRequeridaHospedaje { get; set; }
        public int CapacidadDisponibleTransporte { get; set; }
        public int CapacidadDisponibleHospedaje { get; set; }
        public int? FkIdItinerarioTransporte { get; set; }
        public string NombreItinerario { get; set; }
    }
}
