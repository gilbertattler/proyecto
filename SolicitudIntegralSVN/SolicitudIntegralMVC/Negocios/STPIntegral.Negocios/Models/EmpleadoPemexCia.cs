﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STPIntegral.Negocios.Models
{
    public class EmpleadoPemexCia
    {
        public int IdEmpleadoPemexCia {get; set; }
        public string TipoEmpleado  {get; set; }
        public string FichaRfc  {get; set; }
        public string NombreCompleto  {get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string Categoria { get; set; }
        public string Genero { get; set; }
        public string Nacionalidad { get; set; }
        public string RFC { get; set; }
        public string CURP { get; set; }
        public string FolioLibretaMar { get; set; }
        public DateTime? VigenciaLibretaMar { get; set; }
    }
}
