﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace STPIntegral.Negocios
{
    public class CEmpleadoCia
    {
        public static GestionGL.Datos.C_EMPLEADOS_CIA GuardarEmpleado(GestionGL.Datos.C_EMPLEADOS_CIA empleado_cia)
        {
            GestionGL.Datos.C_EMPLEADOS_CIA resultado = null;

            if (empleado_cia.CATEGORIA == null)
            {
                empleado_cia.CATEGORIA = "";
            }

            empleado_cia.CURP = "";
            empleado_cia.ID_C_CONTRATO = null;

            //Validar que hayan escrito el RFC.
            if(String.IsNullOrEmpty(empleado_cia.RFC_FM3))
            {
                throw new ShowCaseException("Debe especificar el RFC del empleado");
            }

            if (!Utilerias.ValidarRFC(empleado_cia.RFC_FM3))
            {
                throw new ShowCaseException("El RFC del empleado solo puede contener caracteres del alfabeto y numéricos: [A-Z, a-z, 0-9]");
            }

            empleado_cia.RFC_FM3 = Utilerias.FormatearFichaRfc(empleado_cia.RFC_FM3);

            //Validar que hayan escrito el nombre y los apellidos.
            if (String.IsNullOrEmpty(empleado_cia.NOMBRES))
            {
                throw new ShowCaseException("Debe especificar el nombre del empleado");
            }

            //if (String.IsNullOrEmpty(empleado_cia.APELLIDOS))
            //{
            //    throw new ShowCaseException("Debe especificar los apellidos del empleado.");
            //}

            //Validar que hayan escrito el folio de la libreta de Mar
            if (String.IsNullOrEmpty(empleado_cia.FOLIO_LIBRETA_MAR))
            {
                throw new ShowCaseException("Debe especificar un folio de Libreta de Mar.");
            }

            //Validar la vigencia de la libreta de Mar.
            if (!empleado_cia.VIGENCIA_LIBRETA_MAR.HasValue)
            {
                throw new ShowCaseException("Debe especificar la fecha de vigencia de la libreta de Mar.");
            }

            if (empleado_cia.VIGENCIA_LIBRETA_MAR.HasValue && empleado_cia.VIGENCIA_LIBRETA_MAR.Value.Date <= System.DateTime.Now.Date)
            {
                throw new ShowCaseException("La vigencia de la libreta de mar debe ser mayor al dia actual.");
            }

            List<GestionGL.Datos.C_EMPLEADOS_CIA> listado_empleados_cia_busqueda = null;

            empleado_cia.APELLIDOS = empleado_cia.APELLIDOS.ToUpper();
            empleado_cia.CATEGORIA = empleado_cia.CATEGORIA.ToUpper();
            empleado_cia.FOLIO_LIBRETA_MAR = empleado_cia.FOLIO_LIBRETA_MAR.ToUpper();
            empleado_cia.NACIONALIDAD = empleado_cia.NACIONALIDAD.ToUpper();
            empleado_cia.GENERO = empleado_cia.GENERO.ToUpper();
            empleado_cia.NOMBRES = empleado_cia.NOMBRES.ToUpper();
            empleado_cia.RFC_FM3 = empleado_cia.RFC_FM3.ToUpper();

            using (GestionGL.Datos.GestionGLModel gestionGlModel = new GestionGL.Datos.GestionGLModel())
            {
                if (empleado_cia.ID_C_EMPLEADO_CIA <= 0)
                {
                    //Alta de personal
                    //Validar que no exista otro empleado con el mismo RFC ni con el mismo nombre.

                    listado_empleados_cia_busqueda = gestionGlModel.C_EMPLEADOS_CIA.Where(x => x.RFC_FM3 == empleado_cia.RFC_FM3).ToList();

                    if (listado_empleados_cia_busqueda.Count > 0)
                    {
                        throw new ShowCaseException(String.Format("No se puede guardar un empleado nuevo pues ya existe otro con el mismo RFC: [{0}] {1}.",listado_empleados_cia_busqueda[0].RFC_FM3, listado_empleados_cia_busqueda[0].NOMBRES +  " " + listado_empleados_cia_busqueda[0].APELLIDOS),TypeMessage.error);
                    }

                    listado_empleados_cia_busqueda = gestionGlModel.C_EMPLEADOS_CIA.Where(x => x.NOMBRES == empleado_cia.NOMBRES && x.APELLIDOS == empleado_cia.APELLIDOS).ToList();

                    if (listado_empleados_cia_busqueda.Count > 0)
                    {
                        throw new ShowCaseException(String.Format("No se puede guardar un empleado nuevo pues ya existe otro con el mismo nombre y apellidos: [{0}] {1}.", listado_empleados_cia_busqueda[0].RFC_FM3, listado_empleados_cia_busqueda[0].NOMBRES + " " + listado_empleados_cia_busqueda[0].APELLIDOS), TypeMessage.error);
                    }

                    gestionGlModel.C_EMPLEADOS_CIA.Add(empleado_cia);

                    resultado = empleado_cia;
                }
                else
                {
                    //Modificar datos del personal
                    //Validar que no exista otro empleado con el mismo RFC ni con el mismo nombre.
                    listado_empleados_cia_busqueda = gestionGlModel.C_EMPLEADOS_CIA.Where(x => x.RFC_FM3 == empleado_cia.RFC_FM3 && x.ID_C_EMPLEADO_CIA != empleado_cia.ID_C_EMPLEADO_CIA).ToList();

                    if (listado_empleados_cia_busqueda.Count > 0)
                    {
                        throw new ShowCaseException(String.Format("No se puede guardar un empleado nuevo pues ya existe otro con el mismo RFC: [{0}] {1}.", listado_empleados_cia_busqueda[0].RFC_FM3, listado_empleados_cia_busqueda[0].NOMBRES + " " + listado_empleados_cia_busqueda[0].APELLIDOS), TypeMessage.error);
                    }

                    listado_empleados_cia_busqueda = gestionGlModel.C_EMPLEADOS_CIA.Where(x => x.NOMBRES == empleado_cia.NOMBRES && x.APELLIDOS == empleado_cia.APELLIDOS && x.ID_C_EMPLEADO_CIA != empleado_cia.ID_C_EMPLEADO_CIA).ToList();

                    if (listado_empleados_cia_busqueda.Count > 0)
                    {
                        throw new ShowCaseException(String.Format("No se puede guardar un empleado nuevo pues ya existe otro con el mismo nombre y apellidos: [{0}] {1}.", listado_empleados_cia_busqueda[0].RFC_FM3, listado_empleados_cia_busqueda[0].NOMBRES + " " + listado_empleados_cia_busqueda[0].APELLIDOS), TypeMessage.error);
                    }

                    GestionGL.Datos.C_EMPLEADOS_CIA empleado_cia_modificar = gestionGlModel.C_EMPLEADOS_CIA.SingleOrDefault(x => x.ID_C_EMPLEADO_CIA == empleado_cia.ID_C_EMPLEADO_CIA);

                    if (empleado_cia_modificar != null)
                    {
                        //empleado_cia_modificar.APELLIDOS = empleado_cia.APELLIDOS.ToUpper();
                        empleado_cia_modificar.CATEGORIA = empleado_cia.CATEGORIA.ToUpper();
                        empleado_cia_modificar.FOLIO_LIBRETA_MAR = empleado_cia.FOLIO_LIBRETA_MAR.ToUpper();
                        empleado_cia_modificar.NACIONALIDAD = empleado_cia.NACIONALIDAD.ToUpper();
                        empleado_cia_modificar.GENERO = empleado_cia.GENERO.ToUpper();
                        //empleado_cia_modificar.NOMBRES = empleado_cia.NOMBRES.ToUpper();
                        //empleado_cia_modificar.RFC_FM3 = empleado_cia.RFC_FM3.ToUpper();
                        empleado_cia_modificar.VIGENCIA_LIBRETA_MAR = empleado_cia.VIGENCIA_LIBRETA_MAR;
                    }

                    resultado = empleado_cia_modificar;
                }

                gestionGlModel.SaveChanges();
            }
            
            return resultado;
        }
    }
}
