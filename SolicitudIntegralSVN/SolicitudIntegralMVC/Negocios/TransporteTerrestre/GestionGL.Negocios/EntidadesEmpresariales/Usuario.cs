﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace TransporteTerrestre.GestionGL.Negocios.EntidadesEmpresariales
{
    public class Usuario
    {
        #region Atributos Privados // <<<<< -- INICIO de la Declaracion de los atributos privados de la clase --

        private int _IdDUsuario;
        private int _IdMUsuario;
        private int _IdRegion;
        private int _IdActivo;
        private int _IdSubdivision;
        private int _IdSubdivisionContratoCentro;

        private string _Ficha;
        private string _Nombre;
        private string _Region;
        private string _CveRegion;
        private string _Activo;
        private string _CveActivo;
        private string _Subdivision;
        private string _CveSubdivision;
        private System.Collections.Generic.List<string> _Permisos;

        #endregion// >>>>> -- FIN de la Declaracion de los atributos privados de la clase --

        #region Atributos Publicos // <<<<< -- INICIO de la Declaracion de los atributos publicos de la clase --

        public int IdDUsuario
        {
            get
            {
                return _IdDUsuario;
            }
            set
            {
                _IdDUsuario = value;
            }
        }
        public int IdMUsuario
        {
            get
            {
                return _IdMUsuario;
            }
            set
            {
                _IdMUsuario = value;
            }
        }
        public int IdRegion
        {
            get
            {
                return _IdRegion;
            }
            set
            {
                _IdRegion = value;
            }
        }
        public int IdActivo
        {
            get
            {
                return _IdActivo;
            }
            set
            {
                _IdActivo = value;
            }
        }
        public int IdSubdivision
        {
            get
            {
                return _IdSubdivision;
            }
            set
            {
                _IdSubdivision = value;
            }
        }
        public int IdSubdivisionContratoCentro
        {
            get
            {
                return _IdSubdivisionContratoCentro;
            }
            set
            {
                _IdSubdivisionContratoCentro = value;
            }
        }
        public string Ficha
        {
            get
            {
                return _Ficha;
            }
            set
            {
                _Ficha = value;
            }
        }
        public string Nombre
        {
            get
            {
                return _Nombre;
            }
            set
            {
                _Nombre = value;
            }
        }
        public System.Collections.Generic.List<string> Permisos
        {
            get
            {
                return _Permisos;
            }
            set
            {
                _Permisos = value;
            }
        }
        public string Region
        {
            get
            {
                return _Region;
            }
            set
            {
                _Region = value;
            }
        }
        public string CveRegion
        {
            get
            {
                return _CveRegion;
            }
            set
            {
                _CveRegion = value;
            }
        }
        public string Activo
        {
            get
            {
                return _Activo;
            }
            set
            {
                _Activo = value;
            }
        }
        public string CveActivo
        {
            get
            {
                return _CveActivo;
            }
            set
            {
                _CveActivo = value;
            }
        }
        public string Subdivision
        {
            get
            {
                return _Subdivision;
            }
            set
            {
                _Subdivision = value;
            }
        }
        public string CveSubdivision
        {
            get
            {
                return _CveSubdivision;
            }
            set
            {
                _CveSubdivision = value;
            }
        }

        #endregion // >>>>> -- FIN de la Declaraci?n de los atributos publicos de la clase --

        #region Constructores // <<<<< -- INICIO de los Constructores de la clase  --

        public Usuario()
        {
        }

        public Usuario(int idDUsuario, int idMUsuario, int idRegion, int idActivo, int idSubdivision, int idSubdivisionContratoCentro, string ficha, string nombre, string region, string cveRegion, string activo, string cveActivo, string subdivision, string cveSubdivision, System.Collections.Generic.List<string> permisos)
        {
            IdDUsuario = idDUsuario;
            IdMUsuario = idMUsuario;
            IdRegion = idRegion;
            IdActivo = idActivo;
            IdSubdivision = idSubdivision;
            IdSubdivisionContratoCentro = idSubdivision;
            Ficha = ficha;
            Nombre = nombre;
            Permisos = permisos;
            Region = region;
            CveRegion = cveRegion;
            Activo = activo;
            CveActivo = cveActivo;
            Subdivision = subdivision;
            CveSubdivision = cveSubdivision;
        }

        public Usuario(string ExpresionRegular)
        {
            try
            {
                Match regex_match = null;
                Regex regexObj = new Regex(@"(?<ID_D_USUARIO>\d+)\s?-\s?(?<ID_SUBDIVISION>\d+)", RegexOptions.Multiline);
                regex_match = regexObj.Match(ExpresionRegular);

                if (regex_match.Success)
                {
                    SubSonic.DataProvider pvdGestionGL = SubSonic.DataService.GetInstance("pvdGestionGL");
                    GestionGL.Datos.VwDUsuariosGeneralCollection DatosUsuario = new SubSonic.Select(pvdGestionGL).From<GestionGL.Datos.VwDUsuariosGeneral>().Where(GestionGL.Datos.VwDUsuariosGeneral.Columns.IdDUsuario).IsEqualTo(regex_match.Groups["ID_D_USUARIO"].Value).And(GestionGL.Datos.VwDUsuariosGeneral.Columns.IdCSubdivisionContratoCentroGestor).IsEqualTo(regex_match.Groups["ID_SUBDIVISION"].Value).ExecuteAsCollection<GestionGL.Datos.VwDUsuariosGeneralCollection>();
                    GestionGL.Datos.VwUsuariosPermisosGeneralCollection PermisosUsuario = new SubSonic.Select(pvdGestionGL).From<GestionGL.Datos.VwUsuariosPermisosGeneral>().Where(GestionGL.Datos.VwUsuariosPermisosGeneral.Columns.IdDUsuario).IsEqualTo(DatosUsuario[0].IdDUsuario).ExecuteAsCollection<GestionGL.Datos.VwUsuariosPermisosGeneralCollection>();
                    IdDUsuario = Convert.ToInt32(DatosUsuario[0].IdDUsuario);
                    IdMUsuario = Convert.ToInt32(DatosUsuario[0].IdMUsuario);
                    IdRegion = Convert.ToInt32(DatosUsuario[0].IdCRegion);
                    IdActivo = Convert.ToInt32(DatosUsuario[0].IdCActivo);
                    IdSubdivision = Convert.ToInt32(DatosUsuario[0].IdCSubdivision);
                    IdSubdivisionContratoCentro = Convert.ToInt32(DatosUsuario[0].IdCSubdivisionContratoCentroGestor);
                    Ficha = DatosUsuario[0].Ficha;
                    Nombre = DatosUsuario[0].Nombre;
                    Region = DatosUsuario[0].SiglasRegion;
                    CveRegion = DatosUsuario[0].CveRegion;
                    Activo = DatosUsuario[0].SiglasActivo;
                    CveActivo = DatosUsuario[0].CveActivo;
                    Subdivision = DatosUsuario[0].Subdivision;
                    CveSubdivision = DatosUsuario[0].CveSubdivision;
                    Permisos = new List<string>();
                    for (int i = 0; i < PermisosUsuario.Count; i++)
                    {
                        Permisos.Add(PermisosUsuario[i].CvePermisoCPermiso);
                    }
                }
                else
                {
                    throw new Exception("Ha ocurrido un error con la expresion regular");
                }

            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        #endregion // >>>>> -- FIN de los Constructores de la clase  --

        public static TransporteTerrestre.GestionGL.Datos.VwDUsuariosGeneralCollection ValidaUsuarios(string Ficha, string Pass)
        {
            try
            {
                string FichaFormateada = "";
                if (Ficha.Length < 8)
                {
                    FichaFormateada = String.Format("{0:00000000}", Convert.ToInt32(Ficha));
                }
                else
                {
                    FichaFormateada = Ficha;
                }
                Pass = Pass.ToUpper();
                SubSonic.DataProvider pvdGestionGL = SubSonic.DataService.GetInstance("pvdGestionGL");
                GestionGL.Datos.VwDUsuariosGeneralCollection Subdivisiones = new SubSonic.Select(pvdGestionGL).From<GestionGL.Datos.VwDUsuariosGeneral>().Where(GestionGL.Datos.VwDUsuariosGeneral.Columns.Ficha).IsEqualTo(FichaFormateada).And(GestionGL.Datos.VwDUsuariosGeneral.Columns.CveBajal).IsEqualTo("A").And(GestionGL.Datos.VwDUsuariosGeneral.Columns.CveAreaServicio).IsEqualTo("STA").ExecuteAsCollection<GestionGL.Datos.VwDUsuariosGeneralCollection>();
                if (Subdivisiones.Count > 0)
                {
                    for (int i = 0; i < Subdivisiones.Count; i++)
                    {
                        if (Subdivisiones[i].Pass != Pass)
                        {
                            Subdivisiones.RemoveAt(i);
                            i--;
                        }
                    }
                    if (Subdivisiones.Count <= 0)
                    {
                        throw new Exception("Contraseña incorrecta");
                    }
                }
                else
                {
                    throw new Exception("El usuario no se encuentra dado de alta o sus permisos expiraron");
                }
                return Subdivisiones;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

    }
}
