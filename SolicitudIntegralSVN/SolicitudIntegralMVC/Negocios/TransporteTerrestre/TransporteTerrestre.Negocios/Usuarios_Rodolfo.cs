﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace TransporteTerrestre.Negocios
{
    public partial class Usuarios
    {
        #region Variables privadas de la clase
        
        private string _ficha_rfc_usuario;
        private string _nombre_usuario;
        private int _id_m_usuario;
        private string _nodo_usuario;
        //Diccionario de Areas de Servicio, con listado de Id_d_Usuario
        private Dictionary<string, List<int>> _diccionario_id_d_usuario_area_servicio;
        private GestionGL.Datos.VwPermisosUaCollection _listado_permisos_ua;

        private TransporteTerrestre.Negocios.TipoPersonal _tipo_usuario;

        private GestionGL.Datos.VwCSubdivisionesContratosCentrosGestoresGeneralCollection _listado_subdivisiones_disponibles_usuario = null;

        private string _subdivision_actual;
        
        #endregion

        #region Propiedades públicas de la clase

        public int IdMUsuario { get { return _id_m_usuario; } }

        public string FichaRFCUsuario { get {return _ficha_rfc_usuario;} }

        public string NombreUsuario { get { return _nombre_usuario; } }

        public string NodoUsuario { get { return _nodo_usuario; } }

        public TransporteTerrestre.Negocios.TipoPersonal TipoPersonalUsuario { get { return _tipo_usuario; } }

        public GestionGL.Datos.VwCSubdivisionesContratosCentrosGestoresGeneralCollection ListadoSubdivisionesDisponibles { get { return _listado_subdivisiones_disponibles_usuario; } }

        public string ClaveSubdivisionActual
        {
            get { return _subdivision_actual; } 
            
            set 
            { 
                int total_subdivisiones_coinsidentes =
                    (from subdivision in _listado_subdivisiones_disponibles_usuario
                    where subdivision.Subdivision == value
                    select subdivision)
                    .Count<GestionGL.Datos.VwCSubdivisionesContratosCentrosGestoresGeneral>();

                if (total_subdivisiones_coinsidentes > 0)
                {
                    _subdivision_actual = value;
                    EstablecerListadoIdDUsuariosAreasServicio(_subdivision_actual);
                    EvaluarPermisosUsuario();
                }
                else
                {
                    throw new System.Exception("La subdivisión que desea asignar al usuario como actual no se encuentra dentro del listado de las subdivisiones disponibles para el usuario");
                }
            }
        }

        public Dictionary<string, List<int>> DiccionarioAreasServicioIdDUsuarios
        {
            get { return _diccionario_id_d_usuario_area_servicio; }
        }

        private void EstablecerListadoIdDUsuariosAreasServicio(string subdivision)
        {
            SubSonic.DataProvider pvdGestionGL = TransporteTerrestre.Negocios.Utilerias.ObtenerProveedorDatosGestionGl();

            GestionGL.Datos.VwDUsuariosGeneralCollection listado_d_usuarios_areas_servicios = new SubSonic.Select(pvdGestionGL, GestionGL.Datos.VwDUsuariosGeneral.Columns.IdMUsuario, GestionGL.Datos.VwDUsuariosGeneral.Columns.IdDUsuario, GestionGL.Datos.VwDUsuariosGeneral.Columns.CveAreaServicio,GestionGL.Datos.VwDUsuariosGeneral.Columns.Subdivision)
                                                                        .From<GestionGL.Datos.VwDUsuariosGeneral>()
                                                                        .Distinct()
                                                                        .Where(GestionGL.Datos.VwDUsuariosGeneral.Columns.IdMUsuario).IsEqualTo(_id_m_usuario)
                                                                        .And(GestionGL.Datos.VwDUsuariosGeneral.Columns.CveAreaServicio).IsEqualTo("STPVT")
                                                                        .And(GestionGL.Datos.VwDUsuariosGeneral.Columns.CveBajal).IsEqualTo("A")
                                                                        .And(GestionGL.Datos.VwDUsuariosGeneral.Columns.Subdivision).IsEqualTo(subdivision)
                                                                        .ExecuteAsCollection<GestionGL.Datos.VwDUsuariosGeneralCollection>();


            List<string> _areas_servicios_distintas = (from registro in listado_d_usuarios_areas_servicios
                                                       select registro.CveAreaServicio).Distinct<string>().ToList<string>();


            if (_diccionario_id_d_usuario_area_servicio != null)
            {
                _diccionario_id_d_usuario_area_servicio.Clear();
            }

            _diccionario_id_d_usuario_area_servicio = new Dictionary<string, List<int>>();

            List<int> listado_id_d_usuarios;

            foreach (string area_servicio in _areas_servicios_distintas)
            {
                listado_id_d_usuarios = (from registro in listado_d_usuarios_areas_servicios
                                         where registro.CveAreaServicio == area_servicio
                                         select registro.IdDUsuario).Distinct<int>().ToList<int>();

                if (listado_id_d_usuarios != null && listado_id_d_usuarios.Count > 0)
                {
                    _diccionario_id_d_usuario_area_servicio.Add(area_servicio, listado_id_d_usuarios);
                }
            }

            
            //Cheacando que permisos tiene el usuario
            listado_id_d_usuarios = (from registro in listado_d_usuarios_areas_servicios
                                     select registro.IdDUsuario).Distinct<int>().ToList<int>();

            if (listado_id_d_usuarios != null && listado_id_d_usuarios.Count > 0)
            {
                _listado_permisos_ua = new SubSonic.Select(pvdGestionGL).From<GestionGL.Datos.VwPermisosUa>()
                                       .Where(GestionGL.Datos.VwPermisosUa.Columns.IdDUsuario).In(listado_id_d_usuarios)
                                       .ExecuteAsCollection<GestionGL.Datos.VwPermisosUaCollection>();
            }
        }

        private void EvaluarPermisosUsuario()
        {
        }
        

        private String ReemplazarPermisosUAPorValoresBooleanos(Match match) 
        {
            string clave_permiso_encontrado = match.Groups["PERMISO"].Value;
            string resultado = null;

            int posicion_busqueda = _listado_permisos_ua.Find("CvePermiso", clave_permiso_encontrado);

            if (posicion_busqueda >= 0)
            {
                resultado = "true";
            }
            else
            {
                resultado = "false";
            }

            return resultado;
        }

        public string NombreSubdivisionActual
        {
            get 
            {
                string nombre_subdivision = (from subdivision in _listado_subdivisiones_disponibles_usuario
                                             where subdivision.Subdivision == _subdivision_actual
                                             select subdivision.DescripcionCtoGestor).SingleOrDefault<string>();
                return nombre_subdivision; 
            }
        }

        #endregion

        #region Constructores de la clase

        public Usuarios(int id_m_usuario, string ficha_rfc_usuario, string nombre_usuario, TransporteTerrestre.Negocios.TipoPersonal tipo_personal, GestionGL.Datos.VwCSubdivisionesContratosCentrosGestoresGeneralCollection listado_subdivisiones_usuario, string nodo_usuario)
        {
            _id_m_usuario = id_m_usuario;
            _ficha_rfc_usuario = ficha_rfc_usuario;
            _nombre_usuario = nombre_usuario;
            _tipo_usuario = tipo_personal;
            _listado_subdivisiones_disponibles_usuario = listado_subdivisiones_usuario;
            _nodo_usuario = nodo_usuario;
            _diccionario_id_d_usuario_area_servicio = new Dictionary<string, List<int>>();
            
            if (_listado_subdivisiones_disponibles_usuario.Count == 1)
            {
                ClaveSubdivisionActual = _listado_subdivisiones_disponibles_usuario[0].Subdivision;
            }
        }

        #endregion

        #region Funciones Públicas de la clase

        #endregion

        #region Funciones Privadas de la clase

        #endregion
    }
}
