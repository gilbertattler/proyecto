﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;

namespace TransporteTerrestre.Negocios
{
    public class Servicios
    {
        public static int Insertar(string CveServicio, string DescripcionServicio, string Horario, string Tipo, string Origen, string Destino, string CveOrigen, string CveDestino, bool Curso, string InicioHora, string FinHora, string Ficha, string IP, int dias_candado, string Nodo_Usuario)
        {
            try
            {
                TransporteTerrestre.Datos.CatServiciosCollection VerificaServicio = new SubSonic.Select()
                .From<TransporteTerrestre.Datos.CatServicios>()
                .Where(TransporteTerrestre.Datos.CatServicios.CveServicioColumn)
                .IsEqualTo(CveServicio)
                .And(TransporteTerrestre.Datos.CatServicios.BorradoLogicoColumn)
                .IsEqualTo(false)
                .And(TransporteTerrestre.Datos.CatServicios.NodoColumn)
                .IsEqualTo(Nodo_Usuario)
                .ExecuteAsCollection<TransporteTerrestre.Datos.CatServiciosCollection>();

                if (VerificaServicio.Count<TransporteTerrestre.Datos.CatServicios>() > 0)
                {
                    throw new Exception("Ya existe un servicio con la clave: " + CveServicio);
                }

                TransporteTerrestre.Datos.CatServicios Servicio = new TransporteTerrestre.Datos.CatServicios();
                Servicio.CveServicio = CveServicio;
                Servicio.DescripcionServicio = DescripcionServicio;
                Servicio.Horario = Horario;
                Servicio.Tipo = Tipo;
                Servicio.FkIdDestino = Convert.ToInt32(Destino);
                Servicio.FkIdOrigen = Convert.ToInt32(Origen);
                Servicio.CveOrigen = CveOrigen;
                Servicio.CveDestino = CveDestino;
                Servicio.Curso = Curso;
                Servicio.InicioHorarioAtencion = InicioHora;
                Servicio.FinHorarioAtencion = FinHora;
                Servicio.BorradoLogico = false;
                Servicio.DiasCandado = dias_candado;
                Servicio.Nodo = Nodo_Usuario;
                Servicio.Save();

                //Guarda Auditoria
                TransporteTerrestre.Datos.TabAuditorias Auditoria = new TransporteTerrestre.Datos.TabAuditorias();
                Auditoria.Fecha = System.DateTime.Now;
                Auditoria.Ip = IP;
                Auditoria.IdTabla = Servicio.IdServicio;
                Auditoria.Ficha = Ficha;
                Auditoria.TipoMovimiento = "Insertar";
                Auditoria.Descripcion = "Se inserto un nuevo servicio con el Id: " + Servicio.IdServicio;
                Auditoria.Nodo = Nodo_Usuario;
                Auditoria.Save();

                return Servicio.IdServicio;
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }
        }

        public static int Insertar(string CveServicio, string DescripcionServicio, string Horario, string Tipo, string Origen, string Destino, string CveOrigen, string CveDestino, bool Curso, string InicioHora, string FinHora, string Ficha, string IP, int dias_candado, bool Transaccionado, string Nodo_Usuario)
        {
            try
            {
                int ValorRetorno;
                if (Transaccionado)
                {
                    using (TransactionScope Scope = new TransactionScope())
                    {
                        using (SubSonic.SharedDbConnectionScope sharedConnectionScope = new SubSonic.SharedDbConnectionScope())
                        {
                            ValorRetorno = Insertar(CveServicio, DescripcionServicio, Horario, Tipo, Origen, Destino, CveOrigen, CveDestino, Curso, InicioHora, FinHora, Ficha, IP, dias_candado,Nodo_Usuario);
                            Scope.Complete();
                        }
                    }
                }
                else
                {
                    ValorRetorno = Insertar(CveServicio, DescripcionServicio, Horario, Tipo, Origen, Destino, CveOrigen, CveDestino, Curso, InicioHora, FinHora, Ficha, IP, dias_candado,Nodo_Usuario);
                }
                return ValorRetorno;
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }
        }

        public static void Modificar(string IdServicio, string CveServicio, string DescripcionServicio, string Horario, string Tipo, string Origen, string Destino, string CveOrigen, string CveDestino, bool Curso, string InicioHora, string FinHora, string Ficha, string IP, int dias_candado, string Nodo_Usuario)
        {
            if (CveServicio == "")
            {
                throw new System.Exception("Debe proporcionar la clave del servicio.");
            }
            if (DescripcionServicio == "")
            {
                throw new System.Exception("Debe proporcionar la descripción del servicio.");
            }
            if (Horario == "")
            {
                throw new System.Exception("Debe proporcionar el horario del servicio.");
            }
            if (Horario == "")
            {
                throw new System.Exception("Debe proporcionar el horario del servicio.");
            }
            if (InicioHora == "")
            {
                throw new System.Exception("Debe proporcionar el inicio de atención del servicio.");
            }
            if (FinHora == "")
            {
                throw new System.Exception("Debe proporcionar el fin de atención del servicio.");
            }
            //if (dias_candado == null)
            //{
            //    throw new System.Exception("Debe proporcionar los días del servicio.");
            //}
            try
            {
                TransporteTerrestre.Datos.CatServiciosCollection VerificaServicios = new SubSonic.Select()
                .From<TransporteTerrestre.Datos.CatServicios>()
                .Where(TransporteTerrestre.Datos.CatServicios.BorradoLogicoColumn)
                .IsEqualTo(false)
                .And(TransporteTerrestre.Datos.CatServicios.CveServicioColumn)
                .IsEqualTo(CveServicio)
                .And(TransporteTerrestre.Datos.CatServicios.NodoColumn)
                .IsEqualTo(Nodo_Usuario)
                .ExecuteAsCollection<TransporteTerrestre.Datos.CatServiciosCollection>();
                if (VerificaServicios.Count > 0)
                {
                    bool Mismo = false;
                    for (int i = 0; i < VerificaServicios.Count; i++)
                    {
                        if (VerificaServicios[i].IdServicio.ToString() == IdServicio)
                        {
                            Mismo = true;
                            break;
                        }
                    }
                    if (!Mismo)
                    {
                        throw new Exception("Ya existe un servicio con la clave: " + CveServicio);
                    }
                }
                TransporteTerrestre.Datos.CatServicios NuevoServicio = new TransporteTerrestre.Datos.CatServicios();
                NuevoServicio.IsNew = false;
                NuevoServicio.IsLoaded = true;
                NuevoServicio.IdServicio = Convert.ToInt32(IdServicio);
                NuevoServicio.CveServicio = CveServicio;
                NuevoServicio.DescripcionServicio = DescripcionServicio;
                NuevoServicio.Horario = Horario;
                NuevoServicio.Tipo = Tipo; 
                NuevoServicio.CveOrigen = CveOrigen; 
                NuevoServicio.CveDestino = CveDestino; 
                NuevoServicio.FkIdDestino = Convert.ToInt32(Destino); 
                NuevoServicio.FkIdOrigen = Convert.ToInt32(Origen); 
                NuevoServicio.Curso = Curso;
                NuevoServicio.InicioHorarioAtencion = InicioHora;
                NuevoServicio.FinHorarioAtencion = FinHora;
                NuevoServicio.DiasCandado = dias_candado;
                NuevoServicio.BorradoLogico = false;
                NuevoServicio.Nodo = Nodo_Usuario;
                NuevoServicio.Save();

                //Guarda Auditoria
                TransporteTerrestre.Datos.TabAuditorias Auditoria = new TransporteTerrestre.Datos.TabAuditorias();
                Auditoria.Fecha = System.DateTime.Now;
                Auditoria.Ip = IP;
                Auditoria.IdTabla = NuevoServicio.IdServicio;
                Auditoria.Ficha = Ficha;
                Auditoria.TipoMovimiento = "Modificar";
                Auditoria.Descripcion = "Se modifico el servicio con el Id: " + NuevoServicio.IdServicio;
                Auditoria.Nodo = Nodo_Usuario;
                Auditoria.Save();

            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static void Modificar(string IdServicio, string CveServicio, string DescripcionServicio, string Horario, string Tipo, string Origen, string Destino, string CveOrigen, string CveDestino, bool Curso, string InicioHora, string FinHora, string Ficha, string IP, int dias_candado, bool Transaccionado, string Nodo_Usuario)
        {
            try
            {
                if (Transaccionado)
                {
                    using (TransactionScope Scope = new TransactionScope())
                    {
                        using (SubSonic.SharedDbConnectionScope sharedConnectionScope = new SubSonic.SharedDbConnectionScope())
                        {
                            Modificar(IdServicio, CveServicio, DescripcionServicio, Horario, Tipo, Origen, Destino, CveOrigen, CveDestino, Curso,InicioHora,FinHora,Ficha,IP, dias_candado, Nodo_Usuario);
                            Scope.Complete();
                        }
                    }
                }
                else
                {
                    Modificar(IdServicio, CveServicio, DescripcionServicio, Horario, Tipo, Origen, Destino, CveOrigen, CveDestino, Curso, InicioHora, FinHora, Ficha, IP, dias_candado, Nodo_Usuario);
                }
             
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }
        }

        public static void InsertarGuardar(string IdServicio, string CveServicio, string DescripcionServicio, string Horario, string Tipo, string Origen, string Destino, string CveOrigen, string CveDestino, bool Curso, string InicioHora, string FinHora, string Ficha, string IP, int dias_candado, string Nodo_Usuario)
        {
            try
            {
                if (IdServicio == "Nuevo")
                {
                    Insertar(CveServicio, DescripcionServicio, Horario, Tipo, Origen, Destino, CveOrigen, CveDestino, Curso, InicioHora, FinHora, Ficha, IP, dias_candado,Nodo_Usuario);
                }
                else
                {
                    Modificar(IdServicio, CveServicio, DescripcionServicio, Horario, Tipo, Origen, Destino, CveOrigen, CveDestino, Curso, InicioHora, FinHora, Ficha, IP, dias_candado,Nodo_Usuario);
                }
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }

        }

        public static void InsertarGuardar(string IdServicio, string CveServicio, string DescripcionServicio, string Horario, string Tipo, string Origen, string Destino, string CveOrigen, string CveDestino, bool Curso, string InicioHora, string FinHora, string Ficha, string IP, bool Transaccionado, int dias_candado, string Nodo_Usuario)
        {
            try
            {
                if (Transaccionado)
                {
                    using (TransactionScope Scope = new TransactionScope())
                    {
                        using (SubSonic.SharedDbConnectionScope sharedConnectionScope = new SubSonic.SharedDbConnectionScope())
                        {
                            InsertarGuardar(IdServicio, CveServicio, DescripcionServicio, Horario, Tipo, Origen, Destino, CveOrigen, CveDestino, Curso, InicioHora, FinHora, Ficha, IP, dias_candado,Nodo_Usuario);
                            Scope.Complete();
                        }
                    }
                }
                else
                {
                    InsertarGuardar(IdServicio, CveServicio, DescripcionServicio, Horario, Tipo, Origen, Destino, CveOrigen, CveDestino, Curso, InicioHora, FinHora, Ficha, IP, dias_candado,Nodo_Usuario);
                }
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }
        }

        public static void Eliminar(int IdServicio, string Ficha, string IP, string Nodo_Usuario)
        {
            try
            {
                TransporteTerrestre.Datos.CatServicios Servicio = TransporteTerrestre.Datos.CatServicios.FetchByID(IdServicio);
                Servicio.BorradoLogico = true;
                Servicio.Save();

                //Guarda Auditoria
                TransporteTerrestre.Datos.TabAuditorias Auditoria = new TransporteTerrestre.Datos.TabAuditorias();
                Auditoria.Fecha = System.DateTime.Now;
                Auditoria.Ip = IP;
                Auditoria.IdTabla = Servicio.IdServicio;
                Auditoria.Ficha = Ficha;
                Auditoria.TipoMovimiento = "Eliminar";
                Auditoria.Descripcion = "Se elimino el servicio con el Id: " + Servicio.IdServicio;
                Auditoria.Nodo = Nodo_Usuario;
                Auditoria.Save();

            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }
        }

        public static void Eliminar(int IdServicio, string Ficha, string IP, bool Transaccionado, string Nodo_Usuario)
        {
            try
            {
                if (Transaccionado)
                {
                    using (TransactionScope Scope = new TransactionScope())
                    {
                        using (SubSonic.SharedDbConnectionScope sharedConnectionScope = new SubSonic.SharedDbConnectionScope())
                        {
                            Eliminar(IdServicio,Ficha,IP,Nodo_Usuario);
                            Scope.Complete();
                        }
                    }
                }
                else
                {
                    Eliminar(IdServicio,Ficha,IP,Nodo_Usuario);
                }
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }
        }

        /*public static void InsertarServicioPuntos(string CveServicio, string DescripcionServicio, string Horario, string Tipo, List<KeyValuePair<string,string>> DestinoOrden)
        {
            try
            {
                int IdServicio = Insertar(CveServicio, DescripcionServicio, Horario, Tipo, true);
                TransporteTerrestre.Negocios.RelDestinosServicio.Insertar(IdServicio.ToString(), DestinoOrden);                
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }
        }

        public static void InsertarServicioPuntos(string CveServicio, string DescripcionServicio, string Horario, string Tipo, List<KeyValuePair<string, string>> DestinoOrden, bool Transaccionado)
        {
            try
            {
                int ValorRetorno;
                if (Transaccionado)
                {
                    using (TransactionScope Scope = new TransactionScope())
                    {
                        using (SubSonic.SharedDbConnectionScope sharedConnectionScope = new SubSonic.SharedDbConnectionScope())
                        {
                            InsertarServicioPuntos(CveServicio, DescripcionServicio, Horario, Tipo,DestinoOrden);
                            Scope.Complete();
                        }
                    }
                }
                else
                {
                    InsertarServicioPuntos(CveServicio, DescripcionServicio, Horario, Tipo, DestinoOrden);
                }
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }
        }*/

    }
}