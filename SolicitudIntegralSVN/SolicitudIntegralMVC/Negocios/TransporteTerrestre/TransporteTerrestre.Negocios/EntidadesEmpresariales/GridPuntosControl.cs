﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;

namespace TransporteTerrestre.Negocios.EntidadesEmpresariales
{
    public class GridPuntosControl
    {
        public static TransporteTerrestre.Datos.TabPuntosControlCollection OrdenaPuntosAsc(TransporteTerrestre.Datos.TabPuntosControlCollection ListaPuntosControl)
        {
            try
            {
                TransporteTerrestre.Datos.TabPuntosControlCollection ListaPuntosControlOrdenada = new TransporteTerrestre.Datos.TabPuntosControlCollection();
                TransporteTerrestre.Datos.TabPuntosControl NuevoPunto = new TransporteTerrestre.Datos.TabPuntosControl();
                int IdPuntoAnterior = -1;
                for(int i = 0; i<ListaPuntosControl.Count; i++)
                {
                    if (ListaPuntosControl[i].IdPuntoControlAnterior==IdPuntoAnterior)
                    {
                        ListaPuntosControlOrdenada.Add(ListaPuntosControl[i].Clone());
                        IdPuntoAnterior = ListaPuntosControl[i].IdPuntoControl;
                        if (ListaPuntosControl[i].IdPuntoControlSiguiente == -1)
                        {
                            break;
                        }
                        else
                        {
                            i = -1;
                        }
                    }
                }
                return ListaPuntosControlOrdenada;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static TransporteTerrestre.Datos.TabPuntosControlCollection RetornaGridEditable(TransporteTerrestre.Datos.TabPuntosControlCollection ListaPuntosControl)
        {
            try
            {
                TransporteTerrestre.Datos.TabPuntosControl PuntoControlEditable = new TransporteTerrestre.Datos.TabPuntosControl();
                PuntoControlEditable.IdPuntoControlSiguiente = -2;
                PuntoControlEditable.IdPuntoControlAnterior = -2;
                ListaPuntosControl.Add(PuntoControlEditable);
                return ListaPuntosControl;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static TransporteTerrestre.Datos.TabPuntosControlCollection AgregarRow(TransporteTerrestre.Datos.TabPuntosControlCollection GridActual, int Index, string Direccion)
        {
            TransporteTerrestre.Datos.TabPuntosControlCollection NuevosPuntos = new TransporteTerrestre.Datos.TabPuntosControlCollection();
            NuevosPuntos = GridActual.Clone();
            //NuevoGrid = GridActual;
            TransporteTerrestre.Datos.TabPuntosControl NuevoPunto= new TransporteTerrestre.Datos.TabPuntosControl();
            NuevoPunto.IdPuntoControlAnterior = -2;
            NuevoPunto.IdPuntoControlSiguiente = -2;
            if (Direccion == "Arriba")
            {
                NuevosPuntos.Insert(Index, NuevoPunto);
            }
            else
            {
                NuevosPuntos.Insert(Index + 1, NuevoPunto);
            }
            return NuevosPuntos;
        }

        public static TransporteTerrestre.Datos.TabPuntosControlCollection InsertarPunto(TransporteTerrestre.Datos.TabPuntosControlCollection ListaPuntosControl, int Index, string Ficha, string IP, string Nodo_Usuario)
        {
            try
            {
                TransporteTerrestre.Datos.TabPuntosControlCollection PuntosGuardar = new TransporteTerrestre.Datos.TabPuntosControlCollection();
                if (ListaPuntosControl[Index].IdPuntoControlSiguiente == -2 && ListaPuntosControl[Index].IdPuntoControlAnterior == -2)
                {
                    if (Index == 0)
                    {
                        ListaPuntosControl[Index].IdPuntoControlAnterior = -1;
                        TransporteTerrestre.Datos.TabViajes Viaje = TransporteTerrestre.Datos.TabViajes.FetchByID(ListaPuntosControl[Index].FkIdViaje);
                        Viaje.Status = "PROGRESO";
                        Viaje.Save();
                    }
                    else
                    {
                        ListaPuntosControl[Index].IdPuntoControlAnterior = ListaPuntosControl[Index - 1].IdPuntoControl;
                    }
                    if (Index == ListaPuntosControl.Count - 1)
                    {
                        ListaPuntosControl[Index].IdPuntoControlSiguiente = -1;
                    }
                    else
                    {
                        ListaPuntosControl[Index].IdPuntoControlSiguiente = ListaPuntosControl[Index + 1].IdPuntoControl;
                    }
                    TransporteTerrestre.Datos.TabPuntosControl PuntoNuevo = ListaPuntosControl[Index].Clone();
                    PuntoNuevo.Nodo = Nodo_Usuario;
                    PuntoNuevo.Save();
                    if (Index == 0)
                    {
                    }
                    else
                    {
                        ListaPuntosControl[Index - 1].IsNew = false;
                        ListaPuntosControl[Index - 1].IsLoaded = true;
                        ListaPuntosControl[Index - 1].IdPuntoControlSiguiente = PuntoNuevo.IdPuntoControl;
                        ListaPuntosControl[Index - 1].Nodo = Nodo_Usuario;
                        ListaPuntosControl[Index - 1].Save();
                    }
                    if (Index == ListaPuntosControl.Count - 1)
                    {

                    }
                    else
                    {
                        ListaPuntosControl[Index + 1].IsNew = false;
                        ListaPuntosControl[Index + 1].IsLoaded = true;
                        ListaPuntosControl[Index + 1].IdPuntoControlAnterior = PuntoNuevo.IdPuntoControl;
                        ListaPuntosControl[Index + 1].Nodo = Nodo_Usuario;
                        ListaPuntosControl[Index + 1].Save();
                    }
                }
                PuntosGuardar = new SubSonic.Select()
                    .From<TransporteTerrestre.Datos.TabPuntosControl>()
                    .Where(TransporteTerrestre.Datos.TabPuntosControl.FkIdViajeColumn)
                    .IsEqualTo(ListaPuntosControl[Index].FkIdViaje)
                    .And(TransporteTerrestre.Datos.TabPuntosControl.BorradoLogicoColumn)
                    .IsEqualTo(false)
                    .And(TransporteTerrestre.Datos.TabPuntosControl.NodoColumn)
                    .IsEqualTo(Nodo_Usuario)
                    .ExecuteAsCollection<TransporteTerrestre.Datos.TabPuntosControlCollection>();

                PuntosGuardar = OrdenaPuntosAsc(PuntosGuardar);

                //Guarda Auditoria
                TransporteTerrestre.Datos.TabAuditorias Auditoria = new TransporteTerrestre.Datos.TabAuditorias();
                Auditoria.Fecha = System.DateTime.Now;
                Auditoria.Ip = IP;
                //Auditoria.IdTabla = ListaPuntosControl[Index].IdPuntoControl;
                Auditoria.IdTabla = PuntosGuardar[Index].IdPuntoControl;
                Auditoria.Ficha = Ficha;
                Auditoria.TipoMovimiento = "Insertar";
                //Auditoria.Descripcion = "Se inserto el punto con el Id: " + ListaPuntosControl[Index].IdPuntoControl;
                Auditoria.Descripcion = "Se inserto el punto con el Id: " + PuntosGuardar[Index].IdPuntoControl;
                Auditoria.Nodo = Nodo_Usuario;
                Auditoria.Save();

                return PuntosGuardar;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static TransporteTerrestre.Datos.TabPuntosControlCollection InsertarPunto(TransporteTerrestre.Datos.TabPuntosControlCollection ListaPuntosControl, int Index, string Ficha, string IP, bool Transaccionado, string Nodo_Usuario)
        {
            try
            {
                TransporteTerrestre.Datos.TabPuntosControlCollection ValorRetorno = new TransporteTerrestre.Datos.TabPuntosControlCollection();
                if (Transaccionado)
                {
                    using (TransactionScope Scope = new TransactionScope())
                    {
                        using (SubSonic.SharedDbConnectionScope sharedConnectionScope = new SubSonic.SharedDbConnectionScope())
                        {
                            ValorRetorno = InsertarPunto(ListaPuntosControl, Index,Ficha,IP,Nodo_Usuario);
                            Scope.Complete();
                        }
                    }
                }
                else
                {
                    ValorRetorno = InsertarPunto(ListaPuntosControl, Index,Ficha,IP,Nodo_Usuario);
                }
                return ValorRetorno;
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }
        }

        public static TransporteTerrestre.Datos.TabPuntosControlCollection GuardarPunto(TransporteTerrestre.Datos.TabPuntosControlCollection ListaPuntosControl, int Index, string Ficha, string IP, string Nodo_Usuario)
        {
            try
            {
                TransporteTerrestre.Datos.TabPuntosControl PuntoGuardar = new TransporteTerrestre.Datos.TabPuntosControl();
                PuntoGuardar.IsNew = false;
                PuntoGuardar.IsLoaded = true;
                PuntoGuardar.IdPuntoControl = ListaPuntosControl[Index].IdPuntoControl;
                PuntoGuardar.IdPuntoControlAnterior = ListaPuntosControl[Index].IdPuntoControlAnterior;
                PuntoGuardar.IdPuntoControlSiguiente = ListaPuntosControl[Index].IdPuntoControlSiguiente;
                PuntoGuardar.FkIdViaje = ListaPuntosControl[Index].FkIdViaje;
                PuntoGuardar.FechaHoraSalida = ListaPuntosControl[Index].FechaHoraSalida;
                PuntoGuardar.FechaHoraArribo = ListaPuntosControl[Index].FechaHoraArribo;
                PuntoGuardar.NombrePuntoControl = ListaPuntosControl[Index].NombrePuntoControl;
                PuntoGuardar.Nodo = Nodo_Usuario;
                PuntoGuardar.Save();

                ListaPuntosControl = new SubSonic.Select().
                    From<TransporteTerrestre.Datos.TabPuntosControl>()
                    .Where(TransporteTerrestre.Datos.TabPuntosControl.BorradoLogicoColumn)
                    .IsEqualTo(false)
                    .And(TransporteTerrestre.Datos.TabPuntosControl.FkIdViajeColumn)
                    .IsEqualTo(PuntoGuardar.FkIdViaje)
                    .And(TransporteTerrestre.Datos.TabPuntosControl.NodoColumn)
                    .IsEqualTo(Nodo_Usuario)
                    .ExecuteAsCollection<TransporteTerrestre.Datos.TabPuntosControlCollection>();

                //Guarda Auditoria
                TransporteTerrestre.Datos.TabAuditorias Auditoria = new TransporteTerrestre.Datos.TabAuditorias();
                Auditoria.Fecha = System.DateTime.Now;
                Auditoria.Ip = IP;
                Auditoria.IdTabla = PuntoGuardar.IdPuntoControl;
                Auditoria.Ficha = Ficha;
                Auditoria.TipoMovimiento = "Modificar";
                Auditoria.Descripcion = "Se modifico el punto con el Id: " + PuntoGuardar.IdPuntoControl;
                Auditoria.Nodo = Nodo_Usuario;
                Auditoria.Save();

                return OrdenaPuntosAsc(ListaPuntosControl);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static TransporteTerrestre.Datos.TabPuntosControlCollection GuardarPunto(TransporteTerrestre.Datos.TabPuntosControlCollection ListaPuntosControl, int Index, string Ficha, string IP, bool Transaccionado, string Nodo_Usuario)
        {
            try
            {
                TransporteTerrestre.Datos.TabPuntosControlCollection ValorRetorno = new TransporteTerrestre.Datos.TabPuntosControlCollection();
                if (Transaccionado)
                {
                    using (TransactionScope Scope = new TransactionScope())
                    {
                        using (SubSonic.SharedDbConnectionScope sharedConnectionScope = new SubSonic.SharedDbConnectionScope())
                        {
                            ValorRetorno = GuardarPunto(ListaPuntosControl, Index, Ficha, IP, Nodo_Usuario);
                            Scope.Complete();
                        }
                    }
                }
                else
                {
                    ValorRetorno = GuardarPunto(ListaPuntosControl, Index, Ficha, IP, Nodo_Usuario);
                }
                return ValorRetorno;
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }
        }

        public static TransporteTerrestre.Datos.TabPuntosControlCollection EliminaPunto(TransporteTerrestre.Datos.TabPuntosControlCollection ListaPuntosControl, int Index, string Ficha, string IP, string Nodo_Usuario)
        {
            try
            {
                TransporteTerrestre.Datos.TabPuntosControlCollection PuntosGuardar = new TransporteTerrestre.Datos.TabPuntosControlCollection();

                ListaPuntosControl[Index].IsNew = false;
                ListaPuntosControl[Index].IsLoaded = true;
                    if (Index == 0)
                    {
                        if (ListaPuntosControl.Count > 1)
                        {
                            ListaPuntosControl[Index + 1].IsNew = false;
                            ListaPuntosControl[Index + 1].IsLoaded = true;
                            ListaPuntosControl[Index + 1].IdPuntoControlAnterior = -1;
                        }                        
                    }
                    else 
                    {
                        if (Index == ListaPuntosControl.Count - 1)
                        {
                            ListaPuntosControl[Index - 1].IsNew = false;
                            ListaPuntosControl[Index - 1].IsLoaded = true;
                            ListaPuntosControl[Index - 1].IdPuntoControlSiguiente = -1;
                        }     
                        else
                        {
                            ListaPuntosControl[Index - 1].IsNew = false;
                            ListaPuntosControl[Index - 1].IsLoaded = true;
                            ListaPuntosControl[Index + 1].IsNew = false;
                            ListaPuntosControl[Index + 1].IsLoaded = true;
                            ListaPuntosControl[Index + 1].IdPuntoControlAnterior = ListaPuntosControl[Index].IdPuntoControlAnterior;
                            ListaPuntosControl[Index - 1].IdPuntoControlSiguiente = ListaPuntosControl[Index].IdPuntoControlSiguiente;
                        }
                    }
                    ListaPuntosControl[Index].BorradoLogico = true; ;
                    ListaPuntosControl[Index].Save();
                    if (Index == 0)
                    {
                    }
                    else
                    {
                        //ListaPuntosControl[Index - 1].IsNew = false;
                        //ListaPuntosControl[Index - 1].IsLoaded = true;
                        //ListaPuntosControl[Index - 1].IdPuntoControlSiguiente = PuntoNuevo.IdPuntoControl;
                        ListaPuntosControl[Index - 1].Save();
                    }
                    if (Index == ListaPuntosControl.Count - 1)
                    {

                    }
                    else
                    {
                        //ListaPuntosControl[Index + 1].IsNew = false;
                        //ListaPuntosControl[Index + 1].IsLoaded = true;
                        //ListaPuntosControl[Index + 1].IdPuntoControlAnterior = PuntoNuevo.IdPuntoControl;
                        ListaPuntosControl[Index + 1].Save();
                    }
                PuntosGuardar = new SubSonic.Select()
                    .From<TransporteTerrestre.Datos.TabPuntosControl>()
                    .Where(TransporteTerrestre.Datos.TabPuntosControl.FkIdViajeColumn)
                    .IsEqualTo(ListaPuntosControl[Index].FkIdViaje)
                    .And(TransporteTerrestre.Datos.TabPuntosControl.BorradoLogicoColumn)
                    .IsEqualTo(false)
                    .And(TransporteTerrestre.Datos.TabPuntosControl.NodoColumn)
                    .IsEqualTo(Nodo_Usuario)
                    .ExecuteAsCollection<TransporteTerrestre.Datos.TabPuntosControlCollection>();

                PuntosGuardar = OrdenaPuntosAsc(PuntosGuardar);

                //Guarda Auditoria
                TransporteTerrestre.Datos.TabAuditorias Auditoria = new TransporteTerrestre.Datos.TabAuditorias();
                Auditoria.Fecha = System.DateTime.Now;
                Auditoria.Ip = IP;
                //Auditoria.IdTabla = ListaPuntosControl[Index].IdPuntoControl;
                Auditoria.IdTabla = PuntosGuardar[0].IdPuntoControl;
                Auditoria.Ficha = Ficha;
                Auditoria.TipoMovimiento = "Eliminar";
                //Auditoria.Descripcion = "Se elimino el punto con el Id: " + ListaPuntosControl[Index].IdPuntoControl;
                Auditoria.Descripcion = "Se elimino el punto con el Id: " + PuntosGuardar[0].IdPuntoControl;
                Auditoria.Nodo = Nodo_Usuario;
                Auditoria.Save();

                return PuntosGuardar;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static TransporteTerrestre.Datos.TabPuntosControlCollection EliminaPunto(TransporteTerrestre.Datos.TabPuntosControlCollection ListaPuntosControl, int Index, string Ficha, string IP, bool Transaccionado, string Nodo_Usuario)
        {
            try
            {
                TransporteTerrestre.Datos.TabPuntosControlCollection ValorRetorno = new TransporteTerrestre.Datos.TabPuntosControlCollection();
                if (Transaccionado)
                {
                    using (TransactionScope Scope = new TransactionScope())
                    {
                        using (SubSonic.SharedDbConnectionScope sharedConnectionScope = new SubSonic.SharedDbConnectionScope())
                        {
                            ValorRetorno = EliminaPunto(ListaPuntosControl, Index,Ficha,IP,Nodo_Usuario);
                            Scope.Complete();
                        }
                    }
                }
                else
                {
                    ValorRetorno = EliminaPunto(ListaPuntosControl, Index,Ficha,IP, Nodo_Usuario);
                }
                return ValorRetorno;
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }
        }
    }
}
