﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransporteTerrestre.Negocios.EntidadesEmpresariales
{
    public class GridServiciosCollection
    {
        private System.Collections.Generic.List<GridServicios> _ColeccionGridServicios;

        public System.Collections.Generic.List<GridServicios> ColeccionGridServicios
        {
            get
            {
                return _ColeccionGridServicios;
            }
        }

        public GridServiciosCollection()
        {
            _ColeccionGridServicios = new List<GridServicios>();
        }

        public void Agregar(int idServicio, string cveServicio, string horario, string tipo, bool curso)
        {
            try
            {
                TransporteTerrestre.Negocios.EntidadesEmpresariales.GridServicios Servicio = new GridServicios(idServicio, cveServicio, horario, tipo, curso, _ColeccionGridServicios.Count + 1);
                _ColeccionGridServicios.Add(Servicio);
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }

        }

        public void Subir(int Orden)
        {
            try
            {
                if (Orden > 1)
                {
                    _ColeccionGridServicios[Orden - 1].Orden = Orden - 1;
                    _ColeccionGridServicios[Orden - 2].Orden = Orden;

                    var ColeccionOrdenada = from line in _ColeccionGridServicios
                                            orderby line.Orden ascending
                                            select line;
                    _ColeccionGridServicios = ColeccionOrdenada.ToList<TransporteTerrestre.Negocios.EntidadesEmpresariales.GridServicios>();
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public void Bajar(int Orden)
        {
            try
            {
                if (Orden < _ColeccionGridServicios.Count)
                {
                    _ColeccionGridServicios[Orden - 1].Orden = Orden + 1;
                    _ColeccionGridServicios[Orden].Orden = Orden;

                    var ColeccionOrdenada = from line in _ColeccionGridServicios
                                            orderby line.Orden ascending
                                            select line;
                    _ColeccionGridServicios = ColeccionOrdenada.ToList<TransporteTerrestre.Negocios.EntidadesEmpresariales.GridServicios>();
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public void Eliminar(int Orden)
        {
            try
            {
                _ColeccionGridServicios.RemoveAt(Orden-1);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
    }
}
