﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransporteTerrestre.Negocios.EntidadesEmpresariales
{
    public class ServicioOrigenDestino
    {
        #region Atributos Privados
        private int _IdServicio;
        private int _FkIdOrigen;
        private int _FkIdDestino;
        private string _CveServicio;
        private string _DescripcionServicio;
        private string _Horario;
        private string _Tipo;
        private string _NombreOrigen;
        private string _NombreDestino;
        private bool _Curso;
        private bool _BorradoLogico;
        private string _Nodo_Usuario;

        #endregion

        #region Atributos Publicos

        public int IdServicio
        {
            get
            {
                return _IdServicio;
            }
            set
            {
                _IdServicio = value;
            }
        }

        public int FkIdOrigen
        {
            get
            {
                return _FkIdOrigen;
            }
            set
            {
                _FkIdOrigen = value;
            }
        }

        public int FkIdDestino
        {
            get
            {
                return _FkIdDestino;
            }
            set
            {
                _FkIdDestino = value;
            }
        }

        public string CveServicio
        {
            get
            {
                return _CveServicio;
            }
            set
            {
                _CveServicio = value;
            }
        }

        public string DescripcionServicio
        {
            get
            {
                return _DescripcionServicio;
            }
            set
            {
                _DescripcionServicio = value;
            }
        }

        public string Horario
        {
            get
            {
                return _Horario;
            }
            set
            {
                _Horario = value;
            }
        }

        public string Tipo
        {
            get
            {
                return _Tipo;
            }
            set
            {
                _Tipo = value;
            }
        }

        public string NombreOrigen
        {
            get
            {
                return _NombreOrigen;
            }
            set
            {
                _NombreOrigen = value;
            }
        }

        public string NombreDestino
        {
            get
            {
                return _NombreDestino;
            }
            set
            {
                _NombreDestino = value;
            }
        }

        public bool Curso
        {
            get
            {
                return _Curso;
            }
            set
            {
                _Curso = value;
            }
        }

        public bool BorradoLogico
        {
            get
            {
                return _BorradoLogico;
            }
            set
            {
                _BorradoLogico = value;
            }
        }
        public string NodoUsuario
        {
            get
            {
                return _Nodo_Usuario;
            }
            set
            {
                _Nodo_Usuario = value;
            }
        }

        #endregion

        #region Constructores

        public ServicioOrigenDestino()
        {

        }

        public ServicioOrigenDestino(int idServicio, int fkIdOrigen, int fkIdDestino, string cveServicio, string descripcionServicio, string horario, string tipo, bool curso, bool borradoLogico)
        {
            try
            {
                SubSonic.DataProvider pvdGestionGL = SubSonic.DataService.GetInstance("pvdGestionGL");
                GestionGL.Datos.VwInstalacionesGeneralCollection InstalacionOrigen = new SubSonic.Select(pvdGestionGL).From<GestionGL.Datos.VwInstalacionesGeneral>().Where(GestionGL.Datos.VwInstalacionesGeneral.Columns.BorradoLogicoInstalacion).IsEqualTo(false).And(GestionGL.Datos.VwInstalacionesGeneral.Columns.CveAreaServicio).IsEqualTo("AH").And(GestionGL.Datos.VwInstalacionesGeneral.Columns.IdCMInstalacion).IsEqualTo(fkIdOrigen).ExecuteAsCollection<GestionGL.Datos.VwInstalacionesGeneralCollection>();
                GestionGL.Datos.VwInstalacionesGeneralCollection InstalacionDestino = new SubSonic.Select(pvdGestionGL).From<GestionGL.Datos.VwInstalacionesGeneral>().Where(GestionGL.Datos.VwInstalacionesGeneral.Columns.BorradoLogicoInstalacion).IsEqualTo(false).And(GestionGL.Datos.VwInstalacionesGeneral.Columns.CveAreaServicio).IsEqualTo("AH").And(GestionGL.Datos.VwInstalacionesGeneral.Columns.IdCMInstalacion).IsEqualTo(fkIdDestino).ExecuteAsCollection<GestionGL.Datos.VwInstalacionesGeneralCollection>();
                //TransporteTerrestre.Datos.CatServicios Servicio = new TransporteTerrestre.Datos.CatServicio
                IdServicio = idServicio;
                FkIdOrigen = fkIdOrigen;
                FkIdDestino = fkIdDestino;
                CveServicio = cveServicio;
                DescripcionServicio = descripcionServicio;
                Horario = horario;
                Tipo = tipo;
                Curso = curso;
                BorradoLogico = borradoLogico;
                NombreOrigen = InstalacionOrigen[0].NombreInstalacion;
                NombreDestino = InstalacionDestino[0].NombreInstalacion;
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }
        }

        public ServicioOrigenDestino(int idServicio, string Nodo_Usuario)
        {
            _Nodo_Usuario = Nodo_Usuario;
            try
            {
                GestionGL.Datos.VwCDInstalacionesCollection InstalacionOrigen = null;
                GestionGL.Datos.VwCDInstalacionesCollection InstalacionDestino = null;
                SubSonic.DataProvider pvdGestionGL = SubSonic.DataService.GetInstance("pvdGestionGL");
                TransporteTerrestre.Datos.CatServicios Servicio = TransporteTerrestre.Datos.CatServicios.FetchByID(idServicio);

                if (_Nodo_Usuario == "CME")
                {
                    InstalacionOrigen = new SubSonic.Select(pvdGestionGL)
                   .From<GestionGL.Datos.VwCDInstalaciones>()
                   .Where(GestionGL.Datos.VwCDInstalaciones.Columns.BorradoLogico)
                   .IsEqualTo(false).
                   And(GestionGL.Datos.VwCDInstalaciones.Columns.CveAreaServicio)
                   .IsEqualTo("STPVT")
                   .And(GestionGL.Datos.VwCDInstalaciones.Columns.IdCMInstalacion)
                   .IsEqualTo(Servicio.FkIdOrigen)
                   .ExecuteAsCollection<GestionGL.Datos.VwCDInstalacionesCollection>();

                    InstalacionDestino = new SubSonic.Select(pvdGestionGL)
                    .From<GestionGL.Datos.VwCDInstalaciones>()
                    .Where(GestionGL.Datos.VwCDInstalaciones.Columns.BorradoLogico)
                    .IsEqualTo(false)
                    .And(GestionGL.Datos.VwCDInstalaciones.Columns.CveAreaServicio)
                    .IsEqualTo("STPVT")
                    .And(GestionGL.Datos.VwCDInstalaciones.Columns.IdCMInstalacion)
                    .IsEqualTo(Servicio.FkIdDestino)
                    .ExecuteAsCollection<GestionGL.Datos.VwCDInstalacionesCollection>();
                }
                else if (_Nodo_Usuario == "ZN")
                {
                    InstalacionOrigen = new SubSonic.Select(pvdGestionGL)
                    .From<GestionGL.Datos.VwCDInstalaciones>()
                    .Where(GestionGL.Datos.VwCDInstalaciones.Columns.BorradoLogico)
                    .IsEqualTo(false).
                    And(GestionGL.Datos.VwCDInstalaciones.Columns.CveAreaServicio)
                    .IsEqualTo("STPVT_ZN")
                    .And(GestionGL.Datos.VwCDInstalaciones.Columns.IdCMInstalacion)
                    .IsEqualTo(Servicio.FkIdOrigen)
                    .ExecuteAsCollection<GestionGL.Datos.VwCDInstalacionesCollection>();

                    InstalacionDestino = new SubSonic.Select(pvdGestionGL)
                    .From<GestionGL.Datos.VwCDInstalaciones>()
                    .Where(GestionGL.Datos.VwCDInstalaciones.Columns.BorradoLogico)
                    .IsEqualTo(false)
                    .And(GestionGL.Datos.VwCDInstalaciones.Columns.CveAreaServicio)
                    .IsEqualTo("STPVT_ZN")
                    .And(GestionGL.Datos.VwCDInstalaciones.Columns.IdCMInstalacion)
                    .IsEqualTo(Servicio.FkIdDestino)
                    .ExecuteAsCollection<GestionGL.Datos.VwCDInstalacionesCollection>();
                }

                //TransporteTerrestre.Datos.CatServicios Servicio = new TransporteTerrestre.Datos.CatServicio
                IdServicio = idServicio;
                FkIdOrigen = Servicio.FkIdOrigen;
                FkIdDestino = Servicio.FkIdDestino;
                CveServicio = Servicio.CveServicio;
                DescripcionServicio = Servicio.DescripcionServicio;
                Horario = Servicio.Horario;
                Tipo = Servicio.Tipo;
                Curso = Servicio.Curso;
                BorradoLogico = Servicio.BorradoLogico;
                NombreOrigen = InstalacionOrigen[0].NombreInstalacion;
                NombreDestino = InstalacionDestino[0].NombreInstalacion;
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }
        }

        #endregion
    }
}