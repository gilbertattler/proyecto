﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransporteTerrestre.Negocios.EntidadesEmpresariales
{
    public class GridServicios
    {
       #region Atributos Privados
        private int _IdServicio;
        private string _CveServicio;
        private string _Horario;
        private string _Tipo;
        private bool _Curso;
        private int _Orden;
        #endregion

        #region Atributos Publicos

        public int IdServicio
        {
            get
            {
                return _IdServicio;
            }
            set
            {
                _IdServicio = value;
            }
        }

        public string CveServicio
        {
            get
            {
                return _CveServicio;
            }
            set
            {
                _CveServicio = value;
            }
        }

        public string Horario
        {
            get
            {
                return _Horario;
            }
            set
            {
                _Horario = value;
            }
        }

        public string Tipo
        {
            get
            {
                return _Tipo;
            }
            set
            {
                _Tipo = value;
            }
        }

        public bool Curso
        {
            get
            {
                return _Curso;
            }
            set
            {
                _Curso = value;
            }
        }

        public int Orden
        {
            get
            {
                return _Orden;
            }
            set
            {
                _Orden = value;
            }
        }

        #endregion

        #region Constructores

        public GridServicios()
        {

        }

        public GridServicios(int idServicio, string cveServicio, string horario, string tipo, bool curso, int orden)
        {
            IdServicio = idServicio;
            CveServicio = cveServicio;
            Horario = horario;
            Tipo = tipo;
            Curso = curso;
            Orden = orden;
        }

        #endregion
    }
}
