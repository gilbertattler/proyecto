﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransporteTerrestre.Negocios.EntidadesEmpresariales
{
    //public class PuntosControlCollection
    //{
    //    private System.Collections.Generic.List<PuntosControl> _ColeccionPuntosControl;

    //    public System.Collections.Generic.List<PuntosControl> ColeccionPuntosControl
    //    {
    //        get
    //        {
    //            return _ColeccionPuntosControl;
    //        }
    //    }
        
    //    public PuntosControlCollection()
    //    {
    //        _ColeccionPuntosControl = new List<PuntosControl>();
    //    }

    //    public  PuntosControlCollection(int IdServicio)
    //    {
    //        try
    //        {
    //            TransporteTerrestre.Datos.RelDestinosServicioCollection Destinos = new SubSonic.Select().From<TransporteTerrestre.Datos.RelDestinosServicio>().Where(TransporteTerrestre.Datos.RelDestinosServicio.BorradoLogicoColumn).IsEqualTo(false).And(TransporteTerrestre.Datos.RelDestinosServicio.FkIdServicioColumn).IsEqualTo(IdServicio).ExecuteAsCollection<TransporteTerrestre.Datos.RelDestinosServicioCollection>();
    //            SubSonic.DataProvider pvdGestionGL = SubSonic.DataService.GetInstance("pvdGestionGL");
    //            GestionGL.Datos.VwInstalacionesGeneralCollection Instalaciones = new SubSonic.Select(pvdGestionGL).From<GestionGL.Datos.VwInstalacionesGeneral>().Where(GestionGL.Datos.VwInstalacionesGeneral.Columns.BorradoLogicoInstalacion).IsEqualTo(false).And(GestionGL.Datos.VwInstalacionesGeneral.Columns.CveAreaServicio).IsEqualTo("AH").ExecuteAsCollection<GestionGL.Datos.VwInstalacionesGeneralCollection>();

    //            var resultado = from d in Destinos
    //                            join i in Instalaciones
    //                            on d.FkIdDestino equals i.IdCMInstalacion
    //                            select new PuntosControl()
    //                            {
    //                                IdDestino = i.IdCMInstalacion,
    //                                CveDestino = i.Identificador,
    //                                Destino = i.NombreInstalacion,
    //                                Orden = d.Orden,                                    
    //                            };
    //            _ColeccionPuntosControl = resultado.ToList<TransporteTerrestre.Negocios.EntidadesEmpresariales.PuntosControl>();
    //            //return resultado;
    //        }
    //        catch (System.Exception Ex)
    //        {
    //            throw Ex;
    //        }           
    //    }

    //    public void Agregar(int IdDestino ,string CveDestino, string Destino)
    //    {
    //        try
    //        {
    //            TransporteTerrestre.Negocios.EntidadesEmpresariales.PuntosControl PuntoControl = new PuntosControl(IdDestino, CveDestino, Destino, _ColeccionPuntosControl.Count + 1);
    //            _ColeccionPuntosControl.Add(PuntoControl);
    //        }
    //        catch (System.Exception Ex)
    //        {
    //            throw Ex;
    //        }

    //    }

    //    public void Subir(int Orden)
    //    {
    //        if(Orden > 1)
    //        {
    //            _ColeccionPuntosControl[Orden-1].Orden = Orden-1;
    //            _ColeccionPuntosControl[Orden - 2].Orden = Orden;
                
    //            var ColeccionOrdenada = from line in _ColeccionPuntosControl                                        
    //                                    orderby line.Orden ascending
    //                                    select line;
    //            _ColeccionPuntosControl = ColeccionOrdenada.ToList<TransporteTerrestre.Negocios.EntidadesEmpresariales.PuntosControl>();
    //        }
    //    }

    //    public void Bajar(int Orden)
    //    {
    //        if (Orden < _ColeccionPuntosControl.Count)
    //        {
    //            _ColeccionPuntosControl[Orden - 1].Orden = Orden + 1;
    //            _ColeccionPuntosControl[Orden].Orden = Orden;

    //            var ColeccionOrdenada = from line in _ColeccionPuntosControl
    //                                    orderby line.Orden ascending
    //                                    select line;
    //            _ColeccionPuntosControl = ColeccionOrdenada.ToList<TransporteTerrestre.Negocios.EntidadesEmpresariales.PuntosControl>();
    //        }
    //    }
    //}
}
