﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransporteTerrestre.Negocios.EntidadesEmpresariales
{
    public class ServicioOrigenDestinoCollection
    {
        private System.Collections.Generic.List<ServicioOrigenDestino> _ColeccionServicioOrigenDestino;

        public System.Collections.Generic.List<ServicioOrigenDestino> ColeccionServicioOrigenDestino
        {
            get
            {
                return _ColeccionServicioOrigenDestino;
            }
        }

        public ServicioOrigenDestinoCollection()
        {
            _ColeccionServicioOrigenDestino = new List<ServicioOrigenDestino>();
        }

        public ServicioOrigenDestinoCollection(TransporteTerrestre.Datos.CatServiciosCollection Servicios, string Nodo_Usuario)
        {
            try
            {
                _ColeccionServicioOrigenDestino = new List<ServicioOrigenDestino>();
                for (int i = 0; i < Servicios.Count; i++)
                {
                    _ColeccionServicioOrigenDestino.Add(new ServicioOrigenDestino(Servicios[i].IdServicio,Nodo_Usuario));
                }
                
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }
        }
    }
}
