﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransporteTerrestre.Negocios.EntidadesEmpresariales
{
    public class PuntosControl
    {
        #region Atributos Privados
        private int _IdDestino;
        private string _CveDestino;
        private string _Destino;
        private int _Orden;
        #endregion

        #region Atributos Publicos

        public int IdDestino
        {
            get
            {
                return _IdDestino;
            }
            set
            {
                _IdDestino = value;
            }
        }

        public string CveDestino
        {
            get
            {
                return _CveDestino;
            }
            set
            {
                _CveDestino = value;
            }
        }

        public string Destino
        {
            get
            {
                return _Destino;
            }
            set
            {
                _Destino = value;
            }
        }

        public int Orden
        {
            get
            {
                return _Orden;
            }
            set
            {
                _Orden = value;
            }
        }

        #endregion

        #region Constructores

        public PuntosControl()
        {

        }

        public PuntosControl(int idDestino, string cveDestino, string destino, int orden)
        {
            IdDestino = idDestino;
            CveDestino = cveDestino;
            Destino = destino;
            Orden = orden;
        }

        #endregion
    }
}
