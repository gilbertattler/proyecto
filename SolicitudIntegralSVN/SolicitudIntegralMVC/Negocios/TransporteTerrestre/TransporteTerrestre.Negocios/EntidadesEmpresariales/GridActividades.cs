﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;

namespace TransporteTerrestre.Negocios.EntidadesEmpresariales
{
    public class GridActividades
    {
        public static TransporteTerrestre.Datos.TabActividadesPCCollection OrdenaActividadesAsc(TransporteTerrestre.Datos.TabActividadesPCCollection ListaActividades)
        {
            try
            {
                TransporteTerrestre.Datos.TabActividadesPCCollection ListaActividadesOrdenada = new TransporteTerrestre.Datos.TabActividadesPCCollection();
                int IdActividadAnterior = -1;
                for (int i = 0; i < ListaActividades.Count; i++)
                {
                    if (ListaActividades[i].IdActividadPCAnterior == IdActividadAnterior)
                    {
                        ListaActividadesOrdenada.Add(ListaActividades[i].Clone());
                        IdActividadAnterior = ListaActividades[i].IdActividadPC;
                        if (ListaActividades[i].IdActividadPCSiguiente == -1)
                        {
                            break;
                        }
                        else
                        {
                            i = 0;
                        }
                    }
                }
                return ListaActividadesOrdenada;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static TransporteTerrestre.Datos.TabActividadesPCCollection RetornaGridEditable(TransporteTerrestre.Datos.TabActividadesPCCollection ListaActividades)
        {
            try
            {
                TransporteTerrestre.Datos.TabActividadesPC ActividadEditable = new TransporteTerrestre.Datos.TabActividadesPC();
                ActividadEditable.IdActividadPCAnterior = -2;
                ActividadEditable.IdActividadPCSiguiente = -2;
                ListaActividades.Add(ActividadEditable);
                return ListaActividades;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static TransporteTerrestre.Datos.TabActividadesPCCollection InsertaActividad(TransporteTerrestre.Datos.TabActividadesPCCollection ListaActividades, int Index, string Ficha, string IP, string Nodo_Usuario)
        {
            try
            {
                TransporteTerrestre.Datos.TabActividadesPCCollection ActividadesGuardar = new TransporteTerrestre.Datos.TabActividadesPCCollection();
                if (ListaActividades[Index].IdActividadPCSiguiente == -2 && ListaActividades[Index].IdActividadPCAnterior == -2)
                {
                    if (Index == 0)
                    {
                        ListaActividades[Index].IdActividadPCAnterior = -1;
                    }
                    else
                    {
                        ListaActividades[Index].IdActividadPCAnterior = ListaActividades[Index - 1].IdActividadPC;
                    }
                    if (Index == ListaActividades.Count - 1)
                    {
                        ListaActividades[Index].IdActividadPCSiguiente = -1;
                    }
                    else
                    {
                        ListaActividades[Index].IdActividadPCSiguiente = ListaActividades[Index + 1].IdActividadPC;
                    }
                    TransporteTerrestre.Datos.TabActividadesPC ActividadNueva = ListaActividades[Index].Clone();
                    ActividadNueva.Nodo = Nodo_Usuario;
                    ActividadNueva.Save();
                    if (Index == 0)
                    {
                    }
                    else
                    {
                        ListaActividades[Index - 1].IsNew = false;
                        ListaActividades[Index - 1].IsLoaded = true;
                        ListaActividades[Index - 1].IdActividadPCSiguiente = ActividadNueva.IdActividadPC;
                        ListaActividades[Index - 1].Nodo = Nodo_Usuario;
                        ListaActividades[Index - 1].Save();
                    }
                    if (Index == ListaActividades.Count - 1)
                    {

                    }
                    else
                    {
                        ListaActividades[Index + 1].IsNew = false;
                        ListaActividades[Index + 1].IsLoaded = true;
                        ListaActividades[Index + 1].IdActividadPCAnterior = ActividadNueva.IdActividadPC;
                        ListaActividades[Index - 1].Nodo = Nodo_Usuario;
                        ListaActividades[Index + 1].Save();
                    }
                }
                if (ListaActividades[Index].NombreActividad == "INICIO VIAJE")
                {
                    TransporteTerrestre.Datos.TabPuntosControl PuntoControl = TransporteTerrestre.Datos.TabPuntosControl.FetchByID(ListaActividades[Index].FkIdPuntoControl);
                    TransporteTerrestre.Negocios.Viajes.AperturaCierreViaje(PuntoControl.FkIdViaje, Convert.ToDateTime(ListaActividades[Index].FechaHoraInicio), "INICIO VIAJE", Ficha, IP, Nodo_Usuario);
                }
                else if (ListaActividades[Index].NombreActividad == "FIN VIAJE")
                {
                    TransporteTerrestre.Datos.TabPuntosControl PuntoControl = TransporteTerrestre.Datos.TabPuntosControl.FetchByID(ListaActividades[Index].FkIdPuntoControl);
                    TransporteTerrestre.Negocios.Viajes.AperturaCierreViaje(PuntoControl.FkIdViaje, Convert.ToDateTime(ListaActividades[Index].FechaHoraTermino), "FIN VIAJE", Ficha, IP, Nodo_Usuario);
                }

                ActividadesGuardar = new SubSonic.Select().
                   From<TransporteTerrestre.Datos.TabActividadesPC>()
                   .Where(TransporteTerrestre.Datos.TabActividadesPC.FkIdPuntoControlColumn)
                   .IsEqualTo(ListaActividades[Index].FkIdPuntoControl)
                   .And(TransporteTerrestre.Datos.TabActividadesPC.BorradoLogicoColumn)
                   .IsEqualTo(false)
                   .And(TransporteTerrestre.Datos.TabActividadesPC.NodoColumn)
                   .IsEqualTo(Nodo_Usuario)
                   .ExecuteAsCollection<TransporteTerrestre.Datos.TabActividadesPCCollection>();
                
                
                //Guarda Auditoria
                TransporteTerrestre.Datos.TabAuditorias Auditoria = new TransporteTerrestre.Datos.TabAuditorias();
                Auditoria.Fecha = System.DateTime.Now;
                Auditoria.Ip = IP;
                Auditoria.IdTabla = ActividadesGuardar[Index].IdActividadPC;
                Auditoria.Ficha = Ficha;
                Auditoria.TipoMovimiento = "Insertar";
                Auditoria.Descripcion = "Se inserto la actividad con Id: " + ActividadesGuardar[Index].IdActividadPC;
                Auditoria.Nodo = Nodo_Usuario;
                Auditoria.Save();

               ActividadesGuardar = OrdenaActividadesAsc(ActividadesGuardar);

                return ActividadesGuardar;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static TransporteTerrestre.Datos.TabActividadesPCCollection InsertaActividad(TransporteTerrestre.Datos.TabActividadesPCCollection ListaActividades, int Index, string Ficha, string IP, bool Transaccionado, string Nodo_Usuario)
        {
            try
            {
                TransporteTerrestre.Datos.TabActividadesPCCollection ValorRetorno = new TransporteTerrestre.Datos.TabActividadesPCCollection();
                if (Transaccionado)
                {
                    using (TransactionScope Scope = new TransactionScope())
                    {
                        using (SubSonic.SharedDbConnectionScope sharedConnectionScope = new SubSonic.SharedDbConnectionScope())
                        {
                            ValorRetorno = InsertaActividad(ListaActividades, Index,Ficha,IP, Nodo_Usuario);
                            Scope.Complete();
                        }
                    }
                }
                else
                {
                    ValorRetorno = InsertaActividad(ListaActividades, Index,Ficha,IP, Nodo_Usuario);
                }
                return ValorRetorno;
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }
        }

        public static TransporteTerrestre.Datos.TabActividadesPCCollection GuardarActividad(TransporteTerrestre.Datos.TabActividadesPCCollection ListaActividades, int Index, string Ficha, string IP, string Nodo_Usuario)
        {
            try
            {
                TransporteTerrestre.Datos.TabActividadesPC ActividarGuardar = new TransporteTerrestre.Datos.TabActividadesPC();
                ActividarGuardar.IsNew = false;
                ActividarGuardar.IsLoaded = true;
                ActividarGuardar.IdActividadPC = ListaActividades[Index].IdActividadPC;
                ActividarGuardar.IdActividadPCAnterior = ListaActividades[Index].IdActividadPCAnterior;
                ActividarGuardar.IdActividadPCSiguiente = ListaActividades[Index].IdActividadPCSiguiente;
                ActividarGuardar.FkIdPuntoControl = ListaActividades[Index].FkIdPuntoControl;
                ActividarGuardar.FechaHoraInicio = ListaActividades[Index].FechaHoraInicio;
                ActividarGuardar.FechaHoraTermino = ListaActividades[Index].FechaHoraTermino;
                ActividarGuardar.NombreActividad = ListaActividades[Index].NombreActividad;
                ActividarGuardar.PaxCarga = ListaActividades[Index].PaxCarga;
                ActividarGuardar.PaxDescarga = ListaActividades[Index].PaxDescarga;
                ActividarGuardar.Observaciones = ListaActividades[Index].Observaciones;
                ActividarGuardar.Nodo = Nodo_Usuario;
                ActividarGuardar.Save();

                //Guarda Auditoria
                TransporteTerrestre.Datos.TabAuditorias Auditoria = new TransporteTerrestre.Datos.TabAuditorias();
                Auditoria.Fecha = System.DateTime.Now;
                Auditoria.Ip = IP;
                Auditoria.IdTabla = ActividarGuardar.IdActividadPC;
                Auditoria.Ficha = Ficha;
                Auditoria.TipoMovimiento = "Mofdificar";
                Auditoria.Descripcion = "Se modifico la actividad con Id: " + ActividarGuardar.IdActividadPC;
                Auditoria.Nodo = Nodo_Usuario;
                Auditoria.Save();

                if (ActividarGuardar.NombreActividad == "INICIO VIAJE")
                {
                    TransporteTerrestre.Datos.TabPuntosControl PuntoControl = TransporteTerrestre.Datos.TabPuntosControl.FetchByID(ActividarGuardar.FkIdPuntoControl);
                    TransporteTerrestre.Negocios.Viajes.AperturaCierreViaje(PuntoControl.FkIdViaje, System.DateTime.Now, "INICIO VIAJE",Ficha,IP, Nodo_Usuario);
                }
                else if (ActividarGuardar.NombreActividad == "FIN VIAJE")
                {
                    TransporteTerrestre.Datos.TabPuntosControl PuntoControl = TransporteTerrestre.Datos.TabPuntosControl.FetchByID(ActividarGuardar.FkIdPuntoControl);
                    TransporteTerrestre.Negocios.Viajes.AperturaCierreViaje(PuntoControl.FkIdViaje, System.DateTime.Now, "FIN VIAJE",Ficha,IP, Nodo_Usuario);
                }
                
                ListaActividades = new SubSonic.Select()
                    .From<TransporteTerrestre.Datos.TabActividadesPC>()
                    .Where(TransporteTerrestre.Datos.TabActividadesPC.BorradoLogicoColumn)
                    .IsEqualTo(false)
                    .And(TransporteTerrestre.Datos.TabActividadesPC.FkIdPuntoControlColumn)
                    .IsEqualTo(ActividarGuardar.FkIdPuntoControl)
                    .And(TransporteTerrestre.Datos.TabActividadesPC.NodoColumn)
                    .IsEqualTo(Nodo_Usuario)
                    .ExecuteAsCollection<TransporteTerrestre.Datos.TabActividadesPCCollection>();

                return OrdenaActividadesAsc(ListaActividades);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static TransporteTerrestre.Datos.TabActividadesPCCollection GuardarActividad(TransporteTerrestre.Datos.TabActividadesPCCollection ListaActividades, int Index, string Ficha, string IP, bool Transaccionado, string Nodo_Usuario)
        {
            try
            {
                TransporteTerrestre.Datos.TabActividadesPCCollection ValorRetorno = new TransporteTerrestre.Datos.TabActividadesPCCollection();
                if (Transaccionado)
                {
                    using (TransactionScope Scope = new TransactionScope())
                    {
                        using (SubSonic.SharedDbConnectionScope sharedConnectionScope = new SubSonic.SharedDbConnectionScope())
                        {
                            ValorRetorno = GuardarActividad(ListaActividades, Index, Ficha,IP,Nodo_Usuario);
                            Scope.Complete();
                        }
                    }
                }
                else
                {
                    ValorRetorno = GuardarActividad(ListaActividades, Index, Ficha, IP, Nodo_Usuario);
                }
                return ValorRetorno;
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }
        }
    }
}
