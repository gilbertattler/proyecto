﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;

namespace TransporteTerrestre.Negocios
{
    public class Transportes
    {
        public static int Insertar(string FkTipoTransporte, string CveTransporte, string Placa, string Marca, string Modelo, string Capacidad, string TarjetaCirculacion, string VigenciaTarjetaCirculacion, string Tenencia, string VigenciaTenencia, string NumeroSerie, string CompaniaSeguro, string VigenciaSeguro, string CartaFactura, string ContratoSAP, string Convenio, string Costo, string Status, string Ficha, string IP, string Nodo_Usuario)
        {
            try
            {
                SubSonic.DataProvider pvdGestionGL = SubSonic.DataService.GetInstance("pvdGestionGL");
                GestionGL.Datos.CVehiculosCollection VerificaTransporte = new SubSonic.Select(pvdGestionGL)
                .From<GestionGL.Datos.CVehiculos>()
                .Where(GestionGL.Datos.CVehiculos.BorradoLogicoColumn)
                .IsEqualTo(false)
                .And(GestionGL.Datos.CVehiculos.CveTransporteColumn)
                .IsEqualTo(CveTransporte)
                .And(GestionGL.Datos.CVehiculos.NodoColumn)
                .IsEqualTo(Nodo_Usuario)
                .ExecuteAsCollection<GestionGL.Datos.CVehiculosCollection>();

                if (VerificaTransporte.Count > 0)
                {
                    throw new Exception("Ya existe un transporte con la clave: " + CveTransporte);
                }
                GestionGL.Datos.CVehiculos Transporte = new GestionGL.Datos.CVehiculos();
                                
                Transporte.FkTipoTransporte = Convert.ToInt32(FkTipoTransporte);
                Transporte.CveTransporte = CveTransporte;
                Transporte.Placa = Placa;
                Transporte.Marca = Marca;
                Transporte.Modelo = Modelo;
                Transporte.Capacidad = Convert.ToInt32(Capacidad);
                Transporte.TarjetaCirculacion = TarjetaCirculacion;
                Transporte.VigenciaTarjetaCirculacion = Convert.ToDateTime(VigenciaSeguro);
                Transporte.Tenencia = Tenencia;
                Transporte.VigenciaTenencia = Convert.ToDateTime(VigenciaTenencia);
                Transporte.NumeroSerie = NumeroSerie;
                Transporte.CompaniaSeguro = CompaniaSeguro;
                Transporte.VigenciaSeguro = Convert.ToDateTime(VigenciaSeguro);
                Transporte.CartaFactura = CartaFactura;
                Transporte.BorradoLogico = false;
                Transporte.ContratoSAP = ContratoSAP;
                Transporte.Convenio = Convenio;
                Transporte.Costo = Costo;
                Transporte.Status = Status;
                Transporte.Nodo = Nodo_Usuario;
                Transporte.Save();

                //Guarda Auditoria
                TransporteTerrestre.Datos.TabAuditorias Auditoria = new TransporteTerrestre.Datos.TabAuditorias();
                Auditoria.Fecha = System.DateTime.Now;
                Auditoria.Ip = IP;
                Auditoria.IdTabla = Transporte.IdTransporte;
                Auditoria.Ficha = Ficha;
                Auditoria.TipoMovimiento = "Insertar";
                Auditoria.Descripcion = "Se creo el nuevo transporte con Id: " + Transporte.IdTransporte;
                Auditoria.Nodo = Nodo_Usuario;
                Auditoria.Save();

                return Transporte.IdTransporte;
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }
        }
        public static int Insertar(string FkTipoTransporte, string CveTransporte, string Placa, string Marca, string Modelo, string Capacidad, string TarjetaCirculacion, string VigenciaTarjetaCirculacion, string Tenencia, string VigenciaTenencia, string NumeroSerie, string CompaniaSeguro, string VigenciaSeguro, string CartaFactura, string ContratoSAP, string Convenio, string Costo, string Status, string Ficha, string IP, bool Transaccionado, string Nodo_Usuario)
        {
            try
            {
                int ValorRetorno;
                if (Transaccionado)
                {
                    using (TransactionScope Scope = new TransactionScope())
                    {
                        using (SubSonic.SharedDbConnectionScope sharedConnectionScope = new SubSonic.SharedDbConnectionScope())
                        {
                            ValorRetorno = Insertar(FkTipoTransporte,CveTransporte,Placa,Marca,Modelo,Capacidad,TarjetaCirculacion,VigenciaTarjetaCirculacion,Tenencia,VigenciaTenencia,NumeroSerie,CompaniaSeguro,VigenciaSeguro,CartaFactura, ContratoSAP, Convenio, Costo, Status,Ficha,IP,Nodo_Usuario);
                            Scope.Complete();
                        }
                    }
                }
                else
                {
                    ValorRetorno = Insertar(FkTipoTransporte, CveTransporte, Placa, Marca, Modelo, Capacidad, TarjetaCirculacion, VigenciaTarjetaCirculacion, Tenencia, VigenciaTenencia, NumeroSerie, CompaniaSeguro, VigenciaSeguro, CartaFactura, ContratoSAP, Convenio, Costo, Status,Ficha,IP,Nodo_Usuario);
                }
                return ValorRetorno;
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }
        }
        public static void Guardar(string IdTransporte, string FkTipoTransporte, string CveTransporte, string Placa, string Marca, string Modelo, string Capacidad, string TarjetaCirculacion, string VigenciaTarjetaCirculacion, string Tenencia, string VigenciaTenencia, string NumeroSerie, string CompaniaSeguro, string VigenciaSeguro, string CartaFactura, string ContratoSAP, string Convenio, string Costo, string Ficha, string IP, string Nodo_Usuario)
        {
            try
            {
                TransporteTerrestre.Datos.VwTerrestreVehiculosGestionCollection VerificaTransporte = new SubSonic.Select()
                .From<TransporteTerrestre.Datos.VwTerrestreVehiculosGestion>()
                .Where(TransporteTerrestre.Datos.VwTerrestreVehiculosGestion.Columns.BorradoLogico)
                .IsEqualTo(false)
                .And(TransporteTerrestre.Datos.VwTerrestreVehiculosGestion.Columns.CveTransporte)
                .IsEqualTo(CveTransporte)
                .And(TransporteTerrestre.Datos.VwTerrestreVehiculosGestion.Columns.Nodo)
                .IsEqualTo(Nodo_Usuario)
                .ExecuteAsCollection<TransporteTerrestre.Datos.VwTerrestreVehiculosGestionCollection>();

                //SubSonic.DataProvider pvdGestionGL = SubSonic.DataService.GetInstance("pvdGestionGL");
                //GestionGL.Datos.CVehiculosCollection VerificaTransporte = new SubSonic.Select(pvdGestionGL)
                //.From<GestionGL.Datos.CVehiculos>()
                //.Where(GestionGL.Datos.CVehiculos.BorradoLogicoColumn)
                //.IsEqualTo(false)
                //.And(GestionGL.Datos.CVehiculos.CveTransporteColumn)
                //.IsEqualTo(CveTransporte)
                //.And(GestionGL.Datos.CVehiculos.NodoColumn)
                //.IsEqualTo(Nodo_Usuario)
                //.ExecuteAsCollection<GestionGL.Datos.CVehiculosCollection>();
                
                if (VerificaTransporte.Count > 0)
                {
                    bool Mismo = false;
                    for (int i = 0; i < VerificaTransporte.Count; i++)
                    {
                        if(VerificaTransporte[i].IdTransporte.ToString() == IdTransporte)
                        {
                            Mismo = true;
                            break;
                        }
                    }
                    if(!Mismo)
                    {
                        throw new Exception("Ya existe un transporte con la clave: " + CveTransporte);
                    }                    
                }

                //TransporteTerrestre.Datos.CatTransportes Transporte = new TransporteTerrestre.Datos.CatTransportes();
                GestionGL.Datos.CVehiculos Transporte = new GestionGL.Datos.CVehiculos();
                
                Transporte.IsNew = false;
                Transporte.IsLoaded = true;
                Transporte.IdTransporte = Convert.ToInt32(IdTransporte);
                Transporte.FkTipoTransporte = Convert.ToInt32(FkTipoTransporte);
                Transporte.CveTransporte = CveTransporte;
                Transporte.Placa = Placa;
                Transporte.Marca = Marca;
                Transporte.Modelo = Modelo;
                Transporte.Capacidad = Convert.ToInt32(Capacidad) ;
                Transporte.TarjetaCirculacion = TarjetaCirculacion;
                Transporte.VigenciaTarjetaCirculacion = Convert.ToDateTime(VigenciaSeguro);
                Transporte.Tenencia = Tenencia;
                Transporte.VigenciaTenencia = Convert.ToDateTime(VigenciaTenencia);
                Transporte.NumeroSerie = NumeroSerie;
                Transporte.CompaniaSeguro = CompaniaSeguro;
                Transporte.VigenciaSeguro = Convert.ToDateTime(VigenciaSeguro);
                Transporte.CartaFactura = CartaFactura;
                Transporte.BorradoLogico = false;
                Transporte.ContratoSAP = ContratoSAP;
                Transporte.Convenio = Convenio;
                Transporte.Costo = Costo;
                Transporte.Nodo = Nodo_Usuario;
                //Transporte.Status = Status;
                Transporte.Save();

                //Guarda Auditoria
                TransporteTerrestre.Datos.TabAuditorias Auditoria = new TransporteTerrestre.Datos.TabAuditorias();
                Auditoria.Fecha = System.DateTime.Now;
                Auditoria.Ip = IP;
                Auditoria.IdTabla = Transporte.IdTransporte;
                Auditoria.Ficha = Ficha;
                Auditoria.TipoMovimiento = "Modificar";
                Auditoria.Descripcion = "Se modifico el transporte con Id: " + Transporte.IdTransporte;
                Auditoria.Nodo = Nodo_Usuario;
                Auditoria.Save();
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }
        }
        public static void Guardar(string IdTransporte, string FkTipoTransporte, string CveTransporte, string Placa, string Marca, string Modelo, string Capacidad, string TarjetaCirculacion, string VigenciaTarjetaCirculacion, string Tenencia, string VigenciaTenencia, string NumeroSerie, string CompaniaSeguro, string VigenciaSeguro, string CartaFactura, string ContratoSAP, string Convenio, string Costo, string Ficha, string IP, bool Transaccionado, string Nodo_Usuario)
        {
            try
            {
                if (Transaccionado)
                {
                    using (TransactionScope Scope = new TransactionScope())
                    {
                        using (SubSonic.SharedDbConnectionScope sharedConnectionScope = new SubSonic.SharedDbConnectionScope("pvdGestionGL"))
                        {
                            Guardar(IdTransporte,FkTipoTransporte, CveTransporte, Placa, Marca, Modelo, Capacidad, TarjetaCirculacion, VigenciaTarjetaCirculacion, Tenencia, VigenciaTenencia, NumeroSerie, CompaniaSeguro, VigenciaSeguro, CartaFactura, ContratoSAP, Convenio, Costo,Ficha,IP,Nodo_Usuario);
                            Scope.Complete();
                        }
                    }
                }
                else
                {
                    Guardar(IdTransporte,FkTipoTransporte, CveTransporte, Placa, Marca, Modelo, Capacidad, TarjetaCirculacion, VigenciaTarjetaCirculacion, Tenencia, VigenciaTenencia, NumeroSerie, CompaniaSeguro, VigenciaSeguro, CartaFactura, ContratoSAP, Convenio, Costo,Ficha,IP,Nodo_Usuario);
                }
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }
        }
        public static void InsertarGuardar(string IdTransporte, string FkTipoTransporte, string CveTransporte, string Placa, string Marca, string Modelo, string Capacidad, string TarjetaCirculacion, string VigenciaTarjetaCirculacion, string Tenencia, string VigenciaTenencia, string NumeroSerie, string CompaniaSeguro, string VigenciaSeguro, string CartaFactura, string ContratoSAP, string Convenio, string Costo, string Status, string Ficha, string IP, string Nodo_Usuario)
        {
            if (Placa == "")
            {
                throw new System.Exception("Debe proporcionar la placa de vehiculo.");
            }
            if (Marca == "")
            {
                throw new System.Exception("Debe proporcionar la marca del vehiculo.");
            }
            if (CveTransporte == "")
            {
                throw new System.Exception("Debe proporcionar la clave del vehiculo.");
            }
            if (Capacidad == "")
            {
                throw new System.Exception("Debe proporcionar la capacidad del vehiculo.");
            }
            if (TarjetaCirculacion == "")
            {
                throw new System.Exception("Debe proporcionar la número de la tarjeta de circulación del vehiculo.");
            }
            if (VigenciaTarjetaCirculacion == "")
            {
                throw new System.Exception("Debe proporcionar la vigencia de la tarjeta de circulación del vehiculo.");
            }
            if (Tenencia == "")
            {
                throw new System.Exception("Debe proporcionar la tenencia del vehiculo.");
            }
            if (VigenciaTenencia == "")
            {
                throw new System.Exception("Debe proporcionar la vigencia de la tenencia del vehiculo.");
            }
            if (NumeroSerie == "")
            {
                throw new System.Exception("Debe proporcionar el número de serie del vehiculo.");
            }
            if (CompaniaSeguro == "")
            {
                throw new System.Exception("Debe proporcionar el nombre de la compañia aseguradora.");
            }
            if (VigenciaSeguro == "")
            {
                throw new System.Exception("Debe proporcionar la vigencia del seguro.");
            }
            if (CartaFactura == "")
            {
                throw new System.Exception("Debe proporcionar el número de la carta factura y/o factura.");
            }
            if (ContratoSAP == "")
            {
                throw new System.Exception("Debe proporcionar el número de contrato SAP.");
            }
            if (Convenio == "")
            {
                throw new System.Exception("Debe proporcionar el número de convenio.");
            }
            if (Costo == "")
            {
                throw new System.Exception("Debe proporcionar el número de costo.");
            }
            try
            {
                if (IdTransporte == "Nuevo")
                {
                    Insertar(FkTipoTransporte, CveTransporte, Placa, Marca, Modelo, Capacidad, TarjetaCirculacion, VigenciaTarjetaCirculacion, Tenencia, VigenciaTenencia, NumeroSerie, CompaniaSeguro, VigenciaSeguro, CartaFactura,ContratoSAP,Convenio, Costo,Status,Ficha,IP,false, Nodo_Usuario);
                }
                else
                {
                    Guardar(IdTransporte, FkTipoTransporte, CveTransporte, Placa, Marca, Modelo, Capacidad, TarjetaCirculacion, VigenciaTarjetaCirculacion, Tenencia, VigenciaTenencia, NumeroSerie, CompaniaSeguro, VigenciaSeguro, CartaFactura,ContratoSAP,Convenio,Costo,Ficha,IP, false, Nodo_Usuario);
                }
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }

        }
        public static void InsertarGuardar(string IdTransporte, string FkTipoTransporte, string CveTransporte, string Placa, string Marca, string Modelo, string Capacidad, string TarjetaCirculacion, string VigenciaTarjetaCirculacion, string Tenencia, string VigenciaTenencia, string NumeroSerie, string CompaniaSeguro, string VigenciaSeguro, string CartaFactura, string ContratoSAP, string Convenio, string Costo, string Status, string Ficha, string IP, bool Transaccionado, string Nodo_Usuario)
        {
            try
            {
                if (Transaccionado)
                {
                    using (TransactionScope Scope = new TransactionScope())
                    {
                        using (SubSonic.SharedDbConnectionScope sharedConnectionScope = new SubSonic.SharedDbConnectionScope())
                        {
                            InsertarGuardar(IdTransporte, FkTipoTransporte, CveTransporte, Placa, Marca, Modelo, Capacidad, TarjetaCirculacion, VigenciaTarjetaCirculacion, Tenencia, VigenciaTenencia, NumeroSerie, CompaniaSeguro, VigenciaSeguro, CartaFactura,ContratoSAP,Convenio,Costo,Status,Ficha,IP,Nodo_Usuario);
                            Scope.Complete();
                        }
                    }
                }
                else
                {
                    InsertarGuardar(IdTransporte, FkTipoTransporte, CveTransporte, Placa, Marca, Modelo, Capacidad, TarjetaCirculacion, VigenciaTarjetaCirculacion, Tenencia, VigenciaTenencia, NumeroSerie, CompaniaSeguro, VigenciaSeguro, CartaFactura,ContratoSAP,Convenio,Costo,Status,Ficha,IP,Nodo_Usuario);
                }
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }
            
        }
        public static void Eliminar(int IdTransporte, string Ficha, string IP, string Nodo_Usuario)
        {
            try
            {
                //TransporteTerrestre.Datos.CatTransportes Transporte = TransporteTerrestre.Datos.CatTransportes.FetchByID(IdTransporte);
                GestionGL.Datos.CVehiculos Transporte = GestionGL.Datos.CVehiculos.FetchByID(IdTransporte);
                Transporte.BorradoLogico = true;
                Transporte.Save();

                //Guarda Auditoria
                TransporteTerrestre.Datos.TabAuditorias Auditoria = new TransporteTerrestre.Datos.TabAuditorias();
                Auditoria.Fecha = System.DateTime.Now;
                Auditoria.Ip = IP;
                Auditoria.IdTabla = Transporte.IdTransporte;
                Auditoria.Ficha = Ficha;
                Auditoria.TipoMovimiento = "Eliminar";
                Auditoria.Descripcion = "Se elimino el transporte con Id: " + Transporte.IdTransporte;
                Auditoria.Nodo = Nodo_Usuario;
                Auditoria.Save();
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
        public static void Eliminar(int IdTransporte, string Ficha, string IP, bool Transaccionado, string Nodo_Usuario)
        {
            try
            {
                if (Transaccionado)
                {
                    using (TransactionScope Scope = new TransactionScope())
                    {
                        using (SubSonic.SharedDbConnectionScope sharedConnectionScope = new SubSonic.SharedDbConnectionScope())
                        {
                            Eliminar(IdTransporte,Ficha,IP,Nodo_Usuario);
                            Scope.Complete();
                        }
                    }
                }
                else
                {
                    Eliminar(IdTransporte,Ficha,IP, Nodo_Usuario);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
        public static void CambioStatus(int IdTransporte, string Status, string Ficha, string IP,string Nodo_Usuario)
        {
            try
            {
                //TransporteTerrestre.Datos.CatTransportes Transporte = TransporteTerrestre.Datos.CatTransportes.FetchByID(IdTransporte);
                GestionGL.Datos.CVehiculos Transporte = GestionGL.Datos.CVehiculos.FetchByID(IdTransporte);
                TransporteTerrestre.Datos.TabAuditorias Auditoria = new TransporteTerrestre.Datos.TabAuditorias();                
                Transporte.Status = Status;
                Transporte.Save();
                Auditoria.Fecha = System.DateTime.Now;
                Auditoria.Ip = IP;
                Auditoria.IdTabla = IdTransporte;
                Auditoria.Ficha = Ficha;
                if(Status == "FUERA DE SERVICIO")
                {
                    Auditoria.TipoMovimiento = "FUERA DE SERVICIO";
                    Auditoria.Descripcion = "Se ha puesto fuera de servicio el transporte con Id: "+IdTransporte;
                }
                else if(Status == "ACTIVO")
                {
                    Auditoria.TipoMovimiento = "ACTIVO";
                    Auditoria.Descripcion = "Se ha puesto activo el transporte con Id:" + IdTransporte;
                }
                Auditoria.Nodo = Nodo_Usuario;
                Auditoria.Save();
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
        public static void CambioStatus(int IdTransporte, string Status, string Ficha, string IP, bool Transaccionado, string Nodo_Usuario)
        {
            try
            {
                if (Transaccionado)
                {
                    using (TransactionScope Scope = new TransactionScope())
                    {
                        using (SubSonic.SharedDbConnectionScope sharedConnectionScope = new SubSonic.SharedDbConnectionScope())
                        {
                            CambioStatus(IdTransporte,Status, Ficha, IP,Nodo_Usuario );
                            Scope.Complete();
                        }
                    }
                }
                else
                {
                    CambioStatus(IdTransporte, Status, Ficha, IP, Nodo_Usuario);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
    }
}
