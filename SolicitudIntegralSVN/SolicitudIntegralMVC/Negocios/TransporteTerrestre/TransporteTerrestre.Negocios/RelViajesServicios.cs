﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;

namespace TransporteTerrestre.Negocios
{
    public class RelViajesServicioss
    {
        public static void Insertar(int IdViaje, int IdServicio, int Orden)
        {
            try
            {
                TransporteTerrestre.Datos.RelViajesServicios ViajeServicio = new TransporteTerrestre.Datos.RelViajesServicios();
                ViajeServicio.FkIdViaje = IdViaje;
                ViajeServicio.FkIdServicio = IdServicio;
                ViajeServicio.Orden = Orden;
                ViajeServicio.BorradoLogico = false;
                ViajeServicio.Save();
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static void Insertar(int IdViaje, int IdServicio, int Orden, bool Transaccionado)
        {
            try
            {
                if(Transaccionado)
                {
                    using (TransactionScope Scope = new TransactionScope())
                    {
                        using (SubSonic.SharedDbConnectionScope sharedConnectionScope = new SubSonic.SharedDbConnectionScope())
                        {
                            Insertar(IdViaje, IdServicio, Orden);
                            Scope.Complete();
                        }
                    }
                }
                else 
                {
                    Insertar(IdViaje, IdServicio, Orden);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

    }
}
