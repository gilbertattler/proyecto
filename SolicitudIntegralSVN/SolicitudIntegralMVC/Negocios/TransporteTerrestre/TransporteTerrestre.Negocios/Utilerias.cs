﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransporteTerrestre.Negocios
{

    public enum TipoPersonal
    {
        PEMEX = 0,
        COMPANIA = 1,
        VISITA = 2
    }

    public class Utilerias
    {
        public static SubSonic.DataProvider ObtenerProveedorDatosGestionGl()
        {
            return SubSonic.DataService.GetInstance("pvdGestionGL");
        }

        public static SubSonic.DataProvider ObtenerProveedorDatosRhmCases()
        {
            return SubSonic.DataService.GetInstance("pvdRhmCases");
        }

        public static TipoPersonal ObtenerTipoTrabajador(string FichaRFC)
        {

            string regexRFC = "^(?<RFCC>[a-zA-Z]{4})\\W*(?<RFCN>\\d{4,6})(?<RFC>\\w{0,4})$";
            string regexFicha = "^(?<N>[0]*)(?<FICHA>\\d{5,6})$";
            TipoPersonal resultado = TipoPersonal.COMPANIA;

            System.Text.RegularExpressions.RegexOptions options = ((System.Text.RegularExpressions.RegexOptions.IgnorePatternWhitespace | System.Text.RegularExpressions.RegexOptions.Multiline) | System.Text.RegularExpressions.RegexOptions.IgnoreCase);


            if (System.Text.RegularExpressions.Regex.IsMatch(FichaRFC, regexFicha, options))
            {
                resultado = TipoPersonal.PEMEX;
            }
            else
            {
                if (System.Text.RegularExpressions.Regex.IsMatch(FichaRFC, regexRFC, options))
                {
                    resultado = TipoPersonal.COMPANIA;
                }
                else
                {
                    //No coincido con la validacion del RFC sin embargo se va a considerar como de compa
                    resultado = TipoPersonal.COMPANIA;
                }
            }
            return resultado;
        }


        public static string FormatearFichaRFC(string ficha_rfc)
        {
            string resultado = ficha_rfc.Trim();

            TipoPersonal tipo_trabajador = ObtenerTipoTrabajador(resultado);

            if (tipo_trabajador == TipoPersonal.PEMEX)
                resultado = FormatearFicha(resultado);
            else
            {
                if (tipo_trabajador == TipoPersonal.COMPANIA)
                {
                    resultado = FormatearRFCCompania(resultado);
                }
            }
            return resultado;
        }

        public static string FormatearRFCCompania(string RFC)
        {
            return RFC.PadRight(14, '0');
        }


        public static string FormatearFicha(string ficha)
        {
            string resultado = ficha;
            TipoPersonal tipo_trabajador = ObtenerTipoTrabajador(ficha);

            if (tipo_trabajador == TipoPersonal.PEMEX)
                resultado = String.Format("{0:00000000}", Convert.ToInt32(ficha));
            else
            {
                if (tipo_trabajador == TipoPersonal.COMPANIA)
                {
                    resultado = ficha;
                }
            }
            return resultado;
        }
    }
}
