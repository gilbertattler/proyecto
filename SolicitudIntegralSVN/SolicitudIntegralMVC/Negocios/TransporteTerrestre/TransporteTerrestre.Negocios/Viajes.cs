﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;

namespace TransporteTerrestre.Negocios
{
    public class Viajes
    {
        /*public static int Insertar(int IdTransporte, string CveViaje, int Dia, System.DateTime FechaViaje)
        {
            
        }*/

        //public static void InsertarPorVigencia(int IdTransporte, int IdChofer, string CveViaje, System.DateTime Inicio, System.DateTime Fin, TransporteTerrestre.Datos.RelViajesServiciosCollection ViajeServicios)
        //{
        //    try
        //    {
        //        //TimeSpan Dias = Fin - Inicio;
        //        //int NoDias = Dias.Days + 1;
        //        //TransporteTerrestre.Datos.TabViajes Viaje = null;
        //        //string Folio = GeneraFolio(Inicio);

        //        //for (int i = 0; i < NoDias; i++)
        //        //{
        //            Viaje = new TransporteTerrestre.Datos.TabViajes();
        //            Viaje.FkIdTransporte = IdTransporte;
        //            Viaje.CveViaje = CveViaje;
        //            Viaje.FechaViaje = Inicio.AddDays(i);
        //            Viaje.Dia = i + 1;
        //            Viaje.CveViaje = Folio;
        //            Viaje.FkIdChofer = IdChofer;
        //            Viaje.Save();
        //        //    for (int j = 0; j < ViajeServicios.Count; j++)
        //        //    {
        //        //        TransporteTerrestre.Negocios.RelViajesServicioss.Insertar(Viaje.IdViaje, ViajeServicios[j].FkIdServicio, ViajeServicios[j].Orden, false);
        //        //    }
        //        //}
        //    }
        //    catch (System.Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public static void InsertarPorVigencia(int IdTransporte, int IdChofer, string CveViaje, System.DateTime Inicio, System.DateTime Fin, TransporteTerrestre.Datos.RelViajesServiciosCollection ViajeServicios, bool Transaccionado)
        //{
        //    try
        //    {
        //        if (Transaccionado)
        //        {
        //            using (TransactionScope Scope = new TransactionScope())
        //            {
        //                using (SubSonic.SharedDbConnectionScope sharedConnectionScope = new SubSonic.SharedDbConnectionScope())
        //                {
        //                    InsertarPorVigencia(IdTransporte, IdChofer, CveViaje, Inicio, Fin, ViajeServicios);
        //                    Scope.Complete();
        //                }
        //            }
        //        }
        //        else
        //        {
        //            InsertarPorVigencia(IdTransporte, IdChofer, CveViaje, Inicio, Fin, ViajeServicios);
        //        }
        //    }
        //    catch (System.Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public static void Insertar(int IdTransporte, int IdChofer, DateTime FechaViaje, string Ficha, string IP, string Nodo_Usuario)
        {
            try
            {
                TransporteTerrestre.Datos.TabViajes Viaje = new TransporteTerrestre.Datos.TabViajes();                
                Viaje.FkIdTransporte = IdTransporte;
                Viaje.FechaViaje = FechaViaje;
                Viaje.CveViaje = GeneraFolio(FechaViaje);
                Viaje.FkIdChofer = IdChofer;
                Viaje.Status = "PROGRAMADO";
                Viaje.InicioViaje = FechaViaje;
                Viaje.Nodo = Nodo_Usuario;
                Viaje.Save();

                //Guarda Auditoria
                TransporteTerrestre.Datos.TabAuditorias Auditoria = new TransporteTerrestre.Datos.TabAuditorias();
                Auditoria.Fecha = System.DateTime.Now;
                Auditoria.Ip = IP;
                Auditoria.IdTabla = Viaje.IdViaje;
                Auditoria.Ficha = Ficha;
                Auditoria.TipoMovimiento = "Insertar";
                Auditoria.Descripcion = "Se inserto un nuevo viaje con el Id: " + Viaje.IdViaje;
                Auditoria.Nodo = Nodo_Usuario;
                Auditoria.Save();

            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static void Insertar(int IdTransporte, int IdChofer, DateTime FechaViaje, string Ficha, string IP, bool Transaccionado, string Nodo_Usuario)
        {
            try
            {
                if (Transaccionado)
                {
                    using (TransactionScope Scope = new TransactionScope())
                    {
                        using (SubSonic.SharedDbConnectionScope sharedConnectionScope = new SubSonic.SharedDbConnectionScope())
                        {
                            Insertar(IdTransporte, IdChofer, FechaViaje,Ficha,IP,Nodo_Usuario);
                            Scope.Complete();
                        }
                    }
                }
                else
                {
                    Insertar(IdTransporte, IdChofer, FechaViaje,Ficha,IP,Nodo_Usuario);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static void Modificar(string IdViaje, int IdTransporte, int IdChofer, DateTime FechaViaje, string Ficha, string IP, string Nodo_Usuario)
        {
            try
            {
                TransporteTerrestre.Datos.TabViajes Viaje = TransporteTerrestre.Datos.TabViajes.FetchByID(Convert.ToInt32(IdViaje));
                Viaje.FkIdTransporte = IdTransporte;
                Viaje.FkIdChofer = IdChofer;
                Viaje.InicioViaje = FechaViaje;
                Viaje.Save();

                //Guarda Auditoria
                TransporteTerrestre.Datos.TabAuditorias Auditoria = new TransporteTerrestre.Datos.TabAuditorias();
                Auditoria.Fecha = System.DateTime.Now;
                Auditoria.Ip = IP;
                Auditoria.IdTabla = Viaje.IdViaje;
                Auditoria.Ficha = Ficha;
                Auditoria.TipoMovimiento = "Modificar";
                Auditoria.Descripcion = "Se modifico el viaje con el Id: " + Viaje.IdViaje;
                Auditoria.Nodo = Nodo_Usuario;
                Auditoria.Save();

            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static void InsertarGuardar(string IdViaje, int IdTransporte, int IdChofer, DateTime FechaViaje, string Ficha, string IP, string Nodo_Usuario)
        {
            try
            {
                if (IdViaje == "Nuevo")
                {
                    Insertar(IdTransporte,IdChofer,FechaViaje,Ficha,IP,Nodo_Usuario);
                }
                else
                {
                    Modificar(IdViaje,IdTransporte, IdChofer, FechaViaje,Ficha,IP, Nodo_Usuario);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static void InsertarGuardar(string IdViaje, int IdTransporte, int IdChofer, DateTime FechaViaje, string Ficha, string IP, bool Transaccionado, string Nodo_Usuario)
        {
            try
            {
                if (Transaccionado)
                {
                    using (TransactionScope Scope = new TransactionScope())
                    {
                        using (SubSonic.SharedDbConnectionScope sharedConnectionScope = new SubSonic.SharedDbConnectionScope())
                        {
                            InsertarGuardar(IdViaje, IdTransporte, IdChofer, FechaViaje,Ficha,IP,Nodo_Usuario);
                            Scope.Complete();
                        }
                    }
                }
                else
                {
                    InsertarGuardar(IdViaje, IdTransporte, IdChofer, FechaViaje,Ficha,IP, Nodo_Usuario);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static string GeneraFolio(System.DateTime Inicio)
        {
            try
            {
                string Folio = "";
                string PreFolio = String.Format("{0:ddMMyyyy}", System.DateTime.Today) + "-" + "V" + "-";
                System.DateTime AnioActual = new DateTime();
                AnioActual = AnioActual.AddYears(Inicio.Year-1);
                TransporteTerrestre.Datos.TabViajesCollection Viajes = new SubSonic.Select().From<TransporteTerrestre.Datos.TabViajes>().Where(TransporteTerrestre.Datos.TabViajes.FechaViajeColumn).IsGreaterThanOrEqualTo(AnioActual).ExecuteAsCollection<TransporteTerrestre.Datos.TabViajesCollection>();
                System.Collections.Generic.List<int> Folios = new List<int>();


                if (Viajes.Count < 1)
                {
                    Folio = PreFolio + "001";
                }
                else
                {
                    for (int i = 0; i < Viajes.Count; i++)
                    {
                        Folios.Add(Convert.ToInt32(Viajes[i].CveViaje.Substring(11, 3)));
                    }
                    Folios = (from x in Folios
                              orderby x descending
                              select x).ToList<int>();

                    //Viajes = Viajes.OrderByAsc(TransporteTerrestre.Datos.TabViajes.Columns.IdViaje);
                    int intFolio = 0;
                    intFolio = Folios[0] + 1;
                    string PosFolio = String.Format("{0:000}", intFolio);
                    Folio = PreFolio + PosFolio;
                }
                return Folio;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static void StatusViaje(int IdViaje, string Status, string Ficha, string IP, string Nodo_Usuario)
        {
            try
            {
                //IdRelViajesServicioss
                TransporteTerrestre.Datos.TabViajes Viaje = TransporteTerrestre.Datos.TabViajes.FetchByID(IdViaje);
                Viaje.Status = Status;
                Viaje.Save();
                //    TransporteTerrestre.Datos.RelViajesServiciosCollection ServiciosViaje = new SubSonic.Select().From<TransporteTerrestre.Datos.RelViajesServicios>().Where(TransporteTerrestre.Datos.RelViajesServicios.FkIdViajeColumn).IsEqualTo(IdViaje).ExecuteAsCollection<TransporteTerrestre.Datos.RelViajesServiciosCollection>();
                //    for (int i = 0; i < ServiciosViaje.Count; i++)
                //    {
                //        ServiciosViaje[i].BorradoLogico = true;
                //    }
                //    ServiciosViaje.SaveAll();
                //Guarda Auditoria
                TransporteTerrestre.Datos.TabAuditorias Auditoria = new TransporteTerrestre.Datos.TabAuditorias();
                Auditoria.Fecha = System.DateTime.Now;
                Auditoria.Ip = IP;
                Auditoria.IdTabla = Viaje.IdViaje;
                Auditoria.Ficha = Ficha;
                Auditoria.TipoMovimiento = "Eliminar";
                Auditoria.Descripcion = "Se eliminado el viaje con el Id: " + Viaje.IdViaje;
                Auditoria.Nodo = Nodo_Usuario;
                Auditoria.Save();
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static void StatusViaje(int IdViaje, string Status, string Ficha, string IP, bool Transaccionado, string Nodo_Usuario)
        {
            try
            {
                if (Transaccionado)
                {
                    using (TransactionScope Scope = new TransactionScope())
                    {
                        using (SubSonic.SharedDbConnectionScope sharedConnectionScope = new SubSonic.SharedDbConnectionScope())
                        {
                            StatusViaje(IdViaje,Status, Ficha,IP, Nodo_Usuario);
                            Scope.Complete();
                        }
                    }
                }
                else
                {
                    StatusViaje(IdViaje,Status,Ficha,IP, Nodo_Usuario);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static void AperturaCierreViaje(int IdViaje, System.DateTime Horario, string Operacion, string Ficha, string IP, string Nodo_Usuario)
        {
            try
            {
                TransporteTerrestre.Datos.TabViajes Viaje = TransporteTerrestre.Datos.TabViajes.FetchByID(IdViaje);
                if (Operacion == "INICIO VIAJE")
                {
                    Viaje.InicioViaje = Horario;
                    Viaje.Status = "PROGRESO";
                }
                else if(Operacion == "FIN VIAJE")
                {
                    Viaje.FinViaje = Horario;
                    Viaje.Status = "FINALIZADO";
                }
                Viaje.Save();

                //Guarda Auditoria
                TransporteTerrestre.Datos.TabAuditorias Auditoria = new TransporteTerrestre.Datos.TabAuditorias();
                Auditoria.Fecha = System.DateTime.Now;
                Auditoria.Ip = IP;
                Auditoria.IdTabla = Viaje.IdViaje;
                Auditoria.Ficha = Ficha;
                Auditoria.TipoMovimiento = "Modificar";
                Auditoria.Descripcion = "Se modifico el viaje con el Id: " + Viaje.IdViaje;
                Auditoria.Nodo = Nodo_Usuario;
                Auditoria.Save();
                
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static void AperturaCierreViaje(int IdViaje, System.DateTime Horario, string Operacion, string Ficha, string IP, bool Transaccionado, string Nodo_Usuario)
        {
            try
            {
                if (Transaccionado)
                {
                    using (TransactionScope Scope = new TransactionScope())
                    {
                        using (SubSonic.SharedDbConnectionScope sharedConnectionScope = new SubSonic.SharedDbConnectionScope())
                        {
                            AperturaCierreViaje(IdViaje, Horario, Operacion,Ficha,IP, Nodo_Usuario);
                            Scope.Complete();
                        }
                    }
                }
                else
                {
                    AperturaCierreViaje(IdViaje, Horario, Operacion,Ficha,IP, Nodo_Usuario);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

    }
}
