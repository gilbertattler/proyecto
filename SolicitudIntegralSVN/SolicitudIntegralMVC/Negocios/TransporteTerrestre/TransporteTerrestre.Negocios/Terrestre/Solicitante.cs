﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransporteTerrestre.Negocios.Terrestre
{
    public class Solicitante
    {
        public static GestionGL.Datos.VwDUsuariosGeneralCollection BuscaSolicitante(int IdUsuario)
        {
            try
            {
                SubSonic.DataProvider pvdGestionGL = TransporteTerrestre.Negocios.Utilerias.ObtenerProveedorDatosGestionGl();
                GestionGL.Datos.VwDUsuariosGeneralCollection Solicitante = new SubSonic.Select(pvdGestionGL).From<GestionGL.Datos.VwDUsuariosGeneral>().Where(GestionGL.Datos.VwDUsuariosGeneral.Columns.IdDUsuario).IsEqualTo(IdUsuario).ExecuteAsCollection<GestionGL.Datos.VwDUsuariosGeneralCollection>();
                return Solicitante;
            }
            catch (System.Exception ex)
            {
                throw ex;   	
            }
        }

        public static GestionGL.Datos.VwDUsuariosGeneral ValidaSolicitantexServicio(string Ficha, string Subdivision, int IdServicio)
        {
            try
            {
                SubSonic.DataProvider pvdGestionGL = TransporteTerrestre.Negocios.Utilerias.ObtenerProveedorDatosGestionGl();

                string FichaFormateada = "";
                if (Ficha.Length < 8)
                {
                    FichaFormateada = String.Format("{0:00000000}", Convert.ToInt32(Ficha));
                }
                else
                {
                    FichaFormateada = Ficha;
                }

                GestionGL.Datos.VwDUsuariosGeneralCollection Solicitante = new SubSonic.Select(pvdGestionGL).From<GestionGL.Datos.VwDUsuariosGeneral>().Where(GestionGL.Datos.VwDUsuariosGeneral.Columns.Ficha).IsEqualTo(FichaFormateada).And(GestionGL.Datos.VwDUsuariosGeneral.Columns.CveBajal).IsEqualTo("A").ExecuteAsCollection<GestionGL.Datos.VwDUsuariosGeneralCollection>();
                GestionGL.Datos.VwDUsuariosGeneral ValorRetorno = new GestionGL.Datos.VwDUsuariosGeneral();
                var Solicita = from S in Solicitante
                               where S.Subdivision.StartsWith(Subdivision.Substring(0,3))
                               select S;

                if (Solicita.Count<GestionGL.Datos.VwDUsuariosGeneral>() > 0)
                {
                    ValorRetorno = Solicita.First<GestionGL.Datos.VwDUsuariosGeneral>();
                }
                else
                {
                    SubSonic.DataProvider pvdSTPTerrestre = SubSonic.DataService.GetInstance("pvdSTPTerrestre");
                    TransporteTerrestre.Datos.CatServicios Servicio = TransporteTerrestre.Datos.CatServicios.FetchByID(IdServicio);
                    if (Servicio.Curso)
                    {
                        var SolicitaRH = from C in Solicitante
                                         where C.Subdivision == "61214"
                                         select C;
                        if (Solicita.Count<GestionGL.Datos.VwDUsuariosGeneral>() > 0)
                        {
                            ValorRetorno = SolicitaRH.First<GestionGL.Datos.VwDUsuariosGeneral>();
                        }
                        else
                        {
                            throw new Exception("El solicitante no pertenece a la misma subdivision del usuario y tampoco es de RH");
                        }
                    }
                    else
                    {
                        throw new Exception("El solicitante no pertenece a la misma subdivision del usuario");
                    }
                }
                return ValorRetorno;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
    }
}
