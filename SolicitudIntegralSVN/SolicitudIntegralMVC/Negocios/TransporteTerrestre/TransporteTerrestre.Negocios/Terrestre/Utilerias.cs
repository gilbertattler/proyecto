﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransporteTerrestre.Negocios.Terrestre
{
    public class Utilerias
    {
        public static string FormateaFicha(string Ficha)
        {
            try
            {
                string FichaFormateada = "";

                if (Ficha == "")
                {
                    throw new System.Exception("Falta la ficha.");
                }
                else
                {
                    if (Ficha.Length < 8)
                    {
                        FichaFormateada = String.Format("{0:00000000}", Convert.ToInt32(Ficha));
                    }
                    else
                    {
                        FichaFormateada = Ficha;
                    }
                }
                return FichaFormateada;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static string GenerarFolio(string Nodo_Usuario)
        {
            try
            {
                
                string Folio = "";
                TransporteTerrestre.Datos.TerrestreSolicitudCollection Solicitudes = new SubSonic.Select()
                    .From<TransporteTerrestre.Datos.TerrestreSolicitud>()
                    .Where(TransporteTerrestre.Datos.TerrestreSolicitud.NodoColumn)
                    .IsEqualTo(Nodo_Usuario)
                    .ExecuteAsCollection<TransporteTerrestre.Datos.TerrestreSolicitudCollection>();
                if (Solicitudes.Count<1)
                {
                    Folio = "1";
                }
                else                
                {
                    Solicitudes.OrderByAsc(TransporteTerrestre.Datos.TerrestreSolicitud.Columns.IdSolicitudTerrestre);
                    int intFolio = 0;
                    intFolio = Convert.ToInt32(Solicitudes[Solicitudes.Count - 1].Folio);
                    intFolio += 1;
                    Folio = intFolio.ToString();
                }
                return Folio;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static System.Data.DataSet CargarPasajeros(System.IO.Stream contenido_archivo_xml, bool excel, string exel_mime_type, string compania)
        {
            try
            {
                System.Data.DataSet dataset = null;
                System.Data.DataTable tabla = null;

                if (excel)
                {
                    Excel.IExcelDataReader excelReader = null;

                    //Excel.Factory.CreateReader(contenido_archivo_xml, exel_mime_type == "application/vnd.ms-excel" ? Excel.ExcelFileType.Binary : (exel_mime_type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" ? Excel.ExcelFileType.OpenXml : Excel.ExcelFileType.Binary));
                    if (exel_mime_type == "application/vnd.ms-excel")
                    {
                        excelReader = Excel.ExcelReaderFactory.CreateBinaryReader(contenido_archivo_xml);
                    }
                    else
                    {
                        if (exel_mime_type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                        {
                            excelReader = Excel.ExcelReaderFactory.CreateOpenXmlReader(contenido_archivo_xml);
                        }
                        else
                        {
                            throw new System.Exception("No es un formato valido para la carga de registros por archivo plano.");
                        }
                    }

                    // The result of each spreadsheet will be created in the result.Tables
                    dataset = excelReader.AsDataSet();
                    

                    // Liberar recursos. Free resources
                    excelReader.Close();

                    //The result of each spreadsheet was created in WorkbookData DataSet
                    tabla = dataset.Tables[0];

                    //Remover Encabezado
                    for (int i = 0; i < tabla.Columns.Count; ++i)
                    {
                        if (tabla.Rows[0][i].ToString() != "")
                        {
                            tabla.Columns[i].ColumnName = tabla.Rows[0][i].ToString();
                        }
                    }
                    tabla.Rows.Remove(tabla.Rows[0]);
                }
                else
                {
                    System.IO.StreamReader fileStreamUnicode = new System.IO.StreamReader(contenido_archivo_xml, System.Text.Encoding.UTF8);
                    try
                    {
                        dataset.ReadXml(fileStreamUnicode, System.Data.XmlReadMode.Auto);
                        tabla = dataset.Tables[0];
                    }
                    catch (System.Exception ex)
                    {
                        throw ex;
                    }
                }

                //Eliminando registros en blanco en la tabla
                if (tabla.Columns.Contains("FICHA"))
                {
                    List<System.Data.DataRow> registros_a_eliminar = new List<System.Data.DataRow>();

                    foreach (System.Data.DataRow registro in tabla.Rows)
                    {
                        if (registro["FICHA"].ToString() == "")
                        {
                            registros_a_eliminar.Add(registro);
                        }
                    }
                    foreach (System.Data.DataRow registro in registros_a_eliminar)
                    {
                        tabla.Rows.Remove(registro);
                    }
                }

                //Separar VIAJE y RETORNO
                System.Data.DataTable Viaje = tabla.Copy();
                Viaje.Clear();
                Viaje.TableName = "VIAJE";

                System.Data.DataTable Retorno = tabla.Copy();
                Retorno.Clear();
                Retorno.TableName = "RETORNO";

                foreach (System.Data.DataRow registro in tabla.Rows)
                {
                    if(registro["MOVIMIENTO"].ToString()=="VIAJE")
                    {
                        Viaje.Rows.Add(registro.ItemArray);
                    }
                    else if (registro["MOVIMIENTO"].ToString() == "RETORNO")
                    {
                        Retorno.Rows.Add(registro.ItemArray);
                    }
                    else if(registro["MOVIMIENTO"].ToString() == "AMBOS")
                    {
                        Viaje.Rows.Add(registro.ItemArray);
                        Retorno.Rows.Add(registro.ItemArray);
                    }
                }

                System.Data.DataSet dstResultado = new System.Data.DataSet();
                dstResultado.Tables.Add(Viaje);
                dstResultado.Tables.Add(Retorno);

                return dstResultado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool ValidaAnexo(string HoraInicio, string HoraFin, string HoraServicio, DateTime FechaServicio)
        {
            try
            {
                System.DateTime HorarioServicio = Convert.ToDateTime(HoraServicio);
                System.DateTime HorarioInicioServicio = Convert.ToDateTime(HoraInicio);
                System.DateTime HorarioFinServicio = Convert.ToDateTime(HoraFin);
                System.DateTime FechaHoraInicioServicio = FechaServicio.AddHours(HorarioInicioServicio.Hour);
                System.DateTime FechaHoraFinServicio = FechaServicio.AddHours(HorarioFinServicio.Hour);
                bool ValorRetorno = false;
                if (HorarioServicio.Hour >= 0 && HorarioServicio.Hour<=8)
                {
                    FechaHoraFinServicio = FechaHoraFinServicio.AddDays(-1);
                    FechaHoraInicioServicio = FechaHoraInicioServicio.AddDays(-1);
                }
                if (System.DateTime.Now > FechaHoraFinServicio)
                {
                    ValorRetorno = false;
                }
                else
                {
                    ValorRetorno = true;
                }
                return ValorRetorno;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
    }
}
