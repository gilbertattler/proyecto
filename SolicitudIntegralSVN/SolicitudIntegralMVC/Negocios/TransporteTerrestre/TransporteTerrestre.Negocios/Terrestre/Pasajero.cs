﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Transactions;

namespace TransporteTerrestre.Negocios.Terrestre
{
    public class Pasajero
    {
        public static TransporteTerrestre.Datos.TerrestrePasajeros ValidaPasajero(string FichaRFC)
        {
            try
            {
                TransporteTerrestre.Datos.TerrestrePasajeros Pasajero = null;
                Match regex_match = null;
                Regex regexObj = new Regex(@"(^(?<FICHA>(\d{6,8}))$)", RegexOptions.Multiline);
                regex_match = regexObj.Match(FichaRFC);

                if (regex_match.Success)
                {
                    Pasajero = ValidaPasajeroPemex(FichaRFC);
                }
                else
                {
                    Pasajero = ValidaPasajeroCompania(FichaRFC);
                }
                return Pasajero;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static TransporteTerrestre.Datos.TerrestrePasajeros ValidaPasajeroExcel(string FichaRFC)
        {
            TransporteTerrestre.Datos.TerrestrePasajeros Pasajero = null;
            try
            {
                Match regex_match = null;
                Regex regexObj = new Regex(@"(^(?<FICHA>(\d{6,8}))$)", RegexOptions.Multiline);
                regex_match = regexObj.Match(FichaRFC);

                if (regex_match.Success)
                {
                    Pasajero = ValidaPasajeroPemex(FichaRFC);
                }
                else
                {
                   Pasajero = ValidaPasajeroCompania(FichaRFC);
                }
                return Pasajero;
            }
            catch (System.Exception ex)
            {
                if (ex.Message == "El empleado no esta dado de alta en el catalogo de personal")
                {
                    Pasajero = null;
                    return Pasajero;
                }
                else
                {
                    throw ex;
                }
            }
        }

        public static TransporteTerrestre.Datos.TerrestrePasajeros ValidaPasajeroPemex(string Ficha)
        {
            try
            {
                string FichaFormateada = "";
                if (Ficha.Length < 8)
                {
                    FichaFormateada = String.Format("{0:00000000}", Convert.ToInt32(Ficha));
                }
                else
                {
                    FichaFormateada = Ficha;
                }

                //SubSonic.DataProvider pvdGestionGL = TransporteTerrestre.Negocios.Utilerias.ObtenerProveedorDatosGestionGl();
                TransporteTerrestre.Datos.VwGestionGLCEmpleadosPEMEXCollection Empleado = new SubSonic.Select().From<TransporteTerrestre.Datos.VwGestionGLCEmpleadosPEMEX>().Where(TransporteTerrestre.Datos.VwGestionGLCEmpleadosPEMEX.Columns.Ficha).IsEqualTo(FichaFormateada).ExecuteAsCollection<TransporteTerrestre.Datos.VwGestionGLCEmpleadosPEMEXCollection>();
                
                
                TransporteTerrestre.Datos.TerrestrePasajeros Pasajero = new TransporteTerrestre.Datos.TerrestrePasajeros();

                if (Empleado.Count > 0)
                {
                    Pasajero.Ficha = Empleado[0].Ficha;
                    Pasajero.Nombre = Empleado[0].Nombres + Empleado[0].Apellidos;
                    Pasajero.Tipo = "PEMEX";
                    Pasajero.BorradoLogico = false;
                    Pasajero.Status = "SOLICITADO";
                } 
                else
                {
                    throw new Exception("El empleado no esta dado de alta en el catalogo de personal");
                }
                return Pasajero;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static TransporteTerrestre.Datos.TerrestrePasajeros ValidaPasajeroCompania(string RFC)
        {

            try
            {
                string FichaFormateada = "";
                FichaFormateada = RFC;

                TransporteTerrestre.Datos.VwGestionGLCEmpleadosCIACollection Empleado = new SubSonic.Select().From<TransporteTerrestre.Datos.VwGestionGLCEmpleadosCIA>().Where(TransporteTerrestre.Datos.VwGestionGLCEmpleadosCIA.Columns.RfcFM3).ContainsString(FichaFormateada).ExecuteAsCollection<TransporteTerrestre.Datos.VwGestionGLCEmpleadosCIACollection>();

                TransporteTerrestre.Datos.TerrestrePasajeros Pasajero = new TransporteTerrestre.Datos.TerrestrePasajeros();
                if (Empleado.Count > 0)
                {
                    Pasajero.Ficha = Empleado[0].RfcFM3;
                    Pasajero.Nombre = Empleado[0].Nombres + Empleado[0].Apellidos;
                    Pasajero.Tipo = "COMPAÑIA";
                    Pasajero.BorradoLogico = false;
                    Pasajero.Status = "SOLICITADO";
                }
                else
                {
                    throw new Exception("El empleado no esta dado de alta en el catalogo de personal");
                }
                return Pasajero;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static int InsertaPasajero(TransporteTerrestre.Datos.TerrestrePasajeros Pasajero)
        {
            try
            {
                Pasajero.BorradoLogico = false;
                Pasajero.Save();
                int ValorRetorno = Pasajero.IdPasajero;
                return ValorRetorno;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static void InsertaPasajeros(int IdSolicitud, string Movimiento, System.Collections.Generic.List<KeyValuePair<int, string>> PasajerosInsertar, string Nodo_Usuario)
        {
            try
            {
                TransporteTerrestre.Datos.TerrestrePasajerosCollection NuevosPasajeros = new TransporteTerrestre.Datos.TerrestrePasajerosCollection();
                TransporteTerrestre.Datos.TerrestrePasajeros NuevoPasajero = null;
                for (int i=0; i<PasajerosInsertar.Count; i++)
                {
                    TransporteTerrestre.Datos.TerrestreSolicitudDiaCollection SolicitudDia = new SubSonic.Select()
                        .From<TransporteTerrestre.Datos.TerrestreSolicitudDia>()
                        .Where(TransporteTerrestre.Datos.TerrestreSolicitudDia.FkSolicitudTerrestreColumn)
                        .IsEqualTo(IdSolicitud)
                        .And(TransporteTerrestre.Datos.TerrestreSolicitudDia.MovimientoColumn)
                        .IsEqualTo(Movimiento)
                        .And(TransporteTerrestre.Datos.TerrestreSolicitudDia.CveDiaColumn)
                        .IsEqualTo(PasajerosInsertar[i].Key)
                        .And(TransporteTerrestre.Datos.TerrestreSolicitudDia.NodoColumn)
                        .IsEqualTo(Nodo_Usuario)
                        .ExecuteAsCollection<TransporteTerrestre.Datos.TerrestreSolicitudDiaCollection>();

                    NuevoPasajero = new TransporteTerrestre.Datos.TerrestrePasajeros();
                    NuevoPasajero = ValidaPasajero(PasajerosInsertar[i].Value);
                    NuevoPasajero.FkSolicitudDia = SolicitudDia[0].IdSolicitudDia;
                    //nueva linea 25/07/2012
                    NuevoPasajero.FkIdSolicitud = IdSolicitud;
                    NuevoPasajero.Nodo = Nodo_Usuario;
                    //termina nueva linea 25/07/2012
                    NuevosPasajeros.Add(NuevoPasajero);
                }
                NuevosPasajeros.SaveAll();
            }
            catch (System.Exception ex)
            {
                throw ex;	
            }
        }

        public static void EliminaPasajeros(int IdSolicitud, string Movimiento, System.Collections.Generic.List<KeyValuePair<int, string>> PasajerosEliminar, string Nodo_Usuario)
        {
            try
            {
                TransporteTerrestre.Datos.TerrestrePasajeros PasajeroBaja = null;
                TransporteTerrestre.Datos.TerrestrePasajerosCollection PasajerosBaja = new TransporteTerrestre.Datos.TerrestrePasajerosCollection();
                for (int i = 0; i < PasajerosEliminar.Count; i++)
                {
                    TransporteTerrestre.Datos.VWTerrestreSolicitudDiaPasajerosCollection BuscaPasajeroBaja = new SubSonic.Select()
                        .From<TransporteTerrestre.Datos.VWTerrestreSolicitudDiaPasajeros>()
                        .Where(TransporteTerrestre.Datos.VWTerrestreSolicitudDiaPasajeros.Columns.FkSolicitudTerrestre)
                        .IsEqualTo(IdSolicitud)
                        .And(TransporteTerrestre.Datos.VWTerrestreSolicitudDiaPasajeros.Columns.CveDia)
                        .IsEqualTo(PasajerosEliminar[i].Key)
                        .And(TransporteTerrestre.Datos.VWTerrestreSolicitudDiaPasajeros.Columns.Movimiento)
                        .IsEqualTo(Movimiento)
                        .And(TransporteTerrestre.Datos.VWTerrestreSolicitudDiaPasajeros.Columns.Ficha)
                        .IsEqualTo(PasajerosEliminar[i].Value)
                        .And(TransporteTerrestre.Datos.VWTerrestreSolicitudDiaPasajeros.Columns.Nodo)
                        .IsEqualTo(Nodo_Usuario)
                        .ExecuteAsCollection<TransporteTerrestre.Datos.VWTerrestreSolicitudDiaPasajerosCollection>();

                    PasajeroBaja = TransporteTerrestre.Datos.TerrestrePasajeros.FetchByID(BuscaPasajeroBaja[0].IdPasajero);
                    PasajeroBaja.BorradoLogico = true;
                    PasajeroBaja.Status = "ELIMINADO";
                    PasajeroBaja.Nodo = Nodo_Usuario;
                    PasajerosBaja.Add(PasajeroBaja);
                }
                PasajerosBaja.SaveAll();
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static TransporteTerrestre.Datos.TerrestrePasajeros ValidaPasajero(string Ficha, bool Transaccionado)
        {
            try
            {
                TransporteTerrestre.Datos.TerrestrePasajeros Pasajero = null;
                if(Transaccionado)
                {
                    SubSonic.DataProvider pvdGestionGL = TransporteTerrestre.Negocios.Utilerias.ObtenerProveedorDatosGestionGl();
                    using (TransactionScope ScopeGL = new TransactionScope())
                    {
                        using (SubSonic.SharedDbConnectionScope sharedConnectionScopeGL = new SubSonic.SharedDbConnectionScope(pvdGestionGL))
                        {
                            Pasajero = ValidaPasajero(Ficha);                            
                        }
                    }
                }
                else
                {
                    Pasajero = ValidaPasajero(Ficha);
                }
                return Pasajero;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static void ModificaStatusPasajerosPorSol(int IdSolicitud, string Estado)
        {
            try
            {
                TransporteTerrestre.Datos.TerrestrePasajerosCollection Pasajeros = new SubSonic.Select().From<TransporteTerrestre.Datos.TerrestrePasajeros>().Where(TransporteTerrestre.Datos.TerrestrePasajeros.FkIdSolicitudColumn).IsEqualTo(IdSolicitud).ExecuteAsCollection<TransporteTerrestre.Datos.TerrestrePasajerosCollection>();
                for (int i = 0; i < Pasajeros.Count; i++)
                {
                    Pasajeros[0].Status = Estado;
                }
            }
            catch (System.Exception ex)
            {
            	throw ex;
            }
        }

        public static void ProgramarPasajeros(System.Collections.Generic.List<int> Pasajeros, int? IdViaje, string Status)
        {
            try
            {
                TransporteTerrestre.Datos.TerrestrePasajerosCollection PasajerosProgramar = new TransporteTerrestre.Datos.TerrestrePasajerosCollection();
                TransporteTerrestre.Datos.TerrestrePasajeros Pasajero = null;
                for (int i=0; i<Pasajeros.Count; i++)
                {
                    Pasajero = TransporteTerrestre.Datos.TerrestrePasajeros.FetchByID(Pasajeros[i]);
                    Pasajero.Status = Status;
                    Pasajero.FkIdViaje = IdViaje;
                    PasajerosProgramar.Add(Pasajero);
                }
                PasajerosProgramar.SaveAll();
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static void ProgramarPasajeros(System.Collections.Generic.List<int> Pasajeros, int? IdViaje, string Status,  bool Transaccionado)
        {
            try
            {
                SubSonic.DataProvider pvdTransporteTerrestre = SubSonic.DataService.GetInstance("pvdSTPTerrestre");
                if (Transaccionado)
                {
                    using (TransactionScope Scope = new TransactionScope())
                    {
                        using (SubSonic.SharedDbConnectionScope sharedConnectionScope = new SubSonic.SharedDbConnectionScope(pvdTransporteTerrestre))
                        {
                            ProgramarPasajeros(Pasajeros, IdViaje, Status);
                            Scope.Complete();
                        }
                    }
                }
                else
                {
                    ProgramarPasajeros(Pasajeros, IdViaje, Status);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static int ModificaStatusPasajeros(System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<int, string>> ListaPasajeros)
        {
            try
            {
                TransporteTerrestre.Datos.TerrestrePasajerosCollection PasajerosProgramar = new TransporteTerrestre.Datos.TerrestrePasajerosCollection();
                TransporteTerrestre.Datos.TerrestrePasajeros Pasajero = null;
                for (int i = 0; i < ListaPasajeros.Count; i++)
                {
                    Pasajero = TransporteTerrestre.Datos.TerrestrePasajeros.FetchByID(ListaPasajeros[i].Key);
                    if (ListaPasajeros[i].Value == "ABORDO")
                    {
                        Pasajero.Status = "EN DESTINO";
                    }
                    else if (ListaPasajeros[i].Value == "PROGRAMADO")
                    {
                        Pasajero.Status = "ABORDO";
                    }
                    PasajerosProgramar.Add(Pasajero);
                }
                PasajerosProgramar.SaveAll();
                return PasajerosProgramar.Count;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static int ModificaStatusPasajeros(System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<int, string>> ListaPasajeros, bool Transaccionado)
        {
            try
            {
                SubSonic.DataProvider pvdTransporteTerrestre = SubSonic.DataService.GetInstance("pvdSTPTerrestre");
                int ValorRetorno = 0;
                if (Transaccionado)
                {
                    using (TransactionScope Scope = new TransactionScope())
                    {
                        using (SubSonic.SharedDbConnectionScope sharedConnectionScope = new SubSonic.SharedDbConnectionScope(pvdTransporteTerrestre))
                        {
                            ValorRetorno = ModificaStatusPasajeros(ListaPasajeros);
                            Scope.Complete();
                        }
                    }
                }
                else
                {
                    ValorRetorno = ModificaStatusPasajeros(ListaPasajeros);
                }
                return ValorRetorno;
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }
        }

        public static void CobroServicios(System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<int, int>> ListaPasajeros, System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<int, int>> ListaPasajerosDescobro, string Nodo_Usuario)
        {
            try
            {
                TransporteTerrestre.Datos.TerrestrePasajerosCollection Pasajeros = null;
                SubSonic.DataProvider pvdTransporteTerrestre = SubSonic.DataService.GetInstance("pvdSTPTerrestre");

                for (int i = 0; i < ListaPasajeros.Count; i++)
                {
                    Pasajeros = new SubSonic.Select(pvdTransporteTerrestre)
                        .From<TransporteTerrestre.Datos.TerrestrePasajeros>()
                        .Where(TransporteTerrestre.Datos.TerrestrePasajeros.FkIdViajeColumn)
                        .IsEqualTo(ListaPasajeros[i].Key)
                        .And(TransporteTerrestre.Datos.TerrestrePasajeros.FkIdSolicitudColumn)
                        .IsEqualTo(ListaPasajeros[i].Value)
                        .And(TransporteTerrestre.Datos.TerrestrePasajeros.NodoColumn)
                        .IsEqualTo(Nodo_Usuario)
                        .ExecuteAsCollection<TransporteTerrestre.Datos.TerrestrePasajerosCollection>();

                    for (int j = 0; j < Pasajeros.Count; j++)
                    {
                        Pasajeros[j].Cobro = true;
                    }
                    Pasajeros.SaveAll();
                }                
                for (int i = 0; i < ListaPasajerosDescobro.Count; i++)
                {
                    Pasajeros = new SubSonic.Select(pvdTransporteTerrestre).
                        From<TransporteTerrestre.Datos.TerrestrePasajeros>()
                        .Where(TransporteTerrestre.Datos.TerrestrePasajeros.FkIdViajeColumn)
                        .IsEqualTo(ListaPasajerosDescobro[i].Key)
                        .And(TransporteTerrestre.Datos.TerrestrePasajeros.FkIdSolicitudColumn)
                        .IsEqualTo(ListaPasajerosDescobro[i].Value)
                        .And(TransporteTerrestre.Datos.TerrestrePasajeros.NodoColumn)
                        .IsEqualTo(Nodo_Usuario)
                        .ExecuteAsCollection<TransporteTerrestre.Datos.TerrestrePasajerosCollection>();

                    for (int j = 0; j < Pasajeros.Count; j++)
                    {
                        Pasajeros[j].Cobro = false;
                    }
                    Pasajeros.SaveAll();
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static void CobroServicios(System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<int, int>> ListaPasajeros, System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<int, int>> ListaPasajerosDescobro, bool Transaccionado, string Nodo_Usuario)
        {
            try
            {
                SubSonic.DataProvider pvdTransporteTerrestre = SubSonic.DataService.GetInstance("pvdSTPTerrestre");
                
                if (Transaccionado)
                {
                    using (TransactionScope Scope = new TransactionScope())
                    {
                        using (SubSonic.SharedDbConnectionScope sharedConnectionScope = new SubSonic.SharedDbConnectionScope(pvdTransporteTerrestre))
                        {
                            CobroServicios(ListaPasajeros, ListaPasajerosDescobro,Nodo_Usuario);
                            Scope.Complete();
                        }
                    }
                }
                else
                {
                    CobroServicios(ListaPasajeros, ListaPasajerosDescobro,Nodo_Usuario);
                }                
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }
        }

        public static void ModificaStatusPasajeros(System.Collections.Generic.List<int> Pasajeros, string Status)
        {
            try
            {
                TransporteTerrestre.Datos.TerrestrePasajerosCollection PasajerosProgramar = new TransporteTerrestre.Datos.TerrestrePasajerosCollection();
                TransporteTerrestre.Datos.TerrestrePasajeros Pasajero = null;
                for (int i = 0; i < Pasajeros.Count; i++)
                {
                    Pasajero = TransporteTerrestre.Datos.TerrestrePasajeros.FetchByID(Pasajeros[i]);
                    Pasajero.Status = Status;
                    //Pasajero.FkIdViaje = IdViaje;
                    PasajerosProgramar.Add(Pasajero);
                }
                PasajerosProgramar.SaveAll();
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static void ModificaStatusPasajeros(System.Collections.Generic.List<int> Pasajeros, string Status, bool Transaccionado)
        {
            try
            {
                SubSonic.DataProvider pvdTransporteTerrestre = SubSonic.DataService.GetInstance("pvdSTPTerrestre");
                if (Transaccionado)
                {
                    using (TransactionScope Scope = new TransactionScope())
                    {
                        using (SubSonic.SharedDbConnectionScope sharedConnectionScope = new SubSonic.SharedDbConnectionScope(pvdTransporteTerrestre))
                        {
                            ModificaStatusPasajeros(Pasajeros, Status);
                            Scope.Complete();
                        }
                    }
                }
                else
                {
                    ModificaStatusPasajeros(Pasajeros, Status);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
    }
}
