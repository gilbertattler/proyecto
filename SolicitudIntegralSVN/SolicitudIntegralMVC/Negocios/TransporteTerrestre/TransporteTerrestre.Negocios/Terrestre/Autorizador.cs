﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransporteTerrestre.Negocios.Terrestre
{
    public class Autorizador
    {
        public static GestionGL.Datos.VwCDAutorizadores ValidaAutorizador(string Ficha, string Subdivision, string AreaServicio, System.DateTime Inicio, System.DateTime Fin)
        {
            try
            {
                SubSonic.DataProvider pvdGestionGL = TransporteTerrestre.Negocios.Utilerias.ObtenerProveedorDatosGestionGl();

                string FichaFormateada = "";
                if (Ficha.Length < 8)
                {
                    FichaFormateada = String.Format("{0:00000000}", Convert.ToInt32(Ficha));
                }
                else
                {
                    FichaFormateada = Ficha;
                }

                GestionGL.Datos.VwCDAutorizadoresCollection Autorizadores = new SubSonic.Select(pvdGestionGL)
                .From<GestionGL.Datos.VwCDAutorizadores>()
                .Where(GestionGL.Datos.VwCDAutorizadores.Columns.Ficha)
                .IsEqualTo(FichaFormateada)
                .And(GestionGL.Datos.VwDUsuariosGeneral.Columns.Subdivision)
                .IsEqualTo(Subdivision)
                .And(GestionGL.Datos.VwCDAutorizadores.Columns.CveAreaServicio)
                .IsEqualTo(AreaServicio)
                .And(GestionGL.Datos.VwCDAutorizadores.Columns.FechaInicio)
                .IsLessThanOrEqualTo(Inicio)
                .And(GestionGL.Datos.VwCDAutorizadores.Columns.FechaFinal)
                .IsGreaterThanOrEqualTo(Fin)
                .ExecuteAsCollection<GestionGL.Datos.VwCDAutorizadoresCollection>();

                GestionGL.Datos.VwCDAutorizadores ValorRetorno = new GestionGL.Datos.VwCDAutorizadores();

                if (Autorizadores.Count > 0)
                {
                    ValorRetorno = Autorizadores[0].Clone();
                }
                else
                {
                    throw new Exception("El autorizador no cuenta con permisos vigentes para esta subdivisión en el rango de fechas seleccionado");                    
                }

                return ValorRetorno;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
        
    }
}
