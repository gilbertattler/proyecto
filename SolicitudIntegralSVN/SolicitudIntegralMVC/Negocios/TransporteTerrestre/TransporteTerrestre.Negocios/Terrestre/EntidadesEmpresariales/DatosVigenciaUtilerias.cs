﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales
{
    public class DatosVigenciaUtilerias
    {

        private System.DateTime _VigenciaFechaInicio;
        private System.DateTime _VigenciaFechaFin;
        private string _HoraServicio;
        private string _TipoServicio;
        private string _Folio;
        private string _HoraInicioCandado;
        private string _HoraFinCandado;
        private string _EstadoSolicitud;
        private string _OperacionRealizar;
        private int _IdFolioSolicitud;
        private int _IdAutorizador;

        public int IdFolioSolicitud { get { return _IdFolioSolicitud; } set { _IdFolioSolicitud = value; } }
        public int IdAutorizador { get { return _IdAutorizador; } set { _IdAutorizador = value; } }
        
        public string HoraServicio
        {
            get
            {
                return _HoraServicio;
            }
            set
            {
                _HoraServicio = value;
            }
        }
        public string TipoServicio
        {
            get
            {
                return _TipoServicio;
            }
            set
            {
                _TipoServicio = value;
            }
        }
        public System.DateTime VigenciaFechaInicio
        {
            get
            {
                return _VigenciaFechaInicio;
            }
            set
            {
                _VigenciaFechaInicio = value;
            }
        }
        public System.DateTime VigenciaFechaFin
        {
            get
            {
                return _VigenciaFechaFin;
            }
            set
            {
                _VigenciaFechaFin = value;
            }
        }
        public string Folio
        {
            get
            {
                return _Folio;
            }
            set
            {
                _Folio = value;
            }
        }
        public string HoraInicioCandado
        {
            get
            {
                return _HoraInicioCandado;
            }
            set
            {
                _HoraInicioCandado = value;
            }
        }
        public string HoraFinCandado
        {
            get
            {
                return _HoraFinCandado;
            }
            set
            {
                _HoraFinCandado = value;
            }
        }
        public string EstadoSolicitud
        {
            get
            {
                return _EstadoSolicitud;
            }
            set
            {
                _EstadoSolicitud = value;
            }
        }
        public string OperacionRealizar
        {
            get
            {
                return _OperacionRealizar;
            }
            set
            {
                _OperacionRealizar = value;
            }
        }

        public string Origen { get; set; }
        public string Destino { get; set; }


        public DatosVigenciaUtilerias()
        {

        }
        public DatosVigenciaUtilerias(string horaServicio, string tipoServicio, System.DateTime vigenciaFechaInicio, System.DateTime vigenciaFin, string horaInicioCandado, string horaFinCandado, string estadoSolicitud, string operacionRealizar)
        {
            try
            {
                HoraServicio = horaServicio;
                TipoServicio = tipoServicio;
                VigenciaFechaInicio = vigenciaFechaInicio;
                VigenciaFechaFin = vigenciaFin;
                HoraInicioCandado = horaInicioCandado;
                HoraFinCandado = horaFinCandado;
                OperacionRealizar = operacionRealizar;
                EstadoSolicitud = estadoSolicitud;

            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }
        public DatosVigenciaUtilerias(string folio)
        {
            try
            {
                Folio = folio;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }
        public DatosVigenciaUtilerias(string estadoSolicitud, string operacionRealizar)
        {
            try
            {
                OperacionRealizar = operacionRealizar;
                EstadoSolicitud = estadoSolicitud;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
    }
    
}
