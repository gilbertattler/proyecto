﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales
{
    public class PasajerosDia
    {
        private System.Collections.Generic.List<PasajeroMinimo> _Pasajeros;
        private int _CveDia;
        private System.DateTime _FechaDia;
        private System.DateTime _InicioServicio;
        private System.DateTime _FinServicio;
        private string _Observaciones;


        public System.Collections.Generic.List<PasajeroMinimo> Pasajeros
        {
            get
            {
                return _Pasajeros;
            }
        }

        public int CveDia
        {
            get
            {
                return _CveDia;
            }
            set
            {
                _CveDia = value;
            }
        }
        public System.DateTime FechaDia
        {
            get
            {
                return _FechaDia;
            }
            set
            {
                _FechaDia = value;
            }
        }

        public System.DateTime InicioServicio
        {
            get
            {
                return _InicioServicio;
            }
            set
            {
                _InicioServicio = value;
            }
        }

        public System.DateTime FinServicio
        {
            get
            {
                return _FinServicio;
            }
            set
            {
                _FinServicio = value;
            }
        }

        public string Observaciones
        {
            get
            {
                return _Observaciones;
            }
            set
            {
                _Observaciones = value;
            }
        }

        public PasajerosDia()
        {
            _Pasajeros = new List<PasajeroMinimo>();
        }

        public PasajerosDia(int Dia, System.DateTime fechadia, PasajeroMinimo Pasajero, System.DateTime inicioservicio, System.DateTime finservicio, string observaciones)
        {
            _Pasajeros = new List<PasajeroMinimo>();
            CveDia = Dia;
            InicioServicio = inicioservicio;
            FinServicio = finservicio;
            Observaciones = observaciones;
            FechaDia = fechadia;
            if (Pasajero != null)
                _Pasajeros.Add(Pasajero);
        }

        public PasajerosDia(int Dia, System.DateTime fechadia, System.Collections.Generic.List<PasajeroMinimo> Pasajeros, System.DateTime inicioservicio, System.DateTime finservicio, string observaciones)
        {
            CveDia = Dia;
            InicioServicio = inicioservicio;
            FinServicio = finservicio;
            Observaciones = observaciones;
            FechaDia = fechadia;
            _Pasajeros = new List<PasajeroMinimo>();
            PasajeroMinimo Pasajero = null;
            for (int i = 0; i < Pasajeros.Count; i++)
            {
                Pasajero = new PasajeroMinimo();
                Pasajero.Ficha = Pasajeros[i].Ficha;
                Pasajero.Nombre = Pasajeros[i].Nombre;
                Pasajero.Tipo = Pasajeros[i].Tipo;
                Pasajero.SitCont = Pasajeros[i].SitCont;
                Pasajero.Nivel = Pasajeros[i].Nivel;
                Pasajero.Instalacion = Pasajeros[i].Instalacion;
                Pasajero.Plaza = Pasajeros[i].Plaza;
                Pasajero.Rol = Pasajeros[i].Rol;
                Pasajero.Subgerencia = Pasajeros[i].Subgerencia;
                Pasajero.Gerencia = Pasajeros[i].Gerencia;
                Pasajero.Categoria = Pasajeros[i].Categoria;
                _Pasajeros.Add(Pasajero);
            }
        }

        public void Eliminar(string Ficha)
        {
            try
            {
                for (int i = 0; i < _Pasajeros.Count; i++)
                {
                    if (_Pasajeros[i].Ficha == Ficha)
                    {
                        _Pasajeros.RemoveAt(i);
                        break;
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public void AgregarPasajero(PasajeroMinimo NuevoPasajero)
        {
            try
            {
                _Pasajeros.Add(NuevoPasajero);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }  
    }
}
