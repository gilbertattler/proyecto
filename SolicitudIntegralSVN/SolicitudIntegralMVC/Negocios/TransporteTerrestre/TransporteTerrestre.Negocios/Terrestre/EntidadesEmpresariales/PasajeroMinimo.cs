﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales
{
    public class PasajeroMinimo
    {
        private string _Ficha;
        private string _Nombre;
        private string _Tipo;
        private string _SitCont;
        private string _Nivel;
        private string _Categoria;
        private string _Gerencia;
        private string _Instalacion;
        private string _Rol;
        private string _Plaza;
        private string _Subgerencia;
        private string _Clave;
        private string _Curso;


        public string Ficha
        {
            get
            {
                return _Ficha;
            }
            set
            {
                _Ficha = value;
            }
        }
        public string Nombre
        {
            get
            {
                return _Nombre;
            }
            set
            {
                _Nombre = value;
            }
        }
        public string Tipo
        {
            get
            {
                return _Tipo;
            }
            set
            {
                _Tipo = value;
            }
        }
        public string SitCont
        {
            get
            {
                return _SitCont;
            }
            set
            {
                _SitCont = value;
            }
        }
        public string Nivel
        {
            get
            {
                return _Nivel;
            }
            set
            {
                _Nivel = value;
            }
        }
        public string Categoria
        {
            get
            {
                return _Categoria;
            }
            set
            {
                _Categoria = value;
            }
        }
        public string Gerencia
        {
            get
            {
                return _Gerencia;
            }
            set
            {
                _Gerencia = value;
            }
        }
        public string Instalacion
        {
            get
            {
                return _Instalacion;
            }
            set
            {
                _Instalacion = value;
            }
        }
        public string Rol
        {
            get
            {
                return _Rol;
            }
            set
            {
                _Rol = value;
            }
        }
        public string Plaza
        {
            get
            {
                return _Plaza;
            }
            set
            {
                _Plaza = value;
            }
        }
        public string Subgerencia
        {
            get
            {
                return _Subgerencia;
            }
            set
            {
                _Subgerencia = value;
            }
        }
        public string Clave
        {
            get
            {
                return _Clave;
            }
            set
            {
                _Clave = value;
            }
        }
        public string Curso
        {
            get
            {
                return _Curso;
            }
            set
            {
                _Curso = value;
            }
        }


        public PasajeroMinimo()
        {

        }

        public PasajeroMinimo(string ficha, string nombre, string tipo)
        {
            if (tipo == "PEMEX")
            {
                if (ficha.Length < 8)
                {
                    Ficha = String.Format("{0:00000000}", Convert.ToInt32(ficha));
                }
                else
                {
                    Ficha = ficha;
                }
            }
            Nombre = nombre;
            Tipo = tipo;
        }

        public PasajeroMinimo(string ficha, string nombre, string tipo, string sitCont, string nivel, string categoria, string gerencia, string instalacion, string rol, string plaza, string subgerencia, string clave, string curso)
        {
            if (tipo == "PEMEX")
            {
                if (ficha.Length < 8)
                {
                    Ficha = String.Format("{0:00000000}", Convert.ToInt32(ficha));
                }
                else
                {
                    Ficha = ficha;
                }
            }
            Nombre = nombre;
            Tipo = tipo;
            SitCont = sitCont;
            Nivel = nivel;
            Categoria = categoria;
            Gerencia = gerencia;
            Instalacion = instalacion;
            Rol = rol;
            Plaza = plaza;
            Subgerencia = subgerencia;
            Clave = clave;
            Curso = curso;
        }
    }
}
