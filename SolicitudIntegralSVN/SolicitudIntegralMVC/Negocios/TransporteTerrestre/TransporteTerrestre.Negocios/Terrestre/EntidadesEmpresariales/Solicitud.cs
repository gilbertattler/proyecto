﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales
{
    public class Solicitud
    {
        private int _IdSolicitudTerrestre;
        private int _FkIdServicio;
        private int _FkIdSolicitante;
        private int _FkIdAutorizador;
        private int _FkIdDireccionamiento;
        private string _CveServicio;
        private string _FichaSolicitante;
        private string _Solicitante;
        private string _FichaAutorizador;
        private string _Autorizador;
        private string _Direccionamiento;
        private string _Folio;
        private System.DateTime _InicioVigencia;
        private System.DateTime _FinVigencia;
        private string _Status;
        private string _Observaciones;

        public int IdSolicitudTerrestre
        {
            get
            {
                return _IdSolicitudTerrestre;
            }
            set
            {
                _IdSolicitudTerrestre = value;
            }
        }
        public int FkIdServicio
        {
            get
            {
                return _FkIdServicio;
            }
            set
            {
                _FkIdServicio = value;
            }
        }
        public int FkIdSolicitante
        {
            get
            {
                return _FkIdSolicitante;
            }
            set
            {
                _FkIdSolicitante = value;
            }
        }
        public int FkIdAutorizador
        {
            get
            {
                return _FkIdAutorizador;
            }
            set
            {
                _FkIdAutorizador = value;
            }
        }
        public int FkIdDireccionamiento
        {
            get
            {
                return _FkIdDireccionamiento;
            }
            set
            {
                _FkIdDireccionamiento = value;
            }
        }
        public string CveServicio
        {
            get
            {
                return _CveServicio;
            }
            set
            {
                _CveServicio = value;
            }
        }
        public string FichaSolicitante
        {
            get
            {
                return _FichaSolicitante;
            }
            set
            {
                _FichaSolicitante = value;
            }
        }
        public string Solicitante
        {
            get
            {
                return _Solicitante;
            }
            set
            {
                _Solicitante = value;
            }
        }
        public string FichaAutorizador
        {
            get
            {
                return _FichaAutorizador;
            }
            set
            {
                _FichaAutorizador = value;
            }
        }
        public string Autorizador
        {
            get
            {
                return _Autorizador;
            }
            set
            {
                _Autorizador = value;
            }
        }
        public string Direccionamiento
        {
            get
            {
                return _Direccionamiento;
            }
            set
            {
                _Direccionamiento = value;
            }
        }
        public string Folio
        {
            get
            {
                return _Folio;
            }
            set
            {
                _Folio = value;
            }
        }
        public System.DateTime InicioVigencia
        {
            get
            {
                return _InicioVigencia;
            }
            set
            {
                _InicioVigencia = value;
            }
        }
        public System.DateTime FinVigencia
        {
            get
            {
                return _FinVigencia;
            }
            set
            {
                _FinVigencia = value;
            }
        }
        public string Status
        {
            get
            {
                return _Status;
            }
            set
            {
                _Status = value;
            }
        }
        public string Observaciones
        {
            get
            {
                return _Observaciones;
            }
            set
            {
                _Observaciones = value;
            }
        }
        public Solicitud()
        {

        }
        public Solicitud(int idSolicitudTerrestre)
        {
            try
            {
                SubSonic.DataProvider pvdGestionGL = TransporteTerrestre.Negocios.Utilerias.ObtenerProveedorDatosGestionGl();
                SubSonic.DataProvider pvdTransporteTerrestreProvider = SubSonic.DataService.GetInstance("pvdSTPTerrestre");
                SubSonic.DataProvider pvdSTPTerrestre = SubSonic.DataService.GetInstance("pvdSTPTerrestre");

                TransporteTerrestre.Datos.TerrestreSolicitud SolicitudExistente = TransporteTerrestre.Datos.TerrestreSolicitud.FetchByID(idSolicitudTerrestre);
                GestionGL.Datos.VwDUsuariosGeneralCollection SolicitanteExistente = new SubSonic.Select(pvdGestionGL).From<GestionGL.Datos.VwDUsuariosGeneral>().Where(GestionGL.Datos.VwDUsuariosGeneral.Columns.IdDUsuario).IsEqualTo(SolicitudExistente.FkIdSolicitante).ExecuteAsCollection<GestionGL.Datos.VwDUsuariosGeneralCollection>();
                GestionGL.Datos.VwCDAutorizadoresCollection AutorizadorExistente = new SubSonic.Select(pvdGestionGL).From<GestionGL.Datos.VwCDAutorizadores>().Where(GestionGL.Datos.VwCDAutorizadores.Columns.IdCDAutorizador).IsEqualTo(SolicitudExistente.FkIdAutorizador).ExecuteAsCollection<GestionGL.Datos.VwCDAutorizadoresCollection>();
                TransporteTerrestre.Datos.CatServicios ServicioExistente = TransporteTerrestre.Datos.CatServicios.FetchByID(SolicitudExistente.FkIdServicio);
                //Falta verificar el direccionamiento

                IdSolicitudTerrestre = idSolicitudTerrestre;
                FkIdServicio = SolicitudExistente.FkIdServicio;
                FkIdSolicitante = SolicitudExistente.FkIdSolicitante;
                FkIdAutorizador = SolicitudExistente.FkIdAutorizador;
                FkIdDireccionamiento = SolicitudExistente.FkIdDireccionamiento;
                CveServicio = ServicioExistente.CveServicio;
                FichaSolicitante = SolicitanteExistente[0].Ficha;
                Solicitante = SolicitanteExistente[0].Nombre;
                FichaAutorizador = AutorizadorExistente[0].Ficha;
                Autorizador = AutorizadorExistente[0].Nombres;
                Direccionamiento = "Falta Validar Direccionamiento";
                Folio = SolicitudExistente.Folio;
                InicioVigencia = SolicitudExistente.InicioVigencia;
                FinVigencia = SolicitudExistente.FinVigencia;
                Status = SolicitudExistente.Status;
                Observaciones = SolicitudExistente.Observaciones;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }
    }
}
