﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales
{
    public class SolicitudCollection
    {
        private System.Collections.Generic.List<TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.Solicitud> _ColeccionSolicitudes;

        public System.Collections.Generic.List<TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.Solicitud> ColeccionSolicitudes
        {
            get
            {
                return _ColeccionSolicitudes;
            }
        }

        public SolicitudCollection()
        {
            _ColeccionSolicitudes = new List<TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.Solicitud>();
        }

        public SolicitudCollection(TransporteTerrestre.Datos.TerrestreSolicitudCollection SolicitudesExistentes)
        {
            _ColeccionSolicitudes = new List<Solicitud>();
            for (int i = 0; i < SolicitudesExistentes.Count; i++)
            {
                _ColeccionSolicitudes.Add(new Solicitud(SolicitudesExistentes[i].IdSolicitudTerrestre));
            }
        }

        public static SolicitudCollection FiltraSolicitudes(string Filtro, string Subdivision, string Nodo_Usuario)
        {
            try
            {
                SolicitudCollection ValorRetorno = null;
                SubSonic.DataProvider pvdTransporteTerrestreProvider = SubSonic.DataService.GetInstance("pvdSTPTerrestre");
                TransporteTerrestre.Datos.TerrestreSolicitudCollection SolicitudesExistentes = new TransporteTerrestre.Datos.TerrestreSolicitudCollection();
                switch (Filtro)
                {
                    case "TODO": SolicitudesExistentes = new SubSonic.Select(pvdTransporteTerrestreProvider)
                        .From<TransporteTerrestre.Datos.TerrestreSolicitud>()
                        .Where(TransporteTerrestre.Datos.TerrestreSolicitud.Columns.Nodo)
                        .IsEqualTo(Nodo_Usuario)
                        //.And(TransporteTerrestre.Datos.TerrestreSolicitud.Columns.Status)
                        //.IsNotEqualTo("ATENDIDA")
                        .OrderDesc(TransporteTerrestre.Datos.TerrestreSolicitud.Columns.IdSolicitudTerrestre)
                        .ExecuteAsCollection<TransporteTerrestre.Datos.TerrestreSolicitudCollection>();
                	break;
                    case "SUBDIVISION": SolicitudesExistentes = new SubSonic.Select(pvdTransporteTerrestreProvider)
                        .From<TransporteTerrestre.Datos.TerrestreSolicitud>()
                        //.Where(TransporteTerrestre.Datos.TerrestreSolicitud.Columns.Status)
                        //.IsNotEqualTo("CANCELADA")
                        .Where(TransporteTerrestre.Datos.TerrestreSolicitud.Columns.Subdivision)
                        .Like(Subdivision.Substring(0, 3) + "%")
                        .And(TransporteTerrestre.Datos.TerrestreSolicitud.Columns.Nodo)
                        .IsEqualTo(Nodo_Usuario)
                        //.And(TransporteTerrestre.Datos.TerrestreSolicitud.Columns.Status)
                        //.IsNotEqualTo("ATENDIDA")
                        .OrderDesc(TransporteTerrestre.Datos.TerrestreSolicitud.Columns.IdSolicitudTerrestre)
                        .ExecuteAsCollection<TransporteTerrestre.Datos.TerrestreSolicitudCollection>();
                    break;
                }
                ValorRetorno = new SolicitudCollection(SolicitudesExistentes);
                return ValorRetorno;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

    }
}
