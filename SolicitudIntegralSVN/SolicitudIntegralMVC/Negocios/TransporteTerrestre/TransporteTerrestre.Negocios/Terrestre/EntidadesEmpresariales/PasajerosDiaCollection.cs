﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales
{
    public class PasajerosDiaCollection
    {
        private System.Collections.Generic.List<PasajerosDia> _ColeccionPasajerosDia;

        public System.Collections.Generic.List<PasajerosDia> ColeccionPasajerosDia
        {
            get
            {
                return _ColeccionPasajerosDia;
            }
        }

        public PasajerosDiaCollection()
        {
            _ColeccionPasajerosDia = new List<PasajerosDia>();
        }

        public void Agregar(int Dia, System.DateTime FechaDia, PasajeroMinimo Pasajero, System.DateTime InicioServicio, System.DateTime FinServicio, string Observaciones)
        {
            try
            {
                int? Indice = ObtenerIndicexDia(Dia);
                if (Indice != null && Pasajero != null)
                {
                    _ColeccionPasajerosDia[Convert.ToInt32(Indice)].Pasajeros.Add(Pasajero);
                }
                else
                {
                    _ColeccionPasajerosDia.Add(new PasajerosDia(Dia, FechaDia, Pasajero, InicioServicio, FinServicio, Observaciones));
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public int? ObtenerIndicexDia(int Dia)
        {
            try
            {
                int? Indice = null;
                for (int i = 0; i < ColeccionPasajerosDia.Count; i++)
                {
                    if (ColeccionPasajerosDia[i].CveDia == Dia)
                    {
                        Indice = i;
                    }
                }
                return Indice;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }

        public void Clonar(PasajerosDiaCollection PasajerosDiaClonar)
        {
            try
            {
                //Revisar la funcion de clonar pasajeros.
                PasajeroMinimo Minimo = null;
                System.Collections.Generic.List<PasajeroMinimo> Minimos = new List<PasajeroMinimo>();
                _ColeccionPasajerosDia = new List<PasajerosDia>();

                for (int i = 0; i < PasajerosDiaClonar.ColeccionPasajerosDia.Count; i++)
                {
                    for (int j = 0; j < PasajerosDiaClonar.ColeccionPasajerosDia[i].Pasajeros.Count; j++)
                    {
                        Minimo = new PasajeroMinimo();
                        Minimo.Ficha = PasajerosDiaClonar.ColeccionPasajerosDia[i].Pasajeros[j].Ficha;
                        Minimo.Nombre = PasajerosDiaClonar.ColeccionPasajerosDia[i].Pasajeros[j].Nombre;
                        Minimo.Tipo = PasajerosDiaClonar.ColeccionPasajerosDia[i].Pasajeros[j].Tipo;
                        Minimos.Add(Minimo);
                    }
                    _ColeccionPasajerosDia.Add(new PasajerosDia(PasajerosDiaClonar.ColeccionPasajerosDia[i].CveDia, PasajerosDiaClonar.ColeccionPasajerosDia[i].FechaDia, Minimos, PasajerosDiaClonar.ColeccionPasajerosDia[i].InicioServicio, PasajerosDiaClonar.ColeccionPasajerosDia[i].FinServicio, PasajerosDiaClonar.ColeccionPasajerosDia[i].Observaciones));
                    Minimos = new List<PasajeroMinimo>();
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }

        public void ClonarxIndice(PasajerosDiaCollection PasajerosDiaClonar, int CveDia, System.DateTime InicioServicio, System.DateTime FinServicio, string Observaciones)
        {
            try
            {
                int? Indice = PasajerosDiaClonar.ObtenerIndicexDia(CveDia);
                PasajeroMinimo Minimo = null;
                System.Collections.Generic.List<PasajeroMinimo> Minimos = new List<PasajeroMinimo>();
                //_ColeccionPasajerosDia = new List<PasajerosDia>();

                if (Indice != null)
                {
                    int indice = Convert.ToInt32(Indice);
                    for (int j = 0; j < PasajerosDiaClonar.ColeccionPasajerosDia[indice].Pasajeros.Count; j++)
                    {
                        Minimo = new PasajeroMinimo();
                        Minimo.Ficha = PasajerosDiaClonar.ColeccionPasajerosDia[indice].Pasajeros[j].Ficha;
                        Minimo.Nombre = PasajerosDiaClonar.ColeccionPasajerosDia[indice].Pasajeros[j].Nombre;
                        Minimo.Tipo = PasajerosDiaClonar.ColeccionPasajerosDia[indice].Pasajeros[j].Tipo;
                        Minimos.Add(Minimo);
                    }
                    _ColeccionPasajerosDia.Add(new PasajerosDia(PasajerosDiaClonar.ColeccionPasajerosDia[indice].CveDia, PasajerosDiaClonar.ColeccionPasajerosDia[indice].FechaDia, Minimos, InicioServicio, FinServicio, Observaciones));
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public void EliminarPasajero(int Dia, string Ficha)
        {
            try
            {
                //{ aqui modificado el 26/07/2012 eass
                PasajerosDia pasajerodia = (from x in _ColeccionPasajerosDia where x.CveDia == Dia select x).SingleOrDefault();
                if (pasajerodia != null)
                {
                    pasajerodia.Eliminar(Ficha);
                }
           }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public int Count()
        {
            try
            {
                return _ColeccionPasajerosDia.Count;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public void AgregarPasajeroPorDia(int Dia, PasajeroMinimo NuevoPasajero)
        {
            try
            {
                for (int i = 0; i < _ColeccionPasajerosDia.Count; i++)
                {
                    if (_ColeccionPasajerosDia[i].CveDia == Dia)
                    {
                        _ColeccionPasajerosDia[i].AgregarPasajero(NuevoPasajero);
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
    }
}
