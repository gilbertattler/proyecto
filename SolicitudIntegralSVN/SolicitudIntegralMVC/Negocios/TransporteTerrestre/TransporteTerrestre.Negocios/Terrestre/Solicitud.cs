﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;

namespace TransporteTerrestre.Negocios.Terrestre
{
    public class Solicitud
    {
        public static int InsertarSolicitudGeneral(TransporteTerrestre.Datos.TerrestreSolicitud Solicitud)
        {
            try
            {
                TransporteTerrestre.Datos.CatServicios servicio = TransporteTerrestre.Datos.CatServicios.FetchByID(Solicitud.FkIdServicio);

                Solicitud.FkIdCMInstalacionDestino = servicio.FkIdDestino;
                Solicitud.FkIdCMInstalacionOrigen = servicio.FkIdOrigen;
                
                Solicitud.Status = "SOLICITADO";
                Solicitud.Save();
                int ValorRetorno = Solicitud.IdSolicitudTerrestre;
                return ValorRetorno;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static int InsertarSolicitudDia(TransporteTerrestre.Datos.TerrestreSolicitudDia SolicitudDia)
        {
            try
            {
                SolicitudDia.BorradoLogico = false;
                SolicitudDia.Save();
                int ValorRetorno = SolicitudDia.IdSolicitudDia;
                return ValorRetorno;
            }
            catch (System.Exception ex)
            {
                throw  ex;
            }
        }

        public static void InsertarNuevaSolicitudPasajeros(TransporteTerrestre.Datos.TerrestreSolicitud Solicitud, TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection Pasajeros, TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection PasajerosRetorno, string Nodo_Usuario)
        {
            if ((Pasajeros.Count() + PasajerosRetorno.Count()) <= 0)
            {
                throw new System.Exception("No existen pasajeros en la solicitud.");
            }
            try
            {                
                int IdSolicitudGeneral = InsertarSolicitudGeneral(Solicitud);
                InsertarSolicitudDiaPasajeros(Pasajeros, IdSolicitudGeneral, "VIAJE",Nodo_Usuario);
                InsertarSolicitudDiaPasajeros(PasajerosRetorno, IdSolicitudGeneral, "RETORNO", Nodo_Usuario);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static void InsertarNuevaSolicitudPasajeros(TransporteTerrestre.Datos.TerrestreSolicitud Solicitud, TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection Pasajeros, TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection PasajerosRetorno,string Nodo_Usuario, bool Transaccionado)
        {
            try
            {
                if(Transaccionado)
                {
                    using (TransactionScope Scope = new TransactionScope())
                    {
                        using (SubSonic.SharedDbConnectionScope sharedConnectionScope = new SubSonic.SharedDbConnectionScope())
                        {
                            InsertarNuevaSolicitudPasajeros(Solicitud, Pasajeros, PasajerosRetorno, Nodo_Usuario);
                            Scope.Complete();
                        }
                    }
                }
                else
                {
                    InsertarNuevaSolicitudPasajeros(Solicitud, Pasajeros, PasajerosRetorno,Nodo_Usuario);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static void InsertarSolicitudDiaPasajeros(TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection Pasajeros, int IdSolicitud, string Movimiento, string Nodo_Usuario)
        {
            try
            {
                TransporteTerrestre.Datos.TerrestreSolicitudDia NuevaSolicitudDia = null;
                TransporteTerrestre.Datos.TerrestrePasajeros NuevoPasajero = null;
                int IdSolicitudDia;
                
                for (int i = 0; i < Pasajeros.ColeccionPasajerosDia.Count; i++)
                {
                    NuevaSolicitudDia = new TransporteTerrestre.Datos.TerrestreSolicitudDia();
                    NuevaSolicitudDia.FkSolicitudTerrestre = IdSolicitud;
                    NuevaSolicitudDia.CveDia = Pasajeros.ColeccionPasajerosDia[i].CveDia;
                    //NuevaSolicitudDia.Fecha = System.DateTime.Today.AddDays(i);
                    NuevaSolicitudDia.Fecha = Pasajeros.ColeccionPasajerosDia[i].InicioServicio.Date;
                    NuevaSolicitudDia.BorradoLogico = false;
                    NuevaSolicitudDia.InicioServicio = Pasajeros.ColeccionPasajerosDia[i].InicioServicio;
                    NuevaSolicitudDia.TerminoServicio = Pasajeros.ColeccionPasajerosDia[i].FinServicio;
                    NuevaSolicitudDia.Observaciones = Pasajeros.ColeccionPasajerosDia[i].Observaciones;
                    NuevaSolicitudDia.Movimiento = Movimiento;
                    NuevaSolicitudDia.Nodo = Nodo_Usuario;
                    IdSolicitudDia = InsertarSolicitudDia(NuevaSolicitudDia);
                    for (int j = 0; j < Pasajeros.ColeccionPasajerosDia[i].Pasajeros.Count; j++)
                    {
                        NuevoPasajero = new TransporteTerrestre.Datos.TerrestrePasajeros();
                        NuevoPasajero.BorradoLogico = false;
                        NuevoPasajero.Ficha = Pasajeros.ColeccionPasajerosDia[i].Pasajeros[j].Ficha;
                        NuevoPasajero.FkSolicitudDia = IdSolicitudDia;
                        NuevoPasajero.Nombre = Pasajeros.ColeccionPasajerosDia[i].Pasajeros[j].Nombre;
                        NuevoPasajero.Status = "SOLICITADO";
                        NuevoPasajero.Tipo = Pasajeros.ColeccionPasajerosDia[i].Pasajeros[j].Tipo;
                        NuevoPasajero.FkIdSolicitud = IdSolicitud;
                        NuevoPasajero.Nodo = Nodo_Usuario;
                        TransporteTerrestre.Negocios.Terrestre.Pasajero.InsertaPasajero(NuevoPasajero);
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static void ModificarStatus(int idSolicitudTerrestre, string NuevoStatus)
        {
            try
            {
                TransporteTerrestre.Datos.TerrestreSolicitud Solicitud = TransporteTerrestre.Datos.TerrestreSolicitud.FetchByID(idSolicitudTerrestre);

                if (Solicitud.Status == "ATENDIDA")
                {
                    throw new System.Exception("Solicitud atendida no se puede generar anexo.");
                }
                else
                {
                    Solicitud.Status = NuevoStatus;
                    Solicitud.Save();

                    TransporteTerrestre.Datos.TerrestrePasajerosCollection Pasajeros = new SubSonic.Select()
                        .From<TransporteTerrestre.Datos.TerrestrePasajeros>()
                        .Where(TransporteTerrestre.Datos.TerrestrePasajeros.FkIdSolicitudColumn)
                        .IsEqualTo(idSolicitudTerrestre)

                        .ExecuteAsCollection<TransporteTerrestre.Datos.TerrestrePasajerosCollection>();

                    for (int i = 0; i < Pasajeros.Count; i++)
                    {
                        if (Pasajeros[i].Status == "SOLICITADO")
                        {
                            Pasajeros[i].Status = NuevoStatus;
                        }
                    }
                    Pasajeros.SaveAll();
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static void ModificarStatus(int idSolicitudTerrestre, string NuevoStatus, bool Transaccionado)
        {
            try
            {
                SubSonic.DataProvider pvdTransporteTerrestre = SubSonic.DataService.GetInstance("pvdSTPTerrestre");
                if (Transaccionado)
                {
                    using (TransactionScope Scope = new TransactionScope())
                    {
                        using (SubSonic.SharedDbConnectionScope sharedConnectionScope = new SubSonic.SharedDbConnectionScope(pvdTransporteTerrestre))
                        {
                            ModificarStatus(idSolicitudTerrestre, NuevoStatus);
                            Scope.Complete();
                        }
                    }
                }
                else
                {
                    ModificarStatus(idSolicitudTerrestre, NuevoStatus);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static System.Collections.Generic.List<KeyValuePair<int, string>> ObtenerPasajerosNuevos(TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection Pasajeros, TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection EditarPasajeros)
        {
            try
            {
                bool Persiste = false;
                System.Collections.Generic.List<KeyValuePair<int, string>> DiaPasajero = new List<KeyValuePair<int, string>>();

                //Seccion que agrega nuevo Pasajeros Viaje
                for (int i = 0; i < Pasajeros.ColeccionPasajerosDia.Count; i++)
                {
                    for (int j = 0; j < EditarPasajeros.ColeccionPasajerosDia.Count; j++ )
                    {
                        if (Pasajeros.ColeccionPasajerosDia[i].CveDia == EditarPasajeros.ColeccionPasajerosDia[j].CveDia)
                        {
                            for (int k = 0; k < Pasajeros.ColeccionPasajerosDia[i].Pasajeros.Count; k++)
                            {
                                for (int l = 0; l < EditarPasajeros.ColeccionPasajerosDia[j].Pasajeros.Count; l++)
                                {
                                    Persiste = false;
                                    if (Pasajeros.ColeccionPasajerosDia[i].Pasajeros[k].Ficha == EditarPasajeros.ColeccionPasajerosDia[j].Pasajeros[l].Ficha)
                                    {
                                        Persiste = true;
                                        break;
                                    }
                                }
                                if (!Persiste)
                                {
                                    //Revisar el error en este punto.
                                    DiaPasajero.Add(new KeyValuePair<int, string>(Pasajeros.ColeccionPasajerosDia[i].CveDia, Pasajeros.ColeccionPasajerosDia[i].Pasajeros[k].Ficha));
                                }                                
                            }
                            break;
                        }
                    }
                }
                return DiaPasajero;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static System.Collections.Generic.List<KeyValuePair<int, string>> ObtenerPasajerosEliminados(TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection Pasajeros, TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection EditarPasajeros)
        {
            try
            {
                bool Persiste = false;
                //System.Collections.Generic.Dictionary<int, string> DiaPasajero = new Dictionary<int, string>();
                System.Collections.Generic.List<KeyValuePair<int, string>> DiaPasajero = new List<KeyValuePair<int, string>>();

                //Seccion que agrega nuevo Pasajeros Viaje
                for (int i = 0; i < EditarPasajeros.ColeccionPasajerosDia.Count; i++)
                {
                    for (int j = 0; j < Pasajeros.ColeccionPasajerosDia.Count; j++)
                    {
                        if (EditarPasajeros.ColeccionPasajerosDia[i].CveDia == Pasajeros.ColeccionPasajerosDia[j].CveDia)
                        {
                            for (int k = 0; k < EditarPasajeros.ColeccionPasajerosDia[i].Pasajeros.Count; k++)
                            {
                                for (int l = 0; l < Pasajeros.ColeccionPasajerosDia[j].Pasajeros.Count; l++)
                                {
                                    Persiste = false;
                                    if (EditarPasajeros.ColeccionPasajerosDia[i].Pasajeros[k].Ficha == Pasajeros.ColeccionPasajerosDia[j].Pasajeros[l].Ficha)
                                    {
                                        Persiste = true;
                                        break;
                                    }
                                }
                                if (!Persiste)
                                {
                                    DiaPasajero.Add(new KeyValuePair<int,string>(EditarPasajeros.ColeccionPasajerosDia[i].CveDia, EditarPasajeros.ColeccionPasajerosDia[i].Pasajeros[k].Ficha));
                                }
                            }
                            break;
                        }
                    }
                }
                return DiaPasajero;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static void ModificarSolicitud(TransporteTerrestre.Datos.TerrestreSolicitud Solicitud, TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection Pasajeros, TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection PasajerosRetorno, TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection EditarPasajeros, TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection EditarPasajerosRetorno, string Nodo_Usuario)
        {
            try
            {
                System.Collections.Generic.List<KeyValuePair<int, string>> PasajerosViajeNuevos = ObtenerPasajerosNuevos(Pasajeros,EditarPasajeros);
                System.Collections.Generic.List<KeyValuePair<int, string>> PasajerosRetornoNuevos = ObtenerPasajerosNuevos(PasajerosRetorno, EditarPasajerosRetorno);
                System.Collections.Generic.List<KeyValuePair<int, string>> PasajerosViajeEliminar = ObtenerPasajerosEliminados(Pasajeros, EditarPasajeros);
                System.Collections.Generic.List<KeyValuePair<int, string>> PasajerosRetornoEliminar = ObtenerPasajerosEliminados(PasajerosRetorno, EditarPasajerosRetorno);

                TransporteTerrestre.Negocios.Terrestre.Pasajero.EliminaPasajeros(Solicitud.IdSolicitudTerrestre, "VIAJE", PasajerosViajeEliminar, Nodo_Usuario);
                TransporteTerrestre.Negocios.Terrestre.Pasajero.EliminaPasajeros(Solicitud.IdSolicitudTerrestre, "RETORNO", PasajerosRetornoEliminar, Nodo_Usuario);
                TransporteTerrestre.Negocios.Terrestre.Pasajero.InsertaPasajeros(Solicitud.IdSolicitudTerrestre, "VIAJE", PasajerosViajeNuevos, Nodo_Usuario);
                TransporteTerrestre.Negocios.Terrestre.Pasajero.InsertaPasajeros(Solicitud.IdSolicitudTerrestre, "RETORNO", PasajerosRetornoNuevos, Nodo_Usuario);
                Solicitud.Save();
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static void ModificarSolicitud(TransporteTerrestre.Datos.TerrestreSolicitud Solicitud, TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection Pasajeros, TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection PasajerosRetorno, TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection EditarPasajeros, TransporteTerrestre.Negocios.Terrestre.EntidadesEmpresariales.PasajerosDiaCollection EditarPasajerosRetorno, bool Transaccionado, string Nodo_Usuario)
        {
            try
            {
                if(Transaccionado)
                {
                    using (TransactionScope Scope = new TransactionScope())
                    {
                        using (SubSonic.SharedDbConnectionScope sharedConnectionScope = new SubSonic.SharedDbConnectionScope())
                        {
                            ModificarSolicitud(Solicitud, Pasajeros, PasajerosRetorno, EditarPasajeros, EditarPasajerosRetorno, Nodo_Usuario);
                            Scope.Complete();
                        }
                    }
                }
                else
                {
                    ModificarSolicitud(Solicitud, Pasajeros, PasajerosRetorno, EditarPasajeros, EditarPasajerosRetorno, Nodo_Usuario);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
    }
}
