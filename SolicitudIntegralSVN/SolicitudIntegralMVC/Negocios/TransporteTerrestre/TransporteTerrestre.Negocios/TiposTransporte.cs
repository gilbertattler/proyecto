﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;

namespace TransporteTerrestre.Negocios
{
    public class TiposTransporte
    {
        public static int Insertar(string CveTipo, string DescripcionTipo, string Ficha, string IP, string Nodo_Usuario)
        {
            try
            {
                TransporteTerrestre.Datos.CatTipoTransportesCollection VerificaTipo = new SubSonic.Select()
                .From<TransporteTerrestre.Datos.CatTipoTransportes>()
                .Where(TransporteTerrestre.Datos.CatTipoTransportes.BorradoLogicoColumn)
                .IsEqualTo(false)
                .And(TransporteTerrestre.Datos.CatTipoTransportes.CveTipoColumn)
                .IsEqualTo(CveTipo)
                .ExecuteAsCollection<TransporteTerrestre.Datos.CatTipoTransportesCollection>();
                if (VerificaTipo.Count > 0)
                {
                    throw new Exception("Ya existe un tipo de Tipo de Transporte con la clave: " + CveTipo);
                }

                TransporteTerrestre.Datos.CatTipoTransportes TipoTransporte = new TransporteTerrestre.Datos.CatTipoTransportes();
                
                TipoTransporte.CveTipo = CveTipo;
                TipoTransporte.DescripcionTipo = DescripcionTipo;                
                TipoTransporte.BorradoLogico = false;
                TipoTransporte.Nodo = Nodo_Usuario;
                TipoTransporte.Save();


                //Guarda Auditoria
                TransporteTerrestre.Datos.TabAuditorias Auditoria = new TransporteTerrestre.Datos.TabAuditorias();
                Auditoria.Fecha = System.DateTime.Now;
                Auditoria.Ip = IP;
                Auditoria.IdTabla = TipoTransporte.IdTipoTransporte;
                Auditoria.Ficha = Ficha;
                Auditoria.TipoMovimiento = "Insertar";
                Auditoria.Descripcion = "Se inserto un tipo de transporte con el Id: " + TipoTransporte.IdTipoTransporte;
                Auditoria.Nodo = Nodo_Usuario;
                Auditoria.Save();


                return TipoTransporte.IdTipoTransporte;
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }
        }

        public static int Insertar(string CveTipo, string DescripcionTipo, string Ficha, string IP, bool Transaccionado,string Nodo_Usuario)
        {
            try
            {
                int ValorRetorno;
                if (Transaccionado)
                {
                    using (TransactionScope Scope = new TransactionScope())
                    {
                        using (SubSonic.SharedDbConnectionScope sharedConnectionScope = new SubSonic.SharedDbConnectionScope())
                        {
                            ValorRetorno = Insertar(CveTipo, DescripcionTipo,Ficha,IP,Nodo_Usuario);
                            Scope.Complete();
                        }
                    }
                }
                else
                {
                    ValorRetorno = Insertar(CveTipo, DescripcionTipo,Ficha,IP,Nodo_Usuario);
                }
                return ValorRetorno;
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }
        }

        public static void Guardar(string IdTipo, string CveTipo, string DescripcionTipo, string Ficha, string IP, string Nodo_Usuario)
        {
            try
            {
                TransporteTerrestre.Datos.CatTipoTransportesCollection VerificaTipo = new SubSonic.Select()
                .From<TransporteTerrestre.Datos.CatTipoTransportes>()
                .Where(TransporteTerrestre.Datos.CatTipoTransportes.BorradoLogicoColumn)
                .IsEqualTo(false)
                .And(TransporteTerrestre.Datos.CatTipoTransportes.CveTipoColumn)
                .IsEqualTo(CveTipo)
                .And(TransporteTerrestre.Datos.CatTipoTransportes.NodoColumn)
                .IsEqualTo(Nodo_Usuario)
                .ExecuteAsCollection<TransporteTerrestre.Datos.CatTipoTransportesCollection>();
                if (VerificaTipo.Count > 0)
                {
                    bool Mismo = false;
                    for (int i = 0; i < VerificaTipo.Count; i++)
                    {
                        if (VerificaTipo[i].IdTipoTransporte.ToString() == IdTipo)
                        {
                            Mismo = true;
                            break;
                        }
                    }
                    if (!Mismo)
                    {
                        throw new Exception("Ya existe un tipo de Tipo de Transporte con la clave: " + CveTipo);
                    }
                }

                TransporteTerrestre.Datos.CatTipoTransportes TiposTransporte = new TransporteTerrestre.Datos.CatTipoTransportes();
                TiposTransporte.IsNew = false;
                TiposTransporte.IsLoaded = true;
                TiposTransporte.IdTipoTransporte = Convert.ToInt32(IdTipo);
                TiposTransporte.CveTipo = CveTipo;
                TiposTransporte.DescripcionTipo = DescripcionTipo;
                TiposTransporte.BorradoLogico = false;
                TiposTransporte.Nodo = Nodo_Usuario; 
                TiposTransporte.Save();
                //TiposTransporte.Save();

                //Guarda Auditoria
                TransporteTerrestre.Datos.TabAuditorias Auditoria = new TransporteTerrestre.Datos.TabAuditorias();
                Auditoria.Fecha = System.DateTime.Now;
                Auditoria.Ip = IP;
                Auditoria.IdTabla = TiposTransporte.IdTipoTransporte;
                Auditoria.Ficha = Ficha;
                Auditoria.TipoMovimiento = "Modificar";
                Auditoria.Descripcion = "Se modifico el tipo de transporte con el Id: " + TiposTransporte.IdTipoTransporte;
                Auditoria.Nodo = Nodo_Usuario;
                Auditoria.Save();
                
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }
        }

        public static void Guardar(string IdTipo, string CveTipo, string DescripcionTipo, string Ficha, string IP, bool Transaccionado, string Nodo_Usuario)
        {
            try
            {
                if (Transaccionado)
                {
                    using (TransactionScope Scope = new TransactionScope())
                    {
                        using (SubSonic.SharedDbConnectionScope sharedConnectionScope = new SubSonic.SharedDbConnectionScope())
                        {
                            Guardar(IdTipo, CveTipo, DescripcionTipo,Ficha,IP,Nodo_Usuario);
                            Scope.Complete();
                        }
                    }
                }
                else
                {
                    Guardar(IdTipo, CveTipo, DescripcionTipo,Ficha,IP,Nodo_Usuario);
                }
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }
        }

        public static void InsertarGuardar(string IdTipo, string CveTipo, string DescripcionTipo, string Ficha, string IP, string Nodo_Usuario)
        {
            if (CveTipo == "")
            {
                throw new System.Exception("Debe proporcionar la clave del vehiculo.");
            }
            if (DescripcionTipo == "")
            {
                throw new System.Exception("Debe proporcionar la descripción del vehiculo.");
            }
            try
            {
                if (IdTipo == "Nuevo")
                {
                    Insertar(CveTipo, DescripcionTipo,Ficha,IP,Nodo_Usuario);
                }
                else
                {
                    Guardar(IdTipo, CveTipo, DescripcionTipo,Ficha,IP,Nodo_Usuario);
                }
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }

        }

        public static void InsertarGuardar(string IdTipo, string CveTipo, string DescripcionTipo, string Ficha, string IP, bool Transaccionado, string Nodo_Usuario)
        {
            try
            {
                if (Transaccionado)
                {
                    using (TransactionScope Scope = new TransactionScope())
                    {
                        using (SubSonic.SharedDbConnectionScope sharedConnectionScope = new SubSonic.SharedDbConnectionScope())
                        {
                            InsertarGuardar(IdTipo, CveTipo, DescripcionTipo,Ficha,IP,Nodo_Usuario);
                            Scope.Complete();
                        }
                    }
                }
                else
                {
                    InsertarGuardar(IdTipo, CveTipo, DescripcionTipo,Ficha,IP,Nodo_Usuario);
                }
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }
        }

        public static void Eliminar(int IdTipo, string Ficha, string IP, string Nodo_Usuario)
        {
            try
            {
                TransporteTerrestre.Datos.CatTipoTransportes Tipos = TransporteTerrestre.Datos.CatTipoTransportes.FetchByID(IdTipo);
                Tipos.BorradoLogico = true;
                Tipos.Save();

                //Guarda Auditoria
                TransporteTerrestre.Datos.TabAuditorias Auditoria = new TransporteTerrestre.Datos.TabAuditorias();
                Auditoria.Fecha = System.DateTime.Now;
                Auditoria.Ip = IP;
                Auditoria.IdTabla = Tipos.IdTipoTransporte;
                Auditoria.Ficha = Ficha;
                Auditoria.TipoMovimiento = "ELiminar";
                Auditoria.Descripcion = "Se elimino el tipo de transporte con el Id: " + Tipos.IdTipoTransporte;
                Auditoria.Nodo = Nodo_Usuario;
                Auditoria.Save();
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static void Eliminar(int IdTipo, string Ficha, string IP, bool Transaccionado,string Nodo_Usuario)
        {
            try
            {
                if (Transaccionado)
                {
                    using (TransactionScope Scope = new TransactionScope())
                    {
                        using (SubSonic.SharedDbConnectionScope sharedConnectionScope = new SubSonic.SharedDbConnectionScope())
                        {
                            Eliminar(IdTipo,Ficha,IP,Nodo_Usuario);
                            Scope.Complete();
                        }
                    }
                }
                else
                {
                    Eliminar(IdTipo, Ficha, IP,Nodo_Usuario);
                }
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }
        }
    }
}
