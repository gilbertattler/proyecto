﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GestionGL.Datos;
using System.Data;

namespace TransporteTerrestre.Negocios
{
    public partial class Usuarios
    {
        public static Usuarios ValidarUsuario(string ficha, string password)
        {
            Usuarios resultado = null;
            //DataSet dtsUsuarios;
            try
            {

                SubSonic.DataProvider pvdGestionGL = Utilerias.ObtenerProveedorDatosGestionGl();

                GestionGL.Datos.VwDUsuariosGeneralCollection Usuarios = new SubSonic.Select(pvdGestionGL, GestionGL.Datos.VwDUsuariosGeneral.Columns.IdMUsuario, GestionGL.Datos.VwDUsuariosGeneral.Columns.Nombre, GestionGL.Datos.VwDUsuariosGeneral.Columns.Ficha, GestionGL.Datos.VwDUsuariosGeneral.Columns.Pass, GestionGL.Datos.VwDUsuariosGeneral.Columns.Subdivision, GestionGL.Datos.VwDUsuariosGeneral.Columns.DescripcionCtoGestor, GestionGL.Datos.VwDUsuariosGeneral.Columns.IdCSubdivisionContratoCentroGestor, GestionGL.Datos.VwDUsuariosGeneral.Columns.CveAreaServicio, 
                                                                        GestionGL.Datos.VwDUsuariosGeneral.Columns.Nodo)
                                                                        .From<GestionGL.Datos.VwDUsuariosGeneral>()
                                                                        .Distinct()
                                                                        .Where(GestionGL.Datos.VwDUsuariosGeneral.Columns.Ficha).IsEqualTo(ficha)
                                                                        .And(GestionGL.Datos.VwDUsuariosGeneral.Columns.CveAreaServicio).IsEqualTo("STPVT")
                                                                        .And(GestionGL.Datos.VwDUsuariosGeneral.Columns.Pass).IsEqualTo(password)
                                                                        .And(GestionGL.Datos.VwDUsuariosGeneral.Columns.CveBajal).IsEqualTo("A")
                                                                        .And(GestionGL.Datos.VwDUsuariosGeneral.Columns.FecFinDUsuario).IsGreaterThan(System.DateTime.Now)
                                                                        .And(GestionGL.Datos.VwDUsuariosGeneral.Columns.IdCSubdivision).IsNotNull()
                                                                        .ExecuteAsCollection<GestionGL.Datos.VwDUsuariosGeneralCollection>();
                //GestionGL.Datos.VwDUsuariosGeneralCollection Usuarios = new SubSonic.Select("Ficha", "Pass", "Subdivision", "DescripcionCtoGestor").From<GestionGL.Datos.VwDUsuariosGeneral>().Distinct().Where(GestionGL.Datos.VwDUsuariosGeneral.Columns.Ficha).IsEqualTo(ficha).And(GestionGL.Datos.VwDUsuariosGeneral.Columns.Pass).IsEqualTo(password).And(GestionGL.Datos.VwDUsuariosGeneral.Columns.CveAreaServicio).In("AH", "STA", "STPVM", "SGL").ExecuteAsCollection<GestionGL.Datos.VwDUsuariosGeneralCollection>();

                if (Usuarios.Count > 0) 
                {
                    IEnumerable<int?> listado_id_congestores_diferentes =
                        (from dusuario in Usuarios
                         select dusuario.IdCSubdivisionContratoCentroGestor).Distinct<int?>();

                    GestionGL.Datos.VwCSubdivisionesContratosCentrosGestoresGeneralCollection listado_subdivisiones_usuario
                        = new SubSonic.Select(pvdGestionGL)
                        .From<GestionGL.Datos.VwCSubdivisionesContratosCentrosGestoresGeneral>()
                        .Where(GestionGL.Datos.VwCSubdivisionesContratosCentrosGestoresGeneral.Columns.IdCSubdivisionContratoCentroGestor)
                        .In(listado_id_congestores_diferentes)
                        .ExecuteAsCollection<GestionGL.Datos.VwCSubdivisionesContratosCentrosGestoresGeneralCollection>();

                    resultado = new Usuarios(Usuarios[0].IdMUsuario, Usuarios[0].Ficha, Usuarios[0].Nombre, Utilerias.ObtenerTipoTrabajador(Usuarios[0].Ficha), listado_subdivisiones_usuario, Usuarios[0].Nodo);
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            return resultado;
        }
    }
}
