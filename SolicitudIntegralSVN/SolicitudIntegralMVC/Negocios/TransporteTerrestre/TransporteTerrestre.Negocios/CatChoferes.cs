﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;

namespace TransporteTerrestre.Negocios
{
    public class CatChoferes
    {
        public static int Insertar(string Ficha, string Nombre, string Licencia, string TipoLicencia, string VigenciaLicencia, string FichaUsuario, string IP, string Nodo_Usuario)
        {
            try
            {
                TransporteTerrestre.Datos.CatChoferesCollection VerificaChofer = new SubSonic.Select().From<TransporteTerrestre.Datos.CatChoferes>().Where(TransporteTerrestre.Datos.CatChoferes.BorradoLogicoColumn).IsEqualTo(false).And(TransporteTerrestre.Datos.CatChoferes.FichaColumn).IsEqualTo(Ficha).ExecuteAsCollection<TransporteTerrestre.Datos.CatChoferesCollection>();
                if (VerificaChofer.Count > 0)
                {
                    throw new Exception("Ya existe un chofer con la ficha: " + Ficha);
                }

                TransporteTerrestre.Datos.CatChoferes Chofer = new TransporteTerrestre.Datos.CatChoferes();
                Chofer.Ficha = Ficha;
                Chofer.Nombre = Nombre;
                Chofer.Licencia = Licencia;
                Chofer.TipoLicencia = TipoLicencia;
                Chofer.VigenciaLicencia = Convert.ToDateTime(VigenciaLicencia);
                Chofer.BorradoLogico = false;
                Chofer.Nodo = Nodo_Usuario;
                Chofer.Save();

                //Guarda Auditoria
                TransporteTerrestre.Datos.TabAuditorias Auditoria = new TransporteTerrestre.Datos.TabAuditorias();
                Auditoria.Fecha = System.DateTime.Now;
                Auditoria.Ip = IP;
                Auditoria.IdTabla = Chofer.IdChofer;
                Auditoria.Ficha = FichaUsuario;
                Auditoria.TipoMovimiento = "Insertar";
                Auditoria.Descripcion = "Se inserto un nuevo chofer con el Id: " + Chofer.IdChofer;
                Auditoria.Nodo = Nodo_Usuario;
                Auditoria.Save();
                return Chofer.IdChofer;
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }
        }
        public static int Insertar(string Ficha, string Nombre, string Licencia, string TipoLicencia, string VigenciaLicencia, string FichaUsuario, string IP, bool Transaccionado,string Nodo_Usuario)
        {
            try
            {
                int ValorRetorno;
                if (Transaccionado)
                {
                    using (TransactionScope Scope = new TransactionScope())
                    {
                        using (SubSonic.SharedDbConnectionScope sharedConnectionScope = new SubSonic.SharedDbConnectionScope())
                        {
                            ValorRetorno = Insertar(Ficha, Nombre, Licencia, TipoLicencia, VigenciaLicencia, FichaUsuario, IP,Nodo_Usuario);
                            Scope.Complete();
                        }
                    }
                }
                else
                {
                    ValorRetorno = Insertar(Ficha, Nombre, Licencia, TipoLicencia, VigenciaLicencia, FichaUsuario, IP,Nodo_Usuario);
                }
                return ValorRetorno;
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }
        }
        public static void Guardar(string IdChofer, string Ficha, string Nombre, string Licencia, string TipoLicencia, string VigenciaLicencia, string FichaUsuario, string IP, string Nodo_Usuario)
        {
            try
            {
                TransporteTerrestre.Datos.CatChoferesCollection VerificaChofer = new SubSonic.Select().From<TransporteTerrestre.Datos.CatChoferes>().Where(TransporteTerrestre.Datos.CatChoferes.BorradoLogicoColumn).IsEqualTo(false).And(TransporteTerrestre.Datos.CatChoferes.FichaColumn).IsEqualTo(Ficha).ExecuteAsCollection<TransporteTerrestre.Datos.CatChoferesCollection>();
                if (VerificaChofer.Count > 0)
                {
                    bool Mismo = false;
                    for (int i = 0; i < VerificaChofer.Count; i++)
                    {
                        if (VerificaChofer[i].IdChofer.ToString() == IdChofer)
                        {
                            Mismo = true;
                            break;
                        }
                    }
                    if (!Mismo)
                    {
                        throw new Exception("Ya existe un chofer con la ficha: " + Ficha);
                    }
                }

                TransporteTerrestre.Datos.CatChoferes Chofer = new TransporteTerrestre.Datos.CatChoferes();
                Chofer.IsNew = false;
                Chofer.IsLoaded = true;
                Chofer.IdChofer = Convert.ToInt32(IdChofer);
                Chofer.Ficha = Ficha;
                Chofer.Nombre = Nombre;
                Chofer.Licencia = Licencia;
                Chofer.TipoLicencia = TipoLicencia;
                Chofer.VigenciaLicencia = Convert.ToDateTime(VigenciaLicencia);
                Chofer.BorradoLogico = false;
                Chofer.Nodo = Nodo_Usuario;
                Chofer.Save();

                //Guarda Auditoria
                TransporteTerrestre.Datos.TabAuditorias Auditoria = new TransporteTerrestre.Datos.TabAuditorias();
                Auditoria.Fecha = System.DateTime.Now;
                Auditoria.Ip = IP;
                Auditoria.IdTabla = Chofer.IdChofer;
                Auditoria.Ficha = FichaUsuario;
                Auditoria.TipoMovimiento = "Modificar";
                Auditoria.Descripcion = "Se modifico el chofer con el Id: " + Chofer.IdChofer;
                Auditoria.Nodo = Nodo_Usuario;
                Auditoria.Save();
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }
        }
        public static void Guardar(string IdChofer, string Ficha, string Nombre, string Licencia, string TipoLicencia, string VigenciaLicencia, string FichaUsuario, string IP, bool Transaccionado, string Nodo_Usuario)
        {
            try
            {
                if (Transaccionado)
                {
                    using (TransactionScope Scope = new TransactionScope())
                    {
                        using (SubSonic.SharedDbConnectionScope sharedConnectionScope = new SubSonic.SharedDbConnectionScope())
                        {
                            Guardar(IdChofer, Ficha, Nombre, Licencia, TipoLicencia, VigenciaLicencia, FichaUsuario, IP,Nodo_Usuario);
                            Scope.Complete();
                        }
                    }
                }
                else
                {
                    Guardar(IdChofer, Ficha, Nombre, Licencia, TipoLicencia, VigenciaLicencia, FichaUsuario, IP,Nodo_Usuario);
                }
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }
        }
        public static void InsertarGuardar(string IdChofer, string Ficha, string Nombre, string Licencia, string TipoLicencia, string VigenciaLicencia, string FichaUsuario, string IP, string Nodo_Usuario)
        {
            if (Ficha == "")
            {
                throw new System.Exception("Debe proporcionar la ficha/RFC.");
            }

            if (Nombre == "")
            {
                throw new System.Exception("Debe proporcionar el nombre.");
            }

            if (Licencia == "")
            {
                throw new System.Exception("Debe proporcionar el número de la licencia.");
            }

            if (VigenciaLicencia == "")
            {
                throw new System.Exception("Debe proporcionar la vigencia de la licencia.");
            }
            try
            {
                if (IdChofer == "Nuevo")
                {
                    Insertar(Ficha, Nombre, Licencia, TipoLicencia, VigenciaLicencia, FichaUsuario, IP,Nodo_Usuario);
                }
                else
                {
                    Guardar(IdChofer, Ficha, Nombre, Licencia, TipoLicencia, VigenciaLicencia, FichaUsuario, IP,Nodo_Usuario);
                }
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }

        }
        public static void InsertarGuardar(string IdChofer, string Ficha, string Nombre, string Licencia, string TipoLicencia, string VigenciaLicencia, string FichaUsuario, string IP, bool Transaccionado, string Nodo_Usuario)
        {
            try
            {
                if (Transaccionado)
                {
                    using (TransactionScope Scope = new TransactionScope())
                    {
                        using (SubSonic.SharedDbConnectionScope sharedConnectionScope = new SubSonic.SharedDbConnectionScope())
                        {
                            InsertarGuardar(IdChofer, Ficha, Nombre, Licencia, TipoLicencia, VigenciaLicencia, FichaUsuario, IP, Nodo_Usuario);
                            Scope.Complete();
                        }
                    }
                }
                else
                {
                    InsertarGuardar(IdChofer, Ficha, Nombre, Licencia, TipoLicencia, VigenciaLicencia, FichaUsuario, IP, Nodo_Usuario);
                }
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }

        }
        public static void Eliminar(int IdChofer, string FichaUsuario, string IP, string Nodo_Usuario)
        {
            try
            {
                TransporteTerrestre.Datos.CatChoferes Chofer = TransporteTerrestre.Datos.CatChoferes.FetchByID(IdChofer);
                Chofer.BorradoLogico = true;
                Chofer.Save();

                //Guarda Auditoria
                TransporteTerrestre.Datos.TabAuditorias Auditoria = new TransporteTerrestre.Datos.TabAuditorias();
                Auditoria.Fecha = System.DateTime.Now;
                Auditoria.Ip = IP;
                Auditoria.IdTabla = Chofer.IdChofer;
                Auditoria.Ficha = FichaUsuario;
                Auditoria.TipoMovimiento = "Eliminar";
                Auditoria.Descripcion = "Se elimino el chofer con el Id: " + Chofer.IdChofer;
                Auditoria.Nodo = Nodo_Usuario;
                Auditoria.Save();
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }
        }
        public static void Eliminar(int IdChofer, string FichaUsuario, string IP, bool Transaccionado, string Nodo_Usuario)
        {
            try
            {                
                if (Transaccionado)
                {
                    using (TransactionScope Scope = new TransactionScope())
                    {
                        using (SubSonic.SharedDbConnectionScope sharedConnectionScope = new SubSonic.SharedDbConnectionScope())
                        {
                            Eliminar(IdChofer, FichaUsuario, IP, Nodo_Usuario);
                            Scope.Complete();
                        }
                    }
                }
                else
                {
                    Eliminar(IdChofer, FichaUsuario, IP,Nodo_Usuario);
                }               
            }
            catch (System.Exception Ex)
            {
                throw Ex;
            }
        }
    }
}
